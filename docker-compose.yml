# See The Compose Specification here: https://github.com/compose-spec/compose-spec/blob/master/spec.md
version: '3.9'

services:
  # TODO: make app-review work
  app-review:
    build:
      context: ./
      dockerfile: packages/app-review/Dockerfile
    ports:
      - 3000:3000
    depends_on:
      localstack:
        condition: service_healthy
      postgres:
        condition: service_started
      gql-schema-registry:
        condition: service_healthy
      app-gql-gateway:
        condition: service_started
      service-editor-suggestion:
        condition: service_started
    env_file: ./packages/app-review/.env

  localstack:
    image: localstack/localstack:1.3.1
    hostname: xpub-review-localstack
    ports:
      - '4566:4566'
    environment:
      - SERVICES=sns,sqs,s3
      - DOCKER_HOST=unix:///var/run/docker.sock
      - LEGACY_PERSISTENCE=false
      - HOSTNAME_EXTERNAL=xpub-review-localstack
    volumes:
      - ./scripts/localstack/init-localstack.sh:/etc/localstack/init/ready.d/init-localstack.sh
      - ./tmp/localstack:/var/lib/localstack
      - /var/run/docker.sock:/var/run/docker.sock
    healthcheck:
      test:
        [
          'CMD',
          'awslocal',
          's3api',
          'wait',
          'bucket-exists',
          '--bucket',
          'large-events',
        ]
      interval: 5s
      timeout: 10s
      retries: 10
      start_period: 20s

  postgres:
    image: postgres:13
    ports:
      - 5432:5432
    environment:
      POSTGRES_USER: $USER
      POSTGRES_PASSWORD: secret
      POSTGRES_HOST_AUTH_METHOD: trust
    command: >
      -c ssl=on
      -c ssl_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem
      -c ssl_key_file=/etc/ssl/private/ssl-cert-snakeoil.key
    volumes:
      - postgres-volume:/var/lib/postgresql/data

  service-editor-suggestion:
    image: ${AWS_ACCOUNT}.dkr.ecr.${AWS_REGION}.amazonaws.com/service-editor-suggestion:latest
    platform: linux/amd64
    command: sh -c "yarn start"
    ports:
      - 3008:3000
    depends_on:
      localstack:
        condition: service_healthy
      postgres:
        condition: service_started
      gql-schema-registry:
        condition: service_healthy
    env_file: ./.service-editor-suggestion.env

  gql-schema-registry:
    image: pipedrive/graphql-schema-registry:2.0.0
    platform: linux/amd64
    ports:
      - 6001:3000
    environment:
      - NODE_ENV=production
      - DB_HOST=gql-schema-registry-db
      - DB_NAME=schema_registry
      - DB_PORT=3306
      - DB_SECRET=root
      - DB_USERNAME=root
      - PORT=3000
      - REDIS_HOST=gql-schema-registry-redis
      - REDIS_PORT=6004
    depends_on:
      - gql-schema-registry-redis
      - gql-schema-registry-db
    healthcheck:
      test: wget --no-verbose --tries=1 --spider http://localhost:3000 || exit 1
      interval: 10s
      timeout: 10s
      retries: 10

  gql-schema-registry-db:
    image: mysql:8
    command: mysqld --default-authentication-plugin=mysql_native_password --skip-mysqlx
    environment:
      MYSQL_ROOT_PASSWORD: root
      MYSQL_DATABASE: schema_registry
    ports:
      - 6000:3306
    volumes:
      - gql-schema-registry-db-volume:/var/lib/mysql
    healthcheck:
      test:
        ['CMD', 'mysqladmin', 'ping', '-h', 'localhost', '-u', 'healthcheck']
      timeout: 5s
      retries: 10

  gql-schema-registry-redis:
    image: redis:6-alpine
    ports:
      - 6004:6379

  app-gql-gateway:
    image: ${AWS_ACCOUNT}.dkr.ecr.${AWS_REGION}.amazonaws.com/app-gql-gateway:latest
    platform: linux/amd64
    command: sh -c "yarn start"
    ports:
      - 4001:3000
    environment:
      - PORT=3000
      - SCHEMA_REGISTRY_URL=http://gql-schema-registry:3000
      - NODE_ENV=development
    depends_on:
      gql-schema-registry:
        condition: service_healthy

  malware-scanner:
    image: ${AWS_DEV_ACCOUNT}.dkr.ecr.${AWS_REGION}.amazonaws.com/phenom-malware-scanner-svc:latest
    command: sh -c "yarn start-node"
    ports:
      - 3013:3000
    depends_on:
      localstack:
        condition: service_healthy
      postgres:
        condition: service_started
      clamav:
        condition: service_healthy
    env_file: ./.malware-scanner.env

  clamav:
    image: clamav/clamav:latest
    ports:
      - 3310:3310

volumes:
  gql-schema-registry-db-volume:
  postgres-volume:
    name: postgres-db
