#!/bin/bash
set -e

echo "Please login with HindawiDevelopment AWS account to be able to decrypt env files"
aws-azure-login --mode gui --profile HindawiDevelopment
export AWS_PROFILE=HindawiDevelopment

echo "Please provide an email address which will be used as admin user and default email address:"
read emailAddress
if [ -z "$emailAddress" ]
then
  echo "Email provided is empty, exiting..."
  exit 1
fi

sops -d .enc.env > .env
sops -d .enc.service-editor-suggestion.env > .service-editor-suggestion.env
sops -d .enc.malware-scanner.env > .malware-scanner.env
sops -d ./packages/app-review/.enc.env > ./packages/app-review/.env

sed -i.bak "s/\[DB_USER_PLACEHOLDER\]/$USER/g" ./.service-editor-suggestion.env
rm ./.service-editor-suggestion.env.bak

sed -i.bak "s/\[DB_USER_PLACEHOLDER\]/$USER/g" ./.malware-scanner.env
rm ./.malware-scanner.env.bak

sed -i.bak "s/\[DB_USER_PLACEHOLDER\]/$USER/g" ./packages/app-review/.env
sed -i.bak "s/\[EMAIL_ADDRESS_PLACEHOLDER\]/$emailAddress/g" ./packages/app-review/.env
rm ./packages/app-review/.env.bak