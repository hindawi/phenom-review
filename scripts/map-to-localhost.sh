#!/bin/bash
set -e

hostName=$1

function usage() {
    echo "
    Usage: map-to-localhost.sh <host name>
    "
    return
}

function appendHost() {
    hostsFile=/etc/hosts

    if grep -q "$hostName" "$hostsFile";
    then
        echo "Host \"$hostName\" already exists"
    else
        echo "127.0.0.1 $hostName" >> $hostsFile
        echo "Added \"$hostName\" to $hostsFile"
    fi
}

if [ -z ${hostName} ]; then
    usage
    exit 1
else
    appendHost
fi