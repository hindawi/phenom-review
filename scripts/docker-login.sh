#!/bin/bash

profile=$1
region=$2
account=$3

aws ecr get-login-password \
  --profile $profile \
  --region $region \
  | docker login \
    --username AWS \
    --password-stdin $account.dkr.ecr.$region.amazonaws.com

exit 0
