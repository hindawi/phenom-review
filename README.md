# Hindawi Peer Review

[Review](review.hindawi.com) is a system that allows authors to submit manuscripts and editors to manage the peer review for a particular journal on Hindawi.
For more details about Hindawi, please visit: https://www.hindawi.com/

## Structure

This repository is a monorepo containing Pubsweet components used and created by Hindawi.
The app entry point is `packages/app-review`, the place where the `xpub-review` setup and configurations are stored.

Docs and more information about Pubsweet can be found [here](https://pubsweet.coko.foundation/).

## Development

### Tech stack

**Client:** React, Typescript, GraphQL, Styled Components
<br />
**Server:** Express, ApolloServer, Knex.js

### Requirements

- [Node.js (v16)](https://nodejs.org/en/)
- [yarn](https://yarnpkg.com/)
- [Docker](https://docs.docker.com/desktop/mac/install/) - the more recent, the better; be sure to have `docker-compose up` version with --wait option
- [aws-azure-login](https://confluence.wiley.com/display/PPT/AWS+Access)
- brew - `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
- sops - `brew install sops`
- [Tableplus](https://tableplus.com/download), [Postbird](https://github.com/Paxa/postbird) or any other tool that offers you the possibility to access DBs via a GUI
- IDE: Webstorm / Visual Studio Code

### How to setup locally

1. Run `yarn setup`

   - be sure to have Docker up and running
   - will prompt you to login to AWS HindawiDevelopment
   - will prompt you to introduce an email for admin creation (use an existing keycloak user email or update its password after this script has finished)
   - will prompt you to introduce your machine password to add new hosts to /etc/hosts

### How to run it locally

1. Run `yarn dev`

   - be sure to have Docker up and running
   - will prompt you to login to AWS HindawiThinslices and/or HindawiDevelopment if some docker images are missing
   - in some rare situations `app-gql-gateway` will not start properly, you might see this by not being able to login, just rerun the command

### How to run it with malware-scanner

1. Go to `./scripts/start-containers.sh` and add:
   - `phenom-malware-scanner-svc` to the list of images required
   - `malware-scanner` to the list of containers

### How to update encrypted ENV files

1. Login to AWS HindawiDevelopment (also run `export AWS_PROFILE=HindawiDevelopment`)
2. Run `sops [YOUR_ENCRYPTED_FILE_HERE]` e.g. `sops .enc.env` - this will open the file (decrypted) in VIM where you can edited it

**OR**

1. Login to AWS HindawiDevelopment (also run `export AWS_PROFILE=HindawiDevelopment`)
2. Edit any decrypted ENV file that you want to change `sops -d .enc.env > .aux.env`
3. Run `sops -e [YOUR_FILE_HERE] > [YOUR_ENCRYPTED_FILE_HERE]` e.g. `sops -e .aux.env > .enc.env`

### How to manage migrations

In order to create a migration file do the following:

1. Use folder `./packages/app-review/migrations`
2. Add file named `${timestamp}-${name}(.sql/.js)` e.g. `1560160800-user-add-name-field.sql`

In order to execute it:

1. Run `yarn migrate`, this will run all migrations manually (note: successful migrations will not run twice, unless deleted from migrations table)

### How to manage seeds

In order to create a seed file do the following:

1. use folder `./packages/app-review/seeds`
2. add file named `${00}-${name}(.js)` e.g. `01-create-peer-review-models.js`

In order to execute it:

a) Run `yarn seeddb` - will run all the files from the `./packages/app-review/seeds` folder and will add the needed data in the database

b) Run `yarn seeddb --specific=01-create-peer-review-models.js` - Will run a specific seed to add data in the database

### Useful Scripts

- `yarn build` - compiles & builds the project
- `yarn test` - runs all unit tests
- `yarn lint` - runs linting
- `yarn start-containers` - starts all dependencies in docker containers
- `yarn stop-containers` - stops all docker containers
- `yarn cleandb` - cleans the database of all the tables, including pgboss jobs and runs migrations
- `yarn addusers [usertype]` - will add users with `EMAIL_SENDER` (from `.env`) + `usertype` (eg. john.smith+ae@hindawi.com). Multiple users can be added by separating names trough space (eg. `yarn addusers ce ae a`) and if no user is specified, the default users will be added (ea, ce, ae, a1, r1, r2). The passwords will be `Password1!` and can be changed.
- `yarn obfuscatedb` - obfuscates sensitive data (usually used for a production replica, but without any real user data) .

### Community

Join [the Mattermost channel](https://mattermost.coko.foundation/coko/channels/xpub) for discussion of xpub.
