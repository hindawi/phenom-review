import gql from 'graphql-tag'
import { userListFragment } from './fragments'

export const addUserFromAdminPanel = gql`
  mutation addUserFromAdminPanel($input: UserInput) {
    addUserFromAdminPanel(input: $input) {
      ...userListFragment
    }
  }
  ${userListFragment}
`

export const editUserFromAdminPanel = gql`
  mutation editUserFromAdminPanel($id: ID!, $input: EditUserInput!) {
    editUserFromAdminPanel(id: $id, input: $input) {
      ...userListFragment
    }
  }
  ${userListFragment}
`

export const activateUser = gql`
  mutation activateUser($id: ID!) {
    activateUser(id: $id) {
      ...userListFragment
    }
  }
  ${userListFragment}
`
export const deactivateUser = gql`
  mutation deactivate($id: ID!) {
    deactivateUser(id: $id) {
      ...userListFragment
    }
  }
  ${userListFragment}
`

export const assignEditorialRole = gql`
  mutation assignEditorialRole($input: AssignEditorialRoleInput!) {
    assignEditorialRole(input: $input)
  }
`
export const removeEditorialMember = gql`
  mutation removeEditorialMember($teamMemberId: ID!) {
    removeEditorialMember(teamMemberId: $teamMemberId)
  }
`

export const assignLeadEditorialAssistant = gql`
  mutation assignLeadEditorialAssistant($id: ID!) {
    assignLeadEditorialAssistant(id: $id)
  }
`

export const addJournal = gql`
  mutation addJournal($input: AddJournalInput!) {
    addJournal(input: $input) {
      id
    }
  }
`

export const editJournal = gql`
  mutation editJournal($id: ID!, $input: EditJournalInput!) {
    editJournal(id: $id, input: $input)
  }
`
export const addSection = gql`
  mutation addSection($journalId: String, $name: String!) {
    addSection(journalId: $journalId, name: $name) {
      id
      name
    }
  }
`
export const editSection = gql`
  mutation editSection($id: ID!, $input: EditSectionInput!) {
    editSection(id: $id, input: $input)
  }
`
export const addSpecialIssue = gql`
  mutation addSpecialIssue($input: AddSpecialIssueInput!) {
    addSpecialIssue(input: $input) {
      id
      name
      startDate
      endDate
      publicationDate
      callForPapers
      acronym
      isAnnual
      conflictOfInterest
      description
      topics
    }
  }
`
export const editSpecialIssue = gql`
  mutation editSpecialIssue($id: ID!, $input: EditSpecialIssueInput!) {
    editSpecialIssue(id: $id, input: $input) {
      id
      name
      startDate
      endDate
      publicationDate
      callForPapers
      acronym
      isAnnual
      conflictOfInterest
      description
      topics
    }
  }
`
export const cancelSpecialIssue = gql`
  mutation cancelSpecialIssue($id: ID!, $input: CancelSpecialIssueInput) {
    cancelSpecialIssue(id: $id, input: $input)
  }
`

export const publishSpecialIssue = gql`
  mutation publishSpecialIssue($id: ID!) {
    publishSpecialIssue(id: $id)
  }
`

export const deleteSpecialIssue = gql`
  mutation deleteSpecialIssue($id: ID!) {
    deleteSpecialIssue(id: $id)
  }
`
