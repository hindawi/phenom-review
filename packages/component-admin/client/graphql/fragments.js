import gql from 'graphql-tag'

export const userDetails = gql`
  fragment userDetails on User {
    id
    isActive
    isAdmin
    isRIPE
    identities {
      ... on Local {
        name {
          surname
          givenNames
          title
        }
        country
        email
        aff
        affRorId
        isConfirmed
      }
    }
  }
`

export const userListFragment = gql`
  fragment userListFragment on UserWithLocalIdentity {
    id
    isActive
    isConfirmed
    name {
      surname
      givenNames
    }
    email
    aff
    affRorId
  }
`
export const specialIssueFragment = gql`
  fragment specialIssueFragment on SpecialIssue {
    id
    name
    startDate
    endDate
    isCancelled
    isActive
    isPublished
    expirationDate
    publicationDate
    customId
    callForPapers
    section {
      id
    }
    leadGuestEditor {
      id
      alias {
        name {
          givenNames
          surname
        }
      }
    }
    hasManuscripts
    type
    acronym
    isAnnual
    conflictOfInterest
    description
    topics
  }
`

export const adminPanelJournalFragment = gql`
  fragment adminPanelJournalFragment on Journal {
    id
    name
    code
    apc
    issn
    email
    isActive
    activationDate
    articleTypes {
      id
      name
    }
    peerReviewModel {
      id
      name
    }
  }
`

export const journalFragment = gql`
  fragment journalFragment on Journal {
    id
    name
    code
    issn
    apc
    email
    activationDate
    articleTypes {
      id
      name
    }
    isActive
    peerReviewModel {
      id
      name
      hasSections
      hasTriageEditor
      triageEditorLabel
      academicEditorLabel
      hasFigureheadEditor
      figureheadEditorLabel
    }
    sections {
      id
      name
      sectionEditors {
        id
        alias {
          name {
            givenNames
            surname
          }
        }
      }
      specialIssues {
        ...specialIssueFragment
        section {
          id
          name
        }
      }
    }
    specialIssues {
      ...specialIssueFragment
    }
  }
  ${specialIssueFragment}
`
