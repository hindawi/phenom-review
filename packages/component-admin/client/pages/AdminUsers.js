import React, { Fragment } from 'react'
import { get } from 'lodash'
import { graphql } from 'react-apollo'
import styled, { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withModal, Modal } from 'component-modal'
import { compose, withHandlers, withProps } from 'recompose'
import { Spinner, Button } from '@pubsweet/ui'

import {
  Dropdown,
  DropdownOption,
  Breadcrumbs,
  Icon,
  Item,
  Label,
  MultiAction,
  Pagination,
  Row,
  ShadowedBox,
  Text,
  TextTooltip,
} from '@hindawi/ui'
import { parseError } from '../components/utils'

import { AdminAddUserForm, AdminEditUserForm, Search } from '../components'
import withUsersGQL from '../graphql'
import { currentUser } from '../graphql/queries'
import useUserPagination from '../components/useUserPagination'

const USERS_PER_PAGE = 20

const Users = ({
  currentUser,
  getUserName,
  getStatusLabel,
  emailStatus,
  toggleUserStatus,
  addUserFromAdmin,
  editUserFromAdmin,
}) => {
  const {
    users,
    page,
    toLast,
    toFirst,
    setPage,
    loading,
    prevPage,
    nextPage,
    totalUsers,
    searchValue,
    setSearchValue,
  } = useUserPagination(USERS_PER_PAGE)

  return (
    <Root>
      <Row alignItems="center" justify="space-between" mb={2}>
        <Item alignItems="center">
          <Breadcrumbs mr={1} path="/admin">
            <Text secondary>ADMIN DASHBOARD</Text>
          </Breadcrumbs>
        </Item>
      </Row>

      <CustomShadowedBox>
        <Row>
          <Search
            defaultValue={searchValue}
            onSearch={inputValue => {
              toFirst()
              setSearchValue(inputValue)
            }}
          />
          <Item justify="flex-end" mb={5} mt={5}>
            <Modal
              component={AdminAddUserForm}
              modalKey="addUserFromAdmin"
              onConfirm={addUserFromAdmin(toFirst)}
            >
              {showModal => (
                <Button
                  data-test-id="add-user-btn"
                  fontWeight={600}
                  medium
                  onClick={showModal}
                  primary
                >
                  <Icon fontSize="16px" icon="addUser" mr={1} />
                  Add User
                </Button>
              )}
            </Modal>
          </Item>
        </Row>

        {loading ? (
          <Spinner size={4} />
        ) : (
          renderUserTable({
            users,
            getUserName,
            emailStatus,
            getStatusLabel,
            editUserFromAdmin,
            toggleUserStatus,
            totalUsers,
            nextPage,
            page,
            prevPage,
            setPage,
            toFirst,
            toLast,
          })
        )}
      </CustomShadowedBox>
    </Root>
  )
}

const renderUserTable = ({
  users,
  getUserName,
  emailStatus,
  getStatusLabel,
  editUserFromAdmin,
  toggleUserStatus,
  totalUsers,
  nextPage,
  page,
  prevPage,
  setPage,
  toFirst,
  toLast,
}) =>
  users.length !== 0 ? (
    <Fragment>
      <Table>
        <tbody>
          <TableHeader>
            <Td>
              <Label>Full Name</Label>
            </Td>
            <Td colSpan={2}>
              <Label>Email</Label>
            </Td>
            <Td>
              <Label>Affiliation</Label>
            </Td>
            <Td>
              <Label>Status</Label>
            </Td>
            <Td>&nbsp;</Td>
          </TableHeader>

          {users.map(user => (
            <UserRow key={user.id}>
              <Td ml={14} width={240}>
                <WithEllipsis>
                  <TextTooltip title={getUserName(user)}>
                    <Text ellipsis>{getUserName(user)}</Text>
                  </TextTooltip>
                </WithEllipsis>
              </Td>
              <Td colSpan={2} data-test-id="email" ml={14} noWrap width={290}>
                <Item alignItems="baseline">
                  <WithEllipsis>
                    <TextTooltip title={user.email}>
                      <Text ellipsis mt={1} pr={0}>
                        {user.email}
                      </Text>
                    </TextTooltip>
                  </WithEllipsis>
                  <Text invited ml={1} mr={1} small>
                    {emailStatus(user)}
                  </Text>
                </Item>
              </Td>

              <Td ml={14} width={414}>
                <WithEllipsis>
                  <Text ellipsis>
                    <TextTooltip title={user.aff}>{user.aff}</TextTooltip>
                  </Text>
                </WithEllipsis>
              </Td>
              <Td ml={14} secondary width={64}>
                {getStatusLabel(user)}
              </Td>

              <HiddenCell>
                <Dropdown>
                  <Modal
                    component={AdminEditUserForm}
                    currentUser={currentUser}
                    edit
                    modalKey={`editUserFromAdmin-${user.id}`}
                    onConfirm={editUserFromAdmin}
                    user={user}
                  >
                    {showModal => (
                      <DropdownOption onClick={showModal}>
                        Edit User
                      </DropdownOption>
                    )}
                  </Modal>
                  {user.id !== currentUser.id && (
                    <DropdownOption onClick={toggleUserStatus(user)}>
                      {user.isActive ? 'Deactivate' : 'Activate'}
                    </DropdownOption>
                  )}
                </Dropdown>
              </HiddenCell>
            </UserRow>
          ))}
        </tbody>
      </Table>
      {totalUsers > USERS_PER_PAGE ? (
        <Item justify="flex-end" mb={2}>
          <Pagination
            itemsPerPage={USERS_PER_PAGE}
            nextPage={nextPage}
            page={page}
            prevPage={prevPage}
            setPage={setPage}
            toFirst={toFirst}
            toLast={toLast}
            totalCount={totalUsers}
          />
        </Item>
      ) : null}
    </Fragment>
  ) : (
    <Row my={5}>
      <StyledText>No users found</StyledText>
    </Row>
  )

export default compose(
  withUsersGQL,
  withModal({
    component: MultiAction,
    modalKey: 'deactivateUserFromAdmin',
  }),

  graphql(currentUser),
  withProps(({ data }) => ({
    currentUser: get(data, 'currentUser', {}),
  })),
  withHandlers({
    emailStatus: () => ({ isConfirmed }) => {
      if (!isConfirmed) return 'INVITED'
    },
    getStatusLabel: () => ({ admin, isConfirmed, isActive = true }) => {
      if (admin) return <Text customId>ACTIVE</Text>
      return isActive ? (
        <Text customId>ACTIVE</Text>
      ) : (
        <Text error>INACTIVE</Text>
      )
    },
    addUserFromAdmin: ({ addUserFromAdminPanel }) => toFirst => (
      input,
      { setFetching, hideModal, setError, clearError },
    ) => {
      clearError()
      setFetching(true)
      addUserFromAdminPanel({
        variables: {
          input,
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
          toFirst()
        })
        .catch(e => {
          setFetching(false)
          setError(parseError(e))
        })
    },
    editUserFromAdmin: ({ editUserFromAdminPanel }) => (
      {
        id,
        givenNames,
        surname,
        aff,
        affRorId,
        title,
        country,
        isAdmin,
        isRIPE,
      },
      { hideModal, setFetching, setError, clearError },
    ) => {
      clearError()
      setFetching(true)
      editUserFromAdminPanel({
        variables: {
          id,
          input: {
            givenNames,
            surname,
            aff,
            affRorId,
            title,
            country,
            isAdmin,
            isRIPE,
          },
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
        })
        .catch(e => {
          setFetching(false)
          setError(parseError(e))
        })
    },
    getUserName: () => user => {
      if (user.admin) {
        return 'Admin'
      }
      return `${user.givenNames || ''} ${user.surname || ''}`
    },
  }),
  withHandlers({
    toggleUserStatus: ({
      activateUser,
      deactivateUser,
      getUserName,
      showModal,
    }) => user => () => {
      showModal({
        modalKey: 'deactivateUserFromAdmin',
        title: `Are you sure you want to ${
          user.isActive ? 'deactivate' : 'activate'
        } user?`,
        subtitle: getUserName(user),
        confirmText: 'YES',
        cancelText: 'CLOSE',
        onConfirm: ({ hideModal, setFetching, setError, clearError }) => {
          clearError()
          setFetching(true)
          user.isActive
            ? deactivateUser({
                variables: {
                  id: user.id,
                },
              })
                .then(() => {
                  setFetching(false)
                  hideModal()
                })
                .catch(e => {
                  setFetching(false)
                  setError(parseError(e))
                })
            : activateUser({
                variables: {
                  id: user.id,
                },
              })
                .then(() => {
                  setFetching(false)
                  hideModal()
                })
                .catch(e => {
                  setFetching(false)
                  setError(parseError(e))
                })
        },
      })
    },
  }),
)(Users)
// #endregion

// #region styled-components
const colorFn = props => {
  if (props.secondary) {
    return th('actionSecondaryColor')
  }

  if (props.customId) {
    return th('actionPrimaryColor')
  }

  return th('colorText')
}

const noWrap = props => {
  if (props.noWrap) {
    return css`
      white-space: nowrap;
    `
  }
}

const Table = styled.table`
  border-collapse: collapse;
  border-left: 1px solid ${th('colorBorder')};
  border-right: 1px solid ${th('colorBorder')};
  margin-bottom: calc(${th('gridUnit')} * 8);
  min-width: 100%;

  & th {
    border: none;
    height: calc(${th('gridUnit')} * 10);
    padding-left: calc(${th('gridUnit')} * 4);
    text-align: start;
    vertical-align: middle;
  }
`

const Td = styled.td`
  border: none;
  color: ${colorFn};
  height: calc(${th('gridUnit')} * 10);
  padding-left: calc(${th('gridUnit')} * 4);
  text-align: start;
  vertical-align: middle;

  ${noWrap}
`

const WithEllipsis = styled(Item)`
  display: inline-grid;
`
const HiddenCell = styled(Td)`
  display: flex;
  flex-direction: row;
  margin-right: calc(${th('gridUnit')} * 8);
  margin-left: calc(${th('gridUnit')});
  align-items: center;
  display: flex;
  justify-content: flex-start;
  opacity: 0;
`
const TableHeader = styled.tr`
  border-bottom: 1px solid ${th('colorBorder')};
  border-top: 1px solid ${th('colorBorder')};
  font-family: 'Nunito';
`
const UserRow = styled.tr`
  border-bottom: 1px solid ${th('colorBorder')};
  font-family: 'Nunito';
  &:hover {
    background-color: ${th('labelColor')};

    ${HiddenCell} {
      opacity: 1;
    }
  }
`
const CustomShadowedBox = styled(ShadowedBox)`
  min-width: 100%;
`

const StyledText = styled(Text)`
  font-weight: 700;
  font-size: 20px;
  color: ${th('actionSecondaryColor')};
`

const Root = styled.div`
  padding: 0 calc(${th('gridUnit')} * 14);
  padding-top: calc(${th('gridUnit')} * 4);
`
// #endregion
