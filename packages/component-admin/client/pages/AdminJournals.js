import React, { Fragment } from 'react'
import styled from 'styled-components'
import { Modal } from 'component-modal'
import { th } from '@pubsweet/ui-toolkit'
import { Button, TextField } from '@pubsweet/ui'
import { compose, withHandlers } from 'recompose'
import {
  Row,
  Item,
  Loader,
  Pagination,
  Breadcrumbs,
  withFetching,
  ShadowedBox,
  usePaginatedItems,
} from '@hindawi/ui'

import { parseError } from '../components/utils'
import { Journals, AdminJournalForm } from '../components/'
import withJournals from '../graphql/withJournalsGQL'

const filterJournals = (searchQuery = '') => journal => {
  const whereToSearch = `${journal.name}${journal.issn}${journal.email}`.toLowerCase()
  return whereToSearch.includes(searchQuery.trim().toLowerCase())
}

const JournalsPage = ({
  history,
  addJournal,
  editJournal,
  data: { getJournals, loading },
  isFetching,
}) => {
  const {
    totalCount,
    searchQuery,
    clearSearch,
    handleSearch,
    paginatedItems,
    ...pagination
  } = usePaginatedItems({
    itemsPerPage: 10,
    items: getJournals,
    filterFn: filterJournals,
  })

  return (
    <Root>
      <Row alignItems="center" justify="space-between">
        <Item alignItems="center" my={6}>
          <Breadcrumbs path="/admin">ADMIN DASHBOARD</Breadcrumbs>
        </Item>
      </Row>
      <CustomShadowedBox>
        <Row mb={6}>
          <Item>
            <TextField
              onChange={handleSearch}
              placeholder="Search by Name, Email or ISSN"
              value={searchQuery}
            />
            <Button invert ml={4} onClick={clearSearch} secondary small>
              Clear
            </Button>
          </Item>
          <Item alignItems="center" justify="flex-end">
            <Modal
              addJournal={addJournal}
              component={AdminJournalForm}
              confirmText="ADD JOURNAL"
              isFetching={isFetching}
              modalKey="addJournal"
              title="Add Journal"
            >
              {showModal => (
                <Button
                  data-test-id="add-journal"
                  medium
                  onClick={showModal}
                  primary
                  width={36}
                >
                  Add Journal
                </Button>
              )}
            </Modal>
          </Item>
        </Row>
        {loading ? (
          <Row>
            <Loader />
          </Row>
        ) : (
          <Fragment>
            <Journals
              editJournal={editJournal}
              history={history}
              journals={paginatedItems}
            />
            <Row justify="flex-end">
              <Pagination totalCount={totalCount} {...pagination} />
            </Row>
          </Fragment>
        )}
      </CustomShadowedBox>
    </Root>
  )
}

export default compose(
  withFetching,
  withJournals,
  withHandlers({
    addJournal: ({ addJournal }) => (
      {
        code,
        issn,
        name,
        email,
        articleTypes,
        activationDate,
        peerReviewModelId,
      },
      { setFetching, hideModal, setError, clearError },
    ) => {
      clearError()
      setFetching(true)
      addJournal({
        variables: {
          input: {
            code,
            issn,
            name,
            email,
            articleTypes,
            activationDate,
            peerReviewModelId,
          },
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
        })
        .catch(error => {
          setFetching(false)
          setError(parseError(error))
        })
    },

    editJournal: ({ editJournal }) => (
      {
        id,
        name,
        code,
        issn,
        email,
        triageEditorUserId,
        articleTypes,
        activationDate,
        peerReviewModelId,
      },
      { setFetching, hideModal, setError, clearError },
    ) => {
      clearError()
      setFetching(true)
      editJournal({
        variables: {
          id,
          input: {
            name,
            code,
            issn,
            email,
            triageEditorUserId,
            articleTypes,
            activationDate,
            peerReviewModelId,
          },
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
        })
        .catch(error => {
          setFetching(false)
          setError(parseError(error))
        })
    },
  }),
)(JournalsPage)

const Root = styled.div`
  padding: 0 calc(${th('gridUnit')} * 14);
`

const CustomShadowedBox = styled(ShadowedBox)`
  min-width: 100%;
`
