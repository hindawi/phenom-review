/* eslint-disable @typescript-eslint/no-use-before-define */
import React, { Fragment, useState, useContext } from 'react'
import moment from 'moment'
import { H3 } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Row, Text, Icon, Item, Label, TextTooltip } from '@hindawi/ui'
import {
  IconSave,
  IconDelete,
  IconAddUser,
  Button,
  ConfirmationModal,
} from '@hindawi/phenom-ui'
import { Modal } from 'component-modal'
import { CancelSpecialIssueModal } from '../components'
import useCancelSpecialIssue from './useCancelSpecialissue'
import useDeleteSpecialIssue from './useDeleteSpecialissue'
import usePublishSpecialIssue from './usePublishSpecialissue'
import SpecialIssueContext from './SpecialIssueContext'
import JournalTabContext from './JournalTabContext'

const DATE_FORMAT = 'YYYY-MM-DD'
const FULL_FUNCTIONALITY =
  (process.env.FEATURE_PPBK4962_SPECIAL_ISSUE || '').toString() === 'true'

const SpecialIssueCard = ({ specialIssue, journalId }) => {
  const {
    id = '',
    name = '',
    startDate = '',
    endDate = '',
    expirationDate = '',
    isActive,
    isCancelled,
    isPublished,
    leadGuestEditor = {},
    acronym,
    customId,
    hasManuscripts,
  } = specialIssue
  const sectionName = specialIssue.section?.name

  const { handleCancelSpecialIssue } = useCancelSpecialIssue(id, journalId)
  const { handleDeleteSpecialIssue } = useDeleteSpecialIssue(id, journalId)
  const { handlePublishSpecialIssue } = usePublishSpecialIssue(id, journalId)
  const switchJournalTab = useContext(JournalTabContext)
  const showDrawer = useContext(SpecialIssueContext)
  const [
    { isModalPublishSIOpen, isModalDeleteSIOpen, isPublishing, isDeleting },
    setState,
  ] = useState(false)

  const showPublishSpecialIssueModal = () => {
    setState(prevState => ({ ...prevState, isModalPublishSIOpen: true }))
  }
  const showDeleteSpecialIssueModal = () => {
    setState(prevState => ({ ...prevState, isModalDeleteSIOpen: true }))
  }
  const goToAssignEditors = () => {
    switchJournalTab(sectionName ? 2 : 1)
  }
  const dismissPublishModal = () => {
    setState(prevState => ({
      ...prevState,
      isModalPublishSIOpen: false,
    }))
  }
  const dismissDeleteModal = () => {
    setState(prevState => ({ ...prevState, isModalDeleteSIOpen: false }))
  }
  const publishSI = () => {
    handlePublishSpecialIssue({
      setFetching: isPublishing =>
        setState(prevState => ({ ...prevState, isPublishing })),
      setError: err => console.error(err),
      hideModal: dismissPublishModal,
    })
  }
  const deleteSI = () => {
    handleDeleteSpecialIssue({
      setFetching: isDeleting =>
        setState(prevState => ({ ...prevState, isDeleting })),
      setError: err => {
        setState(prevState => ({ ...prevState, isDeleting: false }))
        console.error(err)
      },
      hideModal: dismissDeleteModal,
    })
  }
  return (
    <Card data-test-id={`special-issue-${id}`} justify="center" mb={5} mt={3}>
      <Row alignItems="baseline">
        <NameWrapper
          alignItems="center"
          display="flex"
          justify-content="center"
        >
          <TextTooltip title={name}>
            <CustomH3 ellipsis mt={1} title={name}>
              {name}
            </CustomH3>
          </TextTooltip>
        </NameWrapper>

        {!isCancelled && isActive && (
          <Modal
            component={CancelSpecialIssueModal}
            handleCancelSpecialIssue={handleCancelSpecialIssue}
            modalKey={`cancelSpecialIssue-${id}`}
          >
            {showModal => (
              <TextTooltip title="Cancel">
                <CustomIcon
                  data-test-id="cancel-special-issue"
                  fontSize="16px"
                  icon="remove"
                  onClick={event => {
                    event.stopPropagation()
                    showModal()
                  }}
                />
              </TextTooltip>
            )}
          </Modal>
        )}
        <CustomIcon
          data-test-id="edit-special-issue"
          fontSize="16px"
          icon="edit"
          onClick={() => showDrawer({ specialIssueId: id, journalId })}
        />
      </Row>
      {!isPublished &&
        FULL_FUNCTIONALITY &&
        /* only allow publishing and deleting 'new' style SIs */
        !!acronym && (
          <Fragment>
            <Row alignItems="start">
              <ItemNoGrow pb={2} pl={4}>
                <CompactButton
                  icon={<IconSave />}
                  onClick={showPublishSpecialIssueModal}
                  size="small"
                  type="link"
                >
                  Publish Special Issue
                </CompactButton>
              </ItemNoGrow>
              {!hasManuscripts && (
                <ItemNoGrow pb={2} pl={4}>
                  <CompactGrayButton
                    icon={<IconDelete />}
                    onClick={showDeleteSpecialIssueModal}
                    size="small"
                    type="link"
                  >
                    Delete
                  </CompactGrayButton>
                </ItemNoGrow>
              )}
              <Item data-filler />
            </Row>
            <ConfirmationModal
              cancelButtonProps={{ disabled: isDeleting }}
              cancelText="CANCEL"
              icon="error"
              okButtonProps={{ danger: true, disabled: isDeleting }}
              okText="YES, DELETE SPECIAL ISSUE"
              onCancel={dismissDeleteModal}
              onOk={deleteSI}
              subtitle="Are you sure you want to permanentely delete the Special Issue?"
              title="Delete Special Issue"
              visible={isModalDeleteSIOpen}
              width={600}
            />
            {leadGuestEditor ? (
              <ConfirmationModal
                cancelButtonProps={{ disabled: isPublishing }}
                cancelText="CANCEL"
                confirmLoading={isPublishing}
                icon="info"
                okButtonProps={{ disabled: isPublishing }}
                okText="PUBLISH"
                onCancel={dismissPublishModal}
                onOk={publishSI}
                subtitle={
                  <Fragment>
                    <p>The Special Issue will be published on the website.</p>
                    <p>Do you want to continue?</p>
                  </Fragment>
                }
                title="Publish Special Issue"
                visible={isModalPublishSIOpen}
                width={600}
              />
            ) : (
              <ConfirmationModal
                cancelText="CANCEL"
                confirmLoading={isDeleting}
                icon="alert"
                okText="ASSIGN LEAD GUEST EDITOR"
                onCancel={dismissPublishModal}
                onOk={goToAssignEditors}
                subtitle="The Special Issue can only be published in the journal after assigning a Lead Guest Editor."
                title="Assign Lead Guest Editor"
                visible={isModalPublishSIOpen}
                width={600}
              />
            )}
          </Fragment>
        )}
      <Row alignItems="baseline">
        <Item alignItems="flex-start" pb={2} pl={4}>
          {isCancelled && <Text error>CANCELLED</Text>}
          {!isActive && !isCancelled && <Text secondary>CLOSED</Text>}
          {isActive && !isCancelled && (
            <WithCorrectFontSize customId>OPEN</WithCorrectFontSize>
          )}
        </Item>
      </Row>
      <GreyBox>
        <CustomRow justify="flex-start">
          <Item alignItems="flex-start" flex={0} pr={4} vertical>
            <StyledLabel mb={2}>Submission start date</StyledLabel>
            <Text whiteSpace="nowrap">
              {moment(startDate).format(DATE_FORMAT)}
            </Text>
          </Item>
          <VerticalSeparator />
          <Item alignItems="flex-start" flex={0} pl={4} pr={4} vertical>
            <StyledLabel mb={2}>Submission end date</StyledLabel>
            <Text whiteSpace="nowrap">
              {moment(endDate).format(DATE_FORMAT)}
            </Text>
          </Item>
          <VerticalSeparator />
          <Item alignItems="flex-start" flex={0} pl={4} pr={4} vertical>
            <StyledLabel mb={2}>Expiration date</StyledLabel>
            <Text whiteSpace="nowrap">
              {moment(expirationDate).format(DATE_FORMAT)}
            </Text>
          </Item>
          <VerticalSeparator />
          <Item alignItems="flex-start" flex={0} pl={4} pr={4} vertical>
            <StyledLabel mb={2}>Special Issue ID</StyledLabel>
            <Text whiteSpace="nowrap">{customId}</Text>
          </Item>
          {sectionName && (
            <Fragment>
              <VerticalSeparator />
              <Item
                alignItems="flex-start"
                flex={1}
                flexWrap="wrap"
                minWidth={0}
                pl={4}
              >
                <StyledLabel mb={2}>Section</StyledLabel>
                <WithEllipsis>{sectionName}</WithEllipsis>
              </Item>
            </Fragment>
          )}
          <Item alignItems="flex-end" pr={3} vertical>
            <Label mb={2}>Lead Guest Editor</Label>
            {leadGuestEditor ? (
              <Text whiteSpace="nowrap">
                {`${leadGuestEditor.alias?.name?.givenNames || ''}
                      ${leadGuestEditor.alias?.name?.surname || ''}`}
              </Text>
            ) : (
              <CompactButton
                icon={<IconAddUser />}
                onClick={goToAssignEditors}
                size="small"
                type="link"
              >
                Assign editor
              </CompactButton>
            )}
          </Item>
        </CustomRow>
      </GreyBox>
    </Card>
  )
}

export default SpecialIssueCard

// #region styles

const CustomIcon = styled(Icon)`
  opacity: 0;
  display: block;
  padding: 0 calc(${th('gridUnit')} * 3) calc(${th('gridUnit')} * 3)
    calc(${th('gridUnit')} * 3);
  align-items: center;
`

const StyledLabel = styled(Label)`
  width: 100%;
  > h4 {
    white-space: nowrap;
  }
`

const WithCorrectFontSize = styled(Text)`
  font-size: 14px;
`

const WithEllipsis = styled(Text)`
  display: block;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`
const Card = styled.div`
  flex: 1;
  border: 1px solid ${th('disabledColor')};
  box-shadow: none;
  border-radius: 3px;
  padding: 0;
  margin-bottom: calc(${th('gridUnit')} * 2);

  &:hover {
    box-shadow: ${th('shadows.shadowMedium')};

    ${CustomIcon} {
      opacity: 1;
    }
`
const GreyBox = styled.div`
  background-color: ${th('backgroundColor')};
  border-bottom-left-radius: 3px;
  border-bottom-right-radius: 3px;
`
const CustomRow = styled(Row)`
  padding: calc(${th('gridUnit')} * 2) 0 calc(${th('gridUnit')} * 2)
    calc(${th('gridUnit')} * 4);
`
const NameWrapper = styled(Item)`
  padding: calc(${th('gridUnit')} * 2) 0 calc(${th('gridUnit')} * 2)
    calc(${th('gridUnit')} * 4);
`

const CustomH3 = styled(H3)`
  display: inline-block;
`

const ItemNoGrow = styled(Item)`
  flex-grow: 0;
`

const CompactButton = styled(Button)`
  font-weight: initial;
  padding: 0;
`

const CompactGrayButton = styled(CompactButton)`
  color: rgb(62, 62, 62);
`

const VerticalSeparator = styled.div`
  border-left: 1px solid ${th('colorFurniture')};
  height: calc(${th('gridUnit')} * 12);
  margin: 0px calc(${th('gridUnit')} * 2);
`

// #endregion
