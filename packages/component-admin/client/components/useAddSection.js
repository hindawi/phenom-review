import { useMutation } from 'react-apollo'
import { mutations, queries } from '../graphql'
import { parseError } from './utils'

const useAddSection = journalId => {
  const [addSection] = useMutation(mutations.addSection, {
    refetchQueries: [
      {
        query: queries.getJournal,
        variables: {
          journalId,
        },
      },
    ],
  })

  const handleAddSection = (values, modalProps) => {
    const { name } = values
    modalProps.setFetching(true)
    addSection({
      variables: {
        journalId,
        name,
      },
    })
      .then(() => {
        modalProps.setFetching(false)
        modalProps.hideModal()
      })
      .catch(error => {
        modalProps.setFetching(false)
        modalProps.setError(parseError(error))
      })
  }

  return { handleAddSection }
}

export default useAddSection
