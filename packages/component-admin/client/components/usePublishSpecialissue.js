import { useMutation } from 'react-apollo'
import { mutations, queries } from '../graphql'
import { parseError } from './utils'

const usePublishSpecialIssue = (id, journalId) => {
  const [publishSpecialIssue] = useMutation(mutations.publishSpecialIssue, {
    refetchQueries: [
      {
        query: queries.getJournal,
        variables: {
          journalId,
        },
      },
    ],
  })

  const handlePublishSpecialIssue = ({ setFetching, setError, hideModal }) => {
    setFetching(true)
    publishSpecialIssue({
      variables: {
        id,
      },
    })
      .then(() => {
        setFetching(false)
        hideModal()
      })
      .catch(error => {
        setError(false)
        setError(parseError(error))
      })
  }

  return { handlePublishSpecialIssue }
}

export default usePublishSpecialIssue
