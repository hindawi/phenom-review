import { useMutation } from 'react-apollo'
import { mutations, queries } from '../graphql'
import { parseError } from './utils'

const useAddSpecialIssue = journalId => {
  const [addSpecialIssue] = useMutation(mutations.addSpecialIssue, {
    refetchQueries: [
      {
        query: queries.getJournal,
        variables: {
          journalId,
        },
      },
    ],
  })

  const handleAddSpecialIssue = (values, feedbackProps) => {
    const {
      name,
      startDate,
      endDate,
      publicationDate,
      sectionId,
      callForPapers,
      acronym,
      conflictOfInterest,
      description,
      topics,
      isAnnual,
    } = values
    feedbackProps.setFetching(true)
    addSpecialIssue({
      variables: {
        input: {
          name,
          endDate,
          startDate,
          publicationDate,
          journalId,
          sectionId,
          callForPapers,
          acronym,
          conflictOfInterest,
          description,
          topics,
          isAnnual,
        },
      },
    })
      .then(() => {
        feedbackProps.setFetching(false)
        feedbackProps.onSuccess()
      })
      .catch(error => {
        feedbackProps.setFetching(false)
        feedbackProps.setError(parseError(error))
      })
  }

  return { handleAddSpecialIssue }
}

export default useAddSpecialIssue
