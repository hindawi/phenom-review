import React, { Fragment, useState } from 'react'
import moment from 'moment'
import { Checkbox } from '@pubsweet/ui'
import { Label, DatePicker } from '@hindawi/ui'

const JournalActivation = ({
  value,
  onChange,
  setFieldValue,
  journalStatus,
  journalDate,
}) => {
  const [isButtonActive, setButtonActive] = useState(!!journalDate)

  return (
    <Fragment>
      <Label pb={2}>
        <Checkbox
          checked={isButtonActive}
          data-test-id="active-inactive-journal"
          disabled={journalStatus}
          label="Active for submissions"
          onChange={() => {
            setButtonActive(!isButtonActive)
            if (!isButtonActive) {
              setFieldValue(
                'activationDate',
                new Date(moment(Date.now()).format('YYYY-MM-DD')).toISOString(),
              )
            } else {
              setFieldValue('activationDate', undefined)
            }
          }}
        />
      </Label>
      <DatePicker
        disabled={journalStatus || !isButtonActive}
        onChange={onChange}
        value={value}
      />
    </Fragment>
  )
}
export default JournalActivation
