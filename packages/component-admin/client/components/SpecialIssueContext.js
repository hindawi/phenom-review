import React from 'react'

const SpecialIssueContext = React.createContext(
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  ({ journalId, specialIssueId }) => void 0,
)

export default SpecialIssueContext
