import React from 'react'

const JournalTabContext = React.createContext(
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  tabIndex => void 0,
)

export default JournalTabContext
