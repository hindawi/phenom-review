import { useMutation } from 'react-apollo'
import { mutations, queries } from '../graphql'
import { parseError } from './utils'

const useDeleteSpecialIssue = (id, journalId) => {
  const [deleteSpecialIssue] = useMutation(mutations.deleteSpecialIssue, {
    refetchQueries: [
      {
        query: queries.getJournal,
        variables: {
          journalId,
        },
      },
    ],
  })

  const handleDeleteSpecialIssue = ({ setFetching, setError, hideModal }) => {
    setFetching(true)
    deleteSpecialIssue({
      variables: { id },
    })
      .then(() => {
        setFetching(false)
        hideModal()
      })
      .catch(error => {
        setError(false)
        setError(parseError(error))
      })
  }

  return { handleDeleteSpecialIssue }
}

export default useDeleteSpecialIssue
