import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import EditorialBoardForm from './EditorialBoardForm'
import EditorialBoardTable from './EditorialBoardTable'

const EditorialBoard = ({
  sections,
  journalId,
  peerReviewModel,
  journalSpecialIssues,
}) => (
  <Root>
    <EditorialBoardForm
      journalId={journalId}
      journalSpecialIssues={journalSpecialIssues}
      peerReviewModel={peerReviewModel}
      sections={sections}
    />
    <EditorialBoardTable
      journalId={journalId}
      journalSpecialIssues={journalSpecialIssues}
      peerReviewModel={peerReviewModel}
      sections={sections}
    />
  </Root>
)

const Root = styled.div`
  padding: calc(${th('gridUnit')} * 6);
`

export default EditorialBoard
