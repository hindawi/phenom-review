import { useMutation } from 'react-apollo'
import { useParams } from 'react-router-dom'
import { mutations, queries } from '../graphql'
import { parseError } from './utils'

const useEditSpecialIssue = () => {
  const [editSpecialIssue] = useMutation(mutations.editSpecialIssue)
  const { journalId } = useParams()

  const handleEditSpecialIssue = (values, feedbackProps) => {
    const {
      id,
      name,
      startDate,
      endDate,
      publicationDate,
      sectionId,
      callForPapers,
      acronym,
      conflictOfInterest,
      description,
      topics,
      isAnnual,
    } = values

    feedbackProps.setFetching(true)
    editSpecialIssue({
      variables: {
        id,
        input: {
          name,
          endDate,
          startDate,
          publicationDate,
          sectionId,
          callForPapers,
          acronym,
          conflictOfInterest,
          description,
          topics,
          isAnnual,
        },
      },
      refetchQueries: [
        {
          query: queries.getJournal,
          variables: {
            journalId,
          },
        },
      ],
    })
      .then(() => {
        feedbackProps.setFetching(false)
        feedbackProps.onSuccess()
      })
      .catch(error => {
        feedbackProps.setFetching(false)
        feedbackProps.setError(parseError(error))
      })
  }

  return { handleEditSpecialIssue }
}

export default useEditSpecialIssue
