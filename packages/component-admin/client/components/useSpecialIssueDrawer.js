import React, { useState } from 'react'

const useSpecialIssueDrawer = () => {
  const [{ specialIssueId, journalId, isVisible }, setDrawerState] = useState({
    specialIssueId: null,
    journalId: null,
    isVisible: false,
  })

  return {
    isSpecialIssueDrawerVisible: isVisible,
    specialIssueId,
    journalId,
    showDrawer: ({ journalId, specialIssueId }) => {
      setDrawerState({ journalId, specialIssueId, isVisible: true })
    },
    closeDrawer: () => {
      setDrawerState({
        journalId: null,
        specialIssueId: null,
        isVisible: false,
      })
    },
  }
}

export default useSpecialIssueDrawer
