import React, { Fragment } from 'react'
import moment from 'moment'
import { H3 } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Modal } from 'component-modal'
import {
  ShadowedBox,
  Text,
  Row,
  Label,
  Item,
  Icon,
  TextTooltip,
} from '@hindawi/ui'

import { get } from 'lodash'

import { formatAPC } from '../components/utils'
import { AdminJournalForm } from './index'

const JournalCard = ({ editJournal, journal, ghost, history }) => {
  const id = get(journal, 'id', '')
  const code = get(journal, 'code', '')
  const name = get(journal, 'name', '')
  const issn = get(journal, 'issn', '')
  const apc = get(journal, 'apc', '')
  const email = get(journal, 'email', '')
  const activationDate = get(journal, 'activationDate')
  const isActive = get(journal, 'isActive')

  return (
    <CustomShadowedBox
      data-test-id={`journal-${id}`}
      ghost={ghost}
      justify="center"
      mb={5}
      onClick={() => history.push(`/admin/journals/${id}`)}
    >
      <Row justify="flex-start" pl={4} pt={4}>
        <CustomH3 ellipsis title={name}>
          {name}
        </CustomH3>
        <DotSeparator />
        <TextTooltip title={name}>
          <Text mr={4}>{code}</Text>
        </TextTooltip>

        <Modal
          component={AdminJournalForm}
          confirmText="SAVE"
          editJournal={editJournal}
          editMode
          journal={journal}
          modalKey={id}
          title="Edit Journal"
        >
          {showModal => (
            <TextTooltip title="Edit">
              <CustomIcon
                data-test-id="edit-journal"
                fontSize="16px"
                icon="edit"
                onClick={event => {
                  event.stopPropagation()
                  showModal()
                }}
              />
            </TextTooltip>
          )}
        </Modal>
      </Row>

      <Row justify="flex-start" mb={4} px={4}>
        {isActive ? (
          <Fragment>
            <Text customId fontWeight={700}>
              ACTIVE
            </Text>
            {activationDate && (
              <Text pl={1}>{`since ${moment(activationDate).format(
                'YYYY-MM-DD',
              )}`}</Text>
            )}
          </Fragment>
        ) : (
          <Text error fontWeight={700}>
            INACTIVE
          </Text>
        )}
        {activationDate && !isActive && (
          <Text pl={1}>
            {`until ${moment(activationDate).format('YYYY-MM-DD')}`}
          </Text>
        )}
      </Row>

      <BottomRow justify="space-between" p={4}>
        <WithEllipsis vertical>
          <Label mb={2}>Email</Label>
          <Text ellipsis mr={2} title={email}>
            {email}
          </Text>
        </WithEllipsis>

        <Item alignItems="center" justify="flex-end">
          {issn && (
            <ISSN alignItems="flex-end" flex={0} mr={4} pr={4} vertical>
              <Label mb={2}>ISSN</Label>
              <Text whiteSpace="nowrap">{issn}</Text>
            </ISSN>
          )}
          <Item alignItems="flex-end" flex={0} vertical>
            <Label mb={2}>APC</Label>
            <Text whiteSpace="nowrap">{formatAPC(apc)}</Text>
          </Item>
        </Item>
      </BottomRow>
    </CustomShadowedBox>
  )
}

export default JournalCard

// #region styles
const CustomIcon = styled(Icon)`
  opacity: 0;
  display: block;
  padding: calc(${th('gridUnit')} * 1) calc(${th('gridUnit')} * 3)
    calc(${th('gridUnit')} * 3) 0;
  align-items: center;
`

const CustomShadowedBox = styled(ShadowedBox)`
  cursor: pointer;
  flex: 1 1 calc(${th('gridUnit')} * 144);
  border: 1px solid ${th('disabledColor')};
  box-shadow: none;
  border-radius: ${th('gridUnit')};
  padding: 0;
  justify-content: space-between;

  &:hover {
    box-shadow: ${th('shadows.shadowMedium')};

    ${CustomIcon} {
      opacity: 1;
    }
  }
  @media only screen and (min-width: 1328px) {
    flex: 0 1 49%;
  }
`

const BottomRow = styled(Row)`
  background-color: ${th('backgroundColor')};
  border-bottom-left-radius: ${th('gridUnit')};
  border-bottom-right-radius: ${th('gridUnit')};
`

const WithEllipsis = styled(Item)`
  display: inline-grid;
`

const CustomH3 = styled(H3)`
  display: inline-block;
`

const ISSN = styled(Item)`
  border-right: 1px solid ${th('colorFurniture')};
`

const DotSeparator = styled.div`
  align-self: center;
  background-color: ${th('heading.h2Color')};
  border-radius: 50%;
  height: ${th('gridUnit')};
  margin: 0px calc(${th('gridUnit')} * 2);
  width: ${th('gridUnit')};
`
// #endregion
