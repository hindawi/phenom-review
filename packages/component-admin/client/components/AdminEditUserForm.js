import React, { Fragment } from 'react'
import { Query } from 'react-apollo'
import { get } from 'lodash'
import { TextField, Spinner } from '@pubsweet/ui'
import {
  FormModal,
  Item,
  Label,
  Text,
  Menu,
  MenuCountry,
  Row,
  ValidatedFormField,
  ValidatedCheckboxField,
  validators,
  titleOptions,
  renderRor,
  RorInfoState,
} from '@hindawi/ui'
import { getUser } from '../graphql/queries'

// #region helpers

const rorInfo = new RorInfoState()

const submitHandler = ({
  setFieldValue,
  setTouched,
  submitForm,
  initialValues,
}) => () => {
  if (initialValues.aff && rorInfo.getData().aff === '') {
    // means we're in the edit mode and not touching aff field
    rorInfo.setData({
      aff: initialValues.aff,
      affRorId: initialValues.affRorId,
    })
  }

  const { aff, affRorId } = rorInfo.getData()

  // Ror component is quite custom so we need to manually trigger
  // updates for Formik fields
  setFieldValue('aff', aff)
  setFieldValue('affRorId', affRorId)

  // we need to do this manually since Formik doesn't seem to be doing it for some reason
  setTouched({ aff: true })

  // because we manually update aff field with `setFieldValue`, that triggers some update mechanism
  // inside Formik and prevents it from triggering `submitForm`
  // this is why we need to wait a bit for that action to clear up and then be able to run submit form action
  setTimeout(() => {
    submitForm().then(() => {
      rorInfo.resetData()
    })
  }, 10)
}

const validate = values => {
  const errors = {}

  if (get(values, 'email', '') === '') {
    errors.email = 'Required'
  }

  return errors
}
// #endregion

const FormFields = ({ isCurrentUser, initialValues }) => (
  <Fragment>
    <Row alignItems="baseline" mt={6}>
      <Item mr={2} vertical>
        <Label required>First Name</Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="first-name-input"
          inline
          name="givenNames"
          validate={[validators.required]}
        />
      </Item>
      <Item ml={2} vertical>
        <Label required>Last Name</Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="last-name-input"
          inline
          name="surname"
          validate={[validators.required]}
        />
      </Item>
    </Row>

    <Row mt={1}>
      <Item data-test-id="title-dropdown" mr={2} vertical>
        <Label required>Title</Label>
        <ValidatedFormField
          component={Menu}
          data-test-id="title-input"
          name="title"
          options={titleOptions}
          validate={[validators.required]}
        />
      </Item>
      <Item data-test-id="country" ml={2} vertical>
        <Label required>Country</Label>
        <ValidatedFormField
          component={MenuCountry}
          data-test-id="country-dropdown"
          name="country"
          validate={[validators.required]}
        />
      </Item>
    </Row>

    <Row mt={1}>
      <Item vertical>
        <Label required>Affiliation</Label>
        <ValidatedFormField
          component={renderRor({
            initialValues,
            rorInfo,
            dropdownMatchSelectWidth: 200,
          })}
          data-test-id="affiliation-input"
          inline
          name="aff"
          validate={[validators.required]}
        />
      </Item>
    </Row>

    {!isCurrentUser && (
      <Row>
        <Item mr={2}>
          <ValidatedCheckboxField
            data-test-id="is-admin-input"
            name="isAdmin"
          />
          <Text fontWeight={700} mr={4}>
            Assign Admin
          </Text>
          <ValidatedCheckboxField data-test-id="is-ripe-input" name="isRIPE" />
          <Text fontWeight={700}>Assign RIPE</Text>
        </Item>
      </Row>
    )}
  </Fragment>
)

const AdminEditUserForm = ({
  hideModal,
  user,
  onCancel,
  onConfirm,
  currentUser,
}) => {
  const isCurrentUser = currentUser.id === user.id
  const onSubmit = (values, props) => onConfirm(values, props)
  const onClose = props => {
    if (typeof onCancel === 'function') onCancel(props)
    rorInfo.resetData()
    hideModal()
  }

  return (
    <Query
      fetchPolicy="no-cache"
      query={getUser}
      variables={{ userId: user.id }}
    >
      {({ loading, data }) => {
        if (loading) return <Spinner size={8} />

        const user = get(data, 'getUser')
        const initialValues = {
          id: get(user, 'id', ''),
          givenNames: get(user, 'identities[0].name.givenNames', ''),
          surname: get(user, 'identities[0].name.surname', ''),
          title: get(user, 'identities[0].name.title'),
          email: get(user, 'identities[0].email', ''),
          country: get(user, 'identities[0].country', ''),
          aff: get(user, 'identities[0].aff', ''),
          isAdmin: get(user, 'isAdmin', false),
          isRIPE: get(user, 'isRIPE', false),
        }

        return (
          <FormModal
            cancelText="CANCEL"
            confirmText="SAVE USER"
            content={FormFields}
            customSubmitHandler={submitHandler}
            hideModal={hideModal}
            initialValues={initialValues}
            isCurrentUser={isCurrentUser}
            onCancel={onClose}
            onSubmit={onSubmit}
            subtitle={initialValues.email}
            title="Edit user"
            validate={validate}
          />
        )
      }}
    </Query>
  )
}

export default AdminEditUserForm
