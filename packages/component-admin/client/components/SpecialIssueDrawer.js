import React, { useState, useRef, useLayoutEffect, Fragment } from 'react'
import {
  Drawer,
  Button,
  Checkbox,
  Col,
  DatePicker,
  Form,
  Input,
  Row,
  Space,
  Spinner,
  Title,
  Typography,
  Select,
  ConfirmationModal,
} from '@hindawi/phenom-ui'
import moment from 'moment'
import { useQuery } from '@apollo/react-hooks'
import { setSpecialIssuesInitialValues } from './utils'
import { getJournal, getSpecialIssue } from '../graphql/queries'
import useEditSpecialIssue from '../components/useEditSpecialIssue'
import useAddSpecialIssue from '../components/useAddSpecialIssue'

const RowP = props => (
  // TODO: vertical gutter only supported from antd 3.24.0 on
  <Row gutter={[16, 16]} {...props}>
    {props.children}
  </Row>
)
const VALIDATE_MESSAGES = {
  // eslint-disable-next-line no-template-curly-in-string
  required: '${label} is required',
}
const FULL_FUNCTIONALITY =
  (process.env.FEATURE_PPBK4962_SPECIAL_ISSUE || '').toString() === 'true'
const DATE_FIELDS = ['startDate', 'endDate', 'publicationDate']

const useRunAfterUpdate = () => {
  const afterPaintRef = useRef(null)
  useLayoutEffect(() => {
    if (afterPaintRef.current) {
      afterPaintRef.current()
      afterPaintRef.current = null
    }
  })
  return fn => (afterPaintRef.current = fn)
}

const toFormCompatibleValue = specialIssue => {
  const result = { ...specialIssue }
  result.description = result.description.join('\n\n')
  result.topics = result.topics.length
    ? result.topics.join('\n').replace(/^/gm, '• ')
    : ''
  DATE_FIELDS.forEach(
    df => (result[df] = result[df] ? moment(result[df]) : null),
  )
  return result
}
const toGraphQLCompatibleValue = (formValues, specialIssueId) => {
  const result = { ...formValues }
  result.callForPapers = result.callForPapers || ''
  if (specialIssueId) {
    result.id = specialIssueId
  }
  result.description = (result.description || '')
    .split(/\n+/)
    .map(s => s.trim())
    .filter(s => !!s)
  result.topics = (result.topics || '')
    .replace(/•\s*/g, '')
    .split(/\n+/)
    .map(s => s.trim())
    .filter(s => !!s)
  DATE_FIELDS.forEach(
    df => (result[df] = (result[df] && result[df].toISOString()) || null),
  )
  return result
}
const valueAfterStartDateValidator = message => ({ getFieldValue }) => ({
  validator(_, value) {
    if (!value || moment(value).isAfter(getFieldValue('startDate'), 'day')) {
      return Promise.resolve()
    }
    return Promise.reject(new Error(message))
  },
})
const END_DATE_VALIDATOR = valueAfterStartDateValidator(
  'End date should be after the start date',
)
const PUBLICATION_DATE_VALIDATOR = valueAfterStartDateValidator(
  'Publication date should be after the start date',
)

const FormFields = ({ form, sections, specialIssueId }) => {
  const processText = (text, cursor) => {
    const addBullets = text => {
      const result = text
        .replace(/\n?•(\r?\n|$)/g, '')
        .replace(/•[^\S\r\n]+/g, '')
        .replace(/^/gm, '• ')
      return result === '• ' ? '' : result
    }
    const textBeforeCursor = text.slice(0, cursor)
    const newTextBeforeCursor = addBullets(textBeforeCursor)
    const newCursor = newTextBeforeCursor.length
    const newText = addBullets(newTextBeforeCursor + text.slice(cursor))
    return [newText, newCursor]
  }
  const runAfterUpdate = useRunAfterUpdate()
  const handleTopicsChange = evt => {
    const input = evt.target
    const [topics, cursorPos] = processText(input.value, input.selectionStart)
    const values = form.getFieldsValue()
    form.setFieldsValue({
      ...values,
      topics,
    })
    runAfterUpdate(() => {
      input.selectionStart = input.selectionEnd = cursorPos
    })
  }
  return (
    <>
      <RowP>
        <Col span={24}>
          <Form.Item
            label="Special Issue name"
            name="name"
            rules={[{ required: true }]}
          >
            <Input placeholder="Enter Special Issue name" />
          </Form.Item>
        </Col>
      </RowP>
      {sections && !!sections.length && (
        <RowP>
          <Col span={24}>
            <Form.Item
              label="Journal section"
              name="sectionId"
              rules={[{ required: true }]}
            >
              <Select
                disabled={!!specialIssueId}
                options={sections}
                placeholder="Select journal section"
              />
            </Form.Item>
          </Col>
        </RowP>
      )}
      {FULL_FUNCTIONALITY && (
        <RowP>
          <Col span={24}>
            <Form.Item
              label="Acronym"
              name="acronym"
              rules={[{ required: true }]}
            >
              <Input placeholder="Enter Special Issue acronym" />
            </Form.Item>
          </Col>
        </RowP>
      )}
      <RowP>
        <Col span={12}>
          <Form.Item
            label="Submission start date"
            name="startDate"
            rules={[{ required: true }]}
          >
            <DatePicker style={{ width: '100%' }} />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            dependencies={['startDate']}
            label="Submission end date"
            name="endDate"
            rules={[{ required: true }, END_DATE_VALIDATOR]}
          >
            <DatePicker
              onChange={() => {
                if (!FULL_FUNCTIONALITY) {
                  return
                }
                const formValues = form.getFieldsValue()
                const { startDate, endDate, publicationDate } = formValues
                // set publicationDate 5 weeks from end date, if empty
                if (
                  endDate &&
                  startDate &&
                  endDate > startDate && // end date validation didn't run yet
                  !publicationDate
                ) {
                  // TODO: use setFieldValue once antd is 4.22+
                  form.setFieldsValue({
                    ...formValues,
                    publicationDate: endDate.clone().add(5, 'weeks'),
                  })
                }
              }}
              style={{ width: '100%' }}
            />
          </Form.Item>
        </Col>
      </RowP>
      {FULL_FUNCTIONALITY ? (
        <Fragment>
          <RowP>
            <Col span={12}>
              <Form.Item
                label="Publication date"
                name="publicationDate"
                rules={[{ required: true }, PUBLICATION_DATE_VALIDATOR]}
              >
                <DatePicker style={{ width: '100%' }} />
              </Form.Item>
            </Col>
            <Col span={12} style={{ marginTop: '26px' }}>
              <Form.Item name="isAnnual" valuePropName="checked">
                <Checkbox>
                  <Typography.Text strong>Is annual</Typography.Text>
                </Checkbox>
              </Form.Item>
            </Col>
          </RowP>
          <RowP>
            <Col span={24}>
              <Form.Item
                label="Conflict of interest"
                name="conflictOfInterest"
                rules={[{ required: true }]}
              >
                <Input.TextArea placeholder="Enter conflict of interest text" />
              </Form.Item>
            </Col>
          </RowP>
          <RowP>
            <Col span={24}>
              <Form.Item
                label="Special Issue description"
                name="description"
                rules={[{ required: true }]}
              >
                <Input.TextArea
                  placeholder="Enter Special Issue description"
                  style={{ minHeight: '200px' }}
                />
              </Form.Item>
            </Col>
          </RowP>
          <RowP>
            <Col span={24}>
              <Form.Item
                label="List of topics"
                name="topics"
                rules={[{ required: true }]}
              >
                <Input.TextArea
                  onChange={handleTopicsChange}
                  placeholder="• Enter topic name"
                  style={{ minHeight: '200px' }}
                />
              </Form.Item>
            </Col>
          </RowP>
        </Fragment>
      ) : (
        <RowP>
          <Col span={24}>
            <Form.Item
              label="Call for papers"
              name="callForPapers"
              rules={[{ required: true }]}
            >
              <Input.TextArea
                placeholder="Enter call for papers"
                style={{ minHeight: '200px' }}
              />
            </Form.Item>
          </Col>
        </RowP>
      )}
    </>
  )
}

const SpecialIssueDrawer = ({
  isVisible,
  specialIssueId,
  journalId,
  onClose,
  // eslint-disable-next-line sonarjs/cognitive-complexity
}) => {
  const [form] = Form.useForm()
  const { handleEditSpecialIssue } = useEditSpecialIssue()
  const { handleAddSpecialIssue } = useAddSpecialIssue(journalId)
  const [
    { isSaving, isModalOpen, hasChanges, submitErrorMsg },
    setState,
  ] = useState({
    isSaving: false,
    isModalOpen: false,
    hasChanges: false,
    submitErrorMsg: '',
  })

  const { data: journalData, loading: journalLoading } = useQuery(getJournal, {
    variables: {
      journalId,
    },
    skip: !journalId || !isVisible,
    fetchPolicy: 'network-only',
  })
  const { data: specialIssueData, loading: specialIssueLoading } = useQuery(
    getSpecialIssue,
    {
      variables: {
        specialIssueId,
      },
      skip: !specialIssueId || !isVisible,
      fetchPolicy: 'network-only',
    },
  )
  const { sections: sectionsList = [] } = journalData?.getJournal || {}
  const specialIssue = specialIssueData?.getSpecialIssue || {}
  const initialValuesRaw = setSpecialIssuesInitialValues(specialIssue)
  const initialValues = toFormCompatibleValue(initialValuesRaw)
  const onValuesChange = () => {
    setState(prevState => ({ ...prevState, hasChanges: true }))
  }
  const resetStateAndClose = () => {
    setState(prevState => ({
      ...prevState,
      hasChanges: false,
      isSaving: false,
      submitErrorMsg: '',
    }))
    onClose()
  }
  const sections = sectionsList.map(section => ({
    label: section.name,
    value: section.id,
  }))

  const onSubmit = formValues => {
    const specialIssue = toGraphQLCompatibleValue(formValues, specialIssueId)
    const feedbackProps = {
      setFetching: isSaving =>
        setState(prevState => ({ ...prevState, isSaving })),
      onSuccess: resetStateAndClose,
      setError: submitErrorMsg =>
        setState(prevState => ({ ...prevState, submitErrorMsg })),
    }
    return specialIssueId
      ? handleEditSpecialIssue(specialIssue, feedbackProps)
      : handleAddSpecialIssue(specialIssue, feedbackProps)
  }

  const isLoading = journalLoading || specialIssueLoading
  const showCloseModal = () => {
    if (hasChanges) {
      setState(prevState => ({ ...prevState, isModalOpen: true }))
    } else {
      resetStateAndClose()
    }
  }
  // workaround for form not refreshing
  React.useEffect(() => {
    if (isVisible && !isLoading) {
      form.resetFields()
    }
  }, [isVisible, isLoading])

  return (
    <>
      <ConfirmationModal
        cancelText="DISCARD"
        icon="alert"
        okText="CONTINUE EDITING"
        onCancel={() => {
          setState(prevState => ({
            ...prevState,
            isModalOpen: false,
          }))
          resetStateAndClose()
        }}
        onOk={() => {
          setState(prevState => ({
            ...prevState,
            isModalOpen: false,
          }))
        }}
        open={isModalOpen}
        subtitle="Are you sure you want to discard all changes?"
        title="Unsaved changes"
        visible={isModalOpen}
        width={600}
      />
      <Drawer
        bodyStyle={{ padding: '16px' }}
        closable={!isSaving}
        destroyOnClose
        headerStyle={{ padding: '16px' }}
        onClose={event => {
          event.stopPropagation()
          showCloseModal()
        }}
        placement="right"
        title={
          <Title level={4} style={{ marginTop: '24px', marginBottom: 0 }}>
            {specialIssueId ? 'Edit Special Issue' : 'Add Special Issue'}
          </Title>
        }
        visible={isVisible}
        width="500px"
        zIndex={500}
      >
        {isLoading ? (
          <div
            style={{ textAlign: 'center', lineHeight: 'calc(100vh - 120px)' }}
          >
            <Spinner size="large" />
          </div>
        ) : (
          <Form
            form={form}
            initialValues={initialValues}
            layout="vertical"
            onFinish={onSubmit}
            onValuesChange={onValuesChange}
            validateMessages={VALIDATE_MESSAGES}
          >
            <FormFields
              form={form}
              sections={sections}
              specialIssueId={specialIssueId}
            />
            {!!submitErrorMsg && (
              <RowP>
                <Col span={24}>
                  <Form.Item>
                    <Typography.Text type="danger">
                      {submitErrorMsg}
                    </Typography.Text>
                  </Form.Item>
                </Col>
              </RowP>
            )}
            <RowP>
              <Col span={24}>
                <Form.Item>
                  <Space
                    direction="horizontal"
                    style={{ width: '100%', justifyContent: 'right' }}
                  >
                    <Button
                      data-test-id="special-issue-cancel"
                      disabled={isSaving}
                      onClick={event => {
                        event.stopPropagation()
                        showCloseModal()
                      }}
                      size="large"
                      type="ghost"
                      width={48}
                    >
                      CANCEL
                    </Button>

                    <Button
                      data-test-id="special-issue-save"
                      disabled={isSaving}
                      htmlType="submit"
                      loading={isSaving}
                      primary="true"
                      size="large"
                      type="primary"
                      width={48}
                    >
                      SAVE SPECIAL ISSUE
                    </Button>
                  </Space>
                </Form.Item>
              </Col>
            </RowP>
          </Form>
        )}
      </Drawer>
    </>
  )
}

export default SpecialIssueDrawer
