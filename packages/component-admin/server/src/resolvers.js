const { Promise } = require('bluebird')
const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('./use-cases')
const notification = require('./notifications/notification')
const JournalJobsService = require('./jobs/journal')
const SpecialIssueJobsService = require('./jobs/specialIssue/specialIssue')
const {
  useCases: {
    getUpdatedEditorialAssistantsUseCase,
    redistributeEditorialAssistantsUseCase,
    getSubmissionsWithUpdatedEditorialAssistantsUseCase,
  },
} = require('component-model')
const { updateUserRoleInSSO, removeUserRoleInSSO } = require('component-sso')
const {
  isDateToday,
  areDatesEqual,
  isDateInTheFuture,
} = require('component-date-service')
const events = require('component-events')
const { logger } = require('component-logger')

const resolvers = {
  Query: {
    async getUsersForAdminPanel(_, { page, pageSize, searchValue }, ctx) {
      return useCases.getUsersForAdminPanelUseCase
        .initialize(models)
        .execute({ page, pageSize, searchValue })
    },
    async getJournals(_, { input }, ctx) {
      return useCases.getJournalsUseCase.initialize(models).execute()
    },
    async getUsersForEditorialAssignment(_, { input }, ctx) {
      return useCases.getUsersForEditorialAssignmentUseCase
        .initialize(models)
        .execute({ input })
    },
    async getArticleTypes() {
      return useCases.getArticleTypesUseCase.initialize(models).execute()
    },
    async getEditorialBoard(_, { journalId }, ctx) {
      return useCases.getEditorialBoardUseCase
        .initialize(models)
        .execute(journalId)
    },
    async getSpecialIssue(_, { specialIssueId }, ctx) {
      return useCases.getSpecialIssueUseCase
        .initialize(models)
        .execute(specialIssueId)
    },
    async getPeerReviewModels(_, { input }, ctx) {
      return useCases.getPeerReviewModelsUseCase.initialize(models).execute()
    },
  },
  Mutation: {
    async addUserFromAdminPanel(_, { input }, ctx) {
      const eventsService = events.initialize({ models })
      const SSOService = process.env.KEYCLOAK_SERVER_URL
        ? require('component-sso')
        : null

      return useCases.addUserFromAdminPanelUseCase
        .initialize({
          notification,
          models,
          useCases,
          eventsService,
          SSOService,
        })
        .execute(input)
    },
    async activateUser(_, { id }, ctx) {
      const eventsService = events.initialize({ models })

      return useCases.activateUserUseCase
        .initialize({ models, eventsService })
        .execute(id)
    },
    async deactivateUser(_, { id }, ctx) {
      const eventsService = events.initialize({ models })

      return useCases.deactivateUserUseCase
        .initialize({ models, eventsService })
        .execute({ id, userId: ctx.user })
    },
    async editUserFromAdminPanel(_, { id, input }, ctx) {
      const eventsService = events.initialize({ models })

      return useCases.editUserFromAdminPanelUseCase
        .initialize({
          models,
          useCases,
          eventsService,
          logger,
          updateUserRoleInSSO,
          removeUserRoleInSSO,
        })
        .execute({ input, id, userId: ctx.user })
    },
    async addJournal(_, { input }, ctx) {
      const { Job, Journal } = models
      const eventsService = events.initialize({ models })

      const jobsService = JournalJobsService.initialize({
        Job,
        Journal,
        eventsService,
      })

      return useCases.addJournalUseCase
        .initialize({ models, jobsService, eventsService, isDateToday })
        .execute({ input, userId: ctx.user })
    },
    async editJournal(_, { id, input }, ctx) {
      const { Job, Journal } = models
      const eventsService = events.initialize({ models })

      const jobsService = JournalJobsService.initialize({
        Job,
        Journal,
        eventsService,
      })

      return useCases.editJournalUseCase
        .initialize({ models, jobsService, eventsService, logger, isDateToday })
        .execute({ input, id, reqUserId: ctx.user })
    },
    async assignEditorialRole(_, { input }, ctx) {
      const eventsService = events.initialize({ models })
      const redistributeEditorialAssistants = await redistributeEditorialAssistantsUseCase.initialize(
        {
          models,
          logger,
          Promise,
          useCases: {
            getUpdatedEditorialAssistantsUseCase,
            getSubmissionsWithUpdatedEditorialAssistantsUseCase,
          },
          eventsService,
        },
      )
      return useCases.assignEditorialRoleUseCase
        .initialize({
          models,
          useCases,
          eventsService,
          updateUserRoleInSSO,
          redistributeEditorialAssistants,
          logger,
        })
        .execute({ input })
    },

    async removeEditorialMember(_, { teamMemberId }, ctx) {
      const eventsService = events.initialize({ models })

      return useCases.removeEditorialMemberUseCase
        .initialize({
          models,
          useCases,
          logger,
          eventsService,
          removeUserRoleInSSO,
          getUpdatedEditorialAssistantsUseCase,
          getSubmissionsWithUpdatedEditorialAssistantsUseCase,
        })
        .execute(teamMemberId)
        .catch(error => {
          logger.error(
            `Removing Editorial Member: ${teamMemberId} failed`,
            error,
          )
          throw error
        })
    },
    async addSection(_, { name, journalId }, ctx) {
      const eventsService = events.initialize({ models })

      return useCases.addSectionUseCase
        .initialize({ models, eventsService })
        .execute({ name, journalId })
    },
    async editSection(_, { id, input }, ctx) {
      const eventsService = events.initialize({ models })

      return useCases.editSectionUseCase
        .initialize({ models, eventsService })
        .execute({ id, input })
    },
    async addSpecialIssue(_, { input }, ctx) {
      const eventsService = events.initialize({ models })

      const jobsService = SpecialIssueJobsService.initialize({
        models,
        eventsService,
      })

      return useCases.addSpecialIssueUseCase
        .initialize({
          models,
          isDateToday,
          jobsService,
          eventsService,
          userId: ctx.user,
        })
        .execute(input)
    },
    async cancelSpecialIssue(_, { id, input }, ctx) {
      const eventsService = events.initialize({ models })
      return useCases.cancelSpecialIssueUseCase
        .initialize({ models, eventsService })
        .execute({ id, input })
    },
    async publishSpecialIssue(_, { id }, ctx) {
      const eventsService = events.initialize({ models })
      return useCases.publishSpecialIssueUseCase
        .initialize({ models, eventsService })
        .execute({ id })
    },
    async deleteSpecialIssue(_, { id }, ctx) {
      const eventsService = events.initialize({ models })
      return useCases.deleteSpecialIssueUseCase
        .initialize({ models, eventsService })
        .execute({ id })
    },
    async editSpecialIssue(_, { id, input }, ctx) {
      const eventsService = events.initialize({ models })

      const jobsService = SpecialIssueJobsService.initialize({
        models,
        eventsService,
      })

      return useCases.editSpecialIssueUseCase
        .initialize({
          models,
          jobsService,
          eventsService,
          dateService: { isDateToday, areDatesEqual, isDateInTheFuture },
        })
        .execute({ input, id })
    },
    async assignLeadEditorialAssistant(_, { id }, ctx) {
      return useCases.assignLeadEditorialAssistantUseCase
        .initialize({ models })
        .execute({ id })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
