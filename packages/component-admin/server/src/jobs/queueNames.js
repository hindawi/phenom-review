module.exports = {
  JOURNAL_ACTIVATION_QUEUE: 'journal-activation',
  SPECIAL_ISSUE_ACTIVATION_QUEUE: 'special-issue-activation',
  SPECIAL_ISSUE_DEACTIVATION_QUEUE: 'special-issue-deactivation',
}
