const moment = require('moment')
const config = require('config')

const queueNames = require('../queueNames')
const emailCopyService = require('./emailCopyService')

const accountsEmail = config.get('accountsEmail')

module.exports.initialize = ({ models: { Job } }) => ({
  async scheduleSpecialIssueActivation({ specialIssue, teamMemberId }) {
    const params = {
      specialIssueId: specialIssue.id,
    }

    const executionDate = moment(specialIssue.startDate)
      .add(3, 'hours')
      .toISOString()

    return Job.schedule({
      params,
      teamMemberId,
      executionDate,
      jobType: Job.Type.activation,
      specialIssueId: specialIssue.id,
      queueName: `${queueNames.SPECIAL_ISSUE_ACTIVATION_QUEUE}`,
    })
  },
  async scheduleSpecialIssueDeactivation({ specialIssue, teamMemberId }) {
    const { paragraph, ...bodyProps } = emailCopyService.getEmailCopy({
      specialIssueName: specialIssue.name,
      emailType: `special-issue-deactivated`,
    })

    const emailProps = {
      type: 'user',
      templateType: 'notification',
      fromEmail: `Hindawi Support <${accountsEmail}>`,
      content: {
        paragraph,
        subject: `Special Issue ${specialIssue.customId} has been deactivated`,
        signatureName: 'Hindawi Review System',
      },
      bodyProps,
    }

    return Job.schedule({
      jobType: Job.Type.deactivation,
      params: { emailProps, specialIssueId: specialIssue.id },
      teamMemberId,
      executionDate: moment(specialIssue.endDate)
        .add(3, 'hours') // timezone difference
        .add(23, 'hours') // business requirement for SI closing at midnight
        .add(59, 'minutes')
        .toISOString(),
      specialIssueId: specialIssue.id,
      queueName: `${queueNames.SPECIAL_ISSUE_DEACTIVATION_QUEUE}`,
    })
  },
})
