const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const { isDateToday, isDateInThePast } = require('component-date-service')
const events = require('component-events')

const { updateJournalAPCUseCase } = require('./use-cases')

module.exports = {
  async JournalAPCUpdated(data) {
    const eventsService = events.initialize({ models })
    await updateJournalAPCUseCase
      .initialize({
        models,
        logger,
        isDateInThePast,
        isDateToday,
        eventsService,
      })
      .execute(data)
  },
}
