const uuid = require('uuid')
const { rorService } = require('component-ror')

const initialize = ({
  useCases,
  SSOService,
  eventsService,
  notification: notificationService,
  models: { User, Identity, Team },
}) => ({
  execute: async ({
    isAdmin,
    isRIPE,
    givenNames,
    email,
    surname,
    aff,
    affRorId,
    title,
    country,
  }) => {
    if (isAdmin && isRIPE) {
      throw new Error(
        'User cannot be both admin and research integrity publishing editor.',
      )
    }

    const newUserDetails = {
      agreeTc: true,
      isActive: true,
      isSubscribedToEmails: true,
      unsubscribeToken: uuid.v4(),
      defaultIdentityType: 'local',
      confirmationToken: uuid.v4(),
    }

    const user = new User(newUserDetails)

    const identityProps = {
      type: 'local',
      isConfirmed: false,
      passwordHash: null,
      givenNames,
      surname,
      email,
      aff,
      affRorId,
      title,
      country,
    }

    const identity = new Identity(identityProps)

    await appendRORInfoIfMissing(identity)

    await user.insertWithIdentity(identity)

    let useCase
    if (isAdmin) useCase = 'addAdminFromAdminPanelUseCase'
    if (isRIPE) useCase = 'addRIPEFromAdminPanelUseCase'

    if (useCase) {
      await useCases[useCase]
        .initialize({ models: { Team }, eventsService })
        .execute({ user, alias: identityProps })
    }

    if (SSOService) {
      await SSOService.createSSOUser({
        identity,
        user,
      })
    } else {
      await notificationService.notifyUserAddedByAdmin({
        user,
        identity,
        isAdmin,
        isRIPE,
      })
    }

    if (isAdmin) {
      await SSOService.updateUserRoleInSSO(email, Team.Role.admin)
    }

    await eventsService.publishUserEvent({
      userId: user.id,
      eventName: 'UserAdded',
    })

    return User.findOneWithDefaultIdentity(user.id)
  },
})

const appendRORInfoIfMissing = async identity => {
  if (identity.aff && !identity.affRorId) {
    const bestROR = await rorService.getBestROR(identity.aff, identity.country)
    if (bestROR) {
      identity.affRorId = bestROR.organization.id
    }
  }
}

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
