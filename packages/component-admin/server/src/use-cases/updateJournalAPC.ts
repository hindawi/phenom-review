const initialize = ({
  models: { Journal },
  logger,
  isDateToday,
  eventsService,
  isDateInThePast,
}) => ({
  execute: async data => {
    const { id, apc } = data

    const journal = await Journal.findOneBy({
      queryObject: { id },
    })
    const { activationDate } = journal
    journal.updateProperties({ apc })

    // function isDateInThePast needs to have the date in format 00:00:00+00
    if (
      activationDate &&
      (isDateToday(activationDate) || isDateInThePast(activationDate))
    ) {
      // if the journal becomes active, we need to save the apc, make the journal active and send an event
      journal.updateProperties({ isActive: true })
      await journal.save()

      eventsService.publishJournalEvent({
        journalId: journal.id,
        eventName: 'JournalActivated',
      })
    } else {
      // if the journal will remain inactive, we need to save only the apc
      await journal.save()
      logger.info(`APC for journal with id: ${journal.id} was updated`)
    }
  },
})

export { initialize }
