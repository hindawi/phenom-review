const initialize = ({ models: { Team }, eventsService }) => ({
  execute: async ({ user, alias }) => {
    const journalAdminTeam = await Team.findOrCreate({
      queryObject: {
        role: Team.Role.admin,
      },
      eagerLoadRelations: 'members',
      options: {
        role: Team.Role.admin,
        manuscriptId: null,
        journalId: null,
        sectionId: null,
      },
    })

    journalAdminTeam.addMember({
      user,
      options: { alias },
    })

    await journalAdminTeam.saveGraph({ insertMissing: true, relate: true })

    eventsService.publishUserEvent({
      userId: user.id,
      eventName: 'UserAdded',
    })
  },
})

module.exports = {
  initialize,
}
