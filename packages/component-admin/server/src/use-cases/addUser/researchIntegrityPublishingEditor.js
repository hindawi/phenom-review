const initialize = ({ models: { Team }, eventsService }) => ({
  execute: async ({ user, alias }) => {
    const queryObject = {
      role: Team.Role.researchIntegrityPublishingEditor,
      manuscriptId: null,
      journalId: null,
      sectionId: null,
      specialIssueId: null,
    }
    const ripeTeam = await Team.findOrCreate({
      queryObject,
      eagerLoadRelations: 'members',
      options: queryObject,
    })

    ripeTeam.addMember({ user, options: { alias } })
    await ripeTeam.saveGraph({
      relate: true,
      noUpdate: true,
    })

    eventsService.publishUserEvent({
      userId: user.id,
      eventName: 'UserAdded',
    })
  },
})

module.exports = {
  initialize,
}
