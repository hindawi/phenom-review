const getArticleTypesUseCase = require('./getArticleTypes')
const getPeerReviewModelsUseCase = require('./getPeerReviewModels')

const getJournalsUseCase = require('./getJournals')
const addJournalUseCase = require('./addJournal')
const editJournalUseCase = require('./editJournal')
const updateJournalAPCUseCase = require('./updateJournalAPC')

const addSectionUseCase = require('./addSection')
const editSectionUseCase = require('./editSection')

const addSpecialIssueUseCase = require('./specialIssue/add.js')
const editSpecialIssueUseCase = require('./specialIssue/edit')
const cancelSpecialIssueUseCase = require('./specialIssue/cancel')
const publishSpecialIssueUseCase = require('./specialIssue/publish')
const deleteSpecialIssueUseCase = require('./specialIssue/delete')

const getSpecialIssueUseCase = require('./getSpecialIssue')

const getUsersForAdminPanelUseCase = require('./getUsersForAdminPanel')
const addUserFromAdminPanelUseCase = require('./addUserFromAdminPanel')
const addAdminFromAdminPanelUseCase = require('./addUser/admin')
const addRIPEFromAdminPanelUseCase = require('./addUser/researchIntegrityPublishingEditor')
const editUserFromAdminPanelUseCase = require('./editUserFromAdminPanel')
const activateUserUseCase = require('./activateUser')
const deactivateUserUseCase = require('./deactivateUser')

const getEditorialBoardUseCase = require('./getEditorialBoard')
const getUsersForEditorialAssignmentUseCase = require('./getUsersForEditorialAssignment')

const removeOtherMemberUseCase = require('./removeOtherMember')
const removeEditorialMemberUseCase = require('./removeEditorialMember')
const removeEditorialAssistantUseCase = require('./removeEditorialAssistant')

const assignLeadEditorialAssistantUseCase = require('./assignLeadEditorialAssistant')

const assignEditorialRoleUseCase = require('./assignEditorialRole/assignEditorialRole')

const assignEditorialRoleOnJournalUseCase = require('./assignEditorialRole/journal/journal')
const assignEditorialAssistantOnJournalUseCase = require('./assignEditorialRole/journal/editorialAssistant')
const assignEditorOnJournalUseCase = require('./assignEditorialRole/journal/editors')

const assignEditorialRoleOnSectionUseCase = require('./assignEditorialRole/section/section')

const assignEditorialRoleOnSpecialIssueUseCase = require('./assignEditorialRole/specialIssue/specialIssue')

module.exports = {
  addJournalUseCase,
  addSectionUseCase,
  getJournalsUseCase,
  editJournalUseCase,
  editSectionUseCase,
  activateUserUseCase,
  deactivateUserUseCase,
  getArticleTypesUseCase,
  getSpecialIssueUseCase,
  addSpecialIssueUseCase,
  updateJournalAPCUseCase,
  editSpecialIssueUseCase,
  getEditorialBoardUseCase,
  removeOtherMemberUseCase,
  cancelSpecialIssueUseCase,
  publishSpecialIssueUseCase,
  deleteSpecialIssueUseCase,
  assignEditorialRoleUseCase,
  getPeerReviewModelsUseCase,
  removeEditorialMemberUseCase,
  addRIPEFromAdminPanelUseCase,
  getUsersForAdminPanelUseCase,
  addUserFromAdminPanelUseCase,
  assignEditorOnJournalUseCase,
  addAdminFromAdminPanelUseCase,
  editUserFromAdminPanelUseCase,
  removeEditorialAssistantUseCase,
  assignEditorialRoleOnJournalUseCase,
  assignEditorialRoleOnSectionUseCase,
  assignLeadEditorialAssistantUseCase,
  getUsersForEditorialAssignmentUseCase,
  assignEditorialAssistantOnJournalUseCase,
  assignEditorialRoleOnSpecialIssueUseCase,
}
