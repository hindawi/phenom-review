const initialize = ({ PeerReviewModel }) => ({
  execute: async () => PeerReviewModel.all(),
})
const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
