/* eslint-disable sonarjs/cognitive-complexity */
const { difference } = require('lodash')

const resolveActivationDate = (
  activationDate,
  journal,
  hasActivationDateBeenRemoved,
) => {
  if (journal.isActive) return {}
  if (hasActivationDateBeenRemoved) return { activationDate: null }
  return { activationDate }
}

const initialize = ({
  models: { User, Journal, Team, JournalArticleType, Job },
  jobsService,
  isDateToday,
  eventsService,
  logger,
}) => ({
  execute: async ({ input, id, reqUserId }) => {
    const {
      name,
      activationDate,
      articleTypes,
      code,
      email,
      issn,
      peerReviewModelId,
    } = input

    // user details
    const user = await User.find(reqUserId)
    const teamMembership = await user.findTeamMemberByRole(Team.Role.admin)
    const teamMemberId = teamMembership.id

    // get the current journal with its jobs and journalArticleTypes
    const journal = await Journal.findOneBy({
      queryObject: { id },
      eagerLoadRelations: ['jobs', 'journalArticleTypes'],
    })

    // compute the updated list of journalArticleTypes
    const journalArticleTypes = articleTypes.reduce(
      (acc, currentArticleTypeId) => {
        const existingJAT = journal.journalArticleTypes.find(
          ({ articleTypeId }) => articleTypeId === currentArticleTypeId,
        )

        if (existingJAT) {
          acc.push(existingJAT)
        } else {
          const newJat = new JournalArticleType({
            articleTypeId: currentArticleTypeId,
            journalId: journal.id,
          })
          acc.push(newJat)
        }
        return acc
      },
      [],
    )
    // find unchecked journalArticleTypes and delete them
    const uncheckedJournalArticleTypes = difference(
      journal.journalArticleTypes,
      journalArticleTypes,
    )
    if (uncheckedJournalArticleTypes.length) {
      uncheckedJournalArticleTypes.forEach(uncheckedJAT =>
        uncheckedJAT.delete(),
      )
    }

    // define the status of the journal related to the activation date
    const hasActivationDateBeenRemoved =
      !!journal.activationDate && !activationDate
    const hasActivationDateBeenChanged =
      !!journal.activationDate &&
      !!activationDate &&
      activationDate !== new Date(journal.activationDate).toISOString()
    const hasActivationDateJustBeenSet =
      !journal.activationDate && !!activationDate
    const isActivationDateToday = isDateToday(activationDate)

    // create the updated journal ( current journal + changes )
    // don't activate the journal if it doesn't have the APC set
    const updatedJournal = journal
    updatedJournal.updateProperties({
      name,
      code,
      email,
      issn,
      peerReviewModelId,
      journalArticleTypes,
      ...resolveActivationDate(
        activationDate,
        journal,
        hasActivationDateBeenRemoved,
      ),
      ...(isActivationDateToday && journal.apc ? { isActive: true } : {}),
    })

    // deal with the jobs of the schedule
    let newJobs = journal.jobs

    if (hasActivationDateBeenChanged || hasActivationDateBeenRemoved) {
      await journal.cancelJobs(Job)
      newJobs = []
    }

    if (
      (hasActivationDateBeenChanged || hasActivationDateJustBeenSet) &&
      !isActivationDateToday
    ) {
      const job = await updatedJournal.scheduleJob(teamMemberId, jobsService)
      newJobs.push(job)
    }

    updatedJournal.jobs = newJobs

    // try to save the journal in the databse
    // rollback jobs in case of failure
    try {
      await updatedJournal.saveGraph({
        noDelete: false,
      })
    } catch (e) {
      logger.error('Something went wrong while updating the journal.', e)
      if (
        (hasActivationDateBeenChanged || hasActivationDateJustBeenSet) &&
        !isActivationDateToday
      ) {
        await updatedJournal.cancelJobs(Job)
      }
      if (hasActivationDateBeenChanged || hasActivationDateBeenRemoved) {
        await journal.scheduleJob(teamMemberId, jobsService)
      }
      throw new Error('Something went wrong while updating the journal.')
    }

    // publish an event for the updated journal
    eventsService.publishJournalEvent({
      journalId: id,
      eventName: 'JournalUpdated',
    })
  },
})

const authsomePolicies = ['admin']
module.exports = {
  initialize,
  authsomePolicies,
}
