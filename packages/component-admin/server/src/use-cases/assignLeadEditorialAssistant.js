const initialize = ({ models: { Team, TeamMember } }) => ({
  execute: async ({ id: teamMemberId }) => {
    const teamMember = await TeamMember.find(teamMemberId)
    const team = await Team.find(teamMember.teamId)
    return team.changeCorrespondingTeamMember(teamMember)
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
