const initialize = ({
  models: { Team, TeamMember, Section },
  logger,
  eventsService,
  updateUserRoleInSSO,
}) => ({
  execute: async ({ user, role, sectionId }) => {
    const exitingUserSectionMember = await TeamMember.findOneBySectionAndUser({
      userId: user.id,
      sectionId,
    })
    if (exitingUserSectionMember) {
      throw new ValidationError(
        `User already has an editorial role on this section.`,
      )
    }

    const localIdentity = user.identities.find(
      identity => identity.type === 'local',
    )

    const userEmail = localIdentity.email

    try {
      await updateUserRoleInSSO(userEmail, role)
    } catch (err) {
      logger.error('ERROR: There was a problem with SSO role update:', err)
    }

    const queryObject = {
      role,
      sectionId,
    }
    const team = await Team.findOrCreate({
      queryObject,
      options: queryObject,
      eagerLoadRelations: 'members',
    })

    const teamMember = team.addMember({
      user,
    })
    await teamMember.save()

    const section = await Section.find(sectionId)

    eventsService.publishJournalEvent({
      journalId: section.journalId,
      eventName: 'JournalSectionEditorAssigned',
    })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
