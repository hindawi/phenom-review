const initialize = ({
  models,
  logger,
  useCases,
  transaction,
  eventsService,
  updateUserRoleInSSO,
  redistributeEditorialAssistants,
}) => ({
  execute: async ({ input }) => {
    const { userId, role, journalId, sectionId, specialIssueId } = input
    const { User } = models

    let useCase = 'assignEditorialRoleOnJournalUseCase'
    if (specialIssueId) {
      useCase = 'assignEditorialRoleOnSpecialIssueUseCase'
    }
    if (sectionId) {
      useCase = 'assignEditorialRoleOnSectionUseCase'
    }

    const user = await User.find(userId, 'identities')

    await useCases[useCase]
      .initialize({
        models,
        logger,
        useCases,
        transaction,
        eventsService,
        updateUserRoleInSSO,
        redistributeEditorialAssistants,
      })
      .execute({ user, role, journalId, sectionId, specialIssueId })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
