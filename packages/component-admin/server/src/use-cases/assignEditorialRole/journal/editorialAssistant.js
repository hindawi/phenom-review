const initialize = ({
  eventsService,
  models: { Team, TeamMember },
  updateUserRoleInSSO,
  redistributeEditorialAssistants,
}) => ({
  execute: async ({ user, journalId, role }) => {
    let journalTeam = await Team.findOneByJournalAndRole({ journalId, role })
    if (!journalTeam) journalTeam = new Team({ journalId, role })

    // add ea sso role
    const localIdentity = user.identities.find(
      identity => identity.type === 'local',
    )

    const userEmail = localIdentity.email

    try {
      await updateUserRoleInSSO(userEmail, role)
    } catch (err) {
      logger.error('ERROR: There was a problem with SSO role update:', err)
    }

    journalTeam.addMember({
      user,
      options: { status: TeamMember.Statuses.pending },
    })

    await journalTeam.saveGraph({
      noDelete: true,
      noUpdate: true,
      relate: true,
    })

    await redistributeEditorialAssistants.execute(journalId)

    await eventsService.publishJournalEvent({
      journalId,
      eventName: 'JournalEditorialAssistantAssigned',
    })
  },
})

module.exports = {
  initialize,
}
