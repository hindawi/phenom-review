const initialize = ({
  models: { Team, TeamMember, SpecialIssue, Section },
  logger,
  eventsService,
  updateUserRoleInSSO,
}) => ({
  execute: async ({ user, role, specialIssueId }) => {
    const existingUserSpecialIssueMember = await TeamMember.findOneBySpecialIssueAndUser(
      { userId: user.id, specialIssueId },
    )
    if (existingUserSpecialIssueMember) {
      throw new ValidationError(
        `User already has an editorial role on this special issue.`,
      )
    }

    const localIdentity = user.identities.find(
      identity => identity.type === 'local',
    )

    const userEmail = localIdentity.email

    try {
      await updateUserRoleInSSO(userEmail, role)
    } catch (err) {
      logger.error('ERROR: There was a problem with SSO role update:', err)
    }

    const queryObject = {
      specialIssueId,
      role,
    }
    const team = await Team.findOrCreate({
      queryObject,
      options: queryObject,
      eagerLoadRelations: 'members',
    })

    const teamMember = team.addMember({
      user,
    })
    await teamMember.save()

    const eventObject = { journalId: '', eventName: '' }
    const specialIssue = await SpecialIssue.find(specialIssueId)

    if (specialIssue.sectionId) {
      const section = await Section.find(specialIssue.sectionId)
      eventObject.journalId = section.journalId
      eventObject.eventName = 'JournalSectionSpecialIssueEditorAssigned'
    } else {
      eventObject.journalId = specialIssue.journalId
      eventObject.eventName = 'JournalSpecialIssueEditorAssigned'
    }

    eventsService.publishJournalEvent(eventObject)
    eventsService.publishSpecialIssueEvent({
      specialIssueId,
      eventName: 'SpecialIssueUpdated',
    })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
