const initialize = ({ SpecialIssue }) => ({
  execute: async specialIssueId => SpecialIssue.find(specialIssueId, 'section'),
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
