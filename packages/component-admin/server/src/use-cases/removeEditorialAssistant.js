const Promise = require('bluebird')
const { logger } = require('component-logger')

const { groupBy } = require('lodash')

const initialize = ({
  eventsService,
  models,
  getUpdatedEditorialAssistantsUseCase,
  getSubmissionsWithUpdatedEditorialAssistantsUseCase,
}) => ({
  execute: async teamMemberId => {
    const { Manuscript, Team, TeamMember } = models
    const teamMember = await TeamMember.find(teamMemberId, 'team')
    if (!teamMember) throw new Error('Cannot remove editor.')

    const editorialAssistantWorkingStatuses = [
      ...Manuscript.InProgressStatuses,
      Manuscript.Statuses.inQA,
      Manuscript.Statuses.qualityChecksRequested,
      Manuscript.Statuses.qualityChecksSubmitted,
      Manuscript.Statuses.technicalChecks,
      Manuscript.Statuses.draft,
    ]

    const journalEditorialAssistants = await TeamMember.findAllByJournalWithWorkload(
      {
        journalId: teamMember.team.journalId,
        role: Team.Role.editorialAssistant,
        teamMemberStatuses: [TeamMember.Statuses.active],
        manuscriptStatuses: editorialAssistantWorkingStatuses,
      },
    )

    if (journalEditorialAssistants.length === 1)
      throw new Error(
        "You can't remove the only Editorial Assistant on the journal.",
      )

    const remainingEditorialAssistants = journalEditorialAssistants.filter(
      tm => tm.userId !== teamMember.userId,
    )

    const manuscriptsToRedistribute = await Manuscript.findAllByJournalAndUserAndRoleAndTeamMemberStatusesAndManuscriptStatuses(
      {
        userId: teamMember.userId,
        journalId: teamMember.team.journalId,
        role: Team.Role.editorialAssistant,
        manuscriptStatuses: editorialAssistantWorkingStatuses,
        teamMemberStatuses: [TeamMember.Statuses.active],
      },
    )

    const submissionsToRedistribute = []
    await Promise.each(manuscriptsToRedistribute, async manuscript => {
      const { submissionId } = manuscript
      const manuscriptsInSubmission = await Manuscript.findAll({
        queryObject: {
          submissionId,
        },
      })

      if (!manuscriptsInSubmission.length) return

      // do not add it to the submission list if it is already there
      if (
        submissionsToRedistribute.find(
          submissionManuscripts =>
            submissionManuscripts[0].submissionId === submissionId,
        )
      )
        return

      submissionsToRedistribute.push(manuscriptsInSubmission)
    })

    const {
      count: numberOfSubmissions,
    } = await Manuscript.countSubmissionsByJournalAndStatuses({
      journalId: teamMember.team.journalId,
      statuses: editorialAssistantWorkingStatuses,
    })

    const averageWorkload = Math.floor(
      numberOfSubmissions / remainingEditorialAssistants.length,
    )

    const {
      editorRemovedSubmissions,
      editorAssignedSubmissions,
    } = await getSubmissionsWithUpdatedEditorialAssistantsUseCase
      .initialize({
        models,
        useCases: { getUpdatedEditorialAssistantsUseCase },
        Promise,
      })
      .execute({
        averageWorkload,
        submissions: submissionsToRedistribute,
        editorialAssistantWorkingStatuses,
        journalEditorialAssistant: teamMember,
        journalEditorialAssistants: remainingEditorialAssistants,
      })

    logger.info(
      `Found ${submissionsToRedistribute.length} submissions to be redistributed`,
    )

    logger.info(
      `Found ${editorRemovedSubmissions.length} submissions where the EA will be removed`,
      JSON.stringify(editorRemovedSubmissions),
    )

    logger.info(
      `Found ${editorAssignedSubmissions.length} submissions where the EA will be assigned`,
      JSON.stringify(editorAssignedSubmissions),
    )

    await teamMember.delete()

    eventsService.publishJournalEvent({
      journalId: teamMember.team.journalId,
      eventName: 'JournalEditorRemoved',
    })

    logger.info(
      `Journal Editor Remove Event Sent for journal ${teamMember.team.journalId}`,
    )

    editorRemovedSubmissions.forEach(submissionId => {
      eventsService.publishSubmissionEvent({
        submissionId,
        eventName: 'SubmissionEditorialAssistantRemoved',
      })
    })

    logger.info(
      `All events for submission editor removed sent for journal ${teamMember.team.journalId}`,
    )

    editorAssignedSubmissions.forEach(submissionId => {
      eventsService.publishSubmissionEvent({
        submissionId,
        eventName: 'SubmissionEditorialAssistantAssigned',
      })
    })

    logger.info(
      `All events for submission editor assigned sent for journal ${teamMember.team.journalId}`,
    )
  },
})

module.exports = { initialize }
