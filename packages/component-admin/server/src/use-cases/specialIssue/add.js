const MAX_SI_CUSTOM_ID_TRIES = 100
const initialize = ({
  userId,
  isDateToday,
  jobsService,
  eventsService,
  models: { SpecialIssue, PeerReviewModel, Team, TeamMember },
}) => ({
  execute: async ({
    name,
    startDate,
    endDate,
    publicationDate,
    journalId,
    sectionId,
    callForPapers,
    isAnnual,
    acronym,
    conflictOfInterest,
    description,
    topics,
  }) => {
    if (endDate <= startDate) {
      throw new Error(`End date must be after start date.`)
    }
    if (publicationDate <= startDate) {
      throw new Error(`Publication date must be after start date.`)
    }

    const existingSpecialIssue = await SpecialIssue.findUnique(name)
    if (existingSpecialIssue) throw new Error(`Special issue already exists.`)

    const specialIssuePeerReviewModel = await PeerReviewModel.findOneBy({
      queryObject: {
        name: 'Special Issue Associate Editor',
      },
    })

    let specialIssue = new SpecialIssue({
      name,
      endDate,
      startDate,
      publicationDate,
      callForPapers,
      isAnnual,
      acronym,
      conflictOfInterest,
      description,
      topics,
      customId: await SpecialIssue.generateUniqueCustomId(
        MAX_SI_CUSTOM_ID_TRIES,
      ),
      peerReviewModelId: specialIssuePeerReviewModel.id,
      isActive: isDateToday(startDate),
      ...(journalId && !sectionId ? { journalId } : {}),
      ...(sectionId ? { sectionId } : {}),
    })

    specialIssue = await specialIssue.save()

    const admin = await TeamMember.findOneByUserAndRole({
      userId,
      role: Team.Role.admin,
    })

    if (!specialIssue.isActive) {
      jobsService.scheduleSpecialIssueActivation({
        specialIssue,
        teamMemberId: admin.id,
      })
    }

    jobsService.scheduleSpecialIssueDeactivation({
      specialIssue,
      teamMemberId: admin.id,
    })

    const eventName = sectionId
      ? 'JournalSectionSpecialIssueAdded'
      : 'JournalSpecialIssueAdded'

    eventsService.publishJournalEvent({
      journalId,
      eventName,
    })

    // SpecialIssueAdded intentionally missing, it is raised by the publish action

    return specialIssue
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
