const initialize = ({ eventsService, models: { SpecialIssue, Section } }) => ({
  execute: async ({ id }) => {
    const specialIssue = await SpecialIssue.find(id)

    if (!specialIssue) {
      throw new Error(`Special issue ${id} not found.`)
    }
    if (specialIssue.isPublished) {
      throw new Error(`Special issue already published: ${id}.`)
    }

    let section
    if (specialIssue.sectionId) {
      section = await Section.find(specialIssue.sectionId)
      if (!section) {
        throw new Error(
          `Cannot find section ${sectionId}, assigned to special issue ${id}`,
        )
      }
    }

    specialIssue.updateProperties({
      isPublished: true,
    })

    await specialIssue.save()

    let eventName = 'JournalSpecialIssueUpdated'
    let { journalId } = specialIssue
    if (section) {
      journalId = section.journalId
      eventName = 'JournalSectionSpecialIssueUpdated'
    }
    eventsService.publishJournalEvent({
      journalId,
      eventName,
    })
    eventsService.publishSpecialIssueEvent({
      specialIssueId: id,
      eventName: 'SpecialIssueAdded',
    })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
