const initialize = ({ models: { SpecialIssue, Job } }) => ({
  execute: async ({ id }) => {
    const specialIssue = await SpecialIssue.find(id, '[manuscripts]')

    if (!specialIssue) {
      throw new Error(`Special issue does not exist: ${id}.`)
    }
    if (!specialIssue.acronym) {
      throw new Error(
        `Special issues created using the legacy UI cannot be deleted: ${id}.`,
      )
    }
    if (specialIssue.manuscripts?.length > 0) {
      throw new Error(
        `Special issue cannot be deleted because it has at least one manuscript: ${id}.`,
      )
    }

    await specialIssue.cancelJobByType({
      Job,
      jobType: Job.Type.deactivation,
    })
    await specialIssue.cancelJobByType({
      Job,
      jobType: Job.Type.activation,
    })

    await specialIssue.delete()
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
