const initialize = ({
  eventsService,
  models: { SpecialIssue, Section, Job },
}) => ({
  execute: async ({ id, input: { reason } }) => {
    const specialIssue = await SpecialIssue.find(id)

    if (!specialIssue.isActive) {
      throw new Error(`Inactive special issues can't be cancelled.`)
    }

    specialIssue.updateProperties({
      isActive: false,
      isCancelled: true,
      cancelReason: reason,
    })

    await specialIssue.save()

    await specialIssue.cancelJobByType({
      Job,
      jobType: Job.Type.deactivation,
    })
    await specialIssue.cancelJobByType({
      Job,
      jobType: Job.Type.activation,
    })

    let eventName = 'JournalSpecialIssueUpdated'
    let { journalId } = specialIssue
    if (specialIssue.sectionId) {
      const section = await Section.find(specialIssue.sectionId)
      journalId = section.journalId
      eventName = 'JournalSectionSpecialIssueUpdated'
    }
    eventsService.publishJournalEvent({
      journalId,
      eventName,
    })
    eventsService.publishSpecialIssueEvent({
      specialIssueId: id,
      eventName: 'SpecialIssueCancelled',
    })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
