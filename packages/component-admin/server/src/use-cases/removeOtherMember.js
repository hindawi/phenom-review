const initialize = ({
  models: { TeamMember, Section, SpecialIssue, Team },
  eventsService,
  removeUserRoleInSSO,
  logger,
}) => ({
  execute: async teamMemberId => {
    const teamMember = await TeamMember.find(teamMemberId, 'team')
    if (!teamMember) throw new Error('Cannot remove editor.')

    await teamMember.delete()

    const otherEditorRoles = await TeamMember.findAllByGlobalRole({
      userId: teamMember.userId,
      role: teamMember.team.role,
    })

    if (!otherEditorRoles.length) {
      try {
        await removeUserRoleInSSO(teamMember.alias.email, teamMember.team.role)
      } catch (err) {
        logger.error('ERROR: There was a problem with SSO role removal:', err)
      }
    }

    let eventObject = {
      journalId: teamMember.team.journalId,
      eventName: 'JournalEditorRemoved',
    }

    if (teamMember.team.sectionId) {
      const section = await Section.find(teamMember.team.sectionId)
      eventObject.journalId = section.journalId
      eventObject.eventName = 'JournalSectionEditorRemoved'
    }

    if (teamMember.team.specialIssueId) {
      const specialIssue = await SpecialIssue.find(
        teamMember.team.specialIssueId,
      )
      eventObject = {
        journalId: specialIssue.journalId,
        eventName: 'JournalSpecialIssueEditorRemoved',
      }
      if (specialIssue.sectionId) {
        const section = await Section.find(specialIssue.sectionId)
        eventObject.journalId = section.journalId
        eventObject.eventName = 'JournalSectionSpecialIssueEditorRemoved'
      }
      await eventsService.publishSpecialIssueEvent({
        specialIssueId: specialIssue.id,
        event: 'SpecialIssueUpdated',
      })
    }

    await eventsService.publishJournalEvent(eventObject)
  },
})

module.exports = { initialize }
