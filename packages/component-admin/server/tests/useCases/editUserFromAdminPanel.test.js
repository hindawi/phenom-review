const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { Team, User, Identity } = models
const useCases = require('../../src/use-cases')
const { editUserFromAdminPanelUseCase } = require('../../src/use-cases')
const logger = require('@pubsweet/logger')

const chance = new Chance()
const eventsService = {
  publishUserEvent: jest.fn(),
}
const updateUserRoleInSSO = jest.fn()
const removeUserRoleInSSO = jest.fn()

describe('Edit users from admin panel', () => {
  let admin
  let user
  let input
  beforeEach(() => {
    admin = dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })
    user = fixtures.generateUser({ User, Identity })
    input = {
      surname: chance.last(),
      givenNames: chance.first(),
    }
  })

  it('edits a user information', async () => {
    await editUserFromAdminPanelUseCase
      .initialize({
        models,
        useCases,
        eventsService,
        logger,
        updateUserRoleInSSO,
        removeUserRoleInSSO,
      })
      .execute({ input, id: user.id })

    expect(user.identities[0].surname).toEqual(input.surname)
  })
  it('returns an error when the user id is invalid', async () => {
    const result = editUserFromAdminPanelUseCase
      .initialize({
        models,
        useCases,
        eventsService,
        logger,
        updateUserRoleInSSO,
        removeUserRoleInSSO,
      })
      .execute({ input, id: chance.guid() })

    return expect(result).rejects.toThrow()
  })
  it('returns an error when the user is trying to be set both admin and ripe', async () => {
    input.isAdmin = true
    input.isRIPE = true

    try {
      await editUserFromAdminPanelUseCase
        .initialize({
          models,
          useCases,
          eventsService,
          logger,
          updateUserRoleInSSO,
          removeUserRoleInSSO,
        })
        .execute({ input, id: user.id })
    } catch (e) {
      expect(e.message).toEqual(
        'A user cannot be both admin and research integrity publishing editor.',
      )
    }
  })
  it('sets a user as admin when isAdmin is true', async () => {
    input.isAdmin = true

    await editUserFromAdminPanelUseCase
      .initialize({
        models,
        useCases,
        eventsService,
        logger,
        updateUserRoleInSSO,
        removeUserRoleInSSO,
      })
      .execute({ input, id: user.id })

    expect(admin.team.role).toEqual(models.Team.Role.admin)
  })
  it('sets a user as ripe when isRIPE is true', async () => {
    input.isRIPE = true

    await editUserFromAdminPanelUseCase
      .initialize({
        models,
        useCases,
        eventsService,
        logger,
        updateUserRoleInSSO,
        removeUserRoleInSSO,
      })
      .execute({ input, id: user.id })

    const ripeTeam = await fixtures.getTeamByRole(
      Team.Role.researchIntegrityPublishingEditor,
    )

    const ripe = ripeTeam.members.find(tm => tm.user.id === user.id)
    ripe.team = ripeTeam

    expect(ripe).toBeDefined()
    expect(ripe.team.role).toEqual(Team.Role.researchIntegrityPublishingEditor)
  })
  it('removes admin role when isAdmin is false', async () => {
    input.isAdmin = false

    const existingAdmin = dataService.createGlobalUser({
      models,
      fixtures,
      role: models.Team.Role.admin,
    })

    await editUserFromAdminPanelUseCase
      .initialize({
        models,
        useCases,
        eventsService,
        logger,
        updateUserRoleInSSO,
        removeUserRoleInSSO,
      })
      .execute({ input, id: existingAdmin.userId })

    const adminTeam = fixtures.getTeamByRole(Team.Role.admin)
    const admin = adminTeam.members.find(tm => tm.user.id === user.id)

    expect(admin).toBeUndefined()
    expect(removeUserRoleInSSO).toHaveBeenCalled()
  })
  it('removes ripe role when isRIPE is false', async () => {
    input.isRIPE = false

    const existingRipe = dataService.createGlobalUser({
      models,
      fixtures,
      role: models.Team.Role.researchIntegrityPublishingEditor,
    })

    await editUserFromAdminPanelUseCase
      .initialize({
        models,
        useCases,
        eventsService,
        logger,
        updateUserRoleInSSO,
        removeUserRoleInSSO,
      })
      .execute({ input, id: existingRipe.userId })

    const ripeTeam = fixtures.getTeamByRole(
      Team.Role.researchIntegrityPublishingEditor,
    )
    const ripe = ripeTeam.members.find(tm => tm.user.id === user.id)

    expect(ripe).toBeUndefined()
  })
  it('returns an error when trying to remove own admin rights', async () => {
    input.isAdmin = false

    try {
      await editUserFromAdminPanelUseCase
        .initialize({
          models,
          useCases,
          eventsService,
          logger,
          updateUserRoleInSSO,
          removeUserRoleInSSO,
        })
        .execute({ input, id: admin.userId, userId: admin.userId })
    } catch (e) {
      expect(e.message).toEqual('Cannot remove own admin role.')
    }
  })
})
