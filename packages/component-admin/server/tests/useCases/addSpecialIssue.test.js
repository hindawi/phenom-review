const {
  getTeamRoles,
  generateTeamMember,
  generateSpecialIssue,
} = require('component-generators')
const Chance = require('chance')
const addSpecialIssueUseCase = require('../../src/use-cases/specialIssue/add')

const chance = new Chance()
let models = {}
let PeerReviewModel = {}
let SpecialIssue = {}
let TeamMember = {}

const isDateToday = jest.fn(() => true)
const jobsService = {
  scheduleSpecialIssueActivation: jest.fn(),
  scheduleSpecialIssueDeactivation: jest.fn(),
}
const eventsService = {
  publishJournalEvent: jest.fn(),
  publishSpecialIssueEvent: jest.fn(),
}

describe('addSpecialIssue', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    models = {
      PeerReviewModel: {
        findOneBy: jest.fn(),
      },
      SpecialIssue: jest.fn(args => {
        const specialIssue = {
          ...args,
          save: jest.fn().mockImplementation(() => {
            if (!specialIssue.id) {
              specialIssue.id = chance.guid()
            }
            return specialIssue
          }),
        }
        return specialIssue
      }),
      Team: { Role: getTeamRoles() },
      TeamMember: {
        findOneByUserAndRole: jest.fn(),
      },
    }
    Object.assign(models.SpecialIssue, {
      find: jest.fn(),
      findUnique: jest.fn(),
      generateUniqueCustomId: jest.fn(),
    })(({ PeerReviewModel, SpecialIssue, TeamMember } = models))
  })
  it('Throws an error if the end date is before start date', async () => {
    try {
      await addSpecialIssueUseCase.initialize({ models }).execute({
        startDate: new Date(),
        endDate: new Date() - 1,
      })
    } catch (err) {
      expect(err.message).toBe('End date must be after start date.')
    }
  })
  it('Throws an error if the special issue already exists', async () => {
    const specialIssue = generateSpecialIssue()
    const input = {
      name: 'existing special issue name',
    }

    jest.spyOn(SpecialIssue, 'find').mockResolvedValue(specialIssue)
    jest.spyOn(SpecialIssue, 'findUnique').mockResolvedValue(specialIssue)

    try {
      await addSpecialIssueUseCase.initialize({ models }).execute(input)
    } catch (err) {
      expect(err.message).toBe('Special issue already exists.')
    }
  })
  it('Executes main flow', async () => {
    const specialIssue = generateSpecialIssue()
    const admin = generateTeamMember()
    const input = { name: specialIssue.name }

    jest.spyOn(PeerReviewModel, 'findOneBy').mockResolvedValue(specialIssue)
    jest.spyOn(TeamMember, 'findOneByUserAndRole').mockResolvedValue(admin)

    await addSpecialIssueUseCase
      .initialize({ models, isDateToday, jobsService, eventsService })
      .execute(input)

    expect(jobsService.scheduleSpecialIssueDeactivation).toHaveBeenCalledTimes(
      1,
    )
    expect(eventsService.publishJournalEvent).toHaveBeenCalledTimes(1)
  })
})
