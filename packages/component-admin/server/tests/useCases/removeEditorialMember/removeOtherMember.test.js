const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

jest.mock('@pubsweet/logger', () => ({
  info: jest.fn(),
  warn: jest.fn(),
  error: jest.fn(),
}))

const { Journal, PeerReviewModel, Team, TeamMember } = models
const chance = new Chance()

const useCases = require('../../../src/use-cases')

const generateInput = () => ({
  isActive: false,
  name: chance.company(),
  endDate: new Date(chance.date({ year: 2099, string: true })),
  startDate: new Date(),
  callForPapers: chance.paragraph(),
})
const eventsService = {
  publishJournalEvent: jest.fn(),
}

const removeUserRoleInSSO = jest.fn()
TeamMember.findAllByGlobalRole = jest.fn().mockResolvedValue([])

describe('Remove Other Editorial Members use case', () => {
  let journal
  beforeEach(async () => {
    generateInput()
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
    journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel
  })
  it('should remove the Triage Editor from journal', async () => {
    const triageEditor = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })

    await useCases.removeEditorialMemberUseCase
      .initialize({
        models,
        eventsService,
        useCases,
        removeUserRoleInSSO,
      })
      .execute(triageEditor.id)

    const journalTriageEditorTeam = fixtures.teams.find(
      team =>
        team.role === Team.Role.triageEditor && team.journal.id === journal.id,
    )
    const triageEditorMember = journalTriageEditorTeam.members.find(
      member => member.id === triageEditor.id,
    )
    expect(triageEditorMember).toBeUndefined()
    expect(eventsService.publishJournalEvent).toHaveBeenCalled()
    expect(removeUserRoleInSSO).toHaveBeenCalled()
  })
})
