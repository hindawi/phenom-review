const { models, fixtures } = require('fixture-service')

const { publishSpecialIssueUseCase } = require('../../src/use-cases')

const eventsService = {
  publishJournalEvent: jest.fn(async () => {}),
  publishSpecialIssueEvent: jest.fn(async () => {}),
}

describe('Publish special issue use case', () => {
  it('updates special issue is published field', async () => {
    const { SpecialIssue } = models
    const specialIssue = await fixtures.generateSpecialIssue({
      properties: {
        isPublished: false,
      },
      SpecialIssue,
    })

    await publishSpecialIssueUseCase
      .initialize({ models, eventsService })
      .execute({ id: specialIssue.id })

    expect(specialIssue.isPublished).toBe(true)
    expect(eventsService.publishJournalEvent).toHaveBeenCalledTimes(1)
    expect(eventsService.publishSpecialIssueEvent).toHaveBeenCalledTimes(1)
  })
})
