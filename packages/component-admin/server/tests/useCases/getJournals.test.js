const { getJournalsUseCase } = require('../../src/use-cases')
const { models, fixtures } = require('fixture-service')

const { Journal } = models
const Chance = require('chance')

const chance = new Chance()

const journal1 = fixtures.generateJournal({
  properties: {
    name: chance.company(),
    code: chance.word().toUpperCase(),
    email: chance.email(),
    apc: chance.natural(),
    issn: `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
      min: 999,
      max: 9999,
    })}`,
    articleTypes: [chance.guid(), chance.guid()],
    created: Date.now(),
    activationDate: Date.now() + 3454,
    isActive: false,
  },
  Journal,
})

const journal2 = fixtures.generateJournal({
  properties: {
    name: chance.company(),
    code: chance.word().toUpperCase(),
    email: chance.email(),
    apc: 900,
    articleTypes: [chance.guid(), chance.guid()],
    created: Date.now() + 1,
    isActive: false,
  },
  Journal,
})
describe('getJournals use-case', () => {
  it('should return an array', async () => {
    const res = await getJournalsUseCase.initialize(models).execute()
    expect(Array.isArray(res)).toBeTruthy()
  })

  it('should contain correct number of journals', async () => {
    const res = await getJournalsUseCase.initialize(models).execute()
    expect(res.length).toEqual(2)
  })

  it('should return journals in the correct order', async () => {
    const res = await getJournalsUseCase.initialize(models).execute()
    expect(res[0].name).toEqual(journal2.name)
    expect(res[1].name).toEqual(journal1.name)
  })
})
