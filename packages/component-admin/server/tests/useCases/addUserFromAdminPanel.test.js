process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { Team, Journal, PeerReviewModel } = models
const useCases = require('../../src/use-cases')
const logger = require('@pubsweet/logger')
const { createSSOUser } = require('component-sso')

const chance = new Chance()

const notificationService = {
  notifyUserAddedByAdmin: jest.fn(),
}
const eventsService = {
  publishUserEvent: jest.fn(),
}

const SSOService = {
  updateUserRoleInSSO: jest.fn(),
  createSSOUser: jest.fn(),
}

const generateInput = () => ({
  givenNames: chance.first(),
  surname: chance.first(),
  email: chance.email(),
  country: chance.country(),
  aff: chance.company(),
  title: chance.pickone(['mr', 'mrs', 'miss', 'ms', 'dr', 'prof']),
  role: 'user',
  isAdmin: false,
  isRIPE: false,
})

describe('Add users from admin panel', () => {
  let input
  const peerReviewModel = fixtures.generatePeerReviewModel({
    properties: {
      approvalEditors: [Team.Role.triageEditor],
    },
    PeerReviewModel,
  })

  const journal = fixtures.generateJournal({
    properties: {
      peerReviewModelId: peerReviewModel.id,
    },
    Journal,
  })

  dataService.createUserOnJournal({
    models,
    journal,
    fixtures,
    role: Team.Role.EditorialAssistant,
  })

  beforeEach(async () => {
    input = generateInput()
  })

  it('creates a new user', async () => {
    const initialUsersLength = fixtures.users.length

    await useCases.addUserFromAdminPanelUseCase
      .initialize({
        models,
        useCases,
        eventsService,
        SSOService,
        notification: notificationService,
      })
      .execute(input)

    expect(initialUsersLength).toBeLessThan(fixtures.users.length)
  })

  it('creates a new admin and add him in a team', async () => {
    input.isAdmin = true

    await useCases.addUserFromAdminPanelUseCase
      .initialize({
        models,
        useCases,
        eventsService,
        SSOService,
        notification: notificationService,
      })
      .execute(input)
    expect(
      fixtures.teams.find(team => team.role === Team.Role.admin),
    ).toBeDefined()
  })

  it('set user role in SSO when a new admin is addeed ', async () => {
    input.isAdmin = true

    await useCases.addUserFromAdminPanelUseCase
      .initialize({
        models,
        useCases,
        eventsService,
        SSOService,
        notification: notificationService,
      })
      .execute(input)
    expect(SSOService.updateUserRoleInSSO).toHaveBeenCalled()
  })

  it('creates a new ripe and add him in a team', async () => {
    input.isRIPE = true

    await useCases.addUserFromAdminPanelUseCase
      .initialize({
        models,
        useCases,
        eventsService,
        SSOService,
        notification: notificationService,
      })
      .execute(input)
    expect(
      fixtures.teams.find(
        team => team.role === Team.Role.researchIntegrityPublishingEditor,
      ),
    ).toBeDefined()
  })

  it('returns an error if you try to create both admin and ripe', async () => {
    input.isAdmin = true
    input.isRIPE = true
    const initialUsersLength = fixtures.users.length

    try {
      await useCases.addUserFromAdminPanelUseCase
        .initialize({
          models,
          useCases,
          eventsService,
          SSOService,
          notification: notificationService,
        })
        .execute(input)
    } catch (e) {
      expect(e.message).toEqual(
        'User cannot be both admin and research integrity publishing editor.',
      )
      expect(fixtures.users.length).toEqual(initialUsersLength)
    }
  })

  it('returns an error if the user already exists in the db', async () => {
    await useCases.addUserFromAdminPanelUseCase
      .initialize({
        models,
        useCases,
        eventsService,
        SSOService,
        notification: notificationService,
      })
      .execute(input)

    const initialUsersLength = fixtures.users.length

    try {
      await useCases.addUserFromAdminPanelUseCase
        .initialize({
          models,
          useCases,
          eventsService,
          SSOService,
          notification: notificationService,
        })
        .execute(input)
    } catch (e) {
      expect(initialUsersLength).toEqual(fixtures.users.length)
      expect(fixtures.users.length).toEqual(initialUsersLength)
    }
  })

  it('returns an error if the user already exists in the db (uppercase email)', async () => {
    await useCases.addUserFromAdminPanelUseCase
      .initialize({
        models,
        useCases,
        eventsService,
        SSOService,
        notification: notificationService,
      })
      .execute(input)

    const initialUsersLength = fixtures.users.length
    input.email = input.email.toUpperCase()

    try {
      await useCases.addUserFromAdminPanelUseCase
        .initialize({
          models,
          useCases,
          eventsService,
          SSOService,
          notification: notificationService,
        })
        .execute(input)
    } catch (e) {
      expect(e.message).toEqual('User already exists in the database.')
      expect(fixtures.users.length).toEqual(initialUsersLength)
    }
  })
})
