const {
  generateJournal,
  generateTeamMember,
  getTeamRoles,
  generateJournalArticleType,
} = require('component-generators')

let models = {}
let Team = {}
let User = {}
let Journal = {}
let SpecialIssue = {}

const Chance = require('chance')

const { editJournalUseCase } = require('../../src/use-cases')
const logger = require('@pubsweet/logger')

const jobsService = {
  scheduleJournalActivation: jest.fn(),
}
const eventsService = {
  publishJournalEvent: jest.fn(),
}
const chance = new Chance()
const isDateToday = jest.fn()

const generateInput = () => ({
  name: chance.company(),
  code: chance.word().toUpperCase(),
  email: chance.email(),
  issn: `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
    min: 999,
    max: 9999,
  })}`,
  apc: chance.natural(),
  articleTypes: [chance.guid(), chance.guid()],
})
describe('Edit journal use case', () => {
  let input
  let journal

  beforeEach(async () => {
    input = generateInput()
    journal = generateJournal({ name: 'name' })

    models = {
      Team: {
        Role: getTeamRoles(),
      },
      User: {
        find: jest.fn(),
        findTeamMemberByRole: jest.fn(),
      },
      Journal: { findOneBy: jest.fn() },
      JournalArticleType: jest.fn(),
      SpecialIssue: {
        findAllThatShouldBeActive: jest.fn(),
        updateMany: jest.fn(),
      },
    }
    ;({ Team, User, Journal, SpecialIssue } = models)
  })
  it('should save the new information', async () => {
    const admin = generateTeamMember({
      team: Team.Role.admin,
    })
    journal.journalArticleTypes = [
      generateJournalArticleType({
        journalId: journal.id,
      }),
    ]
    jest.spyOn(User, 'find').mockResolvedValueOnce({
      userId: 'userId',
      findTeamMemberByRole: () => admin,
    })
    jest.spyOn(Journal, 'findOneBy').mockResolvedValueOnce(journal)

    await editJournalUseCase
      .initialize({ models, jobsService, eventsService, isDateToday, logger })
      .execute({
        input,
        adminId: admin.userId,
        id: journal.id,
        reqUserId: admin.userId,
      })
    expect(journal.saveGraph).toHaveBeenCalledTimes(1)
    expect(journal.updateProperties).toBeCalledTimes(1)
    expect(eventsService.publishJournalEvent).toHaveBeenCalledTimes(1)
  })

  it('activates the journal if the activation date is set to today and does not schedule job', async () => {
    const admin = generateTeamMember({
      team: Team.Role.admin,
    })
    jest.spyOn(User, 'find').mockResolvedValueOnce({
      userId: 'userId',
      findTeamMemberByRole: () => admin,
    })
    jest.spyOn(Journal, 'findOneBy').mockResolvedValueOnce(journal)
    journal.journalArticleTypes = [
      generateJournalArticleType({
        journalId: journal.id,
      }),
    ]
    jest
      .spyOn(SpecialIssue, 'findAllThatShouldBeActive')
      .mockResolvedValueOnce([])

    input.activationDate = new Date().toISOString()
    const isDateToday = jest.fn(() => true)
    input.isActive = isDateToday()

    await editJournalUseCase
      .initialize({ models, jobsService, eventsService, isDateToday })
      .execute({
        input,
        adminId: admin.userId,
        id: journal.id,
        reqUserId: admin.userId,
      })

    expect(journal.scheduleJob).toHaveBeenCalledTimes(0)
  })
  it('schedules job if the activation date is in the future', async () => {
    const admin = generateTeamMember({
      team: Team.Role.admin,
    })
    jest.spyOn(User, 'find').mockResolvedValueOnce({
      userId: 'userId',
      findTeamMemberByRole: () => admin,
    })
    jest.spyOn(Journal, 'findOneBy').mockResolvedValueOnce(journal)

    journal.journalArticleTypes = [
      generateJournalArticleType({
        journalId: journal.id,
      }),
    ]
    const futureDate = new Date()
    futureDate.setDate(futureDate.getDate() + 2)

    journal.activationDate = futureDate
    journal.isActive = true
    input.activationDate = futureDate

    await editJournalUseCase
      .initialize({ models, jobsService, eventsService, isDateToday })
      .execute({
        input,
        adminId: admin.userId,
        id: journal.id,
        reqUserId: admin.userId,
      })

    expect(journal.scheduleJob).toHaveBeenCalledTimes(1)
    expect(journal.cancelJobs).toHaveBeenCalledTimes(1)
  })
})
