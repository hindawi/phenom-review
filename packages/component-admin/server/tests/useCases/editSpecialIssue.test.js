const {
  getJobTypes,
  getTeamRoles,
  generateTeamMember,
  generateSpecialIssue,
} = require('component-generators')

const editSpecialIssueUseCase = require('../../src/use-cases/specialIssue/edit')

let models = {}
let SpecialIssue = {}
let TeamMember = {}

const dateService = { areDatesEqual: jest.fn(() => false) }
const jobsService = { scheduleSpecialIssueDeactivation: jest.fn() }
const eventsService = {
  publishJournalEvent: jest.fn(),
  publishSpecialIssueEvent: jest.fn(),
}

describe('editSpecialIssue', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    models = {
      SpecialIssue: {
        find: jest.fn(),
        findUnique: jest.fn(),
      },
      Team: {
        Role: getTeamRoles(),
      },
      TeamMember: {
        findOneByRole: jest.fn(),
      },
      Job: {
        Type: getJobTypes(),
      },
    }
    ;({ SpecialIssue, TeamMember } = models)
  })
  it('Throws an error if the end date is before start date', async () => {
    try {
      await editSpecialIssueUseCase.initialize({ models }).execute({
        input: {
          startDate: new Date(),
          endDate: new Date() - 1,
        },
      })
    } catch (err) {
      expect(err.message).toBe('End date must be after start date.')
    }
  })
  it('Throws an error if the special issue already exists', async () => {
    const specialIssue = generateSpecialIssue()
    const input = {
      name: 'new special issue name',
    }

    jest.spyOn(SpecialIssue, 'find').mockResolvedValue(specialIssue)
    jest.spyOn(SpecialIssue, 'findUnique').mockResolvedValue(specialIssue)

    try {
      await editSpecialIssueUseCase.initialize({ models }).execute({
        input,
        id: specialIssue.id,
      })
    } catch (err) {
      expect(err.message).toBe('Special issue already exists.')
    }
  })
  it('Executes main flow', async () => {
    const specialIssue = generateSpecialIssue({
      toDTO: jest.fn(),
      cancelJobByType: jest.fn(),
      handleActivation: jest.fn(),
    })
    const input = {
      name: specialIssue.name,
    }
    const admin = generateTeamMember()

    jest.spyOn(SpecialIssue, 'find').mockResolvedValue(specialIssue)
    jest.spyOn(TeamMember, 'findOneByRole').mockResolvedValue(admin)

    await editSpecialIssueUseCase
      .initialize({ models, dateService, jobsService, eventsService })
      .execute({
        input,
        id: specialIssue.id,
      })

    expect(specialIssue.save).toHaveBeenCalledTimes(1)
    expect(jobsService.scheduleSpecialIssueDeactivation).toHaveBeenCalledTimes(
      1,
    )
    expect(eventsService.publishJournalEvent).toHaveBeenCalledTimes(1)
    expect(eventsService.publishSpecialIssueEvent).toHaveBeenCalledTimes(1)
  })
})
