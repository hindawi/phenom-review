const { models, fixtures } = require('fixture-service')

const { cancelSpecialIssueUseCase } = require('../../src/use-cases')

const eventsService = {
  publishJournalEvent: jest.fn(async () => {}),
  publishSpecialIssueEvent: jest.fn(async () => {}),
}

describe('Cancel special issue use case', () => {
  it('updates special issue is cancelled and reason fields', async () => {
    const { SpecialIssue } = models
    const specialIssue = await fixtures.generateSpecialIssue({
      properties: {
        isActive: true,
        isCancelled: false,
      },
      SpecialIssue,
    })
    const input = { reason: 'Doamniagiutā' }

    await cancelSpecialIssueUseCase
      .initialize({ models, eventsService })
      .execute({ id: specialIssue.id, input })

    expect(specialIssue.isCancelled).toBe(true)
    expect(specialIssue.cancelReason).toBe(input.reason)
    expect(eventsService.publishJournalEvent).toHaveBeenCalledTimes(1)
    expect(eventsService.publishSpecialIssueEvent).toHaveBeenCalledTimes(1)
  })
  it('throws an error if an inactive special issue is cancelled', async () => {
    const { SpecialIssue } = models
    const specialIssue = await fixtures.generateSpecialIssue({
      properties: {
        isActive: false,
        isCancelled: false,
      },
      SpecialIssue,
    })
    const input = { reason: 'Doamniagiutā' }

    const result = cancelSpecialIssueUseCase
      .initialize({ models, eventsService })
      .execute({ id: specialIssue.id, input })

    return expect(result).rejects.toThrow()
  })
})
