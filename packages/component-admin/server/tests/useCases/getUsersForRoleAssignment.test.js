process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')

const chance = new Chance()

const { models, fixtures } = require('fixture-service')
const { getUsersForEditorialAssignmentUseCase } = require('../../src/use-cases')

describe('Get users for role assignment use case', () => {
  it('returns an array', async () => {
    const users = await getUsersForEditorialAssignmentUseCase
      .initialize(models)
      .execute({ input: '' })

    expect(Array.isArray(users)).toEqual(true)
  })
  it('returns only active users', async () => {
    const { User, Identity } = models

    const identityProp = {
      email: chance.email(),
      surname: chance.name(),
      givenNames: chance.name(),
    }

    fixtures.generateUser({
      properties: { isConfirmed: true, isActive: true },
      identityProperties: identityProp,
      User,
      Identity,
    })

    const users = await getUsersForEditorialAssignmentUseCase
      .initialize(models)
      .execute({ input: identityProp.email })

    expect(users.length).toEqual(1)
    expect(users[0].isActive).toEqual(true)
  })
})
