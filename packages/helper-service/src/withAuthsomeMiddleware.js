/*
  Middleware that applies authsome policies to every GraphQL resolver.
*/

const standardTypes = ['Query', 'Mutation']
const withAuthsomeMiddleware = (resolvers = {}, useCases = {}) =>
  Object.entries(resolvers).reduce(
    (acc, [gqlType, gqlResolvers]) => ({
      ...acc,
      [gqlType]: standardTypes.includes(gqlType)
        ? Object.entries(gqlResolvers).reduce(
            (acc, [name, fn]) => ({
              ...acc,
              [name]: async (parentQuery, params, ctx) => {
                await ctx.helpers.can(
                  ctx.user,
                  {
                    name,
                    policies: useCases[`${name}UseCase`].authsomePolicies,
                    toString: () => gqlType,
                  },
                  {
                    ...params,
                    toString: () => name,
                  },
                )

                return fn(parentQuery, params, ctx)
              },
            }),
            {},
          )
        : gqlResolvers,
    }),
    {},
  )

module.exports = withAuthsomeMiddleware
