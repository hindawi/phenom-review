const withAuthsomeMiddleware = require('./withAuthsomeMiddleware')
const services = require('./services')

module.exports = {
  withAuthsomeMiddleware,
  services,
}
