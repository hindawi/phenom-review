import Keycloak from 'keycloak-js'

const init = (config, onSuccess, onError) => {
  const { authServerURL, realm, clientID } = config

  const keycloak = Keycloak({
    url: authServerURL,
    clientId: clientID,
    realm,
  })

  // https://www.keycloak.org/docs/latest/securing_apps/index.html#_javascript_adapter
  keycloak
    .init({
      onLoad: 'check-sso',
      silentCheckSsoRedirectUri: `${window.location.origin}/silent-check-sso`,
      checkLoginIframe: process.env.NODE_ENV !== 'development',
    })
    .then(() => {
      onSuccess(keycloak)
    })
    .catch(error => {
      onError(error, keycloak)
    })
}

export default {
  init,
}
