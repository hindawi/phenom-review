import useKeycloak from './useKeycloak'

const roles = {
  Academic_editor: 'academic_editor',
  Editorial_assistant: 'editorial_assistant',
  User: 'user',
  Admin: 'admin',
}

function useKeycloakRole() {
  const keycloak = useKeycloak()

  const { clientId, hasResourceRole } = keycloak
  const hasRole = role => hasResourceRole(role, clientId)

  const hasAccessRights = allow => {
    // either allow was not provided or is just an empty array
    if (!allow || !allow.length) return true

    return allow.map(role => hasResourceRole(role, clientId)).some(Boolean)
  }

  return {
    roles,
    hasAERole: () => hasRole(roles.Academic_editor),
    hasEARole: () => hasRole(roles.Editorial_assistant),
    hasAdminRole: () => hasRole(roles.Admin),
    hasAccessRights,
  }
}

export default useKeycloakRole
