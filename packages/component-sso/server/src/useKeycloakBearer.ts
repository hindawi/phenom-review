const config = require('config')
const jwt = require('jsonwebtoken')
const jwksClient = require('jwks-rsa')
const path = require('path')
const BearerStrategy = require('passport-http-bearer').Strategy

const { checkUserIdentity } = require('./keycloakService')

const client = jwksClient({
  cache: true,
  jwksUri: config.keycloak.certs,
})

const getKey = (header, callback) => {
  client.getSigningKey(header.kid, (err, key) => {
    if (err) {
      callback(err)
      return
    }
    const signingKey = key.publicKey || key.rsaPublicKey
    callback(null, signingKey)
  })
}

interface DecodedToken {
  sub: String
  username: String
  email: String
  name: String
  family_name: String
  given_name: String
  preferred_username: String
  realm_access: { roles: String[] }
  resource_access: { clientId: { roles: String[] } }
  clientId: String
}

const checkIdentityAndCallback = (token: String, done: Function) => async (
  err: Error | String,
  decoded: DecodedToken,
) => {
  if (err || !decoded) return done(null)
  const { sub, username, resource_access, clientId } = decoded

  const user = await checkUserIdentity(decoded)

  const contextDetails = {
    id: user.userId || sub,
    username,
    token,
    roles: resource_access?.clientId?.roles,
  }

  return done(null, user.userId || sub, contextDetails)
}

const verifyToken = (token: String, done: Function): void =>
  jwt.verify(
    token,
    getKey,
    {
      algorithms: ['RS256'],
    },
    checkIdentityAndCallback(token, done),
  )

const useKeycloakBearer = () => app => {
  app.get('/silent-check-sso', (_, res, __) => {
    const filename =
      config.util.getEnv('NODE_ENV') === 'production'
        ? path.join('../app-review/', '_build', 'assets')
        : path.join('../app-review/', 'app')

    res.sendFile('silent-check-sso.html', { root: filename })
  })

  if (!config.keycloak.public.authServerURL) {
    return
  }
  const { passport } = app.locals
  passport.use('keycloakBearer', new BearerStrategy(verifyToken))

  app.use(
    '/graphql',
    passport.authenticate(['bearer', 'keycloakBearer', 'anonymous'], {
      session: false,
    }),
  )
}

module.exports = useKeycloakBearer
