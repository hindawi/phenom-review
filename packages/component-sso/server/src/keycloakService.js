const config = require('config')
const { logger } = require('component-logger')
const { User, Identity, Team } = require('@pubsweet/models')
const { roleLabels } = require('./utils')
const {
  default: KeycloakAdminClient,
  requiredAction,
} = require('keycloak-admin')
const { rorService } = require('component-ror')

const {
  public: { authServerURL, realm, clientID },
  admin: { username, password },
} = config.get('keycloak')

const labelMap = {
  [Team.Role.admin]: roleLabels.admin,
  [Team.Role.editorialAssistant]: roleLabels.editorialAssistant,
  [Team.Role.triageEditor]: roleLabels.triageEditor,
  [Team.Role.academicEditor]: roleLabels.academicEditor,
}

const initKeycloakAdminClient = async () => {
  const kcAdminClient = new KeycloakAdminClient({
    baseUrl: authServerURL,
    realmName: 'master',
  })
  await kcAdminClient.auth({
    username,
    password,
    grantType: 'password',
    clientId: 'admin-cli',
  })
  kcAdminClient.setConfig({
    realmName: realm,
  })

  const findOneByEmail = async email => {
    const users = await kcAdminClient.users.find({ email })
    if (!users.length) return
    return users[0]
  }

  const sendVerifyEmail = async ({ id }) => {
    logger.info(`Sending keycloak verify email to keycloak user with id: ${id}`)

    return kcAdminClient.users.sendVerifyEmail({
      id,
      clientId: clientID,
      redirectUri: config.get('pubsweet-client.baseUrl'),
    })
  }

  const createFromProfile = ({
    id,
    aff,
    affRorId,
    email,
    title,
    country,
    surname,
    givenNames,
  }) => {
    logger.info(`Creating keycloak user for: ${email}`)
    return kcAdminClient.users
      .create({
        username: email,
        email,
        enabled: true,
        emailVerified: false,
        firstName: givenNames,
        lastName: surname,
        requiredActions: [requiredAction.UPDATE_PASSWORD],
        attributes: {
          phenomId: id,
          country,
          affiliation: aff,
          rorId: affRorId,
          title,
        },
      })
      .then(sendVerifyEmail)
  }

  const updateUserAttributes = async (id, attributes) => {
    logger.info(`Updating keycloak user for id: ${id}`)
    return kcAdminClient.users.update({ id }, { attributes })
  }

  const updateUserRole = async (userEmail, roleLabel) => {
    const clientId = process.env.KEYCLOAK_CLIENTID
    const [currentClient] = await kcAdminClient.clients.find({ clientId })

    const [user, role] = await Promise.all([
      findOneByEmail(userEmail),
      kcAdminClient.clients.findRole({
        id: currentClient.id,
        roleName: roleLabel,
      }),
    ])

    if (!user || !role) return

    return kcAdminClient.users.addClientRoleMappings({
      id: user.id,
      clientUniqueId: currentClient.id,
      roles: [role],
    })
  }

  const removeUserRole = async (userEmail, roleLabel) => {
    const clientId = process.env.KEYCLOAK_CLIENTID
    const [currentClient] = await kcAdminClient.clients.find({ clientId })

    const [user, role] = await Promise.all([
      findOneByEmail(userEmail),
      kcAdminClient.clients.findRole({
        id: currentClient.id,
        roleName: roleLabel,
      }),
    ])

    if (!user || !role) return

    return kcAdminClient.users.delClientRoleMappings({
      id: user.id,
      clientUniqueId: currentClient.id,
      roles: [role],
    })
  }

  return {
    findOneByEmail,
    sendVerifyEmail,
    createFromProfile,
    updateUserAttributes,
    updateUserRole,
    removeUserRole,
  }
}

const createSSOUser = async profile => {
  const kcAdminClient = await initKeycloakAdminClient()

  const userFromSSO = await kcAdminClient.findOneByEmail(profile.identity.email)

  if (userFromSSO) {
    logger.info(
      `Skipping keycloak user creation for: ${profile.identity.email}`,
    )

    if (!userFromSSO.emailVerified) {
      return kcAdminClient.sendVerifyEmail(userFromSSO)
    }

    return userFromSSO
  }

  return kcAdminClient.createFromProfile({
    ...profile.identity,
    id: profile.user.id,
  })
}

const createDBUser = async profile => {
  logger.info(`Creating user from keycloak for ${profile.email}`)

  const user = new User({
    agreeTc: true,
    isActive: true,
    id: profile.sub,
    defaultIdentityType: 'local',
  })

  const identity = new Identity({
    type: 'local',
    userId: profile.sub,
    isConfirmed: profile.email_verified,
    email: profile.email,
    aff: profile.affiliation,
    affRorId: profile.rorId,
    country: profile.country || '',
    surname: profile.family_name,
    givenNames: profile.given_name,
    title: profile.title,
  })

  user.assignIdentity(identity)

  return user.saveGraph()
}

const confirmIdentity = async identity => {
  logger.info(`Confirming local identity for ${identity.email}`)

  identity.isConfirmed = true
  await identity.save()

  return identity
}

const createRegistrationURL = (redirect_uri = process.env.CLIENT_SERVER_URL) =>
  `${authServerURL}/realms/${realm}/protocol/openid-connect/registrations?client_id=${clientID}&redirect_uri=${encodeURIComponent(
    redirect_uri,
  )}&response_mode=fragment&response_type=code&scope=openid`

const updateUserPhenomIdAttribute = async (ssoUser, dbUser) => {
  const kcAdminClient = await initKeycloakAdminClient()

  if (ssoUser.phenomId) {
    return
  }

  const { country, title, affiliation, rorId } = ssoUser

  return kcAdminClient.updateUserAttributes(ssoUser.sub, {
    phenomId: dbUser.id,
    country,
    title,
    affiliation,
    rorId,
  })
}

const appendRORInfoIfMissing = async identity => {
  if (identity.affiliation && !identity.rorId) {
    const bestROR = await rorService.getBestROR(
      identity.affiliation,
      identity.country,
    )
    if (bestROR) {
      identity.rorId = bestROR.organization.id
    }
  }
}

async function addAffiliationDetailsIfMissing(profile, identity) {
  let thereAreUpdates = false
  if (!identity.aff && !!profile.affiliation) {
    thereAreUpdates = true
    identity.aff = profile.affiliation
  }
  if (!identity.affRorId && !!profile.rorId) {
    thereAreUpdates = true
    identity.affRorId = profile.rorId
  }

  if (!identity.country && !!profile.country) {
    thereAreUpdates = true
    identity.country = profile.country
  }

  if (!identity.title && !!profile.title) {
    thereAreUpdates = true
    identity.title = profile.title
  }

  if (thereAreUpdates) {
    identity = await identity.save()
  }

  return identity
}

const checkUserIdentity = async profile => {
  let identity = await Identity.findOneByEmail(profile.email)

  if (identity) {
    identity = await addAffiliationDetailsIfMissing(profile, identity)

    if (profile.email_verified && !identity.isConfirmed) {
      await confirmIdentity(identity)
    }
    return identity
  }

  await appendRORInfoIfMissing(profile)
  const user = await createDBUser(profile)

  await updateUserPhenomIdAttribute(profile, user)

  return profile
}

const updateUserRole = async (userEmail, roleLabel) => {
  const kcAdminClient = await initKeycloakAdminClient()

  return kcAdminClient.updateUserRole(userEmail, roleLabel)
}

const updateUserRoleInSSO = async (userEmail, role) =>
  updateUserRole(userEmail, labelMap[role])

const removeUserRole = async (userEmail, roleLabel) => {
  const kcAdminClient = await initKeycloakAdminClient()

  return kcAdminClient.removeUserRole(userEmail, roleLabel)
}

const removeUserRoleInSSO = async (userEmail, role) =>
  removeUserRole(userEmail, labelMap[role])

module.exports = {
  createSSOUser,
  createDBUser,
  createRegistrationURL,
  checkUserIdentity,
  updateUserRole,
  removeUserRole,
  updateUserRoleInSSO,
  removeUserRoleInSSO,
}
