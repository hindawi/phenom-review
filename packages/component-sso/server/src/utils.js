const roleLabels = {
  editorialAssistant: 'editorial_assistant',
  triageEditor: 'triage_editor',
  academicEditor: 'academic_editor',
  admin: 'admin',
}

module.exports = {
  roleLabels,
}
