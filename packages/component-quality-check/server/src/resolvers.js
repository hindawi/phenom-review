const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')
const { logEvent } = require('component-activity-log/server')
const events = require('component-events')

const useCases = require('./useCases')
const {
  useCases: { validateFilesUseCase },
} = require('component-files')

const { File } = models
const fileValidator = validateFilesUseCase.initialize({
  File,
})

const resolvers = {
  Query: {},
  Mutation: {
    async submitQualityChecks(_, { submissionId }, ctx) {
      const eventsService = events.initialize({ models })
      return useCases.submitQualityChecksUseCase
        .initialize({
          models,
          logEvent,
          fileValidator,
          eventsService,
        })
        .execute({ submissionId, userId: ctx.user })
    },
    async updateQualityChecksDraft(_, { manuscriptId, autosaveInput }) {
      return useCases.updateQualityChecksDraftUseCase
        .initialize(models)
        .execute({ manuscriptId, autosaveInput })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
