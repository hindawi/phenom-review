const initialize = ({
  models: { Manuscript, TeamMember, Team, User, ArticleType },
  notificationService,
}) => ({
  async execute(data) {
    const { submissionId } = data

    const lastManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
      eagerLoadRelations: ['journal,articleType'],
    })
    lastManuscript.updateProperties({
      status: Manuscript.Statuses.published,
      qualityCheckPassedDate: new Date().toISOString(),
    })
    await lastManuscript.save()

    const articleTypeName =
      lastManuscript.articleType && lastManuscript.articleType.name
    if (ArticleType.ShortArticleTypes.includes(articleTypeName)) {
      const submittingStaffMember = await TeamMember.findOneByManuscriptAndRole(
        {
          manuscriptId: lastManuscript.id,
          role: Team.Role.submittingStaffMember,
          eagerLoadRelations: 'user',
        },
      )

      await notificationService.notifySubmitterWhenManuscriptHasPassedQC({
        manuscript: lastManuscript,
        submittingStaffMember,
      })
    } else {
      const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
        {
          manuscriptId: lastManuscript.id,
          role: Team.Role.editorialAssistant,
          status: TeamMember.Statuses.active,
        },
      )
      const submittingAuthor = await TeamMember.findSubmittingAuthor(
        lastManuscript.id,
      )
      submittingAuthor.user = await User.find(submittingAuthor.userId)

      await notificationService.notifyAuthorWhenMaterialChecksIsCompleted({
        editorialAssistant,
        submittingAuthor,
        manuscript: lastManuscript,
      })
    }
  },
})

module.exports = {
  initialize,
}
