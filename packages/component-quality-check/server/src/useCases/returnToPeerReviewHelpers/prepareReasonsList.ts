import { ReturnToPRReasons, Reason, ImproperReview, COI } from './queue.utils'

export const prepareReasonsList = (
  conflictOfInterest: COI[],
  improperReview: ImproperReview,
  decisionMadeInError: boolean,
): Reason[] => {
  const reasons: Reason[] = [].concat(conflictOfInterest)

  if (improperReview.reviewers && improperReview.reviewers.length) {
    reasons.push({
      type: ReturnToPRReasons.improperReview,
      conflictedTeamMembers: improperReview.reviewers,
    })
  }
  if (decisionMadeInError) {
    reasons.push({ type: ReturnToPRReasons.decisionMadeInError })
  }
  return reasons
}
