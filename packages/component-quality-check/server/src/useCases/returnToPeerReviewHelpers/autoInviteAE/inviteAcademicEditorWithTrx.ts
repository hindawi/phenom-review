import config from 'config'
import { Promise } from 'bluebird'

import { logEvent } from 'component-activity-log/server'
import { TaskType } from '../ScheduledTasksQueue'

const models = require('@pubsweet/models')

const {
  Manuscript,
  TeamMember,
  Job,
  Team,
  PeerReviewModel,
  User,
  Journal,
} = models

const { logger } = require('component-logger')

export const inviteAcademicEditorUseCase = async ({
  trx,
  scheduledTasksQueue,
  manuscript,
  userId,
}) => {
  const editorialAssistantLabel = config.get('roleLabels.editorialAssistant')
  const academicEditorLabel = await getAcademicEditorLabel(manuscript)

  if (manuscriptStatusInvalidForAEInvite(manuscript.status)) {
    throw new Error(
      `Cannot invite an Academic Editor when the manuscript status is ${manuscript.status}`,
    )
  }

  if (await academicEditorIsAlreadyInvited(userId, manuscript.id)) {
    throw new Error(`The ${academicEditorLabel} is already invited.`)
  }

  if (await academicEditorIsUnfit(userId, manuscript)) {
    throw new Error(`The ${academicEditorLabel} can't be invited.`)
  }

  await addAcademicEditorAsPendingToManuscript({
    trx,
    scheduledTasksQueue,
    userId,
    manuscript,
  })

  const journal = await getJournal(manuscript.journalId)
  const editorialAssistant = await getEditorialAssistant(manuscript.id)
  const triageEditor = await getTriageEditor(manuscript.id)
  const triageEditorLabel = await getTriageEditorLabel(manuscript)
  const submittingAuthor = await getSubmittingAuthor(manuscript.id)
  const authorTeamMembers = await getAuthorTeamMembers(manuscript.id)
  const academicEditor = await getPendingAcademicEditor(trx, manuscript.id)
  academicEditor.user = await getUser(userId)

  schedulePublishAcademicEditorInvitedEvent(
    scheduledTasksQueue,
    manuscript.submissionId,
  )
  scheduleSendInvitationToAcademicEditor(scheduledTasksQueue, {
    journal,
    manuscript,
    triageEditor,
    academicEditor,
    submittingAuthor,
    authorTeamMembers,
    editorialAssistant,
    academicEditorLabel,
    eventsService: scheduledTasksQueue.eventsService,
  })
  scheduleLogActivityForUserInvitation(
    scheduledTasksQueue,
    userId,
    manuscript.id,
  )
  scheduleEmailJobForInvitedAE(scheduledTasksQueue, {
    journal,
    manuscript,
    triageEditor,
    academicEditor,
    submittingAuthor,
    authorTeamMembers,
    triageEditorLabel,
    editorialAssistant,
    editorialAssistantLabel,
  })
  scheduleRemovalJobForAE(scheduledTasksQueue, academicEditor.id, manuscript.id)
}

const manuscriptStatusInvalidForAEInvite = status =>
  [
    Manuscript.Statuses.deleted,
    Manuscript.Statuses.withdrawn,
    Manuscript.Statuses.revisionRequested,
  ].includes(status)

const academicEditorIsAlreadyInvited = async (userId, manuscriptId) => {
  const ae = await getAcademicEditorFromManuscript(userId, manuscriptId)
  return ae && ae.status !== TeamMember.Statuses.expired
}

const academicEditorIsUnfit = async (userId, manuscript) => {
  const allUnfitAE = await getAllUnfitAcademicEditorsFromSubmission(
    manuscript.submissionId,
  )
  return allUnfitAE.some(ae => ae.userId === userId)
}

const addAcademicEditorAsPendingToManuscript = async ({
  trx,
  scheduledTasksQueue,
  userId,
  manuscript,
}) => {
  const expiredAcademicEditors = await computeExpiredAcademicEditors(manuscript)
  const pendingAcademicEditors = await computePendingAcademicEditors(
    userId,
    manuscript,
  )

  await updateExpiredAcademicEditors(trx, expiredAcademicEditors)
  await addPendingAcademicEditors(trx, pendingAcademicEditors)

  await scheduleCancelJobsForAcademicEditors(
    scheduledTasksQueue,
    expiredAcademicEditors,
  )
}

const computeExpiredAcademicEditors = async manuscript => {
  const academicEditors = await getAllPendingAcademicEditorsFromSubmission(
    manuscript.submissionId,
  )
  academicEditors.forEach(ae => (ae.status = TeamMember.Statuses.expired))

  return academicEditors
}

const computePendingAcademicEditors = async (userId, manuscript) => {
  const results: Promise<any> = []
  const academicEditorUser = await getUser(userId)
  const manuscripts = await getAllManuscripts(manuscript.submissionId)

  await Promise.each(manuscripts, async manuscript => {
    const academicEditorTeam = await getOrCreateAcademicEditorTeam(
      manuscript.id,
    )
    const academicEditor = createAcademicEditorForTeam(
      academicEditorUser,
      academicEditorTeam,
    )
    results.push(academicEditor)
  })

  return results
}

const updateExpiredAcademicEditors = async (trx, academicEditors) => {
  try {
    await Promise.each(academicEditors, ae => ae.$query(trx).update(ae))
  } catch (err) {
    logger.error(
      'Something went wrong when updating expired AE. No data was updated.',
      err,
    )
    throw new Error(
      'Something went wrong when updating expired AE. No data was updated.',
    )
  }
}

const addPendingAcademicEditors = async (trx, academicEditors) => {
  try {
    await Promise.each(academicEditors, ae =>
      ae
        .$query(trx)
        .insert(ae)
        .onConflict('id')
        .merge(),
    )
  } catch (err) {
    logger.error(
      'Something went wrong when updating pending AE. No data was updated.',
      err,
    )
    throw new Error(
      'Something went wrong when updating pending AE. No data was updated.',
    )
  }
}

const createAcademicEditorForTeam = async (user, team) =>
  team.addMember({
    user,
    options: {
      status: TeamMember.Statuses.pending,
      invitedDate: new Date().toISOString(),
    },
  })

const getAllManuscripts = async submissionId =>
  Manuscript.findManuscriptsBySubmissionId({
    submissionId,
    order: 'ASC',
    orderByField: 'version',
  })

const getAcademicEditorFromManuscript = async (userId, manuscriptId) =>
  TeamMember.findOneByManuscriptAndRoleAndUser({
    role: Team.Role.academicEditor,
    userId,
    manuscriptId,
  })

const getAllUnfitAcademicEditorsFromSubmission = async submissionId =>
  TeamMember.findAllBySubmissionAndRoleAndStatuses({
    role: Team.Role.academicEditor,
    submissionId,
    statuses: [TeamMember.Statuses.declined, TeamMember.Statuses.removed],
  })

const getAllPendingAcademicEditorsFromSubmission = async submissionId =>
  TeamMember.findAllBySubmissionAndRoleAndStatuses({
    role: Team.Role.academicEditor,
    submissionId,
    statuses: [TeamMember.Statuses.pending],
  })

const getUser = async userId => User.find(userId, 'identities')

const getOrCreateAcademicEditorTeam = async manuscriptId =>
  Team.findOrCreate({
    queryObject: {
      manuscriptId,
      role: Team.Role.academicEditor,
    },
    options: { manuscriptId, role: Team.Role.academicEditor },
    eagerLoadRelations: ['members'],
  })

const getJournal = async journalId => Journal.find(journalId)

const getEditorialAssistant = async manuscriptId =>
  TeamMember.findOneByManuscriptAndRoleAndStatus({
    manuscriptId,
    role: Team.Role.editorialAssistant,
    status: TeamMember.Statuses.active,
  })

const getTriageEditor = async manuscriptId =>
  TeamMember.findOneByManuscriptAndRole({
    manuscriptId,
    role: Team.Role.triageEditor,
  })

const getSubmittingAuthor = async manuscriptId =>
  TeamMember.findSubmittingAuthor(manuscriptId)

const getPendingAcademicEditor = async (trx, manuscriptId) =>
  TeamMember.findOneByManuscriptAndRoleAndStatus({
    manuscriptId,
    role: Team.Role.academicEditor,
    status: TeamMember.Statuses.pending,
    trx,
  })

const getAuthorTeamMembers = async manuscriptId =>
  TeamMember.findAllByManuscriptAndRole({
    role: Team.Role.author,
    manuscriptId,
  })

const getAcademicEditorLabel = async manuscript =>
  manuscript.getEditorLabel({
    Team,
    TeamMember,
    PeerReviewModel,
    role: Team.Role.academicEditor,
  })

const getTriageEditorLabel = async manuscript =>
  manuscript.getEditorLabel({
    Team,
    TeamMember,
    PeerReviewModel,
    role: Team.Role.triageEditor,
  })

const scheduleCancelJobsForAcademicEditors = async (
  scheduledTasksQueue,
  academicEditors = [],
) => {
  if (academicEditors.length === 0) {
    return
  }

  const jobs = await Job.findAllByTeamMembers(academicEditors.map(ae => ae.id))

  scheduledTasksQueue.push([
    {
      type: TaskType.jobs,
      svMethod: 'cancelJobs',
      payload: jobs,
    },
  ])
}

const schedulePublishAcademicEditorInvitedEvent = (
  scheduledTasksQueue,
  submissionId,
) => {
  scheduledTasksQueue.push([
    {
      type: TaskType.event,
      svMethod: 'publishSubmissionEvent',
      payload: {
        submissionId,
        eventName: 'SubmissionAcademicEditorInvited',
      },
    },
  ])
}

const scheduleLogActivityForUserInvitation = (
  scheduledTasksQueue,
  userId,
  manuscriptId,
) => {
  scheduledTasksQueue.push([
    {
      type: TaskType.activityLog,
      payload: {
        userId: null,
        manuscriptId,
        action: logEvent.actions.invitation_sent,
        objectType: logEvent.objectType.user,
        objectId: userId,
      },
    },
  ])
}

const scheduleSendInvitationToAcademicEditor = (
  scheduledTasksQueue,
  payload,
) => {
  scheduledTasksQueue.push([
    {
      type: TaskType.invitation,
      svMethod: 'sendInvitationToAcademicEditor',
      payload,
    },
  ])
}

const scheduleEmailJobForInvitedAE = (scheduledTasksQueue, payload) => {
  scheduledTasksQueue.push([
    {
      type: TaskType.emailJob,
      svMethod: 'scheduleEmailsWhenAcademicEditorIsInvited',
      payload,
    },
  ])
}

const scheduleRemovalJobForAE = (
  scheduledTasksQueue,
  invitationId,
  manuscriptId,
) => {
  const timeUnit = config.get(
    'reminders.academicEditor.acceptInvitation.timeUnit',
  )
  const days = config.get('reminders.academicEditor.acceptInvitation.remove')

  scheduledTasksQueue.push([
    {
      type: TaskType.removeJob,
      svMethod: 'scheduleRemovalJobForAcademicEditor',
      payload: {
        timeUnit,
        days,
        manuscriptId,
        invitationId,
      },
    },
  ])
}
