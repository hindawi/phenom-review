// @ts-nocheck
// ^ remove this after merging ts models
import models from '@pubsweet/models'
import { ReturnToPRReasons, TaskPayloadWithTrx } from '../queue.utils'
import { findValidReviews } from './utils'

const computeNewStatus = async ({
  trx,
  reason,
  submissionId,
  latestManuscript,
  improperReviewAndCOIReviewers,
}) => {
  const {
    Manuscript,
    Team,
    Review,
    TeamMember,
    PeerReviewEditorialMapping,
  } = models

  const approvalEditor = await PeerReviewEditorialMapping.getApprovalEditorRoleForSubmission(
    submissionId,
  )

  const computeNewStatusForAEReason = async () => {
    if (approvalEditor === Team.Role.academicEditor)
      return Manuscript.Statuses.academicEditorInvited

    return Manuscript.Statuses.submitted
  }

  const computeNewStatusForReviewersOrImproperReview = async () => {
    const reviews = await findValidReviews({
      submissionId,
      improperReviewAndCOIReviewers,
      models: { Manuscript, Review, Team },
    })

    if (!reviews.length) {
      const acceptedReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
        {
          trx,
          role: Team.Role.reviewer,
          manuscriptId: latestManuscript.id,
          status: TeamMember.Statuses.accepted,
        },
      )
      if (acceptedReviewers.length) {
        return Manuscript.Statuses.underReview
      }
      return Manuscript.Statuses.academicEditorAssigned
    }

    if (approvalEditor === Team.Role.academicEditor) {
      return Manuscript.Statuses.makeDecision
    }
    return Manuscript.Statuses.reviewCompleted
  }

  const computeNewStatusForDecisionMadeInError = async () => {
    if (approvalEditor === Team.Role.academicEditor) {
      return Manuscript.Statuses.makeDecision
    }
    return Manuscript.Statuses.pendingApproval
  }

  const mapReasonToStatus = {
    [ReturnToPRReasons.triageEditor]: () =>
      Manuscript.Statuses.academicEditorInvited,
    [ReturnToPRReasons.academicEditor]: computeNewStatusForAEReason,
    [ReturnToPRReasons.reviewers]: computeNewStatusForReviewersOrImproperReview,
    [ReturnToPRReasons.improperReview]: computeNewStatusForReviewersOrImproperReview,
    [ReturnToPRReasons.decisionMadeInError]: computeNewStatusForDecisionMadeInError,
  }

  return mapReasonToStatus[reason]()
}

export const updateManuscriptStatus = async ({
  improperReviewAndCOIReviewers,
  submissionId,
  reason,
  trx,
}: TaskPayloadWithTrx) => {
  const { Manuscript } = models

  const latestManuscript = await Manuscript.findLastManuscriptBySubmissionId({
    submissionId,
  })

  const newStatus = await computeNewStatus({
    trx,
    reason,
    submissionId,
    latestManuscript,
    improperReviewAndCOIReviewers,
  })

  await latestManuscript.$query(trx).patch({ status: newStatus })
}
