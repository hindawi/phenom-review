import { Manuscript } from '@pubsweet/models'
import { TaskPayloadWithTrx, ReturnToPRReasons } from '../queue.utils'

export const invalidateTriageEditor = async ({
  reason,
  submissionId,
  trx,
}: TaskPayloadWithTrx) => {
  const latestManuscript = await Manuscript.findLastManuscriptBySubmissionId({
    submissionId,
  })

  // if there is a conflict between author and AE(from journal) we don't need to invalidate the TE
  if (
    reason === ReturnToPRReasons.academicEditor &&
    !latestManuscript.specialIssueId
  )
    return

  await latestManuscript.$query(trx).patch({
    hasTriageEditorConflictOfInterest: true,
  })
}
