import models from '@pubsweet/models'
import { TaskType } from '../ScheduledTasksQueue'
import { TaskPayloadWithTrx, ReturnToPRReasons } from '../queue.utils'

export const notifyTEOfOwnConflict = async ({
  submissionId,
  scheduledTasksQueue,
  submittedReasonsToReturnToPR,
}: TaskPayloadWithTrx) => {
  const { TeamMember, Team, Journal, Manuscript, SpecialIssue } = models

  const latestManuscript = await Manuscript.findLastManuscriptBySubmissionId({
    submissionId,
  })
  const triageEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus({
    manuscriptId: latestManuscript.id,
    role: Team.Role.triageEditor,
    status: TeamMember.Statuses.active,
    eagerLoadRelations: 'user',
  })
  const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus({
    manuscriptId: latestManuscript.id,
    role: Team.Role.academicEditor,
    status: TeamMember.Statuses.accepted,
    eagerLoadRelations: 'user',
  })
  const triageEditorIsAlsoAcademicEditor =
    triageEditor.userId === academicEditor.userId
  const aeConflictAvailable = submittedReasonsToReturnToPR.some(
    reason => reason.type === ReturnToPRReasons.academicEditor,
  )

  // if the LGE assigns himself as GE and there is also an AE conflict, don't send the email because
  // we need to send only the email where we notify the GE about his own conflict
  if (triageEditorIsAlsoAcademicEditor && aeConflictAvailable) return

  const specialIssue = latestManuscript.specialIssueId
    ? await SpecialIssue.find(latestManuscript.specialIssueId)
    : undefined
  const journal = await Journal.find(latestManuscript.journalId)
  const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
    {
      manuscriptId: latestManuscript.id,
      role: Team.Role.editorialAssistant,
      status: TeamMember.Statuses.active,
    },
  )

  scheduledTasksQueue.push([
    {
      type: TaskType.notification,
      svMethod: 'notifyTriageEditorAboutHisConflictOfInterest',
      payload: {
        journal,
        specialIssue,
        manuscript: latestManuscript,
        triageEditor,
        editorialAssistant,
      },
    },
  ])
}
