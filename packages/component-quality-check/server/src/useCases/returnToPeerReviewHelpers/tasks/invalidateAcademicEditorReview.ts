import models from '@pubsweet/models'
import invalidateReviewUseCase from '../../conflictOfInterest/invalidateReview'
import { TaskPayloadWithTrx } from '../queue.utils'
import { TaskType } from '../ScheduledTasksQueue'

export const invalidateAcademicEditorReview = async ({
  submissionId,
  trx,
  scheduledTasksQueue,
}: TaskPayloadWithTrx) => {
  const { Team, TeamMember, Manuscript } = models

  const latestManuscript = await Manuscript.findLastManuscriptBySubmissionId({
    submissionId,
  })

  const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus({
    manuscriptId: latestManuscript.id,
    role: Team.Role.academicEditor,
    status: TeamMember.Statuses.accepted,
  })

  await invalidateReviewUseCase.initialize({ models }).execute({
    manuscriptId: latestManuscript.id,
    teamMemberId: academicEditor.id,
    trx,
  })

  scheduledTasksQueue.push([
    {
      type: TaskType.event,
      svMethod: 'publishSubmissionEvent',
      payload: {
        submissionId,
        eventName: 'SubmissionReviewInvalidated',
      },
    },
  ])
}
