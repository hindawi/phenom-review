export const findValidReviews = async ({
  improperReviewAndCOIReviewers,
  submissionId,
  models,
}) => {
  const { Review, Team } = models

  const latestMajorEditorialReview = await Review.findLatestEditorialReviewOfSubmissionByRecommendationType(
    {
      submissionId,
      TeamRole: Team.Role,
      recommendationType: Review.Recommendations.major,
    },
  )

  let reviews = await Review.findAllValidAndSubmitedBySubmissionAndRole({
    submissionId,
    role: Team.Role.reviewer,
  })
  if (
    latestMajorEditorialReview &&
    latestMajorEditorialReview.recommendation === Review.Recommendations.major
  ) {
    reviews = reviews.filter(
      review =>
        new Date(review.submitted) >
        new Date(latestMajorEditorialReview.submitted),
    )
  }

  // filter valid reviews that are submitted by reviewers without COI or Improper Review
  return reviews.filter(review =>
    improperReviewAndCOIReviewers.every(
      reviewer => reviewer.id !== review.teamMemberId,
    ),
  )
}
