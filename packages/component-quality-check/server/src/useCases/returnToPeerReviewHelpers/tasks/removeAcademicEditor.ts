import models from '@pubsweet/models'
import removeSubmissionUserUseCase from '../../conflictOfInterest/removeSubmissionUser'
import { TaskPayloadWithTrx } from '../queue.utils'
import { TaskType } from '../ScheduledTasksQueue'

export const removeAcademicEditor = async ({
  submissionId,
  trx,
  scheduledTasksQueue,
}: TaskPayloadWithTrx) => {
  const { Team, TeamMember, Manuscript } = models

  const latestManuscript = await Manuscript.findLastManuscriptBySubmissionId({
    submissionId,
  })

  const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus({
    manuscriptId: latestManuscript.id,
    role: Team.Role.academicEditor,
    status: TeamMember.Statuses.accepted,
  })

  await removeSubmissionUserUseCase.initialize({ models }).execute({
    submissionId,
    userId: academicEditor.userId,
    role: Team.Role.academicEditor,
    trx,
  })

  scheduledTasksQueue.push([
    {
      type: TaskType.event,
      svMethod: 'publishSubmissionEvent',
      payload: {
        submissionId,
        eventName: 'SubmissionAcademicEditorRemoved',
      },
    },
  ])
}
