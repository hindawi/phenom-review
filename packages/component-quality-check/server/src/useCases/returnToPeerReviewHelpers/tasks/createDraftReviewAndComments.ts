import models from '@pubsweet/models'
import BluebirdPromise from 'bluebird'
import { TaskPayloadWithTrx } from '../queue.utils'

export const createDraftReviewAndComments = async ({
  conflictedTeamMembers,
  submissionId,
  trx,
}: TaskPayloadWithTrx) => {
  const { Review, Manuscript, Comment } = models

  const latestManuscript = await Manuscript.findLastManuscriptBySubmissionId({
    submissionId,
  })

  await BluebirdPromise.each(conflictedTeamMembers, async teamMember => {
    const draftReview = new Review({
      teamMemberId: teamMember.id,
      manuscriptId: latestManuscript.id,
    })
    const trxReview = await draftReview.$query(trx).insertAndFetch(draftReview)

    const publicComment = new Comment({
      reviewId: trxReview.id,
      type: Comment.Types.public,
    })

    const privateComment = new Comment({
      reviewId: trxReview.id,
      type: Comment.Types.private,
    })

    await publicComment.$query(trx).insert(publicComment)
    await privateComment.$query(trx).insert(privateComment)
  })
}
