// @ts-nocheck
// ^ remove this after merging ts models
import models from '@pubsweet/models'
import { ReturnToPRReasons, TaskPayloadWithTrx } from '../queue.utils'
import { TaskType } from '../ScheduledTasksQueue'

export const invalidateDecision = async ({
  submissionId,
  trx,
  reason,
  scheduledTasksQueue,
}: TaskPayloadWithTrx) => {
  const {
    Manuscript,
    Review,
    Team,
    TeamMember,
    User,
    Journal,
    SpecialIssue,
    PeerReviewEditorialMapping,
  } = models

  const manuscript = await Manuscript.findLastManuscriptBySubmissionId({
    submissionId,
    eagerLoadRelations: 'peerReviewEditorialMapping',
  })

  const currentDecision = await Review.findLatestEditorialReview({
    manuscriptId: manuscript.id,
    TeamRole: Team.Role,
  })

  await currentDecision.$query(trx).patch({ isValid: false })

  const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
    {
      manuscriptId: manuscript.id,
      role: Team.Role.editorialAssistant,
      status: TeamMember.Statuses.active,
      eagerLoadRelations: 'user',
    },
  )

  const author = await TeamMember.findSubmittingAuthor(manuscript.id)
  author.user = await User.find(author.userId)

  const approvalEditorRole = await PeerReviewEditorialMapping.getApprovalEditorRoleForSubmission(
    submissionId,
  )

  const activeStatus = TeamMember.getActiveStatusForRole(approvalEditorRole)

  const approvalEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus({
    status: activeStatus,
    role: approvalEditorRole,
    manuscriptId: manuscript.id,
    eagerLoadRelations: 'user',
  })

  const journal = await Journal.find(manuscript.journalId)

  const specialIssue = manuscript.specialIssueId
    ? await SpecialIssue.find(manuscript.specialIssueId)
    : undefined

  if (reason === ReturnToPRReasons.decisionMadeInError) {
    scheduledTasksQueue.push([
      {
        type: TaskType.notification,
        svMethod: 'notifyApprovalEditorAboutTheDecisonMadeInError',
        payload: {
          journal,
          editorialAssistant,
          approvalEditor,
          manuscript,
          author,
        },
      },
    ])
  }

  scheduledTasksQueue.push([
    {
      type: TaskType.notification,
      svMethod: 'notifyAuthorOfManuscriptReturnToPeerReview',
      payload: {
        author,
        editorialAssistant,
        manuscript,
        specialIssue,
        journal,
      },
    },
    {
      type: TaskType.notification,
      svMethod: 'notifyEditorialAssistantOfManuscriptReturnToPeerReview',
      payload: {
        editorialAssistant,
        manuscript,
        journal,
      },
    },
  ])
}
