import models from '@pubsweet/models'
import { autoInviteAE } from '../autoInviteAE/autoInviteAEWithTrx'

import { ReturnToPRReasons, TaskPayloadWithTrx } from '../queue.utils'

export const triggerInviteAE = async ({
  submissionId,
  reason,
  scheduledTasksQueue,
  trx,
}: TaskPayloadWithTrx) => {
  const { Manuscript, PeerReviewModel } = models

  const latestManuscript = await Manuscript.findLastManuscriptBySubmissionId({
    submissionId,
  })

  const prm = await PeerReviewModel.findOneByManuscriptParent(latestManuscript)

  const isSingleTierOrSpecialIssue = [
    PeerReviewModel.Types.singleTierAcademicEditor,
    PeerReviewModel.Types.specialIssueAssociateEditor,
  ].includes(prm.name)

  if (
    // if the COI is with an AE ( which means TE is good ), we don't need to auto-invite another AE
    // If the manuscript is on a single tier journal or on a SI we don't need to auto-invite another AE
    // and there might be a possibility of COI again

    // TODO (15.7.2021): we need unit tests for this logic
    reason === ReturnToPRReasons.academicEditor &&
    !isSingleTierOrSpecialIssue
  ) {
    return
  }

  // we only get to auto invite AE if the conditions above are not met
  await autoInviteAE({
    trx,
    scheduledTasksQueue,
    manuscript: latestManuscript,
  })
  // TODO: ask Raresh what's up with this case. Where mail bro? x2 (AE + TE)
  // send mail to AE that was removed
  // send mail to TE to notify that an AE was automatically invited
  // scheduledTasksQueue.push([
  //   {
  //     type: TaskType.notification,
  //     svMethod: '',
  //     payload: {},
  //   },
  // ])
}
