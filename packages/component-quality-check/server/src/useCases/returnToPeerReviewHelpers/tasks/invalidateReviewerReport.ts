// @ts-nocheck
import * as Bluebird from 'bluebird'
import { logger } from 'component-logger'
import models from '@pubsweet/models'
import { TaskType } from '../ScheduledTasksQueue'
import { TaskPayloadWithTrx } from '../queue.utils'

// when we have conflicts with reviewers or improper reviews, we need to invalidate the existing reviewer reports ( isValid: false )
// we need to notify ae about the conflict found with the reviewer (this applies only if the AE is not removed), and send an event about the review being invalidated
const invalidateReviewerReport = async ({
  submissionId,
  conflictedTeamMembers,
  trx,
  scheduledTasksQueue,
}: TaskPayloadWithTrx) => {
  const { Review } = models

  await Bluebird.each(conflictedTeamMembers, async teamMember => {
    const review = await Review.findOneValidByTeamMember(teamMember.id)

    if (!review) {
      return logger.info(
        `Reviewer ${teamMember.id} does not have a matching review!`,
      )
    }

    await review.$query(trx).patch({ isValid: false })
  })

  scheduledTasksQueue.push([
    {
      type: TaskType.event,
      svMethod: 'publishSubmissionEvent',
      payload: {
        submissionId,
        eventName: 'SubmissionReviewInvalidated',
      },
    },
  ])
}

export const invalidateReviewerReportOnConflicts = invalidateReviewerReport
export const invalidateReviewerReportOnImproperReview = invalidateReviewerReport
