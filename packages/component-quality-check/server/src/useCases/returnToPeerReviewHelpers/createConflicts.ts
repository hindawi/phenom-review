import models from '@pubsweet/models'
import { ConflictOfInterest } from '../../services/ConflictOfInterest'

const { TeamMember, Team } = models

const getAuthorId = async submissionId => {
  const { id } = await TeamMember.findAllBySubmissionAndRole({
    submissionId,
    role: Team.Role.author,
  })
    .limit(1)
    .first()

  return id
}

export const createConflictOfInterests = async (
  conflicts,
  submissionId,
  trx,
) => {
  const authorId = await getAuthorId(submissionId)

  const conflictsToCreate = []

  conflicts.forEach(coi => {
    coi.conflictedTeamMembers.forEach(({ id }) => {
      conflictsToCreate.push(
        ConflictOfInterest.createConflict(
          {
            firstConflictingTeamMemberId: id,
            secondConflictingTeamMemberId: authorId,
          },
          trx,
        ),
      )
    })
  })

  await Promise.all(conflictsToCreate)
}
