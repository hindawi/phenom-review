import Knex from 'knex'
import { ScheduledTasksQueueI } from './ScheduledTasksQueue'

export enum ReturnToPRReasons {
  triageEditor = 'triageEditor',
  academicEditor = 'academicEditor',
  reviewers = 'reviewers',
  improperReview = 'improperReview',
  decisionMadeInError = 'decisionMadeInError',
}

export enum Tasks {
  invalidateDecision = 'invalidateDecision',
  invalidateTriageEditor = 'invalidateTriageEditor',
  removeAcademicEditor = 'removeAcademicEditor',
  invalidateAcademicEditorReview = 'invalidateAcademicEditorReview',
  invalidateReviewerReportOnConflicts = 'invalidateReviewerReportOnConflicts',
  invalidateReviewerReportOnImproperReview = 'invalidateReviewerReportOnImproperReview',
  removeReviewers = 'removeReviewers',
  setReviewerStatusToAccepted = 'setReviewerStatusToAccepted',
  createDraftReviewAndComments = 'createDraftReviewAndComments',
  triggerInviteAE = 'triggerInviteAE',
  updateManuscriptStatus = 'updateManuscriptStatus',
  scheduleActivityLog = 'scheduleActivityLog',
  notifyTEOfOwnConflict = 'notifyTEOfOwnConflict',
  notifyAEOfOwnConflict = 'notifyAEOfOwnConflict',
  notifyTEOfAEConflict = 'notifyTEOfAEConflict',
}

export type IDCollection = { id: string }[]

export type TaskPayload = {
  submissionId?: string
  conflictedTeamMembers?: IDCollection
  isAERemoved?: boolean
  reason: ReturnToPRReasons
  improperReviewAndCOIReviewers?: IDCollection
  submittedReasonsToReturnToPR?: Reason[]
}

export type Transaction = Knex.Transaction

export type TaskPayloadWithTrx = TaskPayload & {
  trx: Transaction
  scheduledTasksQueue: ScheduledTasksQueueI
}

export interface TaskI {
  fn: (payload: TaskPayloadWithTrx) => Promise<void>
  name: string
  payload: TaskPayload
}

export type Queue = TaskI[]

export interface Reason {
  type: ReturnToPRReasons
  conflictedTeamMembers?: IDCollection
}

export type COI = {
  type: string
  conflictedTeamMembers: IDCollection
}

export type CreateTaskQueuePayload = {
  submissionId: string
}
export type ImproperReview = {
  reviewers: IDCollection
}
export type CreateTaskPayloadFn = (args: {
  submissionId: string
  reason: Reason
  submittedReasonsToReturnToPR: Reason[]
}) => TaskPayload

export const returnToPRSteps = {
  [ReturnToPRReasons.triageEditor]: [
    Tasks.invalidateDecision,
    Tasks.invalidateTriageEditor,
    Tasks.removeAcademicEditor,
    Tasks.invalidateAcademicEditorReview,
    Tasks.triggerInviteAE,
    Tasks.updateManuscriptStatus,
    Tasks.notifyTEOfOwnConflict,
    Tasks.scheduleActivityLog,
  ],
  [ReturnToPRReasons.academicEditor]: [
    Tasks.invalidateDecision,
    Tasks.invalidateTriageEditor,
    Tasks.removeAcademicEditor,
    Tasks.invalidateAcademicEditorReview,
    Tasks.updateManuscriptStatus,
    Tasks.triggerInviteAE,
    Tasks.notifyAEOfOwnConflict,
    Tasks.notifyTEOfAEConflict,
    Tasks.scheduleActivityLog,
  ],
  [ReturnToPRReasons.reviewers]: [
    Tasks.invalidateDecision,
    Tasks.invalidateReviewerReportOnConflicts,
    Tasks.removeReviewers,
    Tasks.invalidateAcademicEditorReview,
    Tasks.updateManuscriptStatus,
    Tasks.scheduleActivityLog,
  ],
  [ReturnToPRReasons.improperReview]: [
    Tasks.invalidateDecision,
    Tasks.invalidateAcademicEditorReview,
    Tasks.invalidateReviewerReportOnImproperReview,
    Tasks.setReviewerStatusToAccepted,
    Tasks.createDraftReviewAndComments,
    Tasks.updateManuscriptStatus,
    Tasks.scheduleActivityLog,
  ],
  [ReturnToPRReasons.decisionMadeInError]: [
    Tasks.invalidateDecision,
    Tasks.updateManuscriptStatus,
    Tasks.scheduleActivityLog,
  ],
}
