import * as Bluebird from 'bluebird'
import * as tasksMap from './tasks'
import {
  ReturnToPRReasons,
  returnToPRSteps,
  Reason,
  Queue,
  TaskI,
  Tasks,
  TaskPayload,
  CreateTaskQueuePayload,
  CreateTaskPayloadFn,
} from './queue.utils'

export const updateQueueTasks = (
  taskQueue: Queue,
  steps: string[],
  payload: TaskPayload,
): Queue => {
  const tasksToAdd: TaskI[] = []

  const isAlreadyInQueue = (step: string) =>
    taskQueue.some(task => task.name === step)

  steps.forEach(step => {
    if (!isAlreadyInQueue(step)) {
      tasksToAdd.push({
        fn: tasksMap[step],
        name: step,
        payload,
      })
    }
  })
  return [...taskQueue, ...tasksToAdd]
}

const getReviewersFromEvent = submittedReasonsToReturnToPR => {
  const improperReviewReason = submittedReasonsToReturnToPR.find(
    reason => reason.type === ReturnToPRReasons.improperReview,
  )
  const conflictingReviewersReason = submittedReasonsToReturnToPR.find(
    reason => reason.type === ReturnToPRReasons.reviewers,
  )

  const improperReviewReviewers =
    (improperReviewReason && improperReviewReason.conflictedTeamMembers) || []
  const conflictingReviewers =
    (conflictingReviewersReason &&
      conflictingReviewersReason.conflictedTeamMembers) ||
    []

  return [...improperReviewReviewers, ...conflictingReviewers]
}

const createTaskPayload: CreateTaskPayloadFn = ({
  submittedReasonsToReturnToPR,
  submissionId,
  reason,
}) => {
  const taskPayload: TaskPayload = {
    submissionId,
    reason: reason.type,
    submittedReasonsToReturnToPR,
  }

  taskPayload.isAERemoved = submittedReasonsToReturnToPR.some(reason =>
    [ReturnToPRReasons.triageEditor, ReturnToPRReasons.academicEditor].includes(
      reason.type,
    ),
  )
  const improperReviewAndCOIReviewers = getReviewersFromEvent(
    submittedReasonsToReturnToPR,
  )
  if (
    [
      ReturnToPRReasons.academicEditor,
      ReturnToPRReasons.reviewers,
      ReturnToPRReasons.improperReview,
    ].includes(reason.type)
  ) {
    taskPayload.conflictedTeamMembers = reason.conflictedTeamMembers
    taskPayload.improperReviewAndCOIReviewers = improperReviewAndCOIReviewers
  }

  return taskPayload
}
const moveTaskToTheEndOfQueue = (taskQueue, task) =>
  taskQueue.filter(t => t !== task).concat(taskQueue.filter(t => t === task))

export const createTaskQueue = (
  submittedReasonsToReturnToPR: Reason[],
  payload: CreateTaskQueuePayload,
): Queue => {
  let taskQueue: Queue = []
  const { submissionId } = payload

  submittedReasonsToReturnToPR.forEach((reason: Reason) => {
    const taskPayload: TaskPayload = createTaskPayload({
      submittedReasonsToReturnToPR,
      submissionId,
      reason,
    })

    if (returnToPRSteps[reason.type] === undefined) {
      throw new Error(`Unhandled reason to return to review: ${reason.type}`)
    }

    taskQueue = updateQueueTasks(
      taskQueue,
      returnToPRSteps[reason.type],
      taskPayload,
    )
  })

  // move updateManuscriptStatus task to the end of the array in order to be the last one executed
  const updateManuscriptStatusTask = taskQueue.find(
    task => task.name === Tasks.updateManuscriptStatus,
  )
  return moveTaskToTheEndOfQueue(taskQueue, updateManuscriptStatusTask)
}

export const runTaskQueue = async (
  taskQueue: Queue,
  scheduledTasksQueue,
  trx,
) => {
  await Bluebird.each(taskQueue, async task =>
    task.fn({ ...task.payload, trx, scheduledTasksQueue }),
  )
}
