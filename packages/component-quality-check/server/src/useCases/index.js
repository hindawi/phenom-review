const rejectQualityCheckUseCase = require('./rejectQualityCheck')
const conflictOfInterestUseCases = require('./conflictOfInterest')
const submitQualityChecksUseCase = require('./submitQualityChecks')
const requestQualityChecksUseCase = require('./requestQualityChecks')
const peerReviewCycleCheckPassedUseCase = require('./peerReviewCheckPassed')
const updateQualityChecksDraftUseCase = require('./updateQualityChecksDraft')
const submissionQualityCheckPassedUseCase = require('./submissionQualityCheckPassed')
const autoInviteAEUseCase = require('./autoInviteAE')
const replaceEditorForMultipleTierPRMUseCase = require('./replaceEditorForMultipleTierPRM')
const returnToPeerReview = require('./returnToPeerReview')

module.exports = {
  returnToPeerReview,
  rejectQualityCheckUseCase,
  submitQualityChecksUseCase,
  requestQualityChecksUseCase,
  ...conflictOfInterestUseCases,
  updateQualityChecksDraftUseCase,
  peerReviewCycleCheckPassedUseCase,
  submissionQualityCheckPassedUseCase,
  autoInviteAEUseCase,
  replaceEditorForMultipleTierPRMUseCase,
}
