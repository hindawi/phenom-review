/* eslint-disable sonarjs/cognitive-complexity */
const { logger } = require('component-logger')
const Promise = require('bluebird')

const initialize = ({
  models,
  logEvent,
  transaction,
  eventsService,
  notificationService,
}) => ({
  async execute({ submissionId, reviewerIds }) {
    const { Journal, Manuscript, Team, TeamMember, Review } = models

    // Update manuscript status
    const manuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
    })
    if (!manuscript)
      throw new Error(
        `No manuscript was found for submission id ${submissionId}`,
      )

    const reviewsToUpdate = []
    const reviewersToUpdate = []
    await Promise.each(reviewerIds, async reviewerId => {
      // Update reviewer status
      const reviewer = await TeamMember.find(reviewerId)
      if (!reviewer) throw new Error(`No reviewer found with id ${reviewerId}`)
      reviewer.status = TeamMember.Statuses.removed
      reviewersToUpdate.push(reviewer)

      // Update reviewer review
      const review = await Review.findOneByManuscriptAndTeamMember({
        manuscriptId: manuscript.id,
        teamMemberId: reviewerId,
      })
      if (!review) {
        logger.info(`Reviewer ${reviewerId} does not have a matching review!`)
        return
      }
      review.isValid = false
      reviewsToUpdate.push(review)
    })

    let reviewerReviews = await Review.findAllValidAndSubmittedByManuscriptAndRole(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.reviewer,
      },
    )
    reviewerReviews = reviewerReviews.filter(r => !reviewerIds.includes(r.id))

    if (reviewerReviews.length > 1)
      manuscript.status = Manuscript.Statuses.reviewCompleted
    else {
      let acceptedReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
        {
          role: Team.Role.reviewer,
          manuscriptId: manuscript.id,
          status: TeamMember.Statuses.accepted,
        },
      )
      acceptedReviewers = acceptedReviewers.filter(
        r => !reviewerIds.includes(r.id),
      )

      if (acceptedReviewers.length > 0) {
        manuscript.status = Manuscript.Statuses.underReview
      } else {
        manuscript.status = Manuscript.Statuses.academicEditorAssigned
      }
    }

    // Update academic editor review
    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
        eagerLoadRelations: 'user',
      },
    )
    let editorReview
    if (!academicEditor) {
      logger.info('Manuscript does not have an accepted academic editor.')
    } else {
      editorReview = await Review.findOneByManuscriptAndTeamMember({
        manuscriptId: manuscript.id,
        teamMemberId: academicEditor.id,
      })
      if (!editorReview) {
        logger.info('Could not find an academic editor review!')
      } else {
        editorReview.isValid = false
      }
    }

    // Transaction
    try {
      await transaction(Manuscript.knex(), async trx => {
        await manuscript.$query(trx).update(manuscript)
        await Promise.each(reviewersToUpdate, async reviewer => {
          await reviewer.$query(trx).update(reviewer)
        })
        await Promise.each(reviewsToUpdate, async review => {
          await review.$query(trx).update(review)
        })
        if (editorReview) await editorReview.$query(trx).update(editorReview)
      })
    } catch (err) {
      logger.error('Something went wrong. No data was updated.', err)
      throw new Error('Something went wrong. No data was updated.')
    }

    // Notifications
    const { name: journalName } = await Journal.find(manuscript.journalId)
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )

    notificationService.notifyAcademicEditor({
      journalName,
      editorialAssistant,
      academicEditor,
      manuscript,
      reviewers: reviewersToUpdate,
    })

    // Events
    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionReviewerRemoved',
    })
    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionReviewInvalidated',
    })

    // Log event
    logEvent({
      userId: null, // System
      manuscriptId: manuscript.id,
      action: logEvent.actions.quality_checks_coi_author_reviewer,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

module.exports = {
  initialize,
}
