const { logger } = require('component-logger')

const initialize = ({
  models,
  logEvent,
  useCases,
  transaction,
  eventsService,
  notificationService,
  getSuggestedAcademicEditorUseCase,
  inviteSuggestedAcademicEditorUseCase,
  inviteAcademicEditorUseCase,
  initializeInviteAcademicEditorUseCase,
}) => ({
  async execute({ submissionId, academicEditorId }) {
    const { Manuscript, TeamMember, Team, PeerReviewModel } = models

    const manuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
    })

    const peerReviewModel = await PeerReviewModel.findOneByManuscriptParent(
      manuscript,
    )
    manuscript.status = peerReviewModel.approvalEditors.includes(
      Team.Role.academicEditor,
    )
      ? Manuscript.Statuses.makeDecision
      : Manuscript.Statuses.reviewCompleted

    const academicEditor = await TeamMember.find(academicEditorId)

    const trx = await transaction.start(Manuscript.knex())
    try {
      await manuscript.$query(trx).update(manuscript)

      await useCases.removeSubmissionUserUseCase
        .initialize({ models })
        .execute({
          submissionId,
          role: Team.Role.academicEditor,
          userId: academicEditor.userId,
          trx,
        })

      await useCases.invalidateReviewUseCase.initialize({ models }).execute({
        manuscriptId: manuscript.id,
        teamMemberId: academicEditorId,
        trx,
      })

      await trx.commit()
    } catch (err) {
      logger.error('Something went wrong. No data was updated.', err)
      await trx.rollback(err)
      throw new Error('Something went wrong. No data was updated.')
    }

    let useCase = 'autoInviteAEUseCase'
    if (peerReviewModel.hasTriageEditor)
      useCase = 'replaceEditorForMultipleTierPRMUseCase'
    await useCases[useCase]
      .initialize({
        models,
        notificationService,
        getSuggestedAcademicEditorUseCase,
        inviteSuggestedAcademicEditorUseCase,
        inviteAcademicEditorUseCase,
        initializeInviteAcademicEditorUseCase,
      })
      .execute({ manuscriptId: manuscript.id, academicEditorId })

    eventsService.publishSubmissionEvent({
      submissionId,
      eventName: 'SubmissionAcademicEditorRemoved',
    })
    eventsService.publishSubmissionEvent({
      submissionId,
      eventName: 'SubmissionReviewInvalidated',
    })

    logEvent({
      userId: null,
      manuscriptId: manuscript.id,
      action: logEvent.actions.quality_checks_coi_author_academic_editor,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

module.exports = {
  initialize,
}
