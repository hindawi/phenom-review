const invalidateReviewUseCase = require('./invalidateReview')
const removeSubmissionUserUseCase = require('./removeSubmissionUser')
const handleCOIBetweenAuthorAndReviewersUseCase = require('./authorAndReviewers')
const handleCOIBetweenAuthorAndTriageEditorUseCase = require('./authorAndTriageEditor')
const handleCOIBetweenAuthorAndAcademicEditorUseCase = require('./authorAndAcademicEditor')

module.exports = {
  invalidateReviewUseCase,
  removeSubmissionUserUseCase,
  handleCOIBetweenAuthorAndReviewersUseCase,
  handleCOIBetweenAuthorAndTriageEditorUseCase,
  handleCOIBetweenAuthorAndAcademicEditorUseCase,
}
