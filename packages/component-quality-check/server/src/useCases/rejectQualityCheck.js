const initialize = ({
  models: { Manuscript, Journal, Team, TeamMember, User },
  logEvent,
  notificationService,
}) => ({
  async execute({ data }) {
    const { submissionId } = data
    const lastManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
    })

    lastManuscript.updateStatus(Manuscript.Statuses.refusedToConsider)
    await lastManuscript.save()

    const journal = await Journal.find(lastManuscript.journalId)
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: lastManuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    const author = await TeamMember.findSubmittingAuthor(lastManuscript.id)
    author.user = await User.find(author.userId)

    notificationService.notifyAuthor({
      author,
      editorialAssistant,
      journalName: journal.name,
      manuscript: lastManuscript,
    })

    logEvent({
      userId: null,
      objectId: lastManuscript.id,
      manuscriptId: lastManuscript.id,
      objectType: logEvent.objectType.manuscript,
      action: logEvent.actions.manuscript_refused_to_consider,
    })
  },
})

module.exports = {
  initialize,
}
