const models = require('@pubsweet/models')
const { logEvent } = require('component-activity-log/server')
const { useCases: mtsProductionUseCases } = require('component-mts-production')
const events = require('component-events')
const { transaction } = require('objection')

const { s3service } = require('component-files/server')

const {
  useCases: {
    createMinorVersion,
    createMajorVersion,
    copyFilesBetweenManuscripts,
    copyAuthorsBetweenManuscripts,
    copyEAsBetweenManuscripts,
    copyTEsBetweenManuscripts,
    copyAEsBetweenManuscripts,
    copySubmittingStaffMemberBetweenManuscripts,
  },
} = require('component-model')

const useCases = require('./useCases')
const {
  useCases: { getSuggestedAcademicEditorUseCase },
} = require('component-model')

const {
  useCases: {
    inviteSuggestedAcademicEditorUseCase,
    inviteAcademicEditorUseCase,
    initializeInviteAcademicEditorUseCase,
  },
} = require('component-peer-review')
const notificationService = require('./notifications/notification')

const COI_HANDLERS = {
  reviewers: 'handleCOIBetweenAuthorAndReviewersUseCase',
  triageEditor: 'handleCOIBetweenAuthorAndTriageEditorUseCase',
  academicEditor: 'handleCOIBetweenAuthorAndAcademicEditorUseCase',
}

const eventIsOfNewerType = data =>
  !!data.checks.peerReviewCycle.returnToPeerReviewDetails

const handleMultipleCOITypeEvent = data => {
  const {
    submissionId,
    checks: {
      peerReviewCycle: {
        returnToPeerReviewDetails: {
          reasons: { conflictOfInterest, decisionMadeInError, improperReview },
        },
      },
    },
  } = data

  return useCases.returnToPeerReview.initialize().execute({
    conflictOfInterest,
    submissionId,
    decisionMadeInError,
    improperReview,
  })
}

const handleSingleCOITypeEvent = data => {
  const eventsService = events.initialize({ models })
  const {
    submissionId,
    checks: {
      peerReviewCycle: {
        conflictOfInterest: {
          type,
          triageEditor = {},
          academicEditor = {},
          reviewers = [],
        },
      },
    },
  } = data

  const useCase = useCases[COI_HANDLERS[type]]
  if (!useCase) return

  return useCase
    .initialize({
      models,
      logEvent,
      useCases,
      transaction,
      eventsService,
      notificationService,
      getSuggestedAcademicEditorUseCase,
      inviteSuggestedAcademicEditorUseCase,
      inviteAcademicEditorUseCase,
      initializeInviteAcademicEditorUseCase,
    })
    .execute({
      submissionId,
      triageEditorId: triageEditor.id,
      academicEditorId: academicEditor.id,
      reviewerIds: reviewers.map(r => r.id),
    })
}

module.exports = {
  async SubmissionQualityCheckFilesRequested(data) {
    const useCasesToPass = {
      createMajorVersion,
      createMinorVersion,
      copyFilesBetweenManuscripts,
      copyAuthorsBetweenManuscripts,
      copyEAsBetweenManuscripts,
      copyTEsBetweenManuscripts,
      copyAEsBetweenManuscripts,
      copySubmittingStaffMemberBetweenManuscripts,
    }
    const services = { s3service }
    return useCases.requestQualityChecksUseCase
      .initialize({ models, useCases: useCasesToPass, services, logEvent })
      .execute(data)
  },
  async SubmissionPeerReviewCycleCheckPassed(data) {
    return useCases.peerReviewCycleCheckPassedUseCase
      .initialize({ models, notificationService })
      .execute({ data })
  },
  async SubmissionQualityCheckRTCd(data) {
    return useCases.rejectQualityCheckUseCase
      .initialize({ models, logEvent, notificationService })
      .execute({ data })
  },
  async PeerReviewCycleCheckingProcessSentToPeerReview(data) {
    if (eventIsOfNewerType(data)) {
      return handleMultipleCOITypeEvent(data)
    }

    return handleSingleCOITypeEvent(data)
  },
  async SubmissionQualityCheckPassed(data) {
    await useCases.submissionQualityCheckPassedUseCase
      .initialize({ models, notificationService })
      .execute(data)

    await mtsProductionUseCases.handleQualityCheckedSubmissionUseCase
      .initialize({
        models,
        applicationEventBus,
        useCases: mtsProductionUseCases,
      })
      .execute(data)
  },
}
