const {
  getTeamRoles,
  generateManuscript,
  generateTeamMember,
  getManuscriptStatuses,
  generatePeerReviewModel,
} = require('component-generators')

const {
  handleCOIBetweenAuthorAndAcademicEditorUseCase,
} = require('../src/useCases')

const useCases = {
  removeSubmissionUserUseCase: {
    initialize: jest.fn(() => ({ execute: jest.fn() })),
  },
  invalidateReviewUseCase: {
    initialize: jest.fn(() => ({ execute: jest.fn() })),
  },
  autoInviteAEUseCase: {
    initialize: jest.fn(() => ({ execute: jest.fn() })),
  },
  replaceEditorForMultipleTierPRMUseCase: {
    initialize: jest.fn(() => ({ execute: jest.fn() })),
  },
}

let models = {}
let Manuscript = {}
let Team = {}
let TeamMember = {}
let PeerReviewModel = {}

const notificationService = {}
const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

const logEvent = jest.fn(async () => {})
logEvent.actions = {
  quality_checks_coi_author_academic_editor:
    'quality_checks_coi_author_academic_editor',
}
logEvent.objectType = { manuscript: 'manuscript' }

const trx = {
  commit: jest.fn(),
  rollback: jest.fn(),
}

const transaction = {
  start: jest.fn(() => trx),
}

describe('Handle COI between author and academicEditor use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()

    models = {
      Manuscript: {
        findLastManuscriptBySubmissionId: jest.fn(),
        Statuses: getManuscriptStatuses(),
        knex: jest.fn(),
      },
      TeamMember: {
        find: jest.fn(),
      },
      Team: {
        Role: getTeamRoles(),
      },
      PeerReviewModel: {
        findOneByManuscriptParent: jest.fn(),
      },
    }
    ;({ Manuscript, Team, TeamMember, PeerReviewModel } = models)
  })

  it('Replaces editors on single tier PRM', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.inQA,
      $query: jest.fn(() => ({ update: jest.fn() })),
    })
    const academicEditor = generateTeamMember({
      team: { role: Team.Role.academicEditor },
    })
    const peerReviewModel = generatePeerReviewModel({
      hasTriageEditor: false,
      approvalEditors: [Team.Role.academicEditor],
    })

    jest
      .spyOn(Manuscript, 'findLastManuscriptBySubmissionId')
      .mockResolvedValue(manuscript)
    jest
      .spyOn(PeerReviewModel, 'findOneByManuscriptParent')
      .mockResolvedValueOnce(peerReviewModel)
    jest.spyOn(TeamMember, 'find').mockResolvedValue(academicEditor)

    await handleCOIBetweenAuthorAndAcademicEditorUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        transaction,
        eventsService,
        notificationService,
      })
      .execute({
        submissionId: manuscript.submissionId,
        academicEditorId: academicEditor.id,
      })

    expect(trx.commit).toHaveBeenCalledTimes(1)
    expect(useCases.autoInviteAEUseCase.initialize).toHaveBeenCalledTimes(1)
    expect(logEvent).toHaveBeenCalledTimes(1)
  })
  it('Replaces editors on multiple tier PRM', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.inQA,
      $query: jest.fn(() => ({ update: jest.fn() })),
    })
    const academicEditor = generateTeamMember({
      team: { role: Team.Role.academicEditor },
    })
    const peerReviewModel = generatePeerReviewModel({
      hasTriageEditor: true,
      approvalEditors: [Team.Role.triageEditor],
    })

    jest
      .spyOn(Manuscript, 'findLastManuscriptBySubmissionId')
      .mockResolvedValue(manuscript)
    jest
      .spyOn(PeerReviewModel, 'findOneByManuscriptParent')
      .mockResolvedValueOnce(peerReviewModel)
    jest.spyOn(TeamMember, 'find').mockResolvedValue(academicEditor)

    await handleCOIBetweenAuthorAndAcademicEditorUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        transaction,
        eventsService,
        notificationService,
      })
      .execute({
        submissionId: manuscript.submissionId,
        academicEditorId: academicEditor.id,
      })

    expect(trx.commit).toHaveBeenCalledTimes(1)
    expect(
      useCases.replaceEditorForMultipleTierPRMUseCase.initialize,
    ).toHaveBeenCalledTimes(1)
    expect(logEvent).toHaveBeenCalledTimes(1)
  })
})
