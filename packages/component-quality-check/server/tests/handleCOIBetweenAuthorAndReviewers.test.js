const {
  getTeamRoles,
  generateReview,
  generateManuscript,
  generateTeamMember,
  getManuscriptStatuses,
  getTeamMemberStatuses,
} = require('component-generators')

const {
  initialize,
} = require('../src/useCases/conflictOfInterest/authorAndReviewers')

let models = {}
let Journal = {}
let Manuscript = {}
let Team = {}
let TeamMember = {}
let Review = {}

const notificationService = {
  notifyAcademicEditor: jest.fn(),
}
const eventsService = {
  publishSubmissionEvent: jest.fn(),
}
const logEvent = jest.fn(async () => {})
logEvent.actions = {
  quality_checks_coi_author_reviewer: 'quality_checks_coi_author_reviewer',
}
logEvent.objectType = { manuscript: 'manuscript' }

describe('Handle COI use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()

    models = {
      Journal: {
        find: jest.fn(),
      },
      Manuscript: {
        findLastManuscriptBySubmissionId: jest.fn(),
        Statuses: getManuscriptStatuses(),
        knex: jest.fn(),
      },
      Review: {
        findAllValidAndSubmittedByManuscriptAndRole: jest.fn(() => []),
        findOneByManuscriptAndTeamMember: jest.fn(),
      },
      TeamMember: {
        find: jest.fn(),
        findSubmittingAuthor: jest.fn(),
        Statuses: getTeamMemberStatuses(),
        findOneByManuscriptAndRoleAndStatus: jest.fn(),
        findAllByManuscriptAndRoleAndStatus: jest.fn(() => []),
      },
      Team: {
        Role: getTeamRoles(),
      },
    }
    ;({ Journal, Manuscript, Team, TeamMember, Review } = models)
  })

  it('should set the academicEditorAssigned status when there are no valid reviews', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.inQA,
    })
    const academicEditor = generateTeamMember({
      team: { role: Team.Role.academicEditor },
    })
    const reviewer = generateTeamMember({
      team: { role: Team.Role.reviewer },
    })
    const review = generateReview({ isValid: false })

    jest
      .spyOn(Manuscript, 'findLastManuscriptBySubmissionId')
      .mockResolvedValue(manuscript)
    jest
      .spyOn(Review, 'findOneByManuscriptAndTeamMember')
      .mockResolvedValue(review)
    jest.spyOn(TeamMember, 'find').mockResolvedValue(reviewer)

    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue(academicEditor)
    jest.spyOn(Journal, 'find').mockResolvedValue({ name: 'some-name' })

    await initialize({
      models,
      logEvent,
      transaction: jest.fn(),
      eventsService,
      notificationService,
    }).execute({
      submissionId: manuscript.submissionId,
      reviewerIds: [reviewer.id],
    })

    expect(manuscript.status).toEqual(
      Manuscript.Statuses.academicEditorAssigned,
    )
    expect(logEvent).toHaveBeenCalledTimes(1)
  })

  it('should set the reviewCompleted status when there is at least two valid reviews', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.inQA,
    })
    const academicEditor = generateTeamMember({
      team: { role: Team.Role.academicEditor },
    })
    const reviewer = generateTeamMember({
      team: { role: Team.Role.reviewer },
    })
    const coiReview = generateReview({ isValid: true })

    jest
      .spyOn(Manuscript, 'findLastManuscriptBySubmissionId')
      .mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'find').mockResolvedValue(reviewer)
    jest
      .spyOn(Review, 'findOneByManuscriptAndTeamMember')
      .mockResolvedValue(coiReview)
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue(academicEditor)
    jest
      .spyOn(Review, 'findAllValidAndSubmittedByManuscriptAndRole')
      .mockResolvedValue([
        generateReview({ isValid: true }),
        generateReview({ isValid: true }),
      ])
    jest.spyOn(Journal, 'find').mockResolvedValue({ name: 'some-name' })

    await initialize({
      models,
      logEvent,
      transaction: jest.fn(),
      eventsService,
      notificationService,
    }).execute({
      submissionId: manuscript.submissionId,
      reviewerIds: [reviewer.id],
    })

    expect(manuscript.status).toEqual(Manuscript.Statuses.reviewCompleted)
    expect(logEvent).toHaveBeenCalledTimes(1)
  })
  it('should set the underReview status when there is at least one accepted reviewer', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.inQA,
    })
    const academicEditor = generateTeamMember({
      team: { role: Team.Role.academicEditor },
    })
    const reviewer = generateTeamMember({
      team: { role: Team.Role.reviewer },
    })
    const coiReview = generateReview({ isValid: true })

    jest
      .spyOn(Manuscript, 'findLastManuscriptBySubmissionId')
      .mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'find').mockResolvedValue(reviewer)
    jest
      .spyOn(Review, 'findOneByManuscriptAndTeamMember')
      .mockResolvedValue(coiReview)
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue(academicEditor)
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRoleAndStatus')
      .mockResolvedValue([
        generateTeamMember({
          team: { role: Team.Role.reviewer },
          status: TeamMember.Statuses.accepted,
        }),
      ])
    jest.spyOn(Journal, 'find').mockResolvedValue({ name: 'some-name' })

    await initialize({
      models,
      logEvent,
      transaction: jest.fn(),
      eventsService,
      notificationService,
    }).execute({
      submissionId: manuscript.submissionId,
      reviewerIds: [reviewer.id],
    })

    expect(
      TeamMember.findAllByManuscriptAndRoleAndStatus,
    ).toHaveBeenCalledTimes(1)
    expect(manuscript.status).toEqual(Manuscript.Statuses.underReview)
    expect(logEvent).toHaveBeenCalledTimes(1)
  })
})
