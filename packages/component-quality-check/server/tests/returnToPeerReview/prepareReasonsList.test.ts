import { prepareReasonsList } from '../../src/useCases/returnToPeerReviewHelpers/prepareReasonsList'
import {
  getAllIssuesMock,
  getImproperReviewMock,
  getConflictOfInterestMock,
  getAllIssuesExceptImproperReviewMock,
  getAllIssuesExceptDecisionMadeInErrorMock,
} from '../returnToPeerReview/mocks'

describe('Prepare reasons list', () => {
  it('should create a proper reasons list', () => {
    expect(
      prepareReasonsList(
        getConflictOfInterestMock(),
        getImproperReviewMock(),
        true, // decisionMadeInError,
      ),
    ).toEqual(getAllIssuesMock())

    expect(
      prepareReasonsList(
        getConflictOfInterestMock(),
        getImproperReviewMock(),
        false, // decisionMadeInError,
      ),
    ).toEqual(getAllIssuesExceptDecisionMadeInErrorMock())

    expect(
      prepareReasonsList(
        getConflictOfInterestMock(),
        { reviewers: [] }, // improperReview
        true, // decisionMadeInError,
      ),
    ).toEqual(getAllIssuesExceptImproperReviewMock())
  })
})
