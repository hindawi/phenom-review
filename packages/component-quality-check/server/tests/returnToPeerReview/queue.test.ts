import {
  createTaskQueue,
  updateQueueTasks,
} from '../../src/useCases/returnToPeerReviewHelpers/queue'
import {
  ReturnToPRReasons,
  Tasks,
} from '../../src/useCases/returnToPeerReviewHelpers/queue.utils'

import {
  getAllIssuesMock,
  getAllIssuesTaskQueueMock,
  getQueueWithTriageEditorTasksMock,
} from './mocks'

describe('Queue', () => {
  describe('createTaskQueue()', () => {
    it('should create proper taskQueue given all issues are present', () => {
      const payload = {
        submissionId: 'submission-id',
      }

      expect(createTaskQueue(getAllIssuesMock(), payload)).toEqual(
        getAllIssuesTaskQueueMock(),
      )
    })
  })

  describe('updateQueueTasks()', () => {
    it('should filter duplicate tasks', () => {
      const steps = [
        Tasks.invalidateDecision,
        Tasks.removeAcademicEditor,
        Tasks.invalidateAcademicEditorReview,
      ]

      const queueTask = updateQueueTasks(
        getQueueWithTriageEditorTasksMock(),
        steps,
        { submissionId: 'some-id', reason: ReturnToPRReasons.triageEditor },
      )

      const foundTasks = queueTask.filter(
        task => task.name === Tasks.invalidateDecision,
      )

      expect(foundTasks.length).toBe(1)
    })
  })
})
