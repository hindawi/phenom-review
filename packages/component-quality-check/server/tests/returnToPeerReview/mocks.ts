import * as tasks from '../../src/useCases/returnToPeerReviewHelpers/tasks'
import {
  COI,
  Reason,
  ImproperReview,
  ReturnToPRReasons,
  Queue,
  TaskI,
} from '../../src/useCases/returnToPeerReviewHelpers/queue.utils'

const generateReason = (
  reason: ReturnToPRReasons,
  conflictedTeamMembers = [{ id: 'tm-id' }],
): Reason => {
  const reasonToGenerate: Reason = { type: reason }

  if (reason !== ReturnToPRReasons.decisionMadeInError) {
    reasonToGenerate.conflictedTeamMembers = conflictedTeamMembers
  }

  return reasonToGenerate
}

const generateTask = (name: string, reason: ReturnToPRReasons): TaskI => {
  const submissionId = 'submission-id'

  const taskToGenerate: TaskI = {
    fn: tasks[name],
    name,
    payload: {
      submissionId,
      reason,
      isAERemoved: true,
      submittedReasonsToReturnToPR: getAllIssuesMock(),
    },
  }

  if (
    [
      ReturnToPRReasons.reviewers,
      ReturnToPRReasons.improperReview,
      ReturnToPRReasons.academicEditor,
    ].includes(reason)
  ) {
    taskToGenerate.payload.conflictedTeamMembers = [
      {
        id: 'tm-id',
      },
    ]
    taskToGenerate.payload.improperReviewAndCOIReviewers = [
      { id: 'tm-id' },
      { id: 'tm-id' },
    ]
  }

  return taskToGenerate
}

export const getAllIssuesMock = (): Reason[] => [
  generateReason(ReturnToPRReasons.triageEditor),
  generateReason(ReturnToPRReasons.academicEditor),
  generateReason(ReturnToPRReasons.reviewers),
  generateReason(ReturnToPRReasons.improperReview),
  generateReason(ReturnToPRReasons.decisionMadeInError),
]

export const getAllIssuesExceptDecisionMadeInErrorMock = (): Reason[] => [
  generateReason(ReturnToPRReasons.triageEditor),
  generateReason(ReturnToPRReasons.academicEditor),
  generateReason(ReturnToPRReasons.reviewers),
  generateReason(ReturnToPRReasons.improperReview),
]

export const getAllIssuesExceptImproperReviewMock = (): Reason[] => [
  generateReason(ReturnToPRReasons.triageEditor),
  generateReason(ReturnToPRReasons.academicEditor),
  generateReason(ReturnToPRReasons.reviewers),
  generateReason(ReturnToPRReasons.decisionMadeInError),
]

export const getAllIssuesTaskQueueMock = (): Queue => [
  generateTask('invalidateDecision', ReturnToPRReasons.triageEditor),
  generateTask('invalidateTriageEditor', ReturnToPRReasons.triageEditor),
  generateTask('removeAcademicEditor', ReturnToPRReasons.triageEditor),
  generateTask(
    'invalidateAcademicEditorReview',
    ReturnToPRReasons.triageEditor,
  ),
  generateTask('triggerInviteAE', ReturnToPRReasons.triageEditor),
  generateTask('notifyTEOfOwnConflict', ReturnToPRReasons.triageEditor),
  generateTask('scheduleActivityLog', ReturnToPRReasons.triageEditor),
  generateTask('notifyAEOfOwnConflict', ReturnToPRReasons.academicEditor),
  generateTask('notifyTEOfAEConflict', ReturnToPRReasons.academicEditor),
  generateTask(
    'invalidateReviewerReportOnConflicts',
    ReturnToPRReasons.reviewers,
  ),
  generateTask('removeReviewers', ReturnToPRReasons.reviewers),
  generateTask(
    'invalidateReviewerReportOnImproperReview',
    ReturnToPRReasons.improperReview,
  ),
  generateTask('setReviewerStatusToAccepted', ReturnToPRReasons.improperReview),
  generateTask(
    'createDraftReviewAndComments',
    ReturnToPRReasons.improperReview,
  ),
  generateTask('updateManuscriptStatus', ReturnToPRReasons.triageEditor),
]

export const getConflictOfInterestMock = (): COI[] =>
  [
    ReturnToPRReasons.triageEditor,
    ReturnToPRReasons.academicEditor,
    ReturnToPRReasons.reviewers,
  ].map(reason => ({
    type: reason,
    conflictedTeamMembers: [{ id: 'tm-id' }],
  }))

export const getImproperReviewMock = (): ImproperReview => ({
  reviewers: [{ id: 'tm-id' }],
})

export const getQueueWithTriageEditorTasksMock = (): Queue => [
  generateTask('invalidateDecision', ReturnToPRReasons.triageEditor),
  generateTask('invalidateTriageEditor', ReturnToPRReasons.triageEditor),
  generateTask('removeAcademicEditor', ReturnToPRReasons.triageEditor),
  generateTask(
    'invalidateAcademicEditorReview',
    ReturnToPRReasons.triageEditor,
  ),
]

export const getQueueWithReviewersAndImproperReviewIssues = (): Queue => [
  generateTask('invalidateDecision', ReturnToPRReasons.triageEditor),
  generateTask(
    'invalidateReviewerReportOnConflicts',
    ReturnToPRReasons.reviewers,
  ),
  generateTask(
    'invalidateReviewerReportOnImproperReview',
    ReturnToPRReasons.improperReview,
  ),
  generateTask('removeReviewers', ReturnToPRReasons.reviewers),
  generateTask('invalidateAcademicEditorReview', ReturnToPRReasons.reviewers),
  generateTask('setReviewerStatusToAccepted', ReturnToPRReasons.improperReview),
]
