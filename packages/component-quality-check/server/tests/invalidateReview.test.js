const {
  generateManuscript,
  generateTeamMember,
  generateReview,
} = require('component-generators')

const {
  invalidateReviewUseCase,
} = require('../src/useCases/conflictOfInterest')

let models = {}
let Review = {}

const trx = {}

describe('Invalidate review use-case', () => {
  beforeEach(() => {
    jest.clearAllMocks()

    models = {
      Review: {
        findAllByManuscriptAndTeamMember: jest.fn(),
      },
    }
    ;({ Review } = models)
  })

  it('Works correctly for valid review', async () => {
    const manuscript = generateManuscript()
    const academicEditor = generateTeamMember()
    const review = generateReview({
      $query: jest.fn(() => ({ update: jest.fn() })),
    })

    jest
      .spyOn(Review, 'findAllByManuscriptAndTeamMember')
      .mockResolvedValue([review])

    const res = await invalidateReviewUseCase
      .initialize({
        models,
      })
      .execute({
        submissionId: manuscript.submissionId,
        manuscriptId: manuscript.id,
        teamMemberId: academicEditor.id,
        trx,
      })

    expect(res).toBeDefined()
  })
  it('Returns if no review is found and no event is emitted', async () => {
    const manuscript = generateManuscript()
    const academicEditor = generateTeamMember()

    jest.spyOn(Review, 'findAllByManuscriptAndTeamMember').mockResolvedValue([])

    const res = await invalidateReviewUseCase
      .initialize({
        models,
      })
      .execute({
        submissionId: manuscript.submissionId,
        manuscriptId: manuscript.id,
        teamMemberId: academicEditor.id,
        trx,
      })

    expect(res).toBeUndefined()
  })
})
