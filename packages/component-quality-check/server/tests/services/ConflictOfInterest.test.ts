/* eslint-disable class-methods-use-this */

import { ConflictOfInterest } from '../../src/services/ConflictOfInterest'

const mockCreateConflictRepo = jest.fn()
const mockGetConflictsForTeamMembers = jest.fn().mockReturnValue([])

jest.mock('component-model', () => {
  class ConflictRepository {
    create() {
      mockCreateConflictRepo()
    }
    getConflictsForTeamMembers() {
      return mockGetConflictsForTeamMembers()
    }
  }

  return {
    repos: {
      ConflictRepository,
    },
  }
})

describe('ConflictOfInterest', () => {
  it('should have a createConflict fn', () => {
    expect(ConflictOfInterest.createConflict).toBeDefined()
  })

  it('should call the create fn from repo when trying to create a conflict', async () => {
    await ConflictOfInterest.createConflict({
      firstConflictingTeamMemberId: 'test123',
      secondConflictingTeamMemberId: 'test223',
    })

    expect(mockCreateConflictRepo).toHaveBeenCalled()
  })
})
