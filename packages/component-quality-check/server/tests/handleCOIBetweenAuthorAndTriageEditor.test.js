const {
  getTeamRoles,
  generateManuscript,
  generateTeamMember,
  getTeamMemberStatuses,
  getManuscriptStatuses,
} = require('component-generators')

const {
  handleCOIBetweenAuthorAndTriageEditorUseCase,
} = require('../src/useCases')

const useCases = {
  removeSubmissionUserUseCase: {
    initialize: jest.fn(() => ({ execute: jest.fn() })),
  },
  invalidateReviewUseCase: {
    initialize: jest.fn(() => ({ execute: jest.fn() })),
  },
  autoInviteAEUseCase: {
    initialize: jest.fn(() => ({ execute: jest.fn() })),
  },
}

let models = {}
let Journal = {}
let Manuscript = {}
let Team = {}
let TeamMember = {}

const notificationService = {
  notifyTriageEditorAboutHisConflictOfInterest: jest.fn(),
}
const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

const logEvent = jest.fn(async () => {})
logEvent.actions = {
  quality_checks_coi_author_academic_editor:
    'quality_checks_coi_author_academic_editor',
}
logEvent.objectType = { manuscript: 'manuscript' }

const trx = {
  commit: jest.fn(),
  rollback: jest.fn(),
}

const transaction = {
  start: jest.fn(() => trx),
}
describe('Handle COI between author and triage editor use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()

    models = {
      Journal: {
        find: jest.fn(),
      },
      Manuscript: {
        findLastManuscriptBySubmissionId: jest.fn(),
        Statuses: getManuscriptStatuses(),
        knex: jest.fn(),
      },
      TeamMember: {
        Statuses: getTeamMemberStatuses(),
        find: jest.fn(),
        findOneByManuscriptAndRoleAndStatus: jest.fn(),
        findOneByManuscriptAndRole: jest.fn(),
      },
      Team: {
        Role: getTeamRoles(),
      },
    }
    ;({ Manuscript, Team, TeamMember, Journal } = models)
  })

  it('Main flow works correctly', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.inQA,
      $query: jest.fn(() => ({ patch: jest.fn() })),
    })
    const academicEditor = generateTeamMember({
      team: { role: Team.Role.academicEditor },
      userId: 'ae-user-id',
    })

    jest
      .spyOn(Manuscript, 'findLastManuscriptBySubmissionId')
      .mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'find').mockResolvedValue(academicEditor)
    jest.spyOn(Journal, 'find').mockResolvedValue()
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue()
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue(academicEditor)

    await handleCOIBetweenAuthorAndTriageEditorUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        transaction,
        eventsService,
        notificationService,
      })
      .execute({
        submissionId: manuscript.submissionId,
        academicEditorId: academicEditor.id,
      })

    expect(trx.commit).toHaveBeenCalledTimes(1)
    expect(useCases.autoInviteAEUseCase.initialize).toHaveBeenCalledTimes(1)
    expect(logEvent).toHaveBeenCalledTimes(1)
    expect(
      notificationService.notifyTriageEditorAboutHisConflictOfInterest,
    ).toHaveBeenCalledTimes(1)
  })
})
