import { getEmailCopy } from '../../src/notifications/emailCopy'

describe('getEmailCopy', () => {
  it('for author-manuscript-returns-to-peer-review-for-regular-issue returns proper email data', () => {
    expect(
      getEmailCopy({
        emailType: 'author-manuscript-returns-to-peer-review-for-regular-issue',
        customId: 'test-custom-id',
        title: 'test-manuscript-title',
      }),
    ).toEqual({
      hasLink: false,
      subText: undefined,
      hasIntro: true,
      paragraph: `I am writing in regard to your manuscript test-custom-id, entitled test-manuscript-title. During our post-acceptance checks we have unfortunately identified a potential issue with the handling of your paper during the peer review process.<br/><br/>
      In order to progress your paper, therefore, we have invited another member of the Editorial Board to reassess your manuscript and its peer review thus far.<br/><br/>
      The new Editor will either invite additional reviewers, if necessary, or you will be notified of a decision as soon as possible. You will still be able to see the status of your manuscript on Phenom Review.<br/><br/>
      Please be assured that this is a standard part of our manuscript checks to ensure the rigour of our peer review process.<br/><br/>
      Please accept our apologies for any inconvenience or delay this may cause.<br></br>`,
      upperContent: undefined,
      lowerContent: undefined,
      hasSignature: true,
    })
  })

  it('for author-manuscript-returns-to-peer-review-for-special-issue returns proper email data', () => {
    expect(
      getEmailCopy({
        emailType: 'author-manuscript-returns-to-peer-review-for-special-issue',
        customId: 'test-custom-id',
        title: 'test-manuscript-title',
        specialIssueName: 'test-special-issue-name',
      }),
    ).toEqual({
      hasLink: false,
      subText: undefined,
      hasIntro: true,
      paragraph: `I am writing in regard to your manuscript test-custom-id, entitled test-manuscript-title, submitted to Special Issue test-special-issue-name. During our post-acceptance checks we have unfortunately identified a potential issue with the handling of your paper during the peer review process.<br/><br/>
      In order to progress your paper, therefore, we have invited another member of the Editorial Board to reassess your manuscript and its peer review thus far.<br/><br/>
      The new Editor will either invite additional reviewers, if necessary, or you will be notified of a decision as soon as possible. You will still be able to see the status of your manuscript on Phenom Review.<br/><br/>
      Please be assured that this is a standard part of our manuscript checks to ensure the rigour of our peer review process.<br/><br/>
      Please accept our apologies for any inconvenience or delay this may cause.<br></br>`,
      upperContent: undefined,
      lowerContent: undefined,
      hasSignature: true,
    })
  })

  it('for editorial-assistant-manuscript-returns-to-peer-review returns proper email data', () => {
    expect(
      getEmailCopy({
        emailType: 'editorial-assistant-manuscript-returns-to-peer-review',
        customId: 'test-custom-id',
        title: 'test-manuscript-title',
      }),
    ).toEqual({
      hasLink: false,
      subText: undefined,
      hasIntro: true,
      paragraph:
        'This email is confirmation that this manuscript test-custom-id, ' +
        'titled "test-manuscript-title" has been sent back for reassessment on the system.<br/><br/>' +
        'Please ensure that this manuscript progresses through the re-review process.<br/><br/>',
      upperContent: undefined,
      lowerContent: undefined,
      hasSignature: true,
    })
  })
})
