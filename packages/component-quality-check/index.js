const fs = require('fs')
const path = require('path')

const resolvers = require('./server/dist/resolvers')
const eventHandlers = require('./server/dist/eventHandlers')
const useCases = require('./server/dist/useCases')
const services = require('./server/dist/services')

module.exports = {
  resolvers,
  eventHandlers,
  typeDefs: fs.readFileSync(
    path.join(__dirname, '/server/src/typeDefs.graphqls'),
    'utf8',
  ),
  useCases,
  services,
}
