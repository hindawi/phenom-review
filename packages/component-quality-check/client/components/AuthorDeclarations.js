import React from 'react'
import { space } from 'styled-system'
import styled from 'styled-components'
import { useJournal } from 'component-journal-info'
import { get } from 'lodash'
import { YesOrNo } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import {
  Row,
  ContextualBox,
  Item,
  Label,
  Textarea,
  Text,
  ActionLink,
  ValidatedFormField,
  validators,
} from '@hindawi/ui'

const AuthorDeclarations = ({
  questions,
  formValues,
  setFieldValue,
  isConflictsVisible,
  isDataAvailabilityVisible,
  isFundingStatementVisible,
  isDataAvailabilityRequired,
}) => {
  const {
    links: { coiLink, dataAvailabilityLink },
  } = useJournal()
  return (
    <ContextualBox label="Author Declarations" startExpanded transparent>
      <SectionRoot ml={2} pl={2}>
        {isConflictsVisible && (
          <Row mt={6}>
            <Item vertical>
              <Label mb={2} required={isConflictsVisible}>
                {questions.conflictOfInterest.title}
              </Label>
              <Text display="inline" mb={2} secondary>
                {questions.conflictOfInterest.subtitle}{' '}
                <ActionLink display="inline" secondary to={coiLink}>
                  here.
                </ActionLink>
              </Text>
              <ValidatedFormField
                component={YesOrNo}
                name="meta.hasConflictOfInterest"
                onChange={() =>
                  setFieldValue('meta', {
                    ...formValues.meta,
                    conflictOfInterest: '',
                  })
                }
                validate={[validators.required]}
              />
              {get(formValues, 'meta.hasConflictOfInterest') === 'yes' && (
                <ValidatedFormField
                  component={Textarea}
                  name="meta.conflictOfInterest"
                  placeholder={questions.conflictOfInterest.placeholder}
                  validate={[validators.required]}
                />
              )}
            </Item>
          </Row>
        )}

        {isDataAvailabilityVisible && (
          <Row>
            <Item vertical>
              <Label mb={2} required={isDataAvailabilityRequired}>
                {questions.dataAvailability.title}
              </Label>
              <Text display="inline" mb={2} secondary>
                {questions.dataAvailability.subtitle}{' '}
                <ActionLink
                  display="inline"
                  secondary
                  to={dataAvailabilityLink}
                >
                  here.
                </ActionLink>
              </Text>

              <ValidatedFormField
                component={Textarea}
                minHeight={18}
                name="meta.dataAvailability"
                placeholder={questions.dataAvailability.placeholder}
                validate={
                  isDataAvailabilityRequired ? [validators.required] : []
                }
              />
            </Item>
          </Row>
        )}

        {isFundingStatementVisible && (
          <Row>
            <Item vertical>
              <Label mb={2} required>
                {questions.fundingStatement.title}
              </Label>
              <Text mb={2} secondary>
                {questions.fundingStatement.subtitle}
              </Text>
              <ValidatedFormField
                component={Textarea}
                minHeight={18}
                name="meta.fundingStatement"
                placeholder={questions.fundingStatement.placeholder}
                validate={[validators.required]}
              />
            </Item>
          </Row>
        )}
      </SectionRoot>
    </ContextualBox>
  )
}

const SectionRoot = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  border-left: 2px solid ${th('colorBorder')};

  ${space};
`

export default AuthorDeclarations
