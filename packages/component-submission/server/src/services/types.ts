export enum Activity {
  submission = 'submission',
  screening = 'screening',
  peerReview = 'peerReview',
  qualityChecks = 'qualityChecks',
}

export enum GlobalProperty {
  name = 'name',
  hasEditorialAssistant = 'hasEditorialAssistant',
}

export enum ArticleType {
  editorial = 'Editorial',
  retraction = 'Retraction',
  caseSeries = 'Case Series',
  caseReport = 'Case Report',
  reviewArticle = 'Review Article',
  researchArticle = 'Research Article',
  letterToTheEditor = 'Letter to the Editor',
  expressionOfConcern = 'Expression of Concern',
  erratum = 'Erratum',
  corrigendum = 'Corrigendum',
}

export enum PeerReviewModel {
  chiefMinus = 'Chief Minus',
  singleTierAcademicEditor = 'Single Tier Academic Editor',
  associateEditor = 'Associate Editor',
  sectionEditor = 'Section Editor',
}

export interface IEditorialModelProps {
  name?: string
  hasEditorialAssistant?: boolean
  [Activity.submission]?: {
    hasAbstract: boolean
    hasPreprints: boolean
    hasTransfers: boolean
    hasCoverLetter: boolean
    hasCOIDeclaration: boolean
    hasPrefilledTitle: boolean
    hasFundingStatement: boolean
    hasTermsAndConditions: boolean
    hasSupplementalMaterials: boolean
    hasDataAvailabilityStatement: boolean
  }
  [Activity.screening]?: object
  [Activity.peerReview]?: {
    approvalEditor: string
    triageEditor: {
      enabled: boolean
      label: string
    }
    academicEditor: {
      label: string
      labelInCaseOfConflict: string
      autoInviteInCaseOfConflict: false
    }
    figureHeadEditor: {
      enabled: false
      label: string | null
    }
    minNoOfReviewerReports: number
  }
  [Activity.qualityChecks]?: object
}

export interface ISubmissionEditorialModelProps {
  name?: string
  hasEditorialAssistant?: boolean
  hasAbstract: boolean
  hasPreprints: boolean
  hasTransfers: boolean
  hasCoverLetter: boolean
  hasCOIDeclaration: boolean
  hasPrefilledTitle: boolean
  hasFundingStatement: boolean
  hasTermsAndConditions: boolean
  hasSupplementalMaterials: boolean
  hasDataAvailabilityStatement: boolean
}

export interface FEMService {
  getEditorialModel(
    submissionId: string,
    peerReviewModelName: PeerReviewModel,
    articleType: ArticleType,
    isSpecialIssue: boolean,
    isOnSection: boolean,
  ): Promise<IEditorialModelProps>
  getEMPropertiesForSubmissionActivity(
    submissionId: string,
    peerReviewModelName: PeerReviewModel,
    articleType: ArticleType,
    isSpecialIssue: boolean,
    isOnSection: boolean,
  ): Promise<ISubmissionEditorialModelProps>
}
