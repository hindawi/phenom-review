import axios from 'axios'
import { logger } from 'component-logger'
import {
  FEMService,
  IEditorialModelProps,
  ArticleType,
  ISubmissionEditorialModelProps,
  Activity,
  GlobalProperty,
  PeerReviewModel,
} from './types'

const config = require('config')

const FEM_API_ENDPOINT = config.femApiEndpoint

class FEMServiceImpl implements FEMService {
  getEditorialModel = async (
    submissionId: string,
    peerReviewModelName: PeerReviewModel,
    articleType: ArticleType,
    isSpecialIssue: boolean,
    isOnSection: boolean,
  ): Promise<IEditorialModelProps> => {
    let editorialModel
    const body = {
      submissionId,
      peerReviewModel: peerReviewModelName,
      articleType,
      isSpecialIssue,
      isSection: isOnSection,
    }
    try {
      const { data } = await axios.post<IEditorialModelProps>(
        `${FEM_API_ENDPOINT}/editorial-model`,
        body,
      )
      editorialModel = data
    } catch (error) {
      logger.error(`Could not fetch the editorial model`, error)
    }
    return editorialModel
  }

  getEMPropertiesForSubmissionActivity = async (
    submissionId: string,
    peerReviewModelName: PeerReviewModel,
    articleType: ArticleType,
    isSpecialIssue: boolean,
    isOnSection: boolean,
  ): Promise<ISubmissionEditorialModelProps> => {
    const editorialModel = await this.getEditorialModel(
      submissionId,
      peerReviewModelName,
      articleType,
      isSpecialIssue,
      isOnSection,
    )
    if (!editorialModel) throw new Error('No Editorial Model was found')

    const submissionEditorialModel = { ...editorialModel[Activity.submission] }
    Object.keys(GlobalProperty).forEach(prop => {
      if (typeof editorialModel[prop] !== 'undefined') {
        Object.assign(submissionEditorialModel, {
          [prop]: editorialModel[prop],
        })
      }
    })
    return submissionEditorialModel
  }
}

export const femService = new FEMServiceImpl() as FEMService
