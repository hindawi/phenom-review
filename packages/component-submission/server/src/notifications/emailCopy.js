const getEmailCopy = ({
  emailType,
  manuscript,
  journalName,
  submittingAuthor,
  remainingCustomIdsCount,
  customIdsThreshold,
}) => {
  let paragraph
  const hasIntro = true
  const hasSignature = true
  let hasLink = true
  switch (emailType) {
    case 'confirmed-authors':
    case 'unconfirmed-authors':
      paragraph = `The manuscript titled "${
        manuscript.title
      }" has been submitted to ${journalName} by ${`${submittingAuthor.alias.givenNames} ${submittingAuthor.alias.surname}`}.<br /><br />
      To confirm the submission and view the status of the manuscript, please verify your details by clicking the link below.<br /><br />
      Thank you for submitting your work to ${journalName}.
    `
      break
    case 'submitting-author-manuscript-submitted':
      paragraph = `Congratulations, the manuscript titled "${manuscript.title}" has been successfully submitted to ${journalName}.<br/><br/>
        We will confirm this submission with all authors of the manuscript, but you will be the primary recipient of communications from the journal.
        As submitting author, you will be responsible for responding to editorial queries and making updates to the manuscript. <br/><br/>
        In order to view the status of the manuscript, please visit the manuscript details page.<br/><br/>
        Thank you for submitting your work to ${journalName}.`
      break
    case 'editors-new-manuscript-submitted':
      paragraph = `A new manuscript titled ${
        manuscript.title
      } by ${`${submittingAuthor.alias.givenNames} ${submittingAuthor.alias.surname}`} has been submitted to ${journalName}.<br/><br/>
        To begin the review process, please visit the manuscript details page.`
      break
    case 'alert-when-remaining-customIds-bellow-threshold':
      paragraph = `The number of remaining custom ids is ${remainingCustomIdsCount}, which is under the defined threshold of ${customIdsThreshold}. `
      hasLink = false
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return { paragraph, hasLink, hasIntro, hasSignature }
}

module.exports = {
  getEmailCopy,
}
