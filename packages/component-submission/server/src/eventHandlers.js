const { logger } = require('component-logger')
const useCases = require('./useCases')
const models = require('@pubsweet/models')
const notificationService = require('./notifications/notification')

module.exports = {
  async TriggeredNotifySubmitter(data) {
    return useCases.notifySubmitterUseCase
      .initialize({ models, notificationService })
      .execute({ data })
  },
  /**
   * handle ExternalSubmissionSubmitted event
   * @param data: ExternalSubmissionEvent
   */
  async ExternalSubmissionSubmitted(data) {
    logger.trace('ExternalSubmissionSubmitted handled')
  },
}
