input AuthorInput {
  email: String!
  givenNames: String!
  surname: String!
  aff: String!
  affRorId: String
  country: String!
  isSubmitting: Boolean
  isCorresponding: Boolean
}

input SubmissionEditorialModelInput {
  name: String
  hasTransfers: Boolean
  hasEditorialAssistant: Boolean
  hasPreprints: Boolean
  hasCoverLetter: Boolean
  hasSupplementalMaterials: Boolean
  hasAbstract: Boolean
  hasPrefilledTitle: Boolean
  hasCOIDeclaration: Boolean
  hasDataAvailabilityStatement: Boolean
  hasFundingStatement: Boolean
  hasTermsAndConditions: Boolean
}

input DraftFilesInput {
  figure: [FileInput]
  manuscripts: [FileInput]
  coverLetter: [FileInput]
  supplementary: [FileInput]
}

input MetadataInput {
  title: String!
  abstract: String
  agreeTc: Boolean
  conflictOfInterest: String
  dataAvailability: String
  fundingStatement: String
  articleTypeId: String
}

input DraftAutosaveInput {
  meta: MetadataInput
  authors: [AuthorInput]
  files: [FileInput]
  journalId: String
  sectionId: String
  specialIssueId: String
  preprintValue: String
  sourceJournalId: String
  sourceJournalManuscriptId: String
  linkedSubmissionCustomId: String
}

input CreateDraftManuscriptInput {
  journalId: String
  sectionId: String
  specialIssueId: String
  customId: String
}

extend type Mutation {
  createDraftManuscript(input: CreateDraftManuscriptInput): Manuscript
  updateDraftManuscript(
    manuscriptId: String!
    autosaveInput: DraftAutosaveInput
  ): Manuscript
  addAuthorToManuscript(
    manuscriptId: String!
    authorInput: AuthorInput!
  ): [TeamMember]
  removeAuthorFromManuscript(
    manuscriptId: String!
    authorTeamMemberId: String!
  ): [TeamMember]
  editAuthorFromManuscript(
    manuscriptId: String!
    authorTeamMemberId: String!
    authorInput: AuthorInput!
  ): [TeamMember]
  updateManuscriptFile(fileId: String!, type: String!): File
  submitManuscript(manuscriptId: String!): Boolean
  editManuscript(manuscriptId: String!): Boolean
  setSubmissionEditorialMapping(
    submissionId: String!
    submissionEditorialModel: SubmissionEditorialModelInput!
  ): SubmissionEditorialMapping
}

extend type Query {
  getActiveJournals: [Journal]
  getManuscriptByCustomId(input: String!): Manuscript
  getPublishedManuscripts(
    input: String!
    journalId: String!
    sectionId: String
    specialIssueId: String
  ): [Manuscript]
  getSubmissionEditorialModel(
    submissionId: String
    journalId: String
    issueType: String
    articleType: String
    isOnSection: Boolean
  ): SubmissionEditorialModel
  getSubmissionEditorialMapping(
    submissionId: String!
  ): SubmissionEditorialMapping
}
