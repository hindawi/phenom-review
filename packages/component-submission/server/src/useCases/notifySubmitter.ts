const initialize = ({
  models: { Manuscript, Journal, TeamMember, Team },
  notificationService,
}) => ({
  async execute({ data }) {
    const { submissionId } = data
    const manuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
    })
    const journal = await Journal.find(manuscript.journalId)
    const submittingStaffMember = await TeamMember.findOneByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.submittingStaffMember,
      eagerLoadRelations: 'user',
    })

    await notificationService.notifySubmitter({
      journal,
      manuscript,
      submittingStaffMember,
    })
  },
})

export { initialize }
