const initialize = ({ repos, models, transaction }) => ({
  async execute({ submissionId, submissionEditorialModel }) {
    const { SubmissionEditorialMappingRepo } = repos
    const submissionEditorialMappingRepo = new SubmissionEditorialMappingRepo()
    let submissionEditorialMapping

    const trx = await transaction.start(models.Manuscript.knex())
    try {
      submissionEditorialMapping = await submissionEditorialMappingRepo.createOrUpdateSubmissionEditorialMapping(
        submissionId,
        submissionEditorialModel,
        trx,
      )
      await trx.commit()
    } catch (err) {
      await trx.rollback()
      throw err
    }
    return submissionEditorialMapping
  },
})

const authsomePolicies = ['isAuthenticated']

export { initialize, authsomePolicies }
