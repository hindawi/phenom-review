const initialize = ({ Journal, User }) => ({
  execute: async ({ userId }) => {
    const user = await User.find(userId)
    const isStaffMember =
      (await user.isAdmin()) || (await user.isEditorialAssistant())

    if (isStaffMember) {
      return Journal.getAllActiveJournalsForStaffMembers()
    }
    return Journal.getAllActiveJournals()
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
