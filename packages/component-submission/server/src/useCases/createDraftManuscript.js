/* eslint-disable @typescript-eslint/no-use-before-define */
const config = require('config')

const MAX_CUSTOM_ID_TRIES = 10
const REMAINING_CUSTOM_IDS_ALERT_THRESHOLD = config.get(
  'remainingCustomIdsAlertThreshold',
)

const initialize = ({
  models: { Manuscript, Team, User, TeamMember, AvailableCustomIds },
  notificationService,
  transaction,
  logger,
  loaders,
}) => {
  const execute = async ({
    input: { journalId, sectionId, specialIssueId, customId },
    userId,
  }) => {
    let savedManuscript

    const trx = await transaction.start(Manuscript.knex())
    try {
      const manuscript = new Manuscript({
        journalId,
        sectionId,
        specialIssueId,
        customId: customId || (await getCustomId(trx)),
      })
      savedManuscript = await manuscript.save(trx)

      const teamMember = await addTeamMemberToManuscript({
        userId,
        manuscript: savedManuscript,
        trx,
      })
      await teamMember.$query(trx).insert(teamMember)

      await trx.commit()
    } catch (err) {
      logger.error('Something went wrong. No data was updated.', err)
      await trx.rollback()
      throw new Error('Something went wrong. No data was updated.')
    }
    await notifyIfThresholdOfCustomIdsIsBreached()

    return savedManuscript.toDTO()
  }

  const getCustomId = async trx => {
    let customIdFromPool
    let triesDone = 0

    do {
      try {
        // eslint-disable-next-line no-await-in-loop
        customIdFromPool = await AvailableCustomIds.pullCustomId(trx)
      } catch (err) {
        logger.error('Failed pulling a custom id from existing pool')
      }
      triesDone += 1
    } while (!customIdFromPool && triesDone <= MAX_CUSTOM_ID_TRIES)

    if (!customIdFromPool) {
      throw new Error('Unable to get a custom id')
    }

    return customIdFromPool.customId
  }

  const addTeamMemberToManuscript = async ({ userId, manuscript, trx }) => {
    const user = await User.find(userId, 'identities')

    const staffRoles = await loaders.TeamMember.staffRolesLoader.load(userId)

    return staffRoles && staffRoles.length
      ? addSubmittingStaffMember({ manuscript, user, trx })
      : addAuthor({ manuscript, user, trx })
  }

  const addSubmittingStaffMember = async ({ manuscript, user, trx }) => {
    const team = new Team({
      role: Team.Role.submittingStaffMember,
      manuscriptId: manuscript.id,
    })
    await team.save(trx)

    return team.addMember({
      user,
      options: {
        status: TeamMember.Statuses.pending,
      },
    })
  }

  const addAuthor = async ({ manuscript, user, trx }) => {
    const team = new Team({
      role: Team.Role.author,
      manuscriptId: manuscript.id,
    })
    await team.save(trx)

    return team.addMember({
      user,
      options: {
        isSubmitting: true,
        isCorresponding: true,
        status: TeamMember.Statuses.pending,
      },
    })
  }

  const notifyIfThresholdOfCustomIdsIsBreached = async () => {
    const numberOfCustomIds = await AvailableCustomIds.getNumberOfCustomIds()
    const { count: remainingCustomIdsCount } = numberOfCustomIds
    if (
      remainingCustomIdsCount < REMAINING_CUSTOM_IDS_ALERT_THRESHOLD &&
      Number(remainingCustomIdsCount) !== 0
    ) {
      try {
        await notificationService.sendAlertWhenRemainingCustomIdsBellowThreshold(
          {
            remainingCustomIdsCount,
            customIdsThreshold: REMAINING_CUSTOM_IDS_ALERT_THRESHOLD,
          },
        )
      } catch (err) {
        logger.error(
          'ERROR: The email regarding the number of availableCustomIds was not sent:',
          err,
        )
      }
    }
  }
  return { execute }
}

const authsomePolicies = ['isAuthenticated']
module.exports = {
  initialize,
  authsomePolicies,
}
