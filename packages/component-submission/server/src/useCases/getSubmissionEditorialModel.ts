const initialize = ({ models, femService }) => ({
  async execute({
    submissionId,
    journalId,
    issueType,
    articleType,
    isOnSection,
  }) {
    const peerReviewModel = await models.PeerReviewModel.findOneByJournal(
      journalId,
    )
    const peerReviewModelName = peerReviewModel.name
    const isSpecialIssue = issueType === 'specialIssue'

    return femService.getEMPropertiesForSubmissionActivity(
      submissionId,
      peerReviewModelName,
      articleType,
      isSpecialIssue,
      isOnSection,
    )
  },
})

const authsomePolicies = ['isAuthenticated']

export { initialize, authsomePolicies }
