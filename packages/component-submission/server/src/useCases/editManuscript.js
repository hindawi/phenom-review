const initialize = ({
  eventsService,
  fileValidator,
  models: { Manuscript },
}) => ({
  execute: async ({ manuscriptId }) => {
    await fileValidator.validateSubmissionFiles({ manuscriptId })

    const manuscript = await Manuscript.find(manuscriptId)

    if (
      [
        Manuscript.Statuses.technicalChecks,
        Manuscript.Statuses.inQA,
        Manuscript.Statuses.qualityChecksSubmitted,
      ].includes(manuscript.status)
    ) {
      await eventsService.publishSubmissionEvent({
        submissionId: manuscript.submissionId,
        eventName: 'SubmissionEdited',
      })
    }
  },
})

const authsomePolicies = ['admin', 'isEditorialAssistant']

module.exports = { initialize, authsomePolicies }
