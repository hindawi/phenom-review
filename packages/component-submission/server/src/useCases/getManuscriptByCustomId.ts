const initialize = ({ Manuscript }) => ({
  async execute({ input }) {
    return Manuscript.findLatestVersionByCustomId(input)
  },
})

const authsomePolicies = ['isAuthenticated']

export { initialize, authsomePolicies }
