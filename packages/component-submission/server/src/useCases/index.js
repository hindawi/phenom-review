const submitManuscriptUseCase = require('./submitManuscript')
const editManuscriptUseCase = require('./editManuscript')
const getActiveJournalsUseCase = require('./getActiveJournals')
const updateManuscriptFileUseCase = require('./updateManuscriptFile')
const addAuthorToManuscriptUseCase = require('./addAuthorToManuscript')
const createDraftManuscriptUseCase = require('./createDraftManuscript')
const updateDraftManuscriptUseCase = require('./updateDraftManuscript')
const editAuthorFromManuscriptUseCase = require('./editAuthorFromManuscript')
const removeAuthorFromManuscriptUseCase = require('./removeAuthorFromManuscript')
const assignEditorialAssistantOnManuscriptUseCase = require('./assignEditorialAssistantOnManuscript')
const getManuscriptByCustomIdUseCase = require('./getManuscriptByCustomId')
const getPublishedManuscriptsUseCase = require('./getPublishedManuscripts')
const notifySubmitterUseCase = require('./notifySubmitter')
const getSubmissionEditorialModelUseCase = require('./getSubmissionEditorialModel')
const setSubmissionEditorialMappingUseCase = require('./setSubmissionEditorialMapping')
const getSubmissionEditorialMappingUseCase = require('./getSubmissionEditorialMapping')

module.exports = {
  notifySubmitterUseCase,
  submitManuscriptUseCase,
  editManuscriptUseCase,
  getActiveJournalsUseCase,
  updateManuscriptFileUseCase,
  createDraftManuscriptUseCase,
  updateDraftManuscriptUseCase,
  addAuthorToManuscriptUseCase,
  editAuthorFromManuscriptUseCase,
  removeAuthorFromManuscriptUseCase,
  assignEditorialAssistantOnManuscriptUseCase,
  getManuscriptByCustomIdUseCase,
  getPublishedManuscriptsUseCase,
  getSubmissionEditorialModelUseCase,
  setSubmissionEditorialMappingUseCase,
  getSubmissionEditorialMappingUseCase,
}
