const initialize = ({ Manuscript }) => ({
  async execute({ input, journalId, sectionId, specialIssueId }) {
    const params = {
      'title:likeLower': `%${input}%`,
      'status:in': Manuscript.Statuses.published,
      rangeStart: 0,
      rangeEnd: 10,
    }
    const queryResults = await Manuscript.findAllV2({
      ...params,
      ...(specialIssueId && { specialIssueId }),
      ...(sectionId && { sectionId }),
      ...(journalId && !sectionId && !specialIssueId && { journalId }),
    })

    return queryResults.results.map(m => m.toDTO())
  },
})

const authsomePolicies = ['isAuthenticated']

export { initialize, authsomePolicies }
