const initialize = ({ SubmissionEditorialMappingRepo }) => ({
  async execute({ submissionId }) {
    const submissionEditorialMappingRepo = new SubmissionEditorialMappingRepo()
    return submissionEditorialMappingRepo.getSubmissionEditorialMapping(
      submissionId,
    )
  },
})

const authsomePolicies = ['isAuthenticated']

export { initialize, authsomePolicies }
