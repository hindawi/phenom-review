module.exports = {
  execute: async ({ manuscript, models: { Manuscript } }) => {
    manuscript.updateProperties({
      status: Manuscript.Statuses.inQA,
      submittedDate: new Date().toISOString(),
    })

    await manuscript.save()
  },
}
