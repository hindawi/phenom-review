const { logger } = require('component-logger')

module.exports = {
  execute: async ({
    journal,
    useCases,
    manuscript,
    models,
    eventsService,
    notificationService,
    getUserWithWorkloadUseCase,
  }) => {
    const { TeamMember, Team, User, Manuscript } = models
    const journalEditorialAssistants = await TeamMember.findAllByJournalAndRole(
      { journalId: manuscript.journalId, role: Team.Role.editorialAssistant },
    )
    const manuscriptEditorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    if (
      journalEditorialAssistants.length !== 0 &&
      !manuscriptEditorialAssistant
    ) {
      const getUserWithWorkload = await getUserWithWorkloadUseCase.initialize(
        models,
      )

      await useCases.assignEditorialAssistantOnManuscriptUseCase
        .initialize({
          eventsService,
          models: { TeamMember, Team, User },
          getUserWithWorkload,
          manuscriptStatuses: [
            ...Manuscript.InProgressStatuses,
            Manuscript.Statuses.inQA,
            Manuscript.Statuses.technicalChecks,
            Manuscript.Statuses.qualityChecksRequested,
            Manuscript.Statuses.qualityChecksSubmitted,
          ],
        })
        .execute({
          manuscriptId: manuscript.id,
          submissionId: manuscript.submissionId,
          journalId: journal.id,
          sectionId: manuscript.sectionId,
          specialIssueId: manuscript.specialIssueId,
        })
    }

    await manuscript.submitManuscript()
    logger.info(
      `Manuscript ${manuscript.id} has technicalCheckToken ${manuscript.technicalCheckToken} before saving`,
    )
    if (!manuscript.technicalCheckToken) {
      throw new ConflictError('Something went wrong. Please try again.')
    }
    await manuscript.save()
    logger.info(
      `Manuscript ${manuscript.id} has technicalCheckToken ${manuscript.technicalCheckToken} after saving`,
    )

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    const authorTeamMembers = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.author,
      eagerLoadRelations: 'user',
    })
    const submittingAuthor = authorTeamMembers.find(tm => tm.isSubmitting)

    notificationService.sendSubmittingAuthorConfirmation({
      journal,
      manuscript,
      submittingAuthor,
      editorialAssistant,
    })

    const coAuthors = authorTeamMembers.filter(author => !author.isSubmitting)
    coAuthors.forEach(async author => {
      const isConfirmed = await author.user.isUserConfirmed()
      if (isConfirmed) {
        notificationService.sendToConfirmedAuthors(author, {
          journal,
          manuscript,
          submittingAuthor,
          editorialAssistant,
        })
      } else {
        notificationService.sendToUnconfirmedAuthors(author, {
          journal,
          manuscript,
          submittingAuthor,
          editorialAssistant,
        })
      }
    })
  },
}
