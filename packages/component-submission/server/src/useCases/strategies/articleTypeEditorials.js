module.exports = {
  execute: async ({
    journal,
    useCases,
    manuscript,
    eventsService,
    notificationService,
    models,
    getUserWithWorkloadUseCase,
  }) => {
    const { Manuscript, TeamMember, Team, User } = models
    const journalEditorialAssistants = await TeamMember.findAllByJournalAndRole(
      { journalId: manuscript.journalId, role: Team.Role.editorialAssistant },
    )
    const manuscriptEditorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    if (
      journalEditorialAssistants.length !== 0 &&
      !manuscriptEditorialAssistant
    ) {
      const getUserWithWorkload = await getUserWithWorkloadUseCase.initialize(
        models,
      )

      await useCases.assignEditorialAssistantOnManuscriptUseCase
        .initialize({
          eventsService,
          models: { TeamMember, Team, User },
          getUserWithWorkload,
          manuscriptStatuses: [
            ...Manuscript.InProgressStatuses,
            Manuscript.Statuses.inQA,
            Manuscript.Statuses.technicalChecks,
            Manuscript.Statuses.qualityChecksRequested,
            Manuscript.Statuses.qualityChecksSubmitted,
          ],
        })
        .execute({
          manuscriptId: manuscript.id,
          submissionId: manuscript.submissionId,
          journalId: journal.id,
          sectionId: manuscript.sectionId,
          specialIssueId: manuscript.specialIssueId,
        })
    }
    const authorTeamMembers = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.author,
      eagerLoadRelations: 'user',
    })
    const submittingAuthor = authorTeamMembers.find(tm => tm.isSubmitting)

    manuscript.updateProperties({
      status: Manuscript.Statuses.makeDecision,
      submittedDate: new Date().toISOString(),
    })

    await manuscript.save()

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    notificationService.sendSubmittingAuthorConfirmation({
      journal,
      manuscript,
      submittingAuthor,
      editorialAssistant,
    })

    notificationService.notifyEditorsWhenNewManuscriptSubmitted({
      journal,
      manuscript,
      submittingAuthor,
      editorialAssistant,
    })
  },
}
