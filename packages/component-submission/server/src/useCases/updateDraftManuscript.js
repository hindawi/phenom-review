const { isEqual, sortBy } = require('lodash')

const getAuthorTeamMembers = async ({
  Team,
  TeamMember,
  manuscriptId,
  autosaveInput,
}) => {
  const options = {
    role: Team.Role.author,
    manuscriptId,
  }

  let authorTeam = {}
  let authorTeamMembers = []
  if (autosaveInput.authors.length > 0) {
    authorTeam = await Team.findOrCreate({
      queryObject: options,
      options,
    })
    authorTeamMembers = await TeamMember.findBy({
      teamId: authorTeam.id,
    })
  }

  return authorTeamMembers
}

const updateTeamMembersPosition = async ({
  authorTeamMembers,
  autosaveInput,
}) => {
  const existingAuthorsEmails = sortBy(authorTeamMembers, 'position').map(
    member => member.alias.email,
  )

  const inputAuthorsEmails = autosaveInput.authors.map(author => author.email)

  if (!isEqual(existingAuthorsEmails, inputAuthorsEmails)) {
    await Promise.all(
      inputAuthorsEmails.map(async (email, index) => {
        const teamMember = authorTeamMembers.find(
          member => member.alias.email === email,
        )
        if (!teamMember) return

        teamMember.updateProperties({ position: index })
        await teamMember.save()
      }),
    )
  }
}

const updateManuscriptFiles = async ({ manuscript, autosaveInput }) => {
  if (manuscript.files.length > 1) {
    const existingFilesByType = sortBy(manuscript.files, [
      'type',
      'position',
    ]).map(file => ({ id: file.id, type: file.type }))

    const inputFilesByType = sortBy(autosaveInput.files, [
      'fileType',
      'position',
    ]).map(file => ({ id: file.id, type: file.type }))

    if (!isEqual(existingFilesByType, inputFilesByType)) {
      const parsedFiles = inputFilesByType.reduce(
        (acc, current) => ({
          ...acc,
          [current.type]: [...acc[current.type], { id: current.id }],
        }),
        {
          manuscript: [],
          supplementary: [],
          coverLetter: [],
          figure: [],
        },
      )

      await Promise.all(
        Object.values(parsedFiles).map(files =>
          files.map(async (file, index) => {
            const matchingFile = manuscript.files.find(
              mFile => mFile.id === file.id,
            )
            matchingFile.updateProperties({ position: index })
            await matchingFile.save()
          }),
        ),
      )
    }
  }
}

const initialize = ({
  Team,
  Section,
  Manuscript,
  TeamMember,
  SpecialIssue,
  PeerReviewModel,
}) => ({
  execute: async ({ manuscriptId, autosaveInput }) => {
    const manuscript = await Manuscript.find(manuscriptId, 'files')
    if (
      manuscript.status === Manuscript.Statuses.accepted ||
      manuscript.status === Manuscript.Statuses.rejected
    ) {
      throw new AuthorizationError('Operation not permitted')
    }

    const authorTeamMembers = await getAuthorTeamMembers({
      Team,
      TeamMember,
      manuscriptId,
      autosaveInput,
    })

    await updateTeamMembersPosition({
      authorTeamMembers,
      autosaveInput,
    })

    await updateManuscriptFiles({ manuscript, autosaveInput })

    const {
      journalId,
      sourceJournalId,
      sourceJournalManuscriptId,
      linkedSubmissionCustomId,
      sectionId,
      specialIssueId,
      preprintValue,
    } = autosaveInput

    manuscript.updateProperties({
      ...autosaveInput.meta,
      journalId,
      sourceJournalId,
      sourceJournalManuscriptId,
      linkedSubmissionCustomId,
      sectionId,
      specialIssueId,
      preprintValue,
    })

    if (journalId || specialIssueId) {
      const peerReviewModel = await PeerReviewModel.findOneByManuscriptParent(
        manuscript,
      )

      if (peerReviewModel.academicEditorAutomaticInvitation) {
        manuscript.updateProperties({
          allowAcademicEditorAutomaticInvitation: true,
        })
      } else {
        manuscript.updateProperties({
          allowAcademicEditorAutomaticInvitation: null,
        })
      }
    }

    await manuscript.save()

    if (sectionId) {
      manuscript.section = await Section.findOneBy({
        queryObject: { id: sectionId },
      })
    }
    if (specialIssueId) {
      manuscript.specialIssue = await SpecialIssue.findOneBy({
        queryObject: { id: specialIssueId },
      })
    }

    return manuscript.toDTO()
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
