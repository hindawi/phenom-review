const { findIndex, sortBy } = require('lodash')

const initialize = ({
  models: { Team, Manuscript, TeamMember },
  logEvent,
}) => ({
  execute: async ({ manuscriptId, authorTeamMemberId, userId }) => {
    const authors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.author,
    })
    if (authors.length < 2) {
      throw new Error(
        `There are not enough authors on manuscript ${manuscriptId}`,
      )
    }

    const authorToBeRemoved = authors.find(
      author => author.id === authorTeamMemberId,
    )
    if (!authorToBeRemoved) {
      throw new Error(
        `Author ${authorTeamMemberId} has not been found in authors team of manuscript ${manuscriptId}`,
      )
    }

    const filteredAuthors = authors.filter(
      author => author.id !== authorTeamMemberId,
    )

    const submittingAuthorIndex = findIndex(filteredAuthors, [
      'isSubmitting',
      true,
    ])
    filteredAuthors[submittingAuthorIndex].isCorresponding = true

    await TeamMember.removeOneAndUpdateMany({
      memberToBeRemovedId: authorTeamMemberId,
      membersToBeUpdated: filteredAuthors,
    })

    const manuscript = await Manuscript.find(manuscriptId)
    if (manuscript.status !== Manuscript.Statuses.draft) {
      logEvent({
        userId,
        manuscriptId,
        action: logEvent.actions.author_removed,
        objectType: logEvent.objectType.user,
        objectId: authorToBeRemoved.userId,
      })
    }

    return sortBy(filteredAuthors, 'position').map(a => a.toDTO())
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
