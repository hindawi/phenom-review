const initialize = ({
  eventsService,
  manuscriptStatuses,
  models: { TeamMember, Team, User },
  getUserWithWorkload,
}) => ({
  async execute({
    journalId,
    sectionId,
    manuscriptId,
    submissionId,
    specialIssueId,
  }) {
    const editorialAssistant = await getUserWithWorkload.execute({
      journalId,
      sectionId,
      specialIssueId,
      manuscriptStatuses,
      role: Team.Role.editorialAssistant,
      teamMemberStatuses: [TeamMember.Statuses.active],
    })
    if (!editorialAssistant) {
      throw new Error(
        `No eligible EA has been found for journal ${journalId} on manuscript ${manuscriptId}`,
      )
    }

    const queryObject = {
      manuscriptId,
      role: Team.Role.editorialAssistant,
    }
    const editorialAssistantTeam = await Team.findOrCreate({
      queryObject,
      options: queryObject,
    })
    const editorialAssistantUser = await User.find(
      editorialAssistant.userId,
      'identities',
    )

    editorialAssistantTeam.addMember({
      user: editorialAssistantUser,
      options: {
        status: TeamMember.Statuses.active,
      },
    })

    await editorialAssistantTeam.saveGraph({
      insertMissing: true,
      relate: true,
      noUpdate: true,
    })

    eventsService.publishSubmissionEvent({
      submissionId,
      eventName: 'SubmissionEditorialAssistantAssigned',
    })
  },
})

module.exports = {
  initialize,
}
