const { includes } = require('lodash')

const initialize = ({
  models,
  logEvent,
  useCases,
  eventsService,
  fileValidator,
  notificationService,
  shortArticleType,
  articleTypeEditorials,
  articleTypeWithPeerReview,
  getUserWithWorkloadUseCase,
}) => {
  const { Manuscript, ArticleType } = models

  async function execute({ manuscriptId, userId }) {
    await fileValidator.validateSubmissionFiles({ manuscriptId })

    const manuscript = await Manuscript.find(
      manuscriptId,
      '[articleType, journal]',
    )

    const strategy = getStrategyBasedArticleType(manuscript.articleType)

    await strategy.execute({
      models,
      journal: manuscript.journal,
      useCases,
      manuscript,
      eventsService,
      notificationService,
      getUserWithWorkloadUseCase,
    })

    await eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionSubmitted',
    })

    await logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.manuscript_submitted,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  }

  function getStrategyBasedArticleType(currentArticleType) {
    if (includes(ArticleType.EditorialTypes, currentArticleType.name)) {
      return articleTypeEditorials
    }

    if (includes(ArticleType.ShortArticleTypes, currentArticleType.name)) {
      return shortArticleType
    }

    return articleTypeWithPeerReview
  }

  return { execute }
}

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
