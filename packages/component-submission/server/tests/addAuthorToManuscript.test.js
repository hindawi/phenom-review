process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')
const { pick, get } = require('lodash')

const chance = new Chance()

const { addAuthorToManuscriptUseCase } = require('../src/useCases')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = { author_added: 'author_added' }

describe('Add Author to Manuscript Use Case', () => {
  it('should add an existing user as author to the manuscript', async () => {
    const { Manuscript, Team, User, Identity } = models
    const authorIdentity = {
      givenNames: chance.first(),
      surname: chance.first(),
      email: chance.email(),
      country: chance.country(),
      aff: chance.company(),
    }
    const manuscript = fixtures.generateManuscript({ Manuscript })
    let authorTeam = fixtures.getTeamByManuscriptId(manuscript.id)
    if (!authorTeam)
      authorTeam = fixtures.generateTeam({
        properties: { role: Team.Role.author, manuscriptId: manuscript.id },
        Team,
      })
    manuscript.teams.push(authorTeam)

    const author = fixtures.generateUser({
      properties: { isActive: true },
      identityProperties: authorIdentity,
      User,
      Identity,
    })

    const result = await addAuthorToManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        authorInput: {
          isSubmitting: false,
          isCorresponding: false,
          ...authorIdentity,
        },
      })

    expect(result).toHaveLength(1)
    expect(result[0].user.id).toEqual(author.id)
    expect(result[0].alias.email).toEqual(authorIdentity.email)
  })

  it('should add a new user as author to the manuscript', async () => {
    const { Manuscript, Team, TeamMember, User, Identity } = models
    const newAuthorIdentity = {
      givenNames: chance.first(),
      surname: chance.first(),
      email: chance.email(),
      country: chance.country(),
      aff: chance.company(),
    }
    const manuscript = fixtures.generateManuscript({ Manuscript })
    const authorTeam = fixtures.generateTeam({
      properties: { role: Team.Role.author, manuscriptId: manuscript.id },
      Team,
    })
    manuscript.teams.push(authorTeam)

    const author = fixtures.generateUser({ User, Identity })

    const teamMember = fixtures.generateTeamMember({
      properties: { userId: author.id, teamId: authorTeam.id },
      TeamMember,
    })

    teamMember.user = author
    teamMember.setAlias(author.defaultIdentity)

    authorTeam.members.push(teamMember)

    const result = await addAuthorToManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        authorInput: {
          isSubmitting: false,
          isCorresponding: false,
          ...newAuthorIdentity,
        },
      })
    expect(result[0].alias.email).toEqual(authorTeam.members[1].alias.email)
  })

  it('should return an error when the invited user is not active', async () => {
    const { Manuscript, User, Identity, Team } = models
    const manuscript = fixtures.generateManuscript({ Manuscript })

    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
    })

    const coAuthor = fixtures.generateUser({
      properties: { isActive: false },
      User,
      Identity,
    })
    const identity = get(coAuthor, 'identities')
    const authorIdentity = pick(identity[0], [
      'email',
      'surname',
      'givenNames',
      'country',
      'aff',
    ])
    const result = addAuthorToManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        authorInput: {
          isSubmitting: false,
          isCorresponding: false,
          ...authorIdentity,
        },
      })
    return expect(result).rejects.toThrow('Invited user is inactive.')
  })

  it('should make the added author as submitting and corresponding if it is the first', async () => {
    const { Manuscript, Team, TeamMember, User, Identity } = models
    const authorIdentity = {
      givenNames: chance.first(),
      surname: chance.first(),
      email: chance.email(),
      country: chance.country(),
      aff: chance.company(),
    }
    const manuscript = fixtures.generateManuscript({ Manuscript })
    let authorTeam = fixtures.getTeamByManuscriptId(manuscript.id)
    if (!authorTeam) {
      authorTeam = fixtures.generateTeam({
        properties: { role: Team.Role.author, manuscriptId: manuscript.id },
        Team,
      })
      manuscript.teams.push(authorTeam)
    }

    const author = fixtures.generateUser({ User, Identity })

    const teamMember = fixtures.generateTeamMember({
      properties: { userId: author.id, teamId: authorTeam.id },
      TeamMember,
    })

    teamMember.user = author
    teamMember.setAlias(author.defaultIdentity)

    authorTeam.members.push(teamMember)

    const res = await addAuthorToManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        authorInput: {
          ...authorIdentity,
        },
      })
    expect(res[0].isSubmitting).toEqual(true)
    expect(res[0].isCorresponding).toEqual(true)
  })

  it('should remove the corresponding tag from other authors', async () => {
    const { Manuscript, Team } = models
    const authorIdentity = {
      givenNames: chance.first(),
      surname: chance.first(),
      email: chance.email(),
      country: chance.country(),
      aff: chance.company(),
    }
    const manuscript = fixtures.generateManuscript({ Manuscript })

    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
        isCorresponding: true,
      },
      role: Team.Role.author,
    })

    const res = await addAuthorToManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        authorInput: {
          ...authorIdentity,
          isCorresponding: true,
        },
      })
    expect(res[0].isCorresponding).toBe(true)
    expect(res[1].isCorresponding).toBe(false)
  })
})
