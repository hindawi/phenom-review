const Chance = require('chance')

const chance = new Chance()

const {
  getTeamRoles,
  generateFile,
  generateJournal,
  generateFileTypes,
  generateFileScanStatuses,
  generateTeamMember,
  generateManuscript,
  generateArticleType,
  getTeamMemberStatuses,
  getShortArticleTypes,
  getArticleTypesEditorialTypes,
  getTypesWithPeerReview,
} = require('component-generators')

const { submitManuscriptUseCase } = require('../src/useCases')

const models = {
  Manuscript: {
    find: jest.fn(),
  },
  Team: {
    Role: getTeamRoles(),
  },
  TeamMember: {
    Statuses: getTeamMemberStatuses(),
    findTriageEditor: jest.fn(),
    findAllByJournalAndRole: jest.fn(),
    findAllByManuscriptAndRole: jest.fn(),
    findOneByManuscriptAndRoleAndStatus: jest.fn(),
  },
  ArticleType: {
    EditorialTypes: getArticleTypesEditorialTypes(),
    ShortArticleTypes: getShortArticleTypes(),
    TypesWithPeerReview: getTypesWithPeerReview(),
  },
  File: {
    findBy: jest.fn(),
    Types: generateFileTypes(),
    ScanStatuses: generateFileScanStatuses(),
  },
}
const useCases = {}

const { Manuscript, TeamMember, File } = models

const logEvent = jest.fn()
logEvent.actions = { manuscript_submitted: 'manuscript_submitted' }
logEvent.objectType = { manuscript: 'manuscript' }

const notificationService = {
  sendSubmittingAuthorConfirmation: jest.fn(),
}
const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

const getUserWithWorkloadUseCase = jest.fn()
const userId = chance.guid()

const validateFilesUseCase = {
  initialize: jest.fn(({ File }) => ({
    validateSubmissionFiles: jest.fn(),
    validateCommentFile: jest.fn(),
  })),
}
const fileValidator = validateFilesUseCase.initialize({ File })

describe('submitManuscript', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Should validate files', async () => {
    const journal = generateJournal()
    const articleType = generateArticleType()
    const manuscript = generateManuscript({ journal, articleType })
    const author = generateTeamMember()
    const shortArticleType = { execute: jest.fn() }
    const articleTypeEditorials = { execute: jest.fn() }
    const articleTypeWithPeerReview = { execute: jest.fn() }

    const mainFile = generateFile({ type: 'manuscript', scanStatus: 'healthy' })
    const coverLetter = generateFile({
      type: 'coverLetter',
      scanStatus: 'scanning',
    })

    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'findAllByJournalAndRole').mockResolvedValue([])
    jest.spyOn(File, 'findBy').mockResolvedValue([mainFile, coverLetter])
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([author])

    await submitManuscriptUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        fileValidator,
        eventsService,
        notificationService,
        shortArticleType,
        articleTypeEditorials,
        articleTypeWithPeerReview,
        getUserWithWorkloadUseCase,
      })
      .execute({ manuscriptId: manuscript.id, userId })

    expect(fileValidator.validateSubmissionFiles).toHaveBeenCalledTimes(1)
  })

  it('Should pass successfully if manuscript is an editorial article type', async () => {
    const journal = generateJournal()
    const file = generateFile({ type: 'manuscript', scanStatus: 'healthy' })
    const articleType = generateArticleType()
    const manuscript = generateManuscript({
      journal,
      articleType: { ...articleType, name: 'Commentary' },
    })
    const author = generateTeamMember()
    const shortArticleType = { execute: jest.fn() }
    const articleTypeEditorials = { execute: jest.fn() }
    const articleTypeWithPeerReview = { execute: jest.fn() }

    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'findAllByJournalAndRole').mockResolvedValue([])
    jest.spyOn(File, 'findBy').mockResolvedValue([file])
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([author])

    await submitManuscriptUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        fileValidator,
        eventsService,
        notificationService,
        shortArticleType,
        articleTypeEditorials,
        articleTypeWithPeerReview,
        getUserWithWorkloadUseCase,
      })
      .execute({ manuscriptId: manuscript.id, userId })

    expect(articleTypeEditorials.execute).toHaveBeenCalledTimes(1)
    expect(articleTypeEditorials.execute).toHaveBeenCalledWith({
      models,
      journal,
      useCases,
      manuscript,
      eventsService,
      notificationService,
      getUserWithWorkloadUseCase,
    })

    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledTimes(1)
    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledWith({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionSubmitted',
    })

    expect(logEvent).toHaveBeenCalledTimes(1)
    expect(logEvent).toHaveBeenCalledWith({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.manuscript_submitted,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  })

  it('Should pass successfully if manuscript is an short article type', async () => {
    const journal = generateJournal()
    const file = generateFile({ type: 'manuscript', scanStatus: 'healthy' })
    const articleType = generateArticleType()
    const manuscript = generateManuscript({
      journal,
      articleType: { ...articleType, name: 'Retraction' },
    })
    const author = generateTeamMember()
    const shortArticleType = { execute: jest.fn() }
    const articleTypeEditorials = { execute: jest.fn() }
    const articleTypeWithPeerReview = { execute: jest.fn() }

    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'findAllByJournalAndRole').mockResolvedValue([])
    jest.spyOn(File, 'findBy').mockResolvedValue([file])
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([author])

    await submitManuscriptUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        fileValidator,
        eventsService,
        notificationService,
        shortArticleType,
        articleTypeEditorials,
        articleTypeWithPeerReview,
        getUserWithWorkloadUseCase,
      })
      .execute({ manuscriptId: manuscript.id, userId })

    expect(shortArticleType.execute).toHaveBeenCalledTimes(1)
    expect(shortArticleType.execute).toHaveBeenCalledWith({
      models,
      journal,
      useCases,
      manuscript,
      eventsService,
      notificationService,
      getUserWithWorkloadUseCase,
    })

    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledTimes(1)
    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledWith({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionSubmitted',
    })

    expect(logEvent).toHaveBeenCalledTimes(1)
    expect(logEvent).toHaveBeenCalledWith({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.manuscript_submitted,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  })

  it('Should pass successfully if manuscript has an article type with peer review', async () => {
    const journal = generateJournal()
    const file = generateFile({ type: 'manuscript', scanStatus: 'healthy' })
    const articleType = generateArticleType()
    const manuscript = generateManuscript({
      journal,
      articleType: { ...articleType, name: 'Case Series' },
    })
    const author = generateTeamMember()
    const shortArticleType = { execute: jest.fn() }
    const articleTypeEditorials = { execute: jest.fn() }
    const articleTypeWithPeerReview = { execute: jest.fn() }

    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'findAllByJournalAndRole').mockResolvedValue([])
    jest.spyOn(File, 'findBy').mockResolvedValue([file])
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([author])

    await submitManuscriptUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        fileValidator,
        eventsService,
        notificationService,
        shortArticleType,
        articleTypeEditorials,
        articleTypeWithPeerReview,
        getUserWithWorkloadUseCase,
      })
      .execute({ manuscriptId: manuscript.id, userId })

    expect(articleTypeWithPeerReview.execute).toHaveBeenCalledTimes(1)
    expect(articleTypeWithPeerReview.execute).toHaveBeenCalledWith({
      models,
      journal,
      useCases,
      manuscript,
      eventsService,
      notificationService,
      getUserWithWorkloadUseCase,
    })

    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledTimes(1)
    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledWith({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionSubmitted',
    })

    expect(logEvent).toHaveBeenCalledTimes(1)
    expect(logEvent).toHaveBeenCalledWith({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.manuscript_submitted,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  })
})
