const Chance = require('chance')
const models = require('@pubsweet/models')

const chance = new Chance()

const { editManuscriptUseCase } = require('../src/useCases')

const { Manuscript, File } = models

const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

const validateFilesUseCase = {
  initialize: jest.fn(({ File }) => ({
    validateSubmissionFiles: jest.fn(),
    validateCommentFile: jest.fn(),
  })),
}
const fileValidator = validateFilesUseCase.initialize({ File })

describe('editManuscript', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Should validate files', async () => {
    const manuscriptId = chance.guid()
    const manuscriptFromDb = {
      submissionId: chance.guid(),
      status: Manuscript.Statuses.draft,
    }

    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscriptFromDb)

    await editManuscriptUseCase
      .initialize({
        eventsService,
        fileValidator,
        models: { Manuscript, File },
      })
      .execute({ manuscriptId })

    expect(fileValidator.validateSubmissionFiles).toHaveBeenCalledTimes(1)
  })

  it('Should send event if the manuscript is in Screening/QC', async () => {
    const manuscriptId = chance.guid()
    const manuscriptFromDb = {
      submissionId: chance.guid(),
      status: Manuscript.Statuses.technicalChecks,
    }

    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscriptFromDb)

    await editManuscriptUseCase
      .initialize({
        eventsService,
        fileValidator,
        models: { Manuscript, File },
      })
      .execute({ manuscriptId })

    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledTimes(1)
    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledWith({
      submissionId: manuscriptFromDb.submissionId,
      eventName: 'SubmissionEdited',
    })
  })

  it('Should not send event if the manuscript is not in Screening/QC', async () => {
    const manuscriptId = chance.guid()
    const manuscriptFromDb = {
      submissionId: chance.guid(),
      status: Manuscript.Statuses.submitted,
    }

    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscriptFromDb)

    await editManuscriptUseCase
      .initialize({
        eventsService,
        fileValidator,
        models: { Manuscript, File },
      })
      .execute({ manuscriptId })

    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledTimes(0)
  })
})
