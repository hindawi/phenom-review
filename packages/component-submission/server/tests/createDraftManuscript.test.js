const {
  generateUser,
  getTeamMemberStatuses,
  getTeamRoles,
  generateManuscript,
  generateTeamMember,
} = require('component-generators')

const { createDraftManuscriptUseCase } = require('../src/useCases')

const logger = {
  error: jest.fn(),
}

const trx = {
  commit: jest.fn(),
  rollback: jest.fn(),
}
const transaction = {
  start: jest.fn(() => trx),
}

const models = {
  Manuscript: jest.fn().mockReturnValue({
    save: () =>
      generateManuscript({
        toDTO: function toDTO() {
          return this
        },
      }),
  }),
  Team: jest.fn().mockReturnValue({
    addMember: () =>
      generateTeamMember({
        $query: jest.fn(() => ({
          insert: jest.fn(),
        })),
      }),
    save: jest.fn(),
  }),
  TeamMember: {
    Statuses: getTeamMemberStatuses(),
    findOneByUserAndRoleAndStatus: jest.fn(),
  },
  User: { find: jest.fn() },
  AvailableCustomIds: {
    pullCustomId: () => ({
      preGeneratedCustomId: '1234567',
    }),
    getNumberOfCustomIds: () => {},
    knex: jest.fn(),
  },
}
const { AvailableCustomIds, User, Manuscript } = models

Object.assign(models.Team, {
  Role: getTeamRoles(),
})
Object.assign(models.Manuscript, {
  generateUniqueCustomId: jest.fn(),
  knex: jest.fn(),
})

const notificationService = {
  sendAlertWhenRemainingCustomIdsBellowThreshold: jest.fn(),
}
const journalId = 'journal-id'
const userId = 'user-id'

const mockLoaders = {
  TeamMember: {
    staffRolesLoader: {
      load: (_, __) => Promise.resolve([]),
    },
  },
}

describe('Create Draft manuscript use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('creates draft manuscript and gets customId from pool', async () => {
    const user = generateUser()
    jest.spyOn(User, 'find').mockResolvedValue(user)

    jest
      .spyOn(AvailableCustomIds, 'pullCustomId')
      .mockResolvedValue({ customId: 1234567 })

    jest
      .spyOn(AvailableCustomIds, 'getNumberOfCustomIds')
      .mockResolvedValue({ count: 1 })

    await createDraftManuscriptUseCase
      .initialize({
        notificationService,
        models,
        transaction,
        logger,
        loaders: mockLoaders,
      })
      .execute({
        input: { journalId },
        userId,
      })
    await expect(AvailableCustomIds.pullCustomId).toHaveBeenCalledTimes(1)
    await expect(Manuscript.generateUniqueCustomId).toHaveBeenCalledTimes(0)
    await expect(
      notificationService.sendAlertWhenRemainingCustomIdsBellowThreshold,
    ).toHaveBeenCalledTimes(1)
  })
})
