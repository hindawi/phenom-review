export { default as Wizard } from './pages/Wizard'

export { default as SubmissionFromURL } from './pages/SubmissionFromURL'
export { default as SubmissionConfirmation } from './pages/SubmissionConfirmation'

export * from './graphql'

export { default as SubmitDraft } from './components/SubmitDraft'
export { default as TransferManuscript } from './components/TransferManuscript'
export { default as AutosaveIndicator } from './components/AutosaveIndicator'
export { visibleDeclarations } from './visibleDeclarations'
export {
  checkIfIsShortArticleType,
  checkIfAllowsLinkedArticle,
  checkIfCanBeSubmittedToSI,
} from './articleTypesUtils'
