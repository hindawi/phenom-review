Submission wizard step two.

```js
const { Formik } = require('formik')

const journal = {
  manuscriptTypes: [
    { value: 'research', label: 'Research' },
    { value: 'clinical-study', label: 'Clinical Study' },
    { value: 'academic', label: 'Something Academic' },
  ],
  submission: {
    questions: [
      {
        id: 'conflictsOfInterest',
        commentsFieldName: 'conflicts.message',
        commentsSubtitle: '',
        subtitle: null,
        commentsPlaceholder: 'Please provide the conflicts of interest',
        commentsOn: 'yes',
        radioFieldName: 'conflicts.hasConflicts',
        radioLabel: 'Do any authors have conflicts of interest to declare?',
        required: true,
      },
    ],
  },
}

const initialValues = {
  authors: [
    {
      id: '123',
      email: 'abcalongemailrightherebroshouldoverflowwithstyle@def.ghi',
      firstName: 'Alpha',
      lastName: 'Bravo',
      affiliation: 'Stupidly Long Affiliation Name Here',
      country: 'RO',
    },
    {
      id: '456',
      email: 'jkl@mno.pqr',
      firstName: 'Delta',
      lastName: 'Echo',
      affiliation: 'Foxtrot',
      country: 'RO',
    },
  ],
}

;<Formik initialValues={initialValues}>
  {({ values }) => <WizardStepThree formValues={values} journal={journal} />}
</Formik>
```
