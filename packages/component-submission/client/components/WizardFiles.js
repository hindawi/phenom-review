import React, { Fragment } from 'react'
import { get } from 'lodash'

import FileSection from './FileSection'

const WizardFiles = ({
  files,
  compact,
  onUploadFile,
  onChangeList,
  onDeleteFile,
  manuscriptStatus,
  setIsFileUploading,
}) => {
  const isLastSection = manuscriptStatus !== 'qualityChecksRequested'

  return (
    <Fragment>
      <FileSection
        allowedFileExtensions={['pdf', 'doc', 'docx', 'txt', 'rdf', 'odt']}
        compact={compact}
        files={get(files, `manuscript`, []).filter(Boolean)}
        isFirst
        listName="manuscript"
        maxFiles={1}
        onChangeList={onChangeList}
        onDeleteFile={onDeleteFile}
        onUploadFile={onUploadFile}
        required
        setIsFileUploading={setIsFileUploading}
        title="Main Manuscript"
        withScanning
      />
      <FileSection
        allowedFileExtensions={[
          'pdf',
          'doc',
          'docx',
          'txt',
          'rdf',
          'odt',
          'png',
          'jpg',
          'jpeg',
        ]}
        compact={compact}
        files={get(files, `coverLetter`, []).filter(Boolean)}
        listName="coverLetter"
        maxFiles={1}
        onChangeList={onChangeList}
        onDeleteFile={onDeleteFile}
        onUploadFile={onUploadFile}
        setIsFileUploading={setIsFileUploading}
        title="Cover Letter"
        withScanning
      />
      <FileSection
        compact={compact}
        files={get(files, `supplementary`, []).filter(Boolean)}
        isLast={isLastSection}
        listName="supplementary"
        maxFiles={Number.MAX_SAFE_INTEGER}
        onChangeList={onChangeList}
        onDeleteFile={onDeleteFile}
        onUploadFile={onUploadFile}
        setIsFileUploading={setIsFileUploading}
        title="Supplemental Files"
        withScanning
      />
      {manuscriptStatus === 'qualityChecksRequested' && (
        <FileSection
          compact={compact}
          files={get(files, `figure`, []).filter(Boolean)}
          isLast
          listName="figure"
          maxFiles={Number.MAX_SAFE_INTEGER}
          onChangeList={onChangeList}
          onDeleteFile={onDeleteFile}
          onUploadFile={onUploadFile}
          setIsFileUploading={setIsFileUploading}
          title="Figure Files"
          withScanning
        />
      )}
    </Fragment>
  )
}

export default WizardFiles
