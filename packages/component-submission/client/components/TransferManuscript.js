import React from 'react'
import { Button } from '@pubsweet/ui'
import { chain } from 'lodash'
import { withRouter } from 'react-router-dom'
import { compose, withHandlers, withProps } from 'recompose'

const TransferManuscript = ({
  label = 'Transfer Manuscript',
  transferManuscript,
  isConfirmed,
}) => (
  <Button
    data-test-id="transfer-manuscript"
    disabled={!isConfirmed}
    mr={4}
    onClick={transferManuscript}
    primary
    xs
  >
    {label}
  </Button>
)

export default compose(
  withRouter,
  withHandlers({
    transferManuscript: () => () => {
      window.location.assign(process.env.TRANSFER_MANUSCRIPT_URL)
    },
  }),
  withProps(({ currentUser }) => ({
    isConfirmed: chain(currentUser)
      .get('identities', [])
      .find(i => i.__typename === 'Local')
      .get('isConfirmed', '')
      .value(),
  })),
)(TransferManuscript)
