/* eslint-disable no-nested-ternary */
import React, { Fragment } from 'react'
import { withTheme } from 'styled-components'
import { Button, Spinner } from '@pubsweet/ui'
import { Row, Icon } from '@hindawi/ui'

const isDisabled = ({
  journal,
  isFirst,
  isFetchingEditorialModel,
  hasArticleType,
  hasSubmissionEditorialModel,
}) => {
  if (!journal && !isFirst) return true
  if (isFetchingEditorialModel) return true
  if (hasArticleType && !hasSubmissionEditorialModel) return true
  return false
}

const WizardButtons = ({
  theme,
  isLast,
  isFirst,
  history,
  prevStep,
  editMode,
  isFetching,
  handleSubmit,
  journal,
  isFileUploading,
  isFetchingEditorialModel,
  hasArticleType,
  hasSubmissionEditorialModel,
}) => (
  <Row justify="center" mt={9}>
    {isFetching || isFileUploading ? (
      <Spinner />
    ) : (
      <Fragment>
        <Button
          mr={12}
          onClick={isFirst ? () => history.goBack() : prevStep}
          width={48}
        >
          <Icon color="colorSecondary" icon="caretLeft" pb={1 / 2} pr={1} />
          BACK
        </Button>
        <Button
          disabled={isDisabled({
            journal,
            isFirst,
            isFetchingEditorialModel,
            hasArticleType,
            hasSubmissionEditorialModel,
          })}
          onClick={handleSubmit}
          primary
          width={48}
        >
          {isLast
            ? editMode
              ? 'SAVE CHANGES'
              : 'SUBMIT MANUSCRIPT'
            : 'NEXT STEP'}
          <Icon
            color="colorBackgroundHue"
            icon="caretRight"
            pb={1 / 2}
            pl={1}
          />
        </Button>
      </Fragment>
    )}
  </Row>
)

export default withTheme(WizardButtons)
// #endregion
