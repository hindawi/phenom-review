import React from 'react'
import { Button } from '@pubsweet/ui'
import { chain } from 'lodash'
import { Mutation } from 'react-apollo'
import { withRouter } from 'react-router-dom'
import { compose, withHandlers, withProps } from 'recompose'
import { useJournal } from 'component-journal-info'

import { mutations } from '../graphql'

const SubmitDraft = ({ label = 'Submit', onClickSubmit, isConfirmed }) => {
  const {
    name,
    links: { newGSWSubmissionSystemLink },
  } = useJournal()
  const IS_GSW = name === 'GeoScienceWorld'

  return (
    <Mutation
      mutation={mutations.createDraftManuscript}
      variables={{ input: { journalId: null } }}
    >
      {mutateFn => (
        <Button
          data-test-id="manuscript-submit"
          disabled={!isConfirmed}
          mr={4}
          onClick={onClickSubmit(mutateFn, IS_GSW, newGSWSubmissionSystemLink)}
          primary
          xs
        >
          {label}
        </Button>
      )}
    </Mutation>
  )
}
export default compose(
  withRouter,
  withHandlers({
    onClickSubmit: ({ history }) => (
      createDraft,
      IS_GSW,
      newGSWSubmissionSystemLink,
    ) => () => {
      if (IS_GSW) {
        window.location.assign(newGSWSubmissionSystemLink)
      } else {
        createDraft().then(({ data: { createDraftManuscript } }) => {
          history.push(
            `/submit/${createDraftManuscript.submissionId}/${createDraftManuscript.id}`,
          )
        })
      }
    },
  }),
  withProps(({ currentUser }) => ({
    isConfirmed: chain(currentUser)
      .get('identities', [])
      .find(i => i.__typename === 'Local')
      .get('isConfirmed', '')
      .value(),
  })),
)(SubmitDraft)
