import React from 'react'
import { Formik } from 'formik'
import { DragDropContext } from 'react-dnd'
import { theme } from '@hindawi/ui'
import { render } from '@testing-library/react'
import TestBackend from 'react-dnd-test-backend'
import { ThemeProvider } from 'styled-components'
import { JournalProvider as HindawiContextProvider } from 'component-journal-info'

import { publisher, journal } from './mockData'

export const renderWithDragAndFormik = (
  Component,
  { formProps, compProps },
) => {
  const C = DragDropContext(TestBackend)(Component)
  return render(
    <HindawiContextProvider journal={journal} publisher={publisher}>
      <ThemeProvider theme={theme}>
        <Formik {...formProps}>{() => <C {...compProps} />}</Formik>
      </ThemeProvider>
    </HindawiContextProvider>,
  )
}

export const withDragContext = Component =>
  DragDropContext(TestBackend)(Component)

export const renderWithDrag = ui => {
  const C = DragDropContext(TestBackend)(() => (
    <ThemeProvider theme={theme}>{ui}</ThemeProvider>
  ))
  return render(<C />)
}

export const mockMatchMedia = () =>
  Object.defineProperty(window, 'matchMedia', {
    writable: true,
    value: jest.fn().mockImplementation(query => ({
      matches: false,
      media: query,
      onchange: null,
      addListener: jest.fn(), // Deprecated
      removeListener: jest.fn(), // Deprecated
      addEventListener: jest.fn(),
      removeEventListener: jest.fn(),
      dispatchEvent: jest.fn(),
    })),
  })
