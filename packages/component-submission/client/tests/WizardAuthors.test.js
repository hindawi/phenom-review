import '@testing-library/jest-dom/extend-expect'
import { cleanup, fireEvent } from '@testing-library/react'

import WizardAuthors from '../components/WizardAuthors'
import { renderWithDragAndFormik as render, mockMatchMedia } from './testUtils'

mockMatchMedia()

const authors = [
  {
    id: '123',
    isSubmitting: true,
    isCorresponding: false,
    alias: {
      aff: 'Stupidly Long Affiliation Name Here',
      email: 'abcalongemailrightherebroshouldoverflowwithstyle@def.ghi',
      country: 'RO',
      name: {
        givenNames: 'Alpha',
        surname: 'Bravo',
      },
    },
  },
  {
    id: '456',
    isSubmitting: false,
    isCorresponding: false,
    alias: {
      aff: 'Mormon Church',
      email: 'that.nigerian.prince@niger.com',
      country: 'RO',
      name: {
        givenNames: 'Nairobi',
        surname: 'Mogadishu',
      },
    },
  },
]
const formProps = {
  initialValues: {
    authors,
  },
}

describe('Wizard authors', () => {
  beforeEach(cleanup)
  it('should have no author list', () => {
    const props = {
      formProps: {
        initialValues: {
          authors: [],
        },
      },
      compProps: {
        journal: {
          code: 'TIT',
        },
      },
    }
    const { queryByTestId } = render(WizardAuthors, props)

    expect(queryByTestId(/author-list/i)).toBeNull()
  })

  it('should have all the authors passed by the form', () => {
    const { getByText } = render(WizardAuthors, {
      formProps,
      compProps: {
        journal: {
          code: 'TIT',
        },
      },
    })

    formProps.initialValues.authors.forEach(a =>
      expect(getByText(a.alias.email)).toBeInTheDocument(),
    )
  })

  it('should add a new author', () => {
    const setWizardEditModeMock = jest.fn()

    const props = {
      formProps,
      compProps: {
        setWizardEditMode: setWizardEditModeMock,
        journal: {
          code: 'TIT',
        },
      },
    }
    const { getByText, getByTestId } = render(WizardAuthors, props)

    fireEvent.click(getByText(/add author/i))
    expect(setWizardEditModeMock).toHaveBeenCalledTimes(1)
    expect(getByTestId('sortable-item-unsaved-author')).toBeInTheDocument()
  })

  it('should delete an author', () => {
    const onDeleteAuthorMock = jest.fn()

    const props = {
      formProps,
      compProps: {
        onDeleteAuthor: onDeleteAuthorMock,
        journal: {
          code: 'TIT',
        },
      },
    }

    const { getByTestId } = render(WizardAuthors, props)

    fireEvent.click(getByTestId(`delete-author-${1}`))
    expect(onDeleteAuthorMock).toHaveBeenCalledTimes(1)

    expect(onDeleteAuthorMock).toHaveBeenCalledWith(
      authors[1],
      expect.anything(),
    )
  })

  it('should not be able to delete the submitting author', () => {
    const props = {
      formProps,
      compProps: {
        journal: {
          code: 'TIT',
        },
      },
    }

    const { queryByTestId } = render(WizardAuthors, props)

    expect(queryByTestId(`delete-author-${authors[0].id}`)).toBeNull()
  })
  // addTODO: please revise this test in the near future
  // eslint-disable-next-line
  it.skip('should save a newly added author', async done => {
    const setWizardEditModeMock = jest.fn()
    const onSaveAuthorMock = jest.fn()
    const onEditAuthorMock = jest.fn()

    const newAuthor = {
      id: 'new-author',
      isSubmitting: false,
      isCorresponding: false,
      alias: {
        aff: 'BOR',
        email: 'prince@uzbekistan.com',
        country: 'RO',
        name: {
          givenNames: 'Great',
          surname: 'Firewall',
        },
      },
    }

    const props = {
      formProps: {
        initialValues: {
          isEditing: true,
          authors: [newAuthor, ...authors],
        },
      },
      compProps: {
        onSaveAuthor: onSaveAuthorMock,
        onEditAuthor: onEditAuthorMock,
        journal: {
          code: 'TIT',
        },
      },
    }
    const { getByTestId } = render(WizardAuthors, props)

    fireEvent.click(getByTestId(`save-author-2`)).first()

    setTimeout(() => {
      expect(setWizardEditModeMock).toHaveBeenCalledTimes(1)
      expect(onEditAuthorMock).toHaveBeenCalledTimes(1)
      expect(onSaveAuthorMock).toHaveBeenCalledWith(
        {
          aff: 'BOR',
          country: 'RO',
          email: 'prince@uzbekistan.com',
          givenNames: 'Great',
          id: 'new-author',
          isCorresponding: false,
          isSubmitting: false,
          surname: 'Firewall',
        },
        expect.anything(),
      )

      done()
    })
  })

  it('should edit an existing author', async done => {
    const onEditMock = jest.fn(() => {})
    const setWizardEditModeMock = jest.fn()
    const onSaveAuthorMock = jest.fn()

    const props = {
      formProps: {
        initialValues: {
          authors,
        },
      },
      compProps: {
        onEditAuthor: onEditMock,
        onSaveAuthor: onSaveAuthorMock,
        setWizardEditMode: setWizardEditModeMock,
        journal: {
          code: 'TIT',
        },
      },
    }
    const { getByTestId } = render(WizardAuthors, props)

    fireEvent.click(getByTestId(`edit-author-${0}`))

    fireEvent.change(getByTestId('givenNames-author'), {
      target: { value: 'New Name' },
    })

    fireEvent.click(getByTestId(`save-author-${0}`))

    expect(getByTestId(`givenNames-author`)).toBeTruthy()

    setTimeout(() => {
      expect(onSaveAuthorMock).toHaveBeenCalledTimes(0)
      expect(onEditMock).toHaveBeenCalledTimes(1)
      expect(onEditMock).toHaveBeenCalledWith(
        {
          id: '123',
          country: 'RO',
          aff: 'Stupidly Long Affiliation Name Here',
          affRorId: '',
          email: 'abcalongemailrightherebroshouldoverflowwithstyle@def.ghi',
          givenNames: 'New Name',
          isCorresponding: false,
          isSubmitting: true,
          surname: 'Bravo',
        },
        expect.anything(),
      )
      done()
    }, 10)
  })

  it('should toggle an author edit mode', () => {
    const setWizardEditModeMock = jest.fn()

    const props = {
      formProps: {
        initialValues: {
          authors,
        },
      },
      compProps: {
        setWizardEditMode: setWizardEditModeMock,
        journal: {
          code: 'TIT',
        },
      },
    }
    const { getByTestId } = render(WizardAuthors, props)

    fireEvent.click(getByTestId('edit-author-0'))
    expect(setWizardEditModeMock).toHaveBeenCalledTimes(1)

    fireEvent.click(getByTestId('cancel-edit-0'))
    expect(setWizardEditModeMock).toHaveBeenCalledTimes(2)
  })
})
