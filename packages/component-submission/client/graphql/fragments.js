import gql from 'graphql-tag'
import { fragments as fileFragments } from 'component-files/client'

export const teamMember = gql`
  fragment teamMember on TeamMember {
    id
    isSubmitting
    isCorresponding
    invited
    responded
    reviewerNumber
    user {
      id
    }
    status
    alias {
      aff
      affRorId
      email
      country
      name {
        surname
        givenNames
      }
    }
  }
`
export const specialIssue = gql`
  fragment specialIssue on SpecialIssue {
    id
    name
    callForPapers
    isActive
    endDate
    customId
  }
`

export const draftManuscriptDetails = gql`
  fragment draftManuscriptDetails on Manuscript {
    id
    journalId
    sourceJournalId
    sourceJournalManuscriptId
    linkedSubmissionCustomId
    isAuthorEmailEditable
    submissionId
    preprintValue
    meta {
      ... on ManuscriptMeta {
        title
        abstract
        agreeTc
        articleTypeId
        dataAvailability
        fundingStatement
        conflictOfInterest
      }
    }
    status
    authors {
      ...teamMember
    }
    files {
      ...fileDetails
    }
    specialIssue {
      ...specialIssue
    }
    section {
      id
      name
    }
  }
  ${specialIssue}
  ${teamMember}
  ${fileFragments.fileDetails}
`

export const activeJournals = gql`
  fragment activeJournals on Journal {
    id
    name
    code
    apc
    eissn
    pissn
    preprints {
      type
      format
    }
    preprintDescription
    articleTypes {
      id
      name
    }
    specialIssues {
      ...specialIssue
    }
    sections {
      id
      name
      customId
      specialIssues {
        ...specialIssue
      }
    }
  }
  ${specialIssue}
`

export const sourceJournals = gql`
  fragment sourceJournals on SourceJournal {
    id
    name
    eissn
    pissn
  }
`

export const articleTypes = gql`
  fragment articleTypes on ArticleType {
    id
    name
  }
`

export const submissionEditorialMapping = gql`
  fragment submissionEditorialMapping on SubmissionEditorialMapping {
    submissionId
    submissionEditorialModel {
      name
      hasEditorialAssistant
      hasTransfers
      hasPreprints
      hasCoverLetter
      hasSupplementalMaterials
      hasAbstract
      hasPrefilledTitle
      hasCOIDeclaration
      hasDataAvailabilityStatement
      hasFundingStatement
      hasTermsAndConditions
    }
  }
`

export const submissionEditorialModel = gql`
  fragment submissionEditorialModel on SubmissionEditorialModel {
    name
    hasTransfers
    hasEditorialAssistant
    hasPreprints
    hasCoverLetter
    hasSupplementalMaterials
    hasAbstract
    hasPrefilledTitle
    hasCOIDeclaration
    hasDataAvailabilityStatement
    hasFundingStatement
    hasTermsAndConditions
  }
`
