process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const { models, fixtures } = require('fixture-service')

const { Manuscript } = models
const { archiveManuscriptUseCase } = require('../src/use-cases')

const jobsService = { cancelJobs: jest.fn() }

describe('Archive manuscript use case', () => {
  it('archives a manuscript as admin', async () => {
    const manuscript = fixtures.generateManuscript({
      properties: {
        status: Manuscript.Statuses.academicEditorAssigned,
      },
      Manuscript,
    })
    await archiveManuscriptUseCase
      .initialize({ models, jobsService })
      .execute(manuscript.submissionId)

    expect(manuscript.status).toEqual(Manuscript.Statuses.deleted)
  })

  it('returns an error when deleting a draft manuscript', async () => {
    const manuscript = fixtures.generateManuscript({ Manuscript })
    const result = archiveManuscriptUseCase
      .initialize({ models, jobsService })
      .execute(manuscript.submissionId)

    return expect(result).rejects.toThrow(
      'Draft manuscripts cannot be deleted.',
    )
  })

  it('returns an error when deleting a manuscript already deleted', async () => {
    const manuscript = fixtures.generateManuscript({
      properties: {
        status: Manuscript.Statuses.deleted,
      },
      Manuscript,
    })
    const result = archiveManuscriptUseCase
      .initialize({ models, jobsService })
      .execute(manuscript.submissionId)

    return expect(result).rejects.toThrow('Manuscript already deleted.')
  })
})
