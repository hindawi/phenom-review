const config = require('config')
const Email = require('component-sendgrid')
const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')
const { logEvent } = require('component-activity-log/server')
const { getModifiedText } = require('component-transform-text')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const urlService = require('./urlService/urlService')
const notifications = require('./notifications/notifications')

const useCases = require('./use-cases')
const JobsService = require('./jobsService/jobsService')
const getProps = require('./emailPropsService/emailPropsService')
const getEmailCopy = require('./notifications/emailCopy')
const events = require('component-events')

const {
  useCases: {
    getManuscriptsUseCase: getManuscriptsModel,
    getJournalsUseCase: getJournalsV2UseCase,
    getSectionsUseCase,
    getSpecialIssuesUseCase,
    getStatusesManuscriptsCountUseCase,
  },
} = require('component-model')

const resolvers = {
  Query: {
    async getManuscripts(_, { input }, ctx) {
      return useCases.getManuscriptsUseCase
        .initialize(models)
        .execute({ input, userId: ctx.user })
    },
    async getManuscriptsV2(_, { input }, ctx) {
      const useCasesToPass = { getManuscriptsV2UseCase: getManuscriptsModel }
      return useCases.getManuscriptsV2UseCase
        .initialize(models)
        .execute({ input, userId: ctx.user, useCases: useCasesToPass })
    },
    async getManuscriptsSuggestions(_, { input }, ctx) {
      const useCasesToPass = { getManuscriptsV2UseCase: getManuscriptsModel }
      return useCases.getManuscriptsSuggestionsUseCase
        .initialize(models)
        .execute({ input, userId: ctx.user, useCases: useCasesToPass })
    },
    async getJournalsSuggestions(_, { input }, ctx) {
      const useCasesToPass = { getJournalsV2UseCase }
      return useCases.getJournalsSuggestionsUseCase
        .initialize(models)
        .execute({ input, userId: ctx.user, useCases: useCasesToPass })
    },
    async getSectionsSuggestions(_, { input }, ctx) {
      const useCasesToPass = { getSectionsUseCase }
      return useCases.getSectionsSuggestionsUseCase
        .initialize(models)
        .execute({ input, userId: ctx.user, useCases: useCasesToPass })
    },
    async getSpecialIssuesSuggestions(_, { input }, ctx) {
      const useCasesToPass = { getSpecialIssuesUseCase }
      return useCases.getSpecialIssuesSuggestionsUseCase
        .initialize(models)
        .execute({ input, userId: ctx.user, useCases: useCasesToPass })
    },
    async getStatusesManuscriptsCount(_, { input }, ctx) {
      const useCasesToPass = { getStatusesManuscriptsCountUseCase }
      return useCases.getStatusesManuscriptsCountUseCase
        .initialize(models)
        .execute({ input, userId: ctx.user, useCases: useCasesToPass })
    },
  },
  Mutation: {
    async deleteManuscript(_, { manuscriptId }, ctx) {
      return useCases.deleteManuscriptUseCase
        .initialize(models)
        .execute({ manuscriptId, userId: ctx.user })
    },
    async archiveManuscript(_, { submissionId }, ctx) {
      const jobsService = JobsService.initialize({ models })

      return useCases.archiveManuscriptUseCase
        .initialize({ models, jobsService })
        .execute(submissionId)
    },
    async withdrawManuscript(_, { submissionId }, ctx) {
      const jobsService = JobsService.initialize({ models })
      const eventsService = events.initialize({ models })
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = notifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
      })

      return useCases.withdrawManuscriptUseCase
        .initialize({
          models,
          jobsService,
          logEvent,
          eventsService,
          notificationService,
        })
        .execute({ submissionId, userId: ctx.user })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
