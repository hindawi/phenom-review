export const initialize = models => ({
  async execute({ input, useCases, userId }) {
    const { getJournalsV2UseCase } = useCases
    return getJournalsV2UseCase.initialize(models).execute({ input, userId })
  },
})

export const authsomePolicies = ['isAuthenticated']
