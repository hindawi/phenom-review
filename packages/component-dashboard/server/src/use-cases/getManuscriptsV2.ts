export const initialize = models => ({
  async execute({ input, useCases, userId }) {
    const { Manuscript } = models
    const { getManuscriptsV2UseCase } = useCases
    const queryResults = await getManuscriptsV2UseCase
      .initialize(models)
      .execute({ input, userId })

    const { results, total: totalCount } = queryResults
    const statuses = Object.values(Manuscript.Statuses) // to do: check if we do still need this?
    const manuscripts = results.map(m => m.toDTO()) // to do: remove once we create property resolvers
    return { manuscripts, totalCount, statuses }
  },
})

export const authsomePolicies = ['admin', 'refererIsSameAsOwner']
