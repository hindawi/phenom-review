const initialize = ({ Team, User, Manuscript, ArticleType }) => ({
  async execute({ userId, input = {} }) {
    const currentUser = await User.find(userId)

    let role
    const userIsAdmin = await currentUser.isAdmin()
    if (userIsAdmin) role = Team.Role.admin
    else {
      const userIsEditorialAssistant = await currentUser.isEditorialAssistant()
      if (userIsEditorialAssistant) role = Team.Role.editorialAssistant
    }

    const { results, total } = await Manuscript.getManuscripts({
      ...input,
      user: currentUser,
      Team,
      ArticleType,
      role,
    })

    const statuses = Object.values(Manuscript.Statuses)

    const manuscripts = results.map(m => m.toDTO())

    return { manuscripts, totalCount: total, statuses }
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
