export const initialize = models => ({
  async execute({ input, useCases, userId }) {
    const { getManuscriptsV2UseCase } = useCases
    const queryResults = await getManuscriptsV2UseCase
      .initialize(models)
      .execute({ input, userId })

    return queryResults.results
  },
})

export const authsomePolicies = ['isAuthenticated']
