const getManuscriptsUseCase = require('./getManuscripts')
const deleteManuscriptUseCase = require('./deleteManuscript')
const archiveManuscriptUseCase = require('./archiveManuscript')
const withdrawManuscriptUseCase = require('./withdrawManuscript')
const getJournalsSuggestionsUseCase = require('./getJournalsSuggestions')
const getSpecialIssuesSuggestionsUseCase = require('./getSpecialIssuesSuggestions')
const getSectionsSuggestionsUseCase = require('./getSectionsSuggestions')
const getManuscriptsSuggestionsUseCase = require('./getManuscriptsSuggestions')
const getManuscriptsV2UseCase = require('./getManuscriptsV2')
const getStatusesManuscriptsCountUseCase = require('./getStatusesManuscriptsCount')

module.exports = {
  getManuscriptsUseCase,
  deleteManuscriptUseCase,
  archiveManuscriptUseCase,
  withdrawManuscriptUseCase,
  getJournalsSuggestionsUseCase,
  getSpecialIssuesSuggestionsUseCase,
  getSectionsSuggestionsUseCase,
  getManuscriptsSuggestionsUseCase,
  getManuscriptsV2UseCase,
  getStatusesManuscriptsCountUseCase,
}
