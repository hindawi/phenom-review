export const initialize = models => ({
  async execute({ input, useCases, userId }) {
    const { getStatusesManuscriptsCountUseCase } = useCases
    return getStatusesManuscriptsCountUseCase
      .initialize(models)
      .execute({ input, userId })
  },
})

export const authsomePolicies = ['isAuthenticated']
