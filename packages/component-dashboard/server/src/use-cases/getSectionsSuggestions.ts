export const initialize = models => ({
  async execute({ input, useCases, userId }) {
    const { getSectionsUseCase } = useCases
    return getSectionsUseCase.initialize(models).execute({ input, userId })
  },
})

export const authsomePolicies = ['isAuthenticated']
