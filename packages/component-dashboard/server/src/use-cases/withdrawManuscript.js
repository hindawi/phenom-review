const { flatMap } = require('lodash')

const initialize = ({
  models: { Journal, Manuscript, Job, TeamMember, Team },
  logEvent,
  jobsService,
  eventsService,
  notificationService,
}) => ({
  async execute({ submissionId, userId }) {
    const draftManuscript = await Manuscript.findOneBySubmissionIdAndStatuses({
      submissionId,
      statuses: [Manuscript.Statuses.draft],
    })
    if (draftManuscript && draftManuscript.version === '1')
      throw new ValidationError('Manuscript cannot be withdrawn.')

    const nonWithdrawableManuscript = await Manuscript.findOneBySubmissionIdAndStatuses(
      {
        submissionId,
        statuses: [
          Manuscript.Statuses.rejected,
          Manuscript.Statuses.accepted,
          Manuscript.Statuses.deleted,
          Manuscript.Statuses.published,
        ],
      },
    )
    if (nonWithdrawableManuscript)
      throw new ValidationError('Manuscript cannot be withdrawn.')

    const withdrawnManuscript = await Manuscript.findOneBySubmissionIdAndStatuses(
      {
        submissionId,
        statuses: [Manuscript.Statuses.withdrawn],
      },
    )
    if (withdrawnManuscript)
      throw new ValidationError('Manuscript already withdrawn.')

    const manuscripts = await Manuscript.findBy({ submissionId })
    manuscripts.forEach(m => (m.status = Manuscript.Statuses.withdrawn))
    await Manuscript.updateMany(manuscripts)

    // Jobs
    manuscripts.sort(Manuscript.compareVersion)
    const lastManuscript = manuscripts[manuscripts.length - 1]

    const reviewers = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: lastManuscript.id,
      role: Team.Role.reviewer,
      eagerLoadRelations: '[user, jobs]',
    })
    const academicEditors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: lastManuscript.id,
      role: Team.Role.academicEditor,
      eagerLoadRelations: 'jobs',
    })
    jobsService.cancelJobs([
      ...flatMap(reviewers, r => r.jobs),
      ...flatMap(academicEditors, ae => ae.jobs),
    ])

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: lastManuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    if (editorialAssistant) {
      const editorialAssistantJobs = await Job.findAllByTeamMember(
        editorialAssistant.id,
      )
      if (editorialAssistantJobs) {
        jobsService.cancelStaffMemberJobs({
          staffMemberJobs: editorialAssistantJobs,
          manuscriptId: lastManuscript.id,
        })
      }
    }

    // Emails, Logs, Events
    if (lastManuscript.hasPassedEqs) {
      const journal = await Journal.find(lastManuscript.journalId)
      const triageEditor = await TeamMember.findTriageEditor({
        TeamRole: Team.Role,
        manuscriptId: lastManuscript.id,
        journalId: lastManuscript.journalId,
        sectionId: lastManuscript.sectionId,
      })
      const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
        {
          role: Team.Role.academicEditor,
          manuscriptId: lastManuscript.id,
          status: TeamMember.Statuses.accepted,
        },
      )
      const authors = await TeamMember.findAllByManuscriptAndRole({
        manuscriptId: lastManuscript.id,
        role: Team.Role.author,
      })
      notificationService.notifyManuscriptMembers({
        manuscript: lastManuscript,
        journalName: journal.name,
        reviewers,
        authors,
        academicEditor,
        triageEditor,
        editorialAssistant,
      })
    }

    logEvent({
      userId,
      manuscriptId: lastManuscript.id,
      action: logEvent.actions.withdraw_requested,
      objectType: logEvent.objectType.manuscript,
      objectId: lastManuscript.id,
    })

    eventsService.publishSubmissionEvent({
      submissionId,
      eventName: 'SubmissionWithdrawn',
    })
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
