const { Promise } = require('bluebird')

module.exports.initialize = ({ models: { Job } }) => ({
  async cancelReviewersJobs({ reviewers }) {
    await Promise.each(reviewers, async reviewer => {
      try {
        if (reviewer.jobs) {
          await Promise.each(reviewer.jobs, async job => {
            await Job.cancel(job.id)
            return job.delete()
          })
        }
      } catch (e) {
        throw new Error(e)
      }
    })
  },
  async cancelJobs(jobs) {
    try {
      await Promise.each(jobs, async job => {
        await Job.cancel(job.id)
        return job.delete()
      })
    } catch (e) {
      throw new Error(e)
    }
  },
  async cancelStaffMemberJobs({ staffMemberJobs, manuscriptId }) {
    try {
      await Promise.each(staffMemberJobs, async job => {
        if (job.manuscriptId !== manuscriptId) return

        await Job.cancel(job.id)

        return job.delete()
      })
    } catch (e) {
      throw new Error(e)
    }
  },
})
