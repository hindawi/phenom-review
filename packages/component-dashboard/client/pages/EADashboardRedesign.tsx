import React, { useState } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Row, Col, Card, Radio } from '@hindawi/phenom-ui'
import { localStorageService } from 'component-localstorage-service'
import { useCurrentUser } from 'component-authentication/client'
import {
  EaDashboardFilters,
  SearchManuscriptsWidget,
  ManuscriptsList,
  ManuscriptsListHeader,
  Pagination,
  StatusFilters,
} from '../components'

import { useFiltersManager } from '../store/EADashboard/filters'

import { DashboardProvider } from '../store/EADashboard'
import {
  useManuscripts,
  searchManuscriptInputResolverI,
  useManuscriptsTotal,
} from '../store/EADashboard/manuscripts'
import { ManagerApiI, connect } from '../store/'
import { useStatusesCountManager } from '../store/EADashboard/stausesCount'

const statusFiltersWidth = 325

// #region styles
const Root = styled.div`
  padding: 0 calc(${th('gridUnit')} * 20);
  padding-top: calc(${th('gridUnit')} * 8);

  .ant-row {
    margin-bottom: 12px;
  }
`

const Content = styled.div`
  width: 100%;
`

const CardWithShadow = styled(Card)`
  box-shadow: 0px 4px 8px rgba(51, 51, 51, 0.11);
`

const RightCol = styled(Col)`
  max-width: calc(100% - ${statusFiltersWidth}px);
`

const StyledRadio = styled(Radio)`
  align-items: center;
`
const Title = styled.span`
  font-weight: 700;
  margin-bottom: 4px;
`
const Box = styled.div`
  border: 1px solid rgb(189, 189, 189);
  padding: 12px;
  border-radius: 6px;
`
// #endregion

export interface FiltersPropsI {
  manuscripts: ManagerApiI<any, searchManuscriptInputResolverI>
}

export const EaDashboard: React.FC = () => {
  const statusesCountManager = useStatusesCountManager()
  const manuscriptsManager = useManuscripts()
  const manuscriptTotalManager = useManuscriptsTotal()
  const currentUser = useCurrentUser()

  const userId = (currentUser as { id: string }).id
  const storage = localStorageService()
  const ROLE_KEY = 'role'
  const options = [
    { value: 'editorialAssistant', label: 'Assigned to me' },
    { value: 'submittingStaffMember', label: 'Submitted by me' },
  ]

  const [role, setRole] = useState(storage.get(ROLE_KEY) || options[0].value)

  const filtersManager = useFiltersManager({
    ...(role === 'editorialAssistant' && { editorialAssistantId: userId }),
    ...(role === 'submittingStaffMember' && {
      submittingStaffMemberId: userId,
    }),
    manuscripts: manuscriptsManager,
    statusesCount: statusesCountManager,
  })

  const onChange = e => {
    const { set: updateSelectedRole } = filtersManager.role
    updateSelectedRole(e.target.value)

    storage.set(ROLE_KEY, e.target.value)
    setRole(e.target.value)
  }

  return (
    <Root>
      <Row gutter={[12, 12]}>
        <Col span={24}>
          <SearchManuscriptsWidget editorialAssistantId={userId} />
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Content>
            <Row gutter={[12, 12]}>
              <Col flex={`${statusFiltersWidth}px`}>
                <CardWithShadow bordered={false}>
                  <Row>
                    <Title>Show manuscripts</Title>
                    <Box>
                      <Radio.Group
                        className="manuscripts-by-role-group"
                        defaultValue={storage.get(ROLE_KEY) || options[0].value}
                        onChange={onChange}
                      >
                        {options.map(option => (
                          <StyledRadio
                            className={`${option.value}-role`}
                            key={option.label}
                            value={option.value}
                          >
                            {option.label}
                          </StyledRadio>
                        ))}
                      </Radio.Group>
                    </Box>
                  </Row>
                  <StatusFilters
                    filters={filtersManager}
                    statusesCount={statusesCountManager}
                  />
                </CardWithShadow>
              </Col>
              <RightCol flex="auto">
                <CardWithShadow bordered={false}>
                  <EaDashboardFilters filters={filtersManager} />
                  <ManuscriptsListHeader filters={filtersManager} />
                  <ManuscriptsList
                    loading={manuscriptsManager.loading}
                    manuscripts={manuscriptsManager.value}
                  />
                  <Row justify="end">
                    <Pagination
                      filters={filtersManager}
                      manuscriptsTotalCount={manuscriptTotalManager.value}
                    />
                  </Row>
                </CardWithShadow>
              </RightCol>
            </Row>
          </Content>
        </Col>
      </Row>
    </Root>
  )
}

export default connect(DashboardProvider, EaDashboard)
