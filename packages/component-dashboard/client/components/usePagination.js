import { useState } from 'react'

const usePagination = (itemsPerPage, totalItems) => {
  const [page, setPage] = useState(0)

  const toFirst = () => {
    setPage(0)
  }

  const toLast = () => {
    const floor = Math.floor(totalItems / itemsPerPage)
    setPage(totalItems % itemsPerPage ? floor : floor - 1)
  }

  const nextPage = () => {
    setPage(page =>
      page * itemsPerPage + itemsPerPage < totalItems ? page + 1 : page,
    )
  }

  const prevPage = () => {
    setPage(page => Math.max(0, page - 1))
  }

  return {
    page,
    setPage,
    toLast,
    toFirst,
    prevPage,
    nextPage,
    pageSize: itemsPerPage,
  }
}

export default usePagination
