export { default as DashboardFilters } from './DashboardFilters'
export { default as ManuscriptCard } from './ManuscriptCard'
export { ManuscriptCard as ManuscriptCardV2 } from './ManuscriptCard/ManuscriptCardV2'

// decorators
export { default as withAcademicEditorStatus } from './withAcademicEditorStatus'

export { default as useFilters } from './useFilters'
export { default as usePagination } from './usePagination'

export { default as SearchManuscriptsWidget } from './ea-dashboard/SearchManuscriptsWidget/SearchManuscriptsWidget'
export { ManuscriptsList } from './ea-dashboard/ManuscriptsList/ManuscriptsList'
export { default as ManuscriptsListHeader } from './ea-dashboard/ManuscriptsList/ManuscriptListHeader'
export { default as EaDashboardFilters } from './ea-dashboard/Filters/Filters'
export { default as StatusFilters } from './ea-dashboard/StatusFilters/StatusFilters'
export { default as Pagination } from './ea-dashboard/Pagination/Pagination'
