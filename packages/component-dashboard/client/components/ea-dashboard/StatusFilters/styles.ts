import { Collapse as _Collapse, Checkbox, Space } from '@hindawi/phenom-ui'
import styled from 'styled-components'

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 26px;
`

export const Collapse = styled(_Collapse)`
  .anticon.ant-collapse-arrow {
    padding: 0 !important;
    top: 50% !important;
    transform: translateY(-50%) rotate(90deg) !important;
  }

  .anticon.ant-collapse-arrow.is-active {
    transform: translateY(-50%) rotate(-90deg) !important;
  }

  label {
    vertical-align: middle;
    display: block;
  }
`

export const Title = styled.span`
  font-weight: 700;
`

export const CheckboxGroup = styled(Checkbox.Group)`
  width: 100%;

  .ant-checkbox-wrapper.ant-checkbox-group-item {
    display: flex;
    align-items: center;
    margin-right: 0;

    & > span:last-child {
      width: 100%;
      padding-right: 0;

      & > span {
        display: flex;
        justify-content: space-between;
      }
    }
  }
`

export const CheckboxGroupWrapper = styled(Space)`
  width: 100%;
`

export const StyledLabel = styled.span`
  position: relative;

  .spinner-wrapper {
    svg {
      height: 20px;
      width: 20px;
    }
  }

  .count {
    color: #4f4f4f;
    font-size: 11px;
    font-weight: 700;
  }
`
