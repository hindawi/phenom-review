// this is a generic manager for querying manuscripts
import { useState, useEffect } from 'react'
import { useLazyQuery } from 'react-apollo'
import { useTracking } from 'react-tracking'
import { pickBy } from 'lodash'
import { queries } from '../../graphql'
// import { getVisibleStatusForManuscript } from './StatusFilters/statusCategories'

export type ManuscriptsI = {
  getManuscripts?: (params: any) => Promise<any>
  manuscripts?: Array<any>
  loading?: boolean
  totalCount?: number
  resetManuscripts?: () => void
}

interface searchManuscriptInputResolverI {
  editorialAssistantId?: string
  rangeStart?: number
  rangeEnd?: number
  customId?: string
  id?: string
  authorEmail?: string
  academicEditorEmail?: string
  academicEditorStatus?: string
  journalId?: string
  sectionId?: string
  specialIssueId?: string
  status?: string
}

interface SearchManuscriptFiltersParamsI {
  editorialAssistantId?: string
}

const timer = {
  startTime: 0,
  initStartTime: () => (timer.startTime = performance.now()),
  getElapsedTime: () => performance.now() - timer.startTime,
}

let usedFilters = []

export const useManuscriptsManager = (
  filtersParams: SearchManuscriptFiltersParamsI,
): ManuscriptsI => {
  const tracking = useTracking()
  const [manuscripts, setManuscripts] = useState([])
  const query = 'getManuscriptsV2'
  const [queryManuscripts, { data, loading }] = useLazyQuery(queries[query], {
    fetchPolicy: 'network-only',
    onCompleted: () => {
      tracking.trackEvent({
        userRole: 'Editorial Assistant',
        pageTemplate: 'Editorial Assitant Page',
        queryTime: timer.getElapsedTime(),
        filters: usedFilters,
      })

      usedFilters = []
    },
  })

  useEffect(() => {
    const fetchedManuscripts =
      data && data[query] && data[query].manuscripts
        ? data[query].manuscripts
        : []

    setManuscripts(
      fetchedManuscripts.map(manuscript => ({
        ...manuscript,
        // visibleStatus: getVisibleStatusForManuscript(manuscript.status),
      })),
    )
  }, [data])

  const totalCount =
    data && data[query] && data[query].totalCount ? data[query].totalCount : 0

  const getManuscripts = async ({
    editorialAssistantId,
    rangeStart = 0,
    rangeEnd = 10,
    customId,
    id,
    authorEmail,
    academicEditorEmail,
    academicEditorStatus,
    journalId,
    sectionId,
    specialIssueId,
    status,
  }: searchManuscriptInputResolverI) => {
    timer.initStartTime()

    usedFilters = Object.keys(
      pickBy(
        {
          customId,
          id,
          journalId,
          sectionId,
          specialIssueId,
          authorEmail,
          academicEditorEmail,
          status,
        },
        Boolean,
      ),
    )

    return queryManuscripts({
      variables: {
        input: {
          editorialAssistantId,
          customId,
          id,
          editorialAssistantStatus: 'active',
          rangeStart,
          rangeEnd,
          status: status || undefined,
          authorEmail: authorEmail || undefined,
          academicEditorEmail: academicEditorEmail || undefined,
          academicEditorStatus: academicEditorStatus || undefined,
          orderByDesc: 'created',
          isLatestVersion: true,
          journalId,
          sectionId,
          specialIssueId,
          ...filtersParams,
        },
      },
    })
  }

  const resetManuscripts = () => {
    setManuscripts([])
  }

  return {
    manuscripts,
    loading,
    totalCount,
    getManuscripts,
    resetManuscripts,
  }
}
