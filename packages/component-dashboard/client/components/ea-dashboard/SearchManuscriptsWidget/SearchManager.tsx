import React, { createContext, useContext } from 'react'
import { useCurrentUser } from 'component-authentication/client'
import { SuggestionsI, useSuggestionManager } from '../SuggestionsManager'
import { ManuscriptsI, useManuscriptsManager } from '../ManuscriptsManager'

interface SearchContextI {
  manuscriptSuggestionsSearch?: SuggestionsI
  manuscriptSearch?: ManuscriptsI
}

export const SearchContext: React.Context<SearchContextI> = createContext({})

export const SearchManager: React.FC = ({ children }) => {
  const currentUser = useCurrentUser()

  // Manuscripts suggestions manager
  const manuscriptSuggestionsSearch = useSuggestionManager({
    query: 'getManuscriptsSuggestions',
    inputResolver: (searchTerm: string) => ({
      editorialAssistantId: (currentUser as { id: string }).id,
      title: searchTerm,
      isLatestVersion: true,
    }),
  })

  // Top Manuscripts manager
  const manuscriptSearch = useManuscriptsManager({
    editorialAssistantId: (currentUser as { id: string }).id,
  })

  const context = {
    manuscriptSuggestionsSearch,
    manuscriptSearch,
  }

  return (
    <SearchContext.Provider value={context}>{children}</SearchContext.Provider>
  )
}

export const useSearch = () => useContext(SearchContext)
