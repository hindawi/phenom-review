// this manager is a generic one for autocomplete inputs
import { useState, useEffect, createContext, useContext } from 'react'
import { useLazyQuery } from 'react-apollo'
import { queries } from '../../graphql'

export type Option = {
  value: string
  displayValue: string
}

export type SuggestionsI = {
  selectedSuggestion?: { value: string | null; displayValue: string | null }
  selectSuggestion?: any
  suggestions?: SuggestionTypes[] | []
  loading?: boolean
  getSuggestions?: (searchTerm: string) => Promise<any>
  resetSuggestions?: () => void
}

export type Suggestion = {
  id: string
  name: string
}

export type ManuscriptSuggestion = {
  id: string
  title: string
  journal: { id: string; name: string }
}

export type SuggestionTypes = Suggestion | ManuscriptSuggestion

let timeout
const timeout_duration = 300

export const useSuggestionManager = ({
  query,
  inputResolver,
}): SuggestionsI => {
  const initialSelectedSuggestionState: Option = {
    value: null,
    displayValue: '',
  }

  const [suggestions, setSuggestions] = useState([])

  const [getQuery, { data, loading }] = useLazyQuery(queries[query], {
    fetchPolicy: 'network-only',
  })

  useEffect(() => {
    setSuggestions(data && data[query] ? data[query] : [])
  }, [data])

  const [selectedSuggestion, selectSuggestion] = useState(
    initialSelectedSuggestionState,
  )

  const getSuggestions = async (searchTerm: string) => {
    clearTimeout(timeout)
    timeout = setTimeout(
      () =>
        getQuery({
          variables: {
            input: inputResolver(searchTerm),
          },
        }),
      timeout_duration,
    )
  }

  const resetSuggestions = () => {
    selectSuggestion(initialSelectedSuggestionState)
    setSuggestions([])
  }

  return {
    suggestions,
    loading,
    selectedSuggestion,
    getSuggestions,
    selectSuggestion,
    resetSuggestions,
  }
}

const SuggestionContext: React.Context<SuggestionsI> = createContext({})

export const useSuggestions = () => useContext(SuggestionContext)
