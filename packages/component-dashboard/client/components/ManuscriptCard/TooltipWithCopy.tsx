import React from 'react'
import styled from 'styled-components'
import { Button as _Button, Typography, Space } from '@hindawi/phenom-ui'
import { Tooltip } from 'antd'
import './TooltipWithCopy.css'

const { Text } = Typography

const Button = styled(_Button)`
  background: #828282 !important;

  .ant-typography-copy {
    color: #fff !important;
  }
`
interface TooltipWithCopyProps {
  copyContent: string
  showCopyContent?: boolean
}

export const TooltipWithCopy: React.FC<TooltipWithCopyProps> = ({
  copyContent,
  children,
  showCopyContent,
}) => (
  <Tooltip
    color="#4f4f4f"
    title={
      <Space size="small">
        {showCopyContent && copyContent && <span>{copyContent}</span>}
        <Button size="small" type="secondary">
          <Text
            copyable={{
              text: copyContent,
              icon: ['Copy', 'Copied'],
              tooltips: false,
            }}
          />
        </Button>
      </Space>
    }
  >
    {children}
  </Tooltip>
)
