const capitalize = word => word[0].toUpperCase() + word.slice(1)

const statusToCategoryMap = {
  draft: 'inProgress',
  technicalChecks: 'inProgress',
  submitted: 'inProgress',
  academicEditorInvited: 'inProgress',
  academicEditorAssigned: 'inProgress',
  reviewersInvited: 'inProgress',
  underReview: 'inProgress',
  reviewCompleted: 'inProgress',
  revisionRequested: 'inProgress',
  pendingApproval: 'inProgress',
  rejected: 'rejected',
  inQA: 'inProgress',
  accepted: 'approved',
  withdrawalRequested: 'approved',
  withdrawn: 'withdrawn',
  published: 'approved',
  olderVersion: 'withdrawn',
  academicEditorAssignedEditorialType: 'inProgress',
  makeDecision: 'inProgress',
  qualityChecksRequested: 'inProgress',
  qualityChecksSubmitted: 'inProgress',
  refusedToConsider: 'rejected',
  void: 'withdrawn',
  deleted: 'withdrawn',
}

export const getStatusCategory = status =>
  `status${capitalize(statusToCategoryMap[status])}`
