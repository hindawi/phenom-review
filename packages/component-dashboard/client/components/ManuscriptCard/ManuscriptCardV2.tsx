import React, { useState, useEffect, Fragment } from 'react'
import {
  Row,
  Col,
  Typography,
  IconCaretDown,
  IconCaretUp,
  Space,
} from '@hindawi/phenom-ui'
import { DateParser } from '@pubsweet/ui'
import { StatusTag, AuthorTagList } from '@hindawi/ui' // we need to re-wrtie these
import { TooltipWithCopy } from './TooltipWithCopy'
import { Card, Content, Footer, Label } from './styles'
import { getStatusCategory } from './utils'

const { Text, Title, Link } = Typography

type MemberT = {
  id: string
  status: string
  alias: {
    email: string
    name: {
      givenNames: string
      surname: string
    }
  }
} | null

const stopPropagation = e => e.stopPropagation()
interface ManuscriptCardProps {
  manuscript: {
    customId: string
    meta: {
      title: string | undefined
    }
    journal?: {
      name: string | undefined
    }
    articleType?: {
      hasPeerReview: string | undefined
      name: string | undefined
    }
    version: string
    visibleStatus: string
    status: string
    updated: string
    submittedDate: string
    academicEditor: MemberT
    triageEditor: MemberT
    authors: MemberT[]
    specialIssue: { name: string }
    section: { name: string }
    reviewers: { status: string }[]
    editors: { status: string }[]
    expanded: boolean
    peerReviewModel?: {
      triageEditorLabel: string | undefined
      academicEditorLabel: string | undefined
    }
  }
  onClick: () => void
}

const renderEditorsCells = ({
  triageEditor,
  triageEditorLabel,
  academicEditor,
  academicEditorLabel,
}) =>
  [
    {
      title: triageEditorLabel,
      data: triageEditor,
    },
    {
      title: academicEditorLabel,
      data: academicEditor,
    },
  ].map(({ title, data }) => (
    <Col key={title} onClick={stopPropagation} span={4}>
      <Label>{title}</Label>
      <div>
        {!data ? (
          'Unassigned'
        ) : (
          <TooltipWithCopy copyContent={data.alias.email} showCopyContent>
            <Link>
              {data.alias.name.givenNames} {data.alias.name.surname}
            </Link>
          </TooltipWithCopy>
        )}
      </div>
    </Col>
  ))

const renderShowMore = ({ section, reviewers, editors, specialIssue }) => (
  <>
    <Row gutter={[16, 12]}>
      {section && (
        <Col xl={4} xxl={8}>
          <Label>Section</Label>
          <Text ellipsis>{section.name}</Text>
        </Col>
      )}
      <Col xl={10} xxl={8}>
        <Label>Editor Invites</Label>
        <div>
          <Space size={4}>
            <Label>{editors.length}</Label>
            <span>Invited,</span>
            <Label>
              {
                editors.filter(r => ['accepted', 'active'].includes(r.status))
                  .length
              }
            </Label>
            <span>Agreed,</span>
            <Label>{editors.filter(r => r.status === 'declined').length}</Label>
            <span>Declined,</span>
            <Label>{editors.filter(r => r.status === 'pending').length}</Label>
            <span>Pending</span>
          </Space>
        </div>
      </Col>
      <Col xl={10} xxl={8}>
        <Label>Reviewer Reports</Label>
        <div>
          <Space size={4}>
            <Label>{reviewers.length}</Label>
            <span>Invited,</span>
            <Label>
              {reviewers.filter(r => r.status === 'accepted').length}
            </Label>
            <span>Agreed,</span>
            <Label>
              {reviewers.filter(r => r.status === 'declined').length}
            </Label>
            <span>Declined,</span>
            <Label>
              {reviewers.filter(r => r.status === 'submitted').length}
            </Label>
            <span>Submitted</span>
          </Space>
        </div>
      </Col>
    </Row>
    {specialIssue && (
      <Row gutter={[16, 12]}>
        <Col>
          <Label>Special Issue</Label>
          <Text ellipsis>{specialIssue.name}</Text>
        </Col>
      </Row>
    )}
  </>
)

export const ManuscriptCard: React.FC<ManuscriptCardProps> = ({
  manuscript: {
    customId,
    meta: { title = 'No Title' },
    journal = {},
    articleType = {},
    version,
    visibleStatus,
    status,
    academicEditor,
    triageEditor,
    authors = [],
    submittedDate,
    updated,
    specialIssue,
    section,
    reviewers = [],
    editors = [],
    expanded,
    peerReviewModel = {},
  },
  onClick,
}) => {
  const [showMore, setShowMore] = useState(expanded)

  useEffect(() => {
    setShowMore(expanded)
  }, [expanded])

  reviewers = reviewers || []
  editors = editors || []

  return (
    <Card onClick={onClick} statusColor={getStatusCategory(status)}>
      <Content>
        <Row gutter={[16, 12]}>
          <Col flex="none" onClick={stopPropagation}>
            <Label>ID</Label>
            <TooltipWithCopy copyContent={customId}>
              <Link>{customId}</Link>
            </TooltipWithCopy>
          </Col>
          <Col className="col-oh" flex="auto">
            <Label>
              <Title ellipsis={{ rows: 1 }} level={5}>
                {title}
              </Title>
            </Label>
          </Col>
          <Col flex="none">
            <StatusTag
              hasVersion={articleType && articleType.hasPeerReview}
              statusColor={getStatusCategory(status)}
              version={version}
            >
              {visibleStatus}
            </StatusTag>
          </Col>
        </Row>

        <Row gutter={[16, 12]}>
          {journal && (
            <Fragment>
              <Col span={8}>
                <Label>Journal</Label>
                <Text ellipsis>{journal.name}</Text>
              </Col>

              {renderEditorsCells({
                triageEditor,
                triageEditorLabel: peerReviewModel.triageEditorLabel,
                academicEditor,
                academicEditorLabel: peerReviewModel.academicEditorLabel,
              })}
            </Fragment>
          )}
          {authors && authors.length && (
            <Col span={8}>
              <Label>Authors</Label>
              <AuthorTagList authors={authors} withTooltip />
            </Col>
          )}
        </Row>

        {showMore &&
          renderShowMore({ section, reviewers, editors, specialIssue })}

        <Row gutter={[16, 12]}>
          <Col>
            <div onClick={stopPropagation}>
              <Link onClick={() => setShowMore(!showMore)}>
                <Space>
                  {showMore ? <IconCaretUp /> : <IconCaretDown />}
                  <span>Show {showMore ? 'less' : 'more'} details</span>
                </Space>
              </Link>
            </div>
          </Col>
        </Row>
      </Content>

      <Footer>
        <Row gutter={[24, 0]}>
          {submittedDate && status !== 'draft' && (
            <DateParser humanizeThreshold={0} timestamp={submittedDate}>
              {(timestamp, timeAgo) => (
                <Col>
                  <Label> Submitted date </Label>
                  <Text>{`${timestamp} (${timeAgo} ago)`}</Text>
                </Col>
              )}
            </DateParser>
          )}

          {updated && (
            <DateParser humanizeThreshold={0} timestamp={updated}>
              {(timestamp, timeAgo) => (
                <Col>
                  <Label> Updated on </Label>
                  <Text>{`${timestamp} (${timeAgo} ago)`}</Text>
                </Col>
              )}
            </DateParser>
          )}

          {articleType && articleType.name && (
            <Col>
              <Label> Article Type </Label>
              <span>{articleType.name}</span>
            </Col>
          )}
        </Row>
      </Footer>
    </Card>
  )
}
