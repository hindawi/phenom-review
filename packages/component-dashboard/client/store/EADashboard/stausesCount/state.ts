import { useState } from 'react'

export type StatusCountT = {
  [status: string]: number
}

export const useStatusesCountState = () => {
  const [statusesManuscriptsCount, setStatusesCount] = useState({})

  const statuses = {
    value: statusesManuscriptsCount,
    set: setStatusesCount,
  }

  return statuses
}
