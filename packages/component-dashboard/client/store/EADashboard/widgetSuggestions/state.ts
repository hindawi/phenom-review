import { useState } from 'react'

export type ValueT = any[]

export const useSuggestionsState = () => {
  const [value, set] = useState([])
  return {
    value,
    set,
  }
}

export const useManuscriptSuggestionsState = () => useSuggestionsState()
