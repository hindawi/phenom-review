import { SetStateAction, Dispatch } from 'react'

export interface StateI<I> {
  value: I
  set: Dispatch<SetStateAction<I>>
}

export interface ManagerApiI<I, J> {
  value: I
  loading?: boolean
  fetch?: (props: J) => void
  set: (value: I) => void
  reset?: () => void
}
export interface ManagerPropsI<I> {
  context: StateI<I>
  query?: string
  inputResolver?: (any) => any
}

export type ManagerT<I, J> = (props: ManagerPropsI<I>) => ManagerApiI<I, J>
export * from './connect'
