import React from 'react'
import 'jest-styled-components'
import '@testing-library/jest-dom/extend-expect'
import { theme } from '@hindawi/ui'
import { Router } from 'react-router'
import { createBrowserHistory } from 'history'
import { cleanup, fireEvent, render as rtlRender } from '@testing-library/react'
import { ThemeProvider } from 'styled-components'

import ActivityLog from '../components/ActivityLog'

const history = createBrowserHistory()

const render = ui => {
  const Component = () => (
    <ThemeProvider theme={theme}>
      <Router history={history}>{ui}</Router>
    </ThemeProvider>
  )

  const utils = rtlRender(<Component />)

  return {
    ...utils,
    selectOption: value => {
      fireEvent.click(
        utils.container.querySelector(`[data-test-id="activity-log-filter"]`),
      )
      fireEvent.click(
        utils.container.querySelector(`[data-test-id="activity-log-${value}"]`),
      )
    },
  }
}

const events = [
  {
    version: '1',
    logs: [
      {
        id: '1',
        action: 'sent reviewer invite to ',
        user: {
          email: 'academicEditor@hindawieditorial.com',
          role: 'academicEditor',
        },
        target: {
          email: 'allie.bryan@gmail.com',
          role: null,
        },
        created: 1550629860963,
      },
      {
        id: '2',
        action: 'sent reviewer invite to',
        created: 1550629871097,
        user: {
          email: 'author@hindawieditorial.com',
          role: 'author',
        },
        target: {
          email: 'mike-knight@educentralmanchester.co.uk',
          role: null,
        },
      },
      {
        id: '3',
        action: 'sent reviewer invite to',
        created: 1550629880332,
        user: {
          email: 'triageEditor@hindawieditorial.com',
          role: 'triageEditor',
        },
        target: {
          email: 'charlie.bennett@gmail.com',
          role: null,
        },
      },
      {
        id: '4',
        action: 'accepted invitation to review',
        created: 1550629888928,
        user: {
          email: 'reviewer1@hindawi.com',
          role: 'reviewer',
        },
        target: { email: null, role: null },
      },
      {
        id: '5',
        action: 'sent automatic invitiation to review to',
        created: 1550629902715,
        user: { email: null, role: null },
        target: {
          email: 'curt.weber@facultyofamazingwork.com ',
          role: 'reviewer',
        },
      },
    ],
  },
]

const peerReviewModel = {
  id: 84,
  name: 'Chief Minus',
  hasTriageEditor: true,
  academicEditorLabel: 'Academic Editor',
  hasFigureheadEditor: false,
  triageEditorLabel: 'Chief Editor',
}

describe('Should filter events by role.', () => {
  afterEach(cleanup)

  it('Should filter by academic editor.', () => {
    const { selectOption, getByText } = render(
      <ActivityLog
        academicEditorLabel={peerReviewModel.academicEditorLabel}
        events={events}
        expanded
        peerReviewModel={peerReviewModel}
        triageEditorLabel={peerReviewModel.triageEditorLabel}
      />,
    )
    selectOption('academicEditor')
    expect(getByText('academicEditor@hindawieditorial.com')).toBeInTheDocument()
    expect(getByText('Academic Editor')).toBeInTheDocument()
    expect(getByText('AE')).toBeInTheDocument()
  })

  it('Should filter by Chief Editor', () => {
    const { selectOption, getByText } = render(
      <ActivityLog
        academicEditorLabel={peerReviewModel.academicEditorLabel}
        events={events}
        expanded
        peerReviewModel={peerReviewModel}
        triageEditorLabel={peerReviewModel.triageEditorLabel}
      />,
    )
    selectOption('triageEditor')
    expect(getByText('triageEditor@hindawieditorial.com')).toBeInTheDocument()
    expect(getByText('Chief Editor')).toBeInTheDocument()
    expect(getByText('CE')).toBeInTheDocument()
  })

  it('Should filter by reviewer.', () => {
    const { selectOption, getByText, getAllByText } = render(
      <ActivityLog
        academicEditorLabel={peerReviewModel.academicEditorLabel}
        events={events}
        expanded
        peerReviewModel={peerReviewModel}
        triageEditorLabel={peerReviewModel.triageEditorLabel}
      />,
    )
    selectOption('reviewer')
    expect(getByText('reviewer1@hindawi.com')).toBeInTheDocument()
    expect(getAllByText('Reviewer')[0]).toBeInTheDocument()
  })

  it('Should filter by author.', () => {
    const { selectOption, getByText } = render(
      <ActivityLog
        academicEditorLabel={peerReviewModel.academicEditorLabel}
        events={events}
        expanded
        peerReviewModel={peerReviewModel}
        triageEditorLabel={peerReviewModel.triageEditorLabel}
      />,
    )
    selectOption('author')
    expect(getByText('author@hindawieditorial.com')).toBeInTheDocument()
    expect(getByText('Author')).toBeInTheDocument()
    expect(getByText('SA')).toBeInTheDocument()
  })
  it('Should filter by all.', () => {
    const { selectOption, getByText, getAllByText } = render(
      <ActivityLog
        academicEditorLabel={peerReviewModel.academicEditorLabel}
        events={events}
        expanded
        peerReviewModel={peerReviewModel}
        triageEditorLabel={peerReviewModel.triageEditorLabel}
      />,
    )
    selectOption('all')

    expect(getByText('All')).toBeInTheDocument()
    expect(getByText('academicEditor@hindawieditorial.com')).toBeInTheDocument()
    expect(getByText('AE')).toBeInTheDocument()
    expect(getByText('reviewer1@hindawi.com')).toBeInTheDocument()
    expect(getAllByText('Reviewer')[0]).toBeInTheDocument()
    expect(getByText('author@hindawieditorial.com')).toBeInTheDocument()
    expect(getByText('System')).toBeInTheDocument()
    expect(getByText('SA')).toBeInTheDocument()
    expect(getByText('triageEditor@hindawieditorial.com')).toBeInTheDocument()
    expect(getByText('CE')).toBeInTheDocument()
  })
})
