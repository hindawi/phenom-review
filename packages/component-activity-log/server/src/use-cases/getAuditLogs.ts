// @ts-nocheck
const config = require('config')
const Promise = require('bluebird')

const activityLogEvents = config.get('activityLogEvents')

const initialize = ({ Manuscript, Identity, TeamMember, AuditLog }) => ({
  async execute(submissionId) {
    const auditLogs = await AuditLog.findAllBySubmissionId(submissionId)
    let logs = []

    await Promise.each(auditLogs, async log => {
      let userMember
      if (log.userId) {
        userMember = await TeamMember.findOneByManuscriptAndUser({
          userId: log.userId,
          manuscriptId: log.manuscriptId,
        })
      }
      let target
      if (log.objectType === 'user') {
        target = await AuditLog.getTarget({ target, TeamMember, log, Identity })
      }
      const newLog = log.transformLog({
        target,
        userMember,
        activityLogEvents,
      })
      logs = [...logs, newLog]
    })

    logs = logs.reduce((acc, current) => {
      // if the current log has a different version then acc, create a new object in the big array for the new version
      if (!acc.find(item => item.version === current.version)) {
        acc.push({ version: current.version, logs: [] })
      }
      // find the object for the current log version to push the log into array
      const logGroup = acc.find(item => item.version === current.version)
      logGroup.logs.push(current)
      return acc
    }, [])

    return logs.sort(Manuscript.compareVersion).reverse()
  },
})

const authsomePolicies = ['isEditorialAssistant', 'admin']

export = {
  initialize,
  authsomePolicies,
}
