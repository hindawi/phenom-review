const config = require('config')
const models = require('@pubsweet/models')

const logEvent = async logObj => {
  const { Manuscript, AuditLog, Section } = models
  const { manuscriptId, journalId } = logObj
  if (!journalId) {
    const manuscript = await Manuscript.find(manuscriptId)
    let { journalId } = manuscript
    if (manuscript.sectionId) {
      const section = await Section.find(manuscript.sectionId)
      ;({ journalId } = section)
    }
    logObj.journalId = journalId
  }
  const log = new AuditLog(logObj)
  await log.save()
}

logEvent.actions = Object.keys(config.get('activityLogEvents')).reduce(
  (acc, key) => ({
    ...acc,
    [key]: key,
  }),
  {},
)

logEvent.objectType = Object.keys(config.get('activityLogTypes')).reduce(
  (acc, key) => ({
    ...acc,
    [key]: key,
  }),
  {},
)

module.exports = {
  logEvent,
}
