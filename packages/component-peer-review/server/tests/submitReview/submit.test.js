// const Promise = require('bluebird')
const Chance = require('chance')
const {
  generateComment,
  generatePeerReviewModel,
  generateTeamMember,
  generateJournal,
  generateManuscript,
  generateReview,
  generateArticleType,
} = require('component-generators')
const {
  generatePeerReviewEditorialMapping,
} = require('component-generators/peerReviewEditorialMapping')
const { models } = require('fixture-service')

const { submitReviewUseCase } = require('../../src/useCases/submitReview')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  review_submitted: 'submitted a report',
}
logEvent.objectType = { manuscript: 'manuscript' }

const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

const validateFilesUseCase = {
  initialize: jest.fn(({ File }) => ({
    validateSubmissionFiles: jest.fn(),
    validateCommentFile: jest.fn(),
  })),
}
const fileValidator = validateFilesUseCase.initialize({ File })

const chance = new Chance()
describe('Submit review use case', () => {
  let notificationService = {}
  let jobsService = {}
  beforeEach(() => {
    notificationService = {
      notifyReviewerForSubmittedReport: jest.fn(),
      notifyReviewers: jest.fn(),
      notifyTriageEditor: jest.fn(),
      notifyAcademicEditor: jest.fn(),
      notifySubmittingAuthor: jest.fn(),
    }
    jobsService = {
      cancelJobs: jest.fn(),
      cancelStaffMemberJobs: jest.fn(),
    }
  })

  it('should validate comment file', async () => {
    const { Review, Comment, Manuscript, Journal, TeamMember, User } = models
    const peerReviewModel = generatePeerReviewModel({ approvalEditors: [] })
    const journal = generateJournal({ peerReviewModel })
    const manuscript = generateManuscript({
      status: 'underReview',
      journal,
      peerReviewModel,
      articleType: generateArticleType(),
      updateProperties: jest.fn(),
      save: jest.fn(),
    })
    const reviewer = generateTeamMember({
      updateProperties: jest.fn(),
      save: jest.fn(),
    })
    const author = generateTeamMember({
      getName: jest.fn(),
    })
    const review = generateReview({
      updateProperties: jest.fn(),
      save: jest.fn(),
    })
    const publicComment = generateComment({ type: Comment.Types.public })
    const privateComment = generateComment({
      type: Comment.Types.private,
      content: 'text',
    })
    jest.spyOn(Review, 'find').mockReturnValue(review)
    jest.spyOn(Comment, 'findOneByType').mockReturnValueOnce(publicComment)
    jest.spyOn(Manuscript, 'find').mockReturnValue(manuscript)
    jest.spyOn(Journal, 'find').mockReturnValue(journal)
    jest.spyOn(TeamMember, 'find').mockReturnValue(reviewer)
    jest.spyOn(Comment, 'findOneByType').mockReturnValueOnce(privateComment)
    jest.spyOn(TeamMember, 'findTriageEditor').mockResolvedValue({})
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue({})
    jest.spyOn(User, 'find').mockReturnValue('123')
    jest.spyOn(TeamMember, 'findSubmittingAuthor').mockReturnValue(author)

    await submitReviewUseCase
      .initialize({
        models,
        logEvent,
        jobsService,
        fileValidator,
        eventsService,
        notificationService,
      })
      .execute({
        reviewId: '123',
        userId: '321',
      })
    expect(fileValidator.validateCommentFile).toHaveBeenCalledTimes(1)
  })

  it('submits a review', async () => {
    const {
      Team,
      Review,
      Journal,
      Comment,
      Manuscript,
      TeamMember,
      User,
    } = models

    const peerReviewModel = generatePeerReviewModel({
      approvalEditors: [Team.Role.triageEditor],
    })
    const journal = generateJournal({ peerReviewModelId: peerReviewModel.id })
    journal.peerReviewModel = peerReviewModel

    const manuscript = generateManuscript({
      journalId: journal.id,
      status: Manuscript.Statuses.pendingApproval,
      articleType: generateArticleType(),
      updateProperties: jest.fn(),
      save: jest.fn(),
    })

    const peerReviewEditorialMapping = generatePeerReviewEditorialMapping({
      submissionId: manuscript.submissionId,
    })

    Object.assign(manuscript, { peerReviewEditorialMapping })

    const author = generateTeamMember()
    const reviewer = generateTeamMember({
      status: TeamMember.Statuses.accepted,
      updateProperties: jest.fn(),
      save: jest.fn(),
    })
    const reviewType = chance.pickone([
      Review.Recommendations.minor,
      Review.Recommendations.major,
      Review.Recommendations.reject,
      Review.Recommendations.publish,
    ])
    const review = generateReview({
      teamMemberId: reviewer.id,
      recommendation: reviewType,
      manuscriptId: manuscript.id,
      updateProperties: jest.fn(),
      save: jest.fn(),
    })
    const publicComment = generateComment({
      reviewId: review.id,
      type: Comment.Types.public,
      content: chance.sentence(),
    })
    const privateComment = generateComment({
      type: Comment.Types.private,
      reviewId: review.id,
      content: chance.sentence(),
    })

    jest.spyOn(Review, 'find').mockReturnValue(review)
    jest.spyOn(Comment, 'findOneByType').mockReturnValueOnce(publicComment)
    jest.spyOn(Comment, 'findOneByType').mockReturnValueOnce(privateComment)
    jest.spyOn(Manuscript, 'find').mockReturnValue(manuscript)
    jest.spyOn(TeamMember, 'find').mockReturnValue(reviewer)
    jest.spyOn(Journal, 'find').mockReturnValue(journal)
    jest.spyOn(TeamMember, 'findTriageEditor').mockReturnValue({})
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue({})
    jest.spyOn(User, 'find').mockReturnValue('123')
    jest.spyOn(TeamMember, 'findSubmittingAuthor').mockReturnValue(author)

    await submitReviewUseCase
      .initialize({
        models,
        logEvent,
        jobsService,
        fileValidator,
        eventsService,
        notificationService,
      })
      .execute({
        reviewId: review.id,
        userId: reviewer.userId,
      })
    expect(reviewType).toContain(review.recommendation)
    expect(notificationService.notifyAcademicEditor).toHaveBeenCalledTimes(1)
    expect(manuscript.updateProperties).toHaveBeenCalledWith({
      status: Manuscript.Statuses.reviewCompleted,
    })
  })
})
