import {
  generateJournal,
  generateManuscript,
  generateTeamMember,
  generateUser,
  getTeamRoles,
  getTeamMemberStatuses,
} from 'component-generators'

import { TemplateName } from '@hindawi/phenom-communications-types'

import {
  CommunicationInput,
  TemplateUsage,
} from '../../src/useCases/getCommunication/getCommunication'
import { responseToAcademicEditorInvitationUseCase } from '../../src/useCases/getCommunication/responseToAcademicEditorInvitationUseCase'

const shared = require('../../src/useCases/getCommunication/shared')

describe('reponseToAcademicEditorInvitationUseCase', () => {
  let models
  let data: CommunicationInput
  let responseMock

  const manuscript = generateManuscript({
    customId: 'asd',
    title: 'Manuscript Title',
  })
  const submittingAuthor = generateTeamMember()
  const journal = generateJournal({
    peerReviewModel: { hasTriageEditor: false },
  })
  const signedToken = 'signedToken'

  shared.getSignedToken = () => signedToken

  beforeAll(() => {
    models = {
      Manuscript: {
        findManuscriptsBySubmissionId: jest.fn(() => [manuscript]),
        findManuscriptByTeamMember: jest.fn(() => manuscript),
      },
      TeamMember: {
        findOneByManuscriptAndRoleAndStatus: jest.fn(() =>
          generateTeamMember(),
        ),
        findOneByManuscriptAndRole: jest.fn(() =>
          generateTeamMember({ getName: () => 'John Doe' }),
        ),
        findSubmittingAuthor: jest.fn(() => submittingAuthor),
        findAllByManuscriptAndRole: jest.fn(() => [
          generateTeamMember({ getName: () => 'John Doe' }),
          generateTeamMember({ getName: () => 'John Doi' }),
        ]),
        Statuses: getTeamMemberStatuses(),
      },
      Team: { Role: getTeamRoles() },
      Journal: { find: jest.fn(() => journal) },
    }

    data = {
      submissionId: '1234',
      usage: TemplateUsage.ACCEPTED_ACADEMIC_INVITATION,
      sender: generateUser({
        identities: [{ surname: 'test', givenNames: 'test' }],
      }),
    }

    responseMock = {
      mailInput: {
        manuscript: {
          comments: undefined,
          manuscriptId: manuscript.customId,
          manuscriptTitle: manuscript.title,
          journalName: journal.name,
          submittingAuthor: `${submittingAuthor.alias.givenNames} ${submittingAuthor.alias.surname}`,
        },
      },
      signedInput: signedToken,
    }
  })
  it('should return proper communication data when accepting academic editor invitation', async () => {
    responseMock.templateName = TemplateName.editorInvitationAccepted
    const response = await responseToAcademicEditorInvitationUseCase(
      data,
      models,
    )
    expect(response).toEqual(responseMock)
  })
  it('should return proper communication data when declining academic editor invitation', async () => {
    data.usage = TemplateUsage.DECLINED_ACADEMIC_INVITATION
    responseMock.templateName = TemplateName.editorDeclined
    const response = await responseToAcademicEditorInvitationUseCase(
      data,
      models,
    )
    expect(response).toEqual(responseMock)
  })
})
