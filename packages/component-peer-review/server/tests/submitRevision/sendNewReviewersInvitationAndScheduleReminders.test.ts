import {
  getTeamRoles,
  getTeamMemberStatuses,
  generateManuscript,
  generateJournal,
  generateUser,
} from 'component-generators'
import { generateTeamMember } from 'component-generators/teamMember'
import { sendNewReviewersInvitationAndScheduleRemindersUseCase } from '../../src/useCases/submitRevision'

const models = {
  Team: {
    Role: getTeamRoles(),
  },
  TeamMember: {
    findAllByManuscriptAndRole: jest.fn(),
    Statuses: getTeamMemberStatuses(),
  },
}
const { Team, TeamMember } = models
const logEvent = () => {}
logEvent.actions = {}
logEvent.objectType = {}

const emailJobsService = {
  scheduleEmailsWhenReviewerIsInvitedAfterMajorRevision: jest.fn(),
}
const invitationsService = {
  sendInvitationToReviewerAfterMajorRevision: jest.fn(),
}
const removalJobsService = {
  scheduleRemovalJob: jest.fn(),
}

describe('Send new reviewers invitation and schedule reminders use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('return undefined if we don t have reviewers on the new version', async () => {
    const journal = generateJournal()
    const draftManuscript = generateManuscript({ journalId: journal.id })
    const academicEditor = generateTeamMember()
    const editorialAssistant = generateTeamMember()
    const submittingAuthor = generateTeamMember()
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue(undefined)

    const result = await sendNewReviewersInvitationAndScheduleRemindersUseCase
      .initialize({
        models: { Team, TeamMember },
        logEvent,
        emailJobsService,
        invitationsService,
        removalJobsService,
      })
      .execute(
        draftManuscript,
        journal,
        academicEditor,
        editorialAssistant,
        submittingAuthor,
      )

    expect(result).toBeUndefined()
  })
  it('send invitations to new reviewers if they are subscribed', async () => {
    const journal = generateJournal()
    const draftManuscript = generateManuscript({ journalId: journal.id })
    const academicEditor = generateTeamMember()
    const editorialAssistant = generateTeamMember()
    const submittingAuthor = generateTeamMember({ getName: jest.fn() })
    const reviewerUser = generateUser({ isSubscribedToEmails: true })
    const reviewer = generateTeamMember({ userId: reviewerUser.id })
    reviewer.user = reviewerUser
    const author = generateTeamMember()

    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValueOnce([reviewer])

    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValueOnce([author])

    await sendNewReviewersInvitationAndScheduleRemindersUseCase
      .initialize({
        models: { Team, TeamMember },
        logEvent,
        emailJobsService,
        invitationsService,
        removalJobsService,
      })
      .execute(
        draftManuscript,
        journal,
        academicEditor,
        editorialAssistant,
        submittingAuthor,
      )

    expect(
      emailJobsService.scheduleEmailsWhenReviewerIsInvitedAfterMajorRevision,
    ).toBeCalled()
    expect(removalJobsService.scheduleRemovalJob).toBeCalled()
  })
  it('do not send invitations to new reviewers if they are not subscribed', async () => {
    const journal = generateJournal()
    const draftManuscript = generateManuscript({ journalId: journal.id })
    const academicEditor = generateTeamMember()
    const editorialAssistant = generateTeamMember()
    const submittingAuthor = generateTeamMember({ getName: jest.fn() })
    const reviewerUser = generateUser({ isSubscribedToEmails: false })
    const reviewer = generateTeamMember({ userId: reviewerUser.id })
    reviewer.user = reviewerUser
    const author = generateTeamMember()

    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValueOnce([reviewer])

    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValueOnce([author])

    await sendNewReviewersInvitationAndScheduleRemindersUseCase
      .initialize({
        models: { Team, TeamMember },
        logEvent,
        emailJobsService,
        invitationsService,
        removalJobsService,
      })
      .execute(
        draftManuscript,
        journal,
        academicEditor,
        editorialAssistant,
        submittingAuthor,
      )

    expect(
      emailJobsService.scheduleEmailsWhenReviewerIsInvitedAfterMajorRevision,
    ).toBeCalledTimes(0)
  })
})
