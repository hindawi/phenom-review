const Chance = require('chance')

const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { Manuscript, Team, Review } = models
const useCases = require('../../src/useCases/submitRevision')

const { updateDraftRevisionUseCase } = useCases

const chance = new Chance()

describe('Update Draft Version Use Case', () => {
  it('changes the comment info', async () => {
    const manuscript = fixtures.generateManuscript({
      properties: {
        version: '1',
        submissionId: chance.guid(),
      },
      Manuscript,
    })

    const submittingAuthor = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    const responseToRevision = chance.sentence()
    const input = {
      meta: {
        title: chance.word(),
        abstract: chance.paragraph(),
      },
      authors: [],
      content: responseToRevision,
    }
    const review = await dataService.createReviewOnManuscript({
      models,
      fixtures,
      manuscript,
      recommendation: Review.Recommendations.responseToRevision,
      teamMember: submittingAuthor,
    })

    await updateDraftRevisionUseCase.initialize(models).execute({
      manuscriptId: manuscript.id,
      autosaveInput: input,
    })

    expect(review.comments[0].content).toEqual(responseToRevision)
  })
})
