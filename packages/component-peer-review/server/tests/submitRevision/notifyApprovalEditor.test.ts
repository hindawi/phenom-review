import {
  getTeamRoles,
  getTeamMemberStatuses,
  generateManuscript,
  generateJournal,
} from 'component-generators'
import { generateTeamMember } from 'component-generators/teamMember'
import { notifyApprovalEditorUseCase } from '../../src/useCases/submitRevision'

const models = {
  Team: {
    Role: getTeamRoles(),
  },
  TeamMember: {
    findOneByManuscriptAndRoleAndStatus: jest.fn(),
    Statuses: getTeamMemberStatuses(),
  },
}
const notificationService = {
  notifyTriageEditor: jest.fn(),
  notifyAcademicEditor: jest.fn(),
}
const { Team, TeamMember } = models

describe('Notify Approval Editor Use Case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('if academic editor is approval editor and none are assigned to the manuscript, no one is notified', async () => {
    const approvalEditorRole = Team.Role.academicEditor
    const journal = generateJournal()
    const draftManuscript = generateManuscript({ journalId: journal.id })
    const academicEditor = undefined
    const editorialAssistant = generateTeamMember()
    const submittingAuthor = generateTeamMember()

    await notifyApprovalEditorUseCase
      .initialize({
        models: { Team, TeamMember },
        notificationService,
      })
      .execute(
        approvalEditorRole,
        draftManuscript,
        journal,
        academicEditor,
        editorialAssistant,
        submittingAuthor,
      )

    expect(notificationService.notifyAcademicEditor).toBeCalledTimes(0)
    expect(notificationService.notifyTriageEditor).toBeCalledTimes(0)
  })
  it('notify triage editor if he is the approval editor', async () => {
    const approvalEditorRole = Team.Role.triageEditor
    const journal = generateJournal()
    const draftManuscript = generateManuscript({ journalId: journal.id })
    const academicEditor = undefined
    const editorialAssistant = generateTeamMember()
    const submittingAuthor = generateTeamMember()
    const triageEditor = generateTeamMember({
      status: TeamMember.Statuses.active,
    })

    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue(triageEditor)

    await notifyApprovalEditorUseCase
      .initialize({
        models: { Team, TeamMember },
        notificationService,
      })
      .execute(
        approvalEditorRole,
        draftManuscript,
        journal,
        academicEditor,
        editorialAssistant,
        submittingAuthor,
      )

    expect(notificationService.notifyAcademicEditor).toBeCalledTimes(0)
    expect(notificationService.notifyTriageEditor).toBeCalledTimes(1)
  })
  it('notify academic editor if he is the approval editor', async () => {
    const approvalEditorRole = Team.Role.academicEditor
    const journal = generateJournal()
    const draftManuscript = generateManuscript({ journalId: journal.id })
    const academicEditor = generateTeamMember({
      status: TeamMember.Statuses.accepted,
    })
    const editorialAssistant = generateTeamMember()
    const submittingAuthor = generateTeamMember()

    await notifyApprovalEditorUseCase
      .initialize({
        models: { Team, TeamMember },
        notificationService,
      })
      .execute(
        approvalEditorRole,
        draftManuscript,
        journal,
        academicEditor,
        editorialAssistant,
        submittingAuthor,
      )

    expect(notificationService.notifyAcademicEditor).toBeCalledTimes(1)
    expect(notificationService.notifyTriageEditor).toBeCalledTimes(0)
  })
})
