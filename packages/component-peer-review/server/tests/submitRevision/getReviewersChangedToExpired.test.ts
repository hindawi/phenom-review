import {
  getTeamRoles,
  getTeamMemberStatuses,
  generateTeam,
  generateTeamMember,
} from 'component-generators'
import { getReviewersChangedToExpiredUseCase } from '../../src/useCases/submitRevision'

const models = {
  Team: {
    Role: getTeamRoles(),
  },
  TeamMember: {
    Statuses: getTeamMemberStatuses(),
  },
}

const { TeamMember } = models

describe('Get Reviewers Changed to Expired Use Case', () => {
  beforeEach(() => {})
  it('Returns reviewers with changed status', async () => {
    const team = generateTeam()
    const teamMember = generateTeamMember({ teamId: team.id })
    const result = await getReviewersChangedToExpiredUseCase
      .initialize({
        TeamMember,
      })
      .execute([teamMember])

    expect(result[0].status).toEqual(TeamMember.Statuses.expired)
  })
  it('Returns empty array if the argument is also an empty array', async () => {
    const result = await getReviewersChangedToExpiredUseCase
      .initialize({
        TeamMember,
      })
      .execute([])

    expect(result.length).toEqual(0)
  })
})
