const {
  getTeamRoles,
  generateJournal,
  generateManuscript,
  getRecommendations,
  generateTeamMember,
  generateArticleType,
  getTeamMemberStatuses,
  getManuscriptStatuses,
  generatePeerReviewModel,
  getManuscriptNonActionableStatuses,
} = require('component-generators')

const {
  makeDecisionToPublishUseCase,
} = require('../../src/useCases/makeDecision')

let models = {}
let Journal = {}
let Manuscript = {}
let Team = {}
let TeamMember = {}

const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

const logger = {
  error: jest.fn(),
}

const trx = {
  commit: jest.fn(),
  rollback: jest.fn(),
}

const transaction = {
  start: jest.fn(() => trx),
}

const jobsService = {
  cancelJobs: jest.fn(),
}

const logEvent = jest.fn(async () => {})
logEvent.actions = {
  manuscript_accepted: 'manuscript_accepted',
}
logEvent.objectType = { manuscript: 'manuscript' }

const notificationService = {
  notifyEQA: jest.fn(),
  notifyTriageEditorForSubmittedReport: jest.fn(),
  notifyAcademicEditorForSubmittedReport: jest.fn(),
}

describe('Make decision to publish', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    models = {
      Journal: {
        find: jest.fn(),
      },
      Manuscript: {
        knex: jest.fn(),
        find: jest.fn(),
        Statuses: getManuscriptStatuses(),
        NonActionableStatuses: getManuscriptNonActionableStatuses(),
      },
      Team: {
        Role: getTeamRoles(),
      },
      TeamMember: {
        findOneByRole: jest.fn(),
        findTriageEditor: jest.fn(),
        findSubmittingAuthor: jest.fn(),
        findOneByJournalAndUser: jest.fn(),
        findAllByManuscriptAndRole: jest.fn(),
        findOneByManuscriptAndUser: jest.fn(),
        findAllByManuscriptAndRoleAndStatus: jest.fn(),
        findOneByManuscriptAndRoleAndStatus: jest.fn(),
        Statuses: getTeamMemberStatuses(),
      },
      Job: {
        findAllByTeamMembers: jest.fn(),
        findAllByTeamMemberAndManuscript: jest.fn(),
      },
      Review: jest.fn(), // mock constructor
    }
    models.Review.Recommendations = getRecommendations()
    ;({ Journal, Manuscript, Team, TeamMember } = models)
  })
  it('Throws an error if the manuscript is in a wrong status', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.published,
    })
    const editor = generateTeamMember({
      userId: 'editor-id',
    })

    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)

    const res = makeDecisionToPublishUseCase.initialize({ models }).execute({
      manuscriptId: manuscript.id,
      userId: editor.id,
      content: 'Manuscript rejected',
    })

    return expect(res).rejects.toThrow(
      'Cannot publish a manuscript in the current status.',
    )
  })
  it('Executes the main flow', async () => {
    const peerReviewModel = generatePeerReviewModel({
      approvalEditors: [Team.Role.triageEditor, Team.Role.academicEditor],
    })
    const journal = generateJournal({
      id: 'journal-id',
      peerReviewModel,
    })
    const articleType = generateArticleType()
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.makeDecision,
      articleType,
      $query: jest.fn(() => ({
        updateAndFetch: jest.fn(() => manuscript),
      })),
      $relatedQuery: jest.fn(() => ({
        insert: jest.fn(),
      })),
    })
    const editor = generateTeamMember({
      userId: 'editor-id',
    })
    const editorialAssistant = generateTeamMember({
      id: 'editorial-assistant-id',
    })

    jest.spyOn(Journal, 'find').mockResolvedValue(journal)
    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndUser')
      .mockResolvedValue(editor)
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRoleAndStatus')
      .mockResolvedValue([])
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue(editorialAssistant)
    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue([])

    await makeDecisionToPublishUseCase
      .initialize({
        models,
        logger,
        logEvent,
        transaction,
        jobsService,
        eventsService,
        notificationService,
      })
      .execute({
        manuscriptId: manuscript.id,
        userId: editor.userId,
        content: 'Accepting manuscript',
      })

    expect(trx.commit).toBeCalledTimes(1)
    expect(trx.rollback).toBeCalledTimes(0)
  })
})
