const {
  getReasonsForRejectionUseCase,
} = require('../../../src/useCases/makeDecision')

const models = {
  RejectDecisionInfo: {
    ReasonsForRejection: {
      OUT_OF_SCOPE: 'outOfScope',
      TECHNICAL_OR_SCIENTIFIC_FLAWS: 'technicalOrScientificFlaws',
      PUBLICATIONS_ETHICS_CONCERNS: 'publicationEthicsConcerns',
      LACK_OF_NOVELTY: 'lackOfNovelty',
      OTHER_REASONS: 'otherReasons',
    },
  },
}

describe('getReasonsForRejection use-case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Should return default reason if config variable is not defined', async () => {
    const journalId = '123'
    const config = {
      get: jest.fn(() => null),
    }

    const result = await getReasonsForRejectionUseCase
      .initialize({ config, models })
      .execute({ journalId })

    expect(result).toStrictEqual([
      'outOfScope',
      'technicalOrScientificFlaws',
      'publicationEthicsConcerns',
      'otherReasons',
    ])
  })

  it('Should return default reason if journalId is not included in the config variable', async () => {
    const journalId = '123'
    const config = {
      get: jest.fn(data => {
        if (data === 'journalIdsForOptionalRejectReason') return '456 789'
        return null
      }),
    }

    const result = await getReasonsForRejectionUseCase
      .initialize({ config, models })
      .execute({ journalId })

    expect(result).toStrictEqual([
      'outOfScope',
      'technicalOrScientificFlaws',
      'publicationEthicsConcerns',
      'otherReasons',
    ])
  })

  it('Should return all the reasons if journalId is included in the config variable', async () => {
    const journalId = '123'
    const config = {
      get: jest.fn(data => {
        if (data === 'journalIdsForOptionalRejectReason') return '123 456 789'
        return null
      }),
    }

    const result = await getReasonsForRejectionUseCase
      .initialize({ config, models })
      .execute({ journalId })

    expect(result).toStrictEqual([
      'outOfScope',
      'technicalOrScientificFlaws',
      'publicationEthicsConcerns',
      'lackOfNovelty',
      'otherReasons',
    ])
  })
})
