const {
  getTeamRoles,
  getCommentTypes,
  generateJournal,
  generateManuscript,
  generateTeamMember,
  getRecommendations,
  generateArticleType,
  getManuscriptStatuses,
  getTeamMemberStatuses,
  generatePeerReviewModel,
} = require('component-generators')

const {
  rejectWithPeerReviewUseCase,
} = require('../../../src/useCases/makeDecision')

let models = {}
let Journal = {}
let Manuscript = {}
let Team = {}
let TeamMember = {}

const logger = {
  error: jest.fn(),
}

const trx = {
  commit: jest.fn(),
  rollback: jest.fn(),
}

const transaction = {
  start: jest.fn(() => trx),
}

const jobsService = {
  cancelJobs: jest.fn(),
}

const notifyAcademicEditor = jest.fn()
const notificationService = {
  notifyTriageEditorForSubmittedReport: jest.fn(),
  notifyAuthorsNoPeerReview: jest.fn(),
}

describe('Make decision to reject when article has no peer review', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    models = {
      Journal: {
        find: jest.fn(),
      },
      Manuscript: {
        find: jest.fn(),
        knex: jest.fn(),
        Statuses: getManuscriptStatuses(),
      },
      Team: {
        Role: getTeamRoles(),
      },
      TeamMember: {
        findTriageEditor: jest.fn(),
        findAllByManuscriptAndRole: jest.fn(),
        findOneByManuscriptAndRoleAndStatus: jest.fn(),
        findAllByManuscriptAndRoleAndStatus: jest.fn(),
        Statuses: getTeamMemberStatuses(),
      },
      Job: {
        findAllByTeamMembers: jest.fn(),
        findAllByTeamMemberAndManuscript: jest.fn(),
      },
      Review: jest.fn(), // mock constructor
      Comment: jest.fn(), // mock constructor
    }
    models.Review.Recommendations = getRecommendations()
    models.Comment.Types = getCommentTypes()
    ;({ Journal, Manuscript, Team, TeamMember } = models)
  })
  it('Executes the main flow', async () => {
    const peerReviewModel = generatePeerReviewModel({
      approvalEditors: [Team.Role.triageEditor],
    })
    const journal = generateJournal({
      id: 'journal-id',
      peerReviewModel,
    })
    const articleType = generateArticleType()
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.published,
      articleType,
      $query: jest.fn(() => ({
        updateAndFetch: jest.fn(() => manuscript),
      })),
      $relatedQuery: jest.fn(() => ({
        insert: jest.fn(),
        insertAndFetch: jest.fn(() => manuscript),
      })),
    })
    const editor = generateTeamMember({
      userId: 'editor-id',
    })

    jest.spyOn(Journal, 'find').mockResolvedValue(journal)
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue({})
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRoleAndStatus')
      .mockResolvedValue([])
    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue([])

    await rejectWithPeerReviewUseCase
      .initialize({
        models,
        transaction,
        logger,
        jobsService,
        notificationService,
        notifyAcademicEditor,
      })
      .execute({
        manuscript,
        rejectEditor: editor,
        content: 'Rejecting manuscript',
      })

    expect(trx.commit).toBeCalledTimes(1)
    expect(trx.rollback).toBeCalledTimes(0)
  })
})
