import {
  getTeamRoles,
  getTeamMemberStatuses,
  generateManuscript,
  generateTeam,
  generateTeamMember,
} from 'component-generators'

import { cancelJobsWhenMinimumNumberOfReviewersIsMetUseCase } from '../../src/useCases/inviteReviewers'

const mockModels = {
  Team: {
    Role: getTeamRoles(),
    findOrCreate: jest.fn(),
  },
  TeamMember: {
    Statuses: getTeamMemberStatuses(),
    findAllByManuscriptAndRole: jest.fn(),
  },
  Job: {
    findAllByTeamMember: jest.fn(),
  },
}
const { Team, TeamMember, Job } = mockModels
const jobsService = {
  cancelJobs: jest.fn(),
  cancelStaffMemberJobs: jest.fn(),
}

describe('Cancel jobs when mininum number of reviewers is met use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('returns early if the number of reviewers is smaller than the minimum number of reviewers', async () => {
    const manuscript = generateManuscript()
    const team = generateTeam({
      role: Team.Role.reviewer,
    })
    const reviewer = generateTeamMember({
      teamId: team.id,
      status: TeamMember.Statuses.pending,
    })

    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([reviewer])

    await cancelJobsWhenMinimumNumberOfReviewersIsMetUseCase
      .initialize({ models: { Job, TeamMember }, jobsService })
      .execute({ id: '1' }, { id: '2' }, manuscript.id, Team.Role.reviewer, 2)

    expect(jobsService.cancelJobs).not.toHaveBeenCalled()
  })
  it('cancels jobs if the number of reviewers is greater or equal to the minimum number of reviewers', async () => {
    const manuscript = generateManuscript()
    const team = generateTeam({
      role: Team.Role.reviewer,
    })
    const reviewer = generateTeamMember({
      teamId: team.id,
      status: TeamMember.Statuses.pending,
    })

    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([reviewer])

    await cancelJobsWhenMinimumNumberOfReviewersIsMetUseCase
      .initialize({ models: { Job, TeamMember }, jobsService })
      .execute({ id: '1' }, { id: '2' }, manuscript.id, Team.Role.reviewer, 1)

    expect(jobsService.cancelJobs).toHaveBeenCalled()
    expect(jobsService.cancelStaffMemberJobs).toHaveBeenCalled()
  })
})
