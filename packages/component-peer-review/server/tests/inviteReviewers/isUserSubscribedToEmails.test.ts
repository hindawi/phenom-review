import { generateUser } from 'component-generators'

const {
  isUserSubscribedToEmailsUseCase,
} = require('../../src/useCases/inviteReviewers')

const models = {
  User: {
    findOneByEmail: jest.fn(),
  },
}

const { User } = models

describe('isUserSubscribedToEmails', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('returns true if the user cannot be found', async () => {
    const userId = 'invalid-user-id'

    jest.spyOn(User, 'findOneByEmail').mockResolvedValue(undefined)

    const res = await isUserSubscribedToEmailsUseCase
      .initialize(models)
      .execute(userId)
    return expect(res).toEqual(true)
  })
  it('returns a correct response for subscribed users', async () => {
    const user = generateUser({ isSubscribedToEmails: true })

    jest.spyOn(User, 'findOneByEmail').mockResolvedValue(user)

    const res = await isUserSubscribedToEmailsUseCase
      .initialize(models)
      .execute(user.id)

    expect(res).toEqual(true)
  })
  it('returns a correct response for unsubscribed users', async () => {
    const user = generateUser({ isSubscribedToEmails: false })

    jest.spyOn(User, 'findOneByEmail').mockResolvedValue(user)

    const res = await isUserSubscribedToEmailsUseCase
      .initialize(models)
      .execute(user.id)

    expect(res).toEqual(false)
  })
})
