import { generateTeamMember, generateUser } from 'component-generators'
import { findOrCreateReviewerUserUseCase } from '../../src/useCases/inviteReviewers'

const mockUser = generateUser()
class mockUserModel {
  constructor(args) {
    return {
      id: 'user-id',
      save() {
        return this
      },
      ...args,
    }
  }
  static findOneByEmail() {
    return mockUser
  }
}
class mockIdentityModel {
  constructor(args) {
    return {
      id: 'identity-id',
      save() {
        return this
      },
      ...args,
    }
  }
}
const mockModels = {
  User: mockUserModel,
  Identity: mockIdentityModel,
}
const { User, Identity } = mockModels

describe('Find or create reviewer user use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('returns the user without creating a new one if one is found', async () => {
    const user = generateUser()
    jest.spyOn(User, 'findOneByEmail').mockResolvedValue(user)
    const { alias: input } = generateTeamMember()

    await findOrCreateReviewerUserUseCase
      .initialize(Identity, User)
      .execute(input)

    expect(user.save).not.toHaveBeenCalled()
  })
})
