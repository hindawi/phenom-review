const {
  generateUser,
  getTeamRoles,
  generateManuscript,
  generateTeamMember,
  getManuscriptStatuses,
  getTeamMemberStatuses,
} = require('component-generators')

const logEvent = jest.fn(async () => {})
logEvent.actions = {
  invitation_revoked: 'revoked invitation sent to',
}
logEvent.objectType = { user: 'user' }

const { cancelReviewerInvitationUseCase } = require('../../src/useCases')

const notificationService = {
  notifyReviewer: jest.fn(),
}

const jobsService = {
  cancelJobs: jest.fn(),
}

const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

const logger = {
  error: jest.fn(),
}

const trx = {
  commit: jest.fn(),
  rollback: jest.fn(),
}

const transaction = {
  start: jest.fn(() => trx),
}

const models = {
  Job: {
    findAllByTeamMember: jest.fn(),
  },
  Journal: {
    find: jest.fn(),
  },
  Manuscript: {
    knex: jest.fn(),
    findManuscriptByTeamMember: jest.fn(),
    Statuses: getManuscriptStatuses(),
  },
  ReviewerSuggestion: {
    findOneBy: jest.fn(),
  },
  TeamMember: {
    find: jest.fn(),
    Statuses: getTeamMemberStatuses(),
    findAllByStatuses: jest.fn(),
    findSubmittingAuthor: jest.fn(),
    findOneByManuscriptAndRoleAndStatus: jest.fn(),
  },
  User: {
    find: jest.fn(),
  },
  Team: {
    find: jest.fn(),
    Role: getTeamRoles(),
  },
}

const { Manuscript, Team, TeamMember } = models

describe('Cancel reviewer invitation use-case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Successfully executes the flow', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.reviewersInvited,
      $query: jest.fn(() => ({
        update: jest.fn(),
      })),
    })

    const pendingReviewer = generateTeamMember({
      team: { role: Team.Role.reviewer, delete: jest.fn() },
      status: TeamMember.Statuses.pending,
      $query: jest.fn(() => ({
        update: jest.fn(),
      })),
      user: generateUser({ isSubscribedToEmails: true }),
    })
    const coiReviewer = generateTeamMember({
      team: { role: Team.Role.reviewer },
      status: TeamMember.Statuses.removed,
    })
    const academicEditor = generateTeamMember({
      team: { role: Team.Role.academicEditor },
      status: TeamMember.Statuses.accepted,
    })
    jest.spyOn(TeamMember, 'find').mockResolvedValue(pendingReviewer)
    jest
      .spyOn(Manuscript, 'findManuscriptByTeamMember')
      .mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'findAllByStatuses').mockResolvedValue([coiReviewer])

    await cancelReviewerInvitationUseCase
      .initialize({
        models,
        logger,
        logEvent,
        transaction,
        jobsService,
        eventsService,
        notificationService,
      })
      .execute({
        manuscriptId: manuscript.id,
        teamMemberId: pendingReviewer.id,
        userId: academicEditor.userId,
      })

    expect(trx.commit).toBeCalledTimes(1)
    expect(trx.rollback).toBeCalledTimes(0)

    expect(notificationService.notifyReviewer).toHaveBeenCalledTimes(1)
    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledTimes(1)
    expect(logEvent).toHaveBeenCalledTimes(1)
  })
  it('it does not notify the reviewer if he is unsubscribed', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.reviewersInvited,
      $query: jest.fn(() => ({
        update: jest.fn(),
      })),
    })

    const pendingReviewer = generateTeamMember({
      team: { role: Team.Role.reviewer, delete: jest.fn() },
      status: TeamMember.Statuses.pending,
      $query: jest.fn(() => ({
        update: jest.fn(),
      })),
      user: generateUser({ isSubscribedToEmails: false }),
    })

    const coiReviewer = generateTeamMember({
      team: { role: Team.Role.reviewer },
      status: TeamMember.Statuses.removed,
    })
    const academicEditor = generateTeamMember({
      team: { role: Team.Role.academicEditor },
      status: TeamMember.Statuses.accepted,
    })
    jest.spyOn(TeamMember, 'find').mockResolvedValue(pendingReviewer)
    jest
      .spyOn(Manuscript, 'findManuscriptByTeamMember')
      .mockResolvedValue(manuscript)
    jest.spyOn(TeamMember, 'findAllByStatuses').mockResolvedValue([coiReviewer])

    await cancelReviewerInvitationUseCase
      .initialize({
        models,
        logger,
        logEvent,
        transaction,
        jobsService,
        eventsService,
        notificationService,
      })
      .execute({
        manuscriptId: manuscript.id,
        teamMemberId: pendingReviewer.id,
        userId: academicEditor.userId,
      })

    expect(trx.commit).toBeCalledTimes(1)
    expect(trx.rollback).toBeCalledTimes(0)

    expect(notificationService.notifyReviewer).toHaveBeenCalledTimes(0)
    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledTimes(1)
    expect(logEvent).toHaveBeenCalledTimes(1)
  })
})
