import {
  getTeamRoles,
  getTeamMemberStatuses,
  generateManuscript,
  generateTeam,
  generateTeamMember,
  generateUser,
} from 'component-generators'

import { findOrCreateReviewerUseCase } from '../../src/useCases/inviteReviewers'

const mockReviewer = generateTeamMember()
class mockTeamMemberModel {
  constructor(args) {
    return {
      id: 'teamMemberId',
      save() {
        return this
      },
      ...args,
    }
  }
  static Statuses = getTeamMemberStatuses()
  static findOneByManuscriptAndRoleAndUser() {
    return mockReviewer
  }
}
const mockModels = {
  Team: {
    Role: getTeamRoles(),
    findOrCreate: jest.fn(),
  },
  TeamMember: mockTeamMemberModel,
}
const { Team, TeamMember } = mockModels

describe('Find or create reviewer use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('throws an error if the reviewer is already invited', async () => {
    const manuscript = generateManuscript()
    const team = generateTeam({
      role: Team.Role.reviewer,
    })
    const reviewer = generateTeamMember({
      teamId: team.id,
      status: TeamMember.Statuses.pending,
    })
    const user = generateUser()
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndUser')
      .mockResolvedValue(reviewer)

    const input = reviewer.alias

    const result = findOrCreateReviewerUseCase
      .initialize(TeamMember)
      .execute(input, user.id, team.id, manuscript.id, Team.Role.reviewer, 1)

    await expect(result).rejects.toThrowError(
      `User ${input.email} is already invited as ${Team.Role.reviewer}`,
    )
  })
  it('creates a new reviewer if none is found', async () => {
    const manuscript = generateManuscript()
    const team = generateTeam({
      role: Team.Role.reviewer,
    })
    const reviewer = generateTeamMember({
      teamId: team.id,
      status: TeamMember.Statuses.pending,
    })
    const user = generateUser()
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndUser')
      .mockResolvedValue(undefined)

    const input = reviewer.alias

    const result = await findOrCreateReviewerUseCase
      .initialize(TeamMember)
      .execute(input, user.id, team.id, manuscript.id, Team.Role.reviewer, 1)

    expect(result).toHaveProperty('position')
  })
  it('updates the status of an expired reviewer to pending', async () => {
    const manuscript = generateManuscript()
    const team = generateTeam({
      role: Team.Role.reviewer,
    })
    const reviewer = generateTeamMember({
      teamId: team.id,
      status: TeamMember.Statuses.expired,
    })
    const user = generateUser()
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndUser')
      .mockResolvedValue(reviewer)

    const input = reviewer.alias

    const foundReviewer = await findOrCreateReviewerUseCase
      .initialize(TeamMember)
      .execute(input, user.id, team.id, manuscript.id, Team.Role.reviewer, 1)

    expect(foundReviewer.status).toEqual(TeamMember.Statuses.pending)
  })
})
