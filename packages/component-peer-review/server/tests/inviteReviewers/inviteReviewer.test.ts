import {
  getTeamRoles,
  getManuscriptStatuses,
  getTeamMemberStatuses,
  generateManuscript,
  generateTeam,
  generateTeamMember,
  generateUser,
} from 'component-generators'

const { inviteReviewerUseCase } = require('../../src/useCases/inviteReviewers')

interface EventsService {
  publishSubmissionEvent(object: object): void
}
const eventsService: EventsService = {
  publishSubmissionEvent: () => {},
}
const config = {
  get: jest.fn(),
}

const models = {
  Job: {
    subscribe: jest.fn(),
  },
  Manuscript: {
    find: jest.fn(),
    Statuses: getManuscriptStatuses(),
    findAll: jest.fn(() => []),
  },
  Team: {
    Role: getTeamRoles(),
    findOrCreate: jest.fn(),
  },
  TeamMember: {
    findAllByManuscriptAndRoleAndExcludedStatuses: () => [],
    findOneByManuscriptAndRoleAndStatus: jest.fn(() => {}),
    findOneByRole: jest.fn(() => {}),
    Statuses: getTeamMemberStatuses(),
    findAllByManuscriptsAndRole: jest.fn(() => []),
  },
}
const { Team, Manuscript } = models

const useCase = {
  execute: () => ({}),
}
const mockedUseCases = {
  findOrCreateReviewerUseCase: useCase,
  findOrCreateReviewerUserUseCase: useCase,
  handleEmailsWhenReviewerIsInvitedUseCase: useCase,
  updateReviewerSuggestionInvitedStatusUseCase: useCase,
  dontAllowEditorsAsReviewersOnSpecialIssuesUseCase: useCase,
  cancelJobsWhenMinimumNumberOfReviewersIsMetUseCase: useCase,
  dontAllowReviewersToHaveMultipleRolesOnManuscriptUseCase: useCase,
}

const removalJobsService = {
  scheduleRemovalJob: jest.fn(),
}
const logEvent = () => {}
logEvent.actions = {}
logEvent.objectType = {}

describe('Invite reviewer use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('throws an error if the manuscript is deleted or withdrawn', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.deleted,
    })
    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)

    const result = inviteReviewerUseCase
      .initialize({
        config,
        models,
      })
      .execute({
        manuscriptId: manuscript.id,
      })

    await expect(result).rejects.toThrowError(
      `Cannot invite reviewers on manuscript ${manuscript.id} with status ${manuscript.status}.`,
    )
  })

  it('throws an error if the user is trying to invite himself', async () => {
    const manuscript = generateManuscript({})
    const user = generateUser({})

    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    mockedUseCases.findOrCreateReviewerUserUseCase = {
      execute: () => user,
    }

    const result = inviteReviewerUseCase
      .initialize({
        config,
        models,
        ...mockedUseCases,
      })
      .execute({
        manuscriptId: manuscript.id,
        userId: user.id,
        input: {
          aff: null,
        },
      })

    await expect(result).rejects.toThrowError(
      `You cannot invite yourself as a reviewer.`,
    )
  })

  it('calls the special issue use case if the manuscript is on a special issue', async () => {
    const manuscript = generateManuscript({
      specialIssueId: 'some-si-id',
    })
    const team = generateTeam({
      role: Team.Role.reviewer,
    })
    const reviewer = generateTeamMember({
      teamId: team.id,
    })
    const user = generateUser()
    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(Team, 'findOrCreate').mockResolvedValue(team)

    mockedUseCases.findOrCreateReviewerUseCase = {
      execute: () => reviewer,
    }
    mockedUseCases.findOrCreateReviewerUserUseCase = {
      execute: () => user,
    }
    mockedUseCases.dontAllowEditorsAsReviewersOnSpecialIssuesUseCase = {
      execute: jest.fn(),
    }

    await inviteReviewerUseCase
      .initialize({
        config,
        models,
        eventsService,
        logEvent,
        removalJobsService,
        ...mockedUseCases,
      })
      .execute({
        manuscriptId: manuscript.id,
        input: reviewer.alias,
      })

    expect(
      mockedUseCases.dontAllowEditorsAsReviewersOnSpecialIssuesUseCase.execute,
    ).toHaveBeenCalledTimes(1)
  })
  it('does not handle emails if the user is unsubscribed', async () => {
    const manuscript = generateManuscript()
    const team = generateTeam({
      role: Team.Role.reviewer,
    })
    const reviewer = generateTeamMember({
      teamId: team.id,
    })
    const user = generateUser({
      isSubscribedToEmails: false,
    })

    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(Team, 'findOrCreate').mockResolvedValue(team)
    jest.spyOn(eventsService, 'publishSubmissionEvent')

    mockedUseCases.findOrCreateReviewerUseCase = {
      execute: () => reviewer,
    }
    mockedUseCases.findOrCreateReviewerUserUseCase = {
      execute: () => user,
    }
    mockedUseCases.handleEmailsWhenReviewerIsInvitedUseCase = {
      execute: jest.fn(),
    }

    await inviteReviewerUseCase
      .initialize({
        config,
        models,
        eventsService,
        logEvent,
        removalJobsService,
        ...mockedUseCases,
      })
      .execute({
        manuscriptId: manuscript.id,
        input: reviewer.alias,
      })

    expect(
      mockedUseCases.handleEmailsWhenReviewerIsInvitedUseCase.execute,
    ).not.toHaveBeenCalled()
  })
  it('publishes a submission event if everything works fine', async () => {
    const manuscript = generateManuscript()
    const team = generateTeam({
      role: Team.Role.reviewer,
    })
    const reviewer = generateTeamMember({
      teamId: team.id,
    })
    const user = generateUser()
    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(Team, 'findOrCreate').mockResolvedValue(team)
    jest.spyOn(eventsService, 'publishSubmissionEvent')

    mockedUseCases.findOrCreateReviewerUseCase = {
      execute: () => reviewer,
    }
    mockedUseCases.findOrCreateReviewerUserUseCase = {
      execute: () => user,
    }

    await inviteReviewerUseCase
      .initialize({
        config,
        models,
        eventsService,
        logEvent,
        removalJobsService,
        ...mockedUseCases,
      })
      .execute({
        manuscriptId: manuscript.id,
        input: reviewer.alias,
      })

    expect(eventsService.publishSubmissionEvent).toHaveBeenCalled()
  })
})
