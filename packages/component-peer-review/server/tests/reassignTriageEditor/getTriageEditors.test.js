const models = require('@pubsweet/models')
const { getTriageEditorsUseCase } = require('../../src/useCases')

describe('Get triage editors', () => {
  beforeEach(() => {
    jest.resetAllMocks()
  })

  it('returns all triage editors from the special issue if the manuscript belongs to a special issue and has no COI', async () => {
    const { Manuscript, TeamMember, Team } = models

    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce({
      journalId: 'journal-id',
      specialIssueId: 'special-issue-id',
      sectionId: 'section-id',
      submissionId: 'submission-id',
      getHasSpecialIssueEditorialConflictOfInterest: () => false,
    })
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRole')
      .mockResolvedValueOnce([
        {
          userId: 'user-id-1',
          toDTO: () => ({ userId: 'user-id-from-dto-1' }),
        },
        {
          userId: 'user-id-2',
          toDTO: () => ({ userId: 'user-id-from-dto-2' }),
        },
      ])
    jest.spyOn(TeamMember, 'findAllByJournalAndRole').mockResolvedValue([])
    jest.spyOn(TeamMember, 'findAllBySectionAndRole').mockResolvedValue([])
    jest
      .spyOn(TeamMember, 'findAllBySubmissionAndRoleAndStatuses')
      .mockResolvedValue([])

    const triageEditorsMembers = await getTriageEditorsUseCase
      .initialize(models)
      .execute('manuscript-id')

    expect(triageEditorsMembers).toHaveLength(2)

    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledWith({
      specialIssueId: 'special-issue-id',
      role: Team.Role.triageEditor,
    })

    expect(TeamMember.findAllByJournalAndRole).toHaveBeenCalledTimes(0)
    expect(TeamMember.findAllBySectionAndRole).toHaveBeenCalledTimes(0)
  })

  it('returns all triage editors from the special issue parent if the manuscript belongs to a special issue and has an editorial COI', async () => {
    const { Manuscript, TeamMember, Team } = models

    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce({
      journalId: 'journal-id',
      specialIssueId: 'special-issue-id',
      submissionId: 'submission-id',
      getHasSpecialIssueEditorialConflictOfInterest: () => true,
    })
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRole')
      .mockResolvedValueOnce([])
    jest.spyOn(TeamMember, 'findAllByJournalAndRole').mockResolvedValue([
      {
        userId: 'user-id-1',
        toDTO: () => ({ userId: 'user-id-from-dto-1' }),
      },
      {
        userId: 'user-id-2',
        toDTO: () => ({ userId: 'user-id-from-dto-2' }),
      },
    ])

    jest.spyOn(TeamMember, 'findAllBySectionAndRole').mockResolvedValue([])
    jest
      .spyOn(TeamMember, 'findAllBySubmissionAndRoleAndStatuses')
      .mockResolvedValue([])

    const triageEditorsMembers = await getTriageEditorsUseCase
      .initialize(models)
      .execute('manuscript-id')

    expect(triageEditorsMembers).toHaveLength(2)

    expect(TeamMember.findAllByJournalAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByJournalAndRole).toHaveBeenCalledWith({
      journalId: 'journal-id',
      role: Team.Role.triageEditor,
    })

    expect(TeamMember.findAllBySpecialIssueAndRole).toHaveBeenCalledTimes(0)
    expect(TeamMember.findAllBySectionAndRole).toHaveBeenCalledTimes(0)
  })
})
