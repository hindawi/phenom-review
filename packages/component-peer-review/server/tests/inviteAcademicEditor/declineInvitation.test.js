const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { declineAcademicEditorInvitationUseCase } = require('../../src/useCases')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  invitation_declined: 'invitation_declined',
}
logEvent.objectType = { manuscript: 'manuscript' }

const jobsService = {
  cancelJobs: jest.fn(),
}
const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

describe('Decline invitation use case', () => {
  it('declines an invitation when the input is correct', async () => {
    const {
      Team,
      User,
      Journal,
      Identity,
      Manuscript,
      TeamMember,
      ArticleType,
      PeerReviewModel,
    } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
        hasTriageEditor: true,
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
      input: { status: TeamMember.Statuses.pending },
    })

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
      input: { status: TeamMember.Statuses.pending },
    })

    const academicEditor = fixtures.generateUser({ User, Identity })
    await fixtures.generateArticleTypes(ArticleType)
    const articleType = fixtures.getArticleTypeByPeerReview(true)
    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        articleTypeId: articleType.id,
        status: Manuscript.Statuses.academicEditorInvited,
      },
      Manuscript,
    })

    const manuscriptTeam = fixtures.generateTeam({
      properties: {
        role: Team.Role.academicEditor,
        manuscriptId: manuscript.id,
      },
      Team,
    })
    manuscript.teams.push(manuscriptTeam)
    manuscriptTeam.manuscript = manuscript

    const teamMember = fixtures.generateTeamMember({
      properties: {
        userId: academicEditor.id,
        teamId: manuscriptTeam.id,
        status: TeamMember.Statuses.pending,
      },
      TeamMember,
    })

    teamMember.user = academicEditor
    teamMember.team = manuscriptTeam
    manuscriptTeam.members.push(teamMember)
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: {
        isSubmitting: true,
        status: TeamMember.Statuses.pending,
      },
    })

    await declineAcademicEditorInvitationUseCase
      .initialize({
        models,
        logEvent,
        jobsService,
        eventsService,
      })
      .execute({
        input: {
          teamMemberId: teamMember.id,
          reason: `I don't wanna do this.`,
        },
        userId: teamMember.userId,
      })

    expect(teamMember.status).toEqual(TeamMember.Statuses.declined)
    expect(manuscript.status).toEqual(Manuscript.Statuses.submitted)
  })
  it('notifies the Editorial Assistant when the PRM is STAE', async () => {
    const {
      Team,
      User,
      Journal,
      Identity,
      Manuscript,
      TeamMember,
      ArticleType,
      PeerReviewModel,
    } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.academicEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })

    const academicEditor = fixtures.generateUser({ User, Identity })
    await fixtures.generateArticleTypes(ArticleType)
    const articleType = fixtures.getArticleTypeByPeerReview(true)
    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.academicEditorInvited,
        articleTypeId: articleType.id,
      },
      Manuscript,
    })
    manuscript.articleType = articleType
    manuscript.journal = journal

    const manuscriptTeam = fixtures.generateTeam({
      properties: {
        role: Team.Role.academicEditor,
        manuscriptId: manuscript.id,
      },
      Team,
    })
    manuscript.teams.push(manuscriptTeam)
    manuscriptTeam.manuscript = manuscript

    const teamMember = fixtures.generateTeamMember({
      properties: {
        userId: academicEditor.id,
        teamId: manuscriptTeam.id,
        status: TeamMember.Statuses.pending,
      },
      TeamMember,
    })

    teamMember.user = academicEditor
    teamMember.team = manuscriptTeam
    manuscriptTeam.members.push(teamMember)

    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        status: TeamMember.Statuses.pending,
        isSubmitting: true,
      },
      role: Team.Role.author,
    })

    await declineAcademicEditorInvitationUseCase
      .initialize({
        models,
        logEvent,
        jobsService,
        eventsService,
      })
      .execute({
        input: {
          teamMemberId: teamMember.id,
          reason: `I am not an expert on this topic.`,
        },
        userId: teamMember.userId,
      })

    expect(teamMember.status).toEqual(TeamMember.Statuses.declined)
    expect(manuscript.status).toEqual(Manuscript.Statuses.submitted)
  })
})
