const { when } = require('jest-when')
const {
  getTeamMemberStatuses,
  generateTeamMember,
  generateManuscript,
  generateJob,
  getTeamRoles,
  generateJournal,
  generateUser,
  generateArticleType,
  generatePeerReviewModel,
} = require('component-generators')
const {
  acceptReassignmentAcademicEditorInvitationUseCase,
} = require('../../src/useCases/inviteAcademicEditor')

jest.mock('component-quality-check', () => ({
  services: {
    ConflictOfInterest: { getConflictsForTeamMembers: jest.fn(() => []) },
  },
}))

const models = {
  TeamMember: {
    find: jest.fn(),
    findAllBySubmissionAndRoleAndStatuses: jest.fn(),
    findOneByManuscriptAndRoleAndStatus: jest.fn(),
    findAllBySubmissionAndRole: jest.fn(() => []),
    updateProperties: jest.fn(),
    findSubmittingAuthor: jest.fn(),
    findTriageEditor: jest.fn(),
    Statuses: getTeamMemberStatuses(),
    save: jest.fn(),
  },
  Manuscript: {
    findManuscriptByTeamMember: jest.fn(),
    getEditorLabel: jest.fn(() => 'label'),
  },
  Job: {
    findAllByTeamMember: jest.fn(),
  },
  Journal: {
    find: jest.fn(),
  },
  User: {
    find: jest.fn(),
  },
  ArticleType: {
    find: jest.fn(),
  },
  PeerReviewModel: {
    findOneByManuscriptParent: jest.fn(),
  },
  Team: {
    Role: getTeamRoles(),
  },
}

const services = {
  jobsService: {
    cancelJobs: jest.fn(),
  },
  notificationService: {
    notifyTriageEditor: jest.fn(),
    notifyEditorialAssistant: jest.fn(),
    notifyActiveAcademicEditor: jest.fn(),
    notifyPendingAcademicEditor: jest.fn(),
  },
  emailJobsService: {
    sendAcademicEditorRemindersToInviteReviewers: jest.fn(),
  },
  eventsService: {
    publishSubmissionEvent: jest.fn(),
  },
  logEvent: jest.fn(),
}
services.logEvent.actions = {
  role_academic_editor_reassigned: 'role_academic_editor_reassigned',
  invitation_agreed: 'invitation_agreed',
}
services.logEvent.objectType = {
  manuscript: 'manuscript',
  user: 'user',
}

const executeParams = {
  teamMemberId: 'tm1',
  userId: 'user1',
  useCommunication: false,
}

const mock = {
  manuscript: generateManuscript(),
  pendingAE: generateTeamMember({
    status: models.TeamMember.Statuses.pending,
  }),
  activeAE1: generateTeamMember({
    status: models.TeamMember.Statuses.accepted,
  }),
  activeAE2: generateTeamMember({
    status: models.TeamMember.Statuses.accepted,
  }),
  pendingAEjob: generateJob({
    aeType: 'pending',
  }),
  activeAEjob: generateJob({
    aeType: 'active',
  }),
  journal: generateJournal(),
  pendingUser: generateUser(),
  submittingAuthor: generateTeamMember(),
  articleType: generateArticleType({
    hasPeerReview: true,
  }),
  peerReviewModel: generatePeerReviewModel({
    hasTriageEditor: true,
  }),
}

describe('Accept reassignment invitation', () => {
  beforeAll(async () => {
    jest.clearAllMocks()

    when(models.TeamMember.find)
      .calledWith(executeParams.teamMemberId)
      .mockResolvedValue(mock.pendingAE)

    when(models.Manuscript.findManuscriptByTeamMember)
      .calledWith(executeParams.teamMemberId)
      .mockResolvedValue(mock.manuscript)

    // list of accepted AEs
    when(models.TeamMember.findAllBySubmissionAndRoleAndStatuses)
      .calledWith(
        when(objectArg =>
          objectArg.statuses.includes(models.TeamMember.Statuses.accepted),
        ),
      )
      .mockResolvedValue([mock.activeAE1, mock.activeAE2])

    // one of the accepted AEs ( not sure which one at this point )
    when(models.TeamMember.findOneByManuscriptAndRoleAndStatus)
      .calledWith(
        when(
          objectArg => (objectArg.status = models.TeamMember.Statuses.accepted),
        ),
      )
      .mockResolvedValue(mock.activeAE1)

    // list of pending AEs ( it's a list because our submission can have multiple versions and we have to update the teamMembers from all the versions. )
    when(models.TeamMember.findAllBySubmissionAndRoleAndStatuses)
      .calledWith(
        when(objectArg =>
          objectArg.statuses.includes(models.TeamMember.Statuses.pending),
        ),
      )
      .mockResolvedValue([mock.pendingAE])

    // get jobs for first active AE
    when(models.Job.findAllByTeamMember)
      .calledWith(mock.activeAE1.id)
      .mockResolvedValue([mock.activeAEjob])

    // get jobs for first active AE
    when(models.Job.findAllByTeamMember)
      .calledWith(mock.activeAE2.id)
      .mockResolvedValue([mock.activeAEjob])

    // get jobs for pending AE
    when(models.Job.findAllByTeamMember)
      .calledWith(mock.pendingAE.id)
      .mockResolvedValue([mock.pendingAEjob])

    // journal
    when(models.Journal.find)
      .calledWith(mock.manuscript.journalId)
      .mockResolvedValue(mock.journal)

    // user
    when(models.User.find)
      .calledWith(mock.pendingAE.userId)
      .mockResolvedValue(mock.pendingUser)

    // submitting author
    when(models.TeamMember.findSubmittingAuthor)
      .calledWith(mock.manuscript.id)
      .mockResolvedValue(mock.submittingAuthor)

    // article type
    when(models.ArticleType.find)
      .calledWith(mock.manuscript.articleTypeId)
      .mockResolvedValue(mock.articleType)

    // peer review model
    jest
      .spyOn(models.PeerReviewModel, 'findOneByManuscriptParent')
      .mockResolvedValue(mock.peerReviewModel)

    await acceptReassignmentAcademicEditorInvitationUseCase
      .initialize({
        models,
        ...services,
      })
      .execute(executeParams)
  })

  it('should change all accepted AEs statuses to removed', async () => {
    expect(mock.activeAE1.updateProperties).toBeCalledWith({
      status: models.TeamMember.Statuses.removed,
    })
    expect(mock.activeAE2.updateProperties).toBeCalledWith({
      status: models.TeamMember.Statuses.removed,
    })
  })
  it('should cancel all accepted AEs jobs', () => {
    // called 2 times for actives
    expect(services.jobsService.cancelJobs.mock.calls[0][0][0]).toHaveProperty(
      ['data', 'aeType'],
      'active',
    )
    expect(services.jobsService.cancelJobs.mock.calls[1][0][0]).toHaveProperty(
      ['data', 'aeType'],
      'active',
    )
  })
  it('should change all pendings AEs status to accepted', async () => {
    expect(mock.pendingAE.updateProperties).toBeCalledWith(
      expect.objectContaining({
        status: models.TeamMember.Statuses.accepted,
      }),
    )
  })
  it('should cancel all jobs for pending AE', () => {
    // one job only
    expect(services.jobsService.cancelJobs.mock.calls[2][0][0]).toHaveProperty(
      ['data', 'aeType'],
      'pending',
    )
  })
  it('should notify the TE about the reassignment if PRM does have one', () => {
    expect(services.notificationService.notifyTriageEditor).toBeCalled()
  })

  it('should notify the old active AE if the article type has peer review', () => {
    expect(services.notificationService.notifyActiveAcademicEditor).toBeCalled()
  })
  it('should notify the new AE if the article type has peer review', () => {
    expect(
      services.notificationService.notifyPendingAcademicEditor,
    ).toBeCalled()
  })
  it('should send a reminder to the new AE to invite reviewers if article type has peer review', () => {
    expect(
      services.emailJobsService.sendAcademicEditorRemindersToInviteReviewers,
    ).toBeCalled()
  })

  it('should publish a submission event about AE(s) being removed', () => {
    expect(services.eventsService.publishSubmissionEvent).toBeCalledWith(
      expect.objectContaining({
        eventName: 'SubmissionAcademicEditorRemoved',
      }),
    )
  })
  it('should publish a submission event about AE accepting', () => {
    expect(services.eventsService.publishSubmissionEvent).toBeCalledWith(
      expect.objectContaining({
        eventName: 'SubmissionAcademicEditorAccepted',
      }),
    )
  })

  it('should log the agreed invitation', () => {
    expect(services.logEvent).toBeCalledWith(
      expect.objectContaining({
        action: services.logEvent.actions.invitation_agreed,
      }),
    )
  })
  it('should log the AE reassignment', () => {
    expect(services.logEvent).toBeCalledWith(
      expect.objectContaining({
        action: services.logEvent.actions.role_academic_editor_reassigned,
      }),
    )
  })
})
