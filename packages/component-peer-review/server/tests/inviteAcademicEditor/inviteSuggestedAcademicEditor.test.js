const { Manuscript, Team, TeamMember } = require('@pubsweet/models')
const { inviteSuggestedAcademicEditorUseCase } = require('../../src/useCases')

describe('Ingest Editor Submission Scores', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    jest.resetAllMocks()
  })

  it('should not invite academic editor if manuscript not supporting it', async () => {
    const getSuggestedAcademicEditor = {
      execute: jest.fn(),
    }
    const inviteAcademicEditor = {
      execute: jest.fn(),
    }

    jest
      .spyOn(Manuscript, 'findLastManuscriptBySubmissionId')
      .mockResolvedValueOnce({
        id: 'some-manuscript-id',
        journalId: 'some-journal-id',
        sectionId: 'some-section-id',
        specialIssueId: 'some-special-id',
        version: '1',
        status: Manuscript.Statuses.submitted,
        allowAcademicEditorAutomaticInvitation: false,
      })

    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValueOnce([])

    await inviteSuggestedAcademicEditorUseCase
      .initialize({
        models: {
          Manuscript,
          Team,
          TeamMember,
        },
        useCases: {
          getSuggestedAcademicEditor,
          inviteAcademicEditor,
        },
      })
      .execute('some-submission-id')

    expect(Manuscript.findLastManuscriptBySubmissionId).toHaveBeenCalledTimes(1)
    expect(Manuscript.findLastManuscriptBySubmissionId).toHaveBeenCalledWith({
      submissionId: 'some-submission-id',
    })

    expect(TeamMember.findAllByManuscriptAndRole).toHaveBeenCalledTimes(0)

    expect(getSuggestedAcademicEditor.execute).toHaveBeenCalledTimes(0)
    expect(inviteAcademicEditor.execute).toHaveBeenCalledTimes(0)
  })

  it('should not invite academic editor if another AE was already invited', async () => {
    const getSuggestedAcademicEditor = {
      execute: jest.fn(),
    }
    const inviteAcademicEditor = {
      execute: jest.fn(),
    }

    const updateManuscriptProprieties = jest.fn()
    const saveManuscript = jest.fn()
    const mockedManuscript = {
      id: 'some-manuscript-id',
      journalId: 'some-journal-id',
      sectionId: 'some-section-id',
      specialIssueId: 'some-special-id',
      version: '1',
      status: Manuscript.Statuses.submitted,
      allowAcademicEditorAutomaticInvitation: true,
      updateProperties: updateManuscriptProprieties,
      save: saveManuscript,
    }

    jest
      .spyOn(Manuscript, 'findLastManuscriptBySubmissionId')
      .mockResolvedValueOnce(mockedManuscript)

    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValueOnce([
      {
        id: 'some-id',
        status: 'pending',
      },
    ])

    await inviteSuggestedAcademicEditorUseCase
      .initialize({
        models: {
          Manuscript,
          Team,
          TeamMember,
        },
        useCases: {
          getSuggestedAcademicEditor,
          inviteAcademicEditor,
        },
      })
      .execute('some-submission-id')

    expect(Manuscript.findLastManuscriptBySubmissionId).toHaveBeenCalledTimes(1)
    expect(Manuscript.findLastManuscriptBySubmissionId).toHaveBeenCalledWith({
      submissionId: 'some-submission-id',
    })

    expect(TeamMember.findAllByManuscriptAndRole).toHaveBeenCalledTimes(1)

    expect(updateManuscriptProprieties).toHaveBeenCalledTimes(0)
    expect(saveManuscript).toHaveBeenCalledTimes(0)

    expect(getSuggestedAcademicEditor.execute).toHaveBeenCalledTimes(0)
    expect(inviteAcademicEditor.execute).toHaveBeenCalledTimes(0)
  })

  it('should not invite academic editor if there are no suggested academic editor', async () => {
    const getSuggestedAcademicEditor = {
      execute: jest.fn().mockResolvedValueOnce(null),
    }
    const inviteAcademicEditor = {
      execute: jest.fn(),
    }

    const updateManuscriptProprieties = jest.fn()
    const saveManuscript = jest.fn()
    jest
      .spyOn(Manuscript, 'findLastManuscriptBySubmissionId')
      .mockResolvedValueOnce({
        id: 'some-manuscript-id',
        journalId: 'some-journal-id',
        sectionId: 'some-section-id',
        specialIssueId: 'some-special-id',
        version: '1',
        status: Manuscript.Statuses.submitted,
        allowAcademicEditorAutomaticInvitation: true,
        updateProperties: updateManuscriptProprieties,
        save: saveManuscript,
      })

    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValueOnce([])

    await inviteSuggestedAcademicEditorUseCase
      .initialize({
        models: {
          Manuscript,
          Team,
          TeamMember,
        },
        useCases: {
          getSuggestedAcademicEditor,
          inviteAcademicEditor,
        },
      })
      .execute('some-submission-id')

    expect(Manuscript.findLastManuscriptBySubmissionId).toHaveBeenCalledTimes(1)
    expect(Manuscript.findLastManuscriptBySubmissionId).toHaveBeenCalledWith({
      submissionId: 'some-submission-id',
    })

    expect(TeamMember.findAllByManuscriptAndRole).toHaveBeenCalledTimes(1)

    expect(updateManuscriptProprieties).toHaveBeenCalledTimes(0)
    expect(saveManuscript).toHaveBeenCalledTimes(0)

    expect(getSuggestedAcademicEditor.execute).toHaveBeenCalledTimes(1)
    expect(getSuggestedAcademicEditor.execute).toHaveBeenCalledWith({
      manuscriptId: 'some-manuscript-id',
      journalId: 'some-journal-id',
      sectionId: 'some-section-id',
      specialIssueId: 'some-special-id',
    })

    expect(inviteAcademicEditor.execute).toHaveBeenCalledTimes(0)
  })

  it('should invite academic editor if manuscript supports it and an AE exists', async () => {
    const getSuggestedAcademicEditor = {
      execute: jest.fn().mockResolvedValueOnce({
        userId: 'some-ae-id',
      }),
    }
    const inviteAcademicEditor = {
      execute: jest.fn(),
    }

    const updateManuscriptProprieties = jest.fn()
    const saveManuscript = jest.fn()
    const mockedManuscript = {
      id: 'some-manuscript-id',
      journalId: 'some-journal-id',
      sectionId: 'some-section-id',
      specialIssueId: 'some-special-id',
      submissionId: 'some-submission-id',
      version: '1',
      status: Manuscript.Statuses.submitted,
      allowAcademicEditorAutomaticInvitation: true,
      updateProperties: updateManuscriptProprieties,
      save: saveManuscript,
    }

    jest
      .spyOn(Manuscript, 'findLastManuscriptBySubmissionId')
      .mockResolvedValueOnce(mockedManuscript)

    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValueOnce([])

    await inviteSuggestedAcademicEditorUseCase
      .initialize({
        models: {
          Manuscript,
          Team,
          TeamMember,
        },
        useCases: {
          getSuggestedAcademicEditor,
          inviteAcademicEditor,
        },
      })
      .execute('some-submission-id')

    expect(Manuscript.findLastManuscriptBySubmissionId).toHaveBeenCalledTimes(1)
    expect(Manuscript.findLastManuscriptBySubmissionId).toHaveBeenCalledWith({
      submissionId: 'some-submission-id',
    })

    expect(TeamMember.findAllByManuscriptAndRole).toHaveBeenCalledTimes(1)

    expect(updateManuscriptProprieties).toHaveBeenCalledTimes(0)
    expect(saveManuscript).toHaveBeenCalledTimes(0)

    expect(getSuggestedAcademicEditor.execute).toHaveBeenCalledTimes(1)
    expect(getSuggestedAcademicEditor.execute).toHaveBeenCalledWith({
      manuscriptId: 'some-manuscript-id',
      journalId: 'some-journal-id',
      sectionId: 'some-section-id',
      specialIssueId: 'some-special-id',
    })

    expect(inviteAcademicEditor.execute).toHaveBeenCalledTimes(1)
    expect(inviteAcademicEditor.execute).toHaveBeenCalledWith({
      hasWorkloadAssignment: true,
      userId: 'some-ae-id',
      submissionId: 'some-submission-id',
    })
  })
})
