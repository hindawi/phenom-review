import { when } from 'jest-when'
import { getTeamRoles } from 'component-generators'
import { getAllTriageEditorsUseCase } from '../../src/useCases/reassignTriageEditor'

const models = {
  TeamMember: {
    findAllByJournalAndRole: jest.fn(),
    findAllBySpecialIssueAndRole: jest.fn(),
    findAllBySectionAndRole: jest.fn(),
  },
  Team: {
    Role: getTeamRoles(),
  },
}
const getTriageEditors = getAllTriageEditorsUseCase.initialize(models)
const { authsomePolicies } = getAllTriageEditorsUseCase

describe('getAllTriageEditorsUseCase', () => {
  beforeAll(() => {
    when(models.TeamMember.findAllByJournalAndRole)
      .calledWith({ role: 'triageEditor', journalId: 'JOURNAL_1' })
      .mockResolvedValue(['AE_1', 'AE_2'])

    when(models.TeamMember.findAllBySpecialIssueAndRole)
      .calledWith({ role: 'triageEditor', specialIssueId: 'SPECIAL_ISSUE_1' })
      .mockResolvedValue(['AE_3', 'AE_4'])

    when(models.TeamMember.findAllBySectionAndRole)
      .calledWith({ role: 'triageEditor', sectionId: 'SECTION_1' })
      .mockResolvedValue(['AE_5', 'AE_6'])
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('authsomePolicies are proper', () => {
    expect(authsomePolicies).toEqual([
      'isAuthenticated',
      'isTriageEditor',
      'isEditorialAssistant',
      'admin',
    ])
  })

  it('returns [] if no parameter is passed', async () => {
    const results = await getTriageEditors.execute()
    expect(results).toEqual([])
  })

  it('returns [] if no id is provided', async () => {
    const results = await getTriageEditors.execute({})
    expect(results).toEqual([])
  })

  it('returns only journal AE when journalId is provided', async () => {
    const results = await getTriageEditors.execute({
      journalId: 'JOURNAL_1',
      specialIssueId: 'SPECIAL_ISSUE_1',
      sectionId: 'SECTION_1',
    })
    expect(results).toEqual(['AE_1', 'AE_2'])

    expect(models.TeamMember.findAllByJournalAndRole).toHaveBeenCalledTimes(1)
    expect(
      models.TeamMember.findAllBySpecialIssueAndRole,
    ).toHaveBeenCalledTimes(0)
    expect(models.TeamMember.findAllBySectionAndRole).toHaveBeenCalledTimes(0)
  })

  it('returns only special issue AE when no journalId is provided', async () => {
    const results = await getTriageEditors.execute({
      specialIssueId: 'SPECIAL_ISSUE_1',
      sectionId: 'SECTION_1',
    })
    expect(results).toEqual(['AE_3', 'AE_4'])

    expect(models.TeamMember.findAllByJournalAndRole).toHaveBeenCalledTimes(0)
    expect(
      models.TeamMember.findAllBySpecialIssueAndRole,
    ).toHaveBeenCalledTimes(1)
    expect(models.TeamMember.findAllBySectionAndRole).toHaveBeenCalledTimes(0)
  })

  it('returns only section AE when no journalId or specialIssueId is provided', async () => {
    const results = await getTriageEditors.execute({
      sectionId: 'SECTION_1',
    })
    expect(results).toEqual(['AE_5', 'AE_6'])

    expect(models.TeamMember.findAllByJournalAndRole).toHaveBeenCalledTimes(0)
    expect(
      models.TeamMember.findAllBySpecialIssueAndRole,
    ).toHaveBeenCalledTimes(0)
    expect(models.TeamMember.findAllBySectionAndRole).toHaveBeenCalledTimes(1)
  })
})
