const models = require('@pubsweet/models')
const { noop } = require('lodash')
const { getAcademicEditorsUseCase } = require('../../src/useCases')

describe('Get academic editors', () => {
  beforeEach(() => {
    jest.resetAllMocks()
  })

  it('returns an empty array if there is no journal Academic Editor team and does not belong to special Issue', async () => {
    const { Manuscript, TeamMember, Team } = models

    const mockManuscript = {
      journalId: 'some-journal-id',
      submissionId: 'some-submission-id',
      getHasSpecialIssueEditorialConflictOfInterest: () => false,
    }

    const numberOfAcademicEditorsToShow = 2
    const config = {
      get: noop,
    }

    jest.spyOn(config, 'get').mockReturnValueOnce(numberOfAcademicEditorsToShow)

    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce(mockManuscript)

    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRoleAndSearchValue')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findAllByJournalAndRoleAndSearchValue')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRoleAndStatus')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findAllBySubmissionAndRoleAndStatuses')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'orderAcademicEditorsByScoreAndWorkloadAndEmail')
      .mockResolvedValueOnce([])

    const academicEditorMembers = await getAcademicEditorsUseCase
      .initialize(models, config)
      .execute({
        manuscriptId: 'some-manuscript-id',
        searchValue: 'some-search-value',
      })

    expect(academicEditorMembers).toHaveLength(0)

    expect(Manuscript.find).toHaveBeenCalledTimes(1)
    expect(Manuscript.find).toHaveBeenCalledWith('some-manuscript-id')

    expect(
      TeamMember.findAllBySpecialIssueAndRoleAndSearchValue,
    ).toHaveBeenCalledTimes(0)

    expect(
      TeamMember.findAllByJournalAndRoleAndSearchValue,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findAllByJournalAndRoleAndSearchValue,
    ).toHaveBeenCalledWith({
      journalId: 'some-journal-id',
      role: Team.Role.academicEditor,
      searchValue: 'some-search-value',
      eagerLoadRelations: 'user.identities',
    })

    expect(TeamMember.findAllByManuscriptAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByManuscriptAndRole).toHaveBeenCalledWith({
      manuscriptId: 'some-manuscript-id',
      role: Team.Role.author,
    })

    expect(
      TeamMember.findAllByManuscriptAndRoleAndStatus,
    ).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByManuscriptAndRoleAndStatus).toHaveBeenCalledWith(
      {
        manuscriptId: 'some-manuscript-id',
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.declined,
      },
    )

    expect(
      TeamMember.findAllBySubmissionAndRoleAndStatuses,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findAllBySubmissionAndRoleAndStatuses,
    ).toHaveBeenCalledWith({
      role: Team.Role.academicEditor,
      statuses: [TeamMember.Statuses.removed],
      submissionId: 'some-submission-id',
    })
    expect(
      TeamMember.orderAcademicEditorsByScoreAndWorkloadAndEmail,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.orderAcademicEditorsByScoreAndWorkloadAndEmail,
    ).toHaveBeenCalledWith({
      manuscript: mockManuscript,
      academicEditors: academicEditorMembers,
    })
  })

  it('returns all academic editors from special issue except those that declined', async () => {
    const { Manuscript, TeamMember, Team } = models

    const mockManuscript = {
      journalId: 'some-journal-id',
      specialIssueId: 'some-special-issue-id',
      submissionId: 'some-submission-id',
      getHasSpecialIssueEditorialConflictOfInterest: () => false,
    }

    const numberOfAcademicEditorsToShow = 2

    const config = {
      get: noop,
    }

    jest.spyOn(config, 'get').mockReturnValueOnce(numberOfAcademicEditorsToShow)

    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce(mockManuscript)
    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRoleAndSearchValue')
      .mockResolvedValueOnce([
        {
          userId: 'user-id-1',
        },
        {
          userId: 'user-id-2',
        },
      ])
    jest.spyOn(TeamMember, 'findAllByJournalAndRoleAndSearchValue')
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRoleAndStatus')
      .mockResolvedValueOnce([
        {
          userId: 'user-id-2',
        },
      ])
    jest
      .spyOn(TeamMember, 'findAllBySubmissionAndRoleAndStatuses')
      .mockResolvedValueOnce([])
    jest.spyOn(Team, 'find').mockResolvedValueOnce({
      id: 'teamId',
    })
    jest
      .spyOn(TeamMember, 'orderAcademicEditorsByScoreAndWorkloadAndEmail')
      .mockResolvedValueOnce([
        {
          userId: 'user-id-1',
          created: 'created-date-1',
          alias: {
            surname: 'some-surname-1',
            givenNames: 'some-given-names-1',
            title: 'some-title-1',
          },
        },
        {
          userId: 'user-id-2',
          created: 'created-date-2',
          alias: {
            surname: 'some-surname-2',
            givenNames: 'some-given-names-2',
            title: 'some-title-2',
          },
        },
      ])

    const academicEditorMembers = await getAcademicEditorsUseCase
      .initialize(models, config)
      .execute({
        manuscriptId: 'some-manuscript-id',
        searchValue: 'some-search-value',
      })

    expect(academicEditorMembers).toHaveLength(1)
    expect(academicEditorMembers).toEqual([
      {
        userId: 'user-id-1',
        alias: {
          surname: 'some-surname-1',
          givenNames: 'some-given-names-1',
          title: 'some-title-1',
          name: {
            surname: 'some-surname-1',
            givenNames: 'some-given-names-1',
            title: 'some-title-1',
          },
        },
        invited: 'created-date-1',
        created: 'created-date-1',
      },
    ])

    expect(Manuscript.find).toHaveBeenCalledTimes(1)
    expect(Manuscript.find).toHaveBeenCalledWith('some-manuscript-id')

    expect(
      TeamMember.findAllBySpecialIssueAndRoleAndSearchValue,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findAllBySpecialIssueAndRoleAndSearchValue,
    ).toHaveBeenCalledWith({
      specialIssueId: 'some-special-issue-id',
      role: Team.Role.academicEditor,
      searchValue: 'some-search-value',
      eagerLoadRelations: 'user.identities',
    })

    expect(
      TeamMember.findAllByJournalAndRoleAndSearchValue,
    ).toHaveBeenCalledTimes(0)

    expect(TeamMember.findAllByManuscriptAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByManuscriptAndRole).toHaveBeenCalledWith({
      manuscriptId: 'some-manuscript-id',
      role: Team.Role.author,
    })

    expect(
      TeamMember.findAllByManuscriptAndRoleAndStatus,
    ).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByManuscriptAndRoleAndStatus).toHaveBeenCalledWith(
      {
        manuscriptId: 'some-manuscript-id',
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.declined,
      },
    )

    expect(
      TeamMember.findAllBySubmissionAndRoleAndStatuses,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findAllBySubmissionAndRoleAndStatuses,
    ).toHaveBeenCalledWith({
      role: Team.Role.academicEditor,
      statuses: [TeamMember.Statuses.removed],
      submissionId: 'some-submission-id',
    })

    expect(
      TeamMember.orderAcademicEditorsByScoreAndWorkloadAndEmail,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.orderAcademicEditorsByScoreAndWorkloadAndEmail,
    ).toHaveBeenCalledWith({
      academicEditors: [{ userId: 'user-id-1' }, { userId: 'user-id-2' }],
      manuscript: mockManuscript,
    })
  })

  it('returns all academic editors only from the manuscript`s journal if not Special Issue', async () => {
    const { Manuscript, TeamMember, Team } = models

    const mockManuscript = {
      journalId: 'some-journal-id',
      submissionId: 'some-submission-id',
      getHasSpecialIssueEditorialConflictOfInterest: () => false,
    }

    const numberOfAcademicEditorsToShow = 2

    const config = {
      get: noop,
    }

    jest.spyOn(config, 'get').mockReturnValueOnce(numberOfAcademicEditorsToShow)

    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce(mockManuscript)

    jest
      .spyOn(TeamMember, 'findAllBySpecialIssueAndRoleAndSearchValue')
      .mockResolvedValueOnce([])

    jest
      .spyOn(TeamMember, 'findAllByJournalAndRoleAndSearchValue')
      .mockResolvedValueOnce([
        {
          userId: 'user-id-1',
        },
        {
          userId: 'user-id-2',
        },
      ])

    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValueOnce([])

    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRoleAndStatus')
      .mockResolvedValueOnce([])

    jest
      .spyOn(TeamMember, 'findAllBySubmissionAndRoleAndStatuses')
      .mockResolvedValueOnce([])

    jest
      .spyOn(TeamMember, 'orderAcademicEditorsByScoreAndWorkloadAndEmail')
      .mockResolvedValueOnce([
        {
          userId: 'user-id-1',
          created: 'created-date-1',
          alias: {
            surname: 'some-surname-1',
            givenNames: 'some-given-names-1',
            title: 'some-title-1',
          },
        },
        {
          userId: 'user-id-2',
          created: 'created-date-2',
          alias: {
            surname: 'some-surname-2',
            givenNames: 'some-given-names-2',
            title: 'some-title-2',
          },
        },
      ])

    const academicEditorMembers = await getAcademicEditorsUseCase
      .initialize(models, config)
      .execute({
        manuscriptId: 'some-manuscript-id',
        searchValue: 'some-search-value',
      })

    expect(academicEditorMembers).toHaveLength(2)
    expect(academicEditorMembers).toEqual([
      {
        userId: 'user-id-1',
        alias: {
          surname: 'some-surname-1',
          givenNames: 'some-given-names-1',
          title: 'some-title-1',
          name: {
            surname: 'some-surname-1',
            givenNames: 'some-given-names-1',
            title: 'some-title-1',
          },
        },
        invited: 'created-date-1',
        created: 'created-date-1',
      },
      {
        userId: 'user-id-2',
        alias: {
          surname: 'some-surname-2',
          givenNames: 'some-given-names-2',
          title: 'some-title-2',
          name: {
            surname: 'some-surname-2',
            givenNames: 'some-given-names-2',
            title: 'some-title-2',
          },
        },
        invited: 'created-date-2',
        created: 'created-date-2',
      },
    ])

    expect(Manuscript.find).toHaveBeenCalledTimes(1)
    expect(Manuscript.find).toHaveBeenCalledWith('some-manuscript-id')

    expect(
      TeamMember.findAllBySpecialIssueAndRoleAndSearchValue,
    ).toHaveBeenCalledTimes(0)

    expect(
      TeamMember.findAllByJournalAndRoleAndSearchValue,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findAllByJournalAndRoleAndSearchValue,
    ).toHaveBeenCalledWith({
      role: Team.Role.academicEditor,
      journalId: 'some-journal-id',
      searchValue: 'some-search-value',
      eagerLoadRelations: 'user.identities',
    })

    expect(TeamMember.findAllByManuscriptAndRole).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByManuscriptAndRole).toHaveBeenCalledWith({
      manuscriptId: 'some-manuscript-id',
      role: Team.Role.author,
    })

    expect(
      TeamMember.findAllByManuscriptAndRoleAndStatus,
    ).toHaveBeenCalledTimes(1)
    expect(TeamMember.findAllByManuscriptAndRoleAndStatus).toHaveBeenCalledWith(
      {
        manuscriptId: 'some-manuscript-id',
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.declined,
      },
    )

    expect(
      TeamMember.findAllBySubmissionAndRoleAndStatuses,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.findAllBySubmissionAndRoleAndStatuses,
    ).toHaveBeenCalledWith({
      role: Team.Role.academicEditor,
      statuses: [TeamMember.Statuses.removed],
      submissionId: 'some-submission-id',
    })

    expect(
      TeamMember.orderAcademicEditorsByScoreAndWorkloadAndEmail,
    ).toHaveBeenCalledTimes(1)
    expect(
      TeamMember.orderAcademicEditorsByScoreAndWorkloadAndEmail,
    ).toHaveBeenCalledWith({
      academicEditors: [{ userId: 'user-id-1' }, { userId: 'user-id-2' }],
      manuscript: mockManuscript,
    })
  })
})
