const {
  generateTeam,
  getTeamRoles,
  generateTeamMember,
  generateManuscript,
  getManuscriptStatuses,
  getTeamMemberStatuses,
} = require('component-generators')

const { inviteAcademicEditorUseCase } = require('../../src/useCases')

let models = {}
let Manuscript = {}
let Team = {}
let TeamMember = {}

const invitationsService = {
  sendInvitationToAcademicEditor: jest.fn(),
}

const removalJobsService = {
  scheduleRemovalJobForAcademicEditor: jest.fn(),
}

const emailJobsService = {
  scheduleEmailsWhenAcademicEditorIsInvited: jest.fn(),
}

const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

const jobsService = {
  cancelJobs: jest.fn(),
}

const logger = {
  error: jest.fn(),
}

const trx = {
  commit: jest.fn(),
  rollback: jest.fn(),
}

const transaction = {
  start: jest.fn(() => trx),
}

const logEvent = jest.fn(async () => {})
logEvent.actions = {
  invitation_sent: 'invitation_sent',
}
logEvent.objectType = { manuscript: 'manuscript' }

describe('Invite academic editor use-case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    models = {
      Journal: {
        find: jest.fn(),
      },
      Manuscript: {
        knex: jest.fn(),
        Statuses: getManuscriptStatuses(),
        findManuscriptsBySubmissionId: jest.fn(),
      },
      Team: {
        Role: getTeamRoles(),
        findOrCreate: jest.fn(),
      },
      TeamMember: {
        Statuses: getTeamMemberStatuses(),
        findSubmittingAuthor: jest.fn(),
        findOneByManuscriptAndRole: jest.fn(),
        findAllByManuscriptAndRole: jest.fn(),
        findOneByManuscriptAndRoleAndStatus: jest.fn(),
        findAllBySubmissionAndRoleAndStatuses: jest.fn(),
      },
      User: {
        find: jest.fn(),
      },
    }
    ;({ Manuscript, Team, TeamMember } = models)
  })
  it('Throws an error if the manuscript has Deleted status', async () => {
    const manuscript = generateManuscript({
      submissionId: 'some-submission-id',
      isLatestVersion: true,
      status: Manuscript.Statuses.deleted,
    })

    jest
      .spyOn(Manuscript, 'findManuscriptsBySubmissionId')
      .mockResolvedValue([manuscript])

    const res = inviteAcademicEditorUseCase.initialize({ models }).execute({
      submissionId: manuscript.submissionId,
    })

    return expect(res).rejects.toThrow(
      `Cannot invite an Academic Editor when the manuscript status is ${manuscript.status}`,
    )
  })
  it('Throws an error if the academic editor is already invited', async () => {
    const manuscript = generateManuscript({
      submissionId: 'some-submission-id',
      isLatestVersion: true,
      getEditorLabel: jest.fn(() => 'academic editor'),
    })
    const academicEditor = generateTeamMember({
      userId: 'academic-editor-user-id',
    })

    jest
      .spyOn(Manuscript, 'findManuscriptsBySubmissionId')
      .mockResolvedValue([manuscript])

    jest
      .spyOn(TeamMember, 'findAllByManuscriptAndRole')
      .mockResolvedValue([academicEditor])

    const res = inviteAcademicEditorUseCase
      .initialize({
        models,
      })
      .execute({
        submissionId: manuscript.submissionId,
        userId: academicEditor.userId,
      })

    return expect(res).rejects.toThrow(
      'The academic editor is already invited.',
    )
  })
  it('Throws an error if the academic editor is removed on an older version', async () => {
    const manuscript = generateManuscript({
      submissionId: 'some-submission-id',
      isLatestVersion: true,
      getEditorLabel: jest.fn(() => 'academic editor'),
    })
    const academicEditor = generateTeamMember({
      userId: 'academic-editor-user-id',
      status: TeamMember.Statuses.removed,
    })

    jest
      .spyOn(Manuscript, 'findManuscriptsBySubmissionId')
      .mockResolvedValue([manuscript])
    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue([])
    jest
      .spyOn(TeamMember, 'findAllBySubmissionAndRoleAndStatuses')
      .mockResolvedValueOnce([])
    jest
      .spyOn(TeamMember, 'findAllBySubmissionAndRoleAndStatuses')
      .mockResolvedValueOnce([academicEditor])

    const res = inviteAcademicEditorUseCase
      .initialize({
        models,
      })
      .execute({
        submissionId: manuscript.submissionId,
        userId: academicEditor.userId,
      })

    return expect(res).rejects.toThrow("The academic editor can't be invited.")
  })
  it('Successfully executes the flow', async () => {
    const manuscript = generateManuscript({
      submissionId: 'some-submission-id',
      isLatestVersion: true,
      getEditorLabel: jest.fn(() => 'academic editor'),
      $query: jest.fn(() => ({
        update: jest.fn(),
      })),
    })
    const academicEditor = generateTeamMember({
      userId: 'academic-editor-user-id',
      $query: jest.fn(() => ({
        insert: jest.fn(() => ({
          onConflict: jest.fn(() => ({
            merge: jest.fn(),
          })),
        })),
      })),
    })
    const academicEditorTeam = generateTeam({
      role: Team.Role.academicEditor,
      addMember: jest.fn(() => academicEditor),
    })

    jest
      .spyOn(Manuscript, 'findManuscriptsBySubmissionId')
      .mockResolvedValue([manuscript])
    jest.spyOn(TeamMember, 'findAllByManuscriptAndRole').mockResolvedValue([])
    jest
      .spyOn(TeamMember, 'findAllBySubmissionAndRoleAndStatuses')
      .mockResolvedValue([])
    jest.spyOn(Team, 'findOrCreate').mockResolvedValue(academicEditorTeam)
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue({})
    jest.spyOn(TeamMember, 'findOneByManuscriptAndRole').mockResolvedValue({})
    jest.spyOn(TeamMember, 'findSubmittingAuthor').mockResolvedValue({})

    await inviteAcademicEditorUseCase
      .initialize({
        models,
        logger,
        logEvent,
        jobsService,
        transaction,
        eventsService,
        emailJobsService,
        removalJobsService,
        invitationsService,
      })
      .execute({
        submissionId: manuscript.submissionId,
        userId: academicEditor.userId,
      })

    expect(trx.commit).toBeCalledTimes(1)
    expect(trx.rollback).toBeCalledTimes(0)

    expect(
      invitationsService.sendInvitationToAcademicEditor,
    ).toHaveBeenCalledTimes(1)
    expect(
      emailJobsService.scheduleEmailsWhenAcademicEditorIsInvited,
    ).toHaveBeenCalledTimes(1)
    expect(
      removalJobsService.scheduleRemovalJobForAcademicEditor,
    ).toHaveBeenCalledTimes(1)
    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledTimes(1)
    expect(logEvent).toBeCalledTimes(1)
  })
})
