/* eslint-disable sonarjs/no-duplicate-string */
const { get } = require('lodash')
const config = require('config')
const { logger } = require('component-logger')

const bccAddress = config.get('bccAddress')
const declineReviewPath = config.get('decline-review.url')
const acceptReviewPath = config.get('accept-review.url')
const acceptReviewNewUserPath = config.get('accept-review-new-user.url')

const initialize = ({
  baseUrl,
  urlService,
  footerText,
  unsubscribeSlug,
  getModifiedText,
}) => ({
  getProps({
    toUser,
    subject,
    paragraph,
    fromEmail,
    manuscript,
    journalName,
    signatureName,
  }) {
    return {
      fromEmail,
      type: 'user',
      bcc: bccAddress,
      templateType: 'notification',
      toUser: {
        email: get(toUser, 'alias.email', ''),
        name: get(toUser, 'alias.surname', ''),
      },
      content: {
        subject,
        paragraph,
        signatureName,
        signatureJournal: journalName,
        ctaText: 'MANUSCRIPT DETAILS',
        ctaLink: urlService.createUrl({
          baseUrl,
          slug: `/details/${manuscript.submissionId}/${manuscript.id}`,
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: toUser.alias.email,
        }),
        unsubscribeLink: urlService.createUrl({
          baseUrl,
          slug: unsubscribeSlug,
          queryParams: {
            id: toUser.userId,
            token: get(toUser, 'user.unsubscribeToken'),
          },
        }),
      },
    }
  },
  getEmailData({
    toUser,
    fromUser,
    manuscript,
    journal,
    editorialAssistant,
    submittingAuthor,
    articleTypeName = '',
    additionalComments = 'no comments provided',
    emailContentChanges = [],
    authors = [],
  }) {
    try {
      const senderSurname = get(fromUser, 'alias.surname', '')
      const senderGivenName = get(fromUser, 'alias.givenNames', '')
      const senderEmail = get(fromUser, 'alias.email', '')

      const recipientSurname = get(toUser, 'alias.surname', '')
      const recipientGivenName = get(toUser, 'alias.givenNames', '')
      const recipientEmail = get(toUser, 'alias.email', '')

      const editorialAssistantSurname = get(
        editorialAssistant,
        'alias.surname',
        '',
      )
      const editorialAssistantGivenName = get(
        editorialAssistant,
        'alias.givenNames',
        '',
      )
      const editorialAssistantEmail = get(editorialAssistant, 'alias.email', '')

      const {
        id: manuscriptId,
        submissionId,
        abstract,
        customId,
        title,
      } = manuscript

      const sortedAuthors = authors.sort((a, b) => a.position - b.position)

      const {
        reviewerAgreeLink,
        reviewerDeclineLink,
      } = this.getReviewerInvitationLinks({
        manuscriptId,
        submissionId,
        toUser,
      })

      return {
        templatePlaceholders: {
          SENDER_GIVENNAME: senderGivenName,
          SENDER_SURNAME: senderSurname,
          SENDER_EMAIL: senderEmail,
          SENDER_TITLE: 'Dr.',

          RECIPIENT_GIVENNAME: recipientGivenName,
          RECIPIENT_SURNAME: recipientSurname,
          RECIPIENT_EMAIL: recipientEmail,
          RECIPIENT_TITLE: 'Dr.',

          MANUSCRIPT_ID: customId,
          MANUSCRIPT_TITLE: title,
          MANUSCRIPT_TYPE: articleTypeName,
          MANUSCRIPT_DETAILS_LINK: urlService.createUrl({
            baseUrl,
            slug: `/details/${submissionId}/${manuscriptId}`,
          }),
          MANUSCRIPT_ABSTRACT: abstract,

          REVIEWER_TITLE: 'Dr.',
          REVIEWER_SURNAME: fromUser.getLastName(),

          SUBMITTING_AUTHOR_SURNAME: get(submittingAuthor, 'alias.surname', ''),
          SUBMITTING_AUTHOR_GIVENNAME: get(
            submittingAuthor,
            'alias.givenNames',
            '',
          ),
          SUBMITTING_AUTHOR_EMAIL: get(submittingAuthor, 'alias.email', ''),
          SUBMITTING_AUTHOR_TITLE: 'Dr.',

          UNSUBSCRIBE_LINK: urlService.createUrl({
            baseUrl,
            slug: unsubscribeSlug,
            queryParams: {
              id: toUser.userId,
              token: get(toUser, 'user.unsubscribeToken'),
            },
          }),

          JOURNAL_NAME: journal.name,
          ADDITIONAL_COMMENTS: additionalComments,
          AUTHORS_NAME_LIST: sortedAuthors.map(
            author =>
              `${get(author, 'alias.givenNames', '')} ${get(
                author,
                'alias.surname',
              )}`,
          ),
          AUTHOR_AFFILIATIONS_LIST: sortedAuthors.map(author =>
            get(author, 'alias.aff'),
          ),
          EDITORIAL_ASSISTANT_GIVENNAME: editorialAssistantGivenName,
          EDITORIAL_ASSISTANT_SURNAME: editorialAssistantSurname,
          REVIEWER_INVITATION_AGREE_LINK: reviewerAgreeLink,
          REVIEWER_INVITATION_DECLINE_LINK: reviewerDeclineLink,
        },
        emailInput: {
          sender: {
            name: journal.name,
            email: journal.email,
          },
          recipient: {
            name: `${recipientGivenName} ${recipientSurname}`,
            email: recipientEmail,
          },
          replyTo: {
            name: journal.name,
            email: editorialAssistantEmail,
          },
          bcc: [{ email: bccAddress }],
          updatedHtmlBodies: emailContentChanges,
        },
      }
    } catch (error) {
      logger.error('Failed to get email data', error)
      throw new Error('Failed to get email data')
    }
  },

  getReviewerInvitationLinks({ manuscriptId, submissionId, toUser }) {
    const recipientSurname = get(toUser, 'alias.surname', '')
    const recipientGivenName = get(toUser, 'alias.givenNames', '')
    const recipientEmail = get(toUser, 'alias.email', '')
    const recipientAff = get(toUser, 'alias.aff', '')
    const recipientCountry = get(toUser, 'alias.country', '')

    const reviewerDeclineLink = urlService.createUrl({
      baseUrl,
      slug: declineReviewPath,
      queryParams: {
        invitationId: toUser.id,
        manuscriptId,
      },
    })

    let reviewerAgreeLink = urlService.createUrl({
      baseUrl,
      slug: acceptReviewPath,
      queryParams: {
        invitationId: toUser.id,
        submissionId,
        manuscriptId,
      },
    })

    const reviewerIdentity = toUser?.user?.defaultIdentity
    if (!reviewerIdentity?.isConfirmed) {
      reviewerAgreeLink = urlService.createUrl({
        baseUrl,
        slug: acceptReviewNewUserPath,
        queryParams: {
          invitationId: toUser.id,
          submissionId,
          manuscriptId,
          email: recipientEmail,
          givenNames: recipientGivenName,
          surname: recipientSurname,
          aff: recipientAff,
          country: recipientCountry,
          token: toUser?.user?.passwordResetToken,
          confirmationToken: toUser?.user?.passwordResetToken,
          phenomId: toUser?.user?.id,
        },
      })
    }
    return { reviewerAgreeLink, reviewerDeclineLink }
  },
})

module.exports = { initialize }
