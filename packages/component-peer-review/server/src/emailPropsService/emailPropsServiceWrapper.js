const config = require('config')

const { getModifiedText } = require('component-transform-text')

const emailPropsService = require('./emailPropsService')
const urlService = require('../urlService/urlService')

const BASE_URL = config.get('pubsweet-client.baseUrl')
const UNSUBSCRIBE_URL = config.get('unsubscribe.url')
const FOOTER_TEXT = config.get('emailFooterText.registeredUsers')

const { getProps } = emailPropsService.initialize({
  urlService,
  getModifiedText,
  baseUrl: BASE_URL,
  footerText: FOOTER_TEXT,
  unsubscribeSlug: UNSUBSCRIBE_URL,
})

module.exports = { getProps }
