const initialize = () => ({
  getCopy({
    titleText,
    emailType,
    targetUserName,
    journalName,
    reviewLink,
    shortReviewLink,
  }) {
    let paragraph
    const hasLink = false
    const hasIntro = true
    const hasSignature = true
    switch (emailType) {
      case 'academic-editor':
        paragraph = `We are pleased to inform you that Dr. ${targetUserName} has submitted a reviewer report for ${titleText}.<br/>
          To see the full report, please visit the manuscript details page at 
        <a style="color: #007e92; text-decoration: none;" href="${reviewLink}">${shortReviewLink}</a>.<br/>
          If you are able to make a decision based on the report(s) that have been submitted, please log this decision in the system.
        If you feel you require more reports, we would be grateful if you could invite more reviewers.<br/>
          If you require any assistance with inviting reviewers or making your decision, please do not hesitate to contact me.`
        break
      case 'reviewer':
        paragraph = `
        Thank you for submitting your reviewer report on ${titleText}, and for taking the time and effort to review this manuscript for ${journalName}. <br/><br/>
        `
        break
      default:
        throw new Error(`The ${emailType} email type is not defined.`)
    }
    return { paragraph, hasLink, hasIntro, hasSignature }
  },
})

module.exports = {
  initialize,
}
