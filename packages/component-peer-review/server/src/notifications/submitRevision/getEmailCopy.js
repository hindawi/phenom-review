const initialize = () => ({
  getCopy({ titleText, emailType, targetUserName }) {
    let paragraph
    const hasLink = true
    let hasIntro = true
    const hasSignature = true
    switch (emailType) {
      case 'triage-editor':
        paragraph = `The authors of the manuscript titled "${titleText}" by ${targetUserName} have submitted a revised version. <br/><br/>
      To review this new submission and proceed with the review process, please visit the manuscript details page.`
        break
      case 'academic-editor':
        hasIntro = false
        paragraph = `The authors of the manuscript titled "${titleText}" by ${targetUserName} have submitted a revised version. <br/><br/>
          To review this new submission and proceed with the review process, please visit the manuscript details page.`
        break
      default:
        throw new Error(`The ${emailType} email type is not defined.`)
    }
    return { paragraph, hasLink, hasIntro, hasSignature }
  },
})

module.exports = {
  initialize,
}
