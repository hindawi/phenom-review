const initialize = ({ Email, getPropsService, getEmailCopyService }) => ({
  async notifyEQA({
    journalName,
    manuscript,
    publishingMember,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { customId, title } = manuscript
    const submittingAuthorName = submittingAuthor.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      emailType: 'EQA-manuscript-published',
      titleText: `The manuscript titled "${title}" by ${submittingAuthorName}`,
      approvalEditorName: publishingMember.getName(),
    })
    const emailProps = getPropsService.getProps({
      paragraph,
      manuscript,
      toUser: editorialAssistant,
      signatureName: journalName,
      subject: `${customId}: Manuscript decision finalized`,
      fromEmail: `${journalName} <${editorialAssistant.alias.email}>`,
    })
    emailProps.bodyProps = bodyProps

    const email = new Email(emailProps)
    await email.sendEmail()
  },

  async notifyTriageEditorForSubmittedReport({
    journal,
    manuscript,
    triageEditor,
    articleTypeName,
    submittingAuthor,
    editorialAssistant,
    eventsService,
  }) {
    const emailProps = getPropsService.getEmailData({
      manuscript,
      journal,
      toUser: triageEditor,
      fromUser: editorialAssistant,
      articleTypeName,
      submittingAuthor,
      editorialAssistant,
    })

    eventsService.publishSendEmailEvent({
      usecase: 'DecisionToPublish',
      eventData: emailProps,
    })
  },

  async notifyAcademicEditorForSubmittedReport({
    journal,
    manuscript,
    academicEditor,
    articleTypeName,
    submittingAuthor,
    editorialAssistant,
    eventsService,
  }) {
    const emailProps = getPropsService.getEmailData({
      manuscript,
      journal,
      toUser: academicEditor,
      fromUser: editorialAssistant,
      articleTypeName,
      submittingAuthor,
      editorialAssistant,
    })

    eventsService.publishSendEmailEvent({
      usecase: 'DecisionToPublish',
      eventData: emailProps,
    })
  },
})

module.exports = {
  initialize,
}
