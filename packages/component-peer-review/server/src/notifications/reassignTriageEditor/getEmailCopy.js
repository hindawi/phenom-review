const initialize = () => ({
  getEmailCopy({
    emailType,
    titleText,
    journalName,
    triageEditorLabel,
    editorialAssistantEmail,
  }) {
    let upperContent, manuscriptText, subText, lowerContent, paragraph
    let hasLink = true
    let hasIntro = true
    let hasSignature = true
    switch (emailType) {
      case 'triage-editor-removed':
        hasIntro = true
        hasLink = false
        hasSignature = true
        paragraph = `You have been removed from the manuscript "${titleText}".<br/><br/>
        If you have any questions regarding this action, please let us know at ${editorialAssistantEmail}.<br/><br/>
        Thank you for reviewing ${journalName}.`
        break
      case 'triage-editor-assigned':
        hasIntro = true
        hasLink = false
        hasSignature = true
        paragraph = `You have been assigned as ${triageEditorLabel} on the manuscript "${titleText}".<br/><br/>
        If you have any questions regarding this action, please let us know at ${editorialAssistantEmail}.<br/><br/>
        Thank you for reviewing ${journalName}.`
        break
      default:
        throw new Error(`The ${emailType} email type is not defined.`)
    }
    return {
      hasLink,
      subText,
      hasIntro,
      paragraph,
      upperContent,
      lowerContent,
      hasSignature,
      manuscriptText,
    }
  },
})

module.exports = {
  initialize,
}
