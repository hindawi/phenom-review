const initialize = () => ({
  getCopy({
    emailType,
    titleText,
    staffEmail,
    journalName,
    comments,
    targetUserName,
  }) {
    let paragraph
    let hasLink = true
    let hasIntro = true
    const hasSignature = true
    switch (emailType) {
      case 'accepted-reviewers-after-recommendation':
      case 'pending-reviewers-after-recommendation':
        hasLink = false
        paragraph = `I appreciate any time you may have spent reviewing ${titleText}. However, I am prepared to make an editorial decision and your review is no longer required at this time. I apologize for any inconvenience. <br/><br/>
          If you have comments on this manuscript you believe I should see, please email them to ${staffEmail} as soon as possible. <br/><br/>
          Thank you for your interest and I hope you will consider reviewing for ${journalName} again.`
        break
      case 'triage-editor-recommend-to-publish':
        hasIntro = false
        paragraph = `Dr. ${targetUserName} has recommended accepting ${titleText} for publication.<br/>
            ${comments}<br/>
            To review this decision, please visit the manuscript details page.`
        break
      case 'triage-editor-recommend-to-reject':
        hasIntro = false
        paragraph = `Dr. ${targetUserName} has recommended rejecting ${titleText}.<br/><br/>
            ${comments}<br/><br/>
            To review this decision, please visit the manuscript details page.`
        break
      default:
        throw new Error(`The ${emailType} email type is not defined.`)
    }
    return { paragraph, hasLink, hasIntro, hasSignature }
  },
})

module.exports = {
  initialize,
}
