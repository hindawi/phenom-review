const config = require('config')

const staffEmail = config.get('staffEmail')

const computeCallForPapersLink = ({ journalCode, specialIssueCustomId }) =>
  `https://www.hindawi.com/journals/${journalCode}/si/${specialIssueCustomId}/`

const initialize = ({ Email, getPropsService, getEmailCopyService }) => ({
  async notifyTriageEditor({
    journal,
    manuscript,
    triageEditor,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
    academicEditorLabel,
  }) {
    const { name: journalName } = journal
    const { customId, title } = manuscript
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const editorialAssistantName = editorialAssistant.getName()
    const approvalEditorName = academicEditor.getName()
    const submittingAuthorName = submittingAuthor.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      academicEditorLabel,
      emailType: 'academic-editor-accepted',
      targetUserName: approvalEditorName,
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
    })
    const emailProps = getPropsService.getProps({
      toUser: triageEditor,
      subject: `${customId}: Editor invitation accepted`,
      paragraph,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      manuscript,
      journalName,
      signatureName: editorialAssistantName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
  async notifyActiveAcademicEditor({
    journal,
    manuscript,
    academicEditor,
    editorialAssistant,
    triageEditor,
    submittingAuthorName,
    academicEditorLabel,
  }) {
    const { name: journalName } = journal
    const { customId, title } = manuscript
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const editorialAssistantName = editorialAssistant.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      staffEmail,
      academicEditorLabel,
      emailType: 'academic-editor-revoked',
      targetUserName: triageEditor.getName(),
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
    })
    const emailProps = getPropsService.getProps({
      toUser: academicEditor,
      subject: `${customId}: Editor invitation cancelled`,
      paragraph,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      manuscript,
      journalName,
      signatureName: editorialAssistantName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
  async notifyPendingAcademicEditor({
    journal,
    manuscript,
    academicEditor,
    editorialAssistant,
  }) {
    const { name: journalName } = journal
    const { customId } = manuscript

    const editorialAssistantEmail = editorialAssistant.getEmail()
    const editorialAssistantName = editorialAssistant.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      emailType: 'academic-editor-after-accepted-invitation',
    })

    const emailProps = getPropsService.getProps({
      toUser: academicEditor,
      subject: `${customId}: Inviting reviewers`,
      paragraph,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      manuscript,
      journalName,
      signatureName: editorialAssistantName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
  async notifyPendingAcademicEditorAfterConflictOfInterests({
    journal,
    manuscript,
    specialIssue,
    academicEditor,
    editorialAssistant,
  }) {
    const { name: journalName, code: journalCode } = journal
    const { customId } = manuscript

    const editorialAssistantEmail = editorialAssistant.getEmail()
    const editorialAssistantName = editorialAssistant.getName()

    let callForPapersLink
    if (specialIssue) {
      callForPapersLink = computeCallForPapersLink({
        journalCode,
        specialIssueCustomId: specialIssue.customId,
      })
    }

    const emailType = manuscript.specialIssueId
      ? 'academic-editor-accepted-invitation-after-conflict-of-interests-special-issue'
      : 'academic-editor-accepted-invitation-after-conflict-of-interests-regular-issue'

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      specialIssueName: (specialIssue && specialIssue.name) || '',
      manuscriptCustomId: manuscript.customId,
      manuscriptTitle: manuscript.title,
      callForPapersLink,
      emailType,
    })

    const emailProps = getPropsService.getProps({
      toUser: academicEditor,
      subject: `${customId}: Reassessment required due to potential Conflict of Interest`,
      paragraph,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      manuscript,
      journalName,
      signatureName: editorialAssistantName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
  async notifyEditorialAssistant({
    journal,
    manuscript,
    academicEditor,
    submittingAuthor,
    academicEditorLabel,
    editorialAssistant,
  }) {
    const { name: journalName } = journal
    const { customId, title } = manuscript
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const submittingAuthorName = submittingAuthor.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      emailType: 'academic-editor-accepted',
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
      targetUserName: academicEditor.getName(),
      academicEditorLabel,
    })
    const emailProps = getPropsService.getProps({
      paragraph,
      manuscript,
      journalName,
      toUser: editorialAssistant,
      subject: `${customId}: Editor invitation accepted`,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = {
  initialize,
}
