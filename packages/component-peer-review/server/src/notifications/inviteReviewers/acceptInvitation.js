const { get } = require('lodash')

const initialize = ({
  Email,
  getPropsService,
  getEmailCopyService,
  getExpectedDate,
}) => ({
  async notifyAcademicEditor({
    user,
    journal,
    manuscript,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { title } = manuscript
    const { name: journalName } = journal
    const titleText = `the manuscript titled "${title}" by ${submittingAuthor.getName()}`
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const editorialAssistantName = editorialAssistant.getName()

    const emailType = 'reviewer-accepted'

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      emailType,
      titleText,
      journalName,
      expectedDate: getExpectedDate({
        timestamp: Date.now(),
        daysExpected: 14,
      }),
      targetUserName: user.alias.surname,
    })

    const emailProps = getPropsService.getProps({
      manuscript,
      paragraph,
      journalName,
      toUser: academicEditor,
      signatureName: editorialAssistantName,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      subject: `${manuscript.customId}: A reviewer has agreed`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },

  async notifyReviewer({
    user,
    journal,
    manuscript,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { title } = manuscript
    const { name: journalName } = journal
    const titleText = `the manuscript titled "${title}" by ${submittingAuthor.getName()}`
    const editorialAssistantEmail = get(editorialAssistant, 'alias.email')
    const editorialAssistantName = editorialAssistant.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      emailType: 'reviewer-thank-you',
      titleText,
      journalName,
      editorialAssistantEmail,
      expectedDate: getExpectedDate({
        timestamp: new Date(),
        daysExpected: 14,
      }),
    })

    const emailProps = getPropsService.getProps({
      paragraph,
      manuscript,
      journalName,
      toUser: user,
      signatureName: editorialAssistantName,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      subject: `${manuscript.customId}: Thank you for agreeing to review`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = {
  initialize,
}
