const initialize = () => ({
  getEmailCopy({
    emailType,
    titleText,
    expectedDate,
    targetUserName,
    journalName,
    reviewLink,
    shortReviewLink,
    editorialAssistantEmail,
  }) {
    let upperContent, manuscriptText, subText, lowerContent, paragraph
    let hasLink = true
    const resend = false
    const hasIntro = true
    const hasSignature = true

    switch (emailType) {
      case 'reviewer-accepted':
        paragraph = `We are pleased to inform you that Dr. ${targetUserName} has agreed to review ${titleText}.<br/>
        You should receive the report before ${expectedDate}.`
        hasLink = false
        break
      case 'reviewer-declined':
        paragraph = `We regret to inform you that Dr. ${targetUserName} has declined to review ${titleText}.<br/>
        Therefore, please log into your 
        <a style="color: #007e92; text-decoration: none;" href="${reviewLink}">${shortReviewLink}</a>
        account to invite additional reviewers on this manuscript.`
        break
      case 'reviewer-thank-you':
        paragraph = `Thank you for agreeing to review ${titleText}.<br/><br/>
      You can view the full PDF file of the manuscript and post your review report using the link below by ${expectedDate}.<br/>
      You will be asked to log in before being able to complete your review.`
        break
      case 'reviewer-cancel-invitation':
        paragraph = `We recently invited you to review ${titleText} for ${journalName}.<br/>
      This is to confirm that we no longer require your review.<br/><br/>
      If you have comments on the manuscript you believe I should see, please email them to ${editorialAssistantEmail} as soon as possible.<br/><br/>
      Thank you for your time and I hope you will consider reviewing for ${journalName} in the future.`
        hasLink = false
        break
      default:
        throw new Error(`The ${emailType} email type is not defined.`)
    }
    return {
      resend,
      hasLink,
      paragraph,
      subText,
      upperContent,
      lowerContent,
      manuscriptText,
      hasIntro,
      hasSignature,
    }
  },
})

module.exports = {
  initialize,
}
