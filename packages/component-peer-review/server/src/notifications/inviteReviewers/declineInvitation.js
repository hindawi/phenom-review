const config = require('config')

const publisherConfig = config.get('publisherConfig')

const initialize = ({
  Email,
  getPropsService,
  getEmailCopyService,
  getExpectedDate,
}) => ({
  async notifyAcademicEditor({
    user,
    journal,
    manuscript,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const {
      links: { reviewLink },
      emailData: { shortReviewLink },
    } = publisherConfig
    const { title } = manuscript
    const { name: journalName } = journal
    const titleText = `the manuscript titled "${title}" by ${submittingAuthor.getName()}`
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const editorialAssistantName = editorialAssistant.getName()
    const emailType = 'reviewer-declined'

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      emailType,
      titleText,
      journalName,
      reviewLink,
      shortReviewLink,
      expectedDate: getExpectedDate({
        timestamp: Date.now(),
        daysExpected: 14,
      }),
      targetUserName: user.alias.surname,
    })

    const emailProps = getPropsService.getProps({
      manuscript,
      toUser: academicEditor,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      subject: `${manuscript.customId}: A reviewer has declined`,
      paragraph,
      signatureName: editorialAssistantName,
      journalName,
      reviewLink,
      shortReviewLink,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = {
  initialize,
}
