const initialize = ({ Email, getPropsService, getEmailCopyService }) => ({
  async notifySubmittingAuthor({
    journal,
    manuscript,
    editor,
    submittingAuthor,
    editorialAssistant,
    emailContentChanges,
    additionalComments,
    eventsService,
  }) {
    const eventData = getPropsService.getEmailData({
      manuscript,
      journal,
      toUser: submittingAuthor,
      fromUser: editor,
      submittingAuthor,
      emailContentChanges,
      additionalComments,
      editorialAssistant,
    })

    eventsService.publishSendEmailEvent({
      usecase: 'EditorialRecommendationToRevise(Author)',
      eventData,
    })
  },
  async notifyReviewers({
    journal,
    reviewers,
    manuscript,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { name: journalName } = journal

    const titleText = `the manuscript titled "${
      manuscript.title
    }" by ${submittingAuthor.getName()}`

    const academicEditorName = academicEditor.getName()

    const editorialAssistantEmail = editorialAssistant.alias.email
    const reviewersEmailBody = getEmailCopyService.getCopy({
      emailType: 'reviewers',
      titleText,
      journalName,
      editorialAssistantEmail: editorialAssistant.alias.email,
    })

    const buildSendEmailFunction = emailBodyProps => async reviewer => {
      const { paragraph, ...bodyProps } = emailBodyProps
      const emailProps = getPropsService.getProps({
        manuscript,
        paragraph,
        journalName,
        toUser: reviewer,
        signatureName: academicEditorName,
        fromEmail: `${journalName} <${editorialAssistantEmail}>`,
        subject: `${manuscript.customId}: Review no longer required`,
      })

      emailProps.bodyProps = bodyProps
      const email = new Email(emailProps)
      await email.sendEmail()
    }

    return Promise.all([
      ...reviewers.map(buildSendEmailFunction(reviewersEmailBody)),
    ])
  },
  async notifyTriageEditor({
    journal,
    manuscript,
    triageEditor,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
    eventsService,
  }) {
    const eventData = getPropsService.getEmailData({
      toUser: triageEditor,
      fromUser: academicEditor,
      manuscript,
      journal,
      submittingAuthor,
      editorialAssistant,
    })

    eventsService.publishSendEmailEvent({
      usecase: 'RequestRevision',
      eventData,
    })
  },
})

module.exports = {
  initialize,
}
