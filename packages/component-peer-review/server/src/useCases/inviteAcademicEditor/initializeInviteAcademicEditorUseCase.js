const models = require('@pubsweet/models')
const config = require('config')

const Email = require('component-sendgrid')
const { logEvent } = require('component-activity-log/server')
const { getModifiedText } = require('component-transform-text')
const urlService = require('../../urlService/urlService')
const events = require('component-events')
const { logger } = require('component-logger')
const { transaction } = require('objection')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const inviteAcademicEditorDependencies = {
  invitations: require('../../invitations/invitations'),
  emailJobs: require('../../emailJobs/academicEditor/emailJobs'),
  removalJobs: require('../../removalJobs/academicEditorRemovalJobs'),
  getProps: require('../../emailPropsService/emailPropsService'),
  jobs: require('../../jobsService/jobsService'),
}

function initializeInviteAcademicEditorUseCase(inviteAcademicEditorUseCase) {
  const eventsService = events.initialize({ models })

  const { Manuscript, TeamMember, Job, Team } = models
  const getPropsServiceInviteAcademicEditor = inviteAcademicEditorDependencies.getProps.initialize(
    {
      baseUrl,
      urlService,
      footerText,
      unsubscribeSlug,
      getModifiedText,
    },
  )
  const invitationsService = inviteAcademicEditorDependencies.invitations.initialize(
    { Email, getPropsService: getPropsServiceInviteAcademicEditor },
  )
  const emailJobsService = inviteAcademicEditorDependencies.emailJobs.initialize(
    {
      Job,
      Email,
      logEvent,
      getPropsService: getPropsServiceInviteAcademicEditor,
    },
  )
  const removalJobsService = inviteAcademicEditorDependencies.removalJobs.initialize(
    {
      Job,
      logEvent,
      TeamMember,
      Team,
      Manuscript,
      eventsService,
    },
  )
  const jobsService = inviteAcademicEditorDependencies.jobs.initialize({
    models,
  })

  return inviteAcademicEditorUseCase.initialize({
    models,
    logger,
    logEvent,
    transaction,
    jobsService,
    eventsService,
    emailJobsService,
    invitationsService,
    removalJobsService,
  })
}

module.exports = { initializeInviteAcademicEditorUseCase }
