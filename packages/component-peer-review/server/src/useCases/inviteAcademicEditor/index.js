const getAllAcademicEditorsUseCase = require('./getAllAcademicEditors')
const getAcademicEditorsUseCase = require('./getAcademicEditors')
const inviteAcademicEditorUseCase = require('./inviteAcademicEditor')
const acceptAcademicEditorInvitationUseCase = require('./acceptInvitation')
const cancelAcademicEditorInvitationUseCase = require('./cancelInvitation')
const declineAcademicEditorInvitationUseCase = require('./declineInvitation')
const acceptNormalAcademicEditorInvitationUseCase = require('./acceptNormalInvitation')
const acceptReassignmentAcademicEditorInvitationUseCase = require('./acceptReassignmentInvitation')
const ingestEditorSuggestionsUseCase = require('./ingestEditorSuggestions')
const inviteSuggestedAcademicEditorUseCase = require('./inviteSuggestedAcademicEditor')
const {
  initializeInviteAcademicEditorUseCase,
} = require('./initializeInviteAcademicEditorUseCase')

module.exports = {
  getAllAcademicEditorsUseCase,
  getAcademicEditorsUseCase,
  inviteAcademicEditorUseCase,
  acceptAcademicEditorInvitationUseCase,
  cancelAcademicEditorInvitationUseCase,
  declineAcademicEditorInvitationUseCase,
  acceptNormalAcademicEditorInvitationUseCase,
  acceptReassignmentAcademicEditorInvitationUseCase,
  ingestEditorSuggestionsUseCase,
  initializeInviteAcademicEditorUseCase,
  inviteSuggestedAcademicEditorUseCase,
}
