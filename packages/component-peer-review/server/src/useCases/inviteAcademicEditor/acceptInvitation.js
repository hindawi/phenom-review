const initialize = ({
  models,
  logEvent,
  useCases,
  jobsService,
  eventsService,
  emailJobsService,
  notificationService,
  models: { Manuscript, Team, TeamMember },
}) => ({
  async execute({ teamMemberId, userId, useCommunication }) {
    const manuscript = await Manuscript.findManuscriptByTeamMember(teamMemberId)
    const pendingAcademicEditor = await TeamMember.find(teamMemberId)

    if (userId !== pendingAcademicEditor.userId)
      throw new ValidationError(
        'You are logged in with another user. Log out and try again.',
      )

    const activeAcademicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
        manuscriptId: manuscript.id,
      },
    )

    if (pendingAcademicEditor.status !== TeamMember.Statuses.pending)
      throw new ValidationError('User already responded to the invitation.')

    if (manuscript.status === Manuscript.Statuses.rejected)
      throw new ValidationError('Cannot accept invitation in this status')

    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    )
      throw new AuthorizationError('Unauthorized')

    let useCase = 'acceptNormalAcademicEditorInvitationUseCase'
    if (pendingAcademicEditor && activeAcademicEditor)
      useCase = 'acceptReassignmentAcademicEditorInvitationUseCase'

    await useCases[useCase]
      .initialize({
        models,
        logEvent,
        jobsService,
        eventsService,
        emailJobsService,
        notificationService,
      })
      .execute({ teamMemberId, userId, useCommunication })
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
