const { Promise } = require('bluebird')

const initialize = ({
  logEvent,
  jobsService,
  eventsService,
  notificationService,
  models: {
    Job,
    Team,
    User,
    Journal,
    TeamMember,
    Manuscript,
    ArticleType,
    PeerReviewModel,
  },
}) => ({
  async execute({
    input: { teamMemberId, reason, useCommunication = true },
    userId,
  }) {
    const manuscript = await Manuscript.findManuscriptByTeamMember(teamMemberId)
    const pendingAcademicEditor = await TeamMember.find(teamMemberId)

    if (userId !== pendingAcademicEditor.userId)
      throw new ValidationError(
        'You are logged in with another user. Log out and try again.',
      )

    const activeAcademicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
        manuscriptId: manuscript.id,
      },
    )
    const responded = new Date()

    if (pendingAcademicEditor.status !== TeamMember.Statuses.pending) {
      throw new ValidationError('User already responded to the invitation.')
    }
    if (manuscript.status === Manuscript.Statuses.rejected) {
      throw new ValidationError('Cannot accept invitation in this status')
    }
    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new AuthorizationError('Unauthorized')
    }

    // Academic Editor
    const submissionAcademicEditorMembers = await TeamMember.findAllBySubmissionAndRoleAndUser(
      {
        userId,
        role: Team.Role.academicEditor,
        submissionId: manuscript.submissionId,
      },
    )
    await Promise.all(
      submissionAcademicEditorMembers.map(async tm => {
        tm.updateProperties({
          status: TeamMember.Statuses.declined,
          responded,
          declineReason: reason,
        })
        await tm.save()
      }),
    )

    // Manuscript
    if (!activeAcademicEditor) {
      manuscript.updateStatus(Manuscript.Statuses.submitted)
      await manuscript.save()
    }

    // Jobs
    const pendingAcademicEditorJobs = await Job.findAllByTeamMember(
      pendingAcademicEditor.id,
    )
    await jobsService.cancelJobs(pendingAcademicEditorJobs)

    manuscript.articleType = await ArticleType.find(manuscript.articleTypeId)

    if (!useCommunication) {
      const journal = await Journal.find(
        manuscript.journalId,
        'peerReviewModel',
      )
      const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
        {
          manuscriptId: manuscript.id,
          role: Team.Role.editorialAssistant,
          status: TeamMember.Statuses.active,
        },
      )
      const submittingAuthor = await TeamMember.findSubmittingAuthor(
        manuscript.id,
      )

      if (journal.peerReviewModel.hasTriageEditor) {
        const triageEditor = await TeamMember.findTriageEditor({
          TeamRole: Team.Role,
          journalId: journal.id,
          manuscriptId: manuscript.id,
          sectionId: manuscript.sectionId,
        })
        triageEditor.user = await User.find(triageEditor.userId)

        const academicEditorLabel = await manuscript.getEditorLabel({
          Team,
          TeamMember,
          PeerReviewModel,
          role: Team.Role.academicEditor,
        })

        await notificationService.notifyTriageEditor({
          reason,
          journal,
          manuscript,
          triageEditor,
          submittingAuthor,
          editorialAssistant,
          academicEditorLabel,
          academicEditor: pendingAcademicEditor,
        })
      } else {
        await notificationService.notifyEditorialAssistant({
          reason,
          journal,
          manuscript,
          submittingAuthor,
          editorialAssistant,
          academicEditor: pendingAcademicEditor,
        })
      }
    }
    await eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionAcademicEditorDeclined',
    })

    // Activity Log
    await logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.invitation_declined,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
