const initialize = ({ TeamMember, Team }) => {
  const getSpecialIssueAcademicEditors = specialIssueId =>
    TeamMember.findAllBySpecialIssueAndRole({
      role: Team.Role.academicEditor,
      specialIssueId,
    })

  const getSectionAcademicEditors = sectionId =>
    TeamMember.findAllBySectionAndRole({
      role: Team.Role.academicEditor,
      sectionId,
    })

  const getJournalAcademicEditors = journalId =>
    TeamMember.findAllByJournalAndRole({
      role: Team.Role.academicEditor,
      journalId,
    })

  const execute = async ({ journalId, specialIssueId, sectionId } = {}) => {
    if (journalId) {
      return getJournalAcademicEditors(journalId)
    } else if (specialIssueId) {
      return getSpecialIssueAcademicEditors(specialIssueId)
    } else if (sectionId) {
      return getSectionAcademicEditors(sectionId)
    }

    return []
  }

  return {
    execute,
  }
}

const authsomePolicies = [
  'isAuthenticated',
  'isTriageEditor',
  'isEditorialAssistant',
  'admin',
]

module.exports = {
  initialize,
  authsomePolicies,
}
