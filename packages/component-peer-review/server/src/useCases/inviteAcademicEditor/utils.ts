export const getEditorsTeamMemberIds = async ({
  TeamMember,
  Team,
  submissionId,
}) => {
  const submissionAcademicEditors = await TeamMember.findAllBySubmissionAndRole(
    { submissionId, role: Team.Role.academicEditor },
  )
  const submissionTriageEditors = await TeamMember.findAllBySubmissionAndRole({
    submissionId,
    role: Team.Role.triageEditor,
  })
  return [
    ...submissionAcademicEditors.map(ae => ae.id),
    ...submissionTriageEditors.map(te => te.id),
  ]
}
