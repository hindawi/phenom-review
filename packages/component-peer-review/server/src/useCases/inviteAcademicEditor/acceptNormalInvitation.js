const config = require('config')
const { Promise } = require('bluebird')
const { getEditorsTeamMemberIds } = require('./utils')

const editorialAssistantLabel = config.get('roleLabels.editorialAssistant')

const findValidReviews = async ({ submissionId, models }) => {
  const { Review, Team } = models

  const latestMajorEditorialReview = await Review.findLatestEditorialReviewOfSubmissionByRecommendationType(
    {
      submissionId,
      TeamRole: Team.Role,
      recommendationType: Review.Recommendations.major,
    },
  )

  let reviews = await Review.findAllValidAndSubmitedBySubmissionAndRole({
    submissionId,
    role: Team.Role.reviewer,
  })
  if (
    latestMajorEditorialReview &&
    latestMajorEditorialReview.recommendation === Review.Recommendations.major
  ) {
    reviews = reviews.filter(
      review =>
        new Date(review.submitted) >
        new Date(latestMajorEditorialReview.submitted),
    )
  }

  return reviews
}

const determineManuscriptStatus = ({
  acceptedReviewers,
  statuses,
  hasTriageEditorConflictOfInterest,
  isApprovalEditor,
  hasValidReportsOnSubmission,
}) => {
  // +++++++++ description
  //  Set status https://hindawi.atlassian.net/secure/RapidBoard.jspa?rapidView=75&projectKey=PPBK&modal=detail&selectedIssue=PPBK-1646
  // :white_check_mark: If there are no reports available, the AE needs to invite reviewers (AcademicEditorAssigned) do they need to be before/after major? what is a valid report? any report after last major is valid?
  // If there is at least 1 report available on the last major revision, the AE can make a decision for single tier (makeDecision)
  // If there is at least 1 report available on the last major revision, the AE can make a decision recommendation for 2-tier (reviewCompleted) // ok
  // If there is at least 1 report available on the last major version, the AE can make a decision for 2-tier if the TE is in COI (makeDecision)

  if (!hasValidReportsOnSubmission) {
    if (acceptedReviewers.length) {
      return statuses.underReview
    }
    return statuses.academicEditorAssigned
  }
  if (isApprovalEditor) return statuses.makeDecision
  if (hasTriageEditorConflictOfInterest) return statuses.makeDecision
  return statuses.reviewCompleted
}

const initialize = ({
  logEvent,
  jobsService,
  eventsService,
  emailJobsService,
  notificationService,
  models: {
    Job,
    Team,
    User,
    Review,
    Journal,
    Manuscript,
    TeamMember,
    ArticleType,
    SpecialIssue,
    PeerReviewModel,
    PeerReviewEditorialMapping,
  },
}) => ({
  async execute({ teamMemberId, userId, useCommunication = true }) {
    // TODO: take another look at this
    const { services } = require('component-quality-check')
    const { ConflictOfInterest } = services

    // get ae team member instance
    const academicEditorMember = await TeamMember.find(teamMemberId)
    // get manuscript of team member
    const manuscript = await Manuscript.findManuscriptByTeamMember(teamMemberId)
    const articleType = await ArticleType.findArticleTypeByManuscript(
      manuscript.id,
    )

    const submissionReviewerReports = await findValidReviews({
      submissionId: manuscript.submissionId,
      models: {
        Manuscript,
        Review,
        Team,
      },
    })

    // find Academic Editors on all versions of this submissions
    const submissionAcademicEditorMembers = await TeamMember.findAllBySubmissionAndRoleAndUser(
      {
        userId,
        role: Team.Role.academicEditor,
        submissionId: manuscript.submissionId,
      },
    )

    // update their responded date with new Date
    const responded = new Date()
    await Promise.each(submissionAcademicEditorMembers, async tm => {
      tm.updateProperties({ status: TeamMember.Statuses.accepted, responded })
      await tm.save()
    })

    const { hasTriageEditorConflictOfInterest } = manuscript

    const isApprovalEditor = await TeamMember.isApprovalEditor({
      userId,
      manuscriptId: manuscript.id,
      models: {
        Manuscript,
        Team,
        ArticleType,
        PeerReviewModel,
        PeerReviewEditorialMapping,
      },
    })

    const acceptedReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.reviewer,
        manuscriptId: manuscript.id,
        status: TeamMember.Statuses.accepted,
      },
    )

    // determine new status to be saved
    const status = determineManuscriptStatus({
      acceptedReviewers,
      statuses: Manuscript.Statuses,
      hasTriageEditorConflictOfInterest,
      isApprovalEditor,
      hasValidReportsOnSubmission: !!submissionReviewerReports.length,
    })

    manuscript.updateStatus(status)
    await manuscript.save()

    // Jobs
    const academicEditorMemberJobs = await Job.findAllByTeamMember(
      academicEditorMember.id,
    )
    await jobsService.cancelJobs(academicEditorMemberJobs)

    // Notifications
    const journal = await Journal.find(manuscript.journalId)

    const specialIssue = manuscript.specialIssueId
      ? await SpecialIssue.find(manuscript.specialIssueId)
      : undefined

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    academicEditorMember.user = await User.find(academicEditorMember.userId)

    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )

    manuscript.articleType = articleType
    manuscript.journal = journal

    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      journalId: journal.id,
      manuscriptId: manuscript.id,
      sectionId: manuscript.sectionId,
    })
    if (triageEditor) {
      triageEditor.user = await User.find(triageEditor.userId)
    }

    const academicEditorLabel = await manuscript.getEditorLabel({
      Team,
      TeamMember,
      PeerReviewModel,
      role: Team.Role.academicEditor,
    })

    const peerReviewModel = await PeerReviewModel.findOneByManuscriptParent(
      manuscript,
    )
    if (!useCommunication) {
      if (peerReviewModel.hasTriageEditor) {
        notificationService.notifyTriageEditor({
          journal,
          manuscript,
          triageEditor,
          submittingAuthor,
          editorialAssistant,
          academicEditorLabel,
          academicEditor: academicEditorMember,
        })
      } else {
        notificationService.notifyEditorialAssistant({
          journal,
          manuscript,
          submittingAuthor,
          editorialAssistant,
          academicEditorLabel,
          academicEditor: academicEditorMember,
        })
      }
    }

    const teamMemberIds = await getEditorsTeamMemberIds({
      TeamMember,
      Team,
      submissionId: manuscript.submissionId,
    })
    const editorsConflicts = await ConflictOfInterest.getConflictsForTeamMembers(
      teamMemberIds,
    )

    if (editorsConflicts.length) {
      notificationService.notifyPendingAcademicEditorAfterConflictOfInterests({
        journal,
        manuscript,
        specialIssue,
        editorialAssistant,
        academicEditor: academicEditorMember,
      })
    } else {
      notificationService.notifyPendingAcademicEditor({
        journal,
        manuscript,
        editorialAssistant,
        academicEditor: academicEditorMember,
      })
    }

    const triageEditorLabel = await manuscript.getEditorLabel({
      Team,
      TeamMember,
      PeerReviewModel,
      role: Team.Role.triageEditor,
    })
    emailJobsService.sendAcademicEditorRemindersToInviteReviewers({
      manuscript,
      editorialAssistant,
      triageEditorLabel,
      editorialAssistantLabel,
      journalName: journal.name,
      user: academicEditorMember,
      submittingAuthorName: submittingAuthor.getName(),
    })

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionAcademicEditorAccepted',
    })

    // Activity Log
    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.invitation_agreed,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

module.exports = { initialize }
