const { Promise } = require('bluebird')
const { logger } = require('component-logger')

function initialize({
  models: { Manuscript, EditorSuggestion, TeamMember },
  eventsService,
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, submissionId, editorSuggestions }) {
    const lastManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
    })

    if (lastManuscript.status === Manuscript.Statuses.technicalChecks) {
      throw new Error(
        `Automatic academic editor shouldn't be invited on manuscript:${manuscriptId} in technicalChecks status.`,
      )
    }

    if (!Manuscript.InProgressStatuses.includes(lastManuscript.status)) {
      logger.info(
        `Automatic academic editor shouldn't be invited on manuscript:${manuscriptId} in ${lastManuscript.status} status.`,
      )
      return
    }

    const isSuggestionsListInSyncWithDB = await TeamMember.isSuggestionsListInSyncWithTeamMembers(
      editorSuggestions,
    )

    if (!isSuggestionsListInSyncWithDB) {
      return
    }

    const suggestions = await Promise.map(
      editorSuggestions,
      async ({ editorId, score }) => {
        const suggestionAlreadySavedInDB = await EditorSuggestion.findOneBy({
          queryObject: {
            teamMemberId: editorId,
            manuscriptId,
          },
        })

        if (suggestionAlreadySavedInDB) return suggestionAlreadySavedInDB

        return new EditorSuggestion({
          manuscriptId,
          teamMemberId: editorId,
          score,
        })
      },
    )
    await EditorSuggestion.saveAll(suggestions)

    eventsService.publishSubmissionEvent({
      submissionId,
      eventName: 'SubmissionEditorSuggestionsIngested',
    })
  }
}

module.exports = { initialize }
