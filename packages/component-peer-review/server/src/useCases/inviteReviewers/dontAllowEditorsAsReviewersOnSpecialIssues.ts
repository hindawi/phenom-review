interface TeamRole {
  triageEditor: string
  academicEditor: string
}
interface TeamMemberType {
  findAllBySpecialIssueAndUserAndRoles({
    userId,
    specialIssueId,
    roles,
  }): string
}

const initialize = (TeamMember: TeamMemberType) => ({
  execute(userId: string, specialIssueId: string, TeamRole: TeamRole) {
    const specialIssueEditorRoles = TeamMember.findAllBySpecialIssueAndUserAndRoles(
      {
        userId,
        specialIssueId,
        roles: [TeamRole.triageEditor, TeamRole.academicEditor],
      },
    )

    if (specialIssueEditorRoles.length)
      throw new Error(
        'This user cannot be invited because they are already part of the Special Issue team.',
      )
  },
})

export { initialize }
