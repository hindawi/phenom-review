const config = require('config')
const useCases = require('./index')
const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const { logEvent } = require('component-activity-log/server')
const Email = require('component-sendgrid')
const { getModifiedText } = require('component-transform-text')
const events = require('component-events')
const { transaction } = require('objection')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const urlService = require('../../urlService/urlService')

const acceptInvitationNotifications = require('../../notifications/inviteReviewers/acceptInvitation')
const cancelInvitationNotifications = require('../../notifications/inviteReviewers/cancelInvitation')
const declineInvitationNotifications = require('../../notifications/inviteReviewers/declineInvitation')

const JobsService = require('../../jobsService/jobsService')
const invitations = require('../../invitations/invitations')
const emailJobs = require('../../emailJobs/reviewer/emailJobs')

const { getExpectedDate } = require('component-date-service')

const getProps = require('../../emailPropsService/emailPropsService')
const getEmailCopy = require('../../notifications/inviteReviewers/getEmailCopy')
const pendingReviewersRemovalJobs = require('../../removalJobs/pendingReviewers')
const acceptedReviewersRemovalJobs = require('../../removalJobs/acceptedReviewers')

const resolver = {
  Query: {
    async getReviewersInvited(_, { manuscriptId }) {
      return useCases.getReviewersInvitedUseCase
        .initialize(models)
        .execute(manuscriptId)
    },
    async isUserSubscribedToEmails(_, { email }) {
      return useCases.isUserSubscribedToEmailsUseCase
        .initialize(models)
        .execute(email)
    },
    async getConflictingAuthors(_, { manuscriptId, rorId, aff, country }) {
      return useCases.getConflictingAuthorsUseCase
        .initialize(models)
        .execute(manuscriptId, rorId, aff, country)
    },
  },
  Mutation: {
    async inviteReviewer(_, { manuscriptId, input, emailContentChanges }, ctx) {
      const eventsService = events.initialize({ models })

      const {
        TeamMember,
        ReviewerSuggestion,
        Job,
        Manuscript,
        Team,
        Identity,
        User,
      } = models
      const jobsService = JobsService.initialize({ models })

      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const emailJobsService = emailJobs.initialize({
        Job,
        Email,
        logEvent,
        getPropsService,
      })
      const invitationsService = invitations.initialize({
        Email,
        getPropsService,
      })
      const removalJobsService = pendingReviewersRemovalJobs.initialize({
        logEvent,
        Job,
        Team,
        TeamMember,
        Manuscript,
        eventsService,
        ReviewerSuggestion,
      })

      const findOrCreateReviewerUserUseCase = useCases.findOrCreateReviewerUserUseCase.initialize(
        Identity,
        User,
      )
      const dontAllowEditorsAsReviewersOnSpecialIssuesUseCase = useCases.dontAllowEditorsAsReviewersOnSpecialIssuesUseCase.initialize(
        TeamMember,
      )
      const dontAllowReviewersToHaveMultipleRolesOnManuscriptUseCase = useCases.dontAllowReviewersToHaveMultipleRolesOnManuscriptUseCase.initialize(
        TeamMember,
      )
      const findOrCreateReviewerUseCase = useCases.findOrCreateReviewerUseCase.initialize(
        TeamMember,
      )
      const updateReviewerSuggestionInvitedStatusUseCase = useCases.updateReviewerSuggestionInvitedStatusUseCase.initialize(
        ReviewerSuggestion,
      )
      const cancelJobsWhenMinimumNumberOfReviewersIsMetUseCase = useCases.cancelJobsWhenMinimumNumberOfReviewersIsMetUseCase.initialize(
        {
          models,
          jobsService,
        },
      )
      const handleEmailsWhenReviewerIsInvitedUseCase = useCases.handleEmailsWhenReviewerIsInvitedUseCase.initialize(
        {
          models,
          invitationsService,
          emailJobsService,
          eventsService,
        },
      )

      return useCases.inviteReviewerUseCase
        .initialize({
          models,
          config,
          logEvent,
          eventsService,
          removalJobsService,
          findOrCreateReviewerUseCase,
          findOrCreateReviewerUserUseCase,
          handleEmailsWhenReviewerIsInvitedUseCase,
          updateReviewerSuggestionInvitedStatusUseCase,
          dontAllowEditorsAsReviewersOnSpecialIssuesUseCase,
          cancelJobsWhenMinimumNumberOfReviewersIsMetUseCase,
          dontAllowReviewersToHaveMultipleRolesOnManuscriptUseCase,
        })
        .execute({ manuscriptId, input, userId: ctx.user, emailContentChanges })
    },
    async acceptReviewerInvitation(_, { teamMemberId }, ctx) {
      const eventsService = events.initialize({ models })

      const { Team, TeamMember, Job, ReviewerSuggestion, Manuscript } = models
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = acceptInvitationNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
        getExpectedDate,
      })
      const jobsService = JobsService.initialize({ models })
      const emailJobsService = emailJobs.initialize({
        logEvent,
        Email,
        Job,
        getPropsService,
      })
      const removalJobsService = acceptedReviewersRemovalJobs.initialize({
        logEvent,
        Job,
        Team,
        TeamMember,
        Manuscript,
        eventsService,
        ReviewerSuggestion,
      })
      return useCases.acceptReviewerInvitationUseCase
        .initialize({
          models,
          logEvent,
          jobsService,
          eventsService,
          emailJobsService,
          removalJobsService,
          notificationService,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
    async declineReviewerInvitation(_, { teamMemberId }, ctx) {
      const eventsService = events.initialize({ models })

      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = declineInvitationNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
        getExpectedDate,
      })
      const jobsService = JobsService.initialize({ models })
      return useCases.declineReviewerInvitationUseCase
        .initialize({
          models,
          logEvent,
          jobsService,
          eventsService,
          notificationService,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
    async cancelReviewerInvitation(_, { manuscriptId, teamMemberId }, ctx) {
      const eventsService = events.initialize({ models })

      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = cancelInvitationNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
        getExpectedDate,
      })
      const jobsService = JobsService.initialize({ models })
      return useCases.cancelReviewerInvitationUseCase
        .initialize({
          models,
          logger,
          logEvent,
          transaction,
          jobsService,
          eventsService,
          notificationService,
        })
        .execute({ manuscriptId, teamMemberId, userId: ctx.user })
    },
  },
}

module.exports = resolver
