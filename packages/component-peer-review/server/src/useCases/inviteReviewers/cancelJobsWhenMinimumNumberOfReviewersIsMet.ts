interface TeamMember {
  id: string
}

const initialize = ({ models: { Job, TeamMember }, jobsService }) => ({
  async execute(
    academicEditor: TeamMember,
    staffMember: TeamMember,
    manuscriptId: string,
    ReviewerRole: string,
    minimumNumberOfInvitedReviewers: number,
  ) {
    const reviewerMembers = await TeamMember.findAllByManuscriptAndRole({
      role: ReviewerRole,
      manuscriptId,
    })
    if (reviewerMembers.length < minimumNumberOfInvitedReviewers) {
      return
    }

    const academicEditorJobs = await Job.findAllByTeamMember(academicEditor.id)
    const staffMemberJobs = await Job.findAllByTeamMember(staffMember.id)

    jobsService.cancelJobs(academicEditorJobs)
    jobsService.cancelStaffMemberJobs({
      staffMemberJobs,
      manuscriptId,
    })
  },
})

export { initialize }
