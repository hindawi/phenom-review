import { rorService } from 'component-ror'

const hasCOI = async ({ manuscript, invitee, models }) => {
  const { Manuscript, TeamMember, Team } = models

  const manuscripts = await Manuscript.findAll({
    queryObject: {
      submissionId: manuscript.submissionId,
    },
  })

  const manuscriptIds = manuscripts.map(({ id }) => id)

  const reviewers = await TeamMember.findAllByManuscriptsAndRole({
    manuscriptIds,
    role: Team.Role.reviewer,
  })

  return reviewers
    .filter(reviewer => reviewer.alias.email === invitee.email)
    .some(reviewer => reviewer.status === TeamMember.Statuses.conflicting)
}

const appendRORInfoIfMissing = async identity => {
  if (identity.aff && !identity.affRorId) {
    const bestROR = await rorService.getBestROR(identity.aff, identity.country)
    if (bestROR) {
      identity.affRorId = bestROR.organization.id
    }
  }
}

const initialize = ({
  config,
  models,
  logEvent,
  eventsService,
  removalJobsService,
  findOrCreateReviewerUseCase,
  findOrCreateReviewerUserUseCase,
  handleEmailsWhenReviewerIsInvitedUseCase,
  updateReviewerSuggestionInvitedStatusUseCase,
  dontAllowEditorsAsReviewersOnSpecialIssuesUseCase,
  cancelJobsWhenMinimumNumberOfReviewersIsMetUseCase,
  dontAllowReviewersToHaveMultipleRolesOnManuscriptUseCase,
}) => ({
  async execute({ manuscriptId, input, userId, emailContentChanges }) {
    const removalDays = config.get('reminders.reviewer.acceptInvitation.remove')
    const timeUnit = config.get('reminders.reviewer.acceptInvitation.timeUnit')
    const minimumNumberOfInvitedReviewers = config.get(
      'minimumNumberOfInvitedReviewers',
    )
    const { Team, TeamMember, Manuscript } = models

    const manuscript = await Manuscript.find(manuscriptId)

    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new Error(
        `Cannot invite reviewers on manuscript ${manuscriptId} with status ${manuscript.status}.`,
      )
    }

    const cannotInviteBecauseCOIWithAuthor = await hasCOI({
      manuscript,
      invitee: input,
      models,
    })

    if (cannotInviteBecauseCOIWithAuthor) {
      throw new Error(
        'Cannot invite reviewers that were previously marked as having a Conflict of Interest with the author.',
      )
    }

    await appendRORInfoIfMissing(input)
    const user = await findOrCreateReviewerUserUseCase.execute(input)

    await dontAllowReviewersToHaveMultipleRolesOnManuscriptUseCase.execute({
      manuscriptId,
      userId: user.id,
    })

    if (user.id === userId)
      throw new Error(`You cannot invite yourself as a reviewer.`)

    if (manuscript.specialIssueId) {
      dontAllowEditorsAsReviewersOnSpecialIssuesUseCase.execute(
        user.id,
        manuscript.specialIssueId,
        Team.Role,
      )
    }

    const ReviewerRole = Team.Role.reviewer
    const reviewersTeam = await Team.findOrCreate({
      queryObject: {
        manuscriptId: manuscript.id,
        role: ReviewerRole,
      },
      options: {
        manuscriptId: manuscript.id,
        role: ReviewerRole,
      },
    })
    await reviewersTeam.save()

    const reviewers = await TeamMember.findAllByManuscriptAndRoleAndExcludedStatuses(
      {
        role: ReviewerRole,
        manuscriptId,
        excludedStatuses: [
          TeamMember.Statuses.expired,
          TeamMember.Statuses.removed,
          TeamMember.Statuses.conflicting,
        ],
      },
    )
    if (reviewers.length === 0) {
      manuscript.updateStatus(Manuscript.Statuses.reviewersInvited)
      await manuscript.save()
    }

    const reviewer = await findOrCreateReviewerUseCase.execute(
      input,
      user.id,
      reviewersTeam.id,
      manuscriptId,
      ReviewerRole,
      reviewers.length,
    )

    updateReviewerSuggestionInvitedStatusUseCase.execute(
      input.email,
      manuscriptId,
    )

    let staffMember = await TeamMember.findOneByManuscriptAndRoleAndStatus({
      manuscriptId,
      role: Team.Role.editorialAssistant,
      status: TeamMember.Statuses.active,
    })
    if (!staffMember) {
      staffMember = await TeamMember.findOneByRole({
        role: Team.Role.admin,
      })
    }

    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
      },
    )
    cancelJobsWhenMinimumNumberOfReviewersIsMetUseCase.execute(
      academicEditor,
      staffMember,
      manuscriptId,
      ReviewerRole,
      minimumNumberOfInvitedReviewers,
    )

    reviewer.user = user

    if (user.isSubscribedToEmails) {
      await handleEmailsWhenReviewerIsInvitedUseCase.execute(
        manuscript,
        staffMember,
        academicEditor,
        reviewer,
        Team.Role,
        emailContentChanges,
      )
    }

    await removalJobsService.scheduleRemovalJob({
      timeUnit,
      days: removalDays,
      invitationId: reviewer.id,
      manuscriptId: manuscript.id,
    })

    await eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionReviewerInvited',
    })

    await logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.reviewer_invited,
      objectType: logEvent.objectType.user,
      objectId: reviewer.userId,
    })
  },
})

const authsomePolicies = [
  'isAuthenticated',
  'isAcademicEditorOnManuscript',
  'isEditorialAssistant',
  'admin',
]

export { initialize, authsomePolicies }
