import uuid from 'uuid'

interface InputType {
  givenNames: string
  surname: string
  email: string
  country: string
  aff: string
  affRorId: string
}

const initialize = (Identity, User) => ({
  async execute(input: InputType) {
    let user = await User.findOneByEmail(input.email, 'identities')
    if (user) return user

    user = new User({
      isActive: true,
      agreeTc: true,
      isSubscribedToEmails: true,
      unsubscribeToken: uuid.v4(),
      confirmationToken: uuid.v4(),
      defaultIdentityType: 'local',
      passwordResetToken: uuid.v4(),
    })
    await user.save()

    const identity = new Identity({
      type: 'local',
      isConfirmed: false,
      passwordHash: null,
      givenNames: input.givenNames,
      surname: input.surname,
      email: input.email,
      aff: input.aff || '',
      userId: user.id,
      country: input.country || '',
      affRorId: input.affRorId || null,
    })

    await identity.save()
    user.assignIdentity(identity)

    return user
  },
})

export { initialize }
