const initialize = ({ User }) => ({
  async execute(email) {
    const user = await User.findOneByEmail(email)

    if (!user) return true

    return user.isSubscribedToEmails
  },
})

const authsomePolicies = [
  'isAuthenticated',
  'isAcademicEditorOnManuscript',
  'isEditorialAssistant',
  'admin',
]

export { initialize, authsomePolicies }
