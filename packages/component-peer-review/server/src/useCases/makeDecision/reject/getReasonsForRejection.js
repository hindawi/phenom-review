const initialize = ({ config, models: { RejectDecisionInfo } }) => {
  async function execute({ journalId }) {
    const journalIdsForOptionalRejectReason = getJournalIdsForOptionalRejectReason()

    return [
      RejectDecisionInfo.ReasonsForRejection.OUT_OF_SCOPE,
      RejectDecisionInfo.ReasonsForRejection.TECHNICAL_OR_SCIENTIFIC_FLAWS,
      RejectDecisionInfo.ReasonsForRejection.PUBLICATIONS_ETHICS_CONCERNS,
      journalIdsForOptionalRejectReason.includes(journalId) &&
        RejectDecisionInfo.ReasonsForRejection.LACK_OF_NOVELTY,
      RejectDecisionInfo.ReasonsForRejection.OTHER_REASONS,
    ].filter(Boolean)
  }

  function getJournalIdsForOptionalRejectReason() {
    const journalIdsForOptionalRejectReason = config.get(
      'journalIdsForOptionalRejectReason',
    )

    return journalIdsForOptionalRejectReason
      ? journalIdsForOptionalRejectReason.trim().split(' ')
      : []
  }
  return { execute }
}

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
