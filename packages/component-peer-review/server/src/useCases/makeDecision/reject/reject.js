const initialize = ({
  config,
  models,
  logger,
  logEvent,
  useCases,
  transaction,
  jobsService,
  eventsService,
  notificationService,
  notifyAcademicEditor,
}) => {
  async function execute({
    manuscriptId,
    content,
    rejectDecisionInfo,
    userId,
  }) {
    const { Team, TeamMember, Manuscript, RejectDecisionInfo } = models
    const manuscript = await Manuscript.find(manuscriptId, [
      'articleType',
      'peerReviewEditorialMapping',
    ])

    const nonRejectableStatuses = Manuscript.NonActionableStatuses
    if (nonRejectableStatuses.includes(manuscript.status)) {
      throw new ValidationError(
        `Cannot reject a manuscript in the current status.`,
      )
    }

    let rejectEditor = await TeamMember.findOneByManuscriptAndUser({
      manuscriptId,
      userId,
    })
    if (!rejectEditor) {
      rejectEditor = await TeamMember.findOneByRole({
        role: Team.Role.admin,
      })
    }

    const validatedDecisionInfo = await getValidatedRejectReasons({
      rejectDecisionInfo,
      journalId: manuscript.journalId,
    })
    const decisionInfo = new RejectDecisionInfo(validatedDecisionInfo)

    let useCase = 'rejectWithPeerReviewUseCase'
    if (!manuscript.articleType.hasPeerReview)
      useCase = 'rejectWithNoPeerReviewUseCase'

    await useCases[useCase]
      .initialize({
        logger,
        models,
        transaction,
        jobsService,
        notificationService,
        notifyAcademicEditor,
      })
      .execute({
        manuscript,
        rejectEditor,
        content,
        rejectDecisionInfo: decisionInfo,
      })

    await eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionRejected',
    })

    await logEvent({
      userId,
      manuscriptId,
      objectId: manuscriptId,
      objectType: logEvent.objectType.manuscript,
      action: logEvent.actions.manuscript_rejected,
    })
  }

  async function getValidatedRejectReasons({ rejectDecisionInfo, journalId }) {
    if (rejectDecisionInfo.reasonsForRejection.otherReasons === '') {
      throw new ValidationError(
        'Please specify the other reason(s) for rejecting.',
      )
    }
    if (
      rejectDecisionInfo.transferToAnotherJournal.selectedOption === 'YES' &&
      !rejectDecisionInfo.transferToAnotherJournal.transferSuggestions
    ) {
      throw new ValidationError(`Please write your recommendations.`)
    }

    const reasonsForRejection = await useCases.getReasonsForRejectionUseCase
      .initialize({ config, models })
      .execute({ journalId })

    const validatedReasonsForRejection = reasonsForRejection
      .map(reason => ({
        [reason]: rejectDecisionInfo.reasonsForRejection[reason],
      }))
      .reduce((obj, reason) => ({ ...obj, ...reason }), {})

    return {
      ...validatedReasonsForRejection,
      transferToAnotherJournal:
        rejectDecisionInfo.transferToAnotherJournal.selectedOption === 'YES',
      transferSuggestions:
        rejectDecisionInfo.transferToAnotherJournal.selectedOption === 'YES'
          ? rejectDecisionInfo.transferToAnotherJournal.transferSuggestions
          : null,
    }
  }

  return { execute }
}

const authsomePolicies = ['isApprovalEditor', 'isEditorialAssistant', 'admin']

module.exports = {
  initialize,
  authsomePolicies,
}
