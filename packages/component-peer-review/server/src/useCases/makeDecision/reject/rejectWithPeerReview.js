const Promise = require('bluebird')
const { isEmpty } = require('lodash')

const initialize = ({
  logger,
  transaction,
  jobsService,
  notificationService,
  notifyAcademicEditor,
  models: {
    Job,
    Team,
    User,
    Review,
    Comment,
    Journal,
    Manuscript,
    TeamMember,
    PeerReviewModel,
  },
}) => ({
  async execute({ manuscript, rejectEditor, content, rejectDecisionInfo }) {
    manuscript.status = Manuscript.Statuses.rejected
    const review = new Review({
      teamMemberId: rejectEditor.id,
      submitted: new Date().toISOString(),
      recommendation: Review.Recommendations.reject,
    })
    const comment = new Comment({
      content,
      type: Comment.Types.public,
    })

    const pendingReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.reviewer,
        manuscriptId: manuscript.id,
        status: TeamMember.Statuses.pending,
      },
    )
    const acceptedReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.reviewer,
        manuscriptId: manuscript.id,
        status: TeamMember.Statuses.accepted,
      },
    )
    const expiredReviewers = [...pendingReviewers, ...acceptedReviewers].map(
      reviewer => {
        reviewer.status = TeamMember.Statuses.expired
        return reviewer
      },
    )

    // Transaction
    const trx = await transaction.start(Manuscript.knex())
    try {
      const trxManuscript = await manuscript
        .$query(trx)
        .updateAndFetch(manuscript)
      const trxReview = await trxManuscript
        .$relatedQuery('reviews', trx)
        .insertAndFetch(review)
      await trxReview.$relatedQuery('comments', trx).insert(comment)
      await trxReview
        .$relatedQuery('rejectDecisionInfo', trx)
        .insert(rejectDecisionInfo)
      await Promise.each(expiredReviewers, reviewer =>
        reviewer.$query(trx).update(reviewer),
      )

      await trx.commit()
    } catch (err) {
      logger.error('Something went wrong. No data was updated.', err)
      await trx.rollback(err)
      throw new Error('Something went wrong. No data was updated.')
    }

    // Data queries
    const journal = await Journal.find(manuscript.journalId, 'peerReviewModel')

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      journalId: journal.id,
      manuscriptId: manuscript.id,
      sectionId: manuscript.sectionId,
      eagerLoadRelations: 'user',
    })
    const academicEditors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.academicEditor,
    })

    const authors = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.author,
      manuscriptId: manuscript.id,
      eagerLoadRelations: 'user',
    })
    const submittingAuthor = authors.find(a => a.isSubmitting)
    const coAuthors = authors.find(a => !a.isSubmitting)

    const reviewers = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.reviewer,
      manuscriptId: manuscript.id,
      eagerLoadRelations: 'user',
    })
    const submittingReviewers = reviewers.filter(
      r => r.status === TeamMember.Statuses.submitted,
    )

    // Jobs
    const editorialAssistantJobs = await Job.findAllByTeamMemberAndManuscript({
      teamMemberId: editorialAssistant.id,
      manuscriptId: manuscript.id,
    })
    const academicEditorsJobs = await Job.findAllByTeamMembers(
      academicEditors.map(academicEditor => academicEditor.id),
    )
    const reviewersJobs = await Job.findAllByTeamMembers(
      reviewers.map(reviewer => reviewer.id),
    )
    jobsService.cancelJobs(editorialAssistantJobs)
    jobsService.cancelJobs(academicEditorsJobs)
    jobsService.cancelJobs(reviewersJobs)

    // Notifications
    if (
      triageEditor &&
      manuscript?.peerReviewEditorialMapping?.peerReviewEditorialModel
        ?.approvalEditor === Team.Role.triageEditor
    ) {
      notificationService.notifyTriageEditorForSubmittedReport({
        journal,
        manuscript,
        triageEditor,
        articleTypeName: manuscript.articleType.name,
        submittingAuthor,
        editorialAssistant,
      })
    }

    if (triageEditor && triageEditor.id !== rejectEditor.id) {
      notificationService.notifyTriageEditor({
        journal,
        manuscript,
        triageEditor,
        rejectEditor,
        submittingAuthor,
        editorialAssistant,
      })
    }

    if (!isEmpty(submittingReviewers)) {
      const academicEditorLabel = await manuscript.getEditorLabel({
        Team,
        TeamMember,
        PeerReviewModel,
        role: Team.Role.academicEditor,
      })
      notificationService.notifyReviewers({
        journal,
        manuscript,
        rejectEditor,
        submittingAuthor,
        editorialAssistant,
        academicEditorLabel,
        reviewers: submittingReviewers,
      })
      notificationService.notifyAuthorsAfterPeerReview({
        journal,
        authors: coAuthors,
        comment,
        manuscript,
        rejectEditor,
        editorialAssistant,
      })
    } else {
      notificationService.notifyAuthorsNoPeerReview({
        authors: coAuthors,
        journal,
        comment,
        manuscript,
        rejectEditor,
        editorialAssistant,
      })
    }

    notifyAcademicEditor({
      Team,
      User,
      Review,
      journal,
      manuscript,
      TeamMember,
      rejectEditor,
      submittingAuthor,
      editorialAssistant,
      notificationService,
    })
  },
})

module.exports = {
  initialize,
}
