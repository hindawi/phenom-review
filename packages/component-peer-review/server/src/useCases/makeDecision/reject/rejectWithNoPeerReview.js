const initialize = ({
  logger,
  transaction,
  jobsService,
  notificationService,
  models: { Journal, Manuscript, Team, TeamMember, Review, Comment, Job },
}) => ({
  async execute({ manuscript, rejectEditor, content, rejectDecisionInfo }) {
    manuscript.status = Manuscript.Statuses.rejected
    const review = new Review({
      teamMemberId: rejectEditor.id,
      submitted: new Date().toISOString(),
      recommendation: Review.Recommendations.reject,
    })
    const comment = new Comment({
      content,
      type: Comment.Types.public,
    })

    // Transaction
    const trx = await transaction.start(Manuscript.knex())
    try {
      const trxManuscript = await manuscript
        .$query(trx)
        .updateAndFetch(manuscript)
      const trxReview = await trxManuscript
        .$relatedQuery('reviews', trx)
        .insertAndFetch(review)
      await trxReview.$relatedQuery('comments', trx).insert(comment)
      await trxReview
        .$relatedQuery('rejectDecisionInfo', trx)
        .insert(rejectDecisionInfo)

      await trx.commit()
    } catch (err) {
      logger.error('Something went wrong. No data was updated.', err)
      await trx.rollback(err)
      throw new Error('Something went wrong. No data was updated.')
    }

    // Data queries
    const journal = await Journal.find(manuscript.journalId)

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    const academicEditors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.academicEditor,
    })
    const authors = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.author,
      manuscriptId: manuscript.id,
      eagerLoadRelations: 'user',
    })

    const coAuthors = authors.find(a => !a.isSubmitting)

    // Jobs
    const academicEditorsJobs = await Job.findAllByTeamMembers(
      academicEditors.map(academicEditor => academicEditor.id),
    )
    const editorialAssistantJobs = await Job.findAllByTeamMemberAndManuscript({
      teamMemberId: editorialAssistant.id,
      manuscriptId: manuscript.id,
    })
    jobsService.cancelJobs(editorialAssistantJobs)
    jobsService.cancelJobs(academicEditorsJobs)

    // Notifications
    if (coAuthors) {
      notificationService.notifyAuthorsNoPeerReview({
        journal,
        authors: coAuthors,
        comment,
        manuscript,
        rejectEditor,
        editorialAssistant,
      })
    }
  },
})

module.exports = {
  initialize,
}
