const initialize = ({
  logger,
  logEvent,
  transaction,
  eventsService,
  models: { Team, Review, Comment, TeamMember, Manuscript },
}) => ({
  async execute({ manuscriptId, content, userId }) {
    const manuscript = await Manuscript.find(manuscriptId)

    const nonRejectableStatuses = Manuscript.NonActionableStatuses
    if (nonRejectableStatuses.includes(manuscript.status)) {
      throw new ValidationError(
        `Cannot return a manuscript in the current status.`,
      )
    }

    let returnEditor = await TeamMember.findOneByManuscriptAndUser({
      manuscriptId,
      userId,
    })
    if (!returnEditor) {
      returnEditor = await TeamMember.findOneByRole({
        role: Team.Role.admin,
      })
    }

    manuscript.status = Manuscript.Statuses.reviewCompleted
    const review = new Review({
      manuscriptId,
      teamMemberId: returnEditor.id,
      submitted: new Date().toISOString(),
      recommendation: Review.Recommendations.returnToAcademicEditor,
    })
    const comment = new Comment({
      content,
      reviewId: review.id,
      type: Comment.Types.public,
    })

    // Transaction
    const trx = await transaction.start(Manuscript.knex())
    try {
      const trxManuscript = await manuscript
        .$query(trx)
        .updateAndFetch(manuscript)
      const trxReview = await trxManuscript
        .$relatedQuery('reviews', trx)
        .insertAndFetch(review)
      await trxReview.$relatedQuery('comments', trx).insert(comment)

      await trx.commit()
    } catch (err) {
      logger.error('Something went wrong. No data was updated.', err)
      await trx.rollback(err)
      throw new Error('Something went wrong. No data was updated.')
    }

    // Data queries
    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
        eagerLoadRelations: 'user',
      },
    )

    // Events and Logs
    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionReturnedToAcademicEditor',
    })
    logEvent({
      userId,
      manuscriptId,
      objectId: academicEditor.userId,
      objectType: logEvent.objectType.user,
      action: logEvent.actions.manuscript_returned,
    })
  },
})

const authsomePolicies = ['isApprovalEditor', 'isEditorialAssistant', 'admin']

module.exports = {
  initialize,
  authsomePolicies,
}
