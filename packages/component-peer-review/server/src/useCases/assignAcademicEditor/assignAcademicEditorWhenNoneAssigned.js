const { Promise } = require('bluebird')
const { logger } = require('component-logger')

const initialize = ({
  models,
  logEvent,
  jobsService,
  eventsService,
  notificationService,
}) => ({
  async execute({ manuscripts, latestManuscript, user }) {
    const { Manuscript, Team, TeamMember, Review } = models

    const academicEditorTeams = []
    await Promise.each(manuscripts, async manuscript => {
      const queryObject = {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
      }
      const academicEditorTeam = await Team.findOrCreate({
        queryObject,
        options: queryObject,
        eagerLoadRelations: 'members',
      })
      academicEditorTeam.addMember({
        user,
        options: {
          status: TeamMember.Statuses.accepted,
          responded: new Date(),
        },
      })
      academicEditorTeams.push(academicEditorTeam)
    })

    const manuscriptReviewerReviews = await Review.findAllValidByManuscriptAndRole(
      {
        manuscriptId: latestManuscript.id,
        role: Team.Role.reviewer,
      },
    )

    latestManuscript.status = manuscriptReviewerReviews.length
      ? Manuscript.Statuses.makeDecision
      : Manuscript.Statuses.academicEditorAssigned

    try {
      await Manuscript.updateManuscriptAndTeams({
        teams: academicEditorTeams,
        manuscript: latestManuscript,
        Team,
      })
    } catch (err) {
      logger.error('Something went wrong.', err)
      throw new Error(err)
    }
  },
})

module.exports = { initialize }
