const initialize = ({
  logEvent,
  useCases,
  models,
  jobsService,
  eventsService,
  notificationService,
}) => ({
  async execute({ submissionId, userId, reqUserId }) {
    const { Journal, Manuscript, TeamMember, Team, User } = models

    const user = await User.find(userId, 'identities')
    const manuscripts = await Manuscript.findAll({
      queryObject: { submissionId },
      order: 'DESC',
    })
    const latestManuscript = manuscripts[0]
    const hasSpecialIssueEditorialConflictOfInterest = await latestManuscript.getHasSpecialIssueEditorialConflictOfInterest(
      models,
    )
    if (hasSpecialIssueEditorialConflictOfInterest) {
      throw new ValidationError(`User cannot be assigned.`)
    }
    const academicEditors = await TeamMember.findAllBySubmissionAndRoleAndStatuses(
      {
        submissionId,
        role: Team.Role.academicEditor,
        statuses: [TeamMember.Statuses.pending, TeamMember.Statuses.accepted],
      },
    )

    let useCase = 'assignAcademicEditorWhenNoneAssignedUseCase'
    if (academicEditors.length)
      useCase = 'assignAcademicEditorWhenExistingUseCase'

    await useCases[useCase]
      .initialize({
        models,
        logEvent,
        jobsService,
        eventsService,
        notificationService,
      })
      .execute({ user, latestManuscript, manuscripts, academicEditors })

    const journal = await Journal.find(latestManuscript.journalId)
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.editorialAssistant,
        manuscriptId: latestManuscript.id,
        status: TeamMember.Statuses.active,
      },
    )
    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: latestManuscript.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
      },
    )
    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      latestManuscript.id,
    )

    notificationService.notifyEditorialAssistant({
      journal,
      manuscript: latestManuscript,
      academicEditor,
      submittingAuthor,
      editorialAssistant,
    })

    logEvent({
      userId: user.id,
      manuscriptId: latestManuscript.id,
      objectId: user.id,
      objectType: logEvent.objectType.user,
      action: logEvent.actions.academic_editor_assigned,
    })

    eventsService.publishSubmissionEvent({
      submissionId: latestManuscript.submissionId,
      eventName: 'SubmissionAcademicEditorAccepted',
    })
  },
})

const authsomePolicies = ['isTriageEditor']

module.exports = {
  initialize,
  authsomePolicies,
}
