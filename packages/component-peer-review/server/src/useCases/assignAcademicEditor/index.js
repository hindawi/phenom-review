const assignAcademicEditorUseCase = require('./assignAcademicEditor')
const assignAcademicEditorWhenNoneAssignedUseCase = require('./assignAcademicEditorWhenNoneAssigned')
const assignAcademicEditorWhenExistingUseCase = require('./assignAcademicEditorWhenExisting')

module.exports = {
  assignAcademicEditorUseCase,
  assignAcademicEditorWhenExistingUseCase,
  assignAcademicEditorWhenNoneAssignedUseCase,
}
