const initialize = ({ TeamMember, Team }) => {
  const getSpecialIssueTriageEditors = specialIssueId =>
    TeamMember.findAllBySpecialIssueAndRole({
      role: Team.Role.triageEditor,
      specialIssueId,
    })

  const getSectionTriageEditors = sectionId =>
    TeamMember.findAllBySectionAndRole({
      role: Team.Role.triageEditor,
      sectionId,
    })

  const getJournalTriageEditors = journalId =>
    TeamMember.findAllByJournalAndRole({
      role: Team.Role.triageEditor,
      journalId,
    })

  const execute = async ({ journalId, specialIssueId, sectionId } = {}) => {
    if (journalId) {
      return getJournalTriageEditors(journalId)
    } else if (specialIssueId) {
      return getSpecialIssueTriageEditors(specialIssueId)
    } else if (sectionId) {
      return getSectionTriageEditors(sectionId)
    }

    return []
  }

  return {
    execute,
  }
}

const authsomePolicies = [
  'isAuthenticated',
  'isTriageEditor',
  'isEditorialAssistant',
  'admin',
]

module.exports = {
  initialize,
  authsomePolicies,
}
