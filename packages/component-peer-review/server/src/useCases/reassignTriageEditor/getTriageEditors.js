const { pullAllBy } = require('lodash')

const initialize = models => ({
  async execute(manuscriptId) {
    const { TeamMember, Team, Manuscript } = models
    const manuscript = await Manuscript.find(manuscriptId)

    const hasSpecialIssueEditorialConflictOfInterest = await manuscript.getHasSpecialIssueEditorialConflictOfInterest(
      models,
    )
    const members = await getTriageEditors({
      manuscript,
      TeamMember,
      role: Team.Role.triageEditor,
      hasSpecialIssueEditorialConflictOfInterest,
    })

    const removedMembers = await TeamMember.findAllBySubmissionAndRoleAndStatuses(
      {
        role: Team.Role.triageEditor,
        statuses: [TeamMember.Statuses.removed],
        submissionId: manuscript.submissionId,
      },
    )

    return pullAllBy(members, removedMembers, 'userId').map(tm => tm.toDTO())
  },
})

const authsomePolicies = ['isEditorialAssistant']

module.exports = {
  initialize,
  authsomePolicies,
}

const getTriageEditors = async ({
  role,
  manuscript,
  TeamMember,
  hasSpecialIssueEditorialConflictOfInterest,
}) => {
  if (
    manuscript.specialIssueId &&
    !hasSpecialIssueEditorialConflictOfInterest
  ) {
    return TeamMember.findAllBySpecialIssueAndRole({
      role,
      specialIssueId: manuscript.specialIssueId,
    })
  }

  if (manuscript.sectionId) {
    return TeamMember.findAllBySectionAndRole({
      role,
      sectionId: manuscript.sectionId,
    })
  }

  return TeamMember.findAllByJournalAndRole({
    role,
    journalId: manuscript.journalId,
  })
}
