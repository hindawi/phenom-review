const config = require('config')
const models = require('@pubsweet/models')
const Email = require('component-sendgrid')
const { logEvent } = require('component-activity-log/server')
const { getModifiedText } = require('component-transform-text')
const events = require('component-events')

const useCases = require('./index')
const urlService = require('../../urlService/urlService')

const getProps = require('../../emailPropsService/emailPropsService')
const getEmailCopy = require('../../notifications/reassignTriageEditor/getEmailCopy')
const reassignTriageEditorNotifications = require('../../notifications/reassignTriageEditor/reassignTriageEditor')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const resolver = {
  Query: {
    async getAllTriageEditors(_, input, ctx) {
      return useCases.getAllTriageEditorsUseCase
        .initialize(models)
        .execute(input)
    },
    async getTriageEditors(_, { manuscriptId }, ctx) {
      return useCases.getTriageEditorsUseCase
        .initialize(models)
        .execute(manuscriptId)
    },
  },
  Mutation: {
    async reassignTriageEditor(_, { teamMemberId, manuscriptId }, ctx) {
      const eventsService = events.initialize({ models })
      const getEmailCopyService = getEmailCopy.initialize()
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const notificationService = reassignTriageEditorNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
      })

      return useCases.reassignTriageEditorUseCase
        .initialize({
          models,
          logEvent,
          eventsService,
          notificationService,
        })
        .execute({ teamMemberId, userId: ctx.user, manuscriptId })
    },
  },
}

module.exports = resolver
