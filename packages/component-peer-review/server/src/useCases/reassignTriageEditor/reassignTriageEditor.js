const Promise = require('bluebird')

const initialize = ({
  models: { User, Team, Journal, TeamMember, Manuscript, SpecialIssue },
  logEvent,
  eventsService,
  notificationService,
}) => ({
  async execute({ teamMemberId, manuscriptId, userId }) {
    const journalTriageEditor = await TeamMember.find(teamMemberId)
    const manuscript = await Manuscript.find(manuscriptId)

    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new AuthorizationError('Unauthorized')
    }

    const journal = await Journal.find(manuscript.journalId, 'peerReviewModel')
    const removedTriageEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.triageEditor,
        status: TeamMember.Statuses.active,
      },
    )

    const submissionTriageEditorMembers = await TeamMember.findAllBySubmissionAndRole(
      {
        role: Team.Role.triageEditor,
        submissionId: manuscript.submissionId,
      },
    )
    await Promise.each(submissionTriageEditorMembers, member => {
      member.updateProperties({ status: TeamMember.Statuses.removed })
      return member.save()
    })

    const triageEditorUser = await User.find(
      journalTriageEditor.userId,
      'identities',
    )

    const manuscripts = await Manuscript.findManuscriptsBySubmissionId({
      submissionId: manuscript.submissionId,
    })
    await Promise.each(manuscripts, async manuscript => {
      const queryObject = {
        role: Team.Role.triageEditor,
        manuscriptId: manuscript.id,
      }
      const triageEditorTeam = await Team.findOrCreate({
        queryObject,
        options: queryObject,
      })
      const newTriageEditor = triageEditorTeam.addMember({
        user: triageEditorUser,
        options: {
          status: TeamMember.Statuses.active,
        },
      })
      await newTriageEditor.save()
      await triageEditorTeam.save()

      eventsService.publishSubmissionEvent({
        submissionId: manuscript.submissionId,
        eventName: 'SubmissionTriageEditorReassigned',
      })
    })

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )

    let specialIssue
    if (manuscript.specialIssueId) {
      specialIssue = await SpecialIssue.find(
        manuscript.specialIssueId,
        'peerReviewModel',
      )
    }
    const triageEditorLabel = specialIssue
      ? specialIssue.peerReviewModel.triageEditorLabel
      : journal.peerReviewModel.triageEditorLabel

    const newTriageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      manuscriptId: manuscript.id,
      journalId: manuscript.journalId,
      sectionId: manuscript.sectionId,
    })

    notificationService.notifyAssignedTriageEditor({
      journal,
      manuscript,
      triageEditorLabel,
      editorialAssistant,
      triageEditor: newTriageEditor,
    })

    if (removedTriageEditor) {
      notificationService.notifyRemovedTriageEditor({
        journal,
        manuscript,
        triageEditorLabel,
        editorialAssistant,
        triageEditor: removedTriageEditor,
      })

      logEvent({
        userId,
        manuscriptId: manuscript.id,
        objectType: logEvent.objectType.user,
        objectId: removedTriageEditor.userId,
        action: logEvent.actions.triage_editor_removed,
      })
    }

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      objectType: logEvent.objectType.user,
      objectId: newTriageEditor.userId,
      action: logEvent.actions.triage_editor_assigned,
    })
  },
})

const authsomePolicies = ['isEditorialAssistant', 'admin']

module.exports = {
  initialize,
  authsomePolicies,
}
