const { minimumNumberOfSubmittedReports } = require('config')
const { isEmpty } = require('lodash')

const initialize = ({
  models: { Job, Team, User, Review, Journal, Comment, Manuscript, TeamMember },
  logEvent,
  fileValidator,
  jobsService,
  eventsService,
  notificationService,
}) => ({
  async execute({ reviewId, userId }) {
    const review = await Review.find(reviewId)

    const publicComment = await Comment.findOneByType({
      reviewId,
      type: Comment.Types.public,
    })
    if (!publicComment)
      throw new ValidationError('Cannot submit an empty review')

    await fileValidator.validateCommentFile({ commentId: publicComment.id })

    const manuscript = await Manuscript.find(review.manuscriptId, [
      'articleType',
      'peerReviewEditorialMapping',
    ])
    const nonActionableStatuses = Manuscript.NonActionableStatuses
    if (nonActionableStatuses.includes(manuscript.status)) {
      throw new AuthorizationError('Unauthorized')
    }

    review.updateProperties({
      submitted: new Date(),
    })
    await review.save()

    const journal = await Journal.find(manuscript.journalId, 'peerReviewModel')

    // here we determine if the final decision has been made depending on the existence of a academic editor as approval editor on the editorial model.
    // if there is a triage editor as approval editor, the new status of the manuscript after submitReview will be reviewCompleted
    // else ( if the approval editor is academic editor or there is none set => the academic editor as per specs) the new status is makeDecision
    const newStatus =
      manuscript?.peerReviewEditorialMapping?.peerReviewEditorialModel
        ?.approvalEditor === Team.Role.triageEditor
        ? Manuscript.Statuses.reviewCompleted
        : Manuscript.Statuses.makeDecision

    manuscript.updateProperties({
      status: newStatus,
    })
    await manuscript.save()

    const reviewer = await TeamMember.find(review.teamMemberId, 'user')
    reviewer.updateProperties({
      status: TeamMember.Statuses.submitted,
    })
    await reviewer.save()

    const privateComment = await Comment.findOneByType({
      reviewId,
      type: Comment.Types.private,
    })

    if (isEmpty(privateComment.content)) {
      await privateComment.delete()
    }

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )

    let staffMember = editorialAssistant
    if (!editorialAssistant) {
      staffMember = await TeamMember.findOneByRole({
        role: Team.Role.admin,
      })
    }

    const reviews = await Review.findBy({
      manuscriptId: manuscript.id,
    })
    const submittedReviews = reviews.filter(review => review.submitted)

    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
      },
    )
    academicEditor.user = await User.find(academicEditor.userId)

    if (submittedReviews.length >= minimumNumberOfSubmittedReports) {
      const academicEditorJobs = await Job.findAllByTeamMember(
        academicEditor.id,
      )
      jobsService.cancelJobs(academicEditorJobs)

      const staffMemberJobs = await Job.findAllByTeamMembers([staffMember.id])
      jobsService.cancelStaffMemberJobs({
        staffMemberJobs,
        manuscriptId: manuscript.id,
      })
    }

    const reviewerJobs = await Job.findAllByTeamMembers([reviewer.id])
    jobsService.cancelJobs(reviewerJobs)

    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )

    // Notifications

    notificationService.notifyAcademicEditor({
      reviewer,
      manuscript,
      academicEditor,
      journalName: journal.name,
      submittingAuthorName: submittingAuthor.getName(),
      editorialAssistant,
    })

    notificationService.notifyReviewerForSubmittedReport({
      reviewer,
      manuscript,
      articleType: manuscript.articleType.name,
      journalName: journal.name,
      submittingAuthorName: submittingAuthor.getName(),
      editorialAssistant,
    })

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionReviewerReportSubmitted',
    })

    logEvent({
      userId,
      objectId: manuscript.id,
      manuscriptId: manuscript.id,
      action: logEvent.actions.review_submitted,
      objectType: logEvent.objectType.manuscript,
    })
  },
})

const authsomePolicies = ['isReviewerOnReview']

module.exports = {
  initialize,
  authsomePolicies,
}
