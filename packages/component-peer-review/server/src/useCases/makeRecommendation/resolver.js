const config = require('config')
const useCases = require('./index')
const models = require('@pubsweet/models')
const { logEvent } = require('component-activity-log/server')
const Email = require('component-sendgrid')
const { getModifiedText } = require('component-transform-text')
const events = require('component-events')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const urlService = require('../../urlService/urlService')
const publishNotifications = require('../../notifications/makeRecommendation/publish')
const rejectNotifications = require('../../notifications/makeRecommendation/reject')

const JobsService = require('../../jobsService/jobsService')
const getProps = require('../../emailPropsService/emailPropsService')
const getEmailCopy = require('../../notifications/makeRecommendation/getEmailCopy')

const resolver = {
  Query: {},
  Mutation: {
    async makeRecommendationToPublish(_, { manuscriptId, input }, ctx) {
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = publishNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
      })
      const jobsService = JobsService.initialize({ models })
      const eventsService = events.initialize({ models })

      return useCases.makeRecommendationToPublishUseCase
        .initialize({
          models,
          logEvent,
          jobsService,
          eventsService,
          notificationService,
        })
        .execute({ manuscriptId, input, userId: ctx.user })
    },
    async makeRecommendationToReject(_, { manuscriptId, input }, ctx) {
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = rejectNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
      })

      const jobsService = JobsService.initialize({ models })
      const eventsService = events.initialize({ models })

      return useCases.makeRecommendationToRejectUseCase
        .initialize({
          models,
          logEvent,
          jobsService,
          eventsService,
          notificationService,
        })
        .execute({ manuscriptId, input, userId: ctx.user })
    },
  },
}

module.exports = resolver
