const { isEqual, sortBy } = require('lodash')
const { Promise } = require('bluebird')

const initialize = ({
  Manuscript,
  Team,
  Review,
  TeamMember,
  Comment,
  File,
}) => ({
  execute: async ({ manuscriptId, autosaveInput }) => {
    const manuscript = await Manuscript.find(manuscriptId)
    const existingAuthorsTeam = await Team.findOneBy({
      queryObject: {
        manuscriptId: manuscript.id,
        role: Team.Role.author,
      },
    })

    if (!existingAuthorsTeam) throw new Error('No author team has been found.')

    const existingAuthors = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.author,
      manuscriptId: manuscript.id,
    })
    const existingAuthorsEmails = sortBy(existingAuthors, 'position').map(
      author => author.alias.email,
    )

    const inputAuthorsEmails = autosaveInput.authors.map(author => author.email)

    if (!isEqual(existingAuthorsEmails, inputAuthorsEmails)) {
      await Promise.each(inputAuthorsEmails, async (email, index) => {
        const teamMember = existingAuthors.find(
          member => member.alias.email === email,
        )
        teamMember.updateProperties({ position: index })
        await teamMember.save()
      })
    }
    const manuscriptFiles = await File.findBy({ manuscriptId: manuscript.id })

    if (manuscriptFiles.length > 1) {
      const existingFilesByType = sortBy(manuscriptFiles, [
        'type',
        'position',
      ]).map(file => ({ id: file.id, type: file.type }))
      const inputFilesByType = sortBy(autosaveInput.files, [
        'fileType',
        'position',
      ]).map(file => ({ id: file.id, type: file.type }))

      if (!isEqual(existingFilesByType, inputFilesByType)) {
        const parsedFiles = inputFilesByType.reduce(
          (acc, current) => ({
            ...acc,
            [current.type]: [...acc[current.type], { id: current.id }],
          }),
          {
            manuscript: [],
            supplementary: [],
            coverLetter: [],
            figure: [],
          },
        )

        await Promise.each(Object.values(parsedFiles), async files =>
          Promise.each(files, async (file, index) => {
            const matchingFile = manuscriptFiles.find(
              mFile => mFile.id === file.id,
            )
            await matchingFile.updateProperties({ position: index })
            await matchingFile.save()
          }),
        )
      }
    }

    manuscript.updateProperties(autosaveInput.meta)
    await manuscript.save()

    const responseToRevision = await Review.findOneBy({
      queryObject: {
        recommendation: Review.Recommendations.responseToRevision,
        manuscriptId: manuscript.id,
      },
    })

    const comment = await Comment.findOneBy({
      queryObject: {
        reviewId: responseToRevision.id,
      },
    })
    comment.updateProperties({ content: autosaveInput.content })
    await comment.save()

    manuscript.comment = comment.toDTO()
    return manuscript.toDTO()
  },
})

const authsomePolicies = ['hasAccessToManuscript', 'admin']

module.exports = {
  initialize,
  authsomePolicies,
}
