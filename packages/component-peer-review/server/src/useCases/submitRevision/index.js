const submitRevisionUseCase = require('./submit')
const updateDraftRevisionUseCase = require('./updateDraft')

const getManuscriptStatusUseCase = require('./getManuscriptStatus')
const getReviewersChangedToExpiredUseCase = require('./getReviewersChangedToExpired')
const copyReviewersToNewVersionUseCase = require('./copyReviewersToNewVersion')
const notifyApprovalEditorUseCase = require('./notifyApprovalEditor')
const sendNewReviewersInvitationAndScheduleRemindersUseCase = require('./sendNewReviewersInvitationAndScheduleReminders')

module.exports = {
  submitRevisionUseCase,
  updateDraftRevisionUseCase,
  getManuscriptStatusUseCase,
  notifyApprovalEditorUseCase,
  copyReviewersToNewVersionUseCase,
  getReviewersChangedToExpiredUseCase,
  sendNewReviewersInvitationAndScheduleRemindersUseCase,
}
