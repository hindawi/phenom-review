const initialize = ({ TeamMember, Team }) => ({
  async execute(draftManuscript, reviewers) {
    if (reviewers.length === 0) return []

    const queryObject = {
      role: Team.Role.reviewer,
      manuscriptId: draftManuscript.id,
    }
    const newReviewerTeam = await Team.findOrCreate({
      queryObject,
      options: queryObject,
    })
    if (!newReviewerTeam) return []

    const newReviewers = []
    reviewers.forEach(reviewer => {
      const newMember = new TeamMember({
        ...reviewer,
        teamId: newReviewerTeam.id,
        created: new Date().toISOString(),
        updated: new Date().toISOString(),
        status: TeamMember.Statuses.pending,
      })
      delete newMember.id
      newReviewers.push(newMember)
    })

    return newReviewers
  },
})

export { initialize }
