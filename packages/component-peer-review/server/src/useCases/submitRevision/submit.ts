const { transaction } = require('objection')

const initialize = ({
  models,
  logger,
  logEvent,
  fileValidator,
  eventsService,
  getManuscriptStatus,
  notifyApprovalEditor,
  copyReviewersToNewVersion,
  getReviewersChangedToExpired,
  sendNewReviewersInvitationAndScheduleReminders,
}) => ({
  async execute({ submissionId, userId }) {
    const {
      Team,
      Review,
      Comment,
      Journal,
      TeamMember,
      Manuscript,
      PeerReviewEditorialMapping,
    } = models

    const draftManuscript = await Manuscript.findOneBy({
      queryObject: {
        submissionId,
        status: Manuscript.Statuses.draft,
      },
    })
    await fileValidator.validateSubmissionFiles({
      manuscriptId: draftManuscript.id,
    })

    const responseToRevision = await Review.findOneBy({
      queryObject: {
        recommendation: Review.Recommendations.responseToRevision,
        manuscriptId: draftManuscript.id,
      },
    })

    const responseToRevisionComment = await Comment.findOneByType({
      reviewId: responseToRevision.id,
      type: Comment.Types.public,
    })

    await fileValidator.validateCommentFile({
      commentId: responseToRevisionComment.id,
    })

    const revisionManuscript = await Manuscript.findOneBy({
      queryObject: {
        submissionId,
        status: Manuscript.Statuses.revisionRequested,
      },
    })

    const journal = await Journal.find(revisionManuscript.journalId)

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: revisionManuscript.id,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )

    draftManuscript.updateIsLatestVersionFlag(true) // set the flag to true. it will be saved under each corresponding sub-use cases
    revisionManuscript.updateStatus(Manuscript.Statuses.olderVersion)
    revisionManuscript.updateIsLatestVersionFlag(false)

    const reviewRecommendation = await Review.findLatestEditorialReview({
      manuscriptId: revisionManuscript.id,
      TeamRole: Team.Role,
    })
    if (!reviewRecommendation) {
      throw new Error('No recommendation found')
    }

    if (responseToRevision) {
      responseToRevision.setSubmitted(new Date().toISOString())
    }

    const approvalEditorRole = await PeerReviewEditorialMapping.getApprovalEditorRoleForSubmission(
      revisionManuscript.submissionId,
    )

    draftManuscript.status = await getManuscriptStatus.execute(
      revisionManuscript,
      reviewRecommendation.recommendation,
      approvalEditorRole,
    )

    let pendingReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.reviewer,
        manuscriptId: revisionManuscript.id,
        status: TeamMember.Statuses.pending,
      },
    )
    if (pendingReviewers) {
      pendingReviewers = await getReviewersChangedToExpired.execute(
        pendingReviewers,
      )
    }

    let newSubmittedReviewers = []
    if (reviewRecommendation.recommendation === Review.Recommendations.major) {
      const submittedReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
        {
          role: Team.Role.reviewer,
          manuscriptId: revisionManuscript.id,
          status: TeamMember.Statuses.submitted,
        },
      )

      if (submittedReviewers) {
        newSubmittedReviewers = await copyReviewersToNewVersion.execute(
          draftManuscript,
          submittedReviewers,
        )
      }
    }

    try {
      await transaction(Manuscript.knex(), async trx => {
        await Manuscript.query(trx).upsertGraph(draftManuscript)
        await Manuscript.query(trx).upsertGraph(revisionManuscript)
        await Review.query(trx).upsertGraph(responseToRevision)
        !!pendingReviewers.length &&
          (await TeamMember.query(trx).upsertGraph(pendingReviewers, {
            insertMissing: true,
            relate: true,
          }))
        !!newSubmittedReviewers.length &&
          (await TeamMember.query(trx).upsertGraph(newSubmittedReviewers, {
            insertMissing: true,
            relate: true,
          }))
      })
    } catch (err) {
      logger.error('Something went wrong. No data was updated.', err)
      throw new Error('Something went wrong. No data was updated.')
    }

    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: revisionManuscript.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
        eagerLoadRelations: 'user',
      },
    )
    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      draftManuscript.id,
    )
    await notifyApprovalEditor.execute(
      approvalEditorRole,
      draftManuscript,
      journal,
      academicEditor,
      editorialAssistant,
      submittingAuthor,
    )

    await sendNewReviewersInvitationAndScheduleReminders.execute(
      draftManuscript,
      journal,
      academicEditor,
      editorialAssistant,
      submittingAuthor,
    )

    await eventsService.publishSubmissionEvent({
      submissionId: draftManuscript.submissionId,
      eventName: 'SubmissionRevisionSubmitted',
    })

    await logEvent({
      objectId: revisionManuscript.id,
      userId,
      manuscriptId: revisionManuscript.id,
      objectType: logEvent.objectType.manuscript,
      action: logEvent.actions.revision_submitted,
    })
  },
})

const authsomePolicies = ['hasAccessToSubmission', 'admin']

module.exports = {
  initialize,
  authsomePolicies,
}
