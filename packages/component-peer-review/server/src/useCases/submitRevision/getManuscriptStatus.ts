enum ReviewRecommendations {
  revision = 'revision',
  minor = 'minor',
  major = 'major',
}

const initialize = ({ models: { Review, TeamMember, Team, Manuscript } }) => ({
  async execute(
    revisionManuscript,
    reviewRecommendation: ReviewRecommendations,
  ) {
    const reviewers = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.reviewer,
      manuscriptId: revisionManuscript.id,
    })
    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatuses(
      {
        role: Team.Role.academicEditor,
        statuses: [TeamMember.Statuses.pending, TeamMember.Statuses.accepted],
        manuscriptId: revisionManuscript.id,
      },
    )

    if (!academicEditor) {
      return Manuscript.Statuses.submitted
    }

    if (reviewers.length === 0)
      return Manuscript.Statuses.academicEditorAssigned

    if (reviewRecommendation === Review.Recommendations.minor)
      return Manuscript.Statuses.reviewCompleted

    return Manuscript.Statuses.underReview
  },
})

export { initialize }
