const Promise = require('bluebird')

const initialize = ({ models, useCases, logEvent, services }) => ({
  async execute({
    manuscriptId,
    content,
    type,
    emailContentChanges,
    useCommunicationV2 = true,
    userId,
  }) {
    const { jobsService, eventsService, notificationService } = services
    const { Team, User, Review, Comment, Manuscript, TeamMember } = models
    const manuscript = await Manuscript.find(
      manuscriptId,
      'journal.peerReviewModel',
    )

    // if manuscript is deleted or withdrawn, throw error
    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new AuthorizationError('Unauthorized')
    }

    // find submitting author
    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )
    submittingAuthor.user = await User.find(submittingAuthor.userId)

    // create the next major version
    await useCases.createMajorVersion
      .initialize({
        models,
        useCases,
        services,
      })
      .execute({ manuscript })

    // update status of current version to revision requested
    manuscript.updateStatus(Manuscript.Statuses.revisionRequested)

    // save the current version (manuscript)
    await manuscript.save()

    // get the EA for the current manuscript
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    // find AE for the current manuscript
    let editorialMember = await TeamMember.findOneByManuscriptAndRoleAndUser({
      userId,
      manuscriptId,
      role: Team.Role.academicEditor,
    })

    // if there is no AE, find TE
    if (!editorialMember) {
      editorialMember = await TeamMember.findOneByManuscriptAndRoleAndUser({
        userId,
        manuscriptId,
        role: Team.Role.triageEditor,
      })
    }

    // if there is neither AE or TE, EA becomes editorial member
    if (!editorialMember) editorialMember = editorialAssistant

    // create and save the associated review
    const review = new Review({
      manuscriptId,
      teamMemberId: editorialMember.id,
      submitted: new Date().toISOString(),
      recommendation: Review.Recommendations[type],
    })
    await review.save()

    // create and save the associated comment
    const comment = new Comment({
      content,
      reviewId: review.id,
      type: Comment.Types.public,
    })
    await comment.save()

    // get parent journal of the current manuscript
    const { journal } = manuscript

    // after the Editorial Recommendation preview functionality (CommsV2) was integrated, the Editorial Decision (minor/major request revision - CommsV1) was affected.
    // tow identical emails are sent. One from backend (Comms V2) and one from frontend (Comms V1).
    // to avoid  sending email twice, I added this check. Please remove it when the Editorial Decision will be implemented via CommsV2

    if (useCommunicationV2) {
      notificationService.notifySubmittingAuthor({
        journal,
        manuscript,
        editor: editorialMember,
        submittingAuthor,
        editorialAssistant,
        emailContentChanges,
        additionalComments: content,
        eventsService,
      })
    }

    const eventAction =
      type === Review.Recommendations.major
        ? 'revision_requested_major'
        : 'revision_requested_minor'

    logEvent({
      userId,
      manuscriptId,
      objectId: manuscriptId,
      action: logEvent.actions[eventAction],
      objectType: logEvent.objectType.manuscript,
    })

    await useCases.requestMinorOrMajorUseCase
      .initialize({
        models,
        useCases,
        jobsService,
        eventsService,
        notificationService,
      })
      .execute({
        journal,
        manuscript,
        submittingAuthor,
        editorialAssistant,
        academicEditor: editorialMember,
      })

    const pendingReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.reviewer,
        manuscriptId: manuscript.id,
        status: TeamMember.Statuses.pending,
      },
    )
    const acceptedReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.reviewer,
        manuscriptId: manuscript.id,
        status: TeamMember.Statuses.accepted,
      },
    )

    await Promise.each(
      [...pendingReviewers, ...acceptedReviewers],
      async tm => {
        tm.updateProperties({ status: TeamMember.Statuses.expired })
        await tm.save()
      },
    )

    await useCases.cancelAllJobsUseCase
      .initialize({ models, jobsService })
      .execute({
        manuscript,
        editorialAssistant,
        academicEditor: editorialMember,
      })
  },
})

const authsomePolicies = [
  'isTriageEditor',
  'isAuthenticated',
  'isAcademicEditorOnManuscript',
]

module.exports = {
  initialize,
  authsomePolicies,
}
