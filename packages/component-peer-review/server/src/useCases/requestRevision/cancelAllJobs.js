const initialize = ({ models: { Job, Team, TeamMember }, jobsService }) => ({
  async execute({ manuscript, academicEditor, editorialAssistant }) {
    let staffMember = editorialAssistant
    if (!editorialAssistant) {
      staffMember = await TeamMember.findOneByRole({
        role: Team.Role.admin,
      })
    }
    const staffMemberJobs = await Job.findAllByTeamMembers([staffMember.id])
    jobsService.cancelStaffMemberJobs({
      staffMemberJobs,
      manuscriptId: manuscript.id,
    })

    const reviewers = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.reviewer,
      manuscriptId: manuscript.id,
    })

    const editorialAndManuscriptJobs = await Job.findAllByTeamMembers([
      academicEditor.id,
      ...reviewers.map(r => r.id),
    ])

    jobsService.cancelJobs(editorialAndManuscriptJobs)
  },
})

module.exports = { initialize }
