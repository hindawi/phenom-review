import {
  TemplateInputDto,
  TemplateName,
} from '@hindawi/phenom-communications-types'
import { CommunicationInput, TemplateUsage } from './getCommunication'
import {
  buildLinks,
  getEditorialAssistant,
  getManuscriptData,
  getSignedToken,
} from './shared'

export const editorialDecisionToReviseUseCase = async (
  data: CommunicationInput,
  models,
): Promise<TemplateInputDto> => {
  const { TeamMember } = models
  const { sender, usage } = data

  const { manuscript, journal, authors } = await getManuscriptData(models, data)

  const submittingAuthor = await TeamMember.findSubmittingAuthor(manuscript.id)
  const submittingAuthorName = `${submittingAuthor.alias.givenNames} ${submittingAuthor.alias.surname}`

  const links = buildLinks(manuscript, submittingAuthor)
  const editorialAssistant = await getEditorialAssistant(models, manuscript)

  const signedTokens = getSignedToken({
    links,
    to: {
      name: `Dr. ${submittingAuthor.alias.surname}`,
      email: submittingAuthor.alias.email,
    },
    sender,
    manuscript,
    authors,
    editorialAssistant,
  })

  return {
    templateName:
      usage === TemplateUsage.EDITORIAL_DECISION_TO_REVISE
        ? TemplateName.revisionRequested
        : TemplateName.recommendationToRevise,
    mailInput: {
      manuscript: {
        manuscriptId: manuscript.customId,
        manuscriptTitle: manuscript.title,
        journalName: journal.name,
        submittingAuthor: submittingAuthorName,
      },
    },
    signedInput: signedTokens,
  }
}
