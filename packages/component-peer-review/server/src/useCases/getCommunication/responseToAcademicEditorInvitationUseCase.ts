import {
  TemplateInputDto,
  TemplateName,
} from '@hindawi/phenom-communications-types'
import { CommunicationInput, TemplateUsage } from './getCommunication'

import { buildLinks, getEditorialAssistant, getSignedToken } from './shared'

export const responseToAcademicEditorInvitationUseCase = async (
  data: CommunicationInput,
  models,
): Promise<TemplateInputDto> => {
  const { Manuscript, Team, TeamMember, Journal } = models
  const { sender, teamMemberId, usage, comments } = data
  const manuscript = await Manuscript.findManuscriptByTeamMember(teamMemberId)
  const journal = await Journal.find(manuscript.journalId, 'peerReviewModel')
  let toUser

  const editorialAssistant = await getEditorialAssistant(models, manuscript)

  if (journal.peerReviewModel.hasTriageEditor) {
    toUser = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      journalId: journal.id,
      manuscriptId: manuscript.id,
      sectionId: manuscript.sectionId,
    })
  } else {
    toUser = editorialAssistant
  }

  // fallback to editorial assistant
  if (!toUser) toUser = editorialAssistant

  const submittingAuthor = await TeamMember.findSubmittingAuthor(manuscript.id)

  const authors = await TeamMember.findAllByManuscriptAndRole({
    manuscriptId: manuscript.id,
    role: Team.Role.author,
  })

  const links = buildLinks(manuscript, toUser)

  const signedTokens = getSignedToken({
    links,
    to: {
      email: toUser.alias.email,
      name: `Dr. ${toUser.alias.surname}`,
    },
    sender,
    manuscript,
    authors,
    editorialAssistant,
  })

  return {
    templateName:
      usage === TemplateUsage.ACCEPTED_ACADEMIC_INVITATION
        ? TemplateName.editorInvitationAccepted
        : TemplateName.editorDeclined,
    mailInput: {
      manuscript: {
        manuscriptId: manuscript.customId,
        manuscriptTitle: manuscript.title,
        journalName: journal.name,
        submittingAuthor: `${submittingAuthor.alias.givenNames} ${submittingAuthor.alias.surname}`,
        comments,
      },
    },
    signedInput: signedTokens,
  }
}
