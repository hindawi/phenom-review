import { MailInputDto } from '@hindawi/phenom-communications-types'
import jwt from 'jsonwebtoken'
import { CommunicationInput } from './getCommunication'

const urlService = require('../../urlService/urlService')

const config = require('config')
const { last, get } = require('lodash')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const emailSender = config.get('communications.emailSender')
const jwtSecret = config.get('communications.jwtSecret')
const bccAddress = config.get('bccAddress')

export const buildLinks = (manuscript, recipient) => ({
  unsubscribeLink: urlService.createUrl({
    baseUrl,
    slug: unsubscribeSlug,
    queryParams: {
      id: recipient.id,
      token: recipient.unsubscribeToken,
    },
  }),
  manuscriptDetailsLink: urlService.createUrl({
    baseUrl,
    slug: `/details/${manuscript.submissionId}/${manuscript.id}`,
  }),
  reviewLink: baseUrl,
})

export const getEditorialAssistant = async (models, manuscript) => {
  const { Team, TeamMember } = models
  return TeamMember.findOneByManuscriptAndRole({
    manuscriptId: manuscript.id,
    role: Team.Role.editorialAssistant,
    status: TeamMember.Statuses.active,
    eagerLoadRelations: 'user',
  })
}

export const getManuscriptData = async (models, data: CommunicationInput) => {
  const { Manuscript, Team, TeamMember, Journal } = models
  const { submissionId } = data

  const manuscripts = await Manuscript.findManuscriptsBySubmissionId({
    submissionId,
    order: 'ASC',
    orderByField: 'version',
  })
  const manuscript = last(manuscripts)

  const journal = await Journal.find(manuscript.journalId)

  const authors = await TeamMember.findAllByManuscriptAndRole({
    manuscriptId: manuscript.id,
    role: Team.Role.author,
  })

  return { manuscript, journal, authors }
}

export const getSignedToken = ({
  sender,
  links,
  to,
  manuscript,
  authors,
  bcc = [{ name: 'bcc', email: bccAddress }],
  editorialAssistant,
}) => {
  const { defaultIdentity } = sender
  const tokens: MailInputDto = {
    links,
    from: {
      name: `${defaultIdentity.givenNames} ${defaultIdentity.surname}`,
      email: emailSender,
    },
    to,
    replyTo: {
      name: `${editorialAssistant.alias.givenNames} ${editorialAssistant.alias.surname}`,
      email: editorialAssistant.alias.email,
    },
    bcc,
    manuscript: {
      manuscriptId: manuscript.customId,
      abstract: manuscript.abstract,
      authors: authors.map(a => ({
        name: a.getName(),
        affiliation: get(a, 'alias.aff'),
      })),
    },
    publisherName: config.get('publisherName'),
  }
  return jwt.sign(tokens, jwtSecret)
}
