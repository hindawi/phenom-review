import {
  TemplateInputDto,
  TemplateName,
} from '@hindawi/phenom-communications-types'
import { CommunicationInput } from './getCommunication'
import {
  buildLinks,
  getEditorialAssistant,
  getManuscriptData,
  getSignedToken,
} from './shared'

export const editorialDecisionToReturnUseCase = async (
  data: CommunicationInput,
  models,
): Promise<TemplateInputDto> => {
  const { TeamMember, Team } = models
  const { sender } = data

  const { manuscript, journal, authors } = await getManuscriptData(models, data)

  const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus({
    manuscriptId: manuscript.id,
    role: Team.Role.academicEditor,
    status: TeamMember.Statuses.accepted,
    eagerLoadRelations: 'user',
  })

  const links = buildLinks(manuscript, academicEditor)
  const editorialAssistant = await getEditorialAssistant(models, manuscript)

  const signedTokens = getSignedToken({
    links,
    to: {
      name: `Dr. ${academicEditor.alias.surname}`,
      email: academicEditor.alias.email,
    },
    sender,
    manuscript,
    authors,
    editorialAssistant,
  })

  const submittingAuthor = await TeamMember.findSubmittingAuthor(manuscript.id)

  return {
    templateName: TemplateName.editorialDecisionReturnedWithComments,
    mailInput: {
      manuscript: {
        manuscriptId: manuscript.customId,
        manuscriptTitle: manuscript.title,
        journalName: journal.name,
        submittingAuthor: `${submittingAuthor.alias.givenNames} ${submittingAuthor.alias.surname}`,
      },
    },
    signedInput: signedTokens,
  }
}
