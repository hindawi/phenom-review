import { TemplateInputDto } from '@hindawi/phenom-communications-types'

import { CommunicationInput, getCommunicationPackage } from './getCommunication'

const models = require('@pubsweet/models')

const resolver = {
  Query: {
    async getCommunicationPackage(
      _,
      { input }: Record<'input', CommunicationInput>,
      ctx,
    ): Promise<TemplateInputDto> {
      const { User } = models
      const sender = await User.find(ctx.user, 'identities')
      return getCommunicationPackage({ ...input, sender })
    },
  },
}

module.exports = resolver
