const getEmailCopy = ({
  emailType,
  authorName,
  reviewLink,
  journalName,
  targetUserName,
  shortReviewLink,
  manuscriptTitle,
  academicEditorLabel,
}) => {
  let upperContent, subText, lowerContent
  const hasLink = true
  const hasIntro = true
  const hasSignature = true

  if (!emailType) {
    throw new Error(`The ${emailType} email type is not defined.`)
  }
  if (emailType === 'academic-editor-assigned') {
    upperContent = `
        <strong>${targetUserName}</strong> has invited you to
      serve as the ${academicEditorLabel} for the manuscript titled <strong>"${manuscriptTitle}"</strong>
      by <strong>${authorName}</strong> submitted to <strong>${journalName}</strong>.
        <br/><br/>
        Please use the links below to agree or decline this invitation, or log into your account at 
      <a style="color:#007e92; text-decoration: none;" href="${reviewLink}">${shortReviewLink}</a> 
      to take action. If you are able to handle this submission, we would be grateful if you could please 
      check the quality of the manuscript to see if it is suitable to proceed with the peer review process, 
      and if so please invite reviewers for this manuscript.`
    lowerContent = `Thank you in advance for your help with the assessment of this manuscript.<br/>
      We look forward to hearing from you.`
  }

  return {
    hasLink,
    subText,
    hasIntro,
    upperContent,
    lowerContent,
    hasSignature,
  }
}

module.exports = {
  getEmailCopy,
}
