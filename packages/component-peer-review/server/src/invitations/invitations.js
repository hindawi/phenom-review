const { getExpectedDate } = require('component-date-service')
const { getEmailCopy: getReviewerEmailCopy } = require('./reviewer/emailCopy')
const {
  getInvitationEmailProps: getReviewerInvitationProps,
} = require('./reviewer/getInvitationEmailProps')

const initialize = ({ Email, getPropsService }) => ({
  async sendInvitationToAcademicEditor({
    journal,
    manuscript,
    triageEditor,
    academicEditor,
    submittingAuthor,
    authorTeamMembers,
    editorialAssistant,
    emailContentChanges,
    eventsService,
  }) {
    const eventData = getPropsService.getEmailData({
      manuscript,
      journal,
      toUser: academicEditor,
      fromUser: triageEditor || editorialAssistant,
      submittingAuthor,
      editorialAssistant,
      authors: authorTeamMembers,
      emailContentChanges,
    })

    eventsService.publishSendEmailEvent({
      usecase: 'InvitationToHandleAManuscript(AE)',
      eventData,
    })
  },
  async sendInvitationToReviewerAfterMajorRevision({
    journal,
    authors,
    manuscript,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
    reviewers,
  }) {
    const submittingAuthorName = submittingAuthor.getName()
    const { name: journalName } = journal
    const { title, customId } = manuscript

    reviewers.forEach(reviewerMember => {
      const emailProps = getReviewerInvitationProps({
        manuscript,
        authors,
        journalName,
        academicEditor,
        editorialAssistant,
        reviewer: reviewerMember,
      })

      const { paragraph, ...bodyProps } = getReviewerEmailCopy({
        emailType: 'reviewer-invitation-after-revision',
        titleText: `the manuscript titled <strong>"${title}"</strong> by <strong>${submittingAuthorName}</strong> et al.`,
        expectedDate: getExpectedDate({
          timestamp: new Date(),
          daysExpected: 14,
        }),
        journalName,
      })

      emailProps.bodyProps = bodyProps
      emailProps.content.subject = `${customId}: Review invitation: New Version`
      emailProps.content.signatureName = editorialAssistant.getName()
      emailProps.fromEmail = `${editorialAssistant.getName()} <${editorialAssistant.getEmail()}>`

      const email = new Email(emailProps)

      return email.sendEmail()
    })
  },
  async sendInvitationToReviewer({
    journal,
    reviewer,
    academicEditor,
    authors,
    manuscript,
    editorialAssistant,
    submittingAuthor,
    emailContentChanges,
    eventsService,
  }) {
    const eventData = getPropsService.getEmailData({
      manuscript,
      journal,
      toUser: reviewer,
      fromUser: academicEditor,
      submittingAuthor,
      editorialAssistant,
      authors,
      emailContentChanges,
    })

    eventsService.publishSendEmailEvent({
      usecase: 'InvitationToReviewAManuscript(Reviewer)',
      eventData,
    })
  },
})
module.exports = { initialize }
