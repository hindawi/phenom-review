const models = require('@pubsweet/models')
const {
  useCases: { getSuggestedAcademicEditorUseCase },
} = require('component-model')

const events = require('component-events')
const {
  inviteAcademicEditorUseCase,
} = require('./useCases/inviteAcademicEditor')

const {
  initializeInviteAcademicEditorUseCase,
} = require('./useCases/inviteAcademicEditor/initializeInviteAcademicEditorUseCase')

const {
  ingestEditorSuggestionsUseCase,
  inviteSuggestedAcademicEditorUseCase,
  manuscriptTaEligibilityUpdatedHandler,
} = require('./useCases')

module.exports = {
  async EditorSuggestionListGenerated(data) {
    const eventsService = events.initialize({ models })

    await ingestEditorSuggestionsUseCase
      .initialize({
        models,
        eventsService,
      })
      .execute(data)
  },

  async SubmissionEditorSuggestionsIngested(data) {
    await inviteSuggestedAcademicEditorHandler(data)
  },

  async SubmissionAcademicEditorDeclined(data) {
    await inviteSuggestedAcademicEditorHandler(data)
  },

  async SubmissionAcademicEditorInvitationExpired(data) {
    await inviteSuggestedAcademicEditorHandler(data)
  },

  // TO-DO name of event to be changed
  async ManuscriptTAEligibilityUpdated(data) {
    await manuscriptTaEligibilityUpdatedHandler
      .initialize({ models })
      .execute(data)
  },
}

async function inviteSuggestedAcademicEditorHandler({ submissionId }) {
  const getSuggestedAcademicEditor = getSuggestedAcademicEditorUseCase.initialize(
    {
      models,
    },
  )

  return inviteSuggestedAcademicEditorUseCase
    .initialize({
      models,
      useCases: {
        getSuggestedAcademicEditor,
        inviteAcademicEditor: initializeInviteAcademicEditorUseCase(
          inviteAcademicEditorUseCase,
        ),
      },
    })
    .execute(submissionId)
}
