const moment = require('moment-business-days')

const initialize = ({ Job }) => ({
  async scheduleRemovalJob({ days, timeUnit, invitationId, manuscriptId }) {
    const executionDate = moment()
      .add(days, timeUnit)
      .toISOString()

    const params = {
      days,
      timeUnit,
      invitationId,
      manuscriptId,
    }

    await Job.schedule({
      params,
      executionDate,
      teamMemberId: invitationId,
      queueName: 'removal-pending-reviewer',
    })
  },
})

module.exports = { initialize }
