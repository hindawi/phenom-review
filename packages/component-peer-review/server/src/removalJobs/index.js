module.exports = {
  academicEditor: {
    academicEditorRemovalJobs: require('./academicEditorRemovalJobs'),
  },
  reviewer: {
    acceptedReviewers: require('./acceptedReviewers'),
    pendingReviewers: require('./pendingReviewers'),
  },
}
