const config = require('config')

const staffEmail = config.get('staffEmail')
const daysList = config.get('reminders.reviewer.acceptInvitation.days')
const acceptedReviewerDaysList = config.get(
  'reminders.reviewer.submitReport.days',
)
const removalDayAcceptedReviewer = config.get(
  'reminders.reviewer.submitReport.remove',
)
const { getExpectedDate } = require('component-date-service')

const {
  getInvitationEmailProps,
} = require('../../invitations/reviewer/getInvitationEmailProps')
const reminders = require('./reminders')

const { get } = require('lodash')

const initialize = ({ logEvent, Email, Job, getPropsService }) => ({
  async scheduleEmailsWhenReviewerIsInvited({
    journal,
    authors,
    reviewer,
    manuscript,
    academicEditor,
    editorialAssistant,
    submittingAuthorName,
  }) {
    const { title, customId, id: manuscriptId } = manuscript
    const { created, id: invitationId, userId } = reviewer
    const { name: journalName } = journal
    const expectedDate = getExpectedDate({
      timestamp: new Date(created).getTime(),
      daysExpected: 0,
    })
    const titleText = `the manuscript titled "${title}" by ${submittingAuthorName}`
    const emailProps = getInvitationEmailProps({
      authors,
      reviewer,
      manuscript,
      journalName,
      academicEditor,
      editorialAssistant,
    })
    emailProps.content.subject = `${customId}: Review reminder`
    emailProps.content.signatureName = editorialAssistant.getName()
    emailProps.fromEmail = `${editorialAssistant.getName()} <${editorialAssistant.getEmail()}>`
    const {
      scheduleFirstReviewerReminder,
      scheduleSecondReviewerReminder,
      scheduleThirdReviewerReminder,
    } = reminders.initialize({ logEvent, Email, Job })

    await scheduleFirstReviewerReminder({
      userId,
      journalName,
      titleText,
      emailProps,
      manuscriptId,
      expectedDate,
      firstDate: daysList.first,
      teamMemberId: invitationId,
    })
    await scheduleSecondReviewerReminder({
      userId,
      titleText,
      emailProps,
      manuscriptId,
      expectedDate,
      secondDate: daysList.second,
      teamMemberId: invitationId,
    })
    await scheduleThirdReviewerReminder({
      userId,
      titleText,
      emailProps,
      manuscriptId,
      expectedDate,
      thirdDate: daysList.third,
      teamMemberId: invitationId,
    })
  },
  async scheduleEmailsWhenReviewerIsInvitedAfterMajorRevision({
    journal,
    reviewer,
    authors,
    manuscript,
    academicEditor,
    editorialAssistant,
    submittingAuthorName,
  }) {
    const { name: journalName } = journal
    const { title, customId, id: manuscriptId } = manuscript
    const { created, id: invitationId, userId } = reviewer

    const expectedDate = getExpectedDate({
      timestamp: new Date(created).getTime(),
      daysExpected: 0,
    })
    const titleText = `the manuscript titled "${title}" by ${submittingAuthorName}`
    const emailProps = getInvitationEmailProps({
      manuscript,
      reviewer,
      authors,
      journalName,
      academicEditor,
      editorialAssistant,
    })
    emailProps.content.subject = `${customId}: Review reminder`
    emailProps.content.signatureName = editorialAssistant.getName()
    emailProps.fromEmail = `${editorialAssistant.getName()} <${editorialAssistant.getEmail()}>`

    const {
      scheduledFirstReviewerReminderAfterMajorRevision,
      scheduledSecondReviewerReminderAfterMajorRevision,
      scheduledThirdReviewerReminderAfterMajorRevision,
    } = reminders.initialize({ logEvent, Email, Job })

    await scheduledFirstReviewerReminderAfterMajorRevision({
      userId,
      titleText,
      emailProps,
      manuscriptId,
      expectedDate,
      firstDate: daysList.first,
      teamMemberId: invitationId,
    })
    await scheduledSecondReviewerReminderAfterMajorRevision({
      userId,
      titleText,
      emailProps,
      manuscriptId,
      expectedDate,
      teamMemberId: invitationId,
      secondDate: daysList.second,
    })
    await scheduledThirdReviewerReminderAfterMajorRevision({
      userId,
      titleText,
      emailProps,
      manuscriptId,
      expectedDate,
      thirdDate: daysList.third,
      teamMemberId: invitationId,
    })
  },
  async scheduleEmailsWhenReviewerAcceptsInvitation({
    journal,
    reviewer,
    manuscript,
    submittingAuthorName,
    editorialAssistant,
  }) {
    const { name: journalName } = journal
    const { title, customId, id: manuscriptId } = manuscript
    const { id: invitationId, userId } = reviewer
    const titleText = `the manuscript titled "${title}" by ${submittingAuthorName}`
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail
    const editorialAssistantName = editorialAssistant
      ? editorialAssistant.getName()
      : ''
    const removalDay = getExpectedDate({
      timestamp: reviewer.responded,
      daysExpected: 24,
    })

    const emailProps = getPropsService.getProps({
      manuscript,
      removalDay,
      toUser: reviewer,
      signatureName: editorialAssistantName,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
    })

    const {
      scheduledFirstAcceptedReviewerReminder,
      scheduledSecondAcceptedReviewerReminder,
      scheduledThirdAcceptedReviewerReminder,
      scheduledAcceptedReviewerRemoval,
    } = reminders.initialize({ logEvent, Email, Job })

    await scheduledFirstAcceptedReviewerReminder({
      userId,
      customId,
      titleText,
      emailProps,
      manuscriptId,
      firstDate: acceptedReviewerDaysList.first,
      teamMemberId: invitationId,
    })
    await scheduledSecondAcceptedReviewerReminder({
      userId,
      titleText,
      customId,
      emailProps,
      manuscriptId,
      teamMemberId: invitationId,
      secondDate: acceptedReviewerDaysList.second,
    })
    await scheduledThirdAcceptedReviewerReminder({
      userId,
      titleText,
      customId,
      emailProps,
      manuscriptId,
      journalName,
      thirdDate: acceptedReviewerDaysList.third,
      teamMemberId: invitationId,
      removalDay,
    })
    await scheduledAcceptedReviewerRemoval({
      userId,
      customId,
      titleText,
      emailProps,
      manuscriptId,
      journalName,
      teamMemberId: invitationId,
      removalDay: removalDayAcceptedReviewer,
    })
  },
})

module.exports = {
  initialize,
}
