const moment = require('moment-business-days')
const config = require('config')

const timeUnitAcceptInvitation = config.get(
  'reminders.reviewer.acceptInvitation.timeUnit',
)
const timeUnitSubmitReport = config.get(
  'reminders.reviewer.submitReport.timeUnit',
)
const {
  getEmailCopy: getReviewerInvitationsEmailCopy,
} = require('./invitationsEmailCopy')
const {
  getEmailCopy: getReviewerNotificationsEmailCopy,
} = require('./notificationsEmailCopy')

const QN_REVIEWER_NAME = 'reviewer-email'
const EMAIL_TYPE_REVIEWER_RESEND_INVITATION =
  'reviewer-resend-invitation-after-major-revision-reminder'

const initialize = ({ Job, logEvent }) => ({
  async scheduleFirstReviewerReminder({
    userId,
    journalName,
    titleText,
    firstDate,
    emailProps,
    expectedDate,
    manuscriptId,
    teamMemberId,
  }) {
    const executionDate = moment()
      .add(firstDate, timeUnitAcceptInvitation)
      .toISOString()
    const action = logEvent.actions.reminder_invitation_first

    const { paragraph, ...bodyProps } = getReviewerInvitationsEmailCopy({
      emailType: `reviewer-resend-invitation-first-reminder`,
      titleText,
      journalName,
      expectedDate,
    })

    emailProps.bodyProps = bodyProps

    const params = {
      action,
      userId,
      timeUnit: timeUnitAcceptInvitation,
      manuscriptId,
      executionDate,
      emailProps,
    }

    await Job.schedule({
      params,
      executionDate,
      teamMemberId,
      queueName: QN_REVIEWER_NAME,
    })
  },
  async scheduleSecondReviewerReminder({
    userId,
    titleText,
    secondDate,
    emailProps,
    expectedDate,
    manuscriptId,
    teamMemberId,
  }) {
    const executionDate = moment()
      .add(secondDate, timeUnitAcceptInvitation)
      .toISOString()
    const action = logEvent.actions.reminder_invitation_second

    const { paragraph, ...bodyProps } = getReviewerInvitationsEmailCopy({
      emailType: `reviewer-resend-invitation-second-reminder`,
      titleText,
      expectedDate,
    })

    emailProps.bodyProps = bodyProps

    const params = {
      action,
      userId,
      timeUnit: timeUnitAcceptInvitation,
      emailProps,
      manuscriptId,
      teamMemberId,
      executionDate,
    }

    await Job.schedule({
      params,
      executionDate,
      teamMemberId,
      queueName: QN_REVIEWER_NAME,
    })
  },
  async scheduleThirdReviewerReminder({
    userId,
    titleText,
    thirdDate,
    emailProps,
    expectedDate,
    manuscriptId,
    teamMemberId,
  }) {
    const executionDate = moment()
      .add(thirdDate, timeUnitAcceptInvitation)
      .toISOString()
    const action = logEvent.actions.reminder_invitation_third

    const { paragraph, ...bodyProps } = getReviewerInvitationsEmailCopy({
      emailType: `reviewer-resend-invitation-third-reminder`,
      titleText,
      expectedDate,
    })

    emailProps.bodyProps = bodyProps

    const params = {
      action,
      userId,
      timeUnit: timeUnitAcceptInvitation,
      emailProps,
      manuscriptId,
      executionDate,
    }

    await Job.schedule({
      teamMemberId,
      params,
      executionDate,
      queueName: QN_REVIEWER_NAME,
    })
  },
  async scheduledFirstReviewerReminderAfterMajorRevision({
    userId,
    titleText,
    firstDate,
    emailProps,
    expectedDate,
    manuscriptId,
    teamMemberId,
  }) {
    const executionDate = moment()
      .add(firstDate, timeUnitAcceptInvitation)
      .toISOString()
    const action = logEvent.actions.reminder_invitation_first

    const { paragraph, ...bodyProps } = getReviewerInvitationsEmailCopy({
      emailType: EMAIL_TYPE_REVIEWER_RESEND_INVITATION,
      titleText,
      expectedDate,
    })

    emailProps.bodyProps = bodyProps

    const params = {
      action,
      userId,
      timeUnit: timeUnitAcceptInvitation,
      manuscriptId,
      executionDate,
      emailProps,
    }

    await Job.schedule({
      params,
      executionDate,
      teamMemberId,
      queueName: QN_REVIEWER_NAME,
    })
  },
  async scheduledSecondReviewerReminderAfterMajorRevision({
    userId,
    titleText,
    secondDate,
    emailProps,
    expectedDate,
    manuscriptId,
    teamMemberId,
  }) {
    const executionDate = moment()
      .add(secondDate, timeUnitAcceptInvitation)
      .toISOString()
    const action = logEvent.actions.reminder_invitation_second

    const { paragraph, ...bodyProps } = getReviewerInvitationsEmailCopy({
      emailType: EMAIL_TYPE_REVIEWER_RESEND_INVITATION,
      titleText,
      expectedDate,
    })

    emailProps.bodyProps = bodyProps

    const params = {
      action,
      userId,
      timeUnit: timeUnitAcceptInvitation,
      manuscriptId,
      executionDate,
      emailProps,
    }

    await Job.schedule({
      params,
      executionDate,
      teamMemberId,
      queueName: QN_REVIEWER_NAME,
    })
  },
  async scheduledThirdReviewerReminderAfterMajorRevision({
    userId,
    titleText,
    thirdDate,
    emailProps,
    expectedDate,
    manuscriptId,
    teamMemberId,
  }) {
    const executionDate = moment()
      .add(thirdDate, timeUnitAcceptInvitation)
      .toISOString()
    const action = logEvent.actions.reminder_invitation_third

    const { paragraph, ...bodyProps } = getReviewerInvitationsEmailCopy({
      emailType: EMAIL_TYPE_REVIEWER_RESEND_INVITATION,
      titleText,
      expectedDate,
    })

    emailProps.bodyProps = bodyProps

    const params = {
      action,
      userId,
      timeUnit: timeUnitAcceptInvitation,
      manuscriptId,
      executionDate,
      emailProps,
    }

    await Job.schedule({
      teamMemberId,
      params,
      executionDate,
      queueName: QN_REVIEWER_NAME,
    })
  },
  async scheduledFirstAcceptedReviewerReminder({
    userId,
    customId,
    titleText,
    firstDate,
    emailProps,
    manuscriptId,
    teamMemberId,
  }) {
    const executionDate = moment()
      .add(firstDate, timeUnitSubmitReport)
      .toISOString()

    const action = logEvent.actions.reminder_submit_report_first

    const { paragraph, ...bodyProps } = getReviewerNotificationsEmailCopy({
      emailType: 'reviewer-submit-report-first-reminder',
      titleText,
    })

    emailProps.bodyProps = bodyProps
    emailProps.content.paragraph = paragraph
    emailProps.content.subject = `${customId}: Review Report`

    const params = {
      action,
      userId,
      timeUnit: timeUnitSubmitReport,
      manuscriptId,
      executionDate,
      emailProps,
    }

    await Job.schedule({
      teamMemberId,
      params,
      executionDate,
      queueName: QN_REVIEWER_NAME,
    })
  },
  async scheduledSecondAcceptedReviewerReminder({
    userId,
    customId,
    titleText,
    secondDate,
    emailProps,
    manuscriptId,
    teamMemberId,
  }) {
    const executionDate = moment()
      .add(secondDate, timeUnitSubmitReport)
      .toISOString()
    const action = logEvent.actions.reminder_submit_report_second

    const { paragraph, ...bodyProps } = getReviewerNotificationsEmailCopy({
      emailType: 'reviewer-submit-report-second-reminder',
      titleText,
    })

    emailProps.bodyProps = bodyProps
    emailProps.content.paragraph = paragraph
    emailProps.content.subject = `${customId}: Review Report Overdue`

    const params = {
      action,
      userId,
      timeUnit: timeUnitSubmitReport,
      manuscriptId,
      executionDate,
      emailProps,
    }

    await Job.schedule({
      teamMemberId,
      params,
      executionDate,
      queueName: QN_REVIEWER_NAME,
    })
  },
  async scheduledThirdAcceptedReviewerReminder({
    userId,
    customId,
    thirdDate,
    titleText,
    emailProps,
    removalDay,
    manuscriptId,
    teamMemberId,
  }) {
    const executionDate = moment()
      .add(thirdDate, timeUnitSubmitReport)
      .toISOString()
    const action = logEvent.actions.reminder_submit_report_third

    const { paragraph, ...bodyProps } = getReviewerNotificationsEmailCopy({
      emailType: 'reviewer-submit-report-third-reminder',
      titleText,
      removalDay,
    })

    emailProps.bodyProps = bodyProps
    emailProps.content.paragraph = paragraph
    emailProps.content.subject = `${customId}: Review Report Overdue`

    const params = {
      action,
      userId,
      timeUnit: timeUnitSubmitReport,
      manuscriptId,
      executionDate,
      emailProps,
    }

    await Job.schedule({
      teamMemberId,
      params,
      executionDate,
      queueName: QN_REVIEWER_NAME,
    })
  },

  async scheduledAcceptedReviewerRemoval({
    customId,
    titleText,
    emailProps,
    teamMemberId,
    manuscriptId,
    removalDay,
  }) {
    const executionDate = moment()
      .add(removalDay, timeUnitSubmitReport)
      .toISOString()
    const { paragraph, ...bodyProps } = getReviewerNotificationsEmailCopy({
      emailType: 'reviewer-submit-report-remove',
      titleText,
    })
    emailProps.bodyProps = bodyProps
    emailProps.content.paragraph = paragraph
    emailProps.content.subject = `${customId}:  Review no longer needed`

    const params = {
      manuscriptId,
      emailProps,
      teamMemberId,
      logActivity: false,
    }
    await Job.schedule({
      params,
      executionDate,
      teamMemberId,
    })
  },
})

module.exports = { initialize }
