module.exports = {
  reviewer: {
    emailJobs: require('./reviewer/emailJobs'),
    invitationsEmailCopy: require('./reviewer/invitationsEmailCopy'),
    notificationsEmailCopy: require('./reviewer/notificationsEmailCopy'),
    reminders: require('./reviewer/reminders'),
  },
  academicEditor: {
    emailJobs: require('./academicEditor/emailJobs'),
    invitationsEmailCopy: require('./academicEditor/invitationsEmailCopy'),
    notificationsEmailCopy: require('./academicEditor/notificationsEmailCopy'),
    reminders: require('./academicEditor/reminders'),
  },
}
