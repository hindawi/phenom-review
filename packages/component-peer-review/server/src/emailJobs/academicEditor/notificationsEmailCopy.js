const config = require('config')

const staffEmail = config.get('staffEmail')

const getNotificationsEmailCopy = ({
  emailType,
  titleText,
  reviewLink,
  journalName,
  targetUserName,
  shortReviewLink,
  triageEditorLabel,
  editorialAssistantLabel,
}) => {
  let paragraph
  let hasLink = true
  let hasIntro = true
  let hasSignature = true
  switch (emailType) {
    case 'academic-editor-removed':
      hasIntro = true
      hasLink = false
      hasSignature = true
      paragraph = `The ${triageEditorLabel ||
        editorialAssistantLabel} removed you from ${titleText}.<br/><br/>
        If you have any questions regarding this action, please let us know at ${staffEmail}.<br/><br/>
        Thank you for reviewing ${journalName}.`
      break
    case 'triage-editor-academic-editor-removed':
      paragraph = `Dr. ${targetUserName} has declined to handle ${titleText}. Please log in to the system and assign the manuscript to another editor.<br/><br/>`
      break
    case 'editorial-assistant-academic-editor-removed':
      paragraph = `Dr. ${targetUserName} has declined to handle ${titleText}. Please log in to the system and assign the manuscript to another editor.<br/><br/>`
      break
    case 'academic-editor-reviewer-invitation-first-reminder':
      paragraph = `
        This is a follow up regarding ${titleText}, which you are assigned to handle. 
      We would appreciate it if you could check the manuscript as soon as possible to see if it is 
      of reasonable quality to proceed with the peer review process. If you find that it is, 
      we ask that you please invite at least five external reviewers. If not, you may reject the 
      manuscript right away without sending it out for review.<br/>
        If you have any difficulty inviting reviewers please let us know so that we can assist you.<br/>
        You can view the submitted manuscript and invite reviewers using the following direct link:`
      break
    case 'academic-editor-reviewer-invitation-second-reminder':
      paragraph = `
          This is a follow up regarding ${titleText}. We would appreciate it if you could please take the 
        needed editorial action within two days; otherwise, we will have to consider reassigning the 
        manuscript to another Editor in order to avoid any delay with the review process. If you are 
        having difficulties assigning reviewers via the Review System, please follow the steps below:
        <ol>
          <li>Access the Review System at <a style="color:#007e92; text-decoration: none;" href="${reviewLink}">${shortReviewLink}</a> with your username and password.</li>
          <li>Find the relevant manuscript in your dashboard.</li>
          <li>You can then either invite reviewers or reject the manuscript.</li>
          <li>If you decide to invite reviewers, you will find a list of suggested reviewers from which you can choose at least five reviewers.</li>
          <li>Reviewers from outside the suggested list can be invited by manually adding the details of the reviewers.</li>
        </ol> 
        Once you have invited reviewers, an automatic review request will be sent to them.<br/>
          Please let me know if there is anything that I can help you with.
        `
      hasLink = false
      break
    case 'ea-academic-editor-not-invited-enough-reviewers':
      hasIntro = false
      paragraph = `The Editor has not invited enough reviewers for manuscript ${titleText}. <br/><br/>
Please can you check the reviewer invitations status and contact the Editor directly if required, or contact the ${triageEditorLabel ||
        editorialAssistantLabel} if you feel the Editor is now unresponsive. <br/><br/>`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return {
    hasLink,
    hasIntro,
    paragraph,
    hasSignature,
  }
}

module.exports = {
  getNotificationsEmailCopy,
}
