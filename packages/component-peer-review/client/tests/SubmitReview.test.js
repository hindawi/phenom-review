import React from 'react'
import 'jest-styled-components'
import '@testing-library/jest-dom/extend-expect'
import { cleanup, fireEvent } from '@testing-library/react'
import { render } from './testUtils'
import SubmitReview from '../components/SubmitReview'

const Chance = require('chance')

const chance = new Chance()

const basicReportReview = () => ({
  id: chance.guid(),
  comments: [
    {
      id: chance.guid(),
      content: '',
      type: 'public',
      files: [],
    },
    {
      id: chance.guid(),
      content: '',
      type: 'private',
      files: [],
    },
  ],
  created: chance.natural(),
  member: {
    alias: { name: { surname: chance.last(), givenNames: chance.first() } },
    role: 'reviewer',
    user: { id: chance.guid() },
    reviewerNumber: chance.d20(),
  },
  open: true,
  recommendation: 'publish',
  submitted: null,
})

const recommendations = [
  {
    label: 'Publish Unaltered',
    message: 'Recommend to Publish',
    value: 'publish',
  },
  {
    label: 'Consider after minor revision',
    message: 'Minor Revision Requested',
    value: 'minor',
  },
  {
    label: 'Major revision',
    message: 'Major Revision Requested',
    value: 'major',
  },
  {
    label: 'Reject',
    message: 'Recommend to Reject',
    value: 'reject',
  },
]

describe('Reviewers submit report', () => {
  const reviewerReport = []

  beforeEach(() => {
    reviewerReport.push(basicReportReview())
    reviewerReport.push(basicReportReview())
  })

  afterEach(cleanup)

  it('should check if reviewer recommendation info box is displayed', () => {
    const autosaveReviewForm = jest.fn()
    const toggleNoteMock = jest.fn()
    const updateDraftMock = jest.fn()

    const { getByTestId } = render(
      <SubmitReview
        autosaveReviewForm={autosaveReviewForm}
        isVisible
        options={recommendations}
        reviewerReport={reviewerReport}
        startExpanded
        toggleNote={toggleNoteMock}
        updateDraftReview={updateDraftMock}
      />,
    )

    const reviewerRecommendationInfoBoxTitle = getByTestId(
      'reviewer-recommendation-info-box-title',
    )
    const reviewerRecommendationInfoBoxDescription = getByTestId(
      'reviewer-recommendation-info-box-description',
    )
    expect(reviewerRecommendationInfoBoxTitle).toBeVisible()
    expect(reviewerRecommendationInfoBoxDescription).toBeVisible()
  })

  it('should toggle the private note field', () => {
    const autosaveReviewForm = jest.fn()
    const toggleNoteMock = jest.fn()
    const updateDraftMock = jest.fn()

    const { getByText, getByTestId } = render(
      <SubmitReview
        autosaveReviewForm={autosaveReviewForm}
        isVisible
        options={recommendations}
        reviewerReport={reviewerReport}
        startExpanded
        toggleNote={toggleNoteMock}
        updateDraftReview={updateDraftMock}
      />,
    )

    const togglePrivateNote = getByText(
      'Add Confidential Note for the Editorial Team',
    )
    fireEvent.click(togglePrivateNote)

    const privateNoteText = getByText(
      'Confidential Note for the Editorial Team',
    )
    const privateField = getByTestId('textarea-form-report-add-note')
    const removeField = getByText('Remove')
    expect(privateField).toBeInTheDocument()
    expect(privateNoteText).toBeInTheDocument()

    fireEvent.click(removeField)

    expect(togglePrivateNote).toBeTruthy()
  })

  it('should write in private field and clear on remove', () => {
    const autosaveReviewForm = jest.fn()
    const toggleNoteMock = jest.fn()
    const updateDraftMock = jest.fn()
    const { getByText, getByTestId, queryByText } = render(
      <SubmitReview
        autosaveReviewForm={autosaveReviewForm}
        isVisible
        options={recommendations}
        reviewerReport={reviewerReport}
        startExpanded
        toggleNote={toggleNoteMock}
        updateDraftReview={updateDraftMock}
      />,
    )
    const togglePrivateNote = getByText(
      'Add Confidential Note for the Editorial Team',
    )
    fireEvent.click(togglePrivateNote)

    const privateField = getByTestId('textarea-form-report-add-note')
    fireEvent.change(privateField, {
      target: { name: 'private.content', value: 'Text for test' },
    })
    expect(getByText('Text for test')).toBeInTheDocument()

    const removeField = getByText('Remove')
    expect(removeField).toBeInTheDocument()
    expect(
      getByText('Confidential Note for the Editorial Team'),
    ).toBeInTheDocument()

    fireEvent.click(removeField)
    expect(
      getByText('Add Confidential Note for the Editorial Team'),
    ).toBeInTheDocument()

    fireEvent.click(getByText('Add Confidential Note for the Editorial Team'))
    expect(
      getByText('Confidential Note for the Editorial Team'),
    ).toBeInTheDocument()

    expect(queryByText('Text for test')).toBeNull()
  })

  it('should open modal after pressing submit button', async done => {
    const autosaveReviewForm = jest.fn()
    const toggleNoteMock = jest.fn()
    const updateDraftMock = jest.fn()
    const { getByText, getByTestId, selectOption } = render(
      <SubmitReview
        autosaveReviewForm={autosaveReviewForm}
        isVisible
        options={recommendations}
        reviewerReport={reviewerReport}
        startExpanded
        toggleNote={toggleNoteMock}
        updateDraftReview={updateDraftMock}
      />,
    )

    selectOption('Reject')
    expect(getByText('Reject')).toBeInTheDocument()

    const publicField = getByTestId('form-report-textarea')
    fireEvent.change(publicField, {
      target: { name: 'public.content', value: 'Text for test' },
    })
    expect(getByText('Text for test')).toBeInTheDocument()

    const openModalButton = getByText('Submit Report')
    fireEvent.click(openModalButton)
    setTimeout(() => {
      expect(getByText('Ready to submit your report?')).toBeInTheDocument()
      expect(
        getByText("Once submited, the report can't be modified"),
      ).toBeInTheDocument()
      done()
    })
  })

  it('should submit reviewers reports after pressing submit report ', async done => {
    const autosaveReviewForm = jest.fn()
    const toggleNoteMock = jest.fn()
    const updateDraftMock = jest.fn()
    const onSubmitMock = jest.fn()
    const { getByText, getByTestId, selectOption } = render(
      <SubmitReview
        autosaveReviewForm={autosaveReviewForm}
        isVisible
        onSubmit={onSubmitMock}
        options={recommendations}
        reviewerReport={reviewerReport}
        startExpanded
        toggleNote={toggleNoteMock}
        updateDraftReview={updateDraftMock}
      />,
    )

    selectOption('Reject')
    expect(getByText('Reject')).toBeInTheDocument()

    const publicField = getByTestId('form-report-textarea')
    fireEvent.change(publicField, {
      target: { name: 'public.content', value: 'Text for test' },
    })
    expect(getByText('Text for test')).toBeInTheDocument()

    const openModalButton = getByText('Submit Report')
    fireEvent.click(openModalButton)
    setTimeout(() => {
      expect(getByText('Ready to submit your report?')).toBeInTheDocument()
      expect(
        getByText("Once submited, the report can't be modified"),
      ).toBeInTheDocument()

      const submitButton = getByTestId('modal-confirm')
      expect(submitButton).toBeInTheDocument()
      fireEvent.click(submitButton)
      expect(onSubmitMock).toHaveBeenCalledTimes(1)

      done()
    })
  })
})
