import { checkIfTriageEditorHasCOI } from '../triageEditorWithCOI'

describe('checkIfTriageEditorHasCOI', () => {
  it('returns false if manuscript is undefined', () => {
    expect(
      checkIfTriageEditorHasCOI({
        manuscript: undefined,
      }),
    ).toBe(false)
  })

  it('returns true if hasTriageEditorConflictOfInterest is true', () => {
    expect(
      checkIfTriageEditorHasCOI({
        manuscript: { hasTriageEditorConflictOfInterest: true },
      }),
    ).toBe(true)
  })

  it('returns false if manuscript triageEditor is undefined', () => {
    expect(
      checkIfTriageEditorHasCOI({
        manuscript: { triageEditor: undefined },
      }),
    ).toBe(false)
  })

  it('returns false when manuscript has a TE and is not in SI', () => {
    expect(
      checkIfTriageEditorHasCOI({
        manuscript: {
          triageEditor: {},
          specialIssue: undefined,
        },
      }),
    ).toBe(false)
  })

  it('returns false when manuscript is in SI and specialIssueTriageEditors is undefined', () => {
    expect(
      checkIfTriageEditorHasCOI({
        manuscript: {
          triageEditor: { user: { id: 'TE_1' } },
          specialIssue: {},
        },
        specialIssueTriageEditors: undefined,
      }),
    ).toBe(false)
  })

  it('returns false when manuscript is in SI and TE is not LGE', () => {
    expect(
      checkIfTriageEditorHasCOI({
        manuscript: {
          triageEditor: { user: { id: 'TE_1' } },
          specialIssue: {},
        },
        specialIssueTriageEditors: [{ user: { id: 'TE_2' } }],
      }),
    ).toBe(false)
  })

  it('returns false when manuscript is in SI and TE is LGE and specialIssueAcademicEditors is undefined', () => {
    expect(
      checkIfTriageEditorHasCOI({
        manuscript: {
          triageEditor: { user: { id: 'TE_2' } },
          specialIssue: {},
          academicEditors: [],
        },
        specialIssueTriageEditors: [{ user: { id: 'TE_2' } }],
        specialIssueAcademicEditors: undefined,
      }),
    ).toBe(false)
  })

  it('returns false when manuscript is in SI and TE is LGE and manuscript.academiEditors is undefined', () => {
    expect(
      checkIfTriageEditorHasCOI({
        manuscript: {
          triageEditor: { user: { id: 'TE_2' } },
          specialIssue: {},
          academicEditors: undefined,
        },
        specialIssueTriageEditors: [{ user: { id: 'TE_2' } }],
        specialIssueAcademicEditors: [{ user: { id: 'AE_1' } }],
      }),
    ).toBe(false)
  })

  it('returns false when manuscript is in SI and TE is LGE and manuscript.academiEditors is empty', () => {
    expect(
      checkIfTriageEditorHasCOI({
        manuscript: {
          triageEditor: { user: { id: 'TE_2' } },
          specialIssue: {},
          academicEditors: [],
        },
        specialIssueTriageEditors: [{ user: { id: 'TE_2' } }],
        specialIssueAcademicEditors: [{ user: { id: 'AE_1' } }],
      }),
    ).toBe(false)
  })

  it('returns false when manuscript is in SI and TE is LGE and LGE is not unfit', () => {
    expect(
      checkIfTriageEditorHasCOI({
        manuscript: {
          triageEditor: { user: { id: 'TE_2' } },
          specialIssue: {},
          academicEditors: [{ user: { id: 'AE_1' } }, { user: { id: 'AE_2' } }],
        },
        specialIssueTriageEditors: [{ user: { id: 'TE_2' } }],
        specialIssueAcademicEditors: [
          { user: { id: 'AE_1' } },
          { user: { id: 'AE_2' } },
          { user: { id: 'AE_3' } },
        ],
      }),
    ).toBe(false)
  })

  it('returns true when manuscript is in SI and TE is LGE and LGE is unfit', () => {
    expect(
      checkIfTriageEditorHasCOI({
        manuscript: {
          triageEditor: { user: { id: 'TE_2' } },
          specialIssue: {},
          academicEditors: [{ user: { id: 'AE_1' } }, { user: { id: 'AE_2' } }],
        },
        specialIssueTriageEditors: [{ user: { id: 'TE_2' } }],
        specialIssueAcademicEditors: [{ user: { id: 'AE_1' } }],
      }),
    ).toBe(true)
  })
})
