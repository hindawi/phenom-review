import React from 'react'
import { mockMatchMedia } from '@hindawi/ui'
import { cleanup, fireEvent } from '@testing-library/react'

import { render } from '../testUtils'
import { PreviewEmailModal } from '../../'

mockMatchMedia()

describe('Preview Email Modal', () => {
  afterEach(cleanup)
  const handleMailSubmissionMock = jest.fn()
  const handleOnCloseMock = jest.fn()
  const handleOnRevertMock = jest.fn()
  it('Should render the modal', async done => {
    const { getByText } = render(
      <PreviewEmailModal
        handleMailSubmission={handleMailSubmissionMock}
        handleOnClose={handleOnCloseMock}
        handleOnRevert={handleOnRevertMock}
        visible
      />,
    )
    setTimeout(() => {
      expect(getByText('SEND EMAIL')).toBeTruthy()
      expect(getByText('REVERT CHANGES')).toBeTruthy()
      done()
    }, 1000)
  })

  it('Should handle button click', async () => {
    const { getByText, wait } = render(
      <PreviewEmailModal
        handleMailSubmission={handleMailSubmissionMock}
        handleOnClose={handleOnCloseMock}
        handleOnRevert={handleOnRevertMock}
        visible
      />,
    )
    await wait(() => fireEvent.click(getByText('SEND EMAIL')))
    await wait(() => fireEvent.click(getByText('REVERT CHANGES')))

    expect(handleMailSubmissionMock).toBeCalledTimes(1)
    expect(handleOnRevertMock).toBeCalledTimes(1)
  })
})
