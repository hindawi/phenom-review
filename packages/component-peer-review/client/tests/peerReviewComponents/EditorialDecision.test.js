import React from 'react'
import { cleanup, fireEvent } from '@testing-library/react'
import { MockedProvider } from '@apollo/react-testing'

import { EditorialDecision } from '../..'
import { render, waitForGraphqlQueryDataResponse } from '../testUtils'

import {
  getReasonsForRejectionQueryMock,
  getSubmissionJournalIdQueryMock,
} from '../mocks/graphql/queries'

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useParams: () => ({
    submissionId: 'testid',
  }),
}))

jest.mock('../../graphql/hooks/useGetCommunicationRequest', () =>
  jest.fn(() => ({
    client: {
      query: jest.fn().mockResolvedValue({
        data: {
          template: { body: ['12', '34'], html: '<div>hello world!</div>' },
        },
      }),
      mutation: jest.fn(),
    },
    addCommentsProp: jest.fn(),
    getCommunicationPackage: jest.fn(),
    communicationPackageData: {},
  })),
)

const toggleMock = jest.fn()
const editorDecisions = [
  'publish',
  'returnToAcademicEditor',
  'reject',
  'minor',
  'major',
]
const matchMock = { params: { submissionId: 'testid' } }

const getRenderedComponentWithMockedGQL = (publisherName, onSubmitMock) => {
  const mocks = [
    getSubmissionJournalIdQueryMock,
    getReasonsForRejectionQueryMock,
  ]

  return render(
    <MockedProvider addTypename={false} fetch-policy="no-cache" mocks={mocks}>
      <EditorialDecision
        editorDecisions={editorDecisions}
        match={matchMock}
        onSubmit={onSubmitMock}
        publisher={publisherName}
        toggle={toggleMock}
      />
    </MockedProvider>,
  )
}

describe('Triage Editor Decision for Hindawi publisher', () => {
  const publisherName = 'Hindawi'
  let onSubmitMock, renderedComponent

  beforeEach(() => {
    onSubmitMock = jest.fn(() => Promise.resolve())

    renderedComponent = getRenderedComponentWithMockedGQL(
      publisherName,
      onSubmitMock,
    )
  })

  afterEach(cleanup)

  it('should display final guidelines box', () => {
    const { queryByTestId, getByText } = renderedComponent
    fireEvent.click(getByText('Your Editorial Decision'))
    expect(queryByTestId('final-decision-guidelines-title')).toBeVisible()
    expect(queryByTestId('final-decision-guidelines-description')).toBeVisible()
  })

  it('should not render message textarea when "Publish" was selected from the dropdown', () => {
    const { getByText, getByTestId, queryByTestId } = renderedComponent
    fireEvent.click(getByText('Your Editorial Decision'))
    fireEvent.mouseDown(getByTestId('decision-dropdown').firstChild)
    fireEvent.click(getByText('Publish'))
    expect(queryByTestId('triage-editor-decision-message')).toBe(null)
  })

  it('should render message textarea when option selected from dropdown', async () => {
    const { getByText, getByTestId } = renderedComponent
    fireEvent.click(getByText('Your Editorial Decision'))
    fireEvent.mouseDown(getByTestId('decision-dropdown').firstChild)

    fireEvent.click(getByText('Reject'))

    expect(getByTestId('triage-editor-decision-message')).toBeVisible()
  })

  it('should call onSubmit when manuscript rejected and message provided', async () => {
    const { getByText, getByTestId, container, wait } = renderedComponent
    fireEvent.click(getByText('Your Editorial Decision'))
    fireEvent.mouseDown(getByTestId('decision-dropdown').firstChild)
    fireEvent.click(getByText('Reject'))

    await waitForGraphqlQueryDataResponse(1000)

    fireEvent.change(getByTestId('triage-editor-decision-message'), {
      target: { value: 'test' },
    })

    fireEvent.click(
      container.querySelector(
        '#rejectDecisionInfo_selectedReasons input[value=outOfScope]',
      ),
    )

    fireEvent.click(
      container.querySelector(
        '#transferToAnotherJournal_selectedOption input[value=NO]',
      ),
    )

    await wait(() => fireEvent.click(getByText('Submit Decision')))

    fireEvent.click(getByText('Reject manuscript'))

    expect(onSubmitMock).toHaveBeenCalledTimes(1)
  })

  it('should call onSubmit when minor revision for manuscript is requested and message provided', async () => {
    const { getByText, getByTestId, wait } = renderedComponent
    fireEvent.click(getByText('Your Editorial Decision'))
    fireEvent.mouseDown(getByTestId('decision-dropdown').firstChild)
    fireEvent.click(getByText('Minor Revision'))

    fireEvent.change(getByTestId('triage-editor-decision-message'), {
      target: { value: 'test' },
    })

    await wait(() => fireEvent.click(getByText('Submit Decision')))

    fireEvent.click(getByText('Request minor revision'))

    expect(onSubmitMock).toHaveBeenCalledTimes(1)
  })

  it('should call onSubmit when major revision for manuscript is requested and message provided', async () => {
    const { getByText, getByTestId, wait } = renderedComponent
    fireEvent.click(getByText('Your Editorial Decision'))
    fireEvent.mouseDown(getByTestId('decision-dropdown').firstChild)
    fireEvent.click(getByText('Major Revision'))

    fireEvent.change(getByTestId('triage-editor-decision-message'), {
      target: { value: 'test' },
    })

    await wait(() => fireEvent.click(getByText('Submit Decision')))

    fireEvent.click(getByText('Request major revision'))

    expect(onSubmitMock).toHaveBeenCalledTimes(1)
  })

  it('should call onSubmit when publishing manuscript', async () => {
    const { getByText, getByTestId, wait } = renderedComponent
    fireEvent.click(getByText('Your Editorial Decision'))
    fireEvent.mouseDown(getByTestId('decision-dropdown').firstChild)
    fireEvent.click(getByText('Publish'))

    await wait(() => fireEvent.click(getByText('Submit Decision')))

    fireEvent.click(getByText('Publish manuscript'))

    expect(onSubmitMock).toHaveBeenCalledTimes(1)
  })

  it('should call onSubmit when returning manuscript to academic editor and message was provided', async () => {
    const { getByText, getByTestId, wait } = renderedComponent
    fireEvent.click(getByText('Your Editorial Decision'))
    fireEvent.mouseDown(getByTestId('decision-dropdown').firstChild)
    fireEvent.click(getByText('Return to Academic Editor'))

    fireEvent.change(getByTestId('triage-editor-decision-message'), {
      target: { value: 'test' },
    })

    await wait(() => fireEvent.click(getByText('Submit Decision')))

    fireEvent.click(getByText('Return manuscript'))

    expect(onSubmitMock).toHaveBeenCalledTimes(1)
  })
})

describe('Triage Editor Decision for GSW publisher', () => {
  const publisherName = 'GeoScienceWorld'
  let onSubmitMock, renderedComponent

  beforeEach(() => {
    onSubmitMock = jest.fn(() => Promise.resolve())

    renderedComponent = getRenderedComponentWithMockedGQL(
      publisherName,
      onSubmitMock,
    )
  })

  afterEach(cleanup)

  it('should not render message textarea when "Publish" was selected from the dropdown', () => {
    const { getByText, getByTestId, queryByTestId } = renderedComponent
    fireEvent.click(getByText('Your Editorial Decision'))
    fireEvent.mouseDown(getByTestId('decision-dropdown').firstChild)
    fireEvent.click(getByText('Publish'))
    expect(queryByTestId('triage-editor-decision-message')).toBe(null)
  })

  it('should render message textarea when option selected from dropdown', () => {
    const { getByText, getByTestId } = renderedComponent
    fireEvent.click(getByText('Your Editorial Decision'))
    fireEvent.mouseDown(getByTestId('decision-dropdown').firstChild)

    fireEvent.click(getByText('Reject'))
    expect(getByTestId('triage-editor-decision-message')).toBeVisible()
  })

  it('should call onSubmit when manuscript rejected and message provided', async () => {
    const { getByText, getByTestId, container, wait } = renderedComponent
    fireEvent.click(getByText('Your Editorial Decision'))
    fireEvent.mouseDown(getByTestId('decision-dropdown').firstChild)
    fireEvent.click(getByText('Reject'))

    await waitForGraphqlQueryDataResponse(1000)

    fireEvent.change(getByTestId('triage-editor-decision-message'), {
      target: { value: 'test' },
    })

    fireEvent.click(
      container.querySelector(
        '#rejectDecisionInfo_selectedReasons input[value=outOfScope]',
      ),
    )

    fireEvent.click(
      container.querySelector(
        '#transferToAnotherJournal_selectedOption input[value=NO]',
      ),
    )

    await wait(() => fireEvent.click(getByText('Submit Decision')))

    expect(getByText('Email editor')).toBeVisible()
    fireEvent.click(getByText('SEND EMAIL'))
    expect(onSubmitMock).toHaveBeenCalledTimes(1)
  })

  it('should call onSubmit when minor revision for manuscript is requested and message provided', async () => {
    const { getByText, getByTestId, wait } = renderedComponent
    fireEvent.click(getByText('Your Editorial Decision'))
    fireEvent.mouseDown(getByTestId('decision-dropdown').firstChild)
    fireEvent.click(getByText('Minor Revision'))

    fireEvent.change(getByTestId('triage-editor-decision-message'), {
      target: { value: 'test' },
    })

    await wait(() => fireEvent.click(getByText('Submit Decision')))

    expect(getByText('Email editor')).toBeVisible()
    fireEvent.click(getByText('SEND EMAIL'))
    expect(onSubmitMock).toHaveBeenCalledTimes(1)
  })

  it('should call onSubmit when publishing manuscript', async () => {
    const { getByText, getByTestId, wait } = renderedComponent
    fireEvent.click(getByText('Your Editorial Decision'))
    fireEvent.mouseDown(getByTestId('decision-dropdown').firstChild)
    fireEvent.click(getByText('Publish'))

    await wait(() => fireEvent.click(getByText('Submit Decision')))

    fireEvent.click(getByText('Publish manuscript'))

    expect(onSubmitMock).toHaveBeenCalledTimes(1)
  })

  it('should call onSubmit when returning manuscript to academic editor and message was provided', async () => {
    const { getByText, getByTestId, wait } = renderedComponent
    fireEvent.click(getByText('Your Editorial Decision'))
    fireEvent.mouseDown(getByTestId('decision-dropdown').firstChild)
    fireEvent.click(getByText('Return to Academic Editor'))

    fireEvent.change(getByTestId('triage-editor-decision-message'), {
      target: { value: 'test' },
    })

    await wait(() => fireEvent.click(getByText('Submit Decision')))

    expect(getByText('Email editor')).toBeVisible()
    fireEvent.click(getByText('SEND EMAIL'))
    expect(onSubmitMock).toHaveBeenCalledTimes(1)
  })
})
