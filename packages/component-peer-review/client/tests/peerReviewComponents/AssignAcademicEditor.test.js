import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { cleanup, fireEvent } from '@testing-library/react'

import { render } from '../testUtils'
import { AssignAcademicEditor } from '../..'

const {
  generateManuscript,
  getManuscriptInProgressStatuses,
  getManuscriptStatuses,
  generateTeamMember,
} = require('component-generators')

jest.mock('react-apollo', () => ({
  ...jest.requireActual('react-apollo'),
  useApolloClient: () => ({
    query: jest
      .fn()
      .mockResolvedValue({ data: { getPreviewEmail: { html: 'html' } } }),
  }),
}))

const generateTeamMemberMock = () => generateTeamMember()
jest.mock('../../graphql/hooks/useAcademicEditors', () => ({
  __esModule: true,
  useAcademicEditors: jest.fn(() => [
    [generateTeamMemberMock(), generateTeamMemberMock()],
    false,
  ]),
}))

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // Deprecated
    removeListener: jest.fn(), // Deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  })),
})
const toggleMock = jest.fn()
const matchMock = { params: { submissionId: 'testid' } }
const models = {
  Manuscript: {
    InProgressStatuses: getManuscriptInProgressStatuses(),
    Statuses: getManuscriptStatuses(),
    findAllByJournalAndUserAndRole: jest.fn(),
  },
}
const { Manuscript } = models

const manuscript = generateManuscript({
  status: Manuscript.Statuses.submitted,
})

describe('Invite Academic Editor', () => {
  let renderedComponent, inviteAEMock

  beforeEach(() => {
    inviteAEMock = jest.fn()
    renderedComponent = render(
      <AssignAcademicEditor
        academicEditorLabel="Academic Editor"
        inviteAcademicEditor={inviteAEMock}
        manuscript={manuscript}
        match={matchMock}
        toggle={toggleMock}
      />,
    )
  })

  afterEach(cleanup)
  it('Should call onInvite when the academic editor was invited', async () => {
    const { getByText, getAllByText, wait } = renderedComponent

    fireEvent.click(getByText('Assign Academic Editor'))
    const [inviteEditorButton] = getAllByText('Invite')
    fireEvent.click(inviteEditorButton)
    wait(() => fireEvent.click(getByText('INVITE')))
    wait(() => fireEvent.click(getByText('SEND EMAIL')))

    await wait(() => expect(inviteAEMock).toHaveBeenCalledTimes(1))

    await wait(() =>
      expect(
        getByText('The Academic Editor was invited successfully'),
      ).toBeTruthy(),
    )
  })
})
