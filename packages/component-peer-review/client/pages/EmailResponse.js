import React from 'react'
import { get } from 'lodash'
import { compose, withProps } from 'recompose'
import { withFetching } from '@hindawi/ui'
import { AuthenticatedRoute } from 'component-authentication/client'

import { parseSearchParams } from '../utils'
import { withReviewerGQL } from '../graphql/withGQL'
import {
  NewReviewerEmail,
  AuthedEmailResponse,
  UnauthedEmailResponse,
} from './'
import { AcademicEditorInvitationRedirect } from '../components'

const EmailResponse = ({ action, location, ...props }) => {
  switch (action) {
    case 'accept-review-new-user':
      return <NewReviewerEmail {...props} />
    case 'decline-review':
      return <UnauthedEmailResponse action={action} {...props} />
    case 'accept-he':
    case 'decline-he':
      return (
        <AcademicEditorInvitationRedirect
          action={action}
          history={props.history}
          location={location}
        />
      )
    default:
      return (
        <AuthenticatedRoute location={location}>
          <AuthedEmailResponse action={action} {...props} />
        </AuthenticatedRoute>
      )
  }
}

export default compose(
  withFetching,
  withReviewerGQL,
  withProps(({ location, match }) => ({
    action: get(match, 'params.action'),
    params: parseSearchParams(location.search),
  })),
)(EmailResponse)
