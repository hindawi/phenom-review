import { withFetching } from '@hindawi/ui'
import { compose, lifecycle, withHandlers } from 'recompose'

import { parseError } from '../utils'
import { EmailResponseLayout } from '../components'
import { withReviewerGQL, withAcademicEditorGQL } from '../graphql/withGQL'

export default compose(
  withFetching,
  withReviewerGQL,
  withAcademicEditorGQL,
  withHandlers({
    acceptReviewerInvitation: ({
      history,
      setError,
      setFetching,
      acceptReviewerInvitation,
      params: { invitationId, manuscriptId, submissionId },
    }) => () => {
      setFetching(true)
      acceptReviewerInvitation({
        variables: {
          teamMemberId: invitationId,
          useCommunication: false,
        },
      })
        .then(() => {
          setFetching(false)
          history.replace(`/details/${submissionId}/${manuscriptId}`)
        })
        .catch(e => {
          setError(parseError(e))
          setFetching(false)
        })
    },
    acceptAcademicEditorInvitation: ({
      history,
      setError,
      setFetching,
      acceptAcademicEditorInvitation,
      params: { teamMemberId, manuscriptId, submissionId },
    }) => () => {
      setFetching(true)
      acceptAcademicEditorInvitation({
        variables: {
          teamMemberId,
          useCommunication: false,
        },
      })
        .then(() => {
          setFetching(false)
          history.replace(`/details/${submissionId}/${manuscriptId}`)
        })
        .catch(e => {
          setFetching(false)
          setError(parseError(e.message))
        })
    },
    declineAcademicEditorInvitation: ({
      history,
      setError,
      setFetching,
      params: { teamMemberId },
      declineAcademicEditorInvitation,
    }) => () => {
      setFetching(true)
      declineAcademicEditorInvitation({
        variables: {
          teamMemberId: teamMemberId.trim(),
          reason: '',
          useCommunication: false,
        },
      })
        .then(() => {
          setFetching(false)
          history.replace('/info-page', {
            path: '/',
            title: 'Thank you',
            buttonText: 'GO TO DASHBOARD',
            content: 'Your decision has been submitted.',
          })
        })
        .catch(e => {
          setFetching(false)
          setError(parseError(e.message))
        })
    },
  }),
  lifecycle({
    componentDidMount() {
      const {
        action,
        acceptReviewerInvitation,
        acceptAcademicEditorInvitation,
        declineAcademicEditorInvitation,
      } = this.props
      switch (action) {
        case 'accept-review':
          acceptReviewerInvitation()
          break
        case 'accept-academic-editor':
          acceptAcademicEditorInvitation()
          break
        case 'decline-academic-editor':
          declineAcademicEditorInvitation()
          break
        default:
          break
      }
    },
  }),
)(EmailResponseLayout)
