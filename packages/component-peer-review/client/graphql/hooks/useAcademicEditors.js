import { useState, useEffect } from 'react'
import { useLazyQuery } from 'react-apollo'
import { getAcademicEditors } from '../queries'

export function useAcademicEditors({
  manuscriptId,
  searchValue,
  requestedJournalEditorialBoard,
}) {
  const [
    getAcademicEditorsQuery,
    { data, loading },
  ] = useLazyQuery(getAcademicEditors, { fetchPolicy: 'network-only' })

  const [academicEditors, setAcademicEditors] = useState([])
  useEffect(() => {
    getAcademicEditorsQuery({
      variables: {
        manuscriptId,
        searchValue,
        requestedJournalEditorialBoard,
      },
    })
  }, [
    searchValue,
    requestedJournalEditorialBoard,
    manuscriptId,
    getAcademicEditorsQuery,
  ])

  useEffect(() => {
    if (data && data.getAcademicEditors.length > 0) {
      setAcademicEditors(data.getAcademicEditors)
    }
  }, [data])

  return [academicEditors, loading]
}
