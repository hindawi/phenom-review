import { useState, useEffect } from 'react'
import { useLazyQuery } from 'react-apollo'
import { getAllAcademicEditors } from '../queries'

export const useSpecialIssueAcademicEditors = ({ manuscript }) => {
  const [
    specialIssueAcademicEditors,
    setSpecialIssueAcademicEditors,
  ] = useState([])

  const [
    getAllAcademicEditorsQuery,
    { data, loading },
  ] = useLazyQuery(getAllAcademicEditors, { fetchPolicy: 'network-only' })

  useEffect(() => {
    if (manuscript && manuscript.specialIssue) {
      getAllAcademicEditorsQuery({
        variables: { specialIssueId: manuscript.specialIssue.id },
      })
    }
  }, [manuscript, getAllAcademicEditorsQuery])

  useEffect(() => {
    if (data && data.getAllAcademicEditors) {
      setSpecialIssueAcademicEditors(data.getAllAcademicEditors)
    }
  }, [data])

  return { specialIssueAcademicEditors, loading }
}
