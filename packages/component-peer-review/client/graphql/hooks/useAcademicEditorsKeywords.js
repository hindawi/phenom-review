import { useState, useEffect } from 'react'
import { useLazyQuery } from 'react-apollo'
import { getAcademicEditorsKeywords } from '../queries'

export function useAcademicEditorsKeywords({
  submissionId,
  academicEditors,
  shouldDisplayKeywords,
}) {
  const [getAcademicEditorsKeywordsQuery, { data, loading }] = useLazyQuery(
    getAcademicEditorsKeywords,
    {
      fetchPolicy: 'network-only',
    },
  )
  const [academicEditorsKeywords, setAcademicEditorsKeywords] = useState([])
  useEffect(() => {
    if (shouldDisplayKeywords && academicEditors.length > 0) {
      getAcademicEditorsKeywordsQuery({
        variables: {
          submissionId,
          editorEmails: academicEditors.map(editor => editor.alias.email),
        },
      })
    }
  }, [
    submissionId,
    academicEditors,
    shouldDisplayKeywords,
    getAcademicEditorsKeywordsQuery,
  ])

  useEffect(() => {
    if (data && data.getEditorsKeywords.length > 0) {
      setAcademicEditorsKeywords(data.getEditorsKeywords)
    }
  }, [data])

  return [academicEditorsKeywords, loading]
}
