import { useLazyQuery } from 'react-apollo'
import { isUserSubscribedToEmails } from '../queries'

export const useIsUserUnsubscribed = () => {
  const [doQuery, { data, loading, error }] = useLazyQuery<
    { isUserSubscribedToEmails?: boolean },
    { email: string }
  >(isUserSubscribedToEmails, {
    fetchPolicy: 'network-only',
  })

  return {
    requestIsUserUnsubscribed: doQuery,
    isUserUnsubscribed: data?.isUserSubscribedToEmails === false,
    isUserUnsubscribedLoading: loading,
    isUserUnsubscribedError: error,
  }
}
