import { useState, useEffect } from 'react'
import { useLazyQuery } from 'react-apollo'
import { getAllTriageEditors } from '../queries'

export const useSpecialIssueTriageEditors = ({ manuscript }) => {
  const [specialIssueTriageEditors, setSpecialIssueTriageEditors] = useState([])

  const [
    getAllTriageEditorsQuery,
    { data, loading },
  ] = useLazyQuery(getAllTriageEditors, { fetchPolicy: 'network-only' })

  useEffect(() => {
    if (manuscript && manuscript.specialIssue) {
      getAllTriageEditorsQuery({
        variables: { specialIssueId: manuscript.specialIssue.id },
      })
    }
  }, [manuscript, getAllTriageEditorsQuery])

  useEffect(() => {
    if (data && data.getAllTriageEditors) {
      setSpecialIssueTriageEditors(data.getAllTriageEditors)
    }
  }, [data])

  return { specialIssueTriageEditors, loading }
}
