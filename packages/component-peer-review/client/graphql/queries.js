import gql from 'graphql-tag'

import {
  manuscriptFragment,
  userFragment,
  fileFragment,
  teamMember,
  teamMemberWithIdentities,
} from './fragments'

// Temporary query for redirect
export const getSubmissionIds = gql`
  query getSubmissionIds($customId: String!) {
    getSubmissionIds(customId: $customId) {
      id
      submissionId
    }
  }
`

export const getSubmission = gql`
  query getSubmission($submissionId: String!) {
    getSubmission(submissionId: $submissionId) {
      ...manuscriptDetails
    }
    getDraftRevision(submissionId: $submissionId) {
      ...manuscriptDetails
      comment {
        id
        type
        content
        files {
          ...manuscriptDetailsFile
        }
      }
    }
  }
  ${manuscriptFragment}
  ${fileFragment}
`

export const getSubmissionJournalId = gql`
  query getSubmission($submissionId: String!) {
    getSubmission(submissionId: $submissionId) {
      journalId
    }
  }
`

export const getCurrentUser = gql`
  query {
    getCurrentUser {
      ...manuscriptDetailsUser
    }
  }
  ${userFragment}
`

export const getAllAcademicEditors = gql`
  query getAllAcademicEditors(
    $journalId: String
    $specialIssueId: String
    $sectionId: String
  ) {
    getAllAcademicEditors(
      journalId: $journalId
      specialIssueId: $specialIssueId
      sectionId: $sectionId
    ) {
      ...teamMember
    }
  }
  ${teamMember}
`

export const getAcademicEditors = gql`
  query getAcademicEditors(
    $manuscriptId: String!
    $searchValue: String
    $requestedJournalEditorialBoard: Boolean
  ) {
    getAcademicEditors(
      manuscriptId: $manuscriptId
      searchValue: $searchValue
      requestedJournalEditorialBoard: $requestedJournalEditorialBoard
    ) {
      ...teamMemberWithIdentities
      workload
    }
  }
  ${teamMemberWithIdentities}
`

export const getCommunicationPackage = gql`
  query getCommunicationPackage(
    $submissionId: String
    $userId: String
    $teamMemberId: String
    $comments: String
    $usage: TemplateUsage!
  ) {
    getCommunicationPackage(
      input: {
        submissionId: $submissionId
        userId: $userId
        teamMemberId: $teamMemberId
        comments: $comments
        usage: $usage
      }
    ) {
      templateName
      mailInput {
        manuscript {
          submittingAuthor
          manuscriptId
          manuscriptTitle
          journalName
          comments
        }
      }
      signedInput
    }
  }
`
export const getPreviewEmail = gql`
  query getPreviewEmail($input: GetPreviewEmailInput!) {
    getPreviewEmail(input: $input) {
      html
      subject
    }
  }
`

export const getAcademicEditorsKeywords = gql`
  query getEditorsKeywords($submissionId: String!, $editorEmails: [String]!) {
    getEditorsKeywords(
      submissionId: $submissionId
      editorEmails: $editorEmails
    ) {
      email
      terms
    }
  }
`

export const getAllTriageEditors = gql`
  query getAllTriageEditors(
    $journalId: String
    $specialIssueId: String
    $sectionId: String
  ) {
    getAllTriageEditors(
      journalId: $journalId
      specialIssueId: $specialIssueId
      sectionId: $sectionId
    ) {
      ...teamMember
    }
  }
  ${teamMember}
`

export const getTriageEditors = gql`
  query getTriageEditors($manuscriptId: String!) {
    getTriageEditors(manuscriptId: $manuscriptId) {
      ...teamMember
    }
  }
  ${teamMember}
`
export const loadReviewerSuggestions = gql`
  query loadReviewerSuggestions($manuscriptId: String!) {
    loadReviewerSuggestions(manuscriptId: $manuscriptId) {
      id
      email
      givenNames
      surname
      aff
      profileUrl
      numberOfReviews
      isInvited
    }
  }
`

export const isUserSubscribedToEmails = gql`
  query isUserSubscribedToEmails($email: String!) {
    isUserSubscribedToEmails(email: $email)
  }
`

export const getConflictingAuthors = gql`
  query getConflictingAuthors(
    $manuscriptId: String!
    $rorId: String
    $aff: String
    $country: String
  ) {
    getConflictingAuthors(
      manuscriptId: $manuscriptId
      rorId: $rorId
      aff: $aff
      country: $country
    ) {
      ...teamMember
    }
  }
  ${teamMember}
`

export const getReasonsForRejection = gql`
  query getReasonsForRejection($journalId: String!) {
    getReasonsForRejection(journalId: $journalId)
  }
`
