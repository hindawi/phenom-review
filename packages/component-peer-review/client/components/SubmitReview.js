import React, { Fragment } from 'react'
import { Formik } from 'formik'
import styled from 'styled-components'
import { get, chain, set } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'
import { withModal } from 'component-modal'
import { useJournal } from 'component-journal-info'
import { Button, FilePicker, Spinner } from '@pubsweet/ui'
import {
  FileLayoutWithScanning,
  withGQL as withFilesGQL,
} from 'component-files/client'
import { compose, withHandlers, withProps, withState } from 'recompose'
import { Alert, IconInfoSolid, IconClose } from '@hindawi/phenom-ui'

import {
  Row,
  Item,
  Icon,
  Menu,
  Label,
  Text,
  Textarea,
  ActionLink,
  MultiAction,
  withFetching,
  ContextualBox,
  ValidatedFormField,
} from '@hindawi/ui'

import { parseReviewValues } from '../../client/utils'

const validate = values => {
  const errors = {}
  if (!values.public.content.trim() && values.files.length === 0) {
    set(errors, 'public.content', 'A file or text is required.')
  }
  if (!values.recommendation) {
    set(errors, 'recommendation', 'Required')
  }

  return errors
}

const allowedFileExtensions = ['pdf', 'doc', 'docx', 'txt', 'rdf', 'odt']

const ReportForm = ({
  values = {},
  hasNote,
  onUpload,
  isFetching,
  toggleNote,
  setFieldValue,
  options,
}) => (
  <Fragment>
    <Row mb={4} mt={6} width={62}>
      <Item vertical>
        <Label mb={1} required>
          Recommendation
        </Label>
        <ValidatedFormField
          component={Menu}
          data-test-id="reviewer-report-menu"
          name="recommendation"
          options={options}
          placeholder="Choose in the list"
        />
      </Item>
    </Row>

    <Row>
      <Item alignItems="center" height={6}>
        <Label mr={2} required>
          Your Report
        </Label>
      </Item>
    </Row>
    <Item vertical>
      <ValidatedFormField
        component={Textarea}
        data-test-id="form-report-textarea"
        minHeight={18}
        name="public.content"
      />
    </Item>

    <Row alignItems="start" justify="start" mt={4}>
      {isFetching ? (
        <Spinner />
      ) : (
        values.files.length === 0 && (
          <Row alignItems="center" height={5} justify="start">
            <FilePicker
              allowedFileExtensions={allowedFileExtensions}
              name="files"
              onUpload={file => onUpload({ file, setFieldValue })}
            >
              <ActionLink
                data-test-id="form-report-upload-file"
                fontSize="14px"
                fontWeight="bold"
              >
                <Icon bold fontSize="10px" icon="expand" mr={1} />
                Upload Report File
              </ActionLink>
            </FilePicker>
          </Row>
        )
      )}
    </Row>

    {values.files.length > 0 && (
      <Row justify="flex-start" mb={4}>
        <FileStyle>
          <FileLayoutWithScanning
            file={values.files[0]}
            key={values.files[0].id}
            removeItemFromForm={() => setFieldValue('files', [])}
          />
        </FileStyle>
      </Row>
    )}

    <Row alignItems="center" justify="start" mt={4}>
      <StyledIconInfoSolid />
      <Text>
        Your report and recommendation will be visibile to the other reviewers
        involved once a decision is taken.
      </Text>
    </Row>

    <Separator my={6} />

    <Row alignItems="center" height={5}>
      {hasNote ? (
        <Fragment>
          <Row mt={2}>
            <Item>
              <Label>Confidential Note for the Editorial Team</Label>
            </Item>
            <Item alignItems="center" justify="flex-end">
              <ActionLink
                data-test-id="form-report-remove-note"
                fontSize="14px"
                onClick={toggleNote}
              >
                <Row alignItems="center">
                  <StyledIconClose />
                  Remove
                </Row>
              </ActionLink>
            </Item>
          </Row>
        </Fragment>
      ) : (
        <Item>
          <ActionLink
            data-test-id="form-report-add-note"
            fontSize="14px"
            fontWeight="bold"
            onClick={toggleNote}
          >
            <Icon bold fontSize="10px" icon="expand" mr={1} />
            <Text>Add Confidential Note for the Editorial Team</Text>
          </ActionLink>
        </Item>
      )}
    </Row>

    {hasNote && (
      <Row mt={1}>
        <ValidatedFormField
          component={Textarea}
          data-test-id="textarea-form-report-add-note"
          minHeight={18}
          name="private.content"
        />
      </Row>
    )}
  </Fragment>
)

const SubmitButton = ({ handleSubmit }) => (
  <Row justify="flex-end" mb={1} mt={6}>
    <StyledButton
      data-test-id="submit-reviewer-report"
      onClick={handleSubmit}
      primary
    >
      Submit Report
    </StyledButton>
  </Row>
)

const SubmitReview = ({
  options,
  hasNote,
  onSubmit,
  onUpload,
  onDelete,
  isFetching,
  toggleNote,
  journalCode,
  initialValues,
  reviewerReport,
  autosaveReviewForm,
  ...rest
}) => {
  const { recommendationScreenInfoBox } = useJournal()

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validate={validate}
    >
      {({ handleSubmit, values, setFieldValue }) => (
        <ContextualBox label="Your Report" mt={4} {...rest}>
          {autosaveReviewForm(values)}
          <Root>
            <StyledAlert
              description={
                <div
                  dangerouslySetInnerHTML={{
                    __html: recommendationScreenInfoBox.description,
                  }}
                  data-test-id="reviewer-recommendation-info-box-title"
                />
              }
              message={
                <div
                  dangerouslySetInnerHTML={{
                    __html: recommendationScreenInfoBox.title,
                  }}
                  data-test-id="reviewer-recommendation-info-box-description"
                />
              }
              type="info"
            />

            <ReportForm
              hasNote={hasNote}
              isFetching={isFetching}
              journalCode={journalCode}
              onDelete={onDelete}
              onUpload={onUpload}
              options={options}
              setFieldValue={setFieldValue}
              toggleNote={toggleNote({ setFieldValue, values })}
              values={values}
            />

            <SubmitButton handleSubmit={handleSubmit} values={values} />
          </Root>
        </ContextualBox>
      )}
    </Formik>
  )
}

const mergeWithCustomizer = objValue => {
  if (objValue === null) {
    return objValue || ''
  }
  return objValue
}

export default compose(
  withFetching,
  withFilesGQL(),
  withProps(({ reviewerReport }) => ({
    initialValues: {
      public: chain(reviewerReport)
        .get('comments', [])
        .find(c => c.type === 'public')
        .mergeWith({ content: '' }, mergeWithCustomizer)
        .value(),
      private: chain(reviewerReport)
        .get('comments', [])
        .find(c => c.type === 'private')
        .mergeWith({ content: '' }, mergeWithCustomizer)
        .value(),
      recommendation: get(reviewerReport, 'recommendation')
        ? get(reviewerReport, 'recommendation')
        : undefined,
      reviewId: get(reviewerReport, 'id'),
      files: chain(reviewerReport)
        .get('comments', [])
        .find(c => c.type === 'public')
        .get('files', [])
        .value(),
    },
  })),
  withState('hasNote', 'setNote', false),
  withModal({
    component: MultiAction,
    modalKey: 'reviewerReport',
  }),
  withHandlers({
    toggleNote: ({ setNote, updateDraftReview }) => ({
      setFieldValue,
      values,
    }) => () => {
      setFieldValue('private.content', '')
      setNote(v => !v)
      values.private.content = ''
      const parsedValues = parseReviewValues(values)
      updateDraftReview({ variables: { ...parsedValues } })
    },
    onSubmit: ({ onSubmit, showModal }) => values => {
      showModal({
        cancelText: 'Not yet',
        confirmText: 'Submit report',
        subtitle: "Once submited, the report can't be modified",
        onConfirm: modalProps => onSubmit(values, modalProps),
        title: 'Ready to submit your report?',
      })
    },
    onUpload: ({
      uploadFile,
      initialValues: {
        public: { id },
      },
      setFetching,
      setError,
    }) => ({ file, setFieldValue }) => {
      const fileInput = {
        type: 'reviewComment',
        size: file.size,
      }
      setFetching(true)
      uploadFile({ entityId: id, fileInput, file })
        .then(file => {
          setFieldValue('files', [file])
          setFetching(false)
        })
        .catch(e => {
          setError(e.message)
          setFetching(false)
        })
    },
  }),
)(SubmitReview)

const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  padding: calc(${th('gridUnit')} * 4);

  .erron-wrapper {
    height: auto;
  }
`

const StyledAlert = styled(Alert)`
  ul {
    padding-left: 30px;
  }

  p {
    margin-top: 5px;
    margin-bottom: 0;
  }
`

const StyledIconInfoSolid = styled(IconInfoSolid)`
  svg {
    font-size: 16px;
    margin-right: 4px;
  }

  svg > path {
    fill: ${th('iconSolidInfo')};
  }
`

const StyledIconClose = styled(IconClose)`
  svg {
    font-size: 16px;
    margin-right: 4px;
  }

  svg > path {
    fill: ${th('colorWarning')};
  }
`

const StyledButton = styled(Button)`
  height: 32px;
  padding: 0 12px;
  width: auto;
`

const Separator = styled(Row)`
  background-color: ${th('colorFurniture')};
  height: 1px;
  margin: 24px 0;
`

const FileStyle = styled.div`
  width: 100%;
`
