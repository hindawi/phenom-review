import { useMutation } from 'react-apollo'
import { mutations } from '../graphql'
import { refetchGetSubmission } from '../graphql/refetchQueries'

const useEditorialRecommendation = ({ manuscriptId, match }) => {
  const [requestRevision] = useMutation(mutations.requestRevision, {
    refetchQueries: [refetchGetSubmission(match)],
  })
  const [makeRecommendationToReject] = useMutation(
    mutations.makeRecommendationToReject,
    {
      refetchQueries: [refetchGetSubmission(match)],
    },
  )
  const [makeRecommendationToPublish] = useMutation(
    mutations.makeRecommendationToPublish,
    {
      refetchQueries: [refetchGetSubmission(match)],
    },
  )

  const handleEditorialRecommendation = values => {
    let handleRecommendationFn
    switch (values.recommendation) {
      case 'reject':
        handleRecommendationFn = makeRecommendationToReject({
          variables: {
            manuscriptId,
            input: {
              messageForAuthor: values.public,
              messageForTriage: values.private,
              emailContentChanges: values.emailContentChanges,
            },
          },
        })
        break
      case 'publish':
        handleRecommendationFn = makeRecommendationToPublish({
          variables: {
            manuscriptId,
            input: {
              messageForAuthor: values.public,
              messageForTriage: values.private,
              emailContentChanges: values.emailContentChanges,
            },
          },
        })
        break
      case 'minor':
      case 'major':
        handleRecommendationFn = requestRevision({
          variables: {
            manuscriptId,
            content: values.public,
            type: values.recommendation,
            emailContentChanges: values.emailContentChanges,
          },
        })
        break
      default:
        return null
    }
  }

  return { handleEditorialRecommendation }
}

export default useEditorialRecommendation
