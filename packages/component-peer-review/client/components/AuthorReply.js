import React from 'react'
import { isEmpty, get } from 'lodash'
import { CommentWithFile } from './peerReviewComponents'

const AuthorReply = ({ authorResponse, submittingAuthor, role }) => {
  if (isEmpty(authorResponse)) return null

  const date = get(authorResponse, 'submitted')
  const message = get(authorResponse, 'comments.0.content')
  const file = get(authorResponse, 'comments.0.files.0')
  const authorName = `${get(
    submittingAuthor,
    'alias.name.givenNames',
    '',
  )} ${get(submittingAuthor, 'alias.name.surname', '')}`

  return (
    <CommentWithFile
      authorName={authorName}
      boxLabel="Response to Revision Request"
      commentLabel={role === 'author' ? 'Your Reply' : 'Author Reply'}
      date={date}
      file={file}
      message={message}
      mt={4}
    />
  )
}

export default AuthorReply
