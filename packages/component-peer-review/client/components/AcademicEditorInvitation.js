import React from 'react'
import { Row, ActionLink, Icon } from '@hindawi/ui'

import PersonInvitation from './PersonInvitation'

const AcademicEditorInvitation = ({
  toggle,
  academicEditor,
  cancelInvitation,
}) => (
  <Row justify="flex-start">
    <PersonInvitation
      invitation={academicEditor}
      label="Academic Editor"
      onRevoke={cancelInvitation}
    />
    {!academicEditor.id && (
      <ActionLink fontWeight={600} onClick={toggle} secondary whenHover>
        <Icon icon="addUser" ml={1} mr={1} />
        Invite
      </ActionLink>
    )}
  </Row>
)

export default AcademicEditorInvitation
