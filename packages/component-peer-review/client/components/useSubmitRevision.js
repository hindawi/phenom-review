import { useMutation } from 'react-apollo'
import { get } from 'lodash'
import { mutations as submissionMutations } from 'component-submission/client'
import { mutations } from '../graphql'
import { refetchGetSubmission } from '../graphql/refetchQueries'
import { parseError as parseGraphqlError } from '../utils'

const useSubmitRevision = ({ history, match, revisionDraft }) => {
  const [addAuthor] = useMutation(submissionMutations.addAuthorToManuscript)
  const [editAuthor] = useMutation(submissionMutations.editAuthorFromManuscript)
  const [removeAuthor] = useMutation(
    submissionMutations.removeAuthorFromManuscript,
  )
  const [updateManuscriptFile] = useMutation(
    submissionMutations.updateManuscriptFile,
  )

  const [submitRevisionMutation] = useMutation(mutations.submitRevision, {
    refetchQueries: [refetchGetSubmission(match)],
    awaitRefetchQueries: true,
  })
  const [updateAutosaveRevision] = useMutation(mutations.updateAutosave)
  const [updateDraftRevision] = useMutation(mutations.updateDraftRevision)

  const submitRevision = (values, modalProps) => {
    modalProps.setFetching(true)

    submitRevisionMutation({
      variables: {
        submissionId: get(match, 'params.submissionId', ''),
      },
    })
      .then(() => {
        const path = `/details/${get(match, 'params.submissionId')}/${get(
          revisionDraft,
          'id',
        )}`
        history.push(path)
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(parseGraphqlError(e.message))
      })
  }

  return {
    addAuthor,
    editAuthor,
    removeAuthor,
    submitRevision,
    updateDraftRevision,
    updateManuscriptFile,
    updateAutosaveRevision,
  }
}

export default useSubmitRevision
