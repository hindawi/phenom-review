import React from 'react'
import { get } from 'lodash'
import { space } from 'styled-system'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { compose, withHandlers } from 'recompose'
import { WizardFiles } from 'component-submission/client/components'
import { Row, Text, Icon, ContextualBox } from '@hindawi/ui'

const ManuscriptFiles = ({
  formValues,
  formErrors,
  onChangeList,
  onDeleteFile,
  onUploadFile,
  startExpanded,
  manuscriptStatus,
  setIsFileUploading,
}) => (
  <ContextualBox
    label="Manuscript Files"
    startExpanded={startExpanded}
    transparent
  >
    <SectionRoot ml={2} pl={2}>
      <Row justify="flex-start" mb={4}>
        <Text display="inline" secondary>
          Drag & Drop files in the specific section or click{' '}
          <Icon
            bold
            color="colorSecondary"
            fontSize="10px"
            icon="expand"
            mr={1}
          />
          to upload.
        </Text>
      </Row>

      <WizardFiles
        compact
        files={get(formValues, 'files', [])}
        manuscriptStatus={manuscriptStatus}
        onChangeList={onChangeList}
        onDeleteFile={onDeleteFile}
        onUploadFile={onUploadFile}
        setIsFileUploading={setIsFileUploading}
      />

      {get(formErrors, 'fileError', false) && (
        <Row justify="flex-start" mt={2}>
          <Text error>{get(formErrors, 'fileError', '')}</Text>
        </Row>
      )}
    </SectionRoot>
  </ContextualBox>
)

export default compose(
  withHandlers({
    onChangeList: ({ updateManuscriptFile }) => ({
      fileId,
      sourceProps,
      toListName: type,
      destinationProps,
    }) => {
      updateManuscriptFile({
        variables: {
          fileId,
          type,
        },
      })
        .then(r => {
          const file = r.data.updateManuscriptFile
          sourceProps.remove(sourceProps.index)
          destinationProps.push(file)
        })
        .catch(e => {
          destinationProps.setError(e.message)
        })
    },
    onDeleteFile: ({ handleDelete }) => (
      file,
      { index, remove, setError, setFetching },
    ) => {
      handleDelete(file, {
        setError,
        setFetching,
        index,
        remove,
      })
    },
    onUploadFile: ({ revisionManuscriptId, handleUpload }) => (
      file,
      { type, push, setFetching, setError },
    ) => {
      handleUpload(file, {
        type,
        entityId: revisionManuscriptId,
        setFetching,
        setError,
        push,
      })
    },
  }),
)(ManuscriptFiles)

const SectionRoot = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  border-left: 2px solid ${th('colorBorder')};

  ${space};
`
