import React, { Fragment } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { CopyEmail, Text } from '@hindawi/ui'
import { get } from 'lodash'

const NameTableCell = ({ reviewer }) => {
  const formattedName = [
    get(reviewer, 'alias.name.givenNames', ''),
    get(reviewer, 'alias.name.surname'),
  ].join(' ')

  return (
    <NameTd colSpan={5}>
      <CopyEmail email={get(reviewer, 'alias.email')}>
        <Fragment>
          {formattedName}
          {reviewer.reviewerNumber && (
            <Text customId ml={2}>
              {`Reviewer ${reviewer.reviewerNumber}`}
            </Text>
          )}
        </Fragment>
      </CopyEmail>
    </NameTd>
  )
}

const NameTd = styled.td`
  color: ${th('colorSecondary')};
  font-family: ${th('defaultFont')};
  text-decoration: underline;

  position: relative;
`

export default NameTableCell
