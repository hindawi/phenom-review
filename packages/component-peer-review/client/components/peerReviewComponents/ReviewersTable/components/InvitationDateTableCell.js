import React from 'react'
import { DateParser } from '@pubsweet/ui'

const InvitationDateTableCell = ({ reviewer }) => (
  <td>
    <DateParser timestamp={reviewer.invited}>
      {timestamp => timestamp}
    </DateParser>
  </td>
)

export default InvitationDateTableCell
