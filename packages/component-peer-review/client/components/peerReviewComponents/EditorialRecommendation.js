import React, { useState } from 'react'
import { Formik } from 'formik'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { compose, withProps } from 'recompose'
import { Button } from '@hindawi/phenom-ui'

import {
  Row,
  Text,
  Menu,
  Item,
  Label,
  Textarea,
  validators,
  ContextualBox,
  ValidatedFormField,
} from '@hindawi/ui'
import { MailTemplate } from '@hindawi/phenom-mail-template'

import { PreviewEmailModal } from './PreviewEmailModal'
import useGetPreviewEmailRequest from '../../graphql/hooks/useGetPreviewEmailRequest'

const recommendationTexts = {
  reject: {
    successMessage: 'Manuscript has been recommended for rejection',
    usecase: 'EditorialRecommendationToReject(TE)',
    button: 'Submit Recommendation',
  },
  publish: {
    successMessage: 'Manuscript has been recommended for publish',
    usecase: 'EditorialRecommendationToPublish(TE)',
    button: 'Submit Recommendation',
  },
  minor: {
    successMessage: 'Manuscript has been recommended for revision(minor)',
    usecase: 'EditorialRecommendationToRevise(Author)',
    button: 'Request Revision',
  },
  major: {
    successMessage: 'Manuscript has been recommended for revision(major)',
    usecase: 'EditorialRecommendationToRevise(Author)',
    button: 'Request Revision',
  },
}

const errorMessage = `Please try sending the recommendation again. If the issue persists, contact the Customer Support Team on help@hindawi.com`

const RecommendationForm = ({
  triageEditorLabel,
  options,
  onVisibility,
  formValues,
  handleSubmit,
  setFieldValue,
  fieldForAuthor,
  fieldAuthorOptional,
  fieldForTriageEditor,
  match,
}) => {
  const texts = recommendationTexts[formValues.recommendation]
  const { submissionId } = match.params

  const {
    emailContentChanges,
    showMailPreview,
    currentHtml,
    loading,
    handleSentEmail,
    handleRequestEmailPreview,
    handleOnChange,
    handleOnClose,
    handleOnRevert,
  } = useGetPreviewEmailRequest()

  const isDisabled = !(
    formValues.recommendation === 'publish' || formValues.public
  )
  const [sendEmailLoading, setSendEmailLoading] = useState(false)

  const handleMailSubmission = () => {
    setFieldValue('emailContentChanges', emailContentChanges)
    setSendEmailLoading(true)
    // because we manually update emailContentChanges field with `setFieldValue`, that triggers some update mechanism
    // inside Formik and prevents it from triggering `submitForm`
    // this is why we need to wait a bit for that action to clear up and then be able to run submit form action
    setTimeout(() => {
      handleSubmit(formValues)
      handleSentEmail(
        recommendationTexts[formValues.recommendation].successMessage,
      )
      setSendEmailLoading(false)
    }, 10)
  }

  return (
    <Root>
      <Row width={62}>
        <Item vertical>
          <Label required>Recommendation</Label>
          <ValidatedFormField
            component={Menu}
            data-test-id="recommendation-dropdown"
            name="recommendation"
            options={options}
            validate={[validators.required]}
          />
        </Item>
      </Row>
      <ResponsiveRow mt={2}>
        {fieldForAuthor && (
          <ResponsiveItem
            data-test-id="editorial-recommendation-message-for-author"
            mr={2}
            vertical
          >
            <Label required>Message for Author </Label>
            <ValidatedFormField
              component={Textarea}
              name="public"
              validate={[validators.required]}
            />
          </ResponsiveItem>
        )}

        {fieldAuthorOptional && (
          <ResponsiveItem
            data-test-id="editorial-recommendation-message-for-author"
            ml={2}
            vertical
          >
            <Label>
              Message for Author
              <Text ml={2} secondary>
                Optional
              </Text>
            </Label>
            <ValidatedFormField component={Textarea} name="public" />
          </ResponsiveItem>
        )}

        {fieldForTriageEditor && (
          <ResponsiveItem
            data-test-id="editorial-recommendation-message-for-triage-editor"
            ml={2}
            vertical
          >
            <Label>
              Message for {triageEditorLabel}
              <Text ml={2} secondary>
                Optional
              </Text>
            </Label>
            <ValidatedFormField component={Textarea} name="private" />
          </ResponsiveItem>
        )}
      </ResponsiveRow>

      {onVisibility && (
        <Row justify="flex-end" mb={4} mt={2}>
          <Button
            data-test-id="button-editorial-recommendation-submit"
            disabled={isDisabled}
            onClick={handleRequestEmailPreview({
              errorMessage,
              input: {
                submissionId,
                usecase: texts.usecase,
                additionalComments: formValues.public,
              },
            })}
            type="primary"
            width={48}
          >
            {texts.button}
          </Button>
        </Row>
      )}

      {formValues.recommendation && (
        <PreviewEmailModal
          handleMailSubmission={handleMailSubmission}
          handleOnClose={handleOnClose}
          handleOnRevert={handleOnRevert}
          modalLoading={loading}
          successLoading={sendEmailLoading}
          title="Editorial Recommendation"
          visible={showMailPreview}
        >
          <MailTemplate html={currentHtml} onChange={handleOnChange} />
        </PreviewEmailModal>
      )}
    </Root>
  )
}

const EnhancedRecommendationForm = compose(
  withProps(({ formValues, options, hasPublishOption }) => ({
    fieldAuthorOptional: formValues.recommendation === 'publish',
    fieldForTriageEditor: ['publish', 'reject'].includes(
      formValues.recommendation,
    ),
    fieldForAuthor: ['reject', 'major', 'minor'].includes(
      formValues.recommendation,
    ),
    onVisibility: ['reject', 'minor', 'publish', 'major'].includes(
      formValues.recommendation,
    ),
    options: hasPublishOption
      ? options.filter(o => o.value)
      : options.filter(o => !['publish'].includes(o.value)),
  })),
)(RecommendationForm)

const EditorialRecommendation = ({
  options,
  toggle,
  triageEditorLabel,
  onSubmit,
  hasPeerReview,
  initialValues,
  expanded,
  hasPublishOption,
  highlight,
  match,
  publisher,
  ...rest
}) => (
  <Formik initialValues={initialValues} onSubmit={onSubmit}>
    {({ handleSubmit, values: formValues, setFieldValue }) => (
      <ContextualBox
        expanded={expanded}
        highlight={highlight}
        label="Your Editorial Recommendation"
        mt={4}
        toggle={toggle}
        {...rest}
        data-test-id="contextual-box-editorial-recommendation"
      >
        <EnhancedRecommendationForm
          formValues={formValues}
          handleSubmit={handleSubmit}
          hasPeerReview={hasPeerReview}
          hasPublishOption={hasPublishOption}
          match={match}
          options={options}
          publisher={publisher}
          setFieldValue={setFieldValue}
          triageEditorLabel={triageEditorLabel}
        />
      </ContextualBox>
    )}
  </Formik>
)

export default EditorialRecommendation

EditorialRecommendation.propTypes = {
  /** Specifies AcademicEditor recommendation options */
  options: PropTypes.arrayOf(PropTypes.object),
  /** Handles the submission of the recommendation */
  highlight: PropTypes.bool,
}

EditorialRecommendation.defaultProps = {
  options: [],
  highlight: false,
}

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  display: flex;
  flex-direction: column;
  padding: calc(${th('gridUnit')} * 4);
  padding-bottom: 0;
`
const ResponsiveRow = styled(Row)`
  @media (max-width: 800px) {
    flex-direction: column;
  }
`
const ResponsiveItem = styled(Item)`
  @media (max-width: 800px) {
    margin-right: 0;
    margin-left: 0;
    width: 100%;
  }
`
// #endregion
