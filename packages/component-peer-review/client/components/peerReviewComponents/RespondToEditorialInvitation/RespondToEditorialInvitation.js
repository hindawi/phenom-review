import React, { useState } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Radio, Form } from '@hindawi/phenom-ui'
import { ContextualBox } from '@hindawi/ui'

import {
  DeclineReason,
  ValidationRule,
  ConfirmationButton,
} from '../RespondToEditorialInvitation'

const RespondToEditorialInvitation = ({
  history,
  onSubmit,
  highlight,
  publisher,
  teamMember,
  startExpanded,
  academicEditorLabel,
  hasTriageEditorConflictOfInterest,
}) => {
  const academicEditorInvitationOptions = [
    { label: 'Agree', value: 'yes' },
    { label: 'Decline', value: 'no' },
  ]

  const INITIAL_VALUES = {
    academicEditorDecision: null,
    reason: null,
    declineOption: null,
  }

  const [form] = Form.useForm()

  const [academicEditorDecision, setAcademicEditorDecision] = useState(
    INITIAL_VALUES.academicEditorDecision,
  )
  const handleChangeAcademicEditorDecision = ({ target: { value } }) => {
    form.resetFields(['reason', 'declineOption'])
    setAcademicEditorDecision(value)
  }

  const [reason, setReason] = useState(INITIAL_VALUES.reason)

  return (
    <ContextualBox
      highlight={highlight}
      label="Respond to Editorial Invitation"
      mt={4}
      startExpanded={startExpanded}
    >
      <Form form={form} initialValues={INITIAL_VALUES} layout="vertical">
        <Root>
          <FormItemRadioDecision
            label={`Do you agree to be the ${academicEditorLabel} for this
              manuscript?`}
            name="academicEditorDecision"
            required
            rules={[ValidationRule({ required: true, message: 'Required' })]}
          >
            <Radio.Group
              name="academicEditorDecision"
              onChange={handleChangeAcademicEditorDecision}
              value={academicEditorDecision}
            >
              {academicEditorInvitationOptions.map(({ value, label }) => (
                <StyledRadio key={value} value={value}>
                  {label}
                </StyledRadio>
              ))}
            </Radio.Group>
          </FormItemRadioDecision>

          {academicEditorDecision === 'no' && (
            <DeclineReason form={form} reason={reason} setReason={setReason} />
          )}

          <ConfirmationButton
            academicEditorDecision={academicEditorDecision}
            form={form}
            hasTriageEditorConflictOfInterest={
              hasTriageEditorConflictOfInterest
            }
            history={history}
            isGSW={publisher === 'GeoScienceWorld'}
            onSubmit={onSubmit}
            reason={reason && reason.trim()}
            teamMemberId={teamMember.id}
          />
        </Root>
      </Form>
    </ContextualBox>
  )
}

export { RespondToEditorialInvitation }

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  border-radius: ${th('borderRadius')};
  padding: calc(${th('gridUnit')} * 4);

  .ant-form-item-control-input {
    min-height: initial;
  }

  .ant-form-item-explain.ant-form-item-explain-error {
    position: absolute;
    bottom: -24px;
  }

  .ant-form-item-explain.ant-form-item-explain-success {
    display: none;
  }
`
const FormItemRadioDecision = styled(Form.Item)`
  margin-bottom: 20px;
`

const StyledRadio = styled(Radio)`
  align-items: center;
`
// #endregion
