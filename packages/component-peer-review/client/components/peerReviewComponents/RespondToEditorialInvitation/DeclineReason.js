import React, { useState } from 'react'
import styled from 'styled-components'
import { Input, Radio, Space, Form } from '@hindawi/phenom-ui'

import { ValidationRule } from '../RespondToEditorialInvitation'

function DeclineReason({ form, reason, setReason }) {
  const declineOptions = [
    'It’s not my field of expertise',
    'I am not available right now',
    'I have a conflict of interest',
    'My reason is not in the list above',
  ]

  const [declineOption, setDeclineOption] = useState(null)

  const handleSetReason = ({ target: { value } }) => setReason(value)

  const isLastOptionSelected = declineOption =>
    declineOption === declineOptions.length - 1

  const onOptionsChange = ({ target: { value } }) => {
    const newDeclineOption = Number(value)
    const option = isLastOptionSelected(newDeclineOption)
      ? null
      : declineOptions[value]

    form.resetFields(['reason'])
    setDeclineOption(newDeclineOption)
    setReason(option)
  }

  return (
    <>
      <FormItemInputReason
        label="Please specify the main reason for which you are declining the invitation"
        name="declineOption"
        onChange={onOptionsChange}
        required
        rules={[
          ValidationRule({ required: true, message: 'Please select a reason' }),
        ]}
      >
        <Radio.Group>
          <Space direction="vertical" size={6}>
            {declineOptions.map((value, index) => (
              <StyledRadio key={declineOptions[index]} value={index}>
                {value}
              </StyledRadio>
            ))}
          </Space>
        </Radio.Group>
      </FormItemInputReason>

      <FormItemReasonInput
        hidden={!isLastOptionSelected(declineOption)}
        label="Please describe your reason"
        name="reason"
        onChange={handleSetReason}
        required
        rules={[
          ValidationRule({
            required: isLastOptionSelected(declineOption),
            message: 'Please specify your reason',
          }),
          ValidationRule({
            whitespace: true,
            message: 'Reason cannot be empty',
          }),
        ]}
        value={reason}
      >
        <StyledInput maxLength={500} placeholder="Your reason" />
      </FormItemReasonInput>
    </>
  )
}

export { DeclineReason }

// #region styles
const FormItemInputReason = styled(Form.Item)`
  margin-bottom: 12px;
`

const FormItemReasonInput = styled(Form.Item)`
  margin-left: 24px;
`

const StyledInput = styled(Input)`
  width: 420px;
`

const StyledRadio = styled(Radio)`
  align-items: center;
`
// #endregion
