import React from 'react'
import { IconWarningSolid, Space, Text } from '@hindawi/phenom-ui'
import styled from 'styled-components'

export function ValidationRule({ message, ...rest }) {
  return {
    ...rest,
    message: <DisplayError message={message} />,
  }
}

function DisplayError({ message }) {
  return (
    <Error>
      <Space size={2}>
        <IconWarningSolid />
        <ErrorText>{message}</ErrorText>
      </Space>
    </Error>
  )
}

// #region styles
const ErrorText = styled(Text)`
  color: #ff5547;
`

const Error = styled.div`
  font-size: 12px;

  svg > path {
    fill: #ff5547;
  }
`
// #endregion
