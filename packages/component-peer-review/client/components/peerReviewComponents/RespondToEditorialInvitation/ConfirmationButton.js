import React, { useState } from 'react'
import { MailTemplate } from '@hindawi/phenom-communications-ui'
import styled from 'styled-components'
import {
  Row,
  Form,
  Button,
  message,
  ConfirmationModal,
} from '@hindawi/phenom-ui'

import useGetCommunicationRequest, {
  TemplateUsage,
} from '../../../graphql/hooks/useGetCommunicationRequest'

const respondToInvitationModalProps = {
  yes: {
    message: 'Agree',
    title: 'Please confirm your agreement',
    okText: 'Agree',
    cancelText: 'Cancel',
    className: 'agree-modal',
  },
  no: {
    message: 'Decline',
    title: 'Decline this invitation?',
    okText: 'Decline',
    cancelText: 'Cancel',
    className: 'decline-modal',
  },
}

function ConfirmationButton({
  form,
  isGSW,
  reason,
  history,
  onSubmit,
  teamMemberId,
  academicEditorDecision,
  hasTriageEditorConflictOfInterest,
}) {
  const [confirmationVisible, setConfirmationVisible] = useState(false)
  const [showSendEmailModal, setShowSendEmailModal] = useState(false)
  const [sendCommunicationLoading, setSendCommunicationLoading] = useState(
    false,
  )

  const usageMapping = {
    yes: TemplateUsage.ACCEPTED_ACADEMIC_INVITATION,
    no: TemplateUsage.DECLINED_ACADEMIC_INVITATION,
  }

  const {
    client,
    loading,
    getCommunicationPackage,
    communicationPackageData,
  } = useGetCommunicationRequest()

  const handleInvitationResponse = () => {
    isGSW ? setShowSendEmailModal(false) : setConfirmationVisible(false)

    if (academicEditorDecision === 'no') {
      history.replace('/')
    } else if (academicEditorDecision === 'yes') {
      void message.success({
        content: 'Respond to Editorial Invitation success',
        className: 'communication-notification',
      })
    }
    setSendCommunicationLoading(false)
  }

  return (
    <RowButton>
      <FormItemButton>
        <Button
          data-test-id="button-respond-to-editorial-invitation"
          htmlType="submit"
          loading={loading}
          onClick={() => {
            form
              .validateFields()
              .then(() => {
                getCommunicationPackage({
                  comments: reason,
                  teamMemberId,
                  usage: usageMapping[academicEditorDecision],
                })
                isGSW
                  ? setShowSendEmailModal(true)
                  : setConfirmationVisible(true)
              })
              .catch(error => error)
          }}
          type="primary"
          width={48}
        >
          Respond to Invitation
        </Button>
      </FormItemButton>

      {communicationPackageData ? (
        <MailTemplate
          client={client}
          mailMetadata={communicationPackageData}
          onBeforeSendMail={async () => {
            setSendCommunicationLoading(true)
            await onSubmit({
              academicEditorDecision,
              reason,
            })
            if (hasTriageEditorConflictOfInterest) {
              handleInvitationResponse()
            }
          }}
          onClose={() => {
            setShowSendEmailModal(false)
          }}
          onError={() => {
            void message.error({
              content: 'Please try sending the response again!',
              className: 'communication-notification',
            })
          }}
          onSendEmail={handleInvitationResponse}
          sendLoading={sendCommunicationLoading}
          shouldSendEmail={!hasTriageEditorConflictOfInterest}
          visible={showSendEmailModal}
        >
          {!isGSW &&
            (sendMail => (
              <ConfirmationModal
                {...respondToInvitationModalProps[academicEditorDecision]}
                confirmLoading={sendCommunicationLoading}
                onCancel={() => {
                  setConfirmationVisible(false)
                }}
                onOk={sendMail}
                visible={confirmationVisible}
              />
            ))}
        </MailTemplate>
      ) : null}
    </RowButton>
  )
}

export { ConfirmationButton }

// #region styles
const RowButton = styled(Row)`
  justify-content: flex-end;
  margin-top: 40px;
`

const FormItemButton = styled(Form.Item)`
  margin-bottom: 0;
`
// #endregion
