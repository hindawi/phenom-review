import React, { useState } from 'react'
import { Row } from '@hindawi/ui'
import { Button } from '@pubsweet/ui'
import { MailTemplate } from '@hindawi/phenom-communications-ui'
import {
  ConfirmationModal,
  message as messageController,
} from '@hindawi/phenom-ui'
import {
  rejectDecisionInfoToDTO,
  rejectDecisionInfoDefault,
} from './validateEditorialDecisionForm'

const getEditorDecisionsModalProps = academicEditorLabel => ({
  publish: {
    message: 'Publish',
    title: 'Publish Manuscript?',
    subtitle: 'A publish decision is final',
    confirmText: 'Publish manuscript',
  },
  returnToAcademicEditor: {
    message: 'Return Manuscript',
    title: 'Return Manuscript?',
    subtitle: `A returning manuscript to ${academicEditorLabel} decision is final`,
    confirmText: 'Return manuscript',
  },
  minor: {
    message: 'Minor Revision Requested',
    title: 'Request minor revision?',
    subtitle: null,
    confirmText: 'Request minor revision',
  },
  major: {
    message: 'Major Revision Requested',
    title: 'Request major revision?',
    subtitle: null,
    confirmText: 'Request major revision',
  },
  reject: {
    message: 'Reject',
    title: 'Reject manuscript?',
    subtitle: 'A rejection decision is final',
    confirmText: 'Reject manuscript',
  },
})

const decisionNotification = {
  reject: {
    content: 'Manuscript has been rejected.',
  },
  returnToAcademicEditor: {
    content: 'Manuscript has been returned to academic editor',
  },
  minor: {
    content: 'Manuscript has been sent to minor revision',
  },
  major: {
    content: 'Manuscript has been sent to major revision',
  },
  publish: {
    content: 'Manuscript has been sent for publishing',
  },
}

const SubmitDecisionButton = ({
  form,
  client,
  decision,
  loading,
  message,
  isPublish,
  addCommentsProp,
  academicEditorLabel,
  isGSW,
  onSubmit,
  toggle,
  communicationPackageData,
  setFormErrors,
}) => {
  const [showMailPreview, setShowMailPreview] = useState(false)
  const [showConfirmationModal, setShowConfirmationModal] = useState(false)
  const [sendCommunicationLoading, setSendCommunicationLoading] = useState(
    false,
  )
  const [rejectDecisionInfo, setRejectDecisionInfo] = useState(
    rejectDecisionInfoDefault,
  )

  const editorDecisionsModalProps = getEditorDecisionsModalProps(
    academicEditorLabel,
  )

  const onConfirm = ({ form, values }) =>
    onSubmit(values).then(() => {
      form.resetFields()
      toggle()
    })

  const handleSentEmail = decision => {
    if (!isGSW) {
      setShowConfirmationModal(false)
    } else {
      setShowMailPreview(false)
    }
    void messageController.success({
      content: decisionNotification[decision].content,
      className: 'communication-notification',
    })
  }

  const handleSubmitDecision = values => {
    const { decision, message } = values

    if (decision === 'reject') {
      const rejectDecisionInfo = rejectDecisionInfoToDTO(values)
      setRejectDecisionInfo(rejectDecisionInfo)
    }

    if (message) {
      addCommentsProp(values.message)
    }

    if (isGSW && !isPublish(decision)) {
      setShowMailPreview(true)
      return
    }

    setShowConfirmationModal(true)
  }

  return (
    <>
      {decision && (
        <Row justify="flex-end">
          <Button
            data-test-id="submit-triage-editor-decision"
            medium
            onClick={() =>
              form
                .validateFields()
                .then(values => handleSubmitDecision(values))
                .catch(({ errorFields }) => setFormErrors(errorFields))
            }
            pl={4}
            pr={4}
            primary
          >
            Submit Decision
          </Button>
        </Row>
      )}

      {isPublish(decision) && (
        <ConfirmDecisionModal
          {...editorDecisionsModalProps[decision]}
          loading={loading}
          onCancel={() => {
            setShowConfirmationModal(false)
          }}
          onConfirm={() =>
            onConfirm({
              form,
              values: {
                decision,
                message,
                rejectDecisionInfo,
              },
            })
          }
          visible={showConfirmationModal}
        />
      )}

      {communicationPackageData && (
        <MailTemplate
          client={client}
          mailMetadata={communicationPackageData || { mailInput: {} }}
          onBeforeSendMail={() => {
            setSendCommunicationLoading(true)
            onConfirm({
              form,
              values: {
                decision,
                message,
                rejectDecisionInfo,
              },
            })
          }}
          onClose={() => {
            setShowMailPreview(false)
          }}
          onError={() => {
            void messageController.error({
              content: 'Please try sending the decision again!',
              className: 'communication-notification',
            })
          }}
          onSendEmail={() => {
            handleSentEmail(decision)
            setSendCommunicationLoading(false)
          }}
          sendLoading={sendCommunicationLoading}
          visible={showMailPreview}
        >
          {!isGSW &&
            !isPublish(decision) &&
            (sendMail => (
              <ConfirmDecisionModal
                {...editorDecisionsModalProps[decision]}
                loading={sendCommunicationLoading}
                onCancel={() => {
                  setShowConfirmationModal(false)
                }}
                onConfirm={sendMail}
                visible={showConfirmationModal}
              />
            ))}
        </MailTemplate>
      )}
    </>
  )
}

export default SubmitDecisionButton

const ConfirmDecisionModal = ({
  loading,
  title,
  subtitle,
  confirmText,
  onConfirm,
  onCancel,
  visible,
}) => (
  <ConfirmationModal
    confirmLoading={loading}
    okText={confirmText}
    onCancel={onCancel}
    onOk={onConfirm}
    subtitle={subtitle}
    title={title}
    visible={visible}
  />
)
