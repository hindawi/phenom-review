import React, { useState } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Form, Alert } from '@hindawi/phenom-ui'
import { ContextualBox } from '@hindawi/ui'

import { useJournal } from 'component-journal-info'

import DecisionSelector from './DecisionSelector'
import MessageForAuthor from './MessageForAuthor'
import SubmitDecisionButton from './SubmitDecisionButton'

import useGetCommunicationRequest from '../../../graphql/hooks/useGetCommunicationRequest'
import DecisionToReject from './DecisionToReject/DecisionToReject'
import { INITIAL_FORM_VALUES } from './validateEditorialDecisionForm'

const isPublish = decision => decision === 'publish'

const EditorialDecision = ({
  match,
  toggle,
  loading,
  expanded,
  onSubmit,
  highlight,
  editorDecisions,
  academicEditorLabel,
  publisher = 'hindawi',
}) => {
  const isGSW = publisher === 'GeoScienceWorld'

  const {
    client,
    addCommentsProp,
    getCommunicationPackage,
    communicationPackageData,
  } = useGetCommunicationRequest()

  const {
    editorFinalDecisionInfoBox: {
      title: infoBoxTitle,
      description: infoBoxDescription,
    },
  } = useJournal()

  const { submissionId } = match.params

  const [decision, setDecision] = useState(null)
  const [message, setMessage] = useState(null)

  const [form] = Form.useForm()
  const [formErrors, setFormErrors] = useState(null)

  const showMessageForAuthor = [
    'minor',
    'major',
    'reject',
    'returnToAcademicEditor',
  ].includes(decision)

  return (
    <ContextualBox
      expanded={expanded}
      highlight={highlight}
      label="Your Editorial Decision"
      mt={4}
      toggle={toggle}
    >
      <Form form={form} initialValues={INITIAL_FORM_VALUES} layout="vertical">
        <Root>
          {!isGSW && (
            <StyledAlert
              description={
                <div
                  data-test-id="final-decision-guidelines-description"
                  dangerouslySetInnerHTML={{ __html: infoBoxDescription }}
                />
              }
              message={
                <div
                  data-test-id="final-decision-guidelines-title"
                  dangerouslySetInnerHTML={{ __html: infoBoxTitle }}
                />
              }
              type="info"
            />
          )}

          <DecisionSelector
            editorDecisions={editorDecisions}
            form={form}
            getCommunicationPackage={getCommunicationPackage}
            isPublish={isPublish}
            setDecision={setDecision}
            submissionId={submissionId}
          />

          {showMessageForAuthor && (
            <MessageForAuthor
              academicEditorLabel={academicEditorLabel}
              decision={decision}
              setMessage={setMessage}
            />
          )}

          {decision === 'reject' && <DecisionToReject form={form} />}

          <SubmitDecisionButton
            academicEditorLabel={academicEditorLabel}
            addCommentsProp={addCommentsProp}
            client={client}
            communicationPackageData={communicationPackageData}
            decision={decision}
            form={form}
            formErrors={formErrors}
            isGSW={isGSW}
            isPublish={isPublish}
            loading={loading}
            message={message}
            onSubmit={onSubmit}
            setFormErrors={setFormErrors}
            toggle={toggle}
          />
        </Root>
      </Form>
    </ContextualBox>
  )
}

export default EditorialDecision

const StyledAlert = styled(Alert)`
  margin-bottom: 24px;
  ul {
    padding-left: 30px;
  }

  p {
    margin-bottom: 5px;
  }

  p:last-child {
    margin-bottom: 0;
  }
`

const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  padding: calc(${th('gridUnit')} * 4);

  .ant-form-item-label > label.ant-form-item-required:before {
    content: unset;
  }

  .ant-form-item-label > label.ant-form-item-required:after {
    display: inline-block;
    margin-right: 4px;
    color: #ff4d4f;
    font-size: 14px;
    line-height: 1;
    content: '*';
  }

  .ant-form-item-explain {
    min-height: 16px;
  }
`
