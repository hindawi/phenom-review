import React, { useState } from 'react'
import styled from 'styled-components'
import { Col, Form, Input } from '@hindawi/phenom-ui'
import { ErrorMessage, InfoMessage, hasError } from '../Errors'

const label = 'Transfer suggestions'
const fieldName = ['transferToAnotherJournal', 'transferSuggestions']
const placeholder =
  'Please suggest journals or subject areas you would recommend transferring this manuscript to'
const errorMessage =
  'Please write your recommendations. Not visible to authors and reviewers'

function TransferSuggestionsTextBox({ form }) {
  const [transferSuggestions, setTransferSuggestions] = useState(null)

  const onChange = e => {
    const { value } = e.target
    setTransferSuggestions(value.trim())
  }

  return (
    <Col span={24}>
      <FormItem
        help={showErrorMessage({
          form,
          fieldName,
          transferSuggestions,
          errorMessage,
        })}
        label={label}
        name={fieldName}
        rules={[{ required: true, whitespace: true }]}
      >
        <Input.TextArea
          maxLength={250}
          onChange={onChange}
          placeholder={placeholder}
          showCount
        />
      </FormItem>
    </Col>
  )
}

function showErrorMessage({
  form,
  fieldName,
  transferSuggestions,
  errorMessage,
}) {
  return (transferSuggestions === null || transferSuggestions) &&
    !hasError({ form, fieldName }) ? (
    <InfoMessage />
  ) : (
    <ErrorMessage message={errorMessage} />
  )
}

export default TransferSuggestionsTextBox

const FormItem = styled(Form.Item)`
  .ant-form-item-label {
    padding-bottom: 0;
  }
`
