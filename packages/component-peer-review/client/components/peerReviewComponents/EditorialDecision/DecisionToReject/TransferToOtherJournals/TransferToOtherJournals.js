import React, { useState } from 'react'
import { Col, Row, Text } from '@hindawi/phenom-ui'
import { Title } from '../Errors'
import RecommendationRadioButtons from './RecommendationRadioButtons'
import TransferSuggestionsTextBox from './TransferSuggestionsTextBox'

function TransferToOtherJournals({ form }) {
  const [recommendation, setRecommendation] = useState(null)

  return (
    <Row gutter={[0, 24]}>
      <TransferToOtherJournalsInfo />

      <Row gutter={[0, 20]} style={{ width: '100%' }}>
        <RecommendationRadioButtons
          form={form}
          recommendation={recommendation}
          setRecommendation={setRecommendation}
        />
        {recommendation === 'YES' && <TransferSuggestionsTextBox form={form} />}
      </Row>
    </Row>
  )
}

function TransferToOtherJournalsInfo() {
  return (
    <Col span={24}>
      <Row gutter={[0, 4]}>
        <Col span={24}>
          <Title>Transfer To Other Journals</Title>
        </Col>
        <Col span={24}>
          <Text>
            Other journals might be a better home for this manuscript.
          </Text>
        </Col>
      </Row>
    </Col>
  )
}

export default TransferToOtherJournals
