import styled from 'styled-components'
import { Col, Text, IconLock, IconWarning } from '@hindawi/phenom-ui'
import React from 'react'

const ErrorMessageStyle = styled(Text)`
  font-size: 12px;
  color: ${props => props.color};
`

const Icon = ({ icon, color }) => {
  const style = {
    color,
    fontSize: '11px',
    marginRight: '3px',
  }
  if (icon === 'warning') return <IconWarning style={style} />

  return <IconLock style={style} />
}

export const ErrorMessage = ({ message, color = '#e35f43', icon }) => (
  <Col span={24} style={{ lineHeight: '16px' }}>
    <Icon color={color} icon={icon} />
    <ErrorMessageStyle color={color}>{message}</ErrorMessageStyle>
  </Col>
)

export const InfoMessage = () => (
  <ErrorMessage
    color="#56A03D"
    message="Not visible to authors and reviewers"
  />
)

export const hasError = ({ form, fieldName }) =>
  form.getFieldError(fieldName).length

export const Title = styled(Text)`
  font-size: 16px;
  font-weight: 700;
  line-height: 21.82px;
`
