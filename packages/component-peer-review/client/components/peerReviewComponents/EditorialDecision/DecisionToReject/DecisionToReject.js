import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import ReasonForRejection from './ReasonForRejection/ReasonForRejection'
import TransferToOtherJournals from './TransferToOtherJournals/TransferToOtherJournals'

const DecisionToReject = ({ form }) => (
  <Root>
    <VerticalSeparator />
    <ReasonForRejection form={form} />
    <VerticalSeparator />
    <TransferToOtherJournals form={form} />
  </Root>
)

export default DecisionToReject

const Root = styled.div`
  margin-bottom: 8px;
`

const VerticalSeparator = styled.div`
  background-color: ${th('colorFurniture')};
  height: 1px;
  margin: 24px 0;
}
`
