import React, { useState } from 'react'
import { Text, Row, Col } from '@hindawi/phenom-ui'
import { Title } from '../Errors'
import ReasonCheckbox from './ReasonCheckbox'
import OtherReasonTextArea from './OtherReasonTextArea'

const ReasonForRejection = ({ form }) => {
  const [otherOptionIsSelected, setOtherOptionIsSelected] = useState(false)

  return (
    <Row gutter={[0, 24]}>
      <ReasonInfo />

      <Row gutter={[0, 18]} style={{ width: '100%' }}>
        <ReasonCheckbox
          form={form}
          setOtherOptionIsSelected={setOtherOptionIsSelected}
        />
        {otherOptionIsSelected && <OtherReasonTextArea form={form} />}
      </Row>
    </Row>
  )
}

const ReasonInfo = () => (
  <Row gutter={[0, 4]}>
    <Col span={24}>
      <Title>Reasons For Rejection</Title>
    </Col>
    <Col span={24}>
      <Text>
        This information allows us to improve the publishing experience for
        authors and editors. Thank you for taking the time to answer.
      </Text>
    </Col>
  </Row>
)

export default ReasonForRejection
