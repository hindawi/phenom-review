import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { useQuery, useLazyQuery } from 'react-apollo'
import styled from 'styled-components'

import { Checkbox, Col, Form, Row, Skeleton } from '@hindawi/phenom-ui'

import { useJournal } from 'component-journal-info'

import { ErrorMessage, InfoMessage, hasError } from '../Errors'
import {
  getSubmissionJournalId as getSubmissionJournalIdQuery,
  getReasonsForRejection as getReasonsForRejectionQuery,
} from '../../../../../graphql/queries'

const fieldName = ['rejectDecisionInfo', 'selectedReasons']

const errorMessage =
  'Please specify your reason(s) for rejecting. Not visible to authors and reviewers'

const label = 'Please specify your reason(s) for rejecting this manuscript:'

function ReasonCheckbox({ form, setOtherOptionIsSelected }) {
  const [reasonsSelected, setReasonsSelected] = useState(null)
  const { submissionId } = useParams()

  const {
    data: getSubmissionJournalIdQueryData,
    loading: getSubmissionJournalIdQueryLoading,
  } = useQuery(getSubmissionJournalIdQuery, {
    variables: { submissionId },
  })

  const [
    getReasonsForRejection,
    { data: rejectOptionsQueryData },
  ] = useLazyQuery(getReasonsForRejectionQuery)

  useEffect(() => {
    if (!getSubmissionJournalIdQueryLoading) {
      const { journalId } = getSubmissionJournalIdQueryData.getSubmission[0]

      getReasonsForRejection({ variables: { journalId } })
    }
  }, [
    getSubmissionJournalIdQueryData,
    getSubmissionJournalIdQueryLoading,
    getReasonsForRejection,
  ])

  const onChange = values => {
    setReasonsSelected(values)

    const otherReasonIsSelected = values.some(value => value === 'otherReasons')

    if (otherReasonIsSelected) {
      setOtherOptionIsSelected(true)
    } else {
      setOtherOptionIsSelected(false)
      form.resetFields([['rejectDecisionInfo', 'otherReasons']])
    }
  }

  const {
    rejectDecisionReasonsTranslations: reasonOptionsTranslations,
  } = useJournal()

  if (rejectOptionsQueryData) {
    const { getReasonsForRejection: rejectionReasons } = rejectOptionsQueryData

    return (
      <Col span={24}>
        <FormItem
          hasFeedback={false}
          help={showErrorMessage({
            form,
            fieldName,
            reasonsSelected,
            errorMessage,
          })}
          label={label}
          name={fieldName}
          rules={[{ required: true }]}
        >
          <Checkbox.Group onChange={onChange} style={{ width: '100%' }}>
            <Row>
              {rejectionReasons.map(key => (
                <Col key={key} span={24}>
                  <Checkbox value={key}>
                    {reasonOptionsTranslations[key]}
                  </Checkbox>
                </Col>
              ))}
            </Row>
          </Checkbox.Group>
        </FormItem>
      </Col>
    )
  }

  return (
    <Skeleton
      active
      paragraph={{ rows: 5, width: [200, 200, 200, 200, 200] }}
      title={false}
    />
  )
}

export default ReasonCheckbox

function showErrorMessage({ form, fieldName, reasonsSelected, errorMessage }) {
  return (reasonsSelected === null || reasonsSelected.length) &&
    !hasError({ form, fieldName }) ? (
    <InfoMessage />
  ) : (
    <ErrorMessage message={errorMessage} />
  )
}

const FormItem = styled(Form.Item)`
  .ant-form-item-label {
    padding-bottom: 0;
  }
`
