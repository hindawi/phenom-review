import React, { useState } from 'react'
import { Col, Form, Input, Row } from '@hindawi/phenom-ui'
import { ErrorMessage, InfoMessage, hasError } from '../Errors'

const fieldName = ['rejectDecisionInfo', 'otherReasons']
const errorMessage =
  'Please specify the other reason(s) for rejecting. Not visible to authors and reviewers'
const label = 'Other reason(s)'
const placeholder = 'Please add any other reasons'

function OtherReasonTextArea({ form }) {
  const [otherReasons, setOtherReasons] = useState(null)

  const onChange = e => {
    const { value } = e.target
    setOtherReasons(value.trim())
  }

  return (
    <Row gutter={[0, 4]} style={{ width: '100%' }}>
      <Col span={24}>
        <Form.Item
          help={showErrorMessage({
            form,
            fieldName,
            otherReasons,
            errorMessage,
          })}
          label={label}
          name={fieldName}
          rules={[{ required: true, whitespace: true }]}
        >
          <Input.TextArea
            maxLength={250}
            onChange={onChange}
            placeholder={placeholder}
            showCount
          />
        </Form.Item>
      </Col>
    </Row>
  )
}

function showErrorMessage({ form, fieldName, otherReasons, errorMessage }) {
  return (otherReasons === null || otherReasons) &&
    !hasError({ form, fieldName }) ? (
    <InfoMessage />
  ) : (
    <ErrorMessage message={errorMessage} />
  )
}

export default OtherReasonTextArea
