TriageEditor Decision box

```js
const mockOptions = [
  {
    label: 'Request revision',
    value: 'revision',
  },
  {
    label: 'Reject',
    value: 'reject',
  },
]
;<EditorialDecision
  startExpanded
  isVisible={true}
  options={mockOptions}
  onSubmit={({ decision, message }) => console.log(decision, message)}
/>
```
