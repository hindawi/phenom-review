The preview email modal
This modal is used to render MailTemplate component ('@hindawi/phenom-mail-template')

```js
<PreviewEmailModal
  handleMailSubmission={() => {
    console.log('click SEND EMAIL button')
  }}
  handleOnClose={() => {
    console.log('click close button')
  }}
  handleOnRevert={() => {
    console.log('click REVERT CHANGES button')
  }}
  visible={true}
>
  <MailTemplate
    html="html"
    onChange={(bodies, document) => {
      console.log('handle content changes')
    }}
  />
</PreviewEmailModal>
```
