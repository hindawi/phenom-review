import React from 'react'
import { Button } from '@pubsweet/ui'
import { MultiAction } from '@hindawi/ui'
import { Modal } from 'component-modal'

const AssignMyselfButton = ({
  currentUser,
  academicEditorLabel,
  assignAcademicEditor,
}) => {
  const onInvite = modalProps =>
    assignAcademicEditor(currentUser.id, modalProps)
  return (
    <Modal
      cancelText="CANCEL"
      component={MultiAction}
      confirmText="ASSIGN"
      modalKey="manuscript-assign-oneself-academic-editor"
      onConfirm={onInvite}
      subtitle="This change is irreversible. You will not be able to take actions as with the current role."
      title={`Are you sure you want to assign yourself as ${academicEditorLabel}?`}
    >
      {showModal => (
        <Button medium onClick={showModal} primary>
          Assign myself as Guest Editor
        </Button>
      )}
    </Modal>
  )
}

export default AssignMyselfButton
