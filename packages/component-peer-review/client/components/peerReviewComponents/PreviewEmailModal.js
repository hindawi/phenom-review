import React from 'react'
import { Modal, Button, Row, Card, Col } from '@hindawi/phenom-ui'
import '@hindawi/phenom-mail-template/phenom-mail-template.umd.css'
import styled from 'styled-components'
import { NewTheme } from '@hindawi/ui'

const { LoadingModal } = NewTheme

export const StyledModal = styled(Modal)`
  max-width: 1280px;
  top: 24px;
  height: calc(100vh - 48px);
  padding-bottom: 0;
  .ant-modal-header {
    flex-grow: 0;
  }
  .ant-modal-footer {
    flex-grow: 1;
    display: flex;
    justify-content: flex-end;
    flex-direction: column;
  }
  .ant-modal-content {
    height: 100%;
    display: flex;
    flex-direction: column;
  }
  .mail-template-preview {
    max-width: 600px;
  }
`
export const StyledCard = styled(Card)`
  max-width: 100%;
  height: 100%;
  overflow-y: scroll;
`

export const StyledCol = styled(Col)`
  display: flex;
  justify-content: center;
`
export const StyledRow = styled(Row)`
  display: flex;
  justify-content: flex-end;
  justify-content: space-between;
`

export const ModalFooter = ({ loading, onRevert, onSendEmail, onClose }) => (
  <StyledRow>
    <Button onClick={onRevert} type="secondary">
      REVERT CHANGES
    </Button>
    <div justify="space-between">
      <Button onClick={onClose} type="ghost">
        CANCEL
      </Button>
      <Button loading={loading} onClick={onSendEmail} type="primary">
        SEND EMAIL
      </Button>
    </div>
  </StyledRow>
)

export const PreviewEmailModal = ({
  handleOnRevert,
  handleMailSubmission,
  handleOnClose,
  title = 'Email editor',
  visible,
  successLoading,
  modalLoading,
  children,
}) => {
  if (modalLoading) {
    return <LoadingModal />
  }

  return (
    <StyledModal
      bodyStyle={{ 'overflow-y': 'auto', flexGrow: 0 }}
      footer={
        <ModalFooter
          loading={successLoading}
          onClose={handleOnClose}
          onRevert={handleOnRevert}
          onSendEmail={handleMailSubmission}
        />
      }
      maskStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.8)' }}
      onCancel={handleOnClose}
      title={title}
      visible={visible}
      width="80%"
    >
      <StyledCard
        bodyStyle={{ paddingInlineEnd: '0', paddingInlineStart: '0' }}
      >
        <Row>
          <StyledCol span={24}>{children}</StyledCol>
        </Row>
      </StyledCard>
    </StyledModal>
  )
}

export default PreviewEmailModal
