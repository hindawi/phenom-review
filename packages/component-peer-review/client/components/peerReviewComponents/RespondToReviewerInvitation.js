import React from 'react'
import { Formik } from 'formik'
import { Button } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withModal } from 'component-modal'
import { compose, withHandlers } from 'recompose'

import {
  Item,
  Label,
  validators,
  RadioGroup,
  MultiAction,
  ContextualBox,
  ValidatedFormField,
} from '@hindawi/ui'

const options = [
  { label: 'Agree', value: 'yes' },
  { label: 'Decline', value: 'no' },
]
const RespondToReviewerInvitation = ({
  onSubmit,
  highlight,
  startExpanded,
}) => (
  <ContextualBox
    highlight={highlight}
    label="Respond to Invitation to Review"
    mt={4}
    startExpanded={startExpanded}
  >
    <Formik onSubmit={onSubmit}>
      {({ handleSubmit, values }) => (
        <Root>
          <Item vertical>
            <Label mb={2} required>
              Do you agree to review this manuscript?
            </Label>
            <ValidatedFormField
              component={RadioGroup}
              name="reviewerDecision"
              options={options}
              validate={[validators.required]}
            />
          </Item>

          <Item justify="flex-end">
            <Button
              data-test-id="reviewer-response-submit"
              onClick={handleSubmit}
              primary
              width={48}
            >
              Respond to Invitation
            </Button>
          </Item>
        </Root>
      )}
    </Formik>
  </ContextualBox>
)

export default compose(
  withModal({
    modalKey: 'reviewer-respond',
    component: MultiAction,
  }),
  withHandlers({
    onSubmit: ({ showModal, onSubmit }) => values => {
      const title =
        values.reviewerDecision === 'yes'
          ? 'Please confirm your agreement.'
          : 'Decline this invitation?'
      const confirmText =
        values.reviewerDecision === 'yes' ? 'AGREE' : 'DECLINE'

      showModal({
        title,
        onConfirm: modalProps => onSubmit(values, modalProps),
        confirmText,
      })
    },
  }),
)(RespondToReviewerInvitation)

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  border-radius: ${th('borderRadius')};
  padding: calc(${th('gridUnit')} * 4);
`
// #endregion
