/* eslint-disable sonarjs/cognitive-complexity */
import React, { Fragment, useState, useEffect } from 'react'
import { get } from 'lodash'
import { space } from 'styled-system'
import styled from 'styled-components'
import { Button, TextField } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { ConfirmationModal, IconWarning, IconAddUser } from '@hindawi/phenom-ui'
import { Popover, Tooltip } from 'antd'
import {
  parseAffiliations,
  Row,
  Item,
  Text,
  Icon,
  Label,
  roles,
  Loader,
  ContextualBox,
  ActionLink,
  OrcIDLogo,
} from '@hindawi/ui'
import { MailTemplate } from '@hindawi/phenom-mail-template'

import { PreviewEmailModal } from './PreviewEmailModal'
import AssignAcademicEditorModal from './AssignAcademicEditorModal'
import AssignMyselfButton from './AssignMyselfButton'
import {
  useAcademicEditors,
  useAcademicEditorsKeywords,
} from '../../graphql/hooks'
import useGetPreviewEmailRequest from '../../graphql/hooks/useGetPreviewEmailRequest'

const transformGQLError = gqlError =>
  gqlError?.message?.replace('GraphQL error: ', '')

const config = require('config')

const orcidURL = config.orcid.orcidBaseUrl

const getName = author =>
  `${get(author, 'alias.name.givenNames', '')} ${get(
    author,
    'alias.name.surname',
    '',
  )}`

const getOrcID = academicEditor => {
  const orcidIdentity = academicEditor.user?.identities?.find(
    identity => identity.identifier,
  )
  return orcidIdentity?.identifier
}

const errorMessage = `Please try sending the invitation again. If the issue persists, contact the Customer Support Team on help@hindawi.com`

function getConflictsOfInterestContent(
  reviewer,
  authors,
  affiliations,
  showTitle = true,
  extraText = null,
  style = {},
) {
  const reviewerAuthors = parseAffiliations([reviewer])

  return (
    <AlignLeft style={style}>
      {showTitle && (
        <h3 className="ant-typography small">
          Potential conflict of interest identified!
        </h3>
      )}
      <p>
        <b>
          {reviewer.name}
          <Superscript>
            {reviewerAuthors.authors[0].affiliationNumber}
          </Superscript>
        </b>
        <span style={{ marginLeft: '4px' }}>
          has the same affiliation as the following authors:
        </span>
      </p>
      <ul style={{ marginLeft: '-18px' }}>
        {authors.map(author => (
          <li>
            {getName(author)}
            {author.affiliationNumber && (
              <Superscript>{author.affiliationNumber}</Superscript>
            )}
          </li>
        ))}
      </ul>
      <AffiliationColumn mt={1}>
        {affiliations.map((aff, i) => (
          <Item data-test-id={`affiliation-${i + 1}`} flex={1} key={aff}>
            <Superscript>{i + 1}</Superscript>
            &nbsp;
            <Text color={th('grey70')} style={{ fontSize: '96%' }}>
              {aff}
            </Text>
          </Item>
        ))}
      </AffiliationColumn>
      {extraText && <p style={{ marginTop: '20px' }}>{extraText}</p>}
    </AlignLeft>
  )
}

const AssignAcademicEditor = ({
  toggle,
  expanded,
  manuscript,
  currentUser,
  academicEditorLabel,
  inviteAcademicEditor,
  assignAcademicEditor,
  role,
}) => {
  const isSpecialIssue = !!manuscript.specialIssue

  const shouldDisplayKeywords = process.env.EDITOR_EXPERTISE

  const [searchValue, setSearchValue] = useState('')
  const [inputValue, setInputValue] = useState(searchValue)
  const [targetUser, setTargetUser] = useState()
  const [confirmationVisible, setConfirmationVisible] = useState(false)
  const [sendEmailLoading, setSendEmailLoading] = useState(false)
  const [showJournalEditorialBoard, setShowJournalEditorialBoard] = useState(
    !isSpecialIssue,
  )
  const [
    showEditorialBoardSwitchDisclaimer,
    setShowEditorialBoardSwitchDisclaimer,
  ] = useState(false)

  useEffect(() => {
    if (targetUser && !targetUser.conflictsOfInterest) {
      handleRequestEmailPreview({
        errorMessage,
        input: {
          submissionId: manuscript.submissionId,
          usecase: 'InvitationToHandleAManuscript(AE)',
          targetUser: { id: targetUser.user.id },
          requestedJournalEditorialBoard: showJournalEditorialBoard,
        },
      })()
      setConfirmationVisible(false)
    }
  }, [targetUser])

  const {
    emailContentChanges,
    showMailPreview,
    currentHtml,
    handleSentEmail,
    handleError,
    handleRequestEmailPreview,
    handleOnChange,
    handleOnClose,
    handleOnRevert,
    loading: previewEmailLoading,
  } = useGetPreviewEmailRequest()

  const showEditorialBoardSwitch =
    isSpecialIssue && ['admin', 'editorialAssistant'].includes(role)

  const clearSearch = () => {
    setSearchValue('')
    setInputValue('')
  }

  const handleMailSubmission = () => {
    setSendEmailLoading(true)
    onInvite(targetUser)
      .then(() => {
        handleSentEmail('The Academic Editor was invited successfully')
      })
      .catch(e => {
        handleError(transformGQLError(e))
      })
    setSendEmailLoading(false)
  }

  const [academicEditors, loading] = useAcademicEditors({
    manuscriptId: manuscript.id,
    searchValue,
    requestedJournalEditorialBoard: showJournalEditorialBoard,
  })

  const [academicEditorsKeywords, keywordsLoading] = useAcademicEditorsKeywords(
    {
      submissionId: manuscript.submissionId,
      academicEditors,
      shouldDisplayKeywords,
    },
  )

  const onInvite = async ({ user: { id: userId } }) => {
    try {
      await inviteAcademicEditor(userId, emailContentChanges)
    } catch (error) {
      throw new Error(error.message)
    }
  }

  const filteredAcademicEditors = academicEditors.map(academicEditor => {
    const givenNames = get(academicEditor, 'alias.name.givenNames', '')
    const surname = get(academicEditor, 'alias.name.surname', '')
    const email = get(academicEditor, 'alias.email', '')
    const terms = academicEditorsKeywords.find(editor => editor.email === email)
    return {
      ...academicEditor,
      email,
      name: `${givenNames} ${surname}`,
      searchIndex: `${givenNames} ${surname} ${email}`,
      terms: get(terms, 'terms', []),
    }
  })

  function DisplayEditorKeywords({ keywords }) {
    if (keywordsLoading) {
      return (
        <Item>
          <Loader iconSize={3} mb={2} ml={4} />
        </Item>
      )
    }

    if (!keywordsLoading && keywords.length === 0) {
      return (
        <Item flex={4} mr={6}>
          <Text>No keywords found for this editor</Text>
        </Item>
      )
    }

    const keywordsList = keywords.reduce(
      (result, value, index) =>
        index === 0 ? result + value : `${result}, ${value}`,
      '',
    )

    return (
      <Item display="inline-grid" flex={4} mr={6}>
        <Text ellipsis title={keywordsList}>
          {keywordsList}
        </Text>
      </Item>
    )
  }

  const academicEditorStatus = get(manuscript, 'academicEditor.status', '')
  const pendingAcademicEditor = get(manuscript, 'pendingAcademicEditor', '')
  const assignedEditorId = get(manuscript, 'academicEditor.user.id', '')
  const pendingEditorId = get(manuscript, 'pendingAcademicEditor.user.id', '')

  const canReassign =
    academicEditorStatus && academicEditorStatus === 'accepted'
  const canAssignOneself =
    manuscript.specialIssue &&
    manuscript.role === roles.TRIAGE_EDITOR &&
    !manuscript.hasSpecialIssueEditorialConflictOfInterest

  const pendingApprovalLabel = () =>
    pendingAcademicEditor && (
      <Row alignItems="baseline" fitContent justify="flex-end">
        <Text fontWeight={700} invited mr={6} small upperCase>
          Pending Response
        </Text>
      </Row>
    )

  return (
    <ContextualBox
      data-test-id="assign-academic-editor"
      expanded={expanded}
      height={8}
      highlight={!canReassign}
      label={
        canReassign
          ? `Reassign ${academicEditorLabel}`
          : `Assign ${academicEditorLabel}`
      }
      mt={4}
      rightChildren={pendingApprovalLabel}
      toggle={toggle}
    >
      <Root>
        <Row justify="space-between" p={2} pb={4}>
          <TextContainer>
            <TextField
              data-test-id="manuscript-assign-academic-editor-filter"
              inline
              onChange={e => setInputValue(e.target.value)}
              onKeyPress={e =>
                e.key === 'Enter' &&
                setSearchValue(e.target.value.toLowerCase())
              }
              placeholder="Search by name or email"
              value={inputValue}
            />
            {searchValue !== '' && (
              <StyledIcon
                data-test-id="clear-filter"
                fontSize="16px"
                icon="remove"
                onClick={clearSearch}
              />
            )}
          </TextContainer>
          {canAssignOneself && (
            <AssignMyselfButton
              academicEditorLabel={academicEditorLabel}
              assignAcademicEditor={assignAcademicEditor}
              currentUser={currentUser}
            />
          )}
        </Row>
        {showEditorialBoardSwitch && (
          <>
            <Row justify="flex-start" p={2}>
              <Text>
                {showJournalEditorialBoard && (
                  <IconWarning className="warning-icon" />
                )}
                {showJournalEditorialBoard
                  ? 'You are inviting an editor from Journal Editorial Board'
                  : 'Invite an editor from the Journal Editorial Board as Guest Editor'}
              </Text>
            </Row>
            <Row justify="flex-start" p={2}>
              <Item maxWidth={showJournalEditorialBoard ? 30 : 60}>
                {showJournalEditorialBoard ? (
                  <Button
                    ghost
                    onClick={() => setShowJournalEditorialBoard(false)}
                  >
                    Switch back
                  </Button>
                ) : (
                  <Button
                    ghost
                    onClick={() => setShowEditorialBoardSwitchDisclaimer(true)}
                  >
                    Switch to Journal Editorial Board
                  </Button>
                )}
              </Item>
            </Row>
          </>
        )}
        <Row justify="flex-start" p={2}>
          <Text fontWeight={700}>
            {showJournalEditorialBoard ? 'Journal' : 'Special Issue'} Editorial
            Board
          </Text>
        </Row>
        {loading && (
          <Row>
            <Loader mb={2} />
          </Row>
        )}
        {!loading && filteredAcademicEditors.length === 0 && (
          <Row alignItems="baseline" justify="flex-start" pl={2}>
            <Icon color="colorError" icon="warning" pr={1} />
            <Text medium secondary>
              No {academicEditorLabel}s have been found.
            </Text>
          </Row>
        )}
        {!loading && filteredAcademicEditors.length > 0 && (
          <Fragment>
            <Row height={6} pl={4}>
              <Item flex={2}>
                <Label>Full name</Label>
              </Item>
              <Item flex={1}>
                <Label />
              </Item>
              <Item flex={2}>
                <Label>Email</Label>
              </Item>
              {shouldDisplayKeywords && (
                <Item flex={4} mr={6}>
                  <Label>Expertise</Label>
                </Item>
              )}
              <Item flex={1} justify="flex-end" maxWidth={16}>
                <Label>Workload</Label>
              </Item>
              <Item flex={1} minWidth={40} />
            </Row>

            {filteredAcademicEditors.map((academicEditor, index) => {
              const orcID = getOrcID(academicEditor)
              return (
                <CustomRow
                  data-test-id={`manuscript-assign-academic-editor-invite-${academicEditor.id}`}
                  height={8}
                  isFirst={index === 0}
                  isLast={index === filteredAcademicEditors.length - 1}
                  key={academicEditor.id}
                  pl={4}
                >
                  <Fragment>
                    <Item flex={2} style={{ display: 'flex' }}>
                      <Text
                        ellipsis
                        style={{ paddingRight: '30px' }}
                        title={academicEditor.name}
                      >
                        {academicEditor.name}
                        {academicEditor.conflictsOfInterest &&
                          (() => {
                            const { authors, affiliations } = parseAffiliations(
                              academicEditor.conflictsOfInterest,
                            )

                            const content = getConflictsOfInterestContent(
                              academicEditor,
                              authors,
                              affiliations,
                              true,
                            )

                            return (
                              <Popover content={content} placement="topLeft">
                                <IconWarning
                                  className="warning-icon"
                                  role="button"
                                  style={{
                                    position: 'relative',
                                    left: '5px',
                                    top: '1px',
                                  }}
                                />
                              </Popover>
                            )
                          })()}
                      </Text>
                    </Item>
                    <Item flex={1}>
                      {orcID && (
                        <Tooltip
                          overlayStyle={{ borderRadius: '3px' }}
                          title="View ORCID record"
                        >
                          <ActionLink to={`${orcidURL}/${orcID}`}>
                            <OrcIDLogo />
                          </ActionLink>
                        </Tooltip>
                      )}
                    </Item>
                    <Item flex={2}>
                      <Text>{academicEditor.email}</Text>
                    </Item>
                    {shouldDisplayKeywords && (
                      <DisplayEditorKeywords keywords={academicEditor.terms} />
                    )}
                    <Item flex={1} justify="flex-end" maxWidth={16}>
                      <Text
                        color={
                          academicEditor.workload < 4
                            ? th('actionPrimaryColor')
                            : th('warningColor')
                        }
                        fontWeight={700}
                      >
                        {academicEditor.workload}
                      </Text>
                    </Item>

                    <Item flex={1} justify="flex-end" minWidth={40}>
                      {assignedEditorId === academicEditor.user.id && (
                        <CustomAssignedButton disabled mr={6} xs>
                          Assigned
                        </CustomAssignedButton>
                      )}
                      {pendingEditorId === academicEditor.user.id && (
                        <Text fontWeight={700} invited mr={6} small upperCase>
                          Pending Response
                        </Text>
                      )}
                      {assignedEditorId !== academicEditor.user.id &&
                        pendingEditorId !== academicEditor.user.id && (
                          <CustomButton
                            button
                            mr={6}
                            onClick={() => {
                              setTargetUser(academicEditor)
                              setConfirmationVisible(true)
                            }}
                          >
                            <IconAddUser /> &nbsp; Invite
                          </CustomButton>
                        )}
                    </Item>
                  </Fragment>
                </CustomRow>
              )
            })}
          </Fragment>
        )}
      </Root>
      <ConfirmationModal
        cancelText="CANCEL"
        icon="alert"
        okText="SWITCH TO JOURNAL EDITORIAL BOARD"
        onCancel={() => {
          setShowEditorialBoardSwitchDisclaimer(false)
        }}
        onOk={() => {
          setShowEditorialBoardSwitchDisclaimer(false)
          setShowJournalEditorialBoard(true)
        }}
        subtitle="Are you sure you want to switch to the Journal Editorial Board to invite an editor?"
        title="Special Issues should only be handled by the Special Issue Editorial Board."
        visible={showEditorialBoardSwitchDisclaimer}
      />
      {targetUser && targetUser.conflictsOfInterest && (
        <AssignAcademicEditorModal
          getModalContent={getConflictsOfInterestContent}
          onCancel={() => {
            setConfirmationVisible(false)
          }}
          onOk={() => {
            handleRequestEmailPreview({
              errorMessage,
              input: {
                submissionId: manuscript.submissionId,
                usecase: 'InvitationToHandleAManuscript(AE)',
                targetUser: { id: targetUser.user.id },
                requestedJournalEditorialBoard: showJournalEditorialBoard,
              },
            })()
            setConfirmationVisible(false)
          }}
          targetUser={targetUser}
          visible={confirmationVisible}
        />
      )}
      <PreviewEmailModal
        handleMailSubmission={handleMailSubmission}
        handleOnClose={handleOnClose}
        handleOnRevert={handleOnRevert}
        modalLoading={previewEmailLoading}
        successLoading={sendEmailLoading}
        title="Invite Academic Editor"
        visible={showMailPreview}
      >
        <MailTemplate html={currentHtml} onChange={handleOnChange} />
      </PreviewEmailModal>
    </ContextualBox>
  )
}

export default AssignAcademicEditor

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  border-bottom-left-radius: 3px;
  border-bottom-right-radius: 3px;

  ${space};

  .show-journal-editorial-board {
    position: relative;
  }

  .warning-icon {
    color: ${th('grey70')};

    & svg {
      width: 16px;
      height: 16px;
    }
  }
`

const TextContainer = styled.div`
  width: calc(${th('gridUnit')} * 80);
`

const CustomAssignedButton = styled(Button)`
  pointer-events: none;
  :disabled {
    background-color: ${th('actionSecondaryColor')};
    border-color: ${th('actionSecondaryColor')};
    color: ${th('backgroundColor')};
  }
`

const CustomButton = styled(Button)`
  height: calc(${th('gridUnit')} * 6);
  opacity: 0;
  line-height: calc(${th('gridUnit')} * 4);
  background-color: #000;
  border-color: #000;
  color: ${th('white')};
  width: auto;

  &:hover {
    background-color: ${th('actionSecondaryColor')};
    border-color: ${th('actionSecondaryColor')};
    color: ${th('backgroundColor')};
  }
`

const CustomRow = styled(Row)`
  border-top: ${props => props.isFirst && th('box.border')};
  border-bottom: ${props => !props.isLast && th('box.border')};

  &:hover {
    background-color: #eee;
    border-bottom-left-radius: ${props => props.isLast && '3px'};
    border-bottom-right-radius: ${props => props.isLast && '3px'};

    ${CustomButton} {
      opacity: 1;
    }
  }
`

const StyledIcon = styled(Icon)`
  position: absolute;
  top: 7px;
  right: 8px;
  color: ${({ disabled }) =>
    disabled ? th('colorBorder') : th('textPrimaryColor')};
  cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer')};
`

const Superscript = styled.span`
  position: relative;
  top: -0.5em;
  font-size: 80%;
  font-family: ${th('defaultFont')};
`

const AffiliationColumn = styled(Row)`
  margin-top: 12px;
  align-items: flex-start;
  flex-direction: column;
`

const AlignLeft = styled.div`
  text-align: left;
  max-height: 200px;
  overflow-y: auto;
  color: ${th('grey70')};
`

// #endregion
