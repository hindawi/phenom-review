import React, { useState } from 'react'
import { get } from 'lodash'
import { Formik } from 'formik'
import { space } from 'styled-system'
import { withRouter } from 'react-router-dom'
import { Button } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withModal } from 'component-modal'
import { compose, withProps, withHandlers } from 'recompose'
import { withGQL as withFilesGQL } from 'component-files/client'

import { Row, MultiAction, ContextualBox } from '@hindawi/ui'
import { parseError } from 'component-submission/client/utils'

import { ManuscriptFiles, RespondToReviewer, DetailsAndAuthors } from './'
import { refetchGetSubmission } from '../graphql/refetchQueries'
import { autosaveForm, setInitialValues, validateRevision } from '../utils'

const SubmitRevision = ({
  onSubmit,
  highlight,
  editAuthor,
  journalCode,
  removeAuthor,
  autosaveForm,
  handleUpload,
  handleDelete,
  initialValues,
  revisionDraft,
  updateManuscriptFile,
  revisionManuscriptId,
  addAuthorToManuscript,
  manuscriptType,
}) => {
  const [isFileUploading, setIsFileUploading] = useState(false)
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validate={validateRevision}
    >
      {({ values, errors, handleSubmit, setFieldValue }) =>
        revisionDraft && (
          <ContextualBox
            data-test-id="submit-revision-contextual-box"
            highlight={highlight}
            label="Submit Revision"
            mt={4}
          >
            <Root pr={4} pt={2}>
              {autosaveForm(values)}
              <DetailsAndAuthors
                addAuthorToManuscript={addAuthorToManuscript}
                editAuthor={editAuthor}
                formErrors={errors}
                formValues={values}
                journalCode={journalCode}
                manuscriptType={manuscriptType}
                removeAuthor={removeAuthor}
                revisionManuscriptId={revisionManuscriptId}
                setFieldValue={setFieldValue}
                startExpanded
              />
              <ManuscriptFiles
                formErrors={errors}
                formValues={values}
                handleDelete={handleDelete}
                handleUpload={handleUpload}
                revisionManuscriptId={revisionManuscriptId}
                setIsFileUploading={setIsFileUploading}
                startExpanded
                updateManuscriptFile={updateManuscriptFile}
              />
              <RespondToReviewer
                formValues={values}
                handleUpload={handleUpload}
                revisionDraft={revisionDraft}
                setFieldValue={setFieldValue}
                startExpanded
              />
              <Row justify="flex-end" mb={4}>
                <Button
                  data-test-id="submit-triage-editor-decision"
                  disabled={isFileUploading}
                  onClick={handleSubmit}
                  primary
                  width={48}
                >
                  Submit revision
                </Button>
              </Row>
            </Root>
          </ContextualBox>
        )
      }
    </Formik>
  )
}

export default compose(
  withRouter,
  withFilesGQL(({ match }) => ({
    refetchQueries: [refetchGetSubmission(match)],
  })),
  withModal({
    component: MultiAction,
    modalKey: 'submitRevision',
  }),
  withProps(({ revisionDraft }) => ({
    initialValues: setInitialValues(revisionDraft),
    revisionManuscriptId: get(revisionDraft, 'id'),
    manuscriptType: get(revisionDraft, 'articleType'),
  })),
  withHandlers({
    autosaveForm: ({ updateAutosave, updateDraftRevision }) => values => {
      autosaveForm({
        values,
        updateDraftRevision,
        updateAutosave,
      })
    },
    onSubmit: ({ submitRevision, showModal }) => (values, formikBag) =>
      showModal({
        cancelText: 'Not yet',
        confirmText: 'Submit revision',
        onConfirm: modalProps => submitRevision(values, modalProps, formikBag),
        subtitle: 'Once submitted, no further changes can be made.',
        title: 'Ready to Submit your Revision?',
      }),
    handleUpload: ({ uploadFile }) => (
      file,
      { type, push, entityId, setFetching, setError, clearError, setFileField },
    ) => {
      const fileInput = {
        type,
        size: file.size,
      }

      clearError && clearError()
      setFetching(true)

      uploadFile({ entityId, fileInput, file })
        .then(uploadedFile => {
          setFetching(false)
          push && push(uploadedFile)
          setFileField && setFileField(uploadedFile)
        })
        .catch(e => {
          setFetching(false)
          setError(parseError(e))
        })
    },
    handleDelete: ({ deleteFile }) => (
      file,
      { index, remove, setError, setFetching, setFileField },
    ) => {
      setFetching(true)
      deleteFile(file.id)
        .then(() => {
          setFetching(false)
          remove && remove(index)
          setFileField && setFileField()
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
  }),
)(SubmitRevision)

const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};

  ${space};
`
