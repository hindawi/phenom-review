import { useMutation } from 'react-apollo'
import { get } from 'lodash'
import { mutations } from '../graphql'
import { refetchGetSubmission } from '../graphql/refetchQueries'

const useRespondToEditorialInvitation = ({
  match,
  history,
  academicEditor,
}) => {
  const [acceptAcademicEditorInvitation] = useMutation(
    mutations.acceptAcademicEditorInvitation,
    {
      refetchQueries: match.params.submissionId
        ? [refetchGetSubmission(match)]
        : [],
    },
  )
  const [declineAcademicEditorInvitation] = useMutation(
    mutations.declineAcademicEditorInvitation,
  )

  const respondToEditorialInvitation = async ({
    academicEditorDecision,
    reason,
  }) => {
    const teamMemberId = get(academicEditor, 'id')
    if (academicEditorDecision === 'yes') {
      await acceptAcademicEditorInvitation({
        variables: {
          teamMemberId,
        },
      })
    } else {
      await declineAcademicEditorInvitation({
        variables: {
          teamMemberId,
          reason,
        },
      })
    }
  }

  return { respondToEditorialInvitation }
}

export default useRespondToEditorialInvitation
