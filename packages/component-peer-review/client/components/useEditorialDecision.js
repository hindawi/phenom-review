import { useMutation } from 'react-apollo'
import { mutations } from '../graphql'
import {
  refetchGetAuditLogs,
  refetchGetSubmission,
} from '../graphql/refetchQueries'

const useEditorialDecision = ({ manuscriptId, role, match }) => {
  const [makeDecisionToPublish, { loading: publishLoading }] = useMutation(
    mutations.makeDecisionToPublish,
    {
      refetchQueries:
        role === 'admin'
          ? [refetchGetSubmission(match), refetchGetAuditLogs(match)]
          : [refetchGetSubmission(match)],
    },
  )

  const [makeDecisionToReject, { loading: rejectLoading }] = useMutation(
    mutations.makeDecisionToReject,
    {
      refetchQueries:
        role === 'admin'
          ? [refetchGetSubmission(match), refetchGetAuditLogs(match)]
          : [refetchGetSubmission(match)],
    },
  )

  const [makeDecisionToReturn, { loading: returnLoading }] = useMutation(
    mutations.makeDecisionToReturn,
    {
      refetchQueries:
        role === 'admin'
          ? [refetchGetSubmission(match), refetchGetAuditLogs(match)]
          : [refetchGetSubmission(match)],
    },
  )

  const [requestRevision, { loading: revisionLoading }] = useMutation(
    mutations.requestRevision,
    {
      refetchQueries:
        role === 'admin'
          ? [refetchGetSubmission(match), refetchGetAuditLogs(match)]
          : [refetchGetSubmission(match)],
    },
  )

  const handlePublish = () =>
    makeDecisionToPublish({
      variables: {
        manuscriptId,
      },
    })

  const handleReject = ({ message, rejectDecisionInfo }) =>
    makeDecisionToReject({
      variables: {
        manuscriptId,
        content: message,
        rejectDecisionInfo,
      },
    })

  const handleReturn = content =>
    makeDecisionToReturn({
      variables: {
        manuscriptId,
        content,
      },
    })

  const handleRevision = (decisionType, content) =>
    requestRevision({
      variables: {
        manuscriptId,
        content,
        type: decisionType,
        useCommunicationV2: false,
      },
    })

  const handleEditorialDecision = ({
    decision,
    message,
    rejectDecisionInfo,
  }) => {
    switch (decision) {
      case 'publish':
        return handlePublish()
      case 'reject':
        return handleReject({ message, rejectDecisionInfo })
      case 'returnToAcademicEditor':
        return handleReturn(message)
      // minor, major
      default:
        return handleRevision(decision, message)
    }
  }

  const isMutationLoading = () =>
    publishLoading || returnLoading || rejectLoading || revisionLoading

  return {
    handleEditorialDecision,
    editorialDecisionLoading: isMutationLoading(),
  }
}

export default useEditorialDecision
