Manuscript file list

```js
const files = {
  coverLetter: [
    {
      id:
        '8dca903a-05b9-45ab-89b9-9cb99a9a29c6/02db6c5e-2938-45ac-a5ee-67ae63919bb2',
      name: 'Cover letter.docx',
      size: 59621,
      originalName: 'Cover letter.docx',
    },
  ],
  manuscripts: [
    {
      id:
        '8dca903a-05b9-45ab-89b9-9cb99a9a29c6/5e69e3d9-7f9d-4e8d-b649-6e6a45658d75',
      name: 'Main manuscript.pdf',
      size: 476862,
      originalName: 'Main manuscript.pdf',
    },
  ],
  supplementary: [
    {
      id:
        '8dca903a-05b9-45ab-89b9-9cb99a9a29c6/02db6c5e-2938-45ac-a5ee-67ae63919bb2',
      name: 'Supplementary File 1.jpg',
      size: 59621,
      originalName: 'Supplementary File 1.jpg',
    },
    {
      id:
        '8dca903a-05b9-45ab-89b9-9cb99a9a29c6/5e69e3d9-7f9d-4e8d-b649-6e6a45658d75',
      name: 'Supplementary File 2.docx',
      size: 476862,
      originalName: 'Supplementary File 2.docx',
    },
  ],
  figure: [
    {
      id: '4151f1b6-9702-4d3a-923b-0cce80b5e67a',
      name: 'figure1.jpeg',
      size: 51233,
      originalName: 'figure1.jpeg',
    },
  ],
}
;<ManuscriptFileList
  files={files}
  onDelete={() => alert('delete')}
  onDownload={() => alert('downloading')}
  onPreview={() => alert('No preview')}
/>
```
