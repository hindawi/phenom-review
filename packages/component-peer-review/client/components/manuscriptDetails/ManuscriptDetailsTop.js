import React, { Fragment } from 'react'
import { chain, get, isEmpty } from 'lodash'
import PropTypes from 'prop-types'
import { DateParser, Button } from '@pubsweet/ui'
import { DownloadZip, PreviewFile } from 'component-files/client'
import { compose, withHandlers, withProps } from 'recompose'
import { graphql } from 'react-apollo'
import {
  Row,
  Item,
  Icon,
  Text,
  NewTheme,
  useNavigation,
  roles,
  MultiAction,
} from '@hindawi/ui'
import { Modal } from 'component-modal'
import {
  archiveManuscript,
  deleteManuscript,
  withdrawManuscript,
} from '../../graphql/mutations'

import { ManuscriptVersion } from './'

const { Breadcrumbs } = NewTheme

const ManuscriptDetailsTop = ({
  match,
  history,
  goToEdit,
  versions,
  manuscript,
  manuscriptFile,
  canEditManuscript,
  canWithdrawManuscript,
  canDeleteManuscript,
  archiveManuscript,
  deleteManuscript,
  withdrawManuscript,
  refetchSubmission,
}) => {
  const { goTo } = useNavigation()

  const refetchSubmissionAndCloseModal = modalProps => () => {
    modalProps.setFetching(false)
    refetchSubmission()
    modalProps.hideModal()
  }

  const handleError = modalProps => e => {
    modalProps.setFetching(false)
    modalProps.setError(e.message)
  }

  const onWithdrawManuscript = modalProps => {
    modalProps.setFetching(true)
    withdrawManuscript({
      variables: {
        submissionId: manuscript.submissionId,
      },
    })
      .then(refetchSubmissionAndCloseModal(modalProps))
      .catch(handleError(modalProps))
  }

  const onDeleteManuscript = modalProps => {
    if (manuscript.status === 'draft') {
      modalProps.setFetching(true)
      deleteManuscript({
        variables: {
          manuscriptId: manuscript.id,
        },
      })
        .then(refetchSubmissionAndCloseModal(modalProps))
        .catch(handleError(modalProps))
    } else if (
      manuscript.role === 'admin' ||
      manuscript.role === 'editorialAssistant'
    ) {
      modalProps.setFetching(true)
      archiveManuscript({
        variables: {
          submissionId: manuscript.submissionId,
        },
      })
        .then(refetchSubmissionAndCloseModal(modalProps))
        .catch(handleError(modalProps))
    }
  }

  return (
    <Row alignItems="center" height={4} mb={4}>
      <Breadcrumbs
        backLabel="BACK"
        entityName="ARTICLE DETAILS"
        goBack={goTo('/')}
        label="DASHBOARD"
      />

      <Item alignItems="center" justify="flex-end">
        {canEditManuscript && (
          <Button
            data-test-id="button-qa-manuscript-edit"
            mr={4}
            onClick={goToEdit}
            secondary
            xs
          >
            <Icon fontSize="14px" icon="edit" mr={1} />
            Edit
          </Button>
        )}

        {canWithdrawManuscript && (
          <Modal
            cancelText="CANCEL"
            component={MultiAction}
            confirmText="CONFIRM"
            modalKey={`withdrawManuscript-${manuscript.id}`}
            onConfirm={onWithdrawManuscript}
            title="Are you sure you want to withdraw this manuscript?"
          >
            {showModal => (
              <Button mr={4} onClick={showModal} secondary xs>
                <Icon fontSize="10px" icon="withdraw" mr={1} />
                WITHDRAW
              </Button>
            )}
          </Modal>
        )}

        {canDeleteManuscript && (
          <Modal
            cancelText={manuscript.status === 'draft' ? 'CANCEL' : 'NO'}
            component={MultiAction}
            confirmText={manuscript.status === 'draft' ? 'DELETE' : 'OK'}
            modalKey={`deleteManuscript-${manuscript.id}`}
            onConfirm={onDeleteManuscript}
            title={`Are you sure you want to ${
              manuscript.status === 'draft'
                ? 'delete this submission?'
                : 'remove this manuscript?'
            }`}
          >
            {showModal => (
              <Button mr={4} onClick={showModal} secondary xs>
                <Icon fontSize="10px" icon="delete" mr={1} />
                DELETE
              </Button>
            )}
          </Modal>
        )}

        {!isEmpty(manuscript.files) && (
          <Fragment>
            <PreviewFile file={manuscriptFile} mr={4} />
            <DownloadZip manuscript={manuscript} mr={4} />
          </Fragment>
        )}

        <DateParser
          dateFormat="YYYY-MM-DD"
          durationThreshold={0}
          timestamp={get(manuscript, 'created', '')}
        >
          {timestamp => (
            <Text mr={6} secondary>
              Updated on {timestamp}
            </Text>
          )}
        </DateParser>
        <ManuscriptVersion
          history={history}
          match={match}
          versions={versions}
        />
      </Item>
    </Row>
  )
}

export default compose(
  withProps(({ manuscript }) => ({
    status: get(manuscript, 'status', 'draft'),
    version: get(manuscript, 'version', ''),
    customId: get(manuscript, 'customId', ''),
    manuscriptFile: chain(manuscript)
      .get('files', [])
      .find(file => file.type === 'manuscript')
      .value(),
    role: get(manuscript, 'role', ''),
  })),
  withProps(({ status, isAdminOrEditorialAssistant, isLatestVersion }) => ({
    canEditManuscript:
      isAdminOrEditorialAssistant &&
      isLatestVersion &&
      ![
        'rejected',
        'withdrawn',
        'void',
        'published',
        'refusedToConsider',
      ].includes(status),
  })),
  withProps(({ manuscript: { role, status } }) => ({
    canDeleteManuscript:
      status === 'draft' ||
      (!['draft', 'withdrawn'].includes(status) &&
        [roles.ADMIN, roles.EDITORIAL_ASSISTANT].includes(role)),
    canWithdrawManuscript:
      ![
        'draft',
        'accepted',
        'rejected',
        'deleted',
        'withdrawn',
        'void',
        'published',
        'refusedToConsider',
      ].includes(status) &&
      [
        roles.ADMIN,
        roles.EDITORIAL_ASSISTANT,
        roles.SUBMITTING_STAFF_MEMBER,
      ].includes(role),
  })),
  withHandlers({
    goToEdit: ({ history, manuscript }) => () => {
      history.push(`/submit/${manuscript.submissionId}/${manuscript.id}`)
    },
  }),
  graphql(archiveManuscript, {
    name: 'archiveManuscript',
  }),
  graphql(deleteManuscript, {
    name: 'deleteManuscript',
  }),
  graphql(withdrawManuscript, {
    name: 'withdrawManuscript',
  }),
)(ManuscriptDetailsTop)

ManuscriptDetailsTop.propTypes = {
  /** Object containing the selected fragment. */
  fragment: PropTypes.object, //eslint-disable-line
  /** Object containing the selected collection. */
  collection: PropTypes.object, //eslint-disable-line
  /** Object with versions of manuscript. */
  versions: PropTypes.array, //eslint-disable-line
  /** An async call that takes you to edit. */
  goToEdit: PropTypes.func,
  /** Object containing token for current user. */
  currentUser: PropTypes.object, //eslint-disable-line
}

ManuscriptDetailsTop.defaultProps = {
  fragment: {},
  collection: {},
  versions: [],
  goToEdit: () => {},
  currentUser: {},
}
