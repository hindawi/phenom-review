import React, { Fragment } from 'react'
import { get } from 'lodash'
import { H2, H4, DateParser } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import {
  Row,
  Text,
  Label,
  StatusTag,
  ActionLink,
  AuthorTagList,
  CopyEmail,
  Icon,
  Item,
  Tag,
  roles,
} from '@hindawi/ui'
import moment from 'moment'
import { useMutation } from 'react-apollo'
import { mutations } from '../../graphql'
import { refetchGetSubmission } from '../../graphql/refetchQueries'
import PersonInvitation from '../PersonInvitation'
import ManuscriptMetadata from './ManuscriptMetadata'

const AcademicEditorAssignator = ({
  role,
  academicEditorLabel,
  hasPeerReview,
  canSeeInvite,
  canSeeRevoke,
  assignedAcademicEditor,
  isLatestVersion,
  isApprovalEditor,
  isTriageEditorWithCOI,
  manuscriptStatus,
  toggleAssignAcademicEditor,
  onCancelAcademicEditorInvitation,
  researchIntegrityPublishingEditor,
}) => {
  const articleTypeCheck =
    !hasPeerReview && ['inQA', 'accepted'].includes(manuscriptStatus)
  const hasAEAssigned = get(assignedAcademicEditor, 'id', null)

  const personInvitationLabel =
    (hasAEAssigned || !isTriageEditorWithCOI) && academicEditorLabel

  return (
    (canSeeInvite || canSeeRevoke) &&
    !researchIntegrityPublishingEditor && (
      <Fragment>
        <PersonInvitation
          hasPeerReview={hasPeerReview}
          invitation={assignedAcademicEditor}
          isApprovalEditor={isApprovalEditor}
          label={personInvitationLabel}
          manuscriptStatus={manuscriptStatus}
          onRevoke={onCancelAcademicEditorInvitation}
          role={role}
          toggleAssignAcademicEditor={toggleAssignAcademicEditor}
          withEmailCopyTooltip
        />
        {!hasAEAssigned &&
          isLatestVersion &&
          !isTriageEditorWithCOI &&
          (!articleTypeCheck ? (
            <ActionLink
              fontWeight={600}
              onClick={toggleAssignAcademicEditor}
              secondary
              whenHover
            >
              <Icon icon="addUser" ml={1} mr={1} />
              Invite
            </ActionLink>
          ) : (
            <Text>Unassigned</Text>
          ))}
      </Fragment>
    )
  )
}

const TriageEditorAssignator = ({ toggleReassignTriageEditor, role }) =>
  ['admin', 'editorialAssistant'].includes(role) && (
    <Fragment>
      <ActionLink
        fontWeight={600}
        onClick={toggleReassignTriageEditor}
        secondary
        whenHover
      >
        <Icon icon="reassign" ml={1} mr={1} />
        Reassign
      </ActionLink>
    </Fragment>
  )

const AcademicEditorLabel = ({
  academicEditorLabel,
  academicEditorVisibleStatus,
  canSeeLabelAuthorReviewer,
  canSeeLabelOldVersionsAdminOrTriageEditor,
  canSeeLabelUnassigned,
}) =>
  (canSeeLabelAuthorReviewer ||
    canSeeLabelOldVersionsAdminOrTriageEditor ||
    canSeeLabelUnassigned) && (
    <Fragment>
      <Label mr={2}>{academicEditorLabel}</Label>
      <Text>{academicEditorVisibleStatus}</Text>
    </Fragment>
  )

const RIPELabel = ({ researchIntegrityPublishingEditorName }) => (
  <Fragment>
    <Label mr={2}>Research Integrity Publishing Editor</Label>
    <Text>{researchIntegrityPublishingEditorName}</Text>
  </Fragment>
)

const ManuscriptHeader = ({
  match,
  manuscript,
  triageEditors,
  hasTriageEditor,
  isLatestVersion,
  isApprovalEditor,
  isTriageEditorWithCOI,
  triageEditorLabel,
  academicEditorLabel,
  toggleReassignTriageEditor,
  toggleAssignAcademicEditor,
}) => {
  const { statusColor } = manuscript

  const [cancelAcademicEditorInvitation] = useMutation(
    mutations.cancelAcademicEditorInvitation,
    {
      refetchQueries: [refetchGetSubmission(match)],
    },
  )

  const handleCancelAcademicEditorInvitation = (invitation, modalProps) => {
    modalProps.setFetching(true)
    cancelAcademicEditorInvitation({
      variables: {
        teamMemberId: invitation.id,
      },
    })
      .then(() => {
        modalProps.setFetching(false)
        modalProps.hideModal()
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(e.message)
      })
  }

  const articleType = get(manuscript, 'articleType.name', '')
  const role = get(manuscript, 'role')
  const journalTitle = get(manuscript.journal, 'name', '')
  const manuscriptStatus = get(manuscript, 'status', '')
  const visibleStatus = get(manuscript, 'visibleStatus', '')
  const hasPeerReview = get(manuscript, 'articleType.hasPeerReview', '')
  const assignedAcademicEditor = get(manuscript, 'academicEditor', {})
  const academicEditorStatus = get(manuscript, 'academicEditor.status', '')
  const triageEditor = get(manuscript, 'triageEditor')
  const specialIssueName = get(manuscript, 'specialIssue.name', '')
  const specialIssueEndDate = get(manuscript, 'specialIssue.endDate', '')
  const sectionName =
    get(manuscript, 'section.name', '') ||
    get(manuscript, 'specialIssue.section.name', '')
  const hasStaffRole = [
    roles.ADMIN,
    roles.EDITORIAL_ASSISTANT,
    roles.RESEARCH_INTEGRITY_PUBLISHING_EDITOR,
  ].includes(role)
  const triageEditorName = `${get(
    triageEditor,
    'alias.name.givenNames',
    '',
  )} ${get(triageEditor, 'alias.name.surname', 'Unassigned')}`

  const triageEditorEmail = get(triageEditor, 'alias.email')

  const researchIntegrityPublishingEditor = get(
    manuscript,
    'researchIntegrityPublishingEditor',
    {},
  )
  const researchIntegrityPublishingEditorName = `${get(
    manuscript,
    'researchIntegrityPublishingEditor.alias.name.givenNames',
  )} ${get(manuscript, 'researchIntegrityPublishingEditor.alias.name.surname')}`

  const {
    canSeeLabelAuthorReviewer,
    canSeeLabelOldVersionsAdminOrTriageEditor,
    canSeeLabelUnassigned,
    canSeeInvite,
    canSeeRevoke,
  } = getLabelVisibility({
    role,
    academicEditorStatus,
    isLatestVersion,
    manuscriptStatus,
  })

  const { academicEditorVisibleStatus } = getAcademicEditorStatus({
    role,
    assignedAcademicEditor,
  })

  return (
    <Fragment>
      <Row alignItems="flex-start" p={4} pt={5}>
        <Row alignItems="flex-start" flexDirection="column">
          <H2 pr={20}>{manuscript.meta.title || 'No title'}</H2>
          {manuscript.authors.length > 0 && (
            <Row
              alignItems="center"
              data-test-id="authors-row"
              justify="flex-start"
              mt={2}
            >
              <AuthorTagList
                authors={manuscript.authors}
                hasStaffRole={hasStaffRole}
                withAffiliations
                withEmailCopyTooltip
              />
            </Row>
          )}

          <Row alignItems={specialIssueName ? 'flex-end' : 'flex-start'}>
            <FixedItem vertical>
              <Label mb={1} mt={2}>
                Article Type
              </Label>
              <Text>{articleType}</Text>
              <Label mb={1} mt={2}>
                Journal
              </Label>
              <Text>{journalTitle}</Text>
            </FixedItem>
            <FixedItem vertical>
              {sectionName && (
                <>
                  <Label mb={1} mt={2}>
                    Section
                  </Label>
                  <WithEllipsis>{sectionName}</WithEllipsis>
                </>
              )}
              {specialIssueName && (
                <>
                  <Row justify="end">
                    <Label mb={1} mt={2}>
                      Special Issue
                    </Label>
                    <StyledTag dateLabel>
                      CLOSING DATE:
                      {moment(specialIssueEndDate).format(' YYYY-MM-DD')}
                    </StyledTag>
                  </Row>
                  <WithEllipsis>{specialIssueName}</WithEllipsis>
                </>
              )}
            </FixedItem>
          </Row>
        </Row>

        <Row alignItems="flex-end" flexDirection="column" minWidth={40}>
          <StatusTag
            hasPeerReview={hasPeerReview}
            old={!isLatestVersion}
            statusColor={statusColor}
            version={manuscript.version}
          >
            {isLatestVersion || manuscript.status === 'withdrawn'
              ? `${visibleStatus}`
              : 'Viewing An Older Version'}
          </StatusTag>
          {manuscript.customId && (
            <Text
              data-test-id="manuscript-id"
              newCustomId
              pt={3}
            >{`ID ${manuscript.customId}`}</Text>
          )}
        </Row>
      </Row>

      <CustomBox>
        <Row mb={3}>
          <Row>
            {hasTriageEditor && hasPeerReview && (
              <FixedItem alignItems="center">
                <H4>{triageEditorLabel}</H4>
                {hasStaffRole && triageEditor ? (
                  <CopyEmail email={triageEditorEmail}>
                    <ActionLink ml={2} mr={2}>
                      {triageEditorName}
                    </ActionLink>
                  </CopyEmail>
                ) : (
                  <Text ml={2} mr={2}>
                    {triageEditorName}
                  </Text>
                )}
                {isLatestVersion && triageEditors.length > 1 && (
                  <TriageEditorAssignator
                    role={role}
                    toggleReassignTriageEditor={toggleReassignTriageEditor}
                  />
                )}
              </FixedItem>
            )}
            <FixedItem alignItems="center">
              {researchIntegrityPublishingEditor ? (
                <RIPELabel
                  researchIntegrityPublishingEditorName={
                    researchIntegrityPublishingEditorName
                  }
                />
              ) : (
                <AcademicEditorLabel
                  academicEditorLabel={academicEditorLabel}
                  academicEditorVisibleStatus={academicEditorVisibleStatus}
                  canSeeLabelAuthorReviewer={canSeeLabelAuthorReviewer}
                  canSeeLabelOldVersionsAdminOrTriageEditor={
                    canSeeLabelOldVersionsAdminOrTriageEditor
                  }
                  canSeeLabelUnassigned={canSeeLabelUnassigned}
                />
              )}

              <AcademicEditorAssignator
                academicEditorLabel={academicEditorLabel}
                articleType={articleType}
                assignedAcademicEditor={assignedAcademicEditor}
                canSeeInvite={canSeeInvite}
                canSeeRevoke={canSeeRevoke}
                hasPeerReview={hasPeerReview}
                hasStaffRole={hasStaffRole}
                isApprovalEditor={isApprovalEditor}
                isLatestVersion={isLatestVersion}
                isTriageEditorWithCOI={isTriageEditorWithCOI}
                manuscriptStatus={manuscriptStatus}
                onCancelAcademicEditorInvitation={
                  handleCancelAcademicEditorInvitation
                }
                researchIntegrityPublishingEditor={
                  researchIntegrityPublishingEditor
                }
                role={role}
                toggleAssignAcademicEditor={toggleAssignAcademicEditor}
                visibleStatus={visibleStatus}
              />
            </FixedItem>
          </Row>
          <Item minWidth={40}>
            <DateParser
              dateFormat="YYYY-MM-DD"
              durationThreshold={0}
              timestamp={manuscript.created}
            >
              {(timestamp, timeAgo) => (
                <Date>{`Submitted on ${timestamp} (${timeAgo} ago)`}</Date>
              )}
            </DateParser>
          </Item>
        </Row>
        <ManuscriptMetadata manuscript={manuscript} role={role} />
      </CustomBox>
    </Fragment>
  )
}

export default ManuscriptHeader

// #region styles

// browser bug: flex items using flex-basis and white-space: nowrap broken
// width works.
const FixedItem = styled(Item)`
  width: 50%;
`

const Date = styled(Text)`
  white-space: nowrap;
  transform: translate(-100px, 0);
`

const WithEllipsis = styled(Text)`
  width: 100%;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`

const CustomBox = styled.div`
  padding: calc(${th('gridUnit')} * 3) calc(${th('gridUnit')} * 4)
    calc(${th('gridUnit')} * 4) calc(${th('gridUnit')} * 4);
  background-color: ${th('backgroundColor')};
`
const StyledTag = styled(Tag)`
  color: ${th('labelLineColor')};
  margin-top: calc(${th('gridUnit')} * 1);
  margin-left: calc(${th('gridUnit')} * 1);
`
const getAcademicEditorStatus = ({ role, assignedAcademicEditor }) => {
  const academicEditorStatus = get(assignedAcademicEditor, 'status', '')
  let academicEditorVisibleStatus = 'Unassigned'
  if (academicEditorStatus === 'accepted') {
    academicEditorVisibleStatus = `${get(
      assignedAcademicEditor,
      'alias.name.surname',
      '',
    )} ${get(assignedAcademicEditor, 'alias.name.givenNames', '')}`
  } else if (role === 'author' && academicEditorStatus === 'pending') {
    academicEditorVisibleStatus = 'Assigned'
  } else if (academicEditorStatus === 'pending') {
    academicEditorVisibleStatus = 'Invited'
  }
  return {
    academicEditorVisibleStatus,
  }
}

const getLabelVisibility = ({
  role,
  academicEditorStatus,
  isLatestVersion,
  manuscriptStatus,
}) => ({
  canSeeLabelAuthorReviewer:
    ['pending', 'accepted', ''].includes(academicEditorStatus) &&
    !['admin', 'triageEditor', 'editorialAssistant'].includes(role),

  canSeeLabelOldVersionsAdminOrTriageEditor:
    ['accepted', '', 'pending'].includes(academicEditorStatus) &&
    !isLatestVersion &&
    ['admin', 'triageEditor', 'editorialAssistant'].includes(role),

  canSeeLabelUnassigned:
    academicEditorStatus === '' &&
    ['admin', 'triageEditor'].includes(role) &&
    ['technicalChecks', 'revisionRequested'].includes(manuscriptStatus),

  canSeeInvite:
    isLatestVersion &&
    academicEditorStatus === '' &&
    ['admin', 'triageEditor', 'editorialAssistant'].includes(role) &&
    ![
      'technicalChecks',
      'revisionRequested',
      'rejected',
      'makeDecision',
      'withdrawn',
      'void',
      'refusedToConsider',
    ].includes(manuscriptStatus),

  canSeeRevoke:
    isLatestVersion &&
    academicEditorStatus === 'accepted' &&
    ['admin', 'triageEditor', 'editorialAssistant'].includes(role) &&
    !['technicalChecks', 'rejected', 'accepted'].includes(manuscriptStatus),
})
