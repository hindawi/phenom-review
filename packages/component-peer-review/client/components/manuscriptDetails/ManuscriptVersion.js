import React from 'react'
import PropTypes from 'prop-types'
import { get } from 'lodash'
import { NewTheme } from '@hindawi/ui'
import { compose, withHandlers, withProps } from 'recompose'

const { Dropdown } = NewTheme

const ManuscriptVersion = ({ versions, onChange, value }) =>
  versions.length > 0 && (
    <Dropdown
      onChange={onChange}
      optionLabel="label"
      options={versions}
      value={value}
      variant="small"
    />
  )

ManuscriptVersion.propTypes = {
  /** Object with versions of manuscript. */
  versions: PropTypes.array, //eslint-disable-line
}

ManuscriptVersion.defaultProps = {
  versions: [],
}

export default compose(
  withProps(({ match, versions = [] }) => ({
    value: versions.find(
      version => version.value === get(match, 'params.manuscriptId'),
    ),
  })),
  withHandlers({
    onChange: ({ history, match }) => ({ value }) => {
      history.replace(`/details/${get(match, 'params.submissionId')}/${value}`)
    },
  }),
)(ManuscriptVersion)
