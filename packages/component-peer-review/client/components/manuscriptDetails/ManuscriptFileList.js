import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Text } from '@hindawi/ui'
import { File } from 'component-files/client'

import { parseManuscriptFiles } from '../../utils'

const ManuscriptFileList = ({ files }) => {
  const {
    manuscript,
    coverLetter,
    supplementary,
    figure = [],
  } = parseManuscriptFiles(files)
  return (
    <FileSectionsWrapper>
      <FileSection files={manuscript} label="Main manuscript" />
      <FileSection
        files={coverLetter}
        label={coverLetter.length ? 'Cover letter' : ''}
      />
      <FileSection
        files={supplementary}
        label={supplementary.length ? 'Supplemental files' : ''}
      />
      <FileSection files={figure} label={figure.length ? 'Figure files' : ''} />
    </FileSectionsWrapper>
  )
}

const FileSection = ({ label, files = [] }) => (
  <FileSectionWrapper>
    <Header fontWeight={600} mb={1}>
      {label}
    </Header>
    {files.map(file => (
      <File grey item={file} key={file.id} mb={2} />
    ))}
  </FileSectionWrapper>
)

const FileSectionsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`

const FileSectionWrapper = styled.div`
  flex: 1 0 calc(50% - 16px);

  &:nth-child(odd) {
    margin-right: calc(${th('gridUnit')} * 4);
  }

  margin-bottom: calc(${th('gridUnit')} * 2);
  &:last-child,
  :nth-last-child(2) {
    margin-bottom: 0;
  }
`

const Header = styled(Text)`
  font-size: ${th('h3Size')};
  line-height: ${th('h3LineHeight')};
`

export default ManuscriptFileList
