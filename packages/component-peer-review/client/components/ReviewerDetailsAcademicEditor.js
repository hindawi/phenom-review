import React from 'react'
import { PublonsTable } from 'component-reviewer-suggestions/client'

import { InviteReviewers } from '.'
import { ReviewerDetails, ReviewerReports } from './peerReviewComponents'

const ReviewerDetailsAcademicEditor = ({
  options,
  manuscript,
  highlight,
  startExpanded,
  reviewers = [],
  inviteReviewer,
  reviewerReports,
  cancelReviewerInvitation,
}) => (
  <ReviewerDetails
    highlight={highlight}
    mt={4}
    reviewers={reviewers}
    startExpanded={startExpanded}
    tabButtons={[
      'Reviewer Details',
      'Reviewer Suggestions',
      'Reviewer Reports',
    ]}
  >
    <InviteReviewers
      cancelReviewerInvitation={cancelReviewerInvitation}
      canInviteReviewers={[
        'academicEditorAssigned',
        'reviewersInvited',
        'underReview',
        'makeDecision',
        'reviewCompleted',
      ].includes(manuscript.status)}
      reviewers={reviewers}
    />
    <PublonsTable
      canInvitePublons={[
        'academicEditorAssigned',
        'reviewersInvited',
        'underReview',
        'makeDecision',
        'reviewCompleted',
      ].includes(manuscript.status)}
      manuscriptId={manuscript.id}
      onInvite={inviteReviewer}
    />
    <ReviewerReports options={options} reviewerReports={reviewerReports} />
  </ReviewerDetails>
)

export default ReviewerDetailsAcademicEditor
