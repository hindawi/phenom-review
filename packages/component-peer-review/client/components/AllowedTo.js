import rules from '../rbac-rules'

export const check = (rules, role, action, data) => {
  const permissions = rules[role]
  if (!permissions) return false

  const staticPermissions = permissions.static
  if (staticPermissions && staticPermissions.includes(action)) return true

  const dynamicPermissions = permissions.dynamic
  if (!dynamicPermissions) return false

  const permissionCondition = dynamicPermissions[action]
  if (!permissionCondition || typeof permissionCondition !== 'function') {
    return false
  }
  return permissionCondition(data)
}

const AllowedTo = ({ role, action, data, yes, no }) =>
  check(rules, role, action, data) ? yes : no

AllowedTo.defaultProps = {
  yes: '',
  no: '',
}

export default AllowedTo
