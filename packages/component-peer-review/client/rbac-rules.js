/* eslint-disable sonarjs/no-identical-functions */
import { chain, get, isEmpty } from 'lodash'

const rules = {
  author: {
    static: [],
    dynamic: {
      'manuscript:viewEditorialComments': ({ manuscriptStatus }) =>
        manuscriptStatus &&
        !['pendingApproval', 'inQA'].includes(manuscriptStatus),
      'manuscript:viewAuthorReply': () => true,
      'manuscript:viewReviewerReports': ({
        isLatestVersion,
        manuscriptStatus,
      }) =>
        [
          'revisionRequested',
          'accepted',
          'rejected',
          'withdrawn',
          'void',
          'published',
          'qualityChecksRequested',
          'refusedToConsider',
        ].includes(manuscriptStatus) || !isLatestVersion,
      'manuscript:submitRevision': ({ manuscriptStatus }) =>
        manuscriptStatus === 'revisionRequested',
      'manuscript:submitQualityChecksRevision': ({ manuscriptStatus }) =>
        manuscriptStatus === 'qualityChecksRequested',
    },
  },
  reviewer: {
    static: [],
    dynamic: {
      'manuscript:viewEditorialComments': ({ userId, reviewers }) => {
        if (!reviewers || !userId) return false
        const currentReviewer = reviewers.find(
          reviewer => reviewer.user.id === userId,
        )

        return (
          currentReviewer &&
          ['accepted', 'submitted'].includes(currentReviewer.status)
        )
      },
      'manuscript:viewAuthorReply': ({ userId, reviewers }) => {
        if (!reviewers || !userId) return false
        const currentReviewer = reviewers.find(
          reviewer => reviewer.user.id === userId,
        )

        return (
          currentReviewer &&
          ['accepted', 'submitted'].includes(currentReviewer.status)
        )
      },
      'manuscript:respondToReviewerInvitation': ({ userId, reviewers }) => {
        if (!reviewers || !userId) return false
        const currentReviewer = reviewers.find(
          reviewer => reviewer.user.id === userId,
        )

        return currentReviewer && currentReviewer.status === 'pending'
      },
      'manuscript:submitReview': ({
        userId,
        reviews,
        reviewers,
        manuscriptStatus,
      }) => {
        const currentReviewer = reviewers.find(
          reviewer => reviewer.user.id === userId,
        )

        const reviewerCanSeeHisReport = !!chain(reviews)
          .find(review => get(review, 'member.user.id') === userId)
          .get('submitted')
          .value()
        const reviewerCanSubmitReport =
          get(currentReviewer, 'status') === 'accepted' &&
          ![
            'revisionRequested',
            'olderVersion',
            'pendingApproval',
            'qualityChecksRequested',
            'qualityChecksSubmitted',
            'published',
            'rejected',
            'inQA',
          ].includes(manuscriptStatus)

        return reviewerCanSubmitReport && !reviewerCanSeeHisReport
      },
      'manuscript:viewReviewerReports': ({ userId, reviews }) =>
        !!chain(reviews)
          .find(review => get(review, 'member.user.id') === userId)
          .get('submitted')
          .value(),
    },
  },
  academicEditor: {
    static: [],
    dynamic: {
      'manuscript:viewEditorialComments': ({
        academicEditorStatus,
        academicEditorUserId,
        userId,
      }) =>
        academicEditorStatus === 'accepted' && academicEditorUserId === userId,
      'manuscript:viewAuthorReply': ({
        academicEditorStatus,
        academicEditorUserId,
        userId,
      }) =>
        academicEditorStatus === 'accepted' && academicEditorUserId === userId,
      'manuscript:respondToEditorialInvitation': ({
        userId,
        pendingAcademicEditorStatus,
        pendingAcademicEditorUserId,
        isLatestVersion,
      }) =>
        isLatestVersion &&
        pendingAcademicEditorUserId === userId &&
        pendingAcademicEditorStatus === 'pending',
      'manuscript:inviteReviewers': ({
        userId,
        hasPeerReview,
        manuscriptStatus,
        academicEditorStatus,
        academicEditorUserId,
      }) =>
        !['academicEditorInvited', 'qualityChecksSubmitted'].includes(
          manuscriptStatus,
        ) &&
        academicEditorStatus === 'accepted' &&
        academicEditorUserId === userId &&
        hasPeerReview,
      'manuscript:makeRecommendation': ({
        userId,
        manuscriptStatus,
        hasPeerReview,
        isApprovalEditor,
        academicEditorUserId,
      }) =>
        academicEditorUserId === userId &&
        !isApprovalEditor &&
        [
          'academicEditorAssigned',
          'reviewCompleted',
          'academicEditorAssignedEditorialType',
        ].includes(manuscriptStatus) &&
        hasPeerReview,
      'manuscript:makeDecision': ({
        manuscriptStatus,
        isApprovalEditor,
        isLatestVersion,
        academicEditorStatus,
        hasTriageEditorConflictOfInterest,
        userId,
        academicEditorUserId,
      }) => {
        const currentAcademicEditorIsAccepted =
          academicEditorStatus === 'accepted' && academicEditorUserId === userId

        return (
          currentAcademicEditorIsAccepted &&
          (isApprovalEditor || hasTriageEditorConflictOfInterest) &&
          isLatestVersion &&
          ![
            'inQA',
            'accepted',
            'rejected',
            'published',
            'academicEditorInvited',
            'qualityChecksRequested',
            'qualityChecksSubmitted',
            'revisionRequested',
            'refusedToConsider',
          ].includes(manuscriptStatus)
        )
      },
    },
  },
  triageEditor: {
    static: [],
    dynamic: {
      'manuscript:viewEditorialComments': () => true,
      'manuscript:viewAuthorReply': ({ manuscriptStatus }) =>
        manuscriptStatus !== 'revisionRequested',
      'manuscript:makeDecision': ({
        manuscriptStatus,
        isApprovalEditor,
        isTriageEditorWithCOI,
      }) => {
        if (isTriageEditorWithCOI) return

        return isApprovalEditor
          ? ![
              'inQA',
              'rejected',
              'published',
              'accepted',
              'olderVersion',
              'refusedToConsider',
              'revisionRequested',
              'qualityChecksRequested',
              'qualityChecksSubmitted',
            ].includes(manuscriptStatus)
          : ![
              'academicEditorAssigned',
              'inQA',
              'accepted',
              'rejected',
              'academicEditorAssignedEditorialType',
              'reviewersInvited',
              'underReview',
              'olderVersion',
              'makeDecision',
              'refusedToConsider',
              'revisionRequested',
              'qualityChecksRequested',
              'qualityChecksSubmitted',
              'published',
            ].includes(manuscriptStatus)
      },

      'manuscript:viewReviewers': ({
        academicEditorStatus,
        hasPeerReview,
        isTriageEditorWithCOI,
      }) =>
        (academicEditorStatus === 'accepted' && hasPeerReview) ||
        isTriageEditorWithCOI,
      'manuscript:assignAcademicEditor': ({
        manuscriptStatus,
        isTriageEditorWithCOI,
      }) =>
        ![
          'technicalChecks',
          'rejected',
          'inQA',
          'accepted',
          'olderVersion',
          'withdrawn',
          'qualityChecksRequested',
          'qualityChecksSubmitted',
          'published',
          'refusedToConsider',
          'revisionRequested',
        ].includes(manuscriptStatus) && !isTriageEditorWithCOI,
    },
  },
  editorialAssistant: {
    static: [],
    dynamic: {
      'manuscript:viewEditorialComments': () => true,
      'manuscript:viewAuthorReply': ({ manuscriptStatus }) =>
        manuscriptStatus !== 'revisionRequested',
      'manuscript:inviteReviewers': ({
        manuscriptStatus,
        academicEditorStatus,
        hasPeerReview,
      }) =>
        !['academicEditorInvited', 'qualityChecksSubmitted'].includes(
          manuscriptStatus,
        ) &&
        academicEditorStatus === 'accepted' &&
        hasPeerReview,
      'manuscript:makeDecision': ({ manuscriptStatus, hasPeerReview }) =>
        ![
          'technicalChecks',
          'rejected',
          'inQA',
          'accepted',
          'olderVersion',
          'withdrawn',
          'revisionRequested',
          'qualityChecksRequested',
          'qualityChecksSubmitted',
          'published',
          'refusedToConsider',
        ].includes(manuscriptStatus) && hasPeerReview,
      'manuscript:viewActivityLog': () => true,
      'manuscript:assignAcademicEditor': ({
        manuscriptStatus,
        hasPeerReview,
      }) =>
        ![
          'technicalChecks',
          'rejected',
          'inQA',
          'accepted',
          'olderVersion',
          'withdrawn',
          'qualityChecksRequested',
          'qualityChecksSubmitted',
          'published',
          'refusedToConsider',
          'revisionRequested',
        ].includes(manuscriptStatus) && hasPeerReview,
      'manuscript:reassignTriageEditor': ({
        manuscriptStatus,
        triageEditors,
        hasPeerReview,
      }) =>
        hasPeerReview &&
        !isEmpty(triageEditors) &&
        ![
          'technicalChecks',
          'rejected',
          'inQA',
          'accepted',
          'olderVersion',
          'withdrawn',
          'qualityChecksRequested',
          'qualityChecksSubmitted',
          'published',
          'refusedToConsider',
          'revisionRequested',
        ].includes(manuscriptStatus),
      'manuscript:submitQualityChecksRevision': ({ manuscriptStatus }) =>
        manuscriptStatus === 'qualityChecksRequested',
    },
  },
  researchIntegrityPublishingEditor: {
    static: [],
    dynamic: {
      'manuscript:viewEditorialComments': () => true,
      'manuscript:makeDecision': ({ reviews }) => reviews.length === 0,
    },
  },
  admin: {
    static: [],
    dynamic: {
      'manuscript:viewEditorialComments': () => true,
      'manuscript:viewAuthorReply': ({ manuscriptStatus }) =>
        manuscriptStatus !== 'revisionRequested',
      'manuscript:inviteReviewers': ({
        manuscriptStatus,
        academicEditorStatus,
        hasPeerReview,
      }) =>
        !['academicEditorInvited', 'qualityChecksSubmitted'].includes(
          manuscriptStatus,
        ) &&
        academicEditorStatus === 'accepted' &&
        hasPeerReview,
      'manuscript:makeDecision': ({ manuscriptStatus }) =>
        ![
          'technicalChecks',
          'rejected',
          'inQA',
          'accepted',
          'olderVersion',
          'withdrawn',
          'void',
          'revisionRequested',
          'qualityChecksRequested',
          'qualityChecksSubmitted',
          'published',
          'refusedToConsider',
        ].includes(manuscriptStatus),
      'manuscript:viewActivityLog': () => true,
      'manuscript:assignAcademicEditor': ({
        manuscriptStatus,
        hasPeerReview,
      }) =>
        hasPeerReview &&
        ![
          'technicalChecks',
          'rejected',
          'inQA',
          'accepted',
          'olderVersion',
          'withdrawn',
          'void',
          'qualityChecksRequested',
          'qualityChecksSubmitted',
          'published',
          'refusedToConsider',
        ].includes(manuscriptStatus),
      'manuscript:submitQualityChecksRevision': ({ manuscriptStatus }) =>
        manuscriptStatus === 'qualityChecksRequested',
      'manuscript:reassignTriageEditor': ({
        manuscriptStatus,
        triageEditors,
        hasPeerReview,
      }) =>
        hasPeerReview &&
        !isEmpty(triageEditors) &&
        ![
          'technicalChecks',
          'rejected',
          'inQA',
          'accepted',
          'olderVersion',
          'withdrawn',
          'void',
          'qualityChecksRequested',
          'qualityChecksSubmitted',
          'published',
          'refusedToConsider',
        ].includes(manuscriptStatus),
    },
  },
  submittingStaffMember: {
    static: [],
    dynamic: {
      'manuscript:submitQualityChecksRevision': ({ manuscriptStatus }) =>
        manuscriptStatus === 'qualityChecksRequested',
    },
  },
}

export default rules
