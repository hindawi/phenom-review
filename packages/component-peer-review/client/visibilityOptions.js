import { get, isEmpty } from 'lodash'

export const getExpandedStatus = ({ manuscriptStatus, authorResponse }) => (
  action,
  role,
) => {
  const expandedOption = {
    'manuscript:viewEditorialComments': {
      author: true,
      reviewer: true,
      academicEditor: true,
      triageEditor: true,
      editorialAssistant: true,
      admin: true,
    },
    'manuscript:viewAuthorReply': {
      author: manuscriptStatus !== 'revisionRequested',
      reviewer: manuscriptStatus === 'underReview',
      academicEditor: [
        'academicEditorAssigned',
        'reviewersInvited',
        'underReview',
      ].includes(manuscriptStatus),
      triageEditor: manuscriptStatus === 'submitted',
      editorialAssistant: manuscriptStatus === 'submitted',
      researchIntegrityPublishingEditor: false,
      admin: manuscriptStatus === 'submitted',
    },
    'manuscript:respondToReviewerInvitation': {
      reviewer: true,
    },
    'manuscript:viewReviewerReports': {
      reviewer: true,
    },
    'manuscript:respondToEditorialInvitation': {
      academicEditor: true,
    },
    'manuscript:inviteReviewers': {
      academicEditor:
        (['academicEditorAssigned', 'reviewersInvited', 'underReview'].includes(
          manuscriptStatus,
        ) &&
          isEmpty(authorResponse)) ||
        manuscriptStatus === 'reviewCompleted',
      editorialAssistant:
        (['academicEditorAssigned', 'reviewersInvited', 'underReview'].includes(
          manuscriptStatus,
        ) &&
          isEmpty(authorResponse)) ||
        manuscriptStatus === 'reviewCompleted',
      admin:
        (['academicEditorAssigned', 'reviewersInvited', 'underReview'].includes(
          manuscriptStatus,
        ) &&
          isEmpty(authorResponse)) ||
        manuscriptStatus === 'reviewCompleted',
    },
  }

  return get(expandedOption, `${action}.${role}`, false)
}

export const getHighlightedStatus = ({ manuscriptStatus }) => (
  action,
  role,
) => {
  const highlightedOption = {
    'manuscript:respondToReviewerInvitation': {
      reviewer: true,
    },
    'manuscript:submitReview': {
      reviewer: true,
    },
    'manuscript:submitRevision': {
      author: true,
    },
    'manuscript:respondToEditorialInvitation': {
      academicEditor: true,
    },
    'manuscript:inviteReviewers': {
      academicEditor: [
        'academicEditorAssigned',
        'reviewersInvited',
        'underReview',
      ].includes(manuscriptStatus),
      editorialAssistant: [
        'academicEditorAssigned',
        'reviewersInvited',
        'underReview',
      ].includes(manuscriptStatus),
      admin: [
        'academicEditorAssigned',
        'reviewersInvited',
        'underReview',
      ].includes(manuscriptStatus),
    },
    'manuscript:makeRecommendation': {
      academicEditor: ['reviewCompleted'].includes(manuscriptStatus),
    },
    'manuscript:makeDecision': {
      academicEditor: ['academicEditorAssignedEditorialType'].includes(
        manuscriptStatus,
      ),
      triageEditor: ['pendingApproval'].includes(manuscriptStatus),
      editorialAssistant: ['pendingApproval'].includes(manuscriptStatus),
      researchIntegrityPublishingEditor: ['pendingApproval'].includes(
        manuscriptStatus,
      ),
      admin: ['pendingApproval'].includes(manuscriptStatus),
    },
  }

  return get(highlightedOption, `${action}.${role}`, false)
}
