const {
  getTeamRoles,
  generateJournal,
  generateManuscript,
  getManuscriptStatuses,
} = require('component-generators')

const useCases = require('../src/useCases')

const { handleRejectedSubmissionUseCase } = useCases

interface ManuscriptType {
  find(): Promise<object>
  Statuses: object
  findLastManuscriptBySubmissionId(): Promise<object>
  getFinalRevisionDate(): Promise<object>
}

const Manuscript: ManuscriptType = {
  find: jest.fn(),
  findLastManuscriptBySubmissionId: jest.fn(),
  getFinalRevisionDate: jest.fn(),
  Statuses: getManuscriptStatuses(),
}

const models = {
  Team: {
    Role: getTeamRoles(),
  },
  Manuscript,
}

const applicationEventBus = {
  publishMessage: jest.fn(),
}

const mockedUseCases = {
  getDataForSubmittedManuscriptUseCase: {
    initialize: () => ({
      execute: () => ({}),
    }),
  },
  getDataForPeerReviewedManuscriptUseCase: {
    initialize: () => ({
      execute: () => ({}),
    }),
  },
  getFilesForFtpUploadUseCase: {
    initialize: () => ({
      execute: () => ({}),
    }),
  },
}

jest.mock('../src/eventMapper', () => ({
  createMTSRejectedEventData: jest.fn(),
}))

describe('handle Submission Rejected event use case', () => {
  it('sends the MTS event if the data is correct', async () => {
    const journal = generateJournal()
    const manuscript = generateManuscript({
      journal,
    })

    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce(manuscript)

    await handleRejectedSubmissionUseCase
      .initialize({
        models,
        useCases: mockedUseCases,
        applicationEventBus,
      })
      .execute({
        manuscripts: [manuscript],
      })
    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(1)
  })
  it('throws an error if manuscript is not found', async () => {
    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce(undefined)
    const result = handleRejectedSubmissionUseCase
      .initialize({
        models,
        useCases: mockedUseCases,
        applicationEventBus,
      })
      .execute({
        manuscripts: [{ id: 'some-id' }],
      })

    await expect(result).rejects.toThrowError(
      'Manuscript with id some-id not found',
    )
  })
})
