const createPpEventData = ({
  authors,
  reviews,
  screeners,
  manuscript,
  figureFiles,
  academicEditor,
  editorialAssistant,
  submittingStaffMember,
  correspondingEditorialAssistant,
  finalRevisionDate,
}) => {
  const reviewMapFunc = review => ({
    id: review.id,
    submitted: review.submitted,
    recommendation: review.recommendation,
    member: review.member
      ? {
          created: review.member.created,
          alias: review.member.alias
            ? {
                aff: review.member.alias.aff,
                email: review.member.alias.email,
                title: review.member.alias.title,
                country: review.member.alias.country,
                surname: review.member.alias.surname,
                givenNames: review.member.alias.givenNames,
              }
            : undefined,
          responded: review.member.responded,
        }
      : undefined,
    comments: review.comments.map(comment => ({
      type: comment.type,
      content: comment.content,
      files: comment.files.map(file => ({
        type: file.type,
        fileName: file.fileName,
        originalName: file.originalName,
      })),
    })),
  })

  const identitiesMapFunc = identity => ({
    type: identity.type,
    identifier: identity.identifier,
    updated: identity.updated,
  })

  return {
    finalRevisionDate,
    submissionQualityCheckPassedDate: new Date(),
    authors: authors.map(author => ({
      id: author.id,
      isSubmitting: author.isSubmitting,
      isCorresponding: author.isCorresponding,
      alias: {
        aff: author.alias?.aff,
        email: author.alias?.email,
        title: author.alias?.title,
        country: author.alias?.country,
        surname: author.alias?.surname,
        givenNames: author.alias?.givenNames,
      },
      user: {
        identities: author.user?.identities.map(identitiesMapFunc),
      },
    })),
    reviews: {
      adminReviews: reviews.adminReviews.map(reviewMapFunc),
      authorReviews: reviews.authorReviews.map(reviewMapFunc),
      reviewerReviews: reviews.reviewerReviews.map(reviewMapFunc),
      triageEditorReviews: reviews.triageEditorReviews.map(reviewMapFunc),
      academicEditorReviews: reviews.academicEditorReviews.map(reviewMapFunc),
      editorialAssistantReviews: reviews.editorialAssistantReviews.map(
        reviewMapFunc,
      ),
    },
    screeners: screeners.map(screener => ({
      surname: screener.surname,
      givenNames: screener.givenNames,
      email: screener.email,
      role: screener.role,
    })),
    manuscript: {
      id: manuscript.id,
      updated: manuscript.updated,
      created: manuscript.created,
      title: manuscript.title,
      abstract: manuscript.abstract,
      customId: manuscript.customId,
      version: manuscript.version,
      dataAvailability: manuscript.dataAvailability,
      fundingStatement: manuscript.fundingStatement,
      conflictOfInterest: manuscript.conflictOfInterest,
      submittedDate: manuscript.submittedDate,
      preprintValue: manuscript.preprintValue,
      peerReviewPassedDate: manuscript.peerReviewPassedDate,
      submissionId: manuscript.submissionId,
      section: {
        name: manuscript.section?.name,
      },
      articleType: {
        name: manuscript.articleType?.name,
      },
      sourceJournal: manuscript.sourceJournal
        ? {
            name: manuscript.sourceJournal.name,
          }
        : undefined,
      journal: manuscript.journal
        ? {
            id: manuscript.journal.id,
            name: manuscript.journal.name,
            // publisherName: null,
            code: manuscript.journal.code,
            email: manuscript.journal.email,
            issn: manuscript.journal.issn,
            journalPreprints: manuscript.journal.journalPreprints.map(
              journalPreprint => ({
                preprint: {
                  type: journalPreprint.preprint.type,
                },
              }),
            ),
          }
        : undefined,
      specialIssue: manuscript.specialIssue
        ? {
            id: manuscript.specialIssue.id,
            name: manuscript.specialIssue.name,
            isActive: manuscript.specialIssue.isActive,
            isCancelled: manuscript.specialIssue.isCancelled,
            customId: manuscript.specialIssue.customId,
            cancelReason: manuscript.specialIssue.cancelReason,
          }
        : undefined,
      files: manuscript.files.map(file => ({
        type: file?.type,
        fileName: file?.fileName,
        originalName: file?.originalName,
        providerKey: file?.providerKey,
      })),
    },
    figureFiles,
    academicEditor: academicEditor
      ? {
          id: academicEditor.id,
          alias: academicEditor.alias
            ? {
                aff: academicEditor.alias.aff,
                email: academicEditor.alias.email,
                title: academicEditor.alias.title,
                country: academicEditor.alias.country,
                surname: academicEditor.alias.surname,
                givenNames: academicEditor.alias.givenNames,
              }
            : undefined,
          user: {
            identities: academicEditor.user?.identities?.map(identitiesMapFunc),
          },
        }
      : undefined,
    editorialAssistant: editorialAssistant
      ? {
          alias: {
            aff: editorialAssistant.alias?.aff,
            email: editorialAssistant.alias?.email,
            title: editorialAssistant.alias?.title,
            country: editorialAssistant.alias?.country,
            surname: editorialAssistant.alias?.surname,
            givenNames: editorialAssistant.alias?.givenNames,
          },
        }
      : undefined,
    submittingStaffMember: submittingStaffMember
      ? {
          alias: {
            aff: submittingStaffMember.alias?.aff,
            email: submittingStaffMember.alias?.email,
            title: submittingStaffMember.alias?.title,
            country: submittingStaffMember.alias?.country,
            surname: submittingStaffMember.alias?.surname,
            givenNames: submittingStaffMember.alias?.givenNames,
          },
        }
      : undefined,

    correspondingEditorialAssistant: correspondingEditorialAssistant
      ? {
          alias: {
            aff: correspondingEditorialAssistant.alias?.aff,
            email: correspondingEditorialAssistant.alias?.email,
            title: correspondingEditorialAssistant.alias?.title,
            country: correspondingEditorialAssistant.alias?.country,
            surname: correspondingEditorialAssistant.alias?.surname,
            givenNames: correspondingEditorialAssistant.alias?.givenNames,
          },
        }
      : undefined,
  }
}

export { createPpEventData }
