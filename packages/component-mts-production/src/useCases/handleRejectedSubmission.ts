const BluebirdPromise = require('bluebird')
const eventMapper = require('../eventMapper')
const { logger } = require('component-logger')

interface EventManuscript {
  id: string
}

interface EventData {
  manuscripts: Array<EventManuscript>
}

const initialize = ({ useCases, models, applicationEventBus }) => ({
  async execute(data: EventData) {
    const { Manuscript } = models

    await BluebirdPromise.each(
      data.manuscripts,
      async (eventManuscript: EventManuscript) => {
        const manuscriptId = eventManuscript.id

        const manuscript = await Manuscript.find(
          manuscriptId,
          '[journal.journalPreprints.preprint, articleType, section, specialIssue, files]',
        )
        if (!manuscript)
          throw new Error(`Manuscript with id ${manuscriptId} not found`)

        const {
          authors,
          editorialAssistant,
          correspondingEditorialAssistant,
        } = await useCases.getDataForSubmittedManuscriptUseCase
          .initialize(models)
          .execute({
            manuscriptId,
            journalId: manuscript.journalId,
          })

        const {
          academicEditor,
          reviews,
        } = await useCases.getDataForPeerReviewedManuscriptUseCase
          .initialize(models)
          .execute(manuscriptId)

        const finalRevisionDate = await Manuscript.getFinalRevisionDate({
          submissionId: manuscript.submissionId,
        })

        try {
          const mtsEventData = eventMapper.createMTSRejectedEventData({
            authors,
            reviews,
            manuscript,
            academicEditor,
            finalRevisionDate,
            editorialAssistant,
            correspondingEditorialAssistant,
          })

          await applicationEventBus.publishMessage({
            event: 'MTSRejectedArchiveRequested',
            data: mtsEventData,
          })
        } catch (e) {
          logger.warn(
            `Error creating MTSRejectedArchiveRequested event. Cause: ${e.message}`,
          )
        }
      },
    )
  },
})

export { initialize }
