const BluebirdPromise = require('bluebird')
const { logger } = require('component-logger')
const eventMapper = require('../eventMapper')

interface EventData {
  manuscripts: Array<EventManuscript>
  submissionId: string
  qc: object
  qcLeaders: Array<object>
  es: object
  esLeaders: Array<object>
}

interface EventManuscript {
  id: string
}

const initialize = ({ useCases, models, applicationEventBus }) => ({
  async execute(data: EventData) {
    const { Manuscript } = models

    const { qc, qcLeaders = [], es, esLeaders = [] } = data
    const screeners = [
      ...qcLeaders.map(e => ({ ...e, role: 'qcLeader' })),
      ...esLeaders.map(e => ({ ...e, role: 'esLeader' })),
      ...(qc ? [{ ...qc, role: 'qc' }] : []),
      ...(es ? [{ ...es, role: 'es' }] : []),
    ]

    await BluebirdPromise.each(
      data.manuscripts,
      async (eventManuscript: EventManuscript) => {
        const manuscriptId = eventManuscript.id

        const manuscript = await Manuscript.find(
          manuscriptId,
          '[journal.journalPreprints.preprint, articleType, section, specialIssue, files, sourceJournal]',
        )
        if (!manuscript)
          throw new Error(`Manuscript with id ${manuscriptId} not found`)

        const {
          authors,
          editorialAssistant,
          submittingStaffMember,
          correspondingEditorialAssistant,
        } = await useCases.getDataForSubmittedManuscriptUseCase
          .initialize(models)
          .execute({
            manuscriptId,
            journalId: manuscript.journalId,
          })

        const {
          academicEditor,
          reviews,
        } = await useCases.getDataForPeerReviewedManuscriptUseCase
          .initialize(models)
          .execute(manuscriptId)

        const {
          figureFiles,
        } = await useCases.getFilesForFtpUploadUseCase
          .initialize({ models })
          .execute(manuscriptId)

        const finalRevisionDate = await Manuscript.getFinalRevisionDate({
          submissionId: manuscript.submissionId,
        })

        try {
          const mtsEventData = eventMapper.createMTSAcceptedEventData({
            authors,
            reviews,
            screeners,
            manuscript,
            academicEditor,
            finalRevisionDate,
            editorialAssistant,
            correspondingEditorialAssistant,
          })

          await applicationEventBus.publishMessage({
            event: 'MTSAcceptedArchiveRequested',
            data: mtsEventData,
          })
        } catch (e) {
          logger.warn(
            `Error creating MTSAcceptedArchiveRequested event. Cause: ${e.message}`,
          )
        }

        if (manuscript.isLatestVersion) {
          try {
            if (!manuscript.peerReviewPassedDate) {
              const peerReviewPassedDate = await Manuscript.getPeerReviewPassedDate(
                {
                  submissionId: manuscript.submissionId,
                },
              )
              manuscript.peerReviewPassedDate = peerReviewPassedDate

              logger.info(
                `The peerReviewPassedDate was not found for the manuscript ${manuscript.customId}. The Production package event was updated with ${manuscript.peerReviewPassedDate} found in the previous entries for this submission.`,
              )
            }

            const ppEventData = eventMapper.createPpEventData({
              authors,
              reviews,
              screeners,
              manuscript,
              figureFiles,
              academicEditor,
              editorialAssistant,
              submittingStaffMember,
              correspondingEditorialAssistant,
              finalRevisionDate,
            })

            await applicationEventBus.publishMessage({
              event: 'ProductionPackageQcArchiveRequested',
              data: ppEventData,
            })
          } catch (e) {
            logger.warn(
              `Error creating ProductionPackageQcArchiveRequested event. Cause: ${e.message}`,
            )
          }
        }
      },
    )
  },
})

export { initialize }
