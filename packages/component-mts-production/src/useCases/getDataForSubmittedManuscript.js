const initialize = ({ TeamMember, Team }) => ({
  async execute({ manuscriptId, journalId }) {
    const correspondingEditorialAssistant = await TeamMember.findCorrespondingEditorialAssistant(
      journalId,
    )

    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )

    const submittingStaffMember = await TeamMember.findOneByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.submittingStaffMember,
    })

    const authors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.author,
      eagerLoadRelations: 'user.identities',
    })

    return {
      correspondingEditorialAssistant,
      editorialAssistant,
      submittingStaffMember,
      authors,
    }
  },
})

module.exports = {
  initialize,
}
