const models = require('@pubsweet/models')
const { logger } = require('component-logger')

const useCases = require('./useCases')
const eventMapper = require('./eventMapper')

module.exports = {
  async SubmissionSubmitted(data) {
    if (!data.manuscripts || !data.manuscripts.length)
      throw new Error(`Manuscripts not found on SubmissionSubmitted event`)

    const manuscriptId = data.manuscripts[0].id
    if (!manuscriptId) {
      throw new Error(
        'SubmissionSubmitted event could not be processed due to missing manuscript id',
      )
    }

    const { Manuscript } = models
    // TO DO: check if special issue sections are needed
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[journal.journalPreprints.preprint, articleType, section, specialIssue, files]',
    )
    if (!manuscript)
      throw new Error(`Manuscript with id ${manuscriptId} not found`)

    const {
      authors,
      editorialAssistant,
      correspondingEditorialAssistant,
    } = await useCases.getDataForSubmittedManuscriptUseCase
      .initialize(models)
      .execute({
        manuscriptId,
        journalId: manuscript.journalId,
      })

    try {
      const mtsEventData = eventMapper.createMTSSubmissionSubmittedEventData({
        authors,
        manuscript,
        editorialAssistant,
        correspondingEditorialAssistant,
      })

      await applicationEventBus.publishMessage({
        event: 'MTSSubmissionSubmittedArchiveRequested',
        data: mtsEventData,
      })
    } catch (e) {
      logger.warn(
        `Error creating MTSSubmissionSubmittedArchiveRequested event. Cause: ${e.message}`,
      )
    }
  },
  async SubmissionRejected(data) {
    await useCases.handleRejectedSubmissionUseCase
      .initialize({ models, useCases, applicationEventBus })
      .execute(data)
  },
}
