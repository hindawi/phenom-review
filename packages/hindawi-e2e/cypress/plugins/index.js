/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */

const uuid = require('uuid')
const Promise = require('bluebird')
const atob = require('atob')
const { Client } = require('pg')

const { prms } = require('../fixtures/models/prms')
const { allUsers, admins } = require('../fixtures/models/users')
const { getUsersForJournal } = require('../fixtures/mappings/users-to-journals')

const getDBConfig = config => ({
  user: config.env.dbConfig.user,
  host: config.env.dbConfig.host,
  database: config.env.dbConfig.database,
  password: config.env.dbConfig.password,
  port: 5432,
  publisherName: config.env.publisher,
})

module.exports = (on, config) => {
  const passwordHash = atob(config.env.passwordHashBase64)

  on('task', {
    log(message) {
      // eslint-disable-next-line no-console
      console.log(message)
      return null
    },

    async addUsers() {
      const qaClient = new Client(getDBConfig(config))
      await qaClient.connect()

      for (const user of allUsers) {
        await createUser(qaClient, {
          email: getUserEmail(user),
          givenName: user.firstName,
          surname: user.lastName,
        })
      }

      for (const admin of admins) {
        await assignUserAsAdmin(qaClient, admin)
      }

      await qaClient.end()
      return true
    },

    async addJournals(publisherName) {
      const qaClient = new Client(getDBConfig(config))
      await qaClient.connect()

      const publisherPrms = prms[publisherName]
      if (!publisherPrms) {
        throw new Error(
          `No peer review models found for ${publisherName} publisher`,
        )
      }

      for (const prm in publisherPrms) {
        if (Object.prototype.hasOwnProperty.call(publisherPrms, prm)) {
          const journals = publisherPrms[prm]

          for (const journal of journals) {
            await addJournal(qaClient, journal, prm)
          }
        }
      }

      await qaClient.end()
      return true
    },
  })

  const createUser = async (qaClient, { email, givenName, surname }) => {
    const userId = uuid.v4()

    await qaClient.query(
      `INSERT INTO "user"(id, default_identity_type, is_subscribed_to_emails, unsubscribe_token, agree_tc, is_active)
        VALUES ('${userId}',  'local', true, '${uuid.v4()}', true, true)`,
    )
    await qaClient.query(
      `INSERT INTO identity(user_id, type, is_confirmed, email, given_names, surname, password_hash, title, aff, country)
        VALUES ('${userId}', 'local', true, '${email}', '${givenName}', '${surname}', '${passwordHash}', 'mrs', 'Hindawi Research', 'RO')`,
    )
  }

  const assignUserAsAdmin = async (qaClient, user) => {
    const teamRes = await qaClient.query(
      `SELECT * FROM team WHERE role = 'admin'`,
    )
    const identityRes = await qaClient.query(
      `SELECT * FROM identity WHERE email = '${getUserEmail(user)}'`,
    )

    if (teamRes.rows.length > 0 && identityRes.rows.length > 0) {
      const dbAdminTeam = teamRes.rows[0]
      const dbUser = identityRes.rows[0]

      await qaClient.query(
        `INSERT INTO team_member(user_id, team_id, status, alias)
        VALUES('${dbUser.user_id}', '${dbAdminTeam.id}', 'pending' , '{
          "aff": "Hindawi Research",
          "email": "${dbUser.email}",
          "title": "mrs",
          "country": "RO",
          "surname": "${dbUser.surname}",
          "givenNames": "${dbUser.given_names}"
        }')`,
      )
    }
  }

  const addJournal = async (qaClient, journal, peerReviewModel) => {
    const prm = await qaClient.query(
      `SELECT id FROM peer_review_model WHERE name = '${peerReviewModel}';`,
    )

    await qaClient.query(
      `INSERT INTO journal(name, code, email, apc, is_active, peer_review_model_id)
            VALUES ('${journal.name}', '${journal.code}', '${journal.email}', '${journal.apc}', ${journal.isActive}, '${prm.rows[0].id}')`,
    )

    const dbJournal = await qaClient.query(
      `SELECT * FROM journal WHERE name = '${journal.name}'`,
    )

    await assignArticleType(qaClient, dbJournal.rows[0].id)
    await assignUsersToJournal(qaClient, dbJournal.rows[0])
  }

  const assignUsersToJournal = async (qaClient, journal) => {
    const users = getUsersForJournal(journal.name)

    for (const user of users) {
      const response = await qaClient.query(
        `SELECT * FROM identity WHERE email = '${getUserEmail(user)}'`,
      )

      if (response.rows.length > 0) {
        const dbUser = response.rows[0]

        await assignUserToJournal(qaClient, journal.id, {
          userId: dbUser.user_id,
          email: dbUser.email,
          surname: dbUser.surname,
          givenName: dbUser.given_names,
          role: user.role,
        })
      }
    }
  }

  const assignUserToJournal = async (
    qaClient,
    journalId,
    { userId, email, surname, givenName, role },
  ) => {
    const teamId = uuid.v4()

    await qaClient.query(
      `INSERT INTO team(id, role, journal_id)
        VALUES('${teamId}', '${role}', '${journalId}')`,
    )
    await qaClient.query(
      `INSERT INTO team_member(user_id, team_id, status, alias)
        VALUES('${userId}', '${teamId}', 'accepted' , '{
          "aff": "Hindawi Research",
          "email": "${email}",
          "title": "mrs",
          "country": "RO",
          "surname": "${surname}",
          "givenNames": "${givenName}"
        }')`,
    )
  }

  const assignArticleType = async (qaClient, journalId) => {
    const article_ids = await qaClient.query(`SELECT id FROM article_type;`)

    await Promise.each(article_ids.rows, async article => {
      await qaClient.query(`INSERT INTO journal_article_type(journal_id, article_type_id)
        VALUES('${journalId}', '${article.id}')`)
    })
  }

  const getUserEmail = user => config.env.emailPrefix + user.email
}
