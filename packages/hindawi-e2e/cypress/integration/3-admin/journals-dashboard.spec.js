import { ADMIN, AE } from '../../fixtures/models/users'
import { prmTypes } from '../../fixtures/models/prms'
import { articleTypes } from '../../fixtures/models/article-types'
import { registerGQLInterceptor } from '../../support/interceptors'
import {
  addJournal,
  editJournal,
  assignEditorialRole,
  navigateToJournalsDashboard,
  navigateToJournalDetailsPage,
} from '../../support/admin'
import { generateRandomEmail, getRandomInt, getFullEmail } from '../../utils'

describe('Journals Dashboard', () => {
  beforeEach(() => {
    cy.logoutKeycloak()
    registerGQLInterceptor()
  })

  it('admin can add, edit journal and assign editorial role', () => {
    const randomId = getRandomInt(100000)
    const journalCode = `new-journal-${randomId}`

    cy.loginKeycloak(ADMIN.email)
    navigateToJournalsDashboard()

    addJournal({
      name: `Test New Journal ${randomId}`,
      code: journalCode,
      email: generateRandomEmail(),
      issn: '2049-3630',
      peerReviewModel: prmTypes.CHIEF_MINUS,
      active: false,
      articleTypes: [
        articleTypes.RESEARCH_ARTICLE,
        articleTypes.REVIEW_ARTICLE,
      ],
    })
    editJournal(journalCode, {
      name: `Test New Journal ${randomId}`,
      code: journalCode,
      email: generateRandomEmail(),
      issn: '2049-3630',
      peerReviewModel: prmTypes.SINGLE_TIER,
      active: true,
      articleTypes: [articleTypes.EDITORIAL],
    })

    navigateToJournalDetailsPage(journalCode)
    assignEditorialRole('Academic Editor', getFullEmail(AE.email))
  })
})
