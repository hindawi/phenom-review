import { ASTROLOGY } from '../../fixtures/models/journals'
import { AE } from '../../fixtures/models/users'

describe('User starts submission from url', () => {
  beforeEach(() => {
    cy.logoutKeycloak()
  })

  it('journal is succesfully preselected when code is in url', () => {
    cy.loginKeycloak(AE.email)
    cy.visit(`/submit?journal=${ASTROLOGY.code}`)
    cy.get('#downshift-0-input').should('have.value', ASTROLOGY.name)
  })

  it('journal is not preselected if journal with specific code is not found', () => {
    cy.loginKeycloak(AE.email)
    cy.visit('/submit?journal=SOMECODE')
    cy.get('#downshift-0-input').should('have.value', '')
  })
})
