import { FUZZY_SYSTEMS } from '../../fixtures/models/journals'
import {
  CE,
  AE,
  REVIEWER1,
  REVIEWER2,
  AUTHOR1,
} from '../../fixtures/models/users'
import {
  inviteReviewer,
  acceptInvitationAsAE,
  acceptInvitationAsReviewer,
  navigateToManuscript,
  submitReview,
  recommendMajorRevision,
  recommendMinorRevision,
  recommendPublish,
  makeDecisionToPublish,
  inviteAcademicEditor,
} from '../../support/manuscript'
import {
  expandSubmitRevisionSection,
  updateManuscriptTitle,
  updateManuscriptAbstract,
  deleteMainManuscript,
  uploadMainManuscript,
  addComment,
  confirmRevision,
} from '../../support/manuscript/revision'
import { stopCypressIfTestFailed } from '../../utils'
import { registerGQLInterceptor } from '../../support/interceptors'

describe('Chief Minus Main Flow', () => {
  beforeEach(() => {
    cy.logoutKeycloak()
    registerGQLInterceptor()
  })

  afterEach(function() {
    stopCypressIfTestFailed(this.currentTest)
  })

  it('1. Create Submission via API', () => {
    cy.createSubmission({ journalName: FUZZY_SYSTEMS.name })
  })

  it('2. CE invites Academic Editor', () => {
    cy.loginKeycloak(CE.email)
    navigateToManuscript()
    inviteAcademicEditor(AE.email)
  })

  it('3. AE accepts invitation and invites reviewer', () => {
    cy.loginKeycloak(AE.email)
    navigateToManuscript()
    acceptInvitationAsAE()
    inviteReviewer(REVIEWER1)
  })

  it('4. Reviewer accepts invitation and submits review for major revision', () => {
    cy.loginKeycloak(REVIEWER1.email)
    navigateToManuscript()
    acceptInvitationAsReviewer()
    submitReview('This is rubbish', 'Major Revision')
  })

  it('5. AE makes Major Revision recommendation', () => {
    cy.loginKeycloak(AE.email)
    navigateToManuscript()
    recommendMajorRevision('This looks bad')
  })

  it('6. Author submits revision', () => {
    cy.loginKeycloak(AUTHOR1.email)
    navigateToManuscript()
    expandSubmitRevisionSection()
    updateManuscriptTitle('[Auto] manuscript v2')
    updateManuscriptAbstract('This is version 2')
    deleteMainManuscript('manuscript.txt')
    uploadMainManuscript('manuscript.txt')
    addComment("Here's another revision")
    confirmRevision()
  })

  it('7. Reviewer accepts invitation and submits review for minor revision', () => {
    cy.loginKeycloak(REVIEWER1.email)
    navigateToManuscript()
    acceptInvitationAsReviewer()
    submitReview('This is better but not quite', 'Minor Revision')
  })

  it('8. AE makes Minor Revision recommendation', () => {
    cy.loginKeycloak(AE.email)
    navigateToManuscript()
    recommendMinorRevision('Needs changes')
  })

  it('9. Author submits another revision', () => {
    cy.loginKeycloak(AUTHOR1.email)
    navigateToManuscript()
    expandSubmitRevisionSection()
    updateManuscriptTitle('[Auto] manuscript v3')
    updateManuscriptAbstract('This is version 3')
    addComment('Hopefully this is the end of this')
    confirmRevision()
  })

  it('10. AE invites another reviewer for latest version', () => {
    cy.loginKeycloak(AE.email)
    navigateToManuscript()
    inviteReviewer(REVIEWER2)
  })

  it('11. Reviewer accepts invitation and submits review for publish', () => {
    cy.loginKeycloak(REVIEWER2.email)
    navigateToManuscript()
    acceptInvitationAsReviewer()
    submitReview('Awesome', 'Publish')
  })

  it('12. AE makes recommendation to publish', () => {
    cy.loginKeycloak(AE.email)
    navigateToManuscript()
    recommendPublish()
  })

  it('13. CE makes decision to publish', () => {
    cy.loginKeycloak(CE.email)
    navigateToManuscript()
    makeDecisionToPublish()
  })
})
