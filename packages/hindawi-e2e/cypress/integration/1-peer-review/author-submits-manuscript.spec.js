import { AUTHOR1 } from '../../fixtures/models/users'
import { ASTROLOGY } from '../../fixtures/models/journals'
import { articleTypes } from '../../fixtures/models/article-types'
import {
  beginSubmission,
  selectJournal,
  clickNextBtn,
  agreeTermsAndConditions,
  addManuscriptTitle,
  selectManuscriptType,
  addManuscriptAbstract,
  addAuthor,
  declareConflictOfInterest,
  addDataAvailabilityStatement,
  addFundingStatement,
  uploadMainManuscript,
  uploadCoverLetter,
  uploadSupplementalFile,
  confirmSubmission,
} from '../../support/manuscript/submission'
import { registerGQLInterceptor } from '../../support/interceptors'
import { generateRandomEmail } from '../../utils'

describe('Submission Wizard', () => {
  beforeEach(() => {
    cy.logoutKeycloak()
    registerGQLInterceptor()
  })

  it('author submits manuscript', () => {
    cy.loginKeycloak(AUTHOR1.email)
    cy.visit('/')

    beginSubmission()

    selectJournal(ASTROLOGY.name)
    clickNextBtn()

    agreeTermsAndConditions()
    clickNextBtn()

    addManuscriptTitle('[Auto] manuscript')
    selectManuscriptType(articleTypes.RESEARCH_ARTICLE)
    addManuscriptAbstract('Automatic checks')
    addAuthor({
      firstName: 'Authorel',
      lastName: 'Authoricescu',
      email: generateRandomEmail(),
      country: 'Isle of Man',
      affiliation: 'FiveFinger',
    })
    declareConflictOfInterest(true, 'The AcademicEditor1 is my friend')
    addDataAvailabilityStatement('Wrong side of heaven')
    addFundingStatement('The Righteous Side Of Hell')
    clickNextBtn()

    uploadMainManuscript('manuscript.txt')
    uploadCoverLetter('cover-letter.txt')
    uploadSupplementalFile('supplemental.txt')

    confirmSubmission()
  })
})
