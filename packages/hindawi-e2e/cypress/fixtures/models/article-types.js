const ARTICLE_TYPES = Object.freeze({
  COMMENTARY: 'Commentary',
  EDITORIAL: 'Editorial',
  RETRACTION: 'Retraction',
  CASE_SERIES: 'Case Series',
  CASE_REPORT: 'Case Report',
  REVIEW_ARTICLE: 'Review Article',
  RESEARCH_ARTICLE: 'Research Article',
  LETTER_TO_THE_EDITOR: 'Letter to the Editor',
  EXPRESSION_OF_CONCERN: 'Expression of Concern',
  ERRATUM: 'Erratum',
  CORRIGENDUM: 'Corrigendum',
})

module.exports = {
  articleTypes: ARTICLE_TYPES,
}
