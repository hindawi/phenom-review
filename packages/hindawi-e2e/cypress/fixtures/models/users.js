const ADMIN = {
  username: 'admin@hindawi.com',
  email: 'admin@hindawi.com',
  firstName: 'testAdmin',
  lastName: 'testAdmin',
  affiliation: 'TST',
  country: 'Australia',
  title: 'Professor',
  role: 'admin',
  isAdmin: true,
}

const EA = {
  firstName: 'EditorialAssistantTest',
  lastName: 'editorialAssistant',
  affiliation: 'Automatic checks',
  email: 'ea@hindawi.com',
  username: 'ea@hindawi.com',
  country: 'Australia',
  role: 'editorialAssistant',
  title: 'Professor',
}

const AE = {
  username: 'ae@hindawi.com',
  email: 'ae@hindawi.com',
  firstName: 'AcademicEditorTest',
  lastName: 'academicEditor',
  affiliation: 'Automatic checks',
  country: 'Australia',
  role: 'academicEditor',
  title: 'Professor',
}

const TE = {
  username: 'te@hindawi.com',
  email: 'te@hindawi.com',
  firstName: 'TriageEditorTest',
  lastName: 'triageEditor',
  affiliation: 'Automatic checks',
  country: 'Australia',
  role: 'triageEditor',
  title: 'Professor',
}

const CE = {
  username: 'ce@hindawi.com',
  email: 'ce@hindawi.com',
  firstName: 'ChiefEditorTest',
  lastName: 'chiefEdiotr',
  affiliation: 'Automatic checks',
  country: 'Australia',
  role: 'triageEditor',
  title: 'Professor',
}

const SE = {
  username: 'se@hindawi.com',
  email: 'se@hindawi.com',
  firstName: 'SectionEditorTest',
  lastName: 'sectionEditor',
  affiliation: 'Automatic checks',
  country: 'Australia',
  role: 'triageEditor',
  title: 'Doctor',
}

const AssE = {
  username: 'asse@hindawi.com',
  email: 'asse@hindawi.com',
  firstName: 'AssociateEditorTest',
  lastName: 'associateEditor',
  affiliation: 'Automatic checks',
  country: 'Australia',
  role: 'triageEditor',
  title: 'Professor',
}

const GE = {
  username: 'ge@hindawi.com',
  email: 'ge@hindawi.com',
  firstName: 'GuestEditorTest',
  lastName: 'GuestEditor',
  affiliation: 'Automatic checks',
  country: 'Australia',
  role: 'academicEditor',
  title: 'Professor',
}

const LGE = {
  username: 'lge@hindawi.com',
  email: 'lge@hindawi.com',
  firstName: 'LeadGuestEditorTest',
  lastName: 'leadRuestEditor',
  affiliation: 'Automatic checks',
  country: 'Australia',
  role: 'triageEditor',
  title: 'Professor',
}

const REVIEWER1 = {
  firstName: 'ReviewerTest1',
  lastName: 'Reviewer1',
  affiliation: 'Automatic checks',
  email: 'rev1@hindawi.com',
  country: 'Australia',
  role: 'reviewer',
}

const REVIEWER2 = {
  firstName: 'ReviewerTest2',
  lastName: 'Reviewer2',
  affiliation: 'Automatic checks',
  email: 'rev2@hindawi.com',
  country: 'Australia',
  role: 'reviewer',
}

const REVIEWER3 = {
  firstName: 'ReviewerTest3',
  lastName: 'Reviewer3',
  affiliation: 'Automatic checks',
  email: 'rev3@hindawi.com',
  country: 'Australia',
  role: 'reviewer',
}

const AUTHOR1 = {
  firstName: 'AuthorTest',
  lastName: 'Author',
  affiliation: 'Automatic checks',
  email: 'a1@hindawi.com',
  username: 'a1@hindawi.com',
  country: 'Australia',
  title: 'Professor',
  role: 'author',
}

const admins = [ADMIN]
const roleUsers = [EA, AE, CE, TE, SE, AssE, GE, LGE]
const authors = [AUTHOR1]
const reviewers = [REVIEWER1, REVIEWER2, REVIEWER3]
const allUsers = [...admins, ...roleUsers, ...authors, ...reviewers]

module.exports = {
  allUsers,
  admins,
  roleUsers,
  reviewers,
  authors,

  ADMIN,
  EA,
  AE,
  CE,
  TE,
  SE,
  AssE,
  GE,
  LGE,
  REVIEWER1,
  REVIEWER2,
  REVIEWER3,
  AUTHOR1,
}
