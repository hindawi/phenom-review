const ASTRONOMY = {
  name: 'Advances in Astronomy',
  code: 'AA',
  issn: '9830-1020',
  publisherName: 'Hindawi LTD',
  apc: 1000,
  email: 'hindawiqa+aa@hindawi.com',
  isActive: true,
}

const FUZZY_SYSTEMS = {
  name: 'Advances in Fuzzy Systems',
  code: 'fuz',
  issn: '1120-2001',
  publisherName: 'Hindawi LTD',
  apc: 500,
  email: 'hindawiqa+fuz@hindawi.com',
  isActive: true,
}

const ASTROLOGY = {
  name: 'Advances in Astrology',
  code: 'AAA',
  issn: '5123-3002',
  publisherName: 'Hindawi LTD',
  apc: 2000,
  email: 'hindawiqa+aaa@hindawi.com',
  isActive: true,
}

const METEOROLOGY = {
  name: 'Advances in Meteorology',
  code: 'meteo',
  issn: '1231-4001',
  publisherName: 'Hindawi LTD',
  apc: 300,
  email: 'hindawiqa+met@hindawi.com',
  isActive: true,
}

const LITHOSPHERE = {
  name: 'Lithosphere Model Journal',
  code: 'Lithosphere',
  prm: 'Lithosphere Model',
  issn: '1231-4001',
  publisherName: 'GSW',
  apc: 1300,
  email: 'hindawiqa+lithosphere@hindawi.com',
  isActive: true,
}

const journals = [ASTRONOMY, FUZZY_SYSTEMS, ASTROLOGY, METEOROLOGY, LITHOSPHERE]

module.exports = {
  journals,
  ASTRONOMY,
  FUZZY_SYSTEMS,
  ASTROLOGY,
  METEOROLOGY,
  LITHOSPHERE,
}
