const { EA, AE, CE } = require('../models/users')
const { ASTROLOGY, FUZZY_SYSTEMS } = require('../models/journals')

const usersToJournal = {
  [ASTROLOGY.name]: [EA, AE],
  [FUZZY_SYSTEMS.name]: [EA, CE, AE],
}

const getUsersForJournal = name => usersToJournal[name] || []

module.exports = {
  getUsersForJournal,
}
