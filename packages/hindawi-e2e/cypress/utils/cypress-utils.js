// Some of the flows require multiple login/logout actions but https://www.npmjs.com/package/cypress-keycloak
// recommends using logout/login on separate `it` tests (seems to not work in a single test).
// We are then forced to use a separate test for each swich of role
// Because if one test fails cypress doesn't stop running through the entire list of tests
// we have to stop the remaining tests.
// Unfortunately we cannot stop only one spec file, we have to stop the entire runner
// see open issue: https://github.com/cypress-io/cypress/issues/518
export const stopCypressIfTestFailed = test => {
  if (test.state === 'failed') {
    Cypress.runner.stop()
  }
}
