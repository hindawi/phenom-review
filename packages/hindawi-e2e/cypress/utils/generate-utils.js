export const generateRandomEmail = () =>
  `${Cypress.env('emailPrefix') +
    Math.random()
      .toString(22)
      .substring(8)}@hindawi.com`

export const getRandomInt = max => Math.floor(Math.random() * max)
