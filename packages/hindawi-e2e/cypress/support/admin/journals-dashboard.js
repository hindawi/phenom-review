export const addJournal = ({
  name,
  code,
  email,
  issn,
  peerReviewModel,
  active,
  articleTypes,
}) => {
  clickAddJournalBtn()
  addJournalName(name)
  addJournalCode(code)
  addEmail(email)
  if (issn) addISSN(issn)
  assignPeerReviewModel(peerReviewModel)
  active ? activateSubmissions() : dezactivateSubmissions()
  assignArticleTypes(articleTypes)
  confirmAddJournal()
}

export const editJournal = (
  journalCode,
  { name, code, email, issn, apc, peerReviewModel, active, articleTypes },
) => {
  startEditJournal(journalCode)
  addJournalName(name)
  addJournalCode(code)
  addEmail(email)
  if (issn) addISSN(issn)
  assignPeerReviewModel(peerReviewModel)
  active ? activateSubmissions() : dezactivateSubmissions()
  assignArticleTypes(articleTypes)
  confirmAddJournal()
}

const clickAddJournalBtn = () => {
  cy.get('[data-test-id="add-journal"]').click()
}

const addJournalName = name => {
  cy.get('[data-test-id="journal-name-input"]')
    .clear()
    .type(name)
}

const addJournalCode = code => {
  cy.get('[data-test-id="code-input"]')
    .clear()
    .type(code)
}

const addEmail = email => {
  cy.get('[data-test-id="email-input"]')
    .clear()
    .type(email)
}

const addISSN = issn => {
  cy.get('[data-test-id="issn-input"]')
    .clear()
    .type(issn)
}

const assignPeerReviewModel = prm => {
  cy.get('[data-test-id="prm-input-filter"]')
    .click()
    .get('[role="option"]')
    .contains(prm)
    .click()
}

const activateSubmissions = () => {
  cy.contains('label', 'Active for submissions')
    .find('input[type="checkbox"]')
    .check({ force: true })
}

const dezactivateSubmissions = () => {
  cy.contains('label', 'Active for submissions')
    .find('input[type="checkbox"]')
    .uncheck({ force: true })
}

const assignArticleTypes = (types = []) => {
  cy.get('[data-test-id="article-types-box"]')
    .find('input[type="checkbox"]')
    .each($el => {
      types.includes($el.get(0).name)
        ? cy.wrap($el).check({ force: true })
        : cy.wrap($el).uncheck({ force: true })
    })
}

const confirmAddJournal = () => {
  cy.get('[data-test-id="modal-confirm"]').click()
}

const startEditJournal = journalCode => {
  cy.contains('[data-test-id="dashboard-list-items"] div', journalCode)
    .find('[data-test-id="edit-journal"]')
    .click()
}
