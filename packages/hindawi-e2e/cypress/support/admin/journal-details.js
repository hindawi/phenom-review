export const assignEditorialRole = (role, email) => {
  selectAssignEditorialRoleTab()
  selectEditorialRole(role)
  selectUserForEditorialRole(email)
  clickAssignEditorialRoleBtn()
}

const selectAssignEditorialRoleTab = () => {
  cy.contains('Assign Editorial Role').click()
}

const selectEditorialRole = role => {
  cy.contains('Assign Editorial Role').click()
  cy.get('[data-test-id="role-filter"]').click()
  cy.get('[role="option"]')
    .contains(role)
    .click()
}

const selectUserForEditorialRole = email => {
  cy.get('[data-test-id="user-selector"] input').type(email)
  cy.get('[role="option"]')
    .contains(email)
    .click()
}

const clickAssignEditorialRoleBtn = () => {
  cy.get('.icn_icn_addUser').click()
  cy.wait('@gql-assignEditorialRole-mutation')
}
