import 'cypress-file-upload'
import '@testing-library/cypress'
import '@testing-library/cypress/add-commands'

import {
  loginKeycloak,
  logoutKeycloak,
  getActiveJournals,
  createDraftManuscript,
  updateDraftManuscript,
  submitManuscript,
  uploadManuscriptFile,
  getSubmission,
  approveEQS,
  createSubmission,
} from '../support/hindawi-commands'

Cypress.Commands.add('loginKeycloak', loginKeycloak)
Cypress.Commands.add('logoutKeycloak', logoutKeycloak)
Cypress.Commands.add('getActiveJournals', getActiveJournals)
Cypress.Commands.add('createDraftManuscript', createDraftManuscript)
Cypress.Commands.add('updateDraftManuscript', updateDraftManuscript)
Cypress.Commands.add('submitManuscript', submitManuscript)
Cypress.Commands.add('uploadManuscriptFile', uploadManuscriptFile)
Cypress.Commands.add('getSubmission', getSubmission)
Cypress.Commands.add('approveEQS', approveEQS)
Cypress.Commands.add('createSubmission', createSubmission)
