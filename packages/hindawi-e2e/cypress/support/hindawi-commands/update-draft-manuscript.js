import { getAccessToken } from '../../utils/env-utils'
import { getFullEmail } from '../../utils/fixtures-utils'

export const updateDraftManuscript = ({
  author,
  abstract,
  title,
  conflictOfInterest,
  dataAvailability,
  fundingStatement,
  manuscriptFileName,
}) => {
  const manuscriptId = Cypress.env('manuscriptId')
  const journal = Cypress.env('journal')
  const articleType = Cypress.env('articleType')

  const updateManuscript = `mutation{
    updateDraftManuscript(
      manuscriptId: "${manuscriptId}",
      autosaveInput: { 
        journalId: "${journal.id}"
        authors: [
          { aff: "${author.affiliation}"
            country: "${author.country}"
            email: "${getFullEmail(author.email)}"
            givenNames: "${author.firstName}"
            isCorresponding: true
            isSubmitting: true
            surname: "${author.lastName}"
          }]
        meta: {
          abstract: "${abstract}"
          articleTypeId: "${articleType.id}"
          title: "${title}"
          agreeTc: true
          conflictOfInterest:"${conflictOfInterest}"
          dataAvailability: "${dataAvailability}"
          fundingStatement: "${fundingStatement}"
        }
        files: [
          {
            type: "manuscript"
            name: "${manuscriptFileName}"
          }
        ]
      }
    ){id}
  }`

  cy.request({
    method: 'POST',
    url: `/graphql`,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getAccessToken()}`,
    },
    body: { query: updateManuscript },
    failOnStatusCode: false,
  }).then(response => {
    if (response.body.errors) {
      throw new Error(
        `Update Draft Manuscript failed: ${JSON.stringify(response)}`,
      )
    }
  })
}
