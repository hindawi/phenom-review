import {
  getBaseUrl,
  clearAccessToken,
  setAccessToken,
  getKeycloakConfig,
  getDefaultPassword,
} from '../../utils/env-utils'

import { getFullEmail } from '../../utils/fixtures-utils'

const { authBaseUrl, realm, client_id } = getKeycloakConfig()

const KEYCLOAK_LOGOUT_URL = `${authBaseUrl}/auth/realms/${realm}/protocol/openid-connect/logout`
const KEYCLOAK_LOGIN_URL = `${authBaseUrl}/auth/realms/${realm}/protocol/openid-connect/auth`
const KEYCLOAK_TOKEN_URL = `${authBaseUrl}/auth/realms/${realm}/protocol/openid-connect/token`

export const logoutKeycloak = () =>
  cy
    .request({ url: KEYCLOAK_LOGOUT_URL })
    .clearLocalStorage()
    .then(() => clearAccessToken())

export const loginKeycloak = (email, password = getDefaultPassword()) => {
  cy.request({
    url: KEYCLOAK_LOGIN_URL,
    followRedirect: false,
    qs: {
      scope: 'openid',
      response_type: 'code',
      approval_prompt: 'auto',
      redirect_uri: getBaseUrl(),
      client_id,
    },
  })
    .then(response => {
      const html = document.createElement('html')
      html.innerHTML = response.body
      const form = html.getElementsByTagName('form')[0]

      return cy.request({
        method: 'POST',
        url: form.action,
        followRedirect: false,
        form: true,
        body: {
          username: getFullEmail(email),
          password,
        },
      })
    })
    .then(response => {
      cy.request({
        method: 'post',
        url: KEYCLOAK_TOKEN_URL,
        body: {
          client_id,
          redirect_uri: getBaseUrl(),
          code: getAuthCodeFromLocation(response.headers.location),
          grant_type: 'authorization_code',
        },
        form: true,
        followRedirect: false,
      }).its('body')
    })
    .then(response => {
      setAccessToken(response.access_token)
      return response
    })
}

const getAuthCodeFromLocation = location => {
  const url = new URL(location)
  const params = url.search.substring(1).split('&')

  return params
    .map(param => {
      const [key, value] = param.split('=')
      return { key, value }
    })
    .find(param => param.key === 'code').value
}
