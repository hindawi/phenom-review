import { getAccessToken } from '../../utils/env-utils'

export const approveEQS = () => {
  const manuscriptId = Cypress.env('manuscriptId')

  cy.getSubmission().then(submission => {
    const approveEQSMutation = `mutation{
      approveEQS(
        manuscriptId:"${manuscriptId}",
        input:{
          token: "${submission.technicalCheckToken}",
        }
      )
    }`

    cy.request({
      method: 'POST',
      url: `/graphql`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getAccessToken()}`,
      },
      body: { query: approveEQSMutation },
      failOnStatusCode: false,
    }).then(() => {
      // TODO: approveEQS by mutation is old approach, use approve EQS by event instead,
      // this gives us some errors in response body
      // if (response.body.errors) {
      //   throw new Error(`Approve EQS failed: ${JSON.stringify(response)}`)
      // }
    })
  })
}
