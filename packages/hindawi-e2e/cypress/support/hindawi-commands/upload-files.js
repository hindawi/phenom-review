import { getAccessToken } from '../../utils/env-utils'

export const uploadManuscriptFile = async ({ fileName, fileContent }) => {
  const manuscriptId = Cypress.env('manuscriptId')

  await uploadFileToEntity({
    entityId: manuscriptId,
    fileType: 'manuscript',
    fileName,
    fileContent,
  })
}

const uploadFileToEntity = async ({
  entityId,
  fileType,
  fileName,
  fileContent,
}) => {
  const file = new File([fileContent], fileName, {
    type: 'text/plain',
  })
  const fileInput = { type: fileType, size: file.size }

  const query = `mutation uploadFile(
    $entityId: String!
    $fileInput: FileInput
    $file: Upload!
  ) {
      uploadFile(entityId: $entityId, fileInput: $fileInput, file: $file) {
        id
        type
        originalName
        size
        mimeType
      }
    }
  `
  const uploadQuery = {
    operationName: 'uploadFile',
    query,
    variables: {
      entityId,
      fileInput,
      file: null,
    },
  }

  const data = new FormData()
  data.append('operations', JSON.stringify(uploadQuery))
  data.append('map', `{ "0": ["variables.file"] }`)
  data.append('0', file)

  const res = await fetch('/graphql', {
    method: 'POST',
    headers: {
      Accept: `text/plain`,
      Authorization: `Bearer ${getAccessToken()}`,
    },
    body: data,
  })

  return res.json()
}
