import { getAccessToken } from '../../utils/env-utils'

export const getSubmission = () => {
  const submissionId = Cypress.env('submissionId')

  const getSubmission = `query{
    getSubmission(submissionId:"${submissionId}")
    {
      id,
      technicalCheckToken
    }
  }`

  cy.request({
    method: 'POST',
    url: `/graphql`,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getAccessToken()}`,
    },
    body: { query: getSubmission },
    failOnStatusCode: false,
  }).then(response => {
    if (response.body.errors) {
      throw new Error(`Get Submission failed: ${JSON.stringify(response)}`)
    }

    return response.body.data.getSubmission[0]
  })
}
