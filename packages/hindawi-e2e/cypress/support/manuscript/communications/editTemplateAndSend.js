export const editTemplateAndSend = (
  entity,
  { checkNotification } = { checkNotification: true },
) => {
  if (Cypress.env('editable_emails')) {
    const typedText = 'New text was added by cypress'
    const greetingText = `Dear ${entity.lastName}`
    cy.get('.ant-modal', { timeout: 5000 }).should('exist')
    cy.findByText('Email editor').should('exist')
    cy.findByText('REVERT CHANGES').should('exist')
    cy.findByText('SEND EMAIL').should('exist')

    cy.get('.ant-modal')
      .contains(greetingText)
      .should('exist')
    cy.get('.quill-wrapper.is-not-editable')
      .first()
      .click()
      .wait(1000)
      .type(typedText)

    cy.get('.ant-modal')
      .contains(greetingText)
      .click()
    cy.get('.quill-wrapper.is-not-editable .ql-editor')
      .first()
      .contains(typedText)
      .should('exist')
    cy.contains('SEND EMAIL').click()

    if (checkNotification) {
      cy.get('.communication-notification', { timeout: 5000 }).should('exist')
    }
  } else {
    cy.get('.ant-modal')
      .should('be.visible')
      .find('.ant-btn-primary')
      .click()
  }
}
