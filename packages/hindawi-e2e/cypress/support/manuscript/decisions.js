export const makeDecisionForMinorRevision = reason => {
  makeDecisionToRevise('Minor Revision', reason)
}

export const makeDecisionForMajorRevision = reason => {
  makeDecisionToRevise('Major Revision', reason)
}

export const makeDecisionToPublish = () => {
  expandEditorialDecisionBox()
  selectOption('Publish')
  submitDecision()
  confirmDecision()
  cy.wait('@gql-makeDecisionToPublish-mutation')
}

export const makeDecisionToReject = reason => {
  expandEditorialDecisionBox()
  selectOption('Reject')
  cy.wait('@gql-getCommunicationPackage-query')
  writeCommentsForAuthor(reason)
  submitDecision()
  confirmDecision()
  cy.wait('@gql-makeDecisionToReject-mutation')
}

const makeDecisionToRevise = (type, reason) => {
  expandEditorialDecisionBox()
  selectOption(type)
  cy.wait('@gql-getCommunicationPackage-query')
  writeCommentsForAuthor(reason)
  submitDecision()
  confirmDecision()
  cy.wait('@gql-requestRevision-mutation')
}

const expandEditorialDecisionBox = () => {
  cy.contains('Your Editorial Decision').click()
}

const selectOption = option => {
  cy.get('[data-test-id="decision-dropdown"]')
    .click()
    .get('.ant-select-item-option-content')
    .contains(option)
    .click()
}

const writeCommentsForAuthor = text => {
  cy.get('textarea[data-test-id="triage-editor-decision-message"]').type(text)
}

const submitDecision = () => {
  cy.get('[data-test-id="submit-triage-editor-decision"]').click()
}

const confirmDecision = () => {
  cy.get('.ant-modal-content .ant-modal-footer .ant-btn-primary').click()
}
