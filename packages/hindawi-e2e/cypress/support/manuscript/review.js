export const submitReview = (review, recommendation) => {
  cy.findByText('Your Report').click()
  cy.get('[data-test-id="reviewer-report-menu-filter"]').click()
  cy.get('[role="option"]')
    .contains(recommendation)
    .click()
  cy.wait(['@gql-updateDraftReview-mutation'])

  cy.get('[data-test-id="form-report-textarea"]')
    .click()
    .type(review)
  cy.wait(['@gql-updateDraftReview-mutation'])

  cy.get('[data-test-id="submit-reviewer-report"]').click()
  cy.contains('Ready to submit your report?')
  cy.get('[data-test-id="modal-confirm"]').click()
  cy.wait(['@gql-submitReview-mutation'])
}
