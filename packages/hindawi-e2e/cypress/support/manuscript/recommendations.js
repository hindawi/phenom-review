export const recommendMajorRevision = messageForAuthor => {
  recommendRevision('Major Revision', messageForAuthor)
}

export const recommendMinorRevision = messageForAuthor => {
  recommendRevision('Minor Revision', messageForAuthor)
}

const recommendRevision = (type, messageForAuthor) => {
  expandRecommendationBox()
  selectOption(type)
  writeMessageForAuthor(messageForAuthor)
  submitRecommendation()
  confirmRecommendation()
  cy.wait('@gql-requestRevision-mutation')
}

export const recommendPublish = (messageForAuthor, messageForCE) => {
  expandRecommendationBox()
  selectOption('Publish')
  if (messageForAuthor) {
    writeMessageForAuthor(messageForAuthor)
  }
  if (messageForCE) {
    writeMessageForCE(messageForCE)
  }
  submitRecommendation()
  confirmRecommendation()
  cy.wait('@gql-makeRecommendationToPublish-mutation')
}

const expandRecommendationBox = () => {
  cy.get('[data-test-id="contextual-box-editorial-recommendation"]').click()
}

const selectOption = option => {
  cy.get('[data-test-id="recommendation-dropdown-filter"]')
    .click()
    .get('[role="option"]')
    .contains(option)
    .click()
}

const writeMessageForAuthor = text => {
  cy.get(
    '[data-test-id="editorial-recommendation-message-for-author"] textarea',
  ).type(text)
}

const writeMessageForCE = text => {
  cy.get(
    '[data-test-id="editorial-recommendation-message-for-triage-editor"] textarea',
  ).type(text)
}

const submitRecommendation = () => {
  cy.get('[data-test-id="button-editorial-recommendation-submit"]').click()
}

const confirmRecommendation = () => {
  cy.get('.ant-modal-content .ant-modal-footer .ant-btn-primary').click()
}
