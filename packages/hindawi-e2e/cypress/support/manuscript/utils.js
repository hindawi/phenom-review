export const checkStatus = status => {
  cy.get('[data-test-id="manuscript-status"]').should('contain', status)
}

export const navigateToManuscript = (
  submissionId = `${Cypress.env('submissionId')}`,
  manuscriptId = `${Cypress.env('manuscriptId')}`,
) => {
  cy.visit(`/details/${submissionId}/${manuscriptId}`)
  cy.contains('BACK')
}
