export const inviteReviewer = reviewer => {
  cy.get('[label="Reviewer Details & Reports"]').then($reviewDropdown => {
    if (!$reviewDropdown.text().includes('Invite Reviewer')) {
      cy.wrap($reviewDropdown)
        .find('.icn_icn_expand')
        .click()
    }
  })
  cy.get('[data-test-id="reviewer-email"]').type(
    Cypress.env('emailPrefix') + reviewer.email,
  )
  cy.get('[data-test-id="reviewer-givenNames"]').type(reviewer.firstName)
  cy.get('[data-test-id="reviewer-surname"]').type(reviewer.lastName)
  cy.get('[data-test-id="affiliation-author"] input').type(reviewer.affiliation)
  cy.get('[data-test-id="reviewer-country"]').click()
  cy.contains(reviewer.country).click()
  cy.get('[data-type-id="button-invite-reviewer-invite"]').click()
  cy.findByText('Send').click()
  cy.wait('@gql-inviteReviewer-mutation')
}

export const inviteAcademicEditor = eaEmail => {
  cy.get('[data-test-id="assign-academic-editor"]').click()

  cy.get(
    '[data-test-id="manuscript-assign-academic-editor-filter"]',
  ).type(Cypress.env('emailPrefix') + eaEmail, { force: true })

  cy.get(`[data-test-id^="manuscript-assign-academic-editor-invite"]`)
    .contains('Invite')
    .click()

  cy.get('[class="ant-modal-footer"]')
    .contains('INVITE')
    .click()

  cy.wait('@gql-inviteAcademicEditor-mutation')
  cy.wait('@gql-getSubmission-query')

  cy.get('[class="ant-modal-content"]').should('not.be.visible')
}

export const acceptInvitationAsReviewer = () => {
  cy.get(`[value=yes]`).click()
  cy.get(`[data-test-id="reviewer-response-submit"]`).click()
  cy.contains('Please confirm your agreement').should('exist')
  cy.get(`[data-test-id="modal-confirm"]`).click()
}

export const acceptInvitationAsAE = () => {
  cy.get('input[value="yes"]').check({ force: true })
  cy.get('[data-test-id="button-respond-to-editorial-invitation"]')
    .then($input => {
      $input[0].setAttribute('type', 'button')
      return $input
    })
    .click()
  cy.contains('Please confirm your agreement').should('exist')
  cy.get('[class="ant-modal-footer"] button')
    .contains('Agree')
    .click()
  cy.wait('@gql-acceptAcademicEditorInvitation-mutation')
  cy.wait('@gql-getSubmission-query')
}

export const declineInvitationAsReviewer = () => {
  cy.get(`[value=no]`).click()
  cy.get(`[data-test-id="reviewer-response-submit"]`).click()
  cy.contains('Decline this invitation?').should('exist')
  cy.get(`[data-test-id="modal-confirm"]`).click()
}

export const declineInvitationAsAE = reason => {
  cy.get(`[value=no]`).click()
  if (reason) {
    cy.get(`textarea[name="reason"]`).type(reason)
  }
  cy.contains('Respond to Invitation').click()
  cy.contains('Decline this invitation?').should('exist')
  cy.get('.ant-modal-footer button')
    .contains('Decline')
    .click()
}
