export const navigateToUserProfile = () => {
  cy.get('[data-test-id="admin-menu-button"]').click()
  cy.get('[data-test-id="admin-dropdown-profile"]').click()
}

export const changePassword = (currentPassword, newPassword) => {
  clickChangePassword()
  introduceCurrentPassword(currentPassword)
  introduceNewPassword(newPassword)
  updatePassword()
  navigateBackToUserProfile()
}

export const editProfile = ({
  firstName,
  lastName,
  title,
  country,
  affiliation,
}) => {
  enableEditing()
  updateFirstName(firstName)
  updateLastName(lastName)
  updateTitle(title)
  updateCountry(country)
  updateAffiliation(affiliation)
  saveProfileChanges()
}

const enableEditing = () => {
  cy.get('.icn_icn_edit').click()
}

const updateFirstName = firstName => {
  cy.get('input[name="givenNames"]').type(firstName)
}

const updateLastName = lastName => {
  cy.get('input[name="surname"]').type(lastName)
}

const updateTitle = title => {
  cy.get('[role="listbox"]')
    .get('[data-test-id="title-input-filter"]')
    .click()

  cy.get('[role="listbox"]')
    .get('[role="option"]')
    .contains(title)
    .click()
}

const updateCountry = country => {
  cy.get('[data-test-id="country-dropdown"]').clear()
  cy.get('[role="option"]')
    .contains(country)
    .click()
}

const updateAffiliation = affiliation => {
  cy.get('[data-test-id="affiliation-author"] input').type(affiliation)
}

const saveProfileChanges = () => {
  cy.get('.icn_icn_save').click()
  cy.wait(['@gql-updateUser-mutation'])
}

const clickChangePassword = () => {
  cy.get('[data-test-id="change-password-btn"]').click()
}

const introduceCurrentPassword = password => {
  cy.get('input[name="currentPassword"]').type(password)
}

const introduceNewPassword = password => {
  cy.get('input[name="password"]').type(password)
  cy.get('input[name="confirmPassword"]').type(password)
}

const updatePassword = () => {
  cy.get('button')
    .contains('Update password')
    .click()
}

const navigateBackToUserProfile = () => {
  cy.get('button')
    .contains('Back to account settings')
    .click()
}
