import React from 'react'
import PropTypes from 'prop-types'
import { Query } from 'react-apollo'
import { get, isEmpty } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'
import { withModal } from 'component-modal'
import { Button } from '@pubsweet/ui'
import styled, { css } from 'styled-components'
import { compose, withHandlers } from 'recompose'
import {
  Row,
  Tag,
  Item,
  Text,
  Label,
  ActionLink,
  MultiAction,
  parseAffiliations,
} from '@hindawi/ui'

import { IconWarning, IconNotificationAlert, Spinner } from '@hindawi/phenom-ui'

import { Popover } from 'antd'

import { loadReviewerSuggestions } from '../graphql/queries'

const getName = author =>
  `${get(author, 'alias.name.givenNames', '')} ${get(
    author,
    'alias.name.surname',
    '',
  )}`

function conflictsOfInterestContent(
  suggestion,
  authors,
  affiliations,
  showTitle = true,
  extraText = null,
  style = {},
) {
  return (
    <AlignLeft style={style}>
      {showTitle && (
        <h3 className="ant-typography small">
          Potential conflict of interest identified!
        </h3>
      )}
      <p>
        <b>
          {suggestion.givenNames} {suggestion.surname}
          <Superscript>{authors[0].affiliationNumber}</Superscript>
        </b>
        <span style={{ marginLeft: '4px' }}>
          has the same affiliation as the following authors:
        </span>
      </p>
      <ul style={{ marginLeft: '-18px' }}>
        {authors.map(author => (
          <li>
            {getName(author)}
            {author.affiliationNumber && (
              <Superscript>{author.affiliationNumber}</Superscript>
            )}
          </li>
        ))}
      </ul>
      <AffiliationColumn mt={1}>
        {affiliations.map((aff, i) => (
          <Item data-test-id={`affiliation-${i + 1}`} flex={1} key={aff}>
            <Superscript>{i + 1}</Superscript>
            &nbsp;
            <Text color={th('grey70')} style={{ fontSize: '96%' }}>
              {aff}
            </Text>
          </Item>
        ))}
      </AffiliationColumn>
      {extraText && <p style={{ marginTop: '20px' }}>{extraText}</p>}
    </AlignLeft>
  )
}

const PublonsTable = ({ onInvite, manuscriptId, canInvitePublons }) => (
  <Wrapper>
    <Query query={loadReviewerSuggestions} variables={{ manuscriptId }}>
      {({ data, loading }) => {
        if (loading) {
          return [
            <Row mt={6}>
              <Spinner />
            </Row>,
            <Row mt={6}>
              <div>
                Loading reviewer suggestions <span>&hellip;</span>
              </div>
            </Row>,
          ]
        }
        const reviewers = get(data, 'loadReviewerSuggestions', [])
        if (isEmpty(reviewers)) {
          return (
            <Row mb={2} mt={6}>
              <Item alignItems="center" justify="center">
                <Text data-test-id="error-empty-state" emptyState>
                  There are no reviewer suggestions to display
                </Text>
              </Item>
            </Row>
          )
        }
        return (
          <TableView
            canInvitePublons={canInvitePublons}
            onInvite={onInvite}
            reviewers={reviewers}
          />
        )
      }}
    </Query>
  </Wrapper>
)

const TableView = ({ canInvitePublons, reviewers, onInvite }) => (
  <Table>
    <thead>
      <tr>
        <th>
          <Label>Full Name</Label>
        </th>
        <th>
          <Label>Affiliation</Label>
        </th>
        <th>
          <Label>No. of Reviews</Label>
        </th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      {reviewers.map(reviewer => (
        <TableRow disabled={!!reviewer.isInvited} key={reviewer.id}>
          <td data-test-id={`name-${reviewer.id}`} style={{ display: 'flex' }}>
            <ActionLink
              opacity={reviewer.isInvited ? 0.3 : 1}
              to={get(reviewer, 'profileUrl', 'https://www.publons.com/')}
            >
              {`${get(reviewer, 'givenNames', '')} ${get(
                reviewer,
                'surname',
                '',
              )}`}
            </ActionLink>
            {reviewer.conflictsOfInterest &&
              (() => {
                const { authors, affiliations } = parseAffiliations(
                  reviewer.conflictsOfInterest,
                )

                const content = conflictsOfInterestContent(
                  reviewer,
                  authors,
                  affiliations,
                  true,
                )

                return (
                  <Popover content={content} placement="topLeft">
                    <IconWarning
                      className="warning-icon"
                      role="button"
                      style={{
                        position: 'relative',
                        left: '5px',
                        top: '12px',
                      }}
                    />
                  </Popover>
                )
              })()}
          </td>
          <td>
            <Text
              data-test-id={`aff-${reviewer.id}`}
              display="inline-flex"
              opacity={reviewer.isInvited ? 0.3 : 1}
            >{`${get(reviewer, 'aff', '')}`}</Text>
          </td>
          <td>
            <Text
              data-test-id={`numberOfReviews-${reviewer.id}`}
              display="inline-flex"
              opacity={reviewer.isInvited ? 0.3 : 1}
            >{`${get(reviewer, 'numberOfReviews', '')}`}</Text>
          </td>
          {reviewer.isInvited ? (
            <TagCell>
              <Tag mr={2} pending>
                INVITED
              </Tag>
            </TagCell>
          ) : (
            <HiddenCell>
              {canInvitePublons ? (
                <Button
                  data-test-id={`reviewerPublonsButton-${reviewer.id}`}
                  mr={2}
                  onClick={onInvite(reviewer)}
                  primary
                  width={32}
                  xs
                >
                  SEND
                </Button>
              ) : (
                <Button
                  data-test-id={`reviewerPublonsButton-disabled-${reviewer.id}`}
                  disabled
                  mr={2}
                  size="small"
                >
                  SEND
                </Button>
              )}
            </HiddenCell>
          )}
        </TableRow>
      ))}
    </tbody>
  </Table>
)
export default compose(
  withModal({
    modalKey: 'invite-reviewer',
    component: MultiAction,
  }),
  withHandlers({
    onInvite: ({ showModal, onInvite }) => reviewer => () => {
      const { aff, email, givenNames, surname, conflictsOfInterest } = reviewer

      let title = 'Send Invitation to Review?'
      let confirmText = 'SEND'
      const cancelText = 'CANCEL'
      let content = ''

      if (conflictsOfInterest && conflictsOfInterest.length > 0) {
        const { authors, affiliations } = parseAffiliations(conflictsOfInterest)
        content = conflictsOfInterestContent(
          reviewer,
          authors,
          affiliations,
          false,
          'Do you want to invite this reviewer to handle the manuscript anyway?',
          { marginLeft: '52px' },
        )
        confirmText = 'INVITE ANYWAY'
        title = (
          <Title>
            <IconNotificationAlert />
            <span>Potential conflict of interest identified!</span>
          </Title>
        )
      }

      showModal({
        title,
        confirmText,
        cancelText,
        subtitle: content,
        onConfirm: modalProps =>
          onInvite({ aff, email, givenNames, surname }, modalProps),
      })
    },
  }),
)(PublonsTable)

PublonsTable.propTypes = {
  /** Sends an invitation to the reviewer. */
  onInvite: PropTypes.func,
}

PublonsTable.defaultProps = {
  onInvite: () => {},
}

// #region styles
const Table = styled.table`
  border-collapse: collapse;
  width: 100%;

  & thead {
    border: 1px solid ${th('colorBorder')};
    background-color: ${th('colorBackgroundHue2')};
    padding-top: calc(${th('gridUnit')} * 4);
  }

  & th,
  & td {
    border: none;
    padding-left: calc(${th('gridUnit')} * 4);
    text-align: start;
    vertical-align: middle;

    height: calc(${th('gridUnit')} * 10);
    min-width: calc(${th('gridUnit')} * 24);
  }
`

const HiddenCell = styled.td`
  display: flex;
  justify-content: flex-end;
  opacity: 0;
  padding-top: calc(${th('gridUnit')} * 2);
`

const HidableCell = styled.td`
  opacity: 1;
  padding-top: calc(${th('gridUnit')} * 2);
`

const TagCell = styled.td`
  display: flex;
  justify-content: flex-end;
  padding-top: calc(${th('gridUnit')} * 2);
`
const TableCSS = props => {
  if (get(props, 'disabled', false)) {
    return css`
      background: ${th('colorBackgroundHue3')};
    `
  }
  return css`
    &:hover {
      background: ${th('colorBackgroundHue3')};
      ${HiddenCell} {
        opacity: 1;
      }
      ${HidableCell} {
        opacity: 0;
      }
    }
  `
}

const TableRow = styled.tr`
  border-bottom: 1px solid ${th('colorBorder')};
  & td:first-child {
    min-width: calc(${th('gridUnit')} * 40);
  }
  & td:last-child {
    vertical-align: top;
    text-align: right;
    padding-right: calc(${th('gridUnit')} * 4);
  }
  ${TableCSS}

  .warning-icon {
    color: ${th('grey70')};

    & svg {
      width: 16px;
      height: 16px;
    }
  }
`

const Wrapper = styled.div`
  display: block;
  height: calc(${th('gridUnit')} * 116);
  overflow-x: auto;
  background-color: ${th('colorBackgroundHue2')};
`

const Superscript = styled.span`
  position: relative;
  top: -0.5em;
  font-size: 80%;
  font-family: ${th('defaultFont')};
`

const AffiliationColumn = styled(Row)`
  margin-top: 12px;
  align-items: flex-start;
  flex-direction: column;
`

const AlignLeft = styled.div`
  text-align: left;
  margin-left: 0px;
  max-height: 200px;
  overflow-y: auto;
  padding: 8px;
  color: ${th('grey70')};
`
const Title = styled.div`
  & svg {
    position: relative;
    left: -30px;
    top: 10px;
    width: 1.8em;
    height: 1.8em;
  }

  & span {
    position: relative;
    top: -20px;
  }
`
