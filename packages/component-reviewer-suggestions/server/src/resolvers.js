const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('./use-cases')
const services = require('./services')

const resolvers = {
  Query: {
    async loadReviewerSuggestions(_, { manuscriptId }, ctx) {
      const { Manuscript, ReviewerSuggestion, TeamMember, Team } = models
      return useCases.loadReviewerSuggestionsUseCase
        .initialize({
          models: { Manuscript, ReviewerSuggestion, TeamMember, Team },
          reviewerSuggestionsService: services.publonsService,
        })
        .execute(manuscriptId)
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
