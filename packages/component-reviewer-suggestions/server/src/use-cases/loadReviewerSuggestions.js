const { get, chain, last, initial, isEmpty } = require('lodash')
const Promise = require('bluebird')
const { rorService } = require('component-ror')

function parseReviewerSuggestions(data) {
  return chain(data)
    .filter(rev => rev.profileUrl && get(rev, 'contact.emails.length', 0) > 0)
    .map(rev => ({
      email: process.env.PUBLONS_MOCK_EMAIL
        ? process.env.PUBLONS_MOCK_EMAIL.replace(
            '__NAME__',
            `${rev.contact.emails[0].email.split('@')[0]}`,
          )
        : rev.contact.emails[0].email,
      givenNames: initial(rev.publishingName.split(' ')).join(' '),
      surname: last(rev.publishingName.split(' ')),
      profileUrl: rev.profileUrl,
      numberOfReviews: rev.numVerifiedReviews,
      aff: get(rev, 'recentOrganizations[0].name', 'Non-affiliated'),
      manuscriptId: rev.manuscriptId,
      type: rev.type,
    }))
    .uniqBy('email')
    .value()
}

function checkAndAddConflictsOfInterest(suggestions, authors) {
  suggestions.forEach(suggestion => {
    if (!suggestion.affRorId) return []

    authors.forEach(author => {
      if (author.alias.affRorId === suggestion.affRorId) {
        author.alias = {
          ...author.alias,
          aff: author.alias.aff,
          country: author.alias.country,
          email: author.alias.email,
          name: {
            surname: author.alias.surname,
            givenNames: author.alias.givenNames,
            title: author.alias.title,
          },
        }

        if (!Array.isArray(suggestion.conflictsOfInterest)) {
          suggestion.conflictsOfInterest = []
        }

        suggestion.conflictsOfInterest.push(author)
      }
    })
  })
}

const initialize = ({
  models: { Manuscript, ReviewerSuggestion, TeamMember, Team },
  reviewerSuggestionsService,
}) => ({
  async execute(manuscriptId) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[journal,teams.members]',
    )

    const wrongStatuses = [
      Manuscript.Statuses.draft,
      Manuscript.Statuses.technicalChecks,
    ]
    if (wrongStatuses.includes(manuscript.status)) return []

    const authors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.author,
    })

    // * check if the suggestions are already cached in our db and return them
    const localReviewerSuggestions = await ReviewerSuggestion.findBy({
      manuscriptId: manuscript.id,
    })

    const areSuggestionsCached = !isEmpty(localReviewerSuggestions)

    const reviewerSuggestionsData = areSuggestionsCached
      ? localReviewerSuggestions
      : parseReviewerSuggestions(
          await reviewerSuggestionsService.getReviewerSuggestions(manuscript),
        )

    const publonsReviewerSuggestions = []

    if (!areSuggestionsCached) {
      // * if not cached, make the request to publons
      if (!manuscript.journal) throw new Error('Journal is required.')
      if (!manuscript.teams) throw new Error('Teams are required.')

      // eslint-disable-next-line sonarjs/no-unused-collection
      await Promise.map(
        reviewerSuggestionsData,
        async reviewer => {
          const newReviewer = new ReviewerSuggestion(reviewer)

          // * get RoRId for the reviewer's affiliation
          const bestROR = await rorService.getBestROR(newReviewer.aff, '')
          if (bestROR) {
            newReviewer.affRorId = bestROR.organization.id
          }

          await newReviewer.save()
          publonsReviewerSuggestions.push(newReviewer)
        },
        { concurrency: 10 },
      )
    }

    const reviewerSuggestions = areSuggestionsCached
      ? reviewerSuggestionsData
      : publonsReviewerSuggestions

    // * check the conflicts of interests with the authors
    checkAndAddConflictsOfInterest(reviewerSuggestions, authors)

    return reviewerSuggestions.map(suggestion => suggestion.toDTO())
  },
})

const authsomePolicies = [
  'isAuthenticated',
  'isAcademicEditorOnManuscript',
  'isEditorialAssistant',
  'admin',
]

module.exports = {
  initialize,
  authsomePolicies,
}
