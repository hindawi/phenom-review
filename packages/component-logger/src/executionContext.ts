import { AsyncLocalStorage } from 'async_hooks'

// eslint-disable-next-line sonarjs/no-unused-collection
const contextStore = new Map()

const executionContext = new AsyncLocalStorage()

export { executionContext, contextStore }
