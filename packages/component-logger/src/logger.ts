/* eslint-disable no-console */
/* eslint-disable class-methods-use-this */

import { executionContext } from './executionContext'

function formatMetadata(props) {
  const contextArray: string[] = []
  if (props && props.length) {
    props.forEach(prop => {
      let context = ''
      if (typeof prop === 'object') {
        context = Object.keys(prop)
          .reduce((acc, cv) => `${acc} ${cv}=${prop[cv]}`, '')
          .trim()
      } else {
        context = prop
      }
      contextArray.push(context)
    })
    return contextArray
  }
  return []
}

const applicationLabel = 'review'

class Logger {
  console: any
  labels: Record<string, string>
  private executionContext: any

  constructor() {
    this.console = global.console
    this.labels = {}
    this.executionContext = executionContext
  }

  info(...args) {
    this.console.info(this.formatLog('info', args))
  }

  warn(...args) {
    this.console.warn(this.formatLog('warn', args))
  }

  debug(...args) {
    this.console.debug(this.formatLog('debug', args))
  }

  trace(...args) {
    this.console.trace(this.formatLog('trace', args))
  }

  error(...args) {
    this.console.error(this.formatErrorLog('error', args))
  }

  formatErrorLog(level, ...args) {
    const nowDate: Date = new Date()
    const timestamp: String = nowDate.toISOString()
    const props = Array.prototype.slice.call(args).pop()
    const message = props.shift()
    let { labels } = this

    if (this.executionContext && this.executionContext.getStore()) {
      labels = Object.assign({}, labels, {
        correlationId: this.executionContext.getStore().get('correlationId'),
      })
    }

    const contextArray: string[] = formatMetadata(props.concat([labels]))

    if (typeof message === 'object') {
      // ^this means error is the first argument here

      return `${timestamp} ${level} ${applicationLabel} [${contextArray}] ${message.stack}`
    }

    // when error and error not the first argument, assume is the second one
    const error = props.shift() || {}

    return `${timestamp} ${level} ${applicationLabel} [${contextArray}] ${message} ${error?.stack}`
  }

  formatLog(level, ...args) {
    const nowDate: Date = new Date()
    const timestamp: String = nowDate.toISOString()
    const props = Array.prototype.slice.call(args).pop()

    let message = props.shift()
    message = typeof message === 'object' ? JSON.stringify(message) : message

    let { labels } = this
    if (this.executionContext && this.executionContext.getStore()) {
      labels = Object.assign({}, labels, {
        correlationId: this.executionContext.getStore().get('correlationId'),
      })
    }

    const contextArray: string[] = formatMetadata(props.concat([labels]))

    return `${timestamp} ${level} ${applicationLabel} [${contextArray}] ${message}`
  }

  addLabel(label: string, value: string) {
    this.labels[label] = value
    return this
  }
}

export const logger = new Logger()
