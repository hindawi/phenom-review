const {
  generateJob,
  getTeamRoles,
  generateTeamMember,
  generateManuscript,
  getTeamMemberStatuses,
  getManuscriptStatuses,
  getManuscriptInProgressStatuses,
} = require('component-generators')

const { initialize } = require('../src/existingRemovalJobs')

const models = {
  Job: {
    subscribe: jest.fn(),
  },
  Manuscript: {
    find: jest.fn(),
    Statuses: getManuscriptStatuses(),
    InProgressStatuses: getManuscriptInProgressStatuses(),
  },
  Team: {
    Role: getTeamRoles(),
  },
  TeamMember: {
    find: jest.fn(),
    Statuses: getTeamMemberStatuses(),
    findOneByManuscriptAndRoleAndStatus: jest.fn(),
    findAllByManuscriptAndRoleAndExcludedStatuses: jest.fn(),
  },
  ReviewerSuggestion: {
    findOneBy: jest.fn(),
  },
}

const logEvent = jest.fn()
logEvent.objectType = {
  user: 'user',
}

const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

const logger = {
  warn: jest.fn(),
  info: jest.fn(),
  error: jest.fn(),
}

const { Manuscript, TeamMember, Team } = models

describe('existing removal jobs', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('has the handler exposed', () => {
    const initializer = initialize({ models, logger, logEvent, eventsService })
    const handler = 'handle'
    expect(initializer).toHaveProperty(handler)
    expect(typeof initializer[handler]).toBe('function')
  })

  it('returns a warning when the manuscript Id is not defined', async () => {
    const job = generateJob({
      manuscriptId: undefined,
    })
    await initialize({
      models,
      logger,
      logEvent,
      eventsService,
    }).handle(job)

    expect(logger.warn).toHaveBeenCalledWith('Manuscript id is undefined')
  })

  it('returns a warning when the manuscript is not in progress', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.qualityChecksSubmitted,
    })
    jest.spyOn(models.Manuscript, 'find').mockResolvedValue(manuscript)

    const job = generateJob({ manuscriptId: manuscript.id })

    await initialize({
      models,
      logger,
      logEvent,
      eventsService,
    }).handle(job)

    expect(logger.warn).toHaveBeenCalledWith(
      `Cannot remove team members on manuscript ${manuscript.id} with status ${manuscript.status}`,
    )
  })

  it('returns a warning when the team member status is not accepted or pending', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.pendingApproval,
    })

    const teamMember = generateTeamMember({
      status: TeamMember.Statuses.removed,
      team: {
        role: Team.Role.academicEditor,
      },
    })
    jest.spyOn(models.Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(models.TeamMember, 'find').mockResolvedValue(teamMember)

    const job = generateJob({
      manuscriptId: manuscript.id,
      invitationId: teamMember.id,
    })

    await initialize({
      models,
      logger,
      logEvent,
      eventsService,
    }).handle(job)

    expect(logger.warn).toHaveBeenCalledWith(
      `Cannot remove team members on manuscript ${manuscript.id} with status ${manuscript.status} and team member role ${teamMember.team.role}`,
    )
  })

  it('returns a warning when the team member role is not academic editor or reviewer', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.pendingApproval,
    })
    const teamMember = generateTeamMember({
      status: TeamMember.Statuses.pending,
      team: {
        role: Team.Role.editorialAssistant,
      },
    })
    jest.spyOn(models.Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(models.TeamMember, 'find').mockResolvedValue(teamMember)

    const job = generateJob({
      manuscriptId: manuscript.id,
      invitationId: teamMember.id,
    })

    await initialize({
      models,
      logger,
      logEvent,
      eventsService,
    }).handle(job)

    expect(logger.warn).toHaveBeenCalledWith(
      `Cannot remove team members on manuscript ${manuscript.id} with status ${manuscript.status} and team member role ${teamMember.team.role}`,
    )
  })

  it('returns a warning when the team member role is academic editor and the status is accepted', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.pendingApproval,
    })
    const teamMember = generateTeamMember({
      status: TeamMember.Statuses.accepted,
      team: {
        role: Team.Role.academicEditor,
      },
    })
    jest.spyOn(models.Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(models.TeamMember, 'find').mockResolvedValue(teamMember)

    const job = generateJob({
      manuscriptId: manuscript.id,
      invitationId: teamMember.id,
    })

    await initialize({
      models,
      logger,
      logEvent,
      eventsService,
    }).handle(job)

    expect(logger.warn).toHaveBeenCalledWith(
      `Cannot remove accepted academic editor on manuscript ${manuscript.id}`,
    )
  })

  it('returns a success message when the team member role is academic editor and the status is pending', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.pendingApproval,
    })
    const teamMember = generateTeamMember({
      status: TeamMember.Statuses.pending,
      team: {
        role: Team.Role.academicEditor,
      },
    })
    jest.spyOn(models.Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(models.TeamMember, 'find').mockResolvedValue(teamMember)
    jest
      .spyOn(models.TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue(undefined)
    jest.spyOn(teamMember, 'save').mockReturnValue(teamMember)

    const job = generateJob({
      manuscriptId: manuscript.id,
      invitationId: teamMember.id,
    })

    await initialize({
      models,
      logger,
      logEvent,
      eventsService,
    }).handle(job)

    expect(teamMember.save).toHaveBeenCalled()
    expect(manuscript.save).toHaveBeenCalled()
    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledWith({
      submissionId: manuscript.submissionId,
      eventName: `SubmissionAcademicEditorInvitationExpired`,
    })
    expect(logger.info).toHaveBeenCalledWith(
      `Successfully removed ${teamMember.team.role} with id ${teamMember.id} from manuscript ${manuscript.id}`,
    )
  })
  it('returns a success message when the team member role is reviewer and the status is accepted', async () => {
    const manuscript = generateManuscript({
      status: Manuscript.Statuses.underReview,
    })
    const teamMember = generateTeamMember({
      status: TeamMember.Statuses.accepted,
      team: {
        role: Team.Role.reviewer,
      },
    })
    jest.spyOn(models.Manuscript, 'find').mockResolvedValue(manuscript)
    jest.spyOn(models.TeamMember, 'find').mockResolvedValue(teamMember)
    jest.spyOn(models.ReviewerSuggestion, 'findOneBy').mockResolvedValue()

    jest
      .spyOn(models.TeamMember, 'findAllByManuscriptAndRoleAndExcludedStatuses')
      .mockResolvedValue([])
    jest.spyOn(teamMember, 'save').mockReturnValue(teamMember)

    const job = generateJob({
      manuscriptId: manuscript.id,
      invitationId: teamMember.id,
    })

    await initialize({
      models,
      logger,
      logEvent,
      eventsService,
    }).handle(job)

    expect(teamMember.save).toHaveBeenCalled()
    expect(manuscript.save).toHaveBeenCalled()
    expect(manuscript.updateStatus).toHaveBeenCalledWith(
      Manuscript.Statuses.academicEditorAssigned,
    )
    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledWith({
      submissionId: manuscript.submissionId,
      eventName: `SubmissionReviewerRemoved`,
    })
    expect(logger.info).toHaveBeenCalledWith(
      `Successfully removed ${teamMember.team.role} with id ${teamMember.id} from manuscript ${manuscript.id}`,
    )
  })
})
