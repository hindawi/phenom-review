const { initialize } = require('../src/specialIssueDeactivationJobs')

const {
  generateJob,
  getTeamRoles,
  generateSection,
  generateJournal,
  generateTeamMember,
  generateSpecialIssue,
} = require('component-generators')

const models = {
  Job: {
    subscribe: jest.fn(),
  },
  SpecialIssue: { find: jest.fn() },
  Section: { find: jest.fn() },
  TeamMember: {
    findAllBySpecialIssueAndRole: jest.fn(),
  },
  Team: {
    Role: getTeamRoles(),
  },
  Journal: { find: jest.fn() },
}

const eventsService = {
  publishJournalEvent: jest.fn(),
  publishSpecialIssueEvent: jest.fn(),
}

const job = generateJob()

const logger = {
  info: jest.fn(),
  error: jest.fn(),
}
const Email = jest.fn().mockImplementation(() => ({
  async sendEmail() {
    return jest.fn()
  },
}))

const getPropsService = {
  getProps: jest.fn(),
}

describe('specialIssueDeactivationJobs', () => {
  let wrapper
  beforeAll(() => {
    jest.clearAllMocks()
    wrapper = initialize({
      models,
      logger,
      Email,
      eventsService,
      getPropsService,
    })
  })

  it('should have the handler exposed', () => {
    const handler = 'handle'
    expect(wrapper).toHaveProperty(handler)
    expect(typeof wrapper[handler]).toBe('function')
  })

  it('should have the subscribe function exposed', () => {
    const subscribe = 'subscribe'
    expect(wrapper).toHaveProperty(subscribe)
    expect(typeof wrapper[subscribe]).toBe('function')
  })

  it('should deactivate special issue when isActive is true', async () => {
    const journal = generateJournal({ isActive: false })

    const specialIssue = generateSpecialIssue({
      isActive: true,
      journalId: journal.id,
      id: 12334,
    })
    const guestEditor = generateTeamMember()

    jest.spyOn(models.SpecialIssue, 'find').mockResolvedValue(specialIssue)
    jest.spyOn(models.Journal, 'find').mockResolvedValue(journal)

    jest
      .spyOn(models.TeamMember, 'findAllBySpecialIssueAndRole')
      .mockResolvedValue([guestEditor])
    job.data.specialIssueId = specialIssue.id

    await wrapper.handle(job)

    expect(specialIssue.save).toHaveBeenCalled()
    expect(models.SpecialIssue.find).toHaveBeenCalled()
    expect(eventsService.publishJournalEvent).toHaveBeenCalledWith({
      journalId: specialIssue.journalId,
      eventName: 'JournalSpecialIssueDeactivated',
    })
    expect(eventsService.publishSpecialIssueEvent).toHaveBeenCalledWith({
      specialIssueId: specialIssue.id,
      eventName: 'SpecialIssueUpdated',
    })
  })
  it('should emit JournalSectionSpecialIssueDeactivated event', async () => {
    const journal = generateJournal({ isActive: false })

    const section = generateSection({ jorunalId: journal.id })
    const specialIssue = generateSpecialIssue({
      isActive: true,
      sectionId: section.id,
      id: 12334,
    })
    jest.spyOn(models.SpecialIssue, 'find').mockResolvedValue(specialIssue)
    jest.spyOn(models.Section, 'find').mockResolvedValue(section)
    jest.spyOn(models.Journal, 'find').mockResolvedValue(journal)

    job.data.specialIssueId = specialIssue.id

    await wrapper.handle(job)

    expect(eventsService.publishJournalEvent).toHaveBeenCalledWith({
      journalId: section.journalId,
      eventName: 'JournalSectionSpecialIssueDeactivated',
    })
    expect(eventsService.publishSpecialIssueEvent).toHaveBeenCalledWith({
      specialIssueId: specialIssue.id,
      eventName: 'SpecialIssueUpdated',
    })
  })

  it('should not notify editors if the SI journal is inactive', async () => {
    const journal = generateJournal({ isActive: false })
    const specialIssue = generateSpecialIssue({
      isActive: true,
      journalId: journal.id,
    })

    jest.spyOn(models.SpecialIssue, 'find').mockResolvedValue(specialIssue)
    jest.spyOn(models.Journal, 'find').mockResolvedValue(journal)

    job.data.specialIssueId = specialIssue.id

    await wrapper.handle(job)

    expect(
      models.TeamMember.findAllBySpecialIssueAndRole,
    ).not.toHaveBeenCalled()
  })

  it('should not notify editors if the section SI is inactive', async () => {
    const journal = generateJournal({ isActive: false })
    const section = generateSection({ jorunalId: journal.id })

    const specialIssue = generateSpecialIssue({
      section,
      isActive: true,
      sectionId: section.id,
      id: 12334,
    })

    jest.spyOn(models.SpecialIssue, 'find').mockResolvedValue(specialIssue)
    jest.spyOn(models.Section, 'find').mockResolvedValue(section)
    jest.spyOn(models.Journal, 'find').mockResolvedValue(journal)

    job.data.specialIssueId = specialIssue.id

    await wrapper.handle(job)

    expect(
      models.TeamMember.findAllBySpecialIssueAndRole,
    ).not.toHaveBeenCalled()
  })
})
