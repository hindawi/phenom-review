const {
  generateJob,
  getTeamRoles,
  generateManuscript,
  generateTeamMember,
  getTeamMemberStatuses,
  generateReviewerSuggestion,
} = require('component-generators')

const { initialize } = require('../src/pendingReviewerRemovalJobs')

const models = {
  Team: { Role: getTeamRoles() },
  Manuscript: {
    find: jest.fn(),
  },
  TeamMember: {
    find: jest.fn(),
    findAllByManuscriptAndRoleAndExcludedStatuses: jest.fn(),
    Statuses: getTeamMemberStatuses(),
  },
  ReviewerSuggestion: {
    findOneBy: jest.fn(),
  },
}

const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

const logEvent = jest.fn()
logEvent.actions = {
  reviewer: { reviewer_invitation_removed: '' },
}
logEvent.objectType = {
  user: '',
}

const logger = {
  info: jest.fn(),
}

const job = generateJob()

describe('accepted reviewer removal job', () => {
  let wrapper
  beforeEach(() => {
    jest.clearAllMocks()
    wrapper = initialize({ models, logger, logEvent, eventsService })
  })

  it('has the handler exposed', () => {
    const handler = 'handle'
    expect(wrapper).toHaveProperty(handler)
    expect(typeof wrapper[handler]).toBe('function')
  })

  it('skips invalid jobs', async () => {
    const job = generateJob({
      days: undefined,
      timeUnit: undefined,
      invitationId: undefined,
      manuscriptId: undefined,
    })

    await wrapper.handle(job)

    expect(job.done).toHaveBeenCalled()
  })

  it('successfully executes removal jobs', async () => {
    const manuscript = generateManuscript()
    const teamMember = generateTeamMember()
    const reviewerSuggestion = generateReviewerSuggestion()

    jest.spyOn(models.TeamMember, 'find').mockResolvedValue(teamMember)
    jest
      .spyOn(models.ReviewerSuggestion, 'findOneBy')
      .mockResolvedValue(reviewerSuggestion)
    jest.spyOn(models.Manuscript, 'find').mockResolvedValue(manuscript)
    jest
      .spyOn(models.TeamMember, 'findAllByManuscriptAndRoleAndExcludedStatuses')
      .mockResolvedValue([{}])

    await wrapper.handle(job)

    expect(logger.info).toHaveBeenCalledWith(
      `Job ${job.name}: the ${job.data.days} ${job.data.timeUnit} removal has been executed for invitation ${job.data.invitationId}`,
    )
  })
})
