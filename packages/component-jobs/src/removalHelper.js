const { transaction } = require('objection')

module.exports = {
  async updateTeamMemberAndManuscript({
    teamMember,
    manuscript,
    TeamMember,
    Manuscript,
  }) {
    try {
      return await transaction(TeamMember.knex(), async trx => {
        await TeamMember.query(trx).update(teamMember)
        await Manuscript.query(trx).update(manuscript)
      })
    } catch (err) {
      throw new Error('Something is wrong. No data updated.')
    }
  },
}
