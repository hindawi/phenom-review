module.exports.initialize = ({ models: { Job, Journal }, eventsService }) => ({
  async handle(job) {
    try {
      const { journalId } = job.data
      const journal = await Journal.find(journalId)

      if (journal.isActive || !journal.apc) return

      journal.updateProperties({ isActive: true })
      await journal.save()

      eventsService.publishJournalEvent({
        journalId: journal.id,
        eventName: 'JournalActivated',
      })

      return job.done()
    } catch (e) {
      return job.done(e)
    }
  },
  async subscribe() {
    Job.subscribe({
      queueName: 'journal-activation*',
      jobHandler: this.handle,
    })
  },
})
