/* eslint-disable sonarjs/cognitive-complexity */
module.exports.initialize = ({
  models: { Job, Manuscript, Team, TeamMember },
  Email,
  logger,
  logEvent,
}) => ({
  async handle(job) {
    try {
      const {
        userId,
        action,
        emailProps,
        manuscriptId,
        invitationId,
        teamMemberId,
        logActivity = true,
      } = job.data

      const manuscript = await Manuscript.find(manuscriptId)

      if (!manuscript) {
        logger.error(`No manuscript found with id ${manuscriptId}`)
        return job.done(`No manuscript found with id ${manuscriptId}`)
      }

      if (!Manuscript.InProgressStatuses.includes(manuscript.status)) {
        logger.warn(
          `Skipping emails for manuscript ${manuscriptId} with status ${manuscript.status}`,
        )

        return job.done()
      }

      // skipping emails for manuscript which are close to a decision
      if (
        [
          Manuscript.Statuses.makeDecision,
          Manuscript.Statuses.reviewCompleted,
          Manuscript.Statuses.pendingApproval,
          Manuscript.Statuses.revisionRequested,
        ].includes(manuscript.status)
      ) {
        logger.info(
          `Skipping emails for manuscript ${manuscriptId} with status ${manuscript.status}`,
        )
        return job.done()
      }

      let teamMember
      if (invitationId) {
        teamMember = await TeamMember.find(invitationId, 'team')
        if (!teamMember) {
          logger.error(`No team member found with id ${invitationId}`)
          return job.done()
        }
        teamMember.role = teamMember.team.role
      }

      if (!teamMember && userId) {
        teamMember = await TeamMember.findOneByManuscriptAndUser({
          userId,
          manuscriptId,
        })
        if (!teamMember) {
          logger.error(`No team member found with userId ${userId}`)
          return job.done(`No team member found with userId ${userId}`)
        }
        teamMember.role = teamMember.team.role
      }

      if (!teamMember && teamMemberId) {
        teamMember = await TeamMember.find(teamMemberId, 'team')
        if (!teamMember) {
          logger.error(`No team member found with id ${teamMemberId}`)
          return job.done(`No team member found with id ${teamMemberId}`)
        }
        teamMember.role = teamMember.team.role
      }

      if (!teamMember) {
        logger.error(`No team member found for manuscript id ${manuscriptId}`)
        return job.done(
          `No team member found for manuscript id ${manuscriptId}`,
        )
      }

      if (
        ![Team.Role.reviewer, Team.Role.academicEditor].includes(
          teamMember.role,
        )
      ) {
        logger.warn(
          `Skipping emails for manuscript ${manuscriptId} with status ${manuscript.status} and team member role ${teamMember.role}`,
        )
        return job.done()
      }

      if (manuscript.status === Manuscript.Statuses.submitted) {
        logger.warn(
          `Skipping emails for submitted manuscript ${manuscriptId} when no Academic Editor is invited`,
        )
        return job.done()
      }

      if (
        teamMember.role === Team.Role.academicEditor &&
        teamMember.status === TeamMember.Statuses.accepted &&
        ![
          Manuscript.Statuses.academicEditorAssigned,
          Manuscript.Statuses.reviewersInvited,
          Manuscript.Statuses.underReview,
        ].includes(manuscript.status)
      ) {
        logger.warn(
          `Skipping emails for accepted Academic Editors on manuscript ${manuscriptId} with status ${manuscript.status} because no action is needed`,
        )

        return job.done()
      }

      if (
        teamMember.role === Team.Role.academicEditor &&
        teamMember.status === TeamMember.Statuses.pending &&
        manuscript.status !== Manuscript.Statuses.academicEditorInvited
      ) {
        logger.warn(
          `Skipping emails for pending Academic Editors on manuscript ${manuscriptId} with status ${manuscript.status} because no action is needed`,
        )

        return job.done()
      }

      const email = new Email(emailProps)
      await email.sendEmail()

      if (logActivity) {
        logEvent({
          userId: null,
          manuscriptId,
          action,
          objectType: logEvent.objectType.user,
          objectId: userId,
        })
      }

      return job.done()
    } catch (e) {
      return job.done(e)
    }
  },
  async subscribe() {
    Job.subscribe({
      queueName: 'email-*',
      jobHandler: this.handle,
    })
  },
})
