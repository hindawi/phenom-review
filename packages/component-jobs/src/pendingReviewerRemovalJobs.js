const { updateTeamMemberAndManuscript } = require('./removalHelper')

module.exports.initialize = ({
  models: { Job, Manuscript, Team, TeamMember, ReviewerSuggestion },
  logger,
  logEvent,
  eventsService,
}) => ({
  async handle(job) {
    try {
      const { days, timeUnit, invitationId, manuscriptId } = job.data

      const teamMember = await TeamMember.find(invitationId, 'user.identities')
      const reviewerSuggestion = await ReviewerSuggestion.findOneBy({
        queryObject: {
          email: teamMember.alias.email,
          manuscriptId,
        },
      })
      if (reviewerSuggestion) {
        reviewerSuggestion.isInvited = false
        await reviewerSuggestion.save()
      }

      const manuscript = await Manuscript.find(manuscriptId)
      const reviewers = await TeamMember.findAllByManuscriptAndRoleAndExcludedStatuses(
        {
          role: Team.Role.reviewer,
          manuscriptId,
          excludedStatuses: [
            TeamMember.Statuses.expired,
            TeamMember.Statuses.removed,
            TeamMember.Statuses.conflicting,
          ],
        },
      )

      teamMember.updateProperties({ status: TeamMember.Statuses.expired })

      if (reviewers && reviewers.length === 0) {
        manuscript.updateStatus(Manuscript.Statuses.academicEditorAssigned)
        await updateTeamMemberAndManuscript({
          teamMember,
          manuscript,
          Manuscript,
          TeamMember,
        })
      } else {
        await teamMember.save()
      }

      eventsService.publishSubmissionEvent({
        submissionId: manuscript.submissionId,
        eventName: 'SubmissionReviewerInvitationExpired',
      })

      logEvent({
        userId: null,
        manuscriptId,
        action: logEvent.actions.reviewer_invitation_removed,
        objectType: logEvent.objectType.user,
        objectId: teamMember.user.id,
      })

      const message = `Job ${job.name}: the ${days} ${timeUnit} removal has been executed for invitation ${invitationId}`

      logger.info(message)

      return job.done()
    } catch (e) {
      return job.done(e)
    }
  },
  async subscribe() {
    Job.subscribe({
      queueName: 'pending-reviewer-removal',
      jobHandler: this.handle,
    })
  },
})
