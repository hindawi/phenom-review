module.exports.initialize = ({ models: { Job }, Email, logEvent }) => ({
  async handle(job) {
    try {
      const {
        userId,
        action,
        emailProps,
        manuscriptId,
        logActivity = true,
      } = job.data

      const email = new Email(emailProps)
      await email.sendEmail()
      if (logActivity) {
        logEvent({
          userId: null,
          manuscriptId,
          action,
          objectType: logEvent.objectType.user,
          objectId: userId,
        })
      }
      return job.done()
    } catch (e) {
      return job.done(e)
    }
  },

  async subscribe() {
    Job.subscribe({
      queueName: 'editorial-assistant-email',
      jobHandler: this.handle,
    })
  },
})
