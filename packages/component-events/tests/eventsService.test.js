const {
  generateTeam,
  generateJournal,
  generateSection,
  generateTeamMember,
  generateSpecialIssue,
  generatePeerReviewModel,
} = require('component-generators')

const events = require('../server/src')

let models = {}
let Journal = {}

global.applicationEventBus = {}
global.applicationEventBus = {
  publishMessage: jest.fn(),
}

describe('Events service', () => {
  let eventsService
  beforeEach(() => {
    jest.clearAllMocks()
    models = {
      Journal: {
        find: jest.fn(),
      },
    }
    ;({ Journal } = models)
    eventsService = events.initialize({ models })
  })
  it('calls the publish message function on the journal event', async () => {
    const getEventData = jest.fn()

    const journalPeerReviewModel = generatePeerReviewModel()
    const specialIssuePeerReviewModel = generatePeerReviewModel()

    const journalTeamMember = generateTeamMember({ getEventData })
    const journalTeam = generateTeam({
      members: [journalTeamMember],
    })

    const sectionTeamMember = generateTeamMember({ getEventData })
    const sectionTeam = generateTeam({
      members: [sectionTeamMember],
    })

    const specialIssueTeamMember = generateTeamMember({ getEventData })
    const specialIssueTeam = generateTeam({
      members: [specialIssueTeamMember],
    })
    const sectionSpecialIssueTeamMember = generateTeamMember({ getEventData })
    const sectionSpecialIssueTeam = generateTeam({
      members: [sectionSpecialIssueTeamMember],
    })

    const specialIssue = generateSpecialIssue({
      peerReviewModel: specialIssuePeerReviewModel,
      teams: [specialIssueTeam],
    })

    const sectionSpecialIssue = generateSpecialIssue({
      peerReviewModel: specialIssuePeerReviewModel,
      teams: [sectionSpecialIssueTeam],
    })
    const section = generateSection({
      specialIssues: [sectionSpecialIssue],
      teams: [sectionTeam],
    })

    const journal = generateJournal({
      teams: [journalTeam],
      sections: [section],
      specialIssues: [specialIssue],
      peerReviewModel: journalPeerReviewModel,
      journalArticleTypes: [],
    })

    jest.spyOn(Journal, 'find').mockResolvedValue(journal)

    await eventsService.publishJournalEvent({
      journalId: journal.id,
      eventName: 'JournalUpdated',
    })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(1)
  })
})
