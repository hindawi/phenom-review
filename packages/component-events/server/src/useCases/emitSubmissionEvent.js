const initialize = ({ eventsService, logger }) => ({
  async execute({
    submissionId,
    userId,
    timestamp,
    eventName,
    messageAttributes,
  }) {
    await eventsService.publishSubmissionEvent({
      eventName,
      timestamp,
      submissionId,
      messageAttributes,
    })
    logger.info(
      `The event was triggered by the user with ID: ${userId} at ${new Date().toISOString()}`,
    )
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
