## How to update an event from the event store

In order to update an event from the event store we need to first download the event from S3, then preferably use a script to update the event data and the upload the updated event back in the event store.

### Download events from S3

To be able to download events from S3, we need to setup the AWS credentials on our local machine:

```
vim ~/.aws/credentials
aws_access_key_id = YOUR-KEY-ID
aws_secret_access_key = YOUR-ACCESS-KEY
region=your-region
```

Then we need to run the command which downloads the events from a certain date (i.e. August 13, 2020):

```
aws s3 cp s3://phenom-event-storage-prod-3/2020/08/13 ./s3 --recursive --profile hindawi
```

### Build a script which parses the event and modifies the data

Since the event is stored as a string, we need to first parse it, turn it into a JSON and then write the data back into the file.

```
const fs = require("fs");
const { groupBy, sortBy } = require("lodash");

const filePath =
  "./serverlessrepo-prod-event-storage-pip-BackupStream-1FBAH1QA0ZPP2-3-2020-08-10-16-38-52-ef37cc48-85a2-4a89-b45f-c80723ab41bf";
const file = fs.readFileSync(filePath, "utf-8");
const newLines = [];
for (const line of file.split("\n")) {
  if (line === "") continue;
  const msg = JSON.parse(line);
  const body = JSON.parse(msg.body);

  // only modify a specific event
  if (
    body.event !== "pen:hindawi:hindawi-review:SubmissionQualityChecksSubmitted"
  ) {
    newLines.push(JSON.stringify(msg));
    continue;
  }

  // only modify a specific submission
  if (body.data.submissionId !== "94c79389-bd8f-4ea9-90b0-9c0ecdc10d2f") {
    newLines.push(JSON.stringify(msg));
    continue;
  }

  // very important piece of code which updated the body object

  msg.body = JSON.stringify(body);
  newLines.push(JSON.stringify(msg));
}

const newContent = newLines.join("\n");
fs.writeFileSync(filePath, newContent);
```

### Upload the updated file back into S3

The easiest way to do what is to go into S3, search for the file which contains the event which you modified above, then upload the file from your local machine and you're done.
