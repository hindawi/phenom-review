import {
  WithAwsSecretsServiceProps,
  HindawiServiceChart,
  Cdk8sApp,
} from '@hindawi/phenom-charts'

import { Command } from '../contracts'
import { getOsEnv } from '../env'
import { masterConfig } from '../config.gen'

function camelToKebabCase(str: string) {
  return str.replace(/[A-Z]/g, letter => `-${letter.toLowerCase()}`)
}

function kebabToCamelCase(str: string) {
  return str.replace(/-./g, letter => letter[1].toUpperCase())
}

interface EnvProps {
  tenant: string
  environment: string
  tag: string
  app: string
}
export class BuildManifestCommand implements Command {
  private static parseEnv(): EnvProps {
    const tenant: string = getOsEnv('TENANT')
    const environment: string = kebabToCamelCase(getOsEnv('NODE_ENV'))
    const tag: string = getOsEnv('CI_COMMIT_SHA')
    const app: string = kebabToCamelCase(getOsEnv('APP'))

    return {
      tenant,
      environment,
      tag,
      app,
    }
  }

  async run(): Promise<void> {
    // eslint-disable-next-line no-console
    console.log(this) // force out the eslint warning. do we even need a class here?
    const env = BuildManifestCommand.parseEnv()
    const rootConstruct = new Cdk8sApp({ outdir: 'dist-k8s' })
    let appProps: WithAwsSecretsServiceProps
    try {
      appProps = masterConfig[env.app][env.tenant][env.environment]
      appProps.serviceProps.image!.tag = env.tag
    } catch (error) {
      throw new Error(
        `Did not find configuration for ${env.app}, tenant: ${env.tenant}, environment: ${env.environment}`,
      )
    }

    await HindawiServiceChart.withAwsSecrets(
      rootConstruct,
      camelToKebabCase(env.app),
      appProps,
    )
    rootConstruct.synth()
  }
}
