declare module 'require-glob' {
  export default function(
    patterns: string | Array<string>,
    options?: {
      cwd?: string
    },
  ): Promise<Array<unknown>>

  export function sync(
    patterns: string | Array<string>,
    options?: {
      cwd?: string
    },
  ): Array<unknown>
}
