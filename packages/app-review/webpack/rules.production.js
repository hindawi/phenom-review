const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const include = require('./babel-includes')

module.exports = [
  {
    oneOf: [
      {
        test: /\.(tsx?|jsx?)$/,
        include,
        loader: 'babel-loader',
        options: {
          presets: [
            [require('@babel/preset-env')],
            require('@babel/preset-react'),
            require('@babel/preset-typescript'),
          ],
          plugins: [require('@babel/plugin-proposal-class-properties')],
        },
      },
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: 'javascript/auto',
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          'css-loader',
        ],
      },
      {
        exclude: [/\.(js|jsx)$/, /\.html$/, /\.json$/],
        loader: 'file-loader',
        options: {
          name: 'static/media/[name].[hash:8].[ext]',
        },
      },
    ],
  },
]
