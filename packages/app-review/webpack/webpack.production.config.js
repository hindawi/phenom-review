process.env.NODE_ENV = 'production'
process.env.BABEL_ENV = 'production'

const path = require('path')
const config = require('config')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const rules = require('./rules.production')
const resolve = require('./common-resolve')
const { getGAHead, getGABody } = require('../config/googleAnalytics/scripts')

module.exports = [
  {
    name: 'app',
    target: 'web',
    mode: 'production',
    performance: {
      hints: 'error',
      maxEntrypointSize: 512000,
      maxAssetSize: 512000,
    },
    context: path.join(__dirname, '..', 'app'),
    entry: {
      app: ['./app'],
    },
    output: {
      path: path.join(__dirname, '..', '_build', 'assets'),
      filename: '[name].[hash].js',
      publicPath: '/assets/',
    },
    module: {
      rules,
    },
    resolve,
    plugins: [
      new CleanWebpackPlugin({
        root: path.join(__dirname, '..', '_build'),
      }),
      new HtmlWebpackPlugin({
        title: config.get('publisherConfig.pageTitle'),
        buildTime: new Date().toString(),
        template: '../app/index-production.html',
        inject: 'body',
        gaHead: getGAHead(),
        gaBody: getGABody(),
      }),
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        'process.env.KEYCLOAK': JSON.stringify(config.get('keycloak.public')),
        'process.env.PUBLISHER_NAME':
          JSON.stringify(process.env.PUBLISHER_NAME) || 'hindawi',
        'process.env.GATEWAY_URI': JSON.stringify(process.env.GATEWAY_URI),
        'process.env.EDITOR_EXPERTISE': JSON.stringify(
          config.get('features.EDITOR_EXPERTISE'),
        ),
        'process.env.SHOW_EDITOR_EXPERTISE_FUNCTIONALITY': JSON.parse(
          config.get('features.SHOW_EDITOR_EXPERTISE_FUNCTIONALITY'),
        ),
        'process.env.FEATURE_PPBK4962_SPECIAL_ISSUE': JSON.parse(
          config.get('features.FEATURE_PPBK4962_SPECIAL_ISSUE'),
        ),
      }),
      new webpack.DefinePlugin({
        'process.env.TRANSFER_MANUSCRIPT_URL': JSON.stringify(
          process.env.TRANSFER_MANUSCRIPT_URL,
        ),
      }),
      new webpack.DefinePlugin({
        'process.env.PHENOM_COMMUNICATIONS_API_URL': JSON.stringify(
          process.env.PHENOM_COMMUNICATIONS_API_URL,
        ),
        'process.env.PHENOM_COMMUNICATIONS_EMAIL_SENDER': JSON.stringify(
          process.env.PHENOM_COMMUNICATIONS_EMAIL_SENDER,
        ),
      }),
      new webpack.ContextReplacementPlugin(/./, __dirname, {
        [config.authsome.mode]: config.authsome.mode,
      }),
      new MiniCssExtractPlugin({
        filename: '[name].[hash].css',
        chunkFilename: '[id].css',
      }),
      new CopyWebpackPlugin([
        { from: `../static/${process.env.PUBLISHER_NAME}` },
        {
          from: '../app/silent-check-sso.html',
          to: './silent-check-sso.html',
        },
      ]),
      new webpack.optimize.AggressiveMergingPlugin(),
      new webpack.optimize.OccurrenceOrderPlugin(),
    ],
    node: {
      fs: 'empty',
      __dirname: true,
    },
  },
]
