const path = require('path')

const sep = path.sep.replace('\\', '\\\\')
const notSep = `[^${sep}]`
const dirSuffix = `${notSep}+${sep}`

// paths that use ES6 scripts and CSS modules
// TODO: compile components to ES5 for distribution

module.exports = [
  path.join(__dirname, '..', 'app'),
  new RegExp(`xpub-${dirSuffix}src`),
  new RegExp(`component-${dirSuffix}src`),
  new RegExp(`components-${dirSuffix}src`),
  new RegExp(`pubsweet-${dirSuffix}(?!node_modules)`),
  new RegExp(`@pubsweet${sep}${dirSuffix}(?!node_modules)`),
  // include other packages when this repo is mounted in a workspace
  new RegExp(`packages${sep}${dirSuffix}(?!node_modules)`),
]
