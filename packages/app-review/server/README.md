The purpose of the following document is to describe how we use (or used, depending when you read it) the pubsweet collection of libraries in the Review project.

<br><br><br>

# pubsweet package
Used in `packages/app-review/server/src/app.ts` to start server
```javascript
  const startServer = require('pubsweet/src/startup/start')
```
***! observation***
> In `packages/app-review/server/src/app.ts` we also iterate through the components that have event handlers defined in order to register them with the queue-service service. This is done through a side effect in the `messageQueue` entity in the `queue-service`. we do not directly use this entity.

We user startServer from pubsweet, which requires a pubsweet-server relative to our app and calls the startServer from there.

The webpack bundling also happens in pubsweet package. we can start the server without client bundling, but it does the bundle by default

In `node_modules/pubsweet/src/startup/build.js` they get our predefined webapck configs and build the client apps.

`pubsweet-logger` is already added here for outputing bundling errors.

<br><br>

# pubweet-server package

In `node_modules/pubsweet-server/src/start-server.js` they load the server app configuration we define in `packages/app-review/server/src/app-configurator/configure-app.ts` from the default config file ( if there is no suck custom server, they load the default app.js from the package) together with all the properties. Then they start the newly configured server.

In our configurator, we use the routes from the `pubsweet-server` package (`node_modules/pubsweet-server/src/routes/index.js`).

Here we also load a bunch of other different parts of `pubsweet-server`, like 
`authsome` and `registerComponents` and others from `@pubsweet` scope:
- logger
- errors
- models

<br>

## registerComponents

Iterates over the list of components defined in our config, that we keep in `packages/app-review/config/components.json` and looks for the `server` exported property if there is one  and calls the app defined there by passing the curently defined app like so:
```javascript
  serverComponent()(app)
```

Our components that have servers defined are: 
- `component-files`
- `component-jobs`
- `componenet-sso`
- `component-user-profile`

They basically work as middlewares for the defined server. same principle applies for our app-configurator too.

The rest of our components mainly export resolvers with their use-cases which are loaded into the federated schema in app-configurator -> graphqlApi middleware -> ApolloServer schema, where we iterate through the same predeclared components in the json and get all the resolvers. ( `packages/app-review/server/src/app-configurator/graphql.ts` )

Here we also add the context for our requests with loaders, the current user and helpers which includes authorization ( `node_modules/pubsweet-server/src/helpers/authorization.js` ) that loads our authsome rules.

<br>

## authsome
We use `authsome` from `pubsweet-server` which in turn uses the `authsome` npm package.

In `pubsweet-server` they are creating a new `Authsome` instance with the configurations we pass in `packages/app-review/config/default.js`:authsome property and they also add all our models in the context parameter of the constructor.

They get the models from `@pubsweet/models` which only iterates through our predefined list of components again and looks for the `models` property. We export all our models in the `component-model`.

This new instance of Authsome is saved as a local variable within the application. ( that we do not seem to use directly )
```javascript
    app.locals.authsome = authsome
```

In `packages/app-review/server/src/authsome-mode.js` we keep the policies that get passed to this instance. We will see in a moment how they get applied.

Another way how we use the `authsome` from `pubsweet-server` is through `authorization` helper in their package which holds a few functions that each create an `Authsome` instance the same way as described above ( by loading the policies from config and the models in the context ).

We then set this helper in the grapqhl context here: `packages/app-review/server/src/app-configurator/graphql.ts`

We then use ( only ) the `can` function from here in our `helper-service` which has a `withAuthsomeMiddleware` that gets wrapped around all our reducers. This is where we iterate through all of them and using the context that holds the current user and the policies, we can decide if the policy is enforced or not.

tldr:
- `authsome` from `pubsweet` only creates the instance `Authsome` instance and adds the context
- `authorization` helper from `pubsweet` only has one function that we use in an already existing middleware-wannabe package.

<br><br>

# @pubsweet scope

<br>

## logger

`@pubsweet/logger` is used across all other pubsweet components and was used in our app too. 

How it works is that we pass a logger that can be winston ( an older version of it ) or the extended global console with adjustments ( as we do now ) and that is the logger that is going to be used everywhere.

Because it is so commonly used across all components, `@pubsweet/logger` needs to be one of the lastest ones to be replaced if ever.

<br>

## errors

Defines 4 custom errors that we use directly or not throughout our app:
  - NotFoundError
  - ConflictError
  - ValidationError
  - AuthorizationError

We attach these errors to the global object in `default.js` config file

```javascript
Object.assign(global, require('@pubsweet/errors'))
```

<br>

## component-email-templating & component-send-email

We used to use `@pubsweet/component-send-email` through `@pubsweet/component-email-templating` before we added sendgrid instead by extending the component and replacing the usage.

Now we do not use it anymore. 

`@pubsweet/component-email-templating` can be a good candidate for a first package to remove alltogether

<br>

## db-manager

We only use this package directly in migrations.

Creates an instance of `Knex` with a connection to our db for which it gets the details from config.

It is used by base-model.

In order to replace this component, we need to change/replace base-model first.

<br>

## base-model

Wraps the `Knex` instance into an `Objection` `Model` class, that it extends and adds some extra methods on top.

`hindawiBaseModel.js` extends `@pubsweet/base-model` and each of our models extends `hindawiBaseModel`. 

This way we can use the methods defined in the parents and also, the schema we declare in each model gets loaded into a `jsonSchema` for `Objection` to use.

This can also be a good candidate to for a first package to be replaced.

<br>

## models

All `@pubsweet/models` does is to iterate through our predefined json component list and searches for `models` key. We load them all in `component-model`.

This packages is used to get and load models into the context for example. see `pubsweet-server/authsome` chapter above.

Since it is used in `pubsweet-server/authsome` it is difficult to replace this package without replacing `authsome` first.

<br>

## ui-toolkit

Collection of styles. Mainly used in `@pubsweet/ui` but we also load them directy..

<br>

## ui

Collection of components that are listed as `atoms` or `molecules`. `molecules` components use `atoms` components to create more complex ones.

We use them quite a lot in our application.