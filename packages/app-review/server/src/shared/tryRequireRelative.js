const { logger } = require('component-logger')

const tryRequireRelative = m => {
  let component
  try {
    const componentPath = require.resolve(m, { paths: [process.cwd()] })
    component = require(componentPath)
  } catch (err) {
    logger.error(`Unable to load component ${m} on the server.`)
    logger.error(err)
    component = {}
  }
  return component
}

module.exports = tryRequireRelative
