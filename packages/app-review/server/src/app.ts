// @ts-nocheck
import { logger } from 'component-logger'
import { createQueueService } from '@hindawi/queue-service'
import { forEach } from 'lodash'
import config from 'config'

import startServer from './shared/start'
import tryRequireRelative from './shared/tryRequireRelative'

require('dotenv').config()

// In previous versions of Node.js if there was an unhandled rejection you would get a warning about the rejection and a deprecation warning.
// As of Node.js 15, the default behavior has changed to stopping the node process altogether.
// A workaround for this is handling the unhandled rejection by adding an unhandledRejection listener.
// This will prevent killing our node process, maintaining the current behavior and logging it as error from now on so we can solve them.
process.on('unhandledRejection', reason => {
  logger.error('Unhandled Promise Rejection', reason)
})

const events = {}

function getSchemaRecursively(componentName: string) {
  const component = tryRequireRelative(componentName)

  if (!component) throw new Error(`Could not find component ${componentName}`)

  if (component.eventHandlers) {
    Object.assign(events, component.eventHandlers)
  }
}

if (config.has('pubsweet.components')) {
  config.get('pubsweet.components').forEach((componentName: string) => {
    getSchemaRecursively(componentName)
  })
}

createQueueService()
  .then((messageQueue: unknown) => {
    forEach(events, (handler, event) =>
      messageQueue.registerEventHandler({ event, handler }),
    )
    return messageQueue
  })
  .then(queueService => {
    queueService.start()
    global.applicationEventBus = queueService
  })
  .then(startServer)
  .catch(err => {
    logger.error('FATAL ERROR, SHUTTING DOWN:', err)
    process.exit(1)
  })
