const Promise = require('bluebird')
const { isArray } = require('lodash')

const authsomeMode = async (userId, { name, policies = [] }, object, context) =>
  Promise.reduce(
    policies,
    async (acc, policy) => {
      if (acc === true) return acc

      const getPolicyFn = () => {
        if (typeof policy === 'function') {
          return policy
        }

        if (typeof authsomePolicies[policy] === 'function') {
          return authsomePolicies[policy]
        }

        throw new Error(
          `⛔️ Cannot find policy '${policy}' for action '${name}'.`,
        )
      }

      const fnPolicy = getPolicyFn()

      const res = fnPolicy(userId, name, object, context)

      if (isArray(res)) {
        return checkSubpolicies(userId, name, object, context, res)
      }

      return res
    },
    false,
  )

const checkSubpolicies = (userId, name, object, context, subpoliciesToVerify) =>
  subpoliciesToVerify
    .map(
      subpolicy =>
        subpolicies[subpolicy] &&
        subpolicies[subpolicy](userId, name, object, context),
    )
    .every(Boolean)

const subpolicies = {
  isAuthenticated(userId, name, object, context) {
    return !!userId
  },
  refererIsSameAsOwner(userId, name, object, context) {
    const {
      input: { editorialAssistantId, submittingStaffMemberId },
    } = object

    return userId === editorialAssistantId || userId === submittingStaffMemberId
  },
}

const authsomePolicies = {
  isAuthenticated(userId, name, object, context) {
    return !!userId
  },

  public(userId, name, object, context) {
    return true
  },

  async admin(userId, name, object, context) {
    const { User, Team } = context.models
    const user = await User.find(userId)
    if (!user) return false

    return !!(await user.findTeamMemberByRole(Team.Role.admin))
  },

  async isAuthorOnManuscript(userId, _, object, context) {
    const { Team, TeamMember } = context.models

    return !!(await TeamMember.findOneByManuscriptAndRoleAndUser({
      role: Team.Role.author,
      userId,
      manuscriptId: object.manuscriptId,
    }))
  },

  async isEditorialAssistant(userId, name, object, context) {
    let manuscript
    const { Team, TeamMember, Manuscript } = context.models

    if (object.submissionId) {
      manuscript = await Manuscript.findOneBy({
        queryObject: { submissionId: object.submissionId },
      })
    } else if (object.manuscriptId) {
      manuscript = await Manuscript.find(object.manuscriptId)
    } else if (object.teamMemberId) {
      manuscript = await Manuscript.findManuscriptByTeamMember(
        object.teamMemberId,
      )
    }

    if (!manuscript) return false

    return !!(await TeamMember.findOneByUserAndRoleAndStatusOnManuscript({
      userId,
      manuscriptId: manuscript.id,
      role: Team.Role.editorialAssistant,
      status: TeamMember.Statuses.active,
    }))
  },

  async isAcademicEditorOnManuscript(userId, name, object, context) {
    const { Manuscript, TeamMember, Team } = context.models

    const manuscript = await Manuscript.find(object.manuscriptId)
    if (!manuscript) return false

    return !!(await TeamMember.findOneByUserAndRoleAndStatusOnManuscript({
      userId,
      manuscriptId: manuscript.id,
      role: Team.Role.academicEditor,
      status: TeamMember.Statuses.accepted,
    }))
  },

  async isTriageEditor(userId, name, object, context) {
    const { Manuscript, TeamMember, Team } = context.models

    let manuscript
    if (object.submissionId) {
      manuscript = await Manuscript.findOneBy({
        queryObject: { submissionId: object.submissionId },
      })
    } else if (object.manuscriptId) {
      manuscript = await Manuscript.find(object.manuscriptId)
    } else if (object.teamMemberId) {
      manuscript = await Manuscript.findManuscriptByTeamMember(
        object.teamMemberId,
      )
    }

    if (!manuscript) return false
    if (manuscript.hasTriageEditorConflictOfInterest) return false

    return !!(await TeamMember.findOneByUserAndRoleAndStatusOnManuscript({
      userId,
      manuscriptId: manuscript.id,
      role: Team.Role.triageEditor,
      status: TeamMember.Statuses.active,
    }))
  },

  async hasAccessToManuscript(userId, name, object, context) {
    const { Manuscript, TeamMember } = context.models

    if (!object.manuscriptId) {
      return false
    }
    const manuscript = await Manuscript.find(object.manuscriptId)

    if (!manuscript) {
      return false
    }

    return !!(await TeamMember.findOneByManuscriptAndUser({
      userId,
      manuscriptId: manuscript.id,
    }))
  },

  async hasAccessToSubmission(userId, name, object, context) {
    const { Manuscript, TeamMember, Team } = context.models

    if (!object.submissionId) {
      return false
    }

    const submissionManuscripts = await Manuscript.findManuscriptsBySubmissionId(
      {
        submissionId: object.submissionId,
      },
    )

    if (!submissionManuscripts.length) {
      return false
    }

    return !!(await TeamMember.findOneBySubmissionAndUser({
      userId,
      submissionId: submissionManuscripts[0].submissionId,
      TeamRole: Team.Role,
    }))
  },

  // Temporary policy for redirect
  async hasAccessToSubmissionByCustomId(userId, name, object, context) {
    const { Manuscript } = context.models

    if (!object.customId) return false

    const manuscript = await Manuscript.findByCustomId(object.customId)
    object.submissionId = manuscript.submissionId

    return authsomePolicies.hasAccessToSubmission(userId, name, object, context)
  },

  async isApprovalEditor(userId, name, object, context) {
    const { TeamMember } = context.models

    return TeamMember.isApprovalEditor({
      userId,
      models: context.models,
      manuscriptId: object.manuscriptId,
    })
  },

  async canMakeRecommendation(userId, name, object, context) {
    const { Team, Manuscript } = context.models
    const academicEditorRole = Team.Role.academicEditor
    const manuscript = await Manuscript.find(object.manuscriptId)
    if (!manuscript) return false

    const team = await Team.findOneByManuscriptAndUserAndRole({
      userId,
      role: academicEditorRole,
      manuscriptId: object.manuscriptId,
    })
    if (!team) return false

    return true
  },

  async isReviewerOnReview(userId, name, object, context) {
    const { Team, Review, TeamMember } = context.models
    const review = await Review.find(object.reviewId)
    if (!review) return false

    return !!(await TeamMember.findOneByUserAndRoleAndStatusOnManuscript({
      userId,
      role: Team.Role.reviewer,
      manuscriptId: review.manuscriptId,
      status: TeamMember.Statuses.accepted,
    }))
  },

  refererIsSameAsOwner() {
    return ['isAuthenticated', 'refererIsSameAsOwner']
  },
}

module.exports = authsomeMode
