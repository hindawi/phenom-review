import express from 'express'
import config from 'config'
import passport from 'passport'
import { ApolloServer } from 'apollo-server-express'
import { isEmpty } from 'lodash'
import { logger } from 'component-logger'
import errors from '@pubsweet/errors'
import helpers from '../helpers/authorization'
import loaders from './loaders'
import { getSchema } from './schema'

const authBearerAndPublic = passport.authenticate(['bearer', 'anonymous'], {
  session: false,
})

const hostname = config.has('pubsweet-server.hostname')
  ? config.get('pubsweet-server.hostname')
  : 'localhost'

const extraApolloConfig: any = config.has('pubsweet-server.apollo')
  ? config.get('pubsweet-server.apollo')
  : {}

const api = (app: express.Application) => {
  app.use('/graphql', authBearerAndPublic)
  const server = new ApolloServer({
    schema: getSchema(),
    context: ({ req, res }) => ({
      helpers,
      loaders: loaders(),
      user: req.user,
      req,
      res,
    }),
    formatError: err => {
      const error: any = isEmpty(err.originalError) ? err : err.originalError
      // component-logger uses error.stack path in order to display stacktrace
      if (!error.stack) {
        error.stack = error.extensions?.exception?.stacktrace
      }
      logger.error(error.message, error)

      const isPubsweetDefinedError = Object.values(errors).some(
        (pubsweetError: any) => error instanceof pubsweetError,
      )

      // err is always a GraphQLError which should be passed to the client
      if (!isEmpty(err.originalError) && !isPubsweetDefinedError) {
        return {
          name: 'Server Error',
          message: 'Something went wrong! Please contact help@hindawi.com',
        }
      }

      return {
        name: error.name || 'GraphQLError',
        message: error.message,
        extensions: {
          code: error?.extensions?.code,
        },
      }
    },
    debug: true,
    playground: {
      subscriptionEndpoint: `ws://${hostname}:3000/subscriptions`,
    },
    ...extraApolloConfig,
  })
  server.applyMiddleware({ app })
}

export default api
