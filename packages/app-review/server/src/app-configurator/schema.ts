import config from 'config'
import { merge } from 'lodash'
import { buildFederatedSchema } from '@apollo/federation'
import { gql } from 'apollo-server-express'
import tryRequireRelative from 'pubsweet-server/src/helpers/tryRequireRelative'

const typeDefs = []
const resolvers = {}

// recursively merge in component types and resolvers
function getSchemaRecursively(componentName: string): void {
  const component = tryRequireRelative(componentName)

  if (component.extending) {
    getSchemaRecursively(component.extending)
  }

  if (component.typeDefs) {
    typeDefs.push(component.typeDefs)
  }
  if (component.resolvers) {
    merge(resolvers, component.resolvers)
  }
}

;(config.get('pubsweet.components') as string[]).forEach(componentName => {
  getSchemaRecursively(componentName)
})

function getGraphQLSchemaModule({ typeDefs, resolvers }) {
  const documentNode = gql`
    ${typeDefs}
  `

  return {
    typeDefs: documentNode,
    resolvers,
  }
}

function getSchema() {
  return buildFederatedSchema(getGraphQLSchemaModule({ typeDefs, resolvers }))
}

const { typeDefs: typeDefsSchema } = getGraphQLSchemaModule({
  typeDefs,
  resolvers,
})

export { getSchema, typeDefsSchema }
