import path from 'path'
import crypto from 'crypto'
import config from 'config'

import express from 'express'
import morgan from 'morgan'
import helmet from 'helmet'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import passport from 'passport'
import STATUS from 'http-status-codes'
import { noop } from 'lodash'
import index from 'pubsweet-server/src/routes/index'
import { logger, executionContext, contextStore } from 'component-logger'
import { validateAndRegisterSchema } from '@phenom.pub/schema-registry-cli/lib/register-schema'
import registerComponents from 'pubsweet-server/src/register-components'
import models from '@pubsweet/models'
import authsome from '../helpers/authsome'

import { typeDefsSchema } from './schema'

import gqlApi from './graphql'

declare global {
  interface Crypto {
    randomUUID: () => string
  }
}

type PubsweetApplication = {
  onListen: () => void
  onClose: () => void
} & express.Application

const configureApp = (app: PubsweetApplication) => {
  app.locals.models = models

  app.use(bodyParser.json({ limit: '50mb' }))

  // * AsyncLocalStorage middleware
  app.use((req, res, next) => {
    executionContext.run(contextStore, () => {
      const uuid = (crypto as any).randomUUID()
      ;(executionContext.getStore() as any).set('correlationId', uuid)
      ;(req as any).correlationId = uuid
      next()
    })
  })

  morgan.token<express.Request, express.Response>(
    'correlationId',
    req => (req as any).correlationId,
  )

  morgan.token<express.Request, express.Response>(
    'graphql',
    ({ body }, _, type) => {
      // ! For some reasons, body.operationName is not populated
      // ! for GraphQL calls
      // if (!body.operationName) return ''

      switch (type) {
        case 'query':
          return body.query.replace(/\s+/g, ' ')
        case 'variables':
          return JSON.stringify(body.variables)
        case 'operation':
        default:
          return body.operationName
      }
    },
  )

  const serverConfigs = config.get('pubsweet-server') as any
  app.use(morgan(serverConfigs.morganLogFormat || 'combined'))
  app.use(bodyParser.urlencoded({ extended: false }))
  app.use(cookieParser())
  app.use(helmet())
  app.use(
    express.static(path.resolve('.', '_build'), {
      setHeaders(res, path, stat) {
        res.set('Cache-Control', 'public, max-age=3600')
      },
    }),
  )

  // Passport strategies
  app.use(passport.initialize())
  const authentication = require('pubsweet-server/src/authentication')

  // Register passport authentication strategies
  passport.use('bearer', authentication.strategies.bearer)
  passport.use('anonymous', authentication.strategies.anonymous)
  passport.use('local', authentication.strategies.local)

  app.locals.passport = passport
  app.locals.authsome = authsome

  registerComponents(app)

  // GraphQL API
  gqlApi(app)

  const cacheControl = (req, res, next) => {
    res.set('Cache-Control', 'public, no-cache, max-age=3600')
    next()
  }

  // Serve the index page for front end
  app.use('/', cacheControl, index)

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  app.use((err, _, res, __) => {
    // development error handler, will print stacktrace
    logger.error('Something went wrong', err)

    if (err.name === 'ValidationError') {
      return res.status(STATUS.BAD_REQUEST).json({ message: err.message })
    } else if (err.name === 'ConflictError') {
      return res.status(STATUS.CONFLICT).json({ message: err.message })
    } else if (err.name === 'AuthorizationError') {
      return res.status(err.status).json({ message: err.message })
    } else if (err.name === 'AuthenticationError') {
      return res.status(STATUS.UNAUTHORIZED).json({ message: err.message })
    }
    return res
      .status(err.status || STATUS.INTERNAL_SERVER_ERROR)
      .json({ message: err.message })
  })

  app.onListen = noop
  app.onClose = noop

  // register schema
  ;(async () => {
    const service = {
      name: 'app-review',
      url: config.get('serviceSchemaUrl'),
    }

    try {
      await validateAndRegisterSchema(service, typeDefsSchema)

      logger.info('Schema registered successfully!')
    } catch (err) {
      logger.error(`Schema registration failed: ${err.message}`)
      process.exit(1)
    }
  })()

  return app
}

export default configureApp
