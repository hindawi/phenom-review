const AuthorizationError = require('./errors/AuthorizationError')
const { logger } = require('component-logger')

// check permissions or throw authorization error
const can = async (userId, verb, entity) => {
  const authsome = require('./authsome')
  const permission = await authsome.can(userId, verb, entity)
  if (!permission) {
    logger
      .addLabel('userId', entity.userId)
      .addLabel('manuscriptId', entity.manuscriptId)
      .addLabel('submissionId', entity.submissionId)
      .addLabel('fileId', entity.fileId)
      .error(
        new AuthorizationError(
          ` Operation not permitted: ${userId ||
            'unauthenticated users'} cannot perform ${verb} operation on ${entity.type ||
            entity}`,
        ),
      )
  }
  // return identity if no filter function
  return permission.filter || (id => id)
}
module.exports = {
  can,
}
