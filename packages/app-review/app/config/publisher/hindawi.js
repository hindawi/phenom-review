const REVIEWERS_GUIDELINES_LINK =
  'https://www.hindawi.com/publish-research/reviewers/'
const EDITORIAL_THRESHOLD_GUIDE_LINK =
  'https://editors.hindawi.com/page/editorial-threshold'

module.exports = {
  logo: '/assets/logo.png',
  name: 'Hindawi',
  supportEmail: 'help@hindawi.com',
  pageTitle: 'Hindawi Review',
  links: {
    transferManuscriptLink: 'https://www.hindawi.com/transfer-manuscript',
    websiteLink: 'https://www.hindawi.com/',
    privacyLink: 'https://www.hindawi.com/privacy/',
    termsLink: 'https://www.hindawi.com/terms/',
    ethicsLink: 'https://www.hindawi.com/ethics/',
    coiLink: 'https://www.hindawi.com/ethics/#conflicts-of-interest',
    dataAvailabilityLink:
      'https://www.hindawi.com/publish-research/authors/research-data/',
    apcLink: 'https://www.hindawi.com/journals/{journalCode}/apc',
    authorsGuidelinesLink: 'https://www.hindawi.com/publish-research/authors/',
    journalAuthorsGuidelinesLink:
      'https://www.hindawi.com/journals/{journalCode}/guidelines/',
    reviewLink: 'https://review.hindawi.com',
    invoicingLink: 'https://invoicing.hindawi.com',
    authorServiceLink:
      'https://www.hindawi.com/publish-research/authors/author-services/?utm_source=email&utm_medium=marketing%20referral&utm_campaign=HDW_MRKT_GBL_AWA_EMIL_OWN_AUTH_HIND_X2_10170',
  },
  emailData: {
    logo:
      'https://s3-eu-west-1.amazonaws.com/review.hindawi.com/logo-hindawi.png',
    ctaColor: '#63a945',
    logoLink: 'https://hindawi.com',
    shortReviewLink: 'review.hindawi.com',
    privacy:
      'Hindawi respects your right to privacy. Please see our <a style="color: #007e92; text-decoration: none;" href="https://www.hindawi.com/privacy/">privacy policy</a> for information on how we store, process, and safeguard your data.',
    address:
      'Hindawi Limited, 3rd Floor, Adam House, 1 Fitzroy Square, London, W1T 5HF, United Kingdom',
    publisher: 'Hindawi Limited',
    footerText: {
      unregisteredUsers: `This email was sent to {recipientEmail}. You have received this email in regards to the account creation, submission, or peer review process of a submitted paper, published by Hindawi Limited.`,
      registeredUsers: `This email was sent to {recipientEmail}. You have received this email in regards to the account creation, submission, or peer review process of a submitted paper, published by Hindawi Limited.`,
    },
    communicationServiceMailLogo:
      'https://s3-eu-west-1.amazonaws.com/review.hindawi.com/logo-hindawi.png',
  },
  recommendationScreenInfoBox: {
    title:
      'A reminder that recommendations should not be made solely on novelty or perceived impact/interest',
    description: `
      <p>Research published in the journal must be:</p>
      <ul>
        <li>In scope of the journal;</li>
        <li>Scientifically valid;</li>
        <li>Technically accurate in its methods and results;</li>
        <li>Representative of a specific advance, or replication, or null/negative result, which is worthy of publication;</li>
        <li>As reproducible as possible;</li>
        <li>Ethically sound.</li>
      </ul>
      <p>We do not impose any page or article limitations, and reviews should not take these into consideration.</p>
      <p>Please provide authors with detailed and constructive feedback.</p>
      <p>For more information, please read our <a href=${REVIEWERS_GUIDELINES_LINK} title="Reviewer Guidelines" target="_blank" rel="noreferrer">Reviewer Guidelines</a>.</p>
    `,
  },
  editorFinalDecisionInfoBox: {
    title:
      'A reminder that decisions should not be made solely on novelty or perceived impact/interest.<br/>Any reviewer recommendations to reject simply on this basis can be disregarded.',
    description: `<p>Research published in the journal must be:</p>
      <ul>
        <li>In scope of the journal;</li>
        <li>Scientifically valid;</li>
        <li>Technically accurate in its methods and results;</li>
        <li>Representative of a specific advance, or replication, or null/negative result, which is worthy of publication;</li>
        <li>As reproducible as possible;</li>
        <li>Ethically sound.</li>
      </ul>
      <p>We do not impose any page or article limitations, and decisions should not take these into consideration.</p>
      <p>Please provide authors with detailed and constructive feedback.</p>
      <p>For more information, please read our <a href="${EDITORIAL_THRESHOLD_GUIDE_LINK}" title="Hindawi’s Editorial Threshold" target="_blank" rel="noreferrer">Editorial Threshold Guide for Editors</a>.</p>`,
  },
  rejectDecisionReasonsTranslations: {
    outOfScope: 'Out of scope',
    technicalOrScientificFlaws: 'Technical/scientific flaws',
    publicationEthicsConcerns: 'Publication ethics concerns',
    lackOfNovelty: 'Lack of novelty or perceived impact',
    otherReasons: 'Other reason(s)',
  },
}
