import 'react-app-polyfill/ie9'
import 'react-app-polyfill/ie11'
import 'react-app-polyfill/stable'
import React from 'react'
import ReactDOM from 'react-dom'
import { theme } from '@hindawi/ui'
import { ModalProvider } from 'component-modal'
import { KeycloakProvider, keycloak } from 'component-sso/client'
import { JournalProvider as HindawiContextProvider } from 'component-journal-info'
import { BrowserRouter } from 'react-router-dom'
import { ApolloProvider } from '@apollo/react-components'

import Root from './Root'
import Routes from './routes'
import * as publishers from './config/publisher'
import apolloClientFactory from './apolloClientFactory'

import './styles.css'

const render = keycloak => {
  document.title = publishers[process.env.PUBLISHER_NAME].name
  ReactDOM.render(
    <React.Fragment>
      <ModalProvider>
        <HindawiContextProvider
          publisher={publishers[process.env.PUBLISHER_NAME]}
        >
          <KeycloakProvider keycloak={keycloak}>
            <BrowserRouter>
              <ApolloProvider client={apolloClientFactory()}>
                <Root routes={<Routes />} theme={theme} />
              </ApolloProvider>
            </BrowserRouter>
          </KeycloakProvider>
        </HindawiContextProvider>
      </ModalProvider>
      <div id="ps-modal-root" style={{ height: 0 }} />
      <div id="portal-root" style={{ height: 0 }} />
    </React.Fragment>,
    document.getElementById('root'),
  )
}

const renderError = (keycloakError = {}, keycloak) => {
  const { error = 'An error occurred.', error_description } = keycloakError
  if (error === 'access_denied') {
    keycloak.login()
  }
  ReactDOM.render(
    <React.Fragment>
      <h1 style={{ textAlign: 'center' }}>
        {error_description ? error.error_description : error}
      </h1>
      <p style={{ textAlign: 'center' }}>
        <a href="/">Go back</a>
      </p>
    </React.Fragment>,
    document.getElementById('root'),
  )
}
const keycloakEnv = process.env.KEYCLOAK
if (keycloakEnv && keycloakEnv.authServerURL) {
  keycloak.init(keycloakEnv, render, renderError)
} else {
  render()
}
