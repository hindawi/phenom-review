import React from 'react'
import PropTypes from 'prop-types'
import { ThemeProvider, createGlobalStyle } from 'styled-components'
import { normalize } from 'styled-normalize'

// @tippy.js/react styles
import 'tippy.js/dist/tippy.css'
import 'tippy.js/themes/light.css'

import HindawiApp from './HindawiApp'

const Root = ({ routes, theme }) => (
  <ThemeProvider theme={theme}>
    <HindawiApp>
      <GlobalStyles />
      {routes}
    </HindawiApp>
  </ThemeProvider>
)

Root.propTypes = {
  routes: PropTypes.node.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  theme: PropTypes.object.isRequired,
}

const GlobalStyles = createGlobalStyle`
  ${normalize}

  body {
    height: 100vh;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale; 
  }

  .ant-popover-inner-content {
    width: 524px;
    max-height: 320px;
  }
`

export default Root
