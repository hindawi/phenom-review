import React from 'react'
import { Route } from 'react-router'
import { DragDropContext } from 'react-dnd'
import { Footer } from '@hindawi/ui'
import HTML5Backend from 'react-dnd-html5-backend'
import styled from 'styled-components'
import { AppBar } from 'component-authentication/client'
import { th } from '@pubsweet/ui-toolkit'

import { useJournal } from 'component-journal-info'
import { performHotjarCheck } from './hotjar-check'

const HideOnPath = ({ component: Component, pathnames }) => (
  <Route
    render={({ location }) => {
      if (pathnames.includes(location.pathname)) return null
      return <Component />
    }}
  />
)

const App = ({ autosave, journal = {}, goTo, children }) => {
  const {
    supportEmail,
    name,
    links: { websiteLink, privacyLink, termsLink },
  } = useJournal()

  performHotjarCheck()

  const hiddenAppBarPaths = ['/404', '/subscribe', '/unsubscribe']
  const hiddenFooterPaths = ['/404']

  return (
    <Root>
      <HideOnPath component={() => <AppBar />} pathnames={hiddenAppBarPaths} />

      <PageContent>{children}</PageContent>

      <HideOnPath
        component={() => (
          <Footer
            privacyLink={privacyLink}
            publisher={name}
            supportEmail={supportEmail}
            termsLink={termsLink}
            websiteLink={websiteLink}
          />
        )}
        pathnames={hiddenFooterPaths}
      />
    </Root>
  )
}

export default DragDropContext(HTML5Backend)(App)

const Root = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${th('backgroundColor')};
  font-family: ${th('defaultFont')}, sans-serif;
  font-size: ${th('mainTextSize')};
  color: ${th('colorText')};
  line-height: ${th('mainTextLineHeight')};

  height: 100vh;
  overflow: hidden;

  * {
    box-sizing: border-box;
  }
`

const PageContent = styled.main`
  flex: 1;
  overflow-y: auto;
`
