import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import merge from 'lodash/merge'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = merge({}, defaultValues, {
  secretNames: ['demo/gsw/review/app'],
  serviceProps: {
    image: {
      repository: '916437579680.dkr.ecr.eu-west-1.amazonaws.com/review-app',
      tag: 'demo',
    },
    ingressOptions: {
      rules: [
        {
          host: 'review.demo-gsw.phenom.pub',
        },
      ],
    },
  },
})
