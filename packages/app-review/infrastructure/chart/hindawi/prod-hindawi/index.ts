import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import merge from 'lodash/merge'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = merge({}, defaultValues, {
  secretNames: ['prod/review/app'],
  serviceProps: {
    replicaCount: 2,
    image: {
      repository: '483652493954.dkr.ecr.eu-west-1.amazonaws.com/review-app',
      tag: 'production',
    },
    ingressOptions: {
      rules: [
        {
          host: 'review.prod.phenom.pub',
        },
        {
          host: 'review.hindawi.com',
        },
      ],
    },
  },
})
