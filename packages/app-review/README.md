## Git Branching/Deployment strategy

Documentation can be found [here](https://hindawi.atlassian.net/wiki/spaces/PPT/pages/2461728773/Git+branching+deploying+strategy#Proposed-solution)

We are using [Github Flow](https://guides.github.com/introduction/flow/), with the following considerations:

- At any merge request from feature branch to 'master':

  - run lint & test jobs automatically for each new commit
  - manual deploy to _automation_
  - manual e2e tests run
  - manually deploy to _qa_

- When the branch is merged intro master:

  - deploy to demo automatically
  - manually deploy to production

### Observation:

- Before merging the branch always rebase that branch over master so we can assure that we have the latest code there ( MR is restricted to always use Fast-forward merges )
- It is recommended that we create a separate MR for each change in dependencies in order to avoid complicated conflicts

### Pipelines specificities:

```
  tags:
    - 'cluster:hindawi-dev'
```

this tag will make the job work on a runner in the hindawi-dev cluster. The same with `hindawi-prod` and its environment

---

`build`: builds the image and puts it in the local gitlab container registry

variables:

- DOCKERFILE_PATH: where in the runner container to find the dockerfile
- GITLAB_REGISTRY_REPO: which repo to upload the newly created image to

---

`push`: gets the image from gitlab cr and pushes it to the container registry in AWS

variables:

- GIT_STRATEGY: [GIT_STRATEGY](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#git-strategy)
- GITLAB_REGISTRY_REPO: what gitlab repo to get the image from
- ECR_URL: ecr path to push the image to
- ECR_TAG: tag to put on the image

---

`generate-manifest`: creates the deployment that is saved in artifacts and used in the `deploy` step

variables:

- CLUSTER: self explanatory
- TENANT: hindawi or gsw
- NODE_ENV: environment (qa, demo, prod-hindawi, hindawi-gsw etc)
- IMAGE_TAG: either \$CI_COMMIT_SHA or any other tag we want to use for our image ( production, demo, latest )
- APP: application name ( app-review )

---

`deploy`: applies the generated manifest to the cluster

variables:

- NODE_ENV: environment (qa, demo, prod-hindawi, hindawi-gsw etc)
- APP: application name ( app-review )
