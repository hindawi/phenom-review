const config = require('config')
const sgTransport = require('nodemailer-sendgrid-transport')

const options = {
  auth: {
    api_key: config.sendgridApiKey,
  },
}

module.exports = {
  transport: sgTransport(options),
}
