const gtmContainerId = process.env.GTM_CONTAINER_ID || 'GTM-MQ4MGW'

const getGAHead = () => `
<!-- Google Tag Manager -->
  <script>
    window.isGTMActive = false;
    window.initGTM = function() {
      window.isGTMActive = true;
      (function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
          'gtm.start': new Date().getTime(),
          event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
          j = d.createElement(s),
          dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
      })(window, document, 'script', 'dataLayer', '${gtmContainerId}');
    }
  </script>
<!-- End Google Tag Manager -->`

const getGABody = () => `
<!-- Google Tag Manager (noscript) -->
  <noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=${gtmContainerId}" height="0" width="0" style="display:none;visibility:hidden"></iframe>
  </noscript>
<!-- End Google Tag Manager (noscript) -->`

module.exports = {
  getGAHead,
  getGABody,
}
