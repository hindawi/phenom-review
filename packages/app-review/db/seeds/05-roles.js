exports.seed = async knex => {
  await knex('role').del()
  // Deletes ALL existing entries
  await knex('role').insert([
    { name: 'admin' },
    { name: 'user' },
    { name: 'author' },
    { name: 'reviewer' },
    { name: 'triageEditor' },
    { name: 'academicEditor' },
    { name: 'editorialAssistant' },
    { name: 'submittingStaffMember' },
    { name: 'researchIntegrityPublishingEditor' },
  ])
}
