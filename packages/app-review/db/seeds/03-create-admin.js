const uuid = require('uuid')
const config = require('config')

exports.seed = async knex => {
  const userId = uuid.v4()
  const adminEmail = config.get('adminEmail')
  const SSOService = process.env.KEYCLOAK_SERVER_URL
    ? require('component-sso')
    : null

  const userDetails = {
    id: userId,
    agree_tc: true,
    is_active: true,
    default_identity_type: 'local',
    is_subscribed_to_emails: true,
    unsubscribe_token: uuid.v4(),
  }

  await knex('user').insert([userDetails])

  const alias = {
    given_names: 'Admin',
    surname: 'Admin',
    title: 'dr',
    aff: 'Affiliation',
    country: 'UK',
    email: adminEmail,
  }

  const identity = {
    user_id: userId,
    type: 'local',
    is_confirmed: true,
    ...alias,
    password_hash:
      '$2b$12$.Ll6THdFQ1Tk26WSst75tu9/PvNjAjqu2xhkf9CZAIlomEh.Ah0pS',
  }

  await knex('identity').insert([identity])

  const teamId = uuid.v4()
  await knex('team').insert([
    {
      id: teamId,
      role: 'admin',
    },
  ])

  await knex('team_member').insert([
    {
      alias,
      team_id: teamId,
      user_id: userId,
      status: 'pending',
    },
  ])

  if (SSOService) {
    await SSOService.createSSOUser({
      identity,
      user: userDetails,
    })
    await SSOService.updateUserRoleInSSO(adminEmail, 'admin')
  }
}
