const config = require('config')

const publisherName = config.get('publisherName')

exports.seed = async knex => {
  if (publisherName === 'hindawi') {
    await knex('peer_review_model').insert([
      {
        name: 'Chief Minus',
        approval_editors: ['triageEditor'],
        has_figurehead_editor: false,
        has_triage_editor: true,
        triage_editor_label: 'Chief Editor',
        triage_editor_assignment_tool: '{"manual"}',
        academic_editor_label: 'Academic Editor',
        academic_editor_automatic_invitation: false,
        reviewer_assignment_tool: '{"manual","publons"}',
      },
      {
        name: 'Single Tier Academic Editor',
        approval_editors: ['academicEditor'],
        has_figurehead_editor: false,
        has_triage_editor: false,
        academic_editor_label: 'Academic Editor',
        academic_editor_automatic_invitation: true,
        reviewer_assignment_tool: '{"manual","publons"}',
      },
      {
        name: 'Associate Editor',
        approval_editors: ['academicEditor'],
        has_figurehead_editor: true,
        figurehead_editor_label: 'Chief Editor',
        has_triage_editor: true,
        triage_editor_label: 'Associate Editor',
        triage_editor_assignment_tool: '{"manual"}',
        academic_editor_label: 'Academic Editor',
        academic_editor_automatic_invitation: false,
        reviewer_assignment_tool: '{"manual","publons"}',
      },
      {
        name: 'Special Issue Associate Editor',
        approval_editors: ['academicEditor'],
        has_figurehead_editor: false,
        has_triage_editor: true,
        triage_editor_label: 'Lead Guest Editor',
        triage_editor_assignment_tool: '{"manual"}',
        academic_editor_label: 'Guest Editor',
        academic_editor_automatic_invitation: false,
        reviewer_assignment_tool: '{"manual","publons"}',
      },
      {
        name: 'Section Editor',
        approval_editors: ['academicEditor'],
        has_figurehead_editor: false,
        has_triage_editor: true,
        triage_editor_label: 'Section Editor',
        triage_editor_assignment_tool: '{"manual"}',
        academic_editor_label: 'Academic Editor',
        academic_editor_automatic_invitation: false,
        reviewer_assignment_tool: '{"manual","publons"}',
      },
    ])
  }
  if (publisherName === 'gsw') {
    await knex('peer_review_model').insert([
      {
        name: 'Lithosphere Model',
        approval_editors: ['triageEditor'],
        has_figurehead_editor: false,
        has_triage_editor: true,
        triage_editor_label: 'Section Editor',
        triage_editor_assignment_tool: '{"manual"}',
        academic_editor_label: 'Handling Editor',
        academic_editor_automatic_invitation: false,
        reviewer_assignment_tool: '{"manual","publons"}',
      },
      {
        name: 'Special Issue Associate Editor',
        approval_editors: ['academicEditor'],
        has_figurehead_editor: false,
        has_triage_editor: true,
        triage_editor_label: 'Lead Guest Editor',
        triage_editor_assignment_tool: '{"manual"}',
        academic_editor_label: 'Guest Editor',
        academic_editor_automatic_invitation: false,
        reviewer_assignment_tool: '{"manual","publons"}',
      },
    ])
  }
  if (publisherName !== 'hindawi' && publisherName !== 'gsw') {
    await knex('peer_review_model').insert([
      {
        name: 'Chief Minus',
        approval_editors: ['triageEditor'],
        has_figurehead_editor: false,
        has_triage_editor: true,
        triage_editor_label: 'Chief Editor',
        triage_editor_assignment_tool: '{"manual"}',
        academic_editor_label: 'Academic Editor',
        academic_editor_automatic_invitation: false,
        reviewer_assignment_tool: '{"manual","publons"}',
      },
    ])
  }
}
