exports.seed = async knex => {
  // Deletes ALL existing entries
  await knex('submission_status').del()

  // Inserts seed entries
  await knex('submission_status').insert([
    {
      name: 'draft',
      category: 'inProgress',
    },
    {
      name: 'technicalChecks',
      category: 'inProgress',
    },
    {
      name: 'submitted',
      category: 'inProgress',
    },
    {
      name: 'academicEditorInvited',
      category: 'inProgress',
    },
    {
      name: 'academicEditorAssigned',
      category: 'inProgress',
    },
    {
      name: 'reviewersInvited',
      category: 'inProgress',
    },
    {
      name: 'underReview',
      category: 'inProgress',
    },
    {
      name: 'reviewCompleted',
      category: 'inProgress',
    },
    {
      name: 'revisionRequested',
      category: 'inProgress',
    },
    {
      name: 'pendingApproval',
      category: 'inProgress',
    },
    {
      name: 'rejected',
      category: 'rejected',
    },
    {
      name: 'inQA',
      category: 'inProgress',
    },
    {
      name: 'accepted',
      category: 'approved',
    },
    {
      name: 'withdrawn',
      category: 'withdrawn',
    },
    {
      name: 'deleted',
      category: 'withdrawn',
    },
    {
      name: 'void',
      category: 'withdrawn',
    },
    {
      name: 'published',
      category: 'approved',
    },
    {
      name: 'olderVersion',
      category: 'withdrawn',
    },
    {
      name: 'academicEditorAssignedEditorialType',
      category: 'inProgress',
    },
    {
      name: 'makeDecision',
      category: 'inProgress',
    },
    {
      name: 'qualityChecksRequested',
      category: 'inProgress',
    },
    {
      name: 'qualityChecksSubmitted',
      category: 'approved',
    },
    {
      name: 'refusedToConsider',
      category: 'rejected',
    },
  ])
}
