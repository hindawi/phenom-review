const config = require('config')
const gswJournals = require('./gswJournals.json')

const publisherName = config.get('publisherName')

exports.seed = async knex => {
  if (publisherName === 'gsw') {
    const updates = gswJournals.map(({ journalTitle, eissn, pissn }) =>
      knex('source_journal')
        .update({ eissn, pissn })
        .where({ name: journalTitle }),
    )

    await Promise.all(updates)
  }
}
