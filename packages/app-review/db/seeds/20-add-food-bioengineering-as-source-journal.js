const config = require('config')

const publisherName = config.get('publisherName')

exports.seed = async knex => {
  if (publisherName === 'hindawi') {
    await knex('source_journal').insert([
      {
        name: 'Food Bioengineering',
        eissn: '2770-2081',
        pissn: '2770-2081',
        publisher: 'Wiley',
      },
    ])
  }
}
