const config = require('config')
const wileyJournals = require('./wileyJournals.json')

const publisherName = config.get('publisherName')

exports.seed = async knex => {
  if (publisherName === 'hindawi') {
    const updates = wileyJournals.map(({ journalTitle }) =>
      knex('source_journal')
        .update({ publisher: 'Wiley' })
        .where({ name: journalTitle }),
    )

    await Promise.all(updates)
  }
}
