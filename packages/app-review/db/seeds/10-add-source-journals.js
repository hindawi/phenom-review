const config = require('config')

const publisherName = config.get('publisherName')

exports.seed = async knex => {
  // Deletes ALL existing entries
  await knex('source_journal').del()
  if (publisherName === 'gsw') {
    await knex('source_journal').insert([
      { name: 'AAPG Bulletin', pissn: '0149-1423' },
      { name: 'Geology', pissn: '0091-7613' },
      { name: 'GSA Bulletin', pissn: '0016-7606' },
      { name: 'Journal of the Geological Society', pissn: '0016-7649' },
      {
        name: 'Quarterly Journal of Engineering Geology and Hydrogeology',
        pissn: '1470-9236',
      },
      { name: 'American Mineralogist', pissn: '0003-004X' },
      { name: 'Geophysics', pissn: '0016-8033' },
      { name: 'Interpretation', pissn: '2324-8858' },
      { name: 'The Leading Edge', pissn: '1070-485X' },
      { name: 'Economic Geology', pissn: '0361-0128' },
      { name: 'Journal of Sedimentary Research', pissn: '1527-1404' },
      { name: 'PALAIOS', pissn: '0883-1351' },
      { name: 'Special Publications GSL', pissn: '0305-8719' },
      { name: 'Engineering Geology Special Publications', pissn: '0267-9914' },
      { name: 'Memoirs GSL', pissn: '0435-4052' },
      { name: 'The Sedimentary Record', pissn: '1543-8740' },
      { name: 'Geosphere', pissn: '' },
      { name: 'Lithosphere', pissn: '1947-4253' },
    ])
  }
}
