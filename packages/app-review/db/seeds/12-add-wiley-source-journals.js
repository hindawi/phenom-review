const config = require('config')
const wileyJournals = require('./wileyJournals.json')

const publisherName = config.get('publisherName')

exports.seed = async knex => {
  if (publisherName === 'hindawi') {
    const mappedWileyJournals = wileyJournals.map(
      ({ pissn, eissn, journalTitle }) => ({
        pissn,
        eissn,
        name: journalTitle,
      }),
    )

    await knex('source_journal').insert(mappedWileyJournals)
  }
}
