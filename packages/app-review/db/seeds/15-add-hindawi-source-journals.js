const config = require('config')
const hindawiJournals = require('./hindawiJournals.json')

const publisherName = config.get('publisherName')

exports.seed = async knex => {
  if (publisherName === 'hindawi') {
    const mappedHindawiJournals = hindawiJournals.map(
      ({ eissn, journalTitle }) => ({
        eissn,
        name: journalTitle,
        publisher: 'Hindawi',
      }),
    )

    await knex('source_journal').insert(mappedHindawiJournals)
  }
}
