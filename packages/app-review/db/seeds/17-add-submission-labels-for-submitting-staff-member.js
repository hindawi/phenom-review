const getStatuses = knex => knex('submission_status').select('id', 'name')
const getRoles = knex => knex('role').select('id', 'name')
const getFilters = knex => knex('submission_filter').select('id', 'name')

const mapping = {
  submittingStaffMember: {
    technicalChecks: {
      filter: 'needsAttention',
      name: 'Approve QA',
    },
    submitted: {
      filter: 'needsAttention',
      name: 'Assign AE',
    },
    academicEditorInvited: {
      filter: 'needsAttention',
      name: 'Respond to Invite',
    },
    academicEditorAssigned: {
      filter: 'needsAttention',
      name: 'Invite Reviewers',
    },
    reviewersInvited: {
      filter: 'needsAttention',
      name: 'Respond to Invite',
    },
    underReview: {
      filter: 'needsAttention',
      name: 'Complete Review',
    },
    reviewCompleted: {
      filter: 'needsAttention',
      name: 'Make Recommendation',
    },
    revisionRequested: {
      filter: 'needsAttention',
      name: 'Submit Revision',
    },
    pendingApproval: {
      filter: 'needsAttention',
      name: 'Make Decision',
    },
    rejected: {
      filter: 'archived',
      name: 'Rejected',
    },
    inQA: {
      filter: 'needsAttention',
      name: 'Approve QA',
    },
    accepted: {
      name: 'Accepted',
    },
    withdrawn: {
      filter: 'archived',
      name: 'Withdrawn',
    },
    deleted: {
      filter: 'archived',
      name: 'Deleted',
    },
    published: {
      name: 'Published',
    },
    academicEditorAssignedEditorialType: {
      filter: 'needsAttention',
      name: 'Make Decision',
    },
    makeDecision: {
      filter: 'needsAttention',
      name: 'Make Decision',
    },
    qualityChecksRequested: {
      name: 'Accepted',
    },
    qualityChecksSubmitted: {
      name: 'Accepted',
    },
    refusedToConsider: {
      name: 'Refused To Consider',
    },
  },
}

exports.seed = async knex => {
  // get current statuses
  const statuses = await getStatuses(knex)
  // get current roles
  const roles = await getRoles(knex)
  // get current filters
  const filters = await getFilters(knex)
  // compute entries
  const labels = []

  const getFilterId = ({ roleName, statusName, filters, mapping }) => {
    const filter = filters.find(
      filter =>
        mapping[roleName] &&
        mapping[roleName][statusName] &&
        mapping[roleName][statusName].filter &&
        filter.name === mapping[roleName][statusName].filter,
    )
    if (filter && filter.id) return filter.id
  }

  const getPriority = ({ roleName, statusName, mapping }) =>
    mapping[roleName] &&
    mapping[roleName][statusName] &&
    mapping[roleName][statusName].priority

  roles.forEach(role => {
    statuses.forEach(status => {
      if (mapping[role.name] && mapping[role.name][status.name]) {
        labels.push({
          role_id: role.id,
          submission_status_id: status.id,
          submission_filter_id: getFilterId({
            roleName: role.name,
            statusName: status.name,
            filters,
            mapping,
          }),
          name: mapping[role.name][status.name].name,
          priority: getPriority({
            roleName: role.name,
            statusName: status.name,
            mapping,
          }),
        })
      }
    })
  })
  // insert entries
  await knex('submission_label').insert(labels)
}
