const config = require('config')

const publisherName = config.get('publisherName')

exports.seed = async knex => {
  if (publisherName === 'hindawi') {
    await knex('source_journal').insert([
      {
        name: 'Health Care Science',
        eissn: '2771-1757',
        publisher: 'Wiley',
      },
    ])
  }
}
