// used to add available custom ids from 1000000 to 1099999 in random order
exports.seed = async knex => {
  const generatedCustomIds = []

  for (let i = 1000000; i <= 1099999; i += 1) {
    generatedCustomIds.push({ custom_id: i.toString() })
  }

  generatedCustomIds.sort((_a, _b) => 0.5 - Math.random())

  for (let i = 0; i < generatedCustomIds.length; i += 10000) {
    const limit = Math.min(i + 10000, generatedCustomIds.length)
    const batch = generatedCustomIds.slice(i, limit)

    // eslint-disable-next-line no-await-in-loop
    await knex('available_custom_ids').insert(batch)
  }
}
