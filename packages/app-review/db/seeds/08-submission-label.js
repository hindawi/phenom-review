const getStatuses = knex => knex('submission_status').select('id', 'name')
const getRoles = knex => knex('role').select('id', 'name')
const getFilters = knex => knex('submission_filter').select('id', 'name')

const mapping = {
  author: {
    draft: {
      filter: 'needsAttention',
      name: 'Complete Submission',
    },
    technicalChecks: {
      filter: 'inProgress',
      name: 'Submitted',
    },
    submitted: {
      filter: 'inProgress',
      name: 'Submitted',
    },
    academicEditorInvited: {
      filter: 'inProgress',
      name: 'AE Invited',
    },
    academicEditorAssigned: {
      filter: 'inProgress',
      name: 'AE Assigned',
    },
    reviewersInvited: {
      filter: 'inProgress',
      name: 'Reviewers Invited',
    },
    underReview: {
      filter: 'inProgress',
      name: 'Under Review',
    },
    reviewCompleted: {
      filter: 'inProgress',
      name: 'Under Review',
    },
    revisionRequested: {
      filter: 'needsAttention',
      name: 'Submit Revision',
    },
    pendingApproval: {
      filter: 'inProgress',
      name: 'Pending Approval',
    },
    rejected: {
      filter: 'archived',
      name: 'Rejected',
    },
    inQA: {
      filter: 'inProgress',
      name: 'Pending approval',
    },
    accepted: {
      name: 'Accepted',
    },
    withdrawn: {
      filter: 'archived',
      name: 'Withdrawn',
    },
    void: {
      filter: 'archived',
      name: 'Void',
    },
    deleted: {
      filter: 'archived',
      name: 'Deleted',
    },
    published: {
      name: 'Published',
    },
    academicEditorAssignedEditorialType: {
      filter: 'inProgress',
      name: 'AE Assigned',
    },
    makeDecision: {
      filter: 'inProgress',
      name: 'Pending Approval',
    },
    qualityChecksRequested: {
      filter: 'needsAttention',
      name: 'Submit Updates',
    },
    qualityChecksSubmitted: {
      name: 'Updates Submitted',
    },
    refusedToConsider: {
      name: 'Refused To Consider',
    },
  },
  admin: {
    draft: {
      filter: 'inProgress',
      name: 'Complete Submission',
    },
    technicalChecks: {
      filter: 'needsAttention',
      name: 'Approve QA',
    },
    submitted: {
      filter: 'needsAttention',
      name: 'Assign AE',
    },
    academicEditorInvited: {
      filter: 'needsAttention',
      name: 'Respond to Invite',
    },
    academicEditorAssigned: {
      filter: 'needsAttention',
      name: 'Invite Reviewers',
    },
    reviewersInvited: {
      filter: 'needsAttention',
      name: 'Respond to Invite',
    },
    underReview: {
      filter: 'needsAttention',
      name: 'Complete Review',
    },
    reviewCompleted: {
      filter: 'needsAttention',
      name: 'Make Recommendation',
    },
    revisionRequested: {
      filter: 'needsAttention',
      name: 'Submit Revision',
    },
    pendingApproval: {
      filter: 'needsAttention',
      name: 'Make Decision',
    },
    rejected: {
      filter: 'archived',
      name: 'Rejected',
    },
    inQA: {
      filter: 'needsAttention',
      name: 'Approve QA',
    },
    accepted: {
      name: 'Accepted',
    },
    withdrawn: {
      filter: 'archived',
      name: 'Withdrawn',
    },
    void: {
      filter: 'archived',
      name: 'Void',
    },
    deleted: {
      filter: 'archived',
      name: 'Deleted',
    },
    published: {
      name: 'Published',
    },
    academicEditorAssignedEditorialType: {
      filter: 'needsAttention',
      name: 'Make Decision',
    },
    makeDecision: {
      filter: 'needsAttention',
      name: 'Make Decision',
    },
    qualityChecksRequested: {
      name: 'Accepted',
    },
    qualityChecksSubmitted: {
      name: 'Accepted',
    },
    refusedToConsider: {
      name: 'Refused To Consider',
    },
  },
  reviewer: {
    reviewersInvited: {
      filter: 'needsAttention',
      name: 'Respond to Invite',
    },
    underReview: {
      filter: 'needsAttention',
      name: 'Complete Review',
    },
    reviewCompleted: {
      filter: 'inProgress',
      name: 'Under Review',
    },
    revisionRequested: {
      filter: 'inProgress',
      name: 'Revision Requested',
    },
    pendingApproval: {
      filter: 'inProgress',
      name: 'Pending Approval',
    },
    rejected: {
      filter: 'archived',
      name: 'Rejected',
    },
    inQA: {
      filter: 'inProgress',
      name: 'QA',
    },
    accepted: {
      name: 'Accepted',
    },
    withdrawn: {
      filter: 'archived',
      name: 'Withdrawn',
    },
    deleted: {
      filter: 'archived',
      name: 'Deleted',
    },
    published: {
      name: 'Published',
    },
    olderVersion: {
      name: 'Review Completed',
    },
    makeDecision: {
      filter: 'inProgress',
      name: 'Under Review',
    },
    qualityChecksRequested: {
      name: 'Accepted',
    },
    qualityChecksSubmitted: {
      name: 'Accepted',
    },
    refusedToConsider: {
      name: 'Refused To Consider',
    },
  },
  triageEditor: {
    technicalChecks: {
      filter: 'inProgress',
      name: 'QA',
    },
    submitted: {
      filter: 'needsAttention',
      name: 'Assign AE',
    },
    academicEditorInvited: {
      filter: 'inProgress',
      name: 'AE Invited',
    },
    academicEditorAssigned: {
      filter: 'inProgress',
      name: 'AE Assigned',
    },
    reviewersInvited: {
      filter: 'inProgress',
      name: 'Reviewers Invited',
    },
    underReview: {
      filter: 'inProgress',
      name: 'Under Review',
    },
    reviewCompleted: {
      filter: 'inProgress',
      name: 'Under Review',
    },
    revisionRequested: {
      filter: 'inProgress',
      name: 'Revision Requested',
    },
    pendingApproval: {
      filter: 'needsAttention',
      name: 'Make Decision',
    },
    rejected: {
      filter: 'archived',
      name: 'Rejected',
    },
    inQA: {
      filter: 'inProgress',
      name: 'QA',
    },
    accepted: {
      name: 'Accepted',
    },
    withdrawn: {
      filter: 'archived',
      name: 'Withdrawn',
    },
    deleted: {
      filter: 'archived',
      name: 'Deleted',
    },
    published: {
      name: 'Published',
    },
    academicEditorAssignedEditorialType: {
      filter: 'inProgress',
      name: 'Under Review',
    },
    makeDecision: {
      filter: 'needsAttention',
      name: 'Make Decision',
    },
    qualityChecksRequested: {
      name: 'Accepted',
    },
    qualityChecksSubmitted: {
      name: 'Accepted',
    },
    refusedToConsider: {
      name: 'Refused To Consider',
    },
  },
  academicEditor: {
    academicEditorInvited: {
      filter: 'needsAttention',
      name: 'Respond to Invite',
      priority: 1,
    },
    academicEditorAssigned: {
      filter: 'needsAttention',
      name: 'Invite Reviewers',
      priority: 1,
    },
    reviewersInvited: {
      filter: 'needsAttention',
      name: 'Invitation Pending',
      priority: 1,
    },
    underReview: {
      filter: 'needsAttention',
      name: 'Under Review',
      priority: 1,
    },
    reviewCompleted: {
      filter: 'needsAttention',
      name: 'Make Recommendation',
      priority: 1,
    },
    revisionRequested: {
      filter: 'inProgress',
      name: 'Revision Requested',
      priority: 1,
    },
    pendingApproval: {
      filter: 'inProgress',
      name: 'Pending Approval',
      priority: 1,
    },
    rejected: {
      filter: 'archived',
      name: 'Rejected',
      priority: 1,
    },
    inQA: {
      filter: 'inProgress',
      name: 'QA',
      priority: 1,
    },
    accepted: {
      name: 'Accepted',
      priority: 1,
    },
    withdrawn: {
      filter: 'archived',
      name: 'Withdrawn',
      priority: 1,
    },
    deleted: {
      filter: 'archived',
      name: 'Deleted',
      priority: 1,
    },
    published: {
      name: 'Published',
      priority: 1,
    },
    academicEditorAssignedEditorialType: {
      filter: 'needsAttention',
      name: 'Make Decision',
      priority: 1,
    },
    makeDecision: {
      filter: 'needsAttention',
      name: 'Make Decision',
      priority: 1,
    },
    qualityChecksRequested: {
      name: 'Accepted',
      priority: 1,
    },
    qualityChecksSubmitted: {
      name: 'Accepted',
      priority: 1,
    },
    refusedToConsider: {
      name: 'Refused To Consider',
      priority: 1,
    },
  },
  editorialAssistant: {
    draft: {
      filter: 'inProgress',
      name: 'Complete Submission',
    },
    technicalChecks: {
      filter: 'needsAttention',
      name: 'Approve QA',
    },
    submitted: {
      filter: 'needsAttention',
      name: 'Assign AE',
    },
    academicEditorInvited: {
      filter: 'needsAttention',
      name: 'Respond to Invite',
    },
    academicEditorAssigned: {
      filter: 'needsAttention',
      name: 'Invite Reviewers',
    },
    reviewersInvited: {
      filter: 'needsAttention',
      name: 'Respond to Invite',
    },
    underReview: {
      filter: 'needsAttention',
      name: 'Complete Review',
    },
    reviewCompleted: {
      filter: 'needsAttention',
      name: 'Make Recommendation',
    },
    revisionRequested: {
      filter: 'needsAttention',
      name: 'Submit Revision',
    },
    pendingApproval: {
      filter: 'needsAttention',
      name: 'Make Decision',
    },
    rejected: {
      filter: 'archived',
      name: 'Rejected',
    },
    inQA: {
      filter: 'needsAttention',
      name: 'Approve QA',
    },
    accepted: {
      name: 'Accepted',
    },
    withdrawn: {
      filter: 'archived',
      name: 'Withdrawn',
    },
    deleted: {
      filter: 'archived',
      name: 'Deleted',
    },
    published: {
      name: 'Published',
    },
    academicEditorAssignedEditorialType: {
      filter: 'needsAttention',
      name: 'Make Decision',
    },
    makeDecision: {
      filter: 'needsAttention',
      name: 'Make Decision',
    },
    qualityChecksRequested: {
      name: 'Accepted',
    },
    qualityChecksSubmitted: {
      name: 'Accepted',
    },
    refusedToConsider: {
      name: 'Refused To Consider',
    },
  },
  researchIntegrityPublishingEditor: {
    rejected: {
      filter: 'archived',
      name: 'Rejected',
    },
    inQA: {
      filter: 'inProgress',
      name: 'QA',
    },
    accepted: {
      name: 'Accepted',
    },
    published: {
      name: 'Published',
    },
    makeDecision: {
      filter: 'needsAttention',
      name: 'Make Decision',
    },
    qualityChecksRequested: {
      name: 'Accepted',
    },
    qualityChecksSubmitted: {
      name: 'Accepted',
    },
    refusedToConsider: {
      name: 'Refused To Consider',
    },
  },
  submittingStaffMember: {
    draft: {
      filter: 'inProgress',
      name: 'Complete Submission',
    },
  },
}

exports.seed = async knex => {
  // get current statuses
  const statuses = await getStatuses(knex)
  // get current roles
  const roles = await getRoles(knex)
  // get current filters
  const filters = await getFilters(knex)
  // Deletes ALL existing entries
  await knex('submission_label').del()
  // compute entries
  const labels = []

  const getFilterId = ({ roleName, statusName, filters, mapping }) => {
    const filter = filters.find(
      filter =>
        mapping[roleName] &&
        mapping[roleName][statusName] &&
        mapping[roleName][statusName].filter &&
        filter.name === mapping[roleName][statusName].filter,
    )
    if (filter && filter.id) return filter.id
  }

  const getPriority = ({ roleName, statusName, mapping }) =>
    mapping[roleName] &&
    mapping[roleName][statusName] &&
    mapping[roleName][statusName].priority

  roles.forEach(role => {
    statuses.forEach(status => {
      if (mapping[role.name] && mapping[role.name][status.name]) {
        labels.push({
          role_id: role.id,
          submission_status_id: status.id,
          submission_filter_id: getFilterId({
            roleName: role.name,
            statusName: status.name,
            filters,
            mapping,
          }),
          name: mapping[role.name][status.name].name,
          priority: getPriority({
            roleName: role.name,
            statusName: status.name,
            mapping,
          }),
        })
      }
    })
  })
  // insert entries
  await knex('submission_label').insert(labels)
}
