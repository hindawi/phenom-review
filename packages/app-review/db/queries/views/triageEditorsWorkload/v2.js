module.exports.up = `
CREATE OR REPLACE VIEW "triage_editors_workload" AS (
    SELECT
        count(tm."id") AS workload,
        tm."user_id",
        m."journal_id",
        m."special_issue_id",
        m."section_id"
    FROM
        "team_member" tm
        JOIN "team" t ON t."id" = tm."team_id"
        JOIN "manuscript" m ON m.id = t."manuscript_id"
    WHERE
        tm."status" = 'active'
        AND m."status" in('submitted', 'academicEditorInvited', 'pendingApproval')
        AND t."manuscript_id" IS NOT NULL
        AND t."role" = 'triageEditor'
    GROUP BY
        tm."user_id",
        m."journal_id",
        m."special_issue_id",
        m."section_id"
);
`
module.exports.down = 'DROP VIEW "triage_editors_workload";'
