module.exports.up = `
CREATE OR REPLACE VIEW "author" AS (
  SELECT
  t."manuscript_id" as manuscript_id,
  tm."user_id" as user_id,
  i."email" as email
FROM "team_member" tm
JOIN "team" t ON t."id" = tm."team_id"
JOIN "identity" i ON i."user_id" = tm."user_id"
WHERE t."manuscript_id" IS NOT NULL
AND t."role" = 'author'
);
`
module.exports.down = 'DROP VIEW "author";'
