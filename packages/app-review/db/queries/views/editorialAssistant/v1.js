module.exports.up = `
CREATE OR REPLACE VIEW "editorial_assistant" AS (
  SELECT
  t."manuscript_id" as manuscript_id,
  tm."user_id" as user_id,
  t."journal_id" as journal_id,
  tm."status" as status
FROM "team_member" tm
JOIN "team" t ON t."id" = tm."team_id"
WHERE t."manuscript_id" IS NOT NULL
AND t."role" = 'editorialAssistant'
);
`
module.exports.down = 'DROP VIEW "editorial_assistant";'
