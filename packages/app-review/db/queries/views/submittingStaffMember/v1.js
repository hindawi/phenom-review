module.exports.up = `
CREATE OR REPLACE VIEW "submitting_staff_member" AS (
  SELECT
  tm."user_id" as user_id,
  i."email" as email,
  t."manuscript_id" as manuscript_id
FROM "team_member" tm
JOIN "team" t ON t."id" = tm."team_id"
JOIN "identity" i ON i."user_id" = tm."user_id"
WHERE t."role" = 'submittingStaffMember'
);
`
module.exports.down = 'DROP VIEW "submitting_staff_member";'
