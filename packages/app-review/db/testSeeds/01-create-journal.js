const Chance = require('chance')

const chance = new Chance()

exports.seed = async knex => {
  const peerReviewModels = await knex.select().table('peer_review_model')

  await knex('journal').insert([
    {
      name: chance.company(),
      code: chance.syllable(),
      email: chance.email(),
      apc: chance.integer({ min: 1, max: 999999 }),
      isActive: true,
      activationDate: new Date().toISOString(),
      peerReviewModelId: peerReviewModels[0].id,
    },
  ])

  await knex('journal').insert([
    {
      name: chance.company(),
      code: chance.syllable(),
      email: chance.email(),
      apc: chance.integer({ min: 1, max: 999999 }),
      isActive: true,
      activationDate: new Date().toISOString(),
      peerReviewModelId: peerReviewModels[2].id,
    },
  ])
}
