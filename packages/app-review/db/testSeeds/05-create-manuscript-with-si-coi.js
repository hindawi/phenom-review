const Chance = require('chance')

const chance = new Chance()
const { createTeam, createTeamMember, createUser } = require('../seedUtils')

exports.seed = async knex => {
  const specialIssues = await knex.select().table('special_issue')
  if (specialIssues.length === 0) {
    throw new Error('No special Issues have been found')
  }
  const specialIssueId = specialIssues[0].id
  const { journalId } = specialIssues[0]

  const articleTypes = await knex.select().table('article_type')
  if (articleTypes.length === 0) {
    throw new Error('No article types have been found')
  }
  const articleTypeId = articleTypes[0].id

  const manuscript = await knex('manuscript')
    .insert([
      {
        submissionId: chance.guid(),
        status: 'technicalChecks',
        version: '1',
        title: 'COI',
        abstract: chance.paragraph(),
        journalId,
        agreeTc: true,
        articleTypeId,
        specialIssueId,
      },
    ])
    .returning('*')
    .then(results => results[0])

  const userAndIdentity = await createUser({ knex })
  const academicEditorUserAndIdentity = await createUser({ knex })

  const specialssueEditorialTeam = await createTeam({
    knex,
    specialIssueId,
    role: 'triageEditor',
  })
  await createTeamMember({
    knex,
    teamId: specialssueEditorialTeam.id,
    userId: userAndIdentity.user.id,
    status: 'pending',
  })
  const specialIssueAcademicEditorTeam = await createTeam({
    knex,
    specialIssueId,
    role: 'academicEditor',
  })
  await createTeamMember({
    knex,
    teamId: specialIssueAcademicEditorTeam.id,
    userId: academicEditorUserAndIdentity.user.id,
    status: 'pending',
  })
  const manuscriptAuthorTeam = await createTeam({
    knex,
    manuscriptId: manuscript.id,
    role: 'author',
  })

  await createTeamMember({
    knex,
    teamId: manuscriptAuthorTeam.id,
    userId: userAndIdentity.user.id,
    status: 'pending',
  })
}
