const Chance = require('chance')

const chance = new Chance()
const {
  createTeam,
  createTeamMember,
  createUser,
  createReview,
} = require('../seedUtils')

exports.seed = async knex => {
  const journals = await knex.select().table('journal')
  if (journals.length === 0) {
    throw new Error('No journals have been found')
  }
  const journalId = journals[1].id

  const articleTypes = await knex.select().table('article_type')
  if (articleTypes.length === 0) {
    throw new Error('No article types have been found')
  }
  const articleTypeId = articleTypes[0].id

  const manuscript = await knex('manuscript')
    .insert([
      {
        submissionId: chance.guid(),
        status: 'inQA',
        version: '1',
        title: 'Author Academic Editor COI',
        abstract: chance.paragraph(),
        journalId,
        agreeTc: true,
        articleTypeId,
      },
    ])
    .returning('*')
    .then(results => results[0])

  const userAndIdentity = await createUser({ knex })
  const academicEditorUser = await createUser({ knex })
  const triageEditorUser = await createUser({ knex })
  const reviewerUser = await createUser({ knex })

  const manuscriptAuthorTeam = await createTeam({
    knex,
    manuscriptId: manuscript.id,
    role: 'author',
  })

  const academicEditorTeam = await createTeam({
    knex,
    manuscriptId: manuscript.id,
    role: 'academicEditor',
  })
  const triageEditorTeam = await createTeam({
    knex,
    manuscriptId: manuscript.id,
    role: 'triageEditor',
  })
  const reviewerTeam = await createTeam({
    knex,
    manuscriptId: manuscript.id,
    role: 'reviewer',
  })

  await createTeamMember({
    knex,
    teamId: triageEditorTeam.id,
    userId: triageEditorUser.user.id,
    status: 'active',
  })

  const academicEditor = await createTeamMember({
    knex,
    teamId: academicEditorTeam.id,
    userId: academicEditorUser.user.id,
    status: 'accepted',
  })

  const reviewer = await createTeamMember({
    knex,
    teamId: reviewerTeam.id,
    userId: reviewerUser.user.id,
    status: 'accepted',
  })

  await createTeamMember({
    knex,
    teamId: manuscriptAuthorTeam.id,
    userId: userAndIdentity.user.id,
    status: 'pending',
  })

  await createReview({
    knex,
    teamMemberId: reviewer.id,
    manuscriptId: manuscript.id,
    recommendation: 'publish',
  })

  await createReview({
    knex,
    teamMemberId: academicEditor.id,
    manuscriptId: manuscript.id,
    recommendation: 'publish',
  })
}
