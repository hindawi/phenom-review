const ACRONYM = 'acronym'
const PUBLICATION_DATE = 'publication_date'
const IS_ANNUAL = 'is_annual'
const IS_PUBLISHED = 'is_published'
const DESCRIPTION = 'description'
const CONFLICT_OF_INTEREST = 'conflict_of_interest'
const TOPICS = 'topics'
const TYPE = 'type'

module.exports = {
  up: async knex =>
    knex.schema.table('special_issue', si => {
      si.string(ACRONYM, 12)
      si.date(PUBLICATION_DATE)
      si.boolean(IS_ANNUAL)
        .notNullable()
        .defaultTo(false)
      si.boolean(IS_PUBLISHED)
        .notNullable()
        .defaultTo(false)
      si.specificType(DESCRIPTION, 'text[]')
      si.text(CONFLICT_OF_INTEREST)
      si.specificType(TOPICS, 'text[]')
      si.string(TYPE)
        .notNullable()
        .defaultTo('SI')
    }),
  down: async knex =>
    knex.schema.alterTable('special_issue', si => {
      si.dropColumns(
        ACRONYM,
        PUBLICATION_DATE,
        IS_ANNUAL,
        IS_PUBLISHED,
        DESCRIPTION,
        CONFLICT_OF_INTEREST,
        TOPICS,
        TYPE,
      )
    }),
}
