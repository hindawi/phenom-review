const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .createTable('conflict', table => {
      table
        .uuid('id')
        .primary()
        .defaultTo(knex.raw('uuid_generate_v4()'))
      table
        .timestamp('created')
        .notNullable()
        .defaultTo(knex.fn.now())
      table
        .timestamp('updated')
        .notNullable()
        .defaultTo(knex.fn.now())
      table.uuid('first_conflicting_team_member_id').notNullable()
      table.uuid('second_conflicting_team_member_id').notNullable()
      table
        .foreign('first_conflicting_team_member_id')
        .references('team_member.id')
      table
        .foreign('second_conflicting_team_member_id')
        .references('team_member.id')
    })
    .then(_ => logger.info('conflict table created successfully'))
    .catch(err => logger.error(err))

exports.down = async knex =>
  knex.schema
    .dropTable('conflict')
    .then(_ => logger.info('conflict table dropped successfully'))
    .catch(err => logger.error(err))
