ALTER TABLE job
DROP CONSTRAINT job_team_member_id_fkey;

ALTER TABLE job
ADD CONSTRAINT job_team_member_id_fkey
FOREIGN KEY (team_member_id) REFERENCES team_member (id);
