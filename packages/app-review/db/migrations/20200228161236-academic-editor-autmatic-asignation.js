exports.up = async knex => {
  await knex.schema.table('peer_review_model', table => {
    table.boolean('academic_editor_automatic_invitation')
  })
  await knex('peer_review_model')
    .where({
      academic_editor_assignment_tool: '{"manual"}',
    })
    .update({
      academic_editor_automatic_invitation: false,
    })

  await knex('peer_review_model')
    .where({
      academic_editor_assignment_tool: '{"manual","workload"}',
    })
    .update({
      academic_editor_automatic_invitation: true,
    })

  await knex.schema.table('peer_review_model', table => {
    table.dropColumn('academic_editor_assignment_tool')
  })
}
