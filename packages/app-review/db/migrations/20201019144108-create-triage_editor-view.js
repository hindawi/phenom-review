const path = require('path')

const migration = require(path.resolve(
  __dirname,
  '../db/queries/views/triageEditor/v1.js',
))

module.exports.up = async knex => {
  await knex.raw(migration.up)
}

module.exports.down = async knex => {
  await knex.raw(migration.down)
}
