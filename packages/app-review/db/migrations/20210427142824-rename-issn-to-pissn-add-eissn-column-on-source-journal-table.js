const { logger } = require('component-logger')

module.exports = {
  up: async knex =>
    knex.schema
      .table('source_journal', table => {
        table.renameColumn('issn', 'pissn')
        table.string('eissn')
      })
      .then(_ =>
        logger.info(
          'source_journal: renamed issn to pissn and created eissn column',
        ),
      )
      .catch(err => logger.error(err)),
  down: async knex =>
    knex.schema
      .table('source_journal', table => {
        table.renameColumn('pissn', 'issn')
        table.dropColumn('eissn')
      })
      .then(_ =>
        logger.info(
          'source_journal: reverted pissn to issn and dropped eissn column',
        ),
      )
      .catch(err => logger.error(err)),
}
