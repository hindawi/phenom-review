const { logger } = require('component-logger')

exports.up = async knex => {
  knex.schema
    .createTable('preprint', table => {
      table
        .uuid('id')
        .primary()
        .defaultTo(knex.raw('uuid_generate_v4()'))
      table
        .timestamp('created')
        .notNullable()
        .defaultTo(knex.fn.now())
      table
        .timestamp('updated')
        .notNullable()
        .defaultTo(knex.fn.now())
      table.string('type').notNullable()
      table.string('format').notNullable()
    })
    .catch(err => logger.error(err))
}

exports.down = async knex => {
  knex.schema.dropTable('preprint').catch(err => logger.error(err))
}
