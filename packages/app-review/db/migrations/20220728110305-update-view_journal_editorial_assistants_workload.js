const path = require('path')

const v1 = require(path.resolve(
  __dirname,
  '../db/queries/views/editorialAssistantsJournalWorkload/v2.js',
))

module.exports.up = async knex => {
  await knex.raw(v1.up)
}

module.exports.down = async knex => {
  await knex.raw(v1.down)
}
