const { logger } = require('component-logger')

exports.up = function(knex) {
  return knex
    .raw(
      `
      CREATE TABLE peer_review_editorial_mapping (
        id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
        created timestamp NOT NULL DEFAULT current_timestamp,
        updated timestamp NOT NULL DEFAULT current_timestamp,
        submission_id uuid NOT NULL,
        peer_review_editorial_model jsonb NOT NULL
      );
    `,
    )
    .then(_ =>
      logger.info('peer_review_editorial_mapping table created successfully'),
    )
    .catch(err => logger.error(err))
}

exports.down = function(knex) {
  return knex.schema
    .dropTable('peer_review_editorial_mapping')
    .then(_ =>
      logger.info('peer_review_editorial_mapping table dropped successfully'),
    )
    .catch(err => logger.error(err))
}
