const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .table('reviewer_suggestion', table => {
      table.index('manuscript_id', null, 'hash')
    })
    .then(() => logger.info('reviewer_suggestion indexes added successfully'))
