const PUBLICATION_DATE = 'publication_date'

module.exports = {
  up: async knex =>
    knex.schema.alterTable('special_issue', si =>
      si.timestamp(PUBLICATION_DATE).alter(),
    ),
  down: async knex =>
    knex.schema.alterTable('special_issue', si =>
      si.date(PUBLICATION_DATE).alter(),
    ),
}
