const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .table('file', table => {
      table.index('manuscript_id', null, 'hash')
      table.index('comment_id', null, 'hash')
    })
    .then(() => logger.info('file indexes added successfully'))
