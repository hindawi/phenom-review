module.exports = {
  up: async knex =>
    knex.schema.table('team_member', t => {
      t.timestamp('invited_date')
    }),
  down: async knex =>
    knex.schema.table('team_member', t => {
      t.dropColumn('invited_date')
    }),
}
