ALTER TABLE manuscript
  ADD COLUMN section_id uuid REFERENCES section (id);