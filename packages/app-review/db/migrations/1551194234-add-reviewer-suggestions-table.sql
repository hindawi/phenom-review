CREATE TABLE "reviewer_suggestion" (
    "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    "created" timestamp NOT NULL DEFAULT current_timestamp,
    "updated" timestamp NOT NULL DEFAULT current_timestamp,
    "manuscript_id" uuid NOT NULL REFERENCES manuscript,
    "given_names" text,
    "surname" text,
    "number_of_reviews" integer NOT NULL,
    "aff" text NOT NULL,
    "email" text NOT NULL,
    "profile_url" text NOT NULL,
    "is_invited" boolean NOT NULL,
    "type" text NOT NULL  --  publons,
);
