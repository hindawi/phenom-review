module.exports = {
  up: async knex =>
    knex.schema.table('manuscript', t => {
      t.boolean('hasTriageEditorConflictOfInterest').defaultTo(false)
    }),
  down: async knex =>
    knex.schema.table('manuscript', t => {
      t.dropColumn('hasTriageEditorConflictOfInterest')
    }),
}
