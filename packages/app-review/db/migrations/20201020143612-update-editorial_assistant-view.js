const path = require('path')

const v1 = require(path.resolve(
  __dirname,
  '../db/queries/views/editorialAssistant/v1.js',
))

const v2 = require(path.resolve(
  __dirname,
  '../db/queries/views/editorialAssistant/v2.js',
))

module.exports.up = async knex => {
  await knex.raw(v1.down)
  await knex.raw(v2.up)
}

module.exports.down = async knex => {
  await knex.raw(v2.down)
}
