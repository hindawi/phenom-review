const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .table('identity', table => {
      table.index('user_id', null, 'hash')
      table.index('email', null, 'hash')
    })
    .then(() => logger.info('identity indexes added successfully'))
