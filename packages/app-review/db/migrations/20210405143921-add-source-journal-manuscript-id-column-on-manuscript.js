module.exports = {
  up: async knex =>
    knex.schema.table('manuscript', m => {
      m.string('source_journal_manuscript_id')
    }),
  down: async knex =>
    knex.schema.table('manuscript', m => {
      m.dropColumn('source_journal_manuscript_id')
    }),
}
