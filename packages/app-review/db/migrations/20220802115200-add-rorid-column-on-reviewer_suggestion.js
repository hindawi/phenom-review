module.exports = {
  up: async knex =>
    knex.schema.table('reviewer_suggestion', s => {
      s.string('affRorId')
    }),
  down: async knex =>
    knex.schema.table('reviewer_suggestion', s => {
      s.dropColumn('affRorId')
    }),
}
