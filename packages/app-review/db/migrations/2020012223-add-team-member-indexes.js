const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .table('team_member', table => {
      table.index('user_id', null, 'hash')
      table.index('team_id', null, 'hash')
      table.index('status', null, 'hash')
    })
    .then(() => logger.info('team_member indexes added successfully'))
