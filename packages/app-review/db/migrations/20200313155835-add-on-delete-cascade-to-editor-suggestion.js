exports.up = async knex =>
  knex.schema.table('editor_suggestion', table => {
    table.dropForeign(
      'team_member_id',
      'editor_suggestion_team_member_id_foreign',
    )
    table
      .foreign('team_member_id')
      .references('id')
      .inTable('team_member')
      .onDelete('CASCADE')
  })
