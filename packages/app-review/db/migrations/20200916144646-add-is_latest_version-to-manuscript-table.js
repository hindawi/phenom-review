const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .table('manuscript', m => {
      m.boolean('is_latest_version')
    })
    .catch(err => logger.error(err))

exports.down = async knex =>
  knex.schema
    .table('manuscript', m => {
      m.dropColumn('is_latest_version')
    })
    .catch(err => logger.error(err))
