const { logger } = require('component-logger')

exports.up = async knex => {
  knex
    .raw(`CREATE INDEX on team_member USING GIN ((alias->'givenNames'))`)
    .then(() =>
      logger.info('team member givenNames indexes added successfully'),
    )
}
