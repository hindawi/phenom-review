module.exports = {
  up: async knex =>
    knex.schema.table('team_member', m => {
      m.text('decline_reason')
    }),
  down: async knex =>
    knex.schema.table('team_member', m => {
      m.dropColumn('decline_reason')
    }),
}
