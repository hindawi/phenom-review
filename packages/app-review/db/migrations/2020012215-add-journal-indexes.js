const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .table('journal', table => {
      table.index('peer_review_model_id', null, 'hash')
      table.index('code', null, 'hash')
      table.index('email', null, 'hash')
      table.index('created', null)
      table.index('name', null)
    })
    .then(() => logger.info('journal indexes added successfully'))
