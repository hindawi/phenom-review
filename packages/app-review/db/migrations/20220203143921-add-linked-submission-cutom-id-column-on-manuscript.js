module.exports = {
  up: async knex =>
    knex.schema.table('manuscript', m => {
      m.string('linked_submission_custom_id')
    }),
  down: async knex =>
    knex.schema.table('manuscript', m => {
      m.dropColumn('linked_submission_custom_id')
    }),
}
