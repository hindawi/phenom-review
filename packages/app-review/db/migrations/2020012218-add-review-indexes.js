const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .table('review', table => {
      table.index('manuscript_id', null, 'hash')
      table.index('team_member_id', null, 'hash')
    })
    .then(() => logger.info('review indexes added successfully'))
