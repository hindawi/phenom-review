const { logger } = require('component-logger')

exports.up = function(knex) {
  return knex.schema
    .dropTable('submission_editorial_mapping')
    .then(_ =>
      logger.info('submission_editorial_mapping table dropped successfully'),
    )
    .catch(err => logger.error(err))
}

exports.down = function(knex) {
  return knex
    .raw(
      `
    CREATE TABLE submission_editorial_mapping (
      created timestamp NOT NULL DEFAULT current_timestamp,
      updated timestamp NOT NULL DEFAULT current_timestamp,
      journal_id text NOT NULL DEFAULT 'default',
      issue_type text NOT NULL DEFAULT 'default',
      article_type text NOT NULL DEFAULT 'default',
      is_on_section boolean NOT NULL default false,
      submission_editorial_model jsonb,
      PRIMARY KEY(journal_id, issue_type, article_type)
    );
  `,
    )
    .then(_ =>
      logger.info('submission_editorial_mapping table created successfully'),
    )
    .catch(err => logger.error(err))
}
