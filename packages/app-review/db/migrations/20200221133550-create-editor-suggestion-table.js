exports.up = async knex =>
  knex.schema.createTable('editor_suggestion', table => {
    table
      .uuid('id')
      .primary()
      .defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .timestamp('created')
      .notNullable()
      .defaultTo(knex.fn.now())
    table
      .timestamp('updated')
      .notNullable()
      .defaultTo(knex.fn.now())

    table.uuid('manuscript_id').notNullable()
    table.uuid('team_member_id').notNullable()
    table.float('score').notNullable()

    table.unique(['manuscript_id', 'team_member_id'])
    table.foreign('manuscript_id').references('manuscript.id')
    table.foreign('team_member_id').references('team_member.id')
  })

exports.down = async knex => knex.schema.dropTable('editor_suggestion')
