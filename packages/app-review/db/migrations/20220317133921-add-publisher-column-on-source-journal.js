module.exports = {
  up: async knex =>
    knex.schema.table('source_journal', s => {
      s.string('publisher')
    }),
  down: async knex =>
    knex.schema.table('source_journal', s => {
      s.dropColumn('publisher')
    }),
}
