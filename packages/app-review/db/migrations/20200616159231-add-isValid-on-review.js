module.exports = {
  up: async knex =>
    knex.schema.table('review', t => {
      t.boolean('isValid').defaultTo(true)
    }),
  down: async knex =>
    knex.schema.table('review', t => {
      t.dropColumn('isValid')
    }),
}
