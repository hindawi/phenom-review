module.exports = {
  up: async knex =>
    knex.schema.table('manuscript', t => {
      t.boolean('allowAcademicEditorAutomaticInvitation').defaultTo(null)
    }),
}
