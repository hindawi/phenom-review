CREATE TABLE "journal_article_type" (
    "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    created timestamptz NOT NULL DEFAULT now(),
    updated timestamptz NOT NULL DEFAULT now(),
    journal_id uuid NOT NULL REFERENCES journal (id) ON DELETE CASCADE,
    article_type_id uuid NOT NULL REFERENCES article_type (id) ON DELETE CASCADE
)