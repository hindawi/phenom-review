const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .table('available_custom_ids', table => {
      table.index('custom_id', null, 'hash')
    })
    .then(() => logger.info('available_custom_ids indexes added successfully'))
