const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .table('team', table => {
      table.index('journal_id', null, 'hash')
      table.index('manuscript_id', null, 'hash')
      table.index('section_id', null, 'hash')
      table.index('special_issue_id', null, 'hash')
      table.index('role', null, 'hash')
    })
    .then(() => logger.info('team indexes added successfully'))
