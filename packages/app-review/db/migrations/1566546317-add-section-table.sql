CREATE TABLE "section" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "created" timestamptz NOT NULL DEFAULT now(),
  "updated" timestamptz NOT NULL DEFAULT now(),
  "name" text UNIQUE NOT NULL,
  "journal_id" uuid NOT NULL REFERENCES "journal"
);