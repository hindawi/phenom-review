const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .table('manuscript', m => {
      m.string('preprint_value')
    })
    .catch(err => logger.error(err))

exports.down = async knex =>
  knex.schema
    .table('manuscript', m => {
      m.dropColumn('preprint_value')
    })
    .catch(err => logger.error(err))
