const { logger } = require('component-logger')

exports.up = async knex => {
  knex.schema
    .createTable('journal_preprint', table => {
      table
        .uuid('id')
        .primary()
        .defaultTo(knex.raw('uuid_generate_v4()'))
      table
        .timestamp('created')
        .notNullable()
        .defaultTo(knex.fn.now())
      table
        .timestamp('updated')
        .notNullable()
        .defaultTo(knex.fn.now())
      table.uuid('journal_id').notNullable()
      table.uuid('preprint_id').notNullable()
      table.text('preprint_description')

      table.foreign('journal_id').references('journal.id')
      table.foreign('preprint_id').references('preprint.id')
    })
    .catch(err => logger.error(err))
}

exports.down = async knex => {
  knex.schema.dropTable('journal_preprint').catch(err => logger.error(err))
}
