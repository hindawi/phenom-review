const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .createTable('submission_label', table => {
      table
        .uuid('id')
        .primary()
        .defaultTo(knex.raw('uuid_generate_v4()'))
      table
        .timestamp('created')
        .notNullable()
        .defaultTo(knex.fn.now())
      table
        .timestamp('updated')
        .notNullable()
        .defaultTo(knex.fn.now())
      table.uuid('role_id').notNullable()
      table.uuid('submission_status_id').notNullable()
      table.uuid('submission_filter_id')
      table.integer('priority').defaultTo(0)
      table.string('name').notNullable()

      table.foreign('role_id').references('role.id')
      table.foreign('submission_status_id').references('submission_status.id')
      table.foreign('submission_filter_id').references('submission_filter.id')
    })
    .then(_ => logger.info('submission_label table created successfully'))
    .catch(err => logger.error(err))

exports.down = async knex =>
  knex.schema
    .dropTable('submission_label')
    .then(_ => logger.info('submission_label table dropped successfully'))
    .catch(err => logger.error(err))
