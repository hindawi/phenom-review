ALTER TABLE special_issue
  ADD COLUMN peer_review_model_id uuid REFERENCES peer_review_model (id) ON DELETE CASCADE;

