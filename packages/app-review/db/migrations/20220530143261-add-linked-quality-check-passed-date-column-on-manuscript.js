module.exports = {
  up: async knex =>
    knex.schema.table('manuscript', m => {
      m.timestamp('quality_check_passed_date')
    }),
  down: async knex =>
    knex.schema.table('manuscript', m => {
      m.dropColumn('quality_check_passed_date')
    }),
}
