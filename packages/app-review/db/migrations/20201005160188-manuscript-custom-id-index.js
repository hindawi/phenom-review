const { logger } = require('component-logger')

exports.up = async knex =>
  knex.schema
    .table('manuscript', table => {
      table.index('customId', null, 'hash')
    })
    .then(() => logger.info('custom id index added successfully'))
