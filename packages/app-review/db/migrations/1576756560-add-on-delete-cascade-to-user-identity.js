module.exports = {
  up: async knex => {
    try {
      await knex.schema.table('identity', table => {
        table.dropForeign('user_id', 'identity_user_id_fkey')
        table
          .foreign('user_id')
          .references('id')
          .inTable('user')
          .onDelete('CASCADE')
      })
    } catch (e) {
      throw new Error(e)
    }
  },
  down: async () => {},
}
