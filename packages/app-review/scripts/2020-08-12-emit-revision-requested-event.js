const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const { createQueueService } = require('@hindawi/queue-service')
const events = require('component-events')

const eventsService = events.initialize({ models })

const execute = async () => {
  const submissionId = process.argv[2]

  if (!submissionId) {
    logger.error('Please provide submissionId as argument')
    process.exit()
  }

  await createQueueService()
    .then(queueService => {
      global.applicationEventBus = queueService
    })
    .catch(err => {
      logger.error(err)
      process.exit(1)
    })

  await eventsService.publishSubmissionEvent({
    submissionId,
    eventName: 'SubmissionRevisionRequested',
  })

  process.exit()
}

execute()
