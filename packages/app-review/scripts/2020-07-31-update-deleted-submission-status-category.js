const { db: knex } = require('@pubsweet/db-manager')
const { logger } = require('component-logger')

const execute = async () => {
  const submissionStatus = await knex('submission_status')
    .where('name', 'deleted')
    .returning('id')
    .update('category', 'withdrawn')

  logger.info(submissionStatus)

  logger.info(`Updated submission status with id: ${submissionStatus[0]}`)

  process.exit()
}

execute()
