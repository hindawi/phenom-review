/*
 This script is used by QAlas to replay *EditorAdded events in case we need to recalculate editors' expertise in the editor-suggestion-service
 */

/**
 * 
  Prerequisites:
  - temporary sns topic called 'est-events' created manually in the same account where we want to run the service
  - temporary s3 bucket created manually in the same account where we want to run the service

  In the 'packages/app-review/.env':
    update 
      DB_HOST=
      DATABASE=
      DB_USER=
      DB_PASS=
    with the details of the REVIEW DB you want to use

    add
      EST_DATABASE=
      EST_DB_HOST=
      EST_DB_PASS=
      EST_DB_USER=
    with the EST db details you want to populare

    add
      EST_AWS_SNS_SQS_REGION=eu-west-1
    
    add
      EST_PHENOM_LARGE_EVENTS_BUCKET=
      EST_PHENOM_LARGE_EVENTS_PREFIX=
    with the name of the temporary bucket you've created and leave prefix empty



 */

const models = require('@pubsweet/models')
const events = require('component-events').initialize({ models })
const Promise = require('bluebird')
const { SNS, S3 } = require('aws-sdk')
const uuid = require('uuid')
const Knex = require('knex')

const {
  createLargeEventsService,
} = require('@hindawi/queue-service/createLargeEventsService')

const sns = new SNS({
  region: process.env.AWS_SNS_SQS_REGION,
  // endpoint: 'http://localhost:9911',
})

const largeEventsService = createLargeEventsService({
  storage: new S3(),
  Bucket: process.env.EST_PHENOM_LARGE_EVENTS_BUCKET,
  prefix: process.env.EST_PHENOM_LARGE_EVENTS_PREFIX,
})

const estDB = new Knex({
  client: 'pg',
  connection: {
    database: process.env.EST_DATABASE,
    user: process.env.EST_DB_USER,
    host: process.env.EST_DB_HOST,
    password: process.env.EST_DB_PASS,
  },
})

main()
  .then(() => console.log('done'))
  .catch(e => {
    console.error(e)
    process.exit(1)
  })

async function main() {
  await estDB.transaction(async trx => {
    return trx.raw('TRUNCATE TABLE editor CASCADE')
  })

  const { TopicArn } = await sns.createTopic({ Name: 'est-events' }).promise()

  global.applicationEventBus = {
    publishMessage,
  }

  const journals = await models.Journal.findAll()

  await Promise.each(journals, async j => {
    await events.publishJournalEvent({
      journalId: j.id,
      eventName: 'JournalEditorAssigned',
    })

    await events.publishJournalEvent({
      journalId: j.id,
      eventName: 'JournalSectionSpecialIssueEditorAssigned',
    })

    await events.publishJournalEvent({
      journalId: j.id,
      eventName: 'JournalSpecialIssueEditorAssigned',
    })
  })

  async function publishMessage({
    event,
    data,
    messageAttributes,
    timestamp = new Date().toISOString(),
    id = uuid(),
  }) {
    const MessageObject = {
      event,
      timestamp,
      data,
      id,
    }
    const jsonData = JSON.stringify(MessageObject)

    const MessageAttributesObject = {}
    const MessageAttributes = new Proxy(MessageAttributesObject, {
      get(obj, prop) {
        return prop in obj
          ? {
              DataType: 'String',
              StringValue: obj[prop],
            }
          : undefined
      },
    })

    Object.assign(MessageAttributesObject, messageAttributes)

    let Message = jsonData

    // SNS & SQS Limit Size = 256K including message attributes
    if (
      Buffer.byteLength(Message) + getMessageAttributesSize(MessageAttributes) >
      262144
    ) {
      console.log(
        'Event larger than 256K. Uploading to S3 and sending a reference',
      )
      MessageObject.data = await largeEventsService.put(JSON.stringify(data))
      Message = JSON.stringify(MessageObject)
      Object.assign(MessageAttributesObject, {
        PhenomMessageTooLarge: 'Y',
        type: 'phenom',
      })
    }

    await sns.publish({ TopicArn, Message, MessageAttributes }).promise()

    const aes = data.editors.filter(
      editor => editor.role.type === 'academicEditor',
    )
    console.log(data.id, event, aes.length)
  }
}

function getMessageAttributesSize(messageAttributes) {
  return Object.entries(messageAttributes).reduce(
    (acc, [key, { DataType, StringValue }]) =>
      [key, DataType, StringValue].reduce(
        (sum, val) => sum + Buffer.byteLength(val),
        0,
      ),
    0,
  )
}
