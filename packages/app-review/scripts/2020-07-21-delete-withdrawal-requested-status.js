const { db: knex } = require('@pubsweet/db-manager')
const { logger } = require('component-logger')

const execute = async () => {
  await knex('submission_label')
    .whereIn('name', ['Withdrawal Requested', 'Approve Withdrawal'])
    .del()
  logger.info('Deleted labels for withdrawalRequested')

  await knex('submission_status')
    .where('name', 'withdrawalRequested')
    .del()
  logger.info('Deleted withdrawalRequested status')

  process.exit()
}
execute()
