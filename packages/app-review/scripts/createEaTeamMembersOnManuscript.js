const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const events = require('../../component-events')
const { groupBy } = require('lodash')
const { Promise } = require('bluebird')

const eventsService = events.initialize({ models })

// This will no longer work as the use case is not being exported with the initialize-execute pattern
const {
  getUserForAutoAssignmentBasedOnWorkload,
} = require('../../component-model/src/useCases/user')
const { createQueueService } = require('@hindawi/queue-service')

const execute = async () => {
  createQueueService()
    .then(queueService => {
      global.applicationEventBus = queueService
    })
    .catch(err => {
      logger.error(err)
      process.exit(1)
    })

  const { Manuscript, Team, TeamMember, User } = models
  const manuscripts = await Manuscript.findAllByExcludedStatuses({
    excludedStatuses: [
      Manuscript.Statuses.deleted,
      Manuscript.Statuses.withdrawn,
      Manuscript.Statuses.published,
      Manuscript.Statuses.rejected,
      Manuscript.Statuses.refusedToConsider,
    ],
  })
  const groupedManuscripts = groupBy(manuscripts, 'submissionId')

  const submissions = Object.values(groupedManuscripts)

  await Promise.each(submissions, async versions => {
    const currentEditorialAssistant = await TeamMember.findOneByManuscriptAndRole(
      { manuscriptId: versions[0].id, role: Team.Role.editorialAssistant },
    )
    const journalEditorialAssistant = await TeamMember.findOneByJournalAndRole({
      journalId: versions[0].journalId,
      role: Team.Role.editorialAssistant,
    })
    if (currentEditorialAssistant || !journalEditorialAssistant) return

    const userForAssignment = await getUserForAutoAssignmentBasedOnWorkload({
      role: Team.Role.editorialAssistant,
      journalId: versions[0].journalId,
      manuscriptStatuses: [
        ...Manuscript.InProgressStatuses,
        Manuscript.Statuses.technicalChecks,
      ],
      teamMemberStatuses: [TeamMember.Statuses.active],
      models,
    })
    const user = await User.find(userForAssignment.userId, 'identities')
    await Promise.each(versions, async ({ submissionId, id: manuscriptId }) => {
      const editorialAssistantTeam = new Team({
        role: Team.Role.editorialAssistant,
        manuscriptId,
      })

      editorialAssistantTeam.addMember({
        user,
        options: { status: TeamMember.Statuses.active },
      })
      await editorialAssistantTeam.saveGraph({ relate: true, noUpdate: true })

      logger.info(
        `Addeed Editorial Assistant on manuscript with id ${manuscriptId}`,
      )
    })
    eventsService.publishSubmissionEvent({
      submissionId: versions[0].submissionId,
      eventName: 'SubmissionEditorialAssistantAssigned',
    })
  })
  process.exit()
}
execute()
