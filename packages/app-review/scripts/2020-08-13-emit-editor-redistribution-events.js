const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const { createQueueService } = require('@hindawi/queue-service')
const Promise = require('bluebird')
const events = require('component-events')

const { Journal, Manuscript, Team, TeamMember } = models
const eventsService = events.initialize({ models })

const editorialAssistantWorkingStatuses = [
  ...Manuscript.InProgressStatuses,
  Manuscript.Statuses.qualityChecksRequested,
  Manuscript.Statuses.qualityChecksSubmitted,
  Manuscript.Statuses.technicalChecks,
]

const execute = async () => {
  const isDebug = process.argv[2] === '--debug'

  await createQueueService()
    .then(queueService => {
      global.applicationEventBus = queueService
    })
    .catch(err => {
      logger.error(err)
      process.exit(1)
    })

  logger.info('Successfully initialized queue service.')

  const journalsWithMultipleEAs = await Journal.findAllWithMultipleEAs()

  logger.info(
    `Found ${
      journalsWithMultipleEAs.length
    } journals with multiple EAs: ${journalsWithMultipleEAs
      .map(j => j.id)
      .join(', ')}`,
  )

  const getManuscriptsRedistributed = async journalId =>
    TeamMember.query()
      .select('tm.id', 'tm.created', 'm.submission_id as submissionId')
      .from('team_member as tm')
      .join('team as t', 't.id', 'tm.team_id')
      .join('manuscript as m', 'm.id', 't.manuscript_id')
      .whereIn('m.status', editorialAssistantWorkingStatuses)
      .andWhere('t.role', Team.Role.editorialAssistant)
      // when we run the script 2020-08-02-redistribute-eas, we had a bug and the events wasn't emitted
      // that's why we need this script to emit the events for the manuscripts that was redistributed on 12/08/2020
      .andWhere('tm.created', '>', new Date(2020, 7, 12).toISOString())
      .andWhere('tm.created', '<', new Date(2020, 7, 13).toISOString())
      .andWhere('m.journal_id', journalId)
      .as('submission_editorial_assistants')

  logger.info('---')

  await Promise.each(journalsWithMultipleEAs, async journal => {
    logger.info(`Finding submissions for journal ${journal.id}`)
    const manuscripts = await getManuscriptsRedistributed(journal.id)

    logger.info(`Found ${manuscripts.length} submissions that need events.`)

    await Promise.each(manuscripts, async manuscript => {
      if (isDebug) return

      logger.info(`Emitting events for submission ${manuscript.submissionId}`)

      await eventsService.publishSubmissionEvent({
        submissionId: manuscript.submissionId,
        eventName: 'SubmissionEditorialAssistantRemoved',
      })
      await eventsService.publishSubmissionEvent({
        submissionId: manuscript.submissionId,
        eventName: 'SubmissionEditorialAssistantAssigned',
      })
    })

    logger.info('---')
  })

  if (!isDebug) logger.info('Successfully emitted all events!')

  process.exit()
}

execute()
