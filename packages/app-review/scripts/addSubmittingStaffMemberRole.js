const { db } = require('@pubsweet/db-manager')

const execute = async () => {
  await db('role').insert({ name: 'submittingStaffMember' })

  process.exit()
}

execute()
