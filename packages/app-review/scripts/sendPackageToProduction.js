const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const { createQueueService } = require('@hindawi/queue-service')
const { useCases, eventMapper } = require('component-mts-production')
const { Manuscript } = models

const execute = async () => {
  const submissionId = process.argv[2]

  if (!submissionId) {
    logger.error('Please provide submissionId as argument')
    process.exit()
  }

  await createQueueService()
    .then(queueService => {
      global.applicationEventBus = queueService
    })
    .catch(err => {
      logger.error(err)
      process.exit(1)
    })

  const lastManuscript = await Manuscript.findLastManuscriptBySubmissionId({
    submissionId,
    eagerLoadRelations:
      '[journal.journalPreprints.preprint, articleType, section, specialIssue, files]',
  })
  const {
    authors,
    editorialAssistant,
    submittingStaffMember,
    correspondingEditorialAssistant,
  } = await useCases.getDataForSubmittedManuscriptUseCase
    .initialize(models)
    .execute({
      manuscriptId: lastManuscript.id,
      journalId: lastManuscript.journalId,
    })

  const {
    academicEditor,
    reviews,
  } = await useCases.getDataForPeerReviewedManuscriptUseCase
    .initialize(models)
    .execute(lastManuscript.id)
  const {
    figureFiles,
  } = await useCases.getFilesForFtpUploadUseCase
    .initialize({ models })
    .execute(lastManuscript.id)

  try {
    const finalRevisionDate = await Manuscript.getFinalRevisionDate({
      submissionId: manuscript.submissionId,
    })

    const ppEventData = eventMapper.createPpEventData({
      authors,
      reviews,
      screeners: [],
      manuscript,
      figureFiles,
      academicEditor,
      editorialAssistant,
      submittingStaffMember,
      correspondingEditorialAssistant,
      finalRevisionDate,
    })

    await applicationEventBus.publishMessage({
      event: 'ProductionPackageQcArchiveRequested',
      data: ppEventData,
    })
  } catch (error) {
    logger.warn(
      `Error creating ProductionPackageQcArchiveRequested  event. Cause: ${e.message}`,
    )
  }

  process.exit(0)
}
execute()
