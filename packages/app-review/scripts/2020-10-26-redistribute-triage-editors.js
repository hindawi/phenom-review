const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const { createQueueService } = require('@hindawi/queue-service')
const events = require('component-events')
const Promise = require('bluebird')

const config = require('config')
const Email = require('component-sendgrid')
const { logEvent } = require('component-activity-log/server')
const { getModifiedText } = require('component-transform-text')

const urlService = require('component-peer-review/server/src/urlService/urlService')

const getProps = require('component-peer-review/server/src/emailPropsService/emailPropsService')
const getEmailCopy = require('component-peer-review/server/src/notifications/reassignTriageEditor/getEmailCopy')
const reassignTriageEditorNotifications = require('component-peer-review/server/src/notifications/reassignTriageEditor/reassignTriageEditor')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const {
  useCases: { reassignTriageEditorUseCase },
} = require('component-peer-review')

const { Manuscript, TeamMember, Team, Identity } = models

const execute = async () => {
  const userEmailForReassignment = process.argv[2]
  let manuscriptCustomIds = process.argv[3]

  if (!userEmailForReassignment) {
    logger.error('Please provide an user id for re-assignment as argument')
    process.exit(9)
  }

  if (!manuscriptCustomIds) {
    logger.error(
      'Please provide a list of manuscripts that need to be reassigned as argument',
    )
    process.exit(9)
  }

  manuscriptCustomIds = manuscriptCustomIds.split(',')

  await createQueueService()
    .then(queueService => {
      global.applicationEventBus = queueService
    })
    .catch(err => {
      logger.error(err)
      process.exit(1)
    })

  const eventsService = events.initialize({ models })
  const getEmailCopyService = getEmailCopy.initialize()
  const getPropsService = getProps.initialize({
    baseUrl,
    urlService,
    footerText,
    unsubscribeSlug,
    getModifiedText,
  })
  const notificationService = reassignTriageEditorNotifications.initialize({
    Email,
    getPropsService,
    getEmailCopyService,
  })

  const reassignTriageEditor = reassignTriageEditorUseCase.initialize({
    models,
    logEvent,
    eventsService,
    notificationService,
  })
  const userIdentity = await Identity.findOneBy({
    queryObject: {
      email: userEmailForReassignment,
    },
  })

  await Promise.each(manuscriptCustomIds, async customId => {
    const manuscript = await Manuscript.findByCustomId(customId)
    const activeEditorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
        manuscriptId: manuscript.id,
      },
    )
    const journalTeamMemberForAssignmentMember = await TeamMember.findOneByJournalAndUser(
      { journalId: manuscript.journalId, userId: userIdentity.userId },
    )

    await reassignTriageEditor.execute({
      teamMemberId: journalTeamMemberForAssignmentMember.id,
      userId: activeEditorialAssistant.userId,
      manuscriptId: manuscript.id,
    })
  })

  process.exit()
}

execute()
