const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const { transaction } = require('objection')
const { isEmpty } = require('lodash')

const { Manuscript, Team, TeamMember } = models

const execute = async () => {
  const submissionId = process.argv[2]

  if (!submissionId) {
    logger.error('Please provide an submission id as argument')
    process.exit(9)
  }

  const draftManuscript = await Manuscript.findOneBy({
    queryObject: {
      submissionId,
      status: Manuscript.Statuses.draft,
    },
  })

  if (!draftManuscript) {
    logger.error('Cannot find a draft manuscript with provided submission id')
    process.exit(9)
  }

  let revisionManuscript = await Manuscript.findOneBy({
    queryObject: {
      submissionId,
      status: Manuscript.Statuses.revisionRequested,
    },
  })
  if (!revisionManuscript) {
    revisionManuscript = await Manuscript.findOneBy({
      queryObject: {
        submissionId,
        status: Manuscript.Statuses.qualityChecksRequested,
      },
    })
  }
  if (!revisionManuscript) {
    logger.error(
      'Cannot find a revision or qc manuscript with provided submission id',
    )
    process.exit(9)
  }

  const authors = await createNewMembers({
    teamRole: Team.Role.author,
    draftManuscriptId: draftManuscript.id,
    revisionManuscriptId: revisionManuscript.id,
  })

  const editorialAssistants = await createNewMembers({
    teamRole: Team.Role.editorialAssistant,
    draftManuscriptId: draftManuscript.id,
    revisionManuscriptId: revisionManuscript.id,
  })

  const triageEditors = await createNewMembers({
    teamRole: Team.Role.triageEditor,
    draftManuscriptId: draftManuscript.id,
    revisionManuscriptId: revisionManuscript.id,
  })

  const academicEditors = await createNewMembers({
    teamRole: Team.Role.academicEditor,
    draftManuscriptId: draftManuscript.id,
    revisionManuscriptId: revisionManuscript.id,
  })

  try {
    await transaction(TeamMember.knex(), async trx => {
      authors && (await TeamMember.query(trx).upsertGraph(authors))
      triageEditors && (await TeamMember.query(trx).upsertGraph(triageEditors))
      editorialAssistants &&
        (await TeamMember.query(trx).upsertGraph(editorialAssistants))
      academicEditors &&
        (await TeamMember.query(trx).upsertGraph(academicEditors))
    })
  } catch (err) {
    logger.error(err)
    throw new Error('Something went wrong. No data was updated.')
  }

  process.exit()
}

execute()

const createNewMembers = async ({
  teamRole,
  draftManuscriptId,
  revisionManuscriptId,
}) => {
  const queryObject = {
    role: teamRole,
    manuscriptId: draftManuscriptId,
  }
  const revisionManuscriptTeam = await Team.findOneByManuscriptAndRole({
    manuscriptId: revisionManuscriptId,
    role: teamRole,
  })
  if (!revisionManuscriptTeam) return

  const teamMembers = []
  const draftManuscriptTeam = await Team.findOrCreate({
    queryObject: queryObject,
    eagerLoadRelations: 'members',
    options: queryObject,
  })

  if (isEmpty(draftManuscriptTeam.members)) {
    const revisionManuscriptTeamMembers = await TeamMember.findAllByManuscriptAndRole(
      { manuscriptId: revisionManuscriptId, role: teamRole },
    )
    revisionManuscriptTeamMembers.forEach(member => {
      const newMember = new TeamMember({
        ...member,
        teamId: draftManuscriptTeam.id,
      })
      delete newMember.id
      teamMembers.push(newMember)
    })
  }
  return teamMembers
}
