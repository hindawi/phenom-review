const { v4 } = require('uuid')
const Promise = require('bluebird')
const { logger } = require('component-logger')
const { User, Identity } = require('@pubsweet/models')

const createUser = async role => {
  const email = process.env.EMAIL_SENDER.replace('@', `+${role}@`)

  const isCreated = await Identity.findOneBy({
    queryObject: { email },
  })
  if (isCreated) {
    logger.info(`User with email ${email} already exists.`)
    return
  }

  const user = new User({
    defaultIdentityType: 'local',
    unsubscribeToken: v4(),
    agreeTc: true,
    isActive: true,
  })
  await user.save()
  const identity = new Identity({
    userId: user.id,
    type: 'local',
    isConfirmed: true,
    email,
    givenNames: role.toUpperCase(),
    surname: role.toUpperCase(),
    title: 'dr',
    aff: 'Hindawi',
    country: 'RO',
    passwordHash:
      '$2b$12$.Ll6THdFQ1Tk26WSst75tu9/PvNjAjqu2xhkf9CZAIlomEh.Ah0pS',
  })
  await identity.save()
}

const execute = async () => {
  if (!process.env.EMAIL_SENDER) {
    logger.error('Please provide EMAIL_SENDER in "packages/app-review/.env"')
    throw new Error('No EMAIL_SENDER variable found')
  }

  let users = process.argv.slice(2)
  if (!users[0]) {
    logger.info('No users provided. Creating default users.')
    users = ['ea', 'ce', 'ae', 'a1', 'r1', 'r2']
  }
  await Promise.each(users, createUser)
}

execute()
