require('dotenv').config()

const baseConfig = {
  FEM_SERVICE_URL: process.env.FEM_SERVICE_URL,
  LOG_FILE: process.env.LOG_FILE,
}

const QAConfig = Object.assign(
  {
    ENV: 'qa',
    REVIEW_DB_HOST: process.env.QA_REVIEW_DB_HOST,
    REVIEW_DB_USER: process.env.QA_REVIEW_DB_USER,
    REVIEW_DB_PASSWORD: process.env.QA_REVIEW_DB_PASSWORD,
    REVIEW_DB_DBNAME: process.env.QA_REVIEW_DB_DBNAME,
  },
  baseConfig,
)

const QAGSWConfig = Object.assign(
  {
    ENV: 'qa-gsw',
    REVIEW_DB_HOST: process.env.QA_GSW_REVIEW_DB_HOST,
    REVIEW_DB_USER: process.env.QA_GSW_REVIEW_DB_USER,
    REVIEW_DB_PASSWORD: process.env.QA_GSW_REVIEW_DB_PASSWORD,
    REVIEW_DB_DBNAME: process.env.QA_GSW_REVIEW_DB_DBNAME,
  },
  baseConfig,
)

const QAGammaConfig = Object.assign(
  {
    ENV: 'qa-gamma',
    REVIEW_DB_HOST: process.env.QA_GAMMA_REVIEW_DB_HOST,
    REVIEW_DB_USER: process.env.QA_GAMMA_REVIEW_DB_USER,
    REVIEW_DB_PASSWORD: process.env.QA_GAMMA_REVIEW_DB_PASSWORD,
    REVIEW_DB_DBNAME: process.env.QA_GAMMA_REVIEW_DB_DBNAME,
  },
  baseConfig,
)

const DemoConfig = Object.assign(
  {
    ENV: 'demo',
    REVIEW_DB_HOST: process.env.DEMO_REVIEW_DB_HOST,
    REVIEW_DB_USER: process.env.DEMO_REVIEW_DB_USER,
    REVIEW_DB_PASSWORD: process.env.DEMO_REVIEW_DB_PASSWORD,
    REVIEW_DB_DBNAME: process.env.DEMO_REVIEW_DB_DBNAME,
  },
  baseConfig,
)

const DemoGSWConfig = Object.assign(
  {
    ENV: 'demo-gsw',
    REVIEW_DB_HOST: process.env.DEMO_GSW_REVIEW_DB_HOST,
    REVIEW_DB_USER: process.env.DEMO_GSW_REVIEW_DB_USER,
    REVIEW_DB_PASSWORD: process.env.DEMO_GSW_REVIEW_DB_PASSWORD,
    REVIEW_DB_DBNAME: process.env.DEMO_GSW_REVIEW_DB_DBNAME,
  },
  baseConfig,
)

const DemoSalesConfig = Object.assign(
  {
    ENV: 'demo-sales',
    REVIEW_DB_HOST: process.env.DEMO_SALES_REVIEW_DB_HOST,
    REVIEW_DB_USER: process.env.DEMO_SALES_REVIEW_DB_USER,
    REVIEW_DB_PASSWORD: process.env.DEMO_SALES_REVIEW_DB_PASSWORD,
    REVIEW_DB_DBNAME: process.env.DEMO_SALES_REVIEW_DB_DBNAME,
  },
  baseConfig,
)

const ProdConfig = Object.assign(
  {
    ENV: 'production',
    REVIEW_DB_HOST: process.env.PROD_REVIEW_DB_HOST,
    REVIEW_DB_USER: process.env.PROD_REVIEW_DB_USER,
    REVIEW_DB_PASSWORD: process.env.PROD_REVIEW_DB_PASSWORD,
    REVIEW_DB_DBNAME: process.env.PROD_REVIEW_DB_DBNAME,
  },
  baseConfig,
)

const ProdGSWConfig = Object.assign(
  {
    ENV: 'production-gsw',
    REVIEW_DB_HOST: process.env.PROD_GSW_REVIEW_DB_HOST,
    REVIEW_DB_USER: process.env.PROD_GSW_REVIEW_DB_USER,
    REVIEW_DB_PASSWORD: process.env.PROD_GSW_REVIEW_DB_PASSWORD,
    REVIEW_DB_DBNAME: process.env.PROD_GSW_REVIEW_DB_DBNAME,
  },
  baseConfig,
)

const LocalConfig = Object.assign(
  {
    ENV: 'development',
    REVIEW_DB_HOST: process.env.DB_HOST,
    REVIEW_DB_USER: process.env.DB_USER,
    REVIEW_DB_PASSWORD: process.env.DB_PASS,
    REVIEW_DB_DBNAME: process.env.DATABASE,
  },
  baseConfig,
)

const inProgressStatuses = [
  'submitted',
  'underReview',
  'makeDecision',
  'reviewCompleted',
  'pendingApproval',
  'reviewersInvited',
  'revisionRequested',
  'academicEditorInvited',
  'academicEditorAssigned',
  'qualityChecksSubmitted',
  'inQA',
  'qualityChecksRequested',
  'accepted',
  'technicalChecks',
]

const peerReviewStatuses = [
  'submitted',
  'underReview',
  'makeDecision',
  'reviewCompleted',
  'pendingApproval',
  'reviewersInvited',
  'revisionRequested',
  'academicEditorInvited',
  'academicEditorAssigned',
  'qualityChecksSubmitted',
  'inQA',
  'qualityChecksRequested',
  'accepted',
]

module.exports = {
  QAConfig,
  QAGSWConfig,
  QAGammaConfig,
  DemoConfig,
  DemoGSWConfig,
  DemoSalesConfig,
  ProdConfig,
  ProdGSWConfig,
  LocalConfig,
  inProgressStatuses,
  peerReviewStatuses,
}
