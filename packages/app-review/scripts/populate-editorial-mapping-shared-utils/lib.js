const Knex = require('knex')
const axios = require('axios')

/**
 * Create a db connection
 *
 * @param {*} config
 * @returns dbConnection
 */
function getConnectionToReviewDB(config) {
  return new Knex({
    client: 'pg',
    connection: {
      host: config.REVIEW_DB_HOST,
      user: config.REVIEW_DB_USER,
      password: config.REVIEW_DB_PASSWORD,
      database: config.REVIEW_DB_DBNAME,
    },
  })
}

/**
 * Count the results from a query
 *
 * @param {*} connection
 * @param {*} query
 */
async function count(connection, queryBuilder) {
  return connection
    .select(connection.raw('count(*)'))
    .from(queryBuilder.clone().as('entries'))
    .first()
}

/**
 * Batch a query by adding limit and offset
 *
 * @param {*} queryBuilder
 * @param {*} offset
 * @param {*} limit
 */
async function batch(queryBuilder, offset, limit) {
  const addLimits = query => query.limit(limit).offset(offset)

  return queryBuilder.modify(addLimits)
}

/**
 * Get manuscripts following the rules:
 * - newer than 3 months old if in status Draft
 * - have an article_type_id different from null
 * - deduplicated by submission_id <- hence defined as submissions
 *
 * @param {*} connection
 * @param {*} inProgressStatuses
 * @returns Query Builder for get submission
 */
function getSubmissions(connection, inProgressStatuses) {
  return connection
    .select(
      connection.raw(
        'DISTINCT ON (manuscript.submission_id) manuscript.*, prm.name as peer_review_model, at.name as article_type',
      ),
    )
    .from('manuscript')
    .join('journal', 'journal.id', 'manuscript.journal_id')
    .join('peer_review_model as prm', 'prm.id', 'journal.peer_review_model_id')
    .join('article_type as at', 'at.id', 'manuscript.article_type_id')
    .whereNot('manuscript.article_type_id', null)
    .andWhere(function() {
      this.whereIn('manuscript.status', inProgressStatuses)
    })
    .orWhere(function() {
      this.where('manuscript.status', 'draft').andWhere(
        'manuscript.created',
        '>',
        connection.raw("CURRENT_DATE - INTERVAL '3 months'"),
      )
    })
    .orderBy('manuscript.submission_id', 'asc')
}

/**
 * Get manuscripts following the rules:
 * - newer than 3 months old if in status Draft
 * - have an article_type_id different from null
 * - deduplicated by submission_id <- hence defined as submissions
 *
 * @param {*} connection
 * @param {*} inProgressStatuses
 * @returns Query Builder for get submission
 */
function getCreatedButNotPopulated(connection, inProgressStatuses) {
  const statusesWithDraft = [...inProgressStatuses, 'draft']

  const editorialModelForSubmission = query =>
    query
      .select('mapping.id')
      .from('submission_editorial_mapping as mapping')
      .whereRaw('mapping.submission_id = manuscript.submission_id')

  return connection
    .select(
      connection.raw(
        'DISTINCT ON (manuscript.submission_id) manuscript.*, prm.name as peer_review_model, at.name as article_type',
      ),
    )
    .from('manuscript')
    .join('journal', 'journal.id', 'manuscript.journal_id')
    .join('peer_review_model as prm', 'prm.id', 'journal.peer_review_model_id')
    .join('article_type as at', 'at.id', 'manuscript.article_type_id')
    .whereNot('manuscript.article_type_id', null)
    .andWhere(
      'manuscript.created',
      '>',
      connection.raw("CURRENT_DATE - INTERVAL '4 days'"),
    )
    .andWhere(function() {
      this.whereIn('manuscript.status', statusesWithDraft)
    })
    .andWhere(query => query.whereNotExists(editorialModelForSubmission))
    .orderBy('manuscript.submission_id', 'asc')
}

/**
 * Get manuscripts following the rules:
 * - newer than 3 months old if in status Draft
 * - have an article_type_id different from null
 * - deduplicated by submission_id <- hence defined as submissions
 *
 * @param {*} connection
 * @param {*} inProgressStatuses
 * @returns Query Builder for get submission
 */
function getCreatedAfterPopulate(connection, inProgressStatuses) {
  const statusesWithDraft = [...inProgressStatuses, 'draft']
  return connection
    .select(
      connection.raw(
        'DISTINCT ON (manuscript.submission_id) manuscript.*, prm.name as peer_review_model, at.name as article_type',
      ),
    )
    .from('manuscript')
    .join('journal', 'journal.id', 'manuscript.journal_id')
    .join('peer_review_model as prm', 'prm.id', 'journal.peer_review_model_id')
    .join('article_type as at', 'at.id', 'manuscript.article_type_id')
    .whereNot('manuscript.article_type_id', null)
    .andWhere(
      'manuscript.created',
      '>',
      connection.raw("CURRENT_DATE - INTERVAL '4 days'"),
    )
    .andWhere(function() {
      this.whereIn('manuscript.status', statusesWithDraft)
    })
    .orderBy('manuscript.submission_id', 'asc')
}

/**
 * Get manuscripts following the rules:
 * - older than date of population ( 25.11 )
 * - newer than 3 months old if in status Draft
 * - have an article_type_id different from null
 * - dont have an editorial model assigned
 * - deduplicated by submission_id <- hence defined as submissions
 *
 * @param {*} connection
 * @param {*} inProgressStatuses
 * @returns Query Builder for get submission
 */
function getSubmissionsWithoutSubmissionEditorialMapping(
  connection,
  inProgressStatuses,
) {
  const editorialModelForSubmission = query =>
    query
      .select('mapping.id')
      .from('submission_editorial_mapping as mapping')
      .whereRaw('mapping.submission_id = manuscript.submission_id')

  return (
    connection
      .select(
        connection.raw(
          'DISTINCT ON (manuscript.submission_id) manuscript.*, prm.name as peer_review_model, at.name as article_type',
        ),
      )
      .from('manuscript')
      .join('journal', 'journal.id', 'manuscript.journal_id')
      .join(
        'peer_review_model as prm',
        'prm.id',
        'journal.peer_review_model_id',
      )
      .join('article_type as at', 'at.id', 'manuscript.article_type_id')
      .whereNot('manuscript.article_type_id', null)
      // .andWhere("manuscript.created", ">", connection.raw("CURRENT_DATE - INTERVAL '3 days'"))
      .andWhere(function() {
        this.where(function() {
          this.whereIn('manuscript.status', inProgressStatuses)
        }).orWhere(function() {
          this.where('manuscript.status', 'draft').andWhere(
            'manuscript.created',
            '>',
            connection.raw("CURRENT_DATE - INTERVAL '3 months'"),
          )
        })
      })
      .andWhere(query => query.whereNotExists(editorialModelForSubmission))
      .orderBy('manuscript.submission_id', 'asc')
  )
}

/**
 * @param {*} connection
 * @param {*} inProgressStatuses
 * @returns Query Builder for get submission
 */
function getInProgressSubmissionsWithoutPeerReviewMapping(
  connection,
  inProgressStatuses,
) {
  const peerReviewEditorialMappingForSubmission = query =>
    query
      .select('prem.id')
      .from('peer_review_editorial_mapping as prem')
      .whereRaw('prem.submission_id = manuscript.submission_id')

  return connection
    .select(
      connection.raw(
        'DISTINCT ON (manuscript.submission_id) manuscript.*, prm.name as peer_review_model, at.name as article_type',
      ),
    )
    .from('manuscript')
    .join('journal', 'journal.id', 'manuscript.journal_id')
    .join('peer_review_model as prm', 'prm.id', 'journal.peer_review_model_id')
    .join('article_type as at', 'at.id', 'manuscript.article_type_id')
    .whereNotExists(peerReviewEditorialMappingForSubmission)
    .andWhereNot('manuscript.article_type_id', null)
    .andWhere(function() {
      this.whereIn('manuscript.status', inProgressStatuses)
    })
    .andWhere(function() {
      this.whereNotIn('at.name', [
        'Corrigendum',
        'Expression of Concern',
        'Retraction',
        'Erratum',
        'Letter to the Editor',
        'Editorial',
      ])
    })
    .orderBy(['manuscript.submission_id', 'manuscript.created'], ['asc', 'asc'])
}

/**
 * Makes a request to the FEM service with the manuscript details tuple
 *
 * @param {*} femUrl
 * @param {*} manuscript
 * @returns a full Editorial Model
 */
async function requestEditorialModel(femUrl, manuscript) {
  const response = await axios.post(femUrl, {
    submissionId: manuscript.submission_id,
    peerReviewModel: manuscript.peer_review_model,
    articleType: manuscript.article_type,
    isSpecialIssue: !!manuscript.special_issue_id,
    isSection: !!manuscript.section_id,
  })

  return response.data
}

/**
 * Makes a get request to the FEM service with the submissionId
 *
 * @param {*} femUrl
 * @param {*} submissionId
 * @returns a full Editorial Mapping
 */
async function requestEditorialMappingBySubmissionId(femUrl, submissionId) {
  const response = await axios.get(
    `${femUrl}/editorial-model?submissionId=${encodeURI(submissionId)}`,
  )
  return response.data
}

/**
 * Inserts the Submission Editorial Model for a specific submission id
 *
 * @param {*} connection
 * @param {*} submission_id
 * @param {*} submission_editorial_model
 * @returns
 */
async function saveEditorialModelForSubmissionInReview(
  connection,
  submission_id,
  submission_editorial_model,
) {
  const response = await connection
    .insert(
      {
        submission_id,
        submission_editorial_model,
      },
      ['id'],
    )
    .into('manuscript_submission_editorial_mapping')

  return response[0].id
}

/**
 * Inserts the Peer Review Editorial Mapping for a specific submission id
 *
 * @param {*} connection
 * @param {*} submission_id
 * @param {*} peer_review_editorial_mapping
 * @returns
 */
async function savePeerReviewEditorialMappingForSubmissionInReview(
  connection,
  submission_id,
  peer_review_editorial_model,
) {
  const response = await connection
    .insert(
      {
        submission_id,
        peer_review_editorial_model,
      },
      ['id'],
    )
    .into('peer_review_editorial_mapping')

  return response[0].id
}

/**
 * Extracts the global properties and the submission specific properties from the full Editorial Model
 *
 * @param {*} editorialModel
 * @returns Submission Editorial Model
 */
function extractSubmissionEditorialModel(editorialModel) {
  delete editorialModel.screening
  delete editorialModel.peerReview
  delete editorialModel.qualityChecks

  const submissionProperties = editorialModel.submission

  delete editorialModel.submission

  return {
    ...editorialModel,
    ...submissionProperties,
  }
}

/**
 * Extracts the global properties and the peer review specific properties from the full Editorial Model
 *
 * @param {*} editorialModel
 * @returns Peer Review Editorial Mapping
 */
function extractPeerReviewEditorialModel(editorialModel) {
  const peerReviewActivity = editorialModel.activities.find(
    activity => activity.name === 'peerReview',
  )
  return {
    ...peerReviewActivity.properties,
    ...peerReviewActivity.globalProperties,
  }
}

/**
 * Finds one submission editorial model after the submission_id
 *
 * @param {*} connection
 * @param {*} submission_id
 * @returns
 */
function getSubmissionEditorialModelBySubmissionId(connection, submission_id) {
  return connection
    .select('*')
    .from('manuscript_submission_editorial_mapping')
    .where('submission_id', submission_id)
    .limit(1)
    .first()
}

/**
 * Finds one peer review editorial mapping after the submission_id
 *
 * @param {*} connection
 * @param {*} submission_id
 * @returns
 */
function getPeerReviewEditorialMappingBySubmissionId(
  connection,
  submission_id,
) {
  return connection
    .select('*')
    .from('peer_review_editorial_mapping')
    .where('submission_id', submission_id)
    .limit(1)
    .first()
}

/**
 * Updates submission editorial model found by submission_id
 *
 * @param {*} connection
 * @param {*} submission_id
 * @param {*} submission_editorial_model
 */
function updateSubmissionEditorialModel(
  connection,
  submission_id,
  submission_editorial_model,
) {
  return connection('manuscript_submission_editorial_mapping')
    .where('submission_id', submission_id)
    .update({ submission_editorial_model }, ['id'])
}

module.exports = {
  getConnectionToReviewDB,
  count,
  batch,
  getSubmissions,
  getCreatedAfterPopulate,
  getSubmissionsWithoutSubmissionEditorialMapping,
  getCreatedButNotPopulated,
  getInProgressSubmissionsWithoutPeerReviewMapping,
  requestEditorialModel,
  requestEditorialMappingBySubmissionId,
  saveEditorialModelForSubmissionInReview,
  savePeerReviewEditorialMappingForSubmissionInReview,
  updateSubmissionEditorialModel,
  extractSubmissionEditorialModel,
  extractPeerReviewEditorialModel,
  getSubmissionEditorialModelBySubmissionId,
  getPeerReviewEditorialMappingBySubmissionId,
}
