const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const { createQueueService } = require('@hindawi/queue-service')
const events = require('component-events')
const Promise = require('bluebird')

const eventsService = events.initialize({ models })
const { Manuscript, SourceJournal } = models

const execute = async () => {
  const sourceJournalName = process.argv[2]
  let manuscriptCustomIds = process.argv[3]

  if (!sourceJournalName) {
    logger.error('Please provide a source journal name as argument')
    process.exit(9)
  }

  if (!manuscriptCustomIds) {
    logger.error(
      'Please provide a list of manuscripts that need to be edited as argument',
    )
    process.exit(9)
  }

  manuscriptCustomIds = manuscriptCustomIds.split(',')

  createQueueService()
    .then(queueService => {
      global.applicationEventBus = queueService
    })
    .catch(err => {
      logger.error(err)
      process.exit(1)
    })

  const sourceJournal = await SourceJournal.findOneBy({
    queryObject: {
      name: sourceJournalName,
    },
  })

  await Promise.each(manuscriptCustomIds, async customId => {
    const manuscriptVersions = await Manuscript.findAll({
      queryObject: {
        customId,
      },
    })
    await Promise.each(manuscriptVersions, async manuscript => {
      manuscript.updateProperties({ sourceJournalId: sourceJournal.id })
      await manuscript.save()
    })

    logger.info(
      `Manuscript with custom id: ${customId} was saved with source journal: ${sourceJournalName}`,
    )
    await eventsService.publishSubmissionEvent({
      submissionId: manuscriptVersions[0].submissionId,
      eventName: 'SubmissionEdited',
    })
  })

  process.exit()
}

execute()
