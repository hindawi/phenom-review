/**
 *
 * run this locally
 *
 * prereq:
 * - start fem service locally
 * - truncate submission_editorial_model table in the local fem service db
 * - add env var connection details to review database you want to populate
 *
 * implementation:
 * 1. get first batch of in progress submissions with no manuscript_submission_editorial_model ( define statuses for in progress )
 * 2. for each submission in batch
 *  - make a request to the local fem service with the tuple details
 *    ( this will save the mapping in the fem service db and will return the editorial model for the tuple on the manuscript )
 *  - save the returned editorial model in the manuscript_submission_editorial_mapping table
 * 3. when all promises resolve, get the next batch and repeat until batch size is 0
 *
 * postreq:
 * - export submission_editorial_model table from fem local db
 * - import into submission_editorial_model from the environment review db was queries from the exported data
 * - truncate local submission_editorial_model table from local fem db
 */

const fs = require('fs')

const {
  QAConfig,
  QAGammaConfig,
  QAGSWConfig,
  DemoConfig,
  DemoGSWConfig,
  ProdConfig,
  ProdGSWConfig,
  inProgressStatuses,
} = require('../populate-editorial-mapping-shared-utils/config')

const {
  getConnectionToReviewDB,
  count,
  batch,
  getSubmissions,
  getCreatedAfterPopulate,
  getSubmissionsWithoutSubmissionEditorialMapping,
  getCreatedButNotPopulated,
  requestEditorialModel,
  extractSubmissionEditorialModel,
  saveEditorialModelForSubmissionInReview,
  getSubmissionEditorialModelBySubmissionId,
  updateSubmissionEditorialModel,
} = require('../populate-editorial-mapping-shared-utils/lib')

const {
  displayLoader,
} = require('../populate-editorial-mapping-shared-utils/helpers')

async function populate(config, inProgressStatuses) {
  console.log(`▓ running populate script on ${config.ENV} environment.`)
  const reviewDBConnection = getConnectionToReviewDB(config)
  const batchSize = 100
  let submissionsPopulated = 0

  const hasMissingDetails = manuscript => {
    return (
      !manuscript.peer_review_model ||
      !manuscript.article_type ||
      typeof manuscript.special_issue_id === 'undefined' ||
      typeof manuscript.section_id === 'undefined'
    )
  }

  const totalNumberOfSubmissions = await count(
    reviewDBConnection,
    getSubmissionsWithoutSubmissionEditorialMapping(
      reviewDBConnection,
      inProgressStatuses,
    ),
  )

  console.log(
    `▓ total number of submissions to be processed: ${totalNumberOfSubmissions.count} \n`,
  )

  displayLoader(0, totalNumberOfSubmissions.count)

  const logStream = fs.openSync(config.LOG_FILE, 'w')
  do {
    const offset = submissionsPopulated
    const remainingSubmissions = totalNumberOfSubmissions.count - offset
    const limit = Math.max(Math.min(batchSize, remainingSubmissions), 0)

    const manuscriptsBatch = await batch(
      getSubmissionsWithoutSubmissionEditorialMapping(
        reviewDBConnection,
        inProgressStatuses,
      ),
      offset,
      limit,
    )

    const localBatchSize = Math.min(batchSize, manuscriptsBatch.length)

    for (let i = 0; i < localBatchSize; i++) {
      let editorialModel

      if (hasMissingDetails(manuscriptsBatch[i])) {
        await fs.writeSync(
          logStream,
          `!!![SUBMISSION_DETAILS_MISSING] there are missing details for submission: ${manuscriptsBatch[i].submission_id} \n`,
        )
        continue
      }

      try {
        editorialModel = await requestEditorialModel(
          config.FEM_SERVICE_URL,
          manuscriptsBatch[i],
        )
      } catch (err) {
        await fs.writeSync(
          logStream,
          `!!![EM_REQUEST_ERROR] there was a problem fetching the editorial model for submission: ${manuscriptsBatch[i].submission_id} \n`,
        )
        continue
      }

      if (!editorialModel) {
        await fs.writeSync(
          logStream,
          `!!![EM_NOT_FOUND_ERROR] there was no editorial model found for submission: ${manuscriptsBatch[i].submission_id} \n`,
        )
        continue
      }

      const submissionEditorialModel = extractSubmissionEditorialModel(
        editorialModel,
      )

      try {
        const existingEntryForSubmission = await getSubmissionEditorialModelBySubmissionId(
          reviewDBConnection,
          manuscriptsBatch[i].submission_id,
        )

        let idOfSavedEntry

        if (existingEntryForSubmission) {
          const updateResult = await updateSubmissionEditorialModel(
            reviewDBConnection,
            manuscriptsBatch[i].submission_id,
            submissionEditorialModel,
          )
          idOfSavedEntry = updateResult[0].id
          await fs.writeSync(
            logStream,
            `■ editorial model for submission: ${manuscriptsBatch[i].submission_id} has been updated where id: ${idOfSavedEntry} \n`,
          )
        } else {
          idOfSavedEntry = await saveEditorialModelForSubmissionInReview(
            reviewDBConnection,
            manuscriptsBatch[i].submission_id,
            submissionEditorialModel,
          )
          await fs.writeSync(
            logStream,
            `■ editorial model for submission: ${manuscriptsBatch[i].submission_id} has been inserted with id: ${idOfSavedEntry} \n`,
          )
        }
      } catch (err) {
        await fs.writeSync(
          logStream,
          `!!![EM_HAS_NOT_BEEN_SAVED_ERROR] there was a problem saving the editorial model for submission: ${manuscriptsBatch[i].submission_id} \n`,
        )
        continue
      }
    }

    submissionsPopulated = submissionsPopulated + batchSize

    displayLoader(submissionsPopulated, totalNumberOfSubmissions.count)
  } while (submissionsPopulated < totalNumberOfSubmissions.count)
  fs.closeSync(logStream)

  process.exit()
}

try {
  populate(DemoGSWConfig, inProgressStatuses)
} catch (err) {
  console.log(err)
}
