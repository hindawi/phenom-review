const { raw } = require('objection')
const models = require('@pubsweet/models')
const {
  useCases: { assignEditorialAssistantOnManuscriptUseCase },
} = require('component-submission')
const {
  useCases: { getUserWithWorkloadUseCase },
} = require('component-model')

const journalIdToFix = process.argv.slice(2)[0]

const events = require('../../component-events')

const { Manuscript, Team, TeamMember } = models

const manuscriptStatuses = [
  ...Manuscript.InProgressStatuses,
  Manuscript.Statuses.inQA,
  Manuscript.Statuses.technicalChecks,
  Manuscript.Statuses.qualityChecksRequested,
  Manuscript.Statuses.qualityChecksSubmitted,
  Manuscript.Statuses.draft,
]

const latest = Manuscript.query()
  .select(
    raw(
      'DISTINCT ON (m.submission_id) m.id, m.is_latest_version, m.custom_id, m.status, m.version, m.journal_id',
    ),
  )
  .from('manuscript as m')
  .where(query =>
    query
      .whereNot('m.version', '1')
      .orWhereNot('m.status', Manuscript.Statuses.draft),
  )
  .orderByRaw(
    `m.submission_id,
      m.created DESC,
      m.updated DESC`,
  )
  .as('latest')

function findManuscriptsWithNoEATeamOnJournal(journalId) {
  return Manuscript.query()
    .select('*')
    .from(latest)
    .whereNotExists(
      Team.query()
        .whereRaw('latest.id = team.manuscript_id')
        .andWhere('team.role', 'editorialAssistant'),
    )
    .andWhere('latest.journalId', journalId)
    .andWhere(query => query.whereIn('latest.status', manuscriptStatuses))
}

function findManuscriptsWithNoActiveEA(journalId) {
  return Manuscript.query()
    .select('*')
    .from(latest)
    .whereNotExists(
      Team.query()
        .whereExists(
          TeamMember.query()
            .whereRaw('team.id = team_member.team_id')
            .andWhere('team_member.status', 'active'),
        )
        .andWhereRaw('latest.id = team.manuscript_id')
        .andWhere('team.role', 'editorialAssistant'),
    )
    .andWhere('latest.journalId', journalId)
    .andWhere(query => query.whereIn('latest.status', manuscriptStatuses))
}

async function fixManuscriptEAAssignmentForJournal(journalId) {
  console.log(`started looking in the journal with id: ${journalId}`)

  const manuscriptsWithCoruptEAAssignment = await findManuscriptsWithNoActiveEA(
    journalId,
  )
  console.log(
    `found ${manuscriptsWithCoruptEAAssignment.length} manuscripts with no EA TEAM or no active member in the EA team`,
  )

  if (!manuscriptsWithCoruptEAAssignment.length) {
    console.log('no unassigned manuscripts found. exiting')
    process.exit(0)
  }

  // start assigning
  const eventsService = events.initialize({ models })

  const getUserWithWorkload = await getUserWithWorkloadUseCase.initialize(
    models,
  )

  const initializedAssigningUseCase = assignEditorialAssistantOnManuscriptUseCase.initialize(
    {
      eventsService,
      manuscriptStatuses,
      models,
      getUserWithWorkload,
    },
  )

  for await (manuscript of manuscriptsWithCoruptEAAssignment) {
    const {
      journalId,
      sectionId,
      id: manuscriptId,
      submissionId,
      specialIssueId,
    } = manuscript

    await initializedAssigningUseCase.execute({
      journalId,
      sectionId,
      manuscriptId,
      submissionId,
      specialIssueId,
    })
  }

  console.log(
    `following ${manuscriptsWithCoruptEAAssignment.length} manuscripts (custom id) were reassigned:`,
  )
  console.dir(
    manuscriptsWithCoruptEAAssignment.map(m => m.customId),
    { maxArrayLength: Infinity },
  )

  process.exit(0)
}

fixManuscriptEAAssignmentForJournal(journalIdToFix)
