const models = require('@pubsweet/models')
const logger = require('@pubsweet/logger')
const { createQueueService } = require('@hindawi/queue-service')
const { Promise } = require('bluebird')
const events = require('component-events')
const eventsService = events.initialize({ models })

const specialIssues = require('./specialIssueIds.json')
const { SpecialIssue, Section } = models

const execute = async () => {
  try {
    await createQueueService()
      .then(queueService => {
        global.applicationEventBus = queueService
      })
      .catch(err => {
        logger.error(err)
        process.exit(1)
      })
    await Promise.each(specialIssues, async specialIssue => {
      const si = await SpecialIssue.findOneByNameAndCustomId({
        name: specialIssue.name,
        customId: specialIssue.customId,
      })
      if (!si) {
        logger.info(
          `No special issue was found with name: ${specialIssue.name} and customId: ${specialIssue.customId}`,
        )
        return
      }

      si.updateProperties({
        customId: await generateUniqueCustomId(),
      })
      await si.save()
      logger.info(
        `Updated customId for Special Issue: ${si.name}. New customId: ${si.customId}`,
      )

      // event
      let parentType = 'Journal'
      let { journalId, sectionId } = si

      if (sectionId) {
        const section = await Section.find(sectionId)
        ;({ journalId } = section)
        parentType = 'JournalSection'
      }

      await eventsService.publishJournalEvent({
        journalId,
        eventName: `${parentType}SpecialIssueUpdated`,
      })
      await eventsService.publishSpecialIssueEvent({
        specialIssueId,
        eventName: 'SpecialIssueUpdated',
      })
    })

    process.exit()
  } catch (err) {
    logger.error(err)
    throw new Error('Something went wrong.')
  }
}
execute()

async function generateUniqueCustomId() {
  const customId = `${Math.round(100000 + Math.random() * 899990).toString()}`
  const found = await SpecialIssue.findByCustomId(customId)

  if (!found) {
    return customId
  }

  return generateUniqueCustomId()
}
