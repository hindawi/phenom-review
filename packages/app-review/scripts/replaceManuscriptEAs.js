const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const events = require('../../component-events')
const { groupBy } = require('lodash')
const { Promise } = require('bluebird')

const eventsService = events.initialize({ models })
// This will no longer work as the use case is not being exported with the initialize-execute pattern
const {
  getUserForAutoAssignmentBasedOnWorkload,
} = require('../../component-model/src/useCases/user')
const { createQueueService } = require('@hindawi/queue-service')

const { Manuscript, Team, TeamMember, Journal, User } = models

const execute = async () => {
  try {
    createQueueService()
      .then(queueService => {
        global.applicationEventBus = queueService
      })
      .catch(err => {
        logger.error(err)
        process.exit(1)
      })

    const journals = await Journal.findAllActiveJournals()

    await Promise.each(journals, async journal => {
      const journalEditorialAssistants = await TeamMember.findAllByJournalAndRole(
        {
          journalId: journal.id,
          role: Team.Role.editorialAssistant,
        },
      )
      const journalEditorialAssistantsUserIds = journalEditorialAssistants.map(
        journalEditorialAssistant => journalEditorialAssistant.userId,
      )
      const manuscriptsWithWrongEA = await Manuscript.findManuscriptsWithWrongEA(
        {
          journalId: journal.id,
          journalEditorialAssistantsUserIds,
          Team,
          TeamMember,
        },
      )

      const groupedManuscripts = groupBy(manuscriptsWithWrongEA, 'submissionId')

      const submissions = Object.values(groupedManuscripts)

      await Promise.each(submissions, async submission => {
        await removeWrongEditorialAssistant({
          submission,
        })
      })
      await Promise.each(submissions, async submission => {
        await assignEditorialAssistant({
          submission,
        })
      })
    })
    process.exit()
  } catch (err) {
    logger.error(err)
    throw new Error('Something went wrong.')
  }
}
execute()

async function removeWrongEditorialAssistant({ submission }) {
  await Promise.each(submission, async ({ id: manuscriptId }) => {
    const currentEditorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    await currentEditorialAssistant.delete()

    logger.info(
      `Removed Editorial Assistant with id ${currentEditorialAssistant.userId} from manuscript with id ${manuscriptId}`,
    )
  })
  await eventsService.publishSubmissionEvent({
    submissionId: submission[0].submissionId,
    eventName: 'SubmissionEditorialAssistantRemoved',
  })
}
async function assignEditorialAssistant({ submission }) {
  const userForAssignment = await getUserForAutoAssignmentBasedOnWorkload({
    role: Team.Role.editorialAssistant,
    journalId: submission[0].journalId,
    manuscriptStatuses: [
      ...Manuscript.InProgressStatuses,
      Manuscript.Statuses.technicalChecks,
    ],
    teamMemberStatuses: [TeamMember.Statuses.active],
    models,
  })
  const user = await User.find(userForAssignment.userId, 'identities')
  await Promise.each(submission, async ({ id: manuscriptId }) => {
    const editorialAssistantTeam = await Team.findOneByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.editorialAssistant,
    })
    const editorialAssistantTeamMember = editorialAssistantTeam.addMember({
      user,
      options: { status: TeamMember.Statuses.active },
    })
    await editorialAssistantTeamMember.save()

    logger.info(
      `Assigned Editorial Assistant with id ${user.id} on manuscript with id ${manuscriptId}`,
    )
  })
  await eventsService.publishSubmissionEvent({
    submissionId: submission[0].submissionId,
    eventName: 'SubmissionEditorialAssistantAssigned',
  })
}
