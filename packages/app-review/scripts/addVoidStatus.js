const { db: knex } = require('@pubsweet/db-manager')
const { logger } = require('component-logger')

const execute = async () => {
  const affectedRoles = ['author', 'admin']

  // get filter
  const filter = await knex('submission_filter')
    .select('id')
    .where('name', 'archived')
    .first()

  const affectedRolesFromDB = await knex('role')
    .select('id')
    .whereIn('name', affectedRoles)

  // insert void status and get its id
  const insertedStatusesIds = await knex('submission_status')
    .insert({
      name: 'void',
      category: 'withdrawn',
    })
    .returning('id')

  const voidStatusId = insertedStatusesIds[0]

  logger.info(`inserted status with id: ${voidStatusId}`)

  // configure label objects
  const labels = affectedRolesFromDB.map(role => ({
    role_id: role.id,
    submission_status_id: voidStatusId,
    priority: 0,
    submission_filter_id: filter.id,
    name: 'Void',
  }))

  // insert labels
  const insertedLabelIds = await knex('submission_label')
    .insert(labels)
    .returning('id')

  logger.info(`inserted lalbels with ids: ${insertedLabelIds}`)

  process.exit()
}

execute()
