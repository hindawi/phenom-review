const fs = require('fs')
const { difference, shuffle } = require('lodash')

const CUSTOM_ID_FIELD = 'custom_id'
const GSW_PERCENTAGE = 1

const execute = async () => {
  const csvPaths = process.argv.slice(2)

  let allUsedIds = []
  for (let i = 0; i < csvPaths.length; i++) {
    const ids = await getArrayFromCSV(csvPaths[i])
    allUsedIds = allUsedIds.concat(ids)
  }
  console.log(`Found ${allUsedIds.length} used ids`)

  const availableIds = generateAvailableIds(allUsedIds)
  console.log(`Found ${availableIds.length} available ids`)

  const numberOfGSWcustomIds = Math.round(
    (GSW_PERCENTAGE * availableIds.length) / 100,
  )

  const gswAvailableIds = availableIds.slice(0, numberOfGSWcustomIds)
  writeToCSV('gswCustomIds.csv', gswAvailableIds, CUSTOM_ID_FIELD)

  const hindawiAvailableIds = availableIds.slice(numberOfGSWcustomIds)
  writeToCSV('hindawiCustomIds.csv', hindawiAvailableIds, CUSTOM_ID_FIELD)

  console.log(
    `Checksum ids split\n`,
    `All available ids count: ${availableIds.length}\n`,
    `GSW + Hindawi ids count: ${gswAvailableIds.length +
      hindawiAvailableIds.length}\n`,
  )
}

const writeToCSV = (fileName, arr, fieldName) => {
  fs.writeFileSync(fileName, `${fieldName}\n${arr.join('\n')}`, err => {
    if (err) console.log(err)
  })
  console.log(`${fileName} created with ${arr.length} custom ids`)
}

const getArrayFromCSV = async path => {
  const rawData = fs.readFileSync(path, 'utf8', err => {
    if (err) console.log(err)
  })

  const records = rawData.split('\n').slice(1)
  return records.filter(record => !!record).map(record => parseInt(record))
}
const generateAvailableIds = allUsedIds => {
  const allIds = []
  for (let i = 1000000; i <= 9999999; i++) {
    allIds.push(i)
  }
  const availableIds = difference(allIds, allUsedIds)
  return shuffle(availableIds)
}

execute()
