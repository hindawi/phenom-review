const {
  QAConfig,
  QAGSWConfig,
  QAGammaConfig,
  DemoConfig,
  DemoGSWConfig,
  DemoSalesConfig,
  ProdConfig,
  ProdGSWConfig,
  LocalConfig,
  peerReviewStatuses,
} = require('../populate-editorial-mapping-shared-utils/config')

const {
  getConnectionToReviewDB,
  count,
  getInProgressSubmissionsWithoutPeerReviewMapping,
} = require('../populate-editorial-mapping-shared-utils/lib')

async function countSubmissions(config, peerReviewStatuses) {
  console.log(`▓ running count script on the ${config.ENV} environment.`)

  const reviewDBConnection = await getConnectionToReviewDB(config)

  const totalNumberOfSubmissions = await count(
    reviewDBConnection,
    getInProgressSubmissionsWithoutPeerReviewMapping(
      reviewDBConnection,
      peerReviewStatuses,
    ),
  )

  console.log(
    `▓ total number of submissions to process: ${totalNumberOfSubmissions.count}.`,
  )

  process.exit()
}

try {
  countSubmissions(LocalConfig, peerReviewStatuses)
} catch (err) {
  console.log(err)
}
