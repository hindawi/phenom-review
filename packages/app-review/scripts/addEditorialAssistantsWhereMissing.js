const { groupBy } = require('lodash')
const { Promise } = require('bluebird')

const { db } = require('@pubsweet/db-manager')
const models = require('@pubsweet/models')
// This will no longer work as the use case is not being exported with the initialize-execute pattern
const {
  getUserForAutoAssignmentBasedOnWorkload,
} = require('../../component-model/src/useCases/user')

const { logger } = require('component-logger')
const events = require('../../component-events')

// control function
const execute = async () => {
  const eventsService = events.initialize({ models })
  const manuscripts = await getManuscriptsWithNoEATeam({ models, db })
  const groupedManuscriptsList = Object.values(
    groupBy(manuscripts, 'submissionId'),
  )
  await Promise.each(groupedManuscriptsList, async groupedManuscripts => {
    let user = await getExistingEAOnSubmission({
      submissionId: groupedManuscripts[0].submissionId,
      models,
    })[0]

    if (!user) {
      logger.info(
        `No existing EA found for submission with id ${groupedManuscripts[0].submissionId}. Will assign a new one by workload`,
      )
      user = await getNewEAByWorkload({
        journalId: groupedManuscripts[0].journalId,
        getUserByWorkload: getUserForAutoAssignmentBasedOnWorkload,
        models,
      })
    }

    await Promise.each(groupedManuscripts, async manuscript => {
      await assignEditorialAssistant({
        manuscript,
        user,
        models,
        eventsService,
        logger,
      })

      return Promise.resolve()
    })

    return Promise.resolve()
  })

  process.exit()
}

// [start] core functions

function getManuscriptsWithNoEATeam({ models, db }) {
  const { Manuscript } = models

  const manuscriptJournalIdColumnIdentifier = Manuscript.ref('m.journalId')
  const manuscriptIdColumnIdentifier = Manuscript.ref('m.id')

  return Manuscript.query()
    .alias('m')
    .select('m.id', 'm.submissionId', 'm.journalId')
    .whereExists(query =>
      query
        .select('tj.id')
        .from('team as tj')
        .where('tj.role', 'editorialAssistant')
        .andWhere('tj.journalId', manuscriptJournalIdColumnIdentifier),
    )
    .andWhere(query =>
      query.whereNotExists(query =>
        query
          .select('t.manuscriptId', 't.role')
          .from('team as t')
          .where('t.manuscriptId', manuscriptIdColumnIdentifier)
          .andWhere('t.role', 'editorialAssistant'),
      ),
    )
    .andWhere(db.raw('m.version::DOUBLE PRECISION'), '>', 1.0)
}

function getExistingEAOnSubmission({ submissionId, models }) {
  const { User } = models

  return User.query()
    .alias('u')
    .join('team_member as tm', 'u.id', 'tm.userId')
    .join('team as t', 'tm.teamId', 't.id')
    .join('manuscript as m', 't.manuscriptId', 'm.id')
    .where('m.submissionId', submissionId)
    .andWhere('t.role', 'editorialAssistant')
    .andWhere('tm.status', 'active')
    .withGraphFetched('identities')
}

async function getNewEAByWorkload({ journalId, models, getUserByWorkload }) {
  const { Manuscript, TeamMember, Team, User } = models
  const userForAssignment = await getUserByWorkload({
    role: Team.Role.editorialAssistant,
    journalId,
    manuscriptStatuses: [
      ...Manuscript.InProgressStatuses,
      Manuscript.Statuses.technicalChecks,
    ],
    teamMemberStatuses: [TeamMember.Statuses.active],
    models,
  })

  const user = await User.find(userForAssignment.userId, 'identities')
  return user
}

async function assignEditorialAssistant({
  manuscript,
  user,
  models,
  eventsService,
  logger,
}) {
  const { TeamMember, Team } = models
  const { id: manuscriptId, submissionId } = manuscript

  const editorialAssistantTeam = new Team({
    role: Team.Role.editorialAssistant,
    manuscriptId,
  })

  editorialAssistantTeam.addMember({
    user,
    options: { status: TeamMember.Statuses.active },
  })
  await editorialAssistantTeam.saveGraph({
    relate: true,
    noUpdate: true,
  })
  logger.info(
    `Assigned Editorial Assistant with id ${user.id} on manuscript with id ${manuscriptId}`,
  )
  eventsService.publishSubmissionEvent({
    submissionId,
    eventName: 'SubmissionEditorialAssistantAssigned',
  })
}

// [end] core functions

execute()
