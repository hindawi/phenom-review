const config = require('config')
const { default: KeycloakAdminClient } = require('keycloak-admin')
const models = require('@pubsweet/models')
const Knex = require('knex')

const {
  public: { authServerURL, realm },
  admin: { username, password },
} = config.get('keycloak')

const PAGE_SIZE = +process.env.PAGE_SIZE || 20

main().catch(console.error)

async function main() {
  const ssoIds = await fetchSsoIdsEmailMap()
  const keycloakUpdater = await initKeycloakUpdater(ssoIds)

  let notDone = true
  let currentIndex = 0

  const progress = setInterval(() => {
    console.log('Count ', currentIndex)
  }, 2000 * 60)

  while (notDone) {
    // eslint-disable-next-line no-await-in-loop
    const identities = await models.Identity.query()
      .where({ type: 'local' })
      .orderBy('id', 'desc')
      .offset(currentIndex)
      .limit(PAGE_SIZE)

    currentIndex += identities.length

    // eslint-disable-next-line no-await-in-loop
    try {
      await Promise.all(identities.map(keycloakUpdater.updatePhenomIdInSSO))
    } catch (e) {
      await keycloakUpdater.kcAdminAuth()
      await Promise.all(identities.map(keycloakUpdater.updatePhenomIdInSSO))
    }

    notDone = identities.length === PAGE_SIZE
  }

  clearInterval(progress)
}

async function initKeycloakUpdater(ssoIds) {
  const kcAdminClient = new KeycloakAdminClient({
    baseUrl: authServerURL,
    realmName: 'master',
  })
  await kcAdminClient.auth({
    username,
    password,
    grantType: 'password',
    clientId: 'admin-cli',
  })
  kcAdminClient.setConfig({
    realmName: realm,
  })

  return {
    async kcAdminAuth() {
      await kcAdminClient.auth({
        username,
        password,
        grantType: 'password',
        clientId: 'admin-cli',
      })
    },
    async updatePhenomIdInSSO(identity) {
      if (!ssoIds[identity.email]) {
        console.log('non existent user ' + identity.email)
        return
      }

      const ssoUser = await kcAdminClient.users.findOne({
        id: ssoIds[identity.email],
      })

      if (!ssoUser) return

      await kcAdminClient.users.update(
        { id: ssoUser.id },
        {
          attributes: {
            ...ssoUser.attributes,
            phenomId: identity.userId,
          },
        },
      )
    },
  }
}

async function fetchSsoIdsEmailMap() {
  const knexSSODB = new Knex({
    client: 'pg',
    connection: {
      host: process.env.SSO_DB_URL,
      user: process.env.SSO_DB_USER,
      password: process.env.SSO_DB_PASS,
      database: process.env.SSO_DB_DATABASE,
    },
  })
  const { rows: ssoIds } = await knexSSODB.raw(
    'select json_object_agg(username, id) from user_entity',
  )
  return ssoIds[0].json_object_agg
}
