const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const Promise = require('bluebird')
const { get } = require('lodash')

const { User, TeamMember, Identity } = models
const execute = async () => {
  const users = await User.findAllWithoutIdentity()

  await Promise.each(users, async user => {
    const teamMember = await TeamMember.findOneBy({
      queryObject: {
        userId: user.id,
      },
    })
    if (!teamMember) return
    const identity = new Identity({
      userId: user.id,
      type: 'local',
      isConfirmed: true,
      email: get(teamMember, 'alias.email'),
      givenNames: get(teamMember, 'alias.givenNames'),
      surname: get(teamMember, 'alias.surname'),
      title: get(teamMember, 'alias.title'),
      aff: get(teamMember, 'alias.aff'),
      country: get(teamMember, 'alias.country'),
    })
    await identity.save()
    logger.info(`Created identity for user id ${user.id}`)
  })

  process.exit()
}
execute()
