const { db } = require('@pubsweet/db-manager')
const { updateUserRole } = require('component-sso')
const { Promise } = require('bluebird')

const TEAM_MEMBER_STATUS_MAP = {
  admin: ['pending'],
  editorialAssistant: ['active', 'accepted'],
  triageEditor: ['pending'],
  academicEditor: ['pending'],
}

const parallelSSORequests = process.env.PARALLEL_SSO_REQUESTS
const concurrentSSORequests = +process.env.PARALLEL_SSO_REQUESTS || 20

const findUsersWithRole = db => async role =>
  db
    .select(db.raw('DISTINCT ON (i.email) i.email'))
    .from('identity as i')
    .join('user as u', 'i.userId', 'u.id')
    .join('team_member as tm', 'u.id', 'tm.userId')
    .join('team as t', 'tm.teamId', 't.id')
    .whereNotNull('i.email')
    .andWhere('t.role', role)
    .andWhere(builder => {
      if (role === 'academicEditor') {
        return builder
          .whereIn('tm.status', TEAM_MEMBER_STATUS_MAP[role])
          .whereNull('t.manuscript_id') // select only academic editors with pending status on journal/section/si not on manuscript
      }
      return builder.whereIn('tm.status', TEAM_MEMBER_STATUS_MAP[role])
    })

const setRoleToSSO = ({ keycloak, role }) => async user => {
  try {
    await keycloak.updateUserRole(user.email, role)
  } catch (err) {
    console.error(err)
  }
  console.info(`Added role ${role} to users with email ${user.email}.`)
}

const treatRole = ({ db, keycloak, rolesToUpdate }) => async role => {
  const userEmailsOfThisRole = await findUsersWithRole(db)(role)

  console.info(`Found ${userEmailsOfThisRole.length} users with role ${role}.`)

  await Promise.map(
    userEmailsOfThisRole,
    async userEmail =>
      setRoleToSSO({ keycloak, role: rolesToUpdate[role] })(userEmail),
    { concurrency: concurrentSSORequests },
  )
}

const initialize = ({ db, keycloak }) => ({
  execute: async ({ rolesToUpdate }) => {
    const localRolesToBeUpdated = Object.keys(rolesToUpdate)

    await Promise.all(
      localRolesToBeUpdated.map(treatRole({ db, keycloak, rolesToUpdate })),
    )

    process.exit()
  },
})

const keycloak = { updateUserRole }

initialize({ db, keycloak }).execute({
  // add here whatever roles you need to update
  rolesToUpdate: {
    editorialAssistant: 'editorial_assistant',
    triageEditor: 'triage_editor',
    admin: 'admin',
    academicEditor: 'academic_editor',
  },
})
