const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const events = require('../../component-events')
const { groupBy } = require('lodash')
const { Promise } = require('bluebird')

const eventsService = events.initialize({ models })
const {
  useCases: { getUserWithWorkloadUseCase },
} = require('component-model')
const { createQueueService } = require('@hindawi/queue-service')

const { Manuscript, Team, TeamMember, User } = models

const execute = async () => {
  try {
    createQueueService()
      .then(queueService => {
        global.applicationEventBus = queueService
      })
      .catch(err => {
        logger.error(err)
        process.exit(1)
      })

    const manuscriptsWithWrongTriageEditor = await Manuscript.findAllWithWrongTriageEditor(
      {
        Team,
        TeamMember,
      },
    )

    const groupedManuscripts = groupBy(
      manuscriptsWithWrongTriageEditor,
      'submissionId',
    )

    const submissions = Object.values(groupedManuscripts)

    await Promise.each(submissions, async submission => {
      await removeWrongTriageEditor(submission)
      await assignTriageEditor(submission)
    })

    process.exit()
  } catch (err) {
    logger.error(err)
    throw new Error('Something went wrong.')
  }
}
execute()

async function removeWrongTriageEditor(submission) {
  await Promise.each(submission, async ({ id: manuscriptId }) => {
    const currentTriageEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.triageEditor,
        status: TeamMember.Statuses.active,
      },
    )
    currentTriageEditor.updateProperties({
      status: TeamMember.Statuses.removed,
    })
    await currentTriageEditor.save()

    logger.info(
      `Removed the Triage Editor with user id ${currentTriageEditor.userId} from manuscript with id ${manuscriptId}`,
    )
  })
}

async function assignTriageEditor(submission) {
  const getUserWithWorkload = await getUserWithWorkloadUseCase.initialize(
    models,
  )

  const userForAssignment = await getUserWithWorkload.execute({
    role: Team.Role.triageEditor,
    journalId: submission[0].journalId,
    sectionId: submission[0].sectionId,
    specialIssueId: submission[0].specialIssueId,
    teamMemberStatuses: [TeamMember.Statuses.active],
    manuscriptStatuses: [
      ...Manuscript.InProgressStatuses,
      Manuscript.Statuses.technicalChecks,
    ],
  })

  const user = await User.find(userForAssignment.userId, 'identities')
  await Promise.each(submission, async ({ id: manuscriptId }) => {
    const triageEditorTeam = await Team.findOneByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.triageEditor,
    })
    const triageEditorTeamMember = triageEditorTeam.addMember({
      user,
      options: { status: TeamMember.Statuses.active },
    })
    await triageEditorTeamMember.save()

    logger.info(
      `Assigned the Triage Editor with user id ${user.id} on manuscript with id ${manuscriptId}`,
    )
  })
  await eventsService.publishSubmissionEvent({
    submissionId: submission[0].submissionId,
    eventName: 'SubmissionTriageEditorReassigned',
  })
}
