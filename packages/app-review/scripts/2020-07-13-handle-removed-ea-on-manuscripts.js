const { groupBy } = require('lodash')
const { Promise } = require('bluebird')

const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const { createQueueService } = require('@hindawi/queue-service')

const events = require('component-events')
const {
  useCases: { getUserWithWorkloadUseCase },
} = require('component-model')

const getManuscriptsWithRemovedEA = ({ models }) => {
  const { Manuscript, Team, TeamMember } = models

  const manuscriptJournalIdColumnIdentifier = Manuscript.ref('m.journalId')
  const manuscriptPrimaryKeyColumnIdentifier = Manuscript.ref('m.id')

  const journalEA = query =>
    query
      .select('tm.id')
      .from('teamMember as tm')
      .join('team as t', 't.id', 'tm.teamId')
      .where('t.role', Team.Role.editorialAssistant)
      .andWhere('t.journalId', manuscriptJournalIdColumnIdentifier)

  const removedManuscriptEA = query =>
    query
      .select('tm.id')
      .from('teamMember as tm')
      .join('team as t', 't.id', 'tm.teamId')
      .where('tm.status', TeamMember.Statuses.removed)
      .andWhere('t.role', Team.Role.editorialAssistant)
      .andWhere('t.manuscriptId', manuscriptPrimaryKeyColumnIdentifier)

  const activeManuscriptEA = query =>
    query
      .select('tm.id')
      .from('teamMember as tm')
      .join('team as t', 't.id', 'tm.teamId')
      .where('tm.status', TeamMember.Statuses.active)
      .andWhere('t.role', Team.Role.editorialAssistant)
      .andWhere('t.manuscriptId', manuscriptPrimaryKeyColumnIdentifier)

  return Manuscript.query()
    .alias('m')
    .select('m.id', 'm.submissionId', 'm.journalId')
    .whereExists(journalEA)
    .andWhere(q => q.whereExists(removedManuscriptEA))
    .andWhere(q => q.whereNotExists(activeManuscriptEA))
}

const getNewEAByWorkload = async ({ journalId, models }) => {
  const { Manuscript, TeamMember, Team, User } = models

  const getUserWithWorkload = await getUserWithWorkloadUseCase.initialize(
    models,
  )

  const userForAssignment = await getUserWithWorkload.execute({
    role: Team.Role.editorialAssistant,
    journalId,
    manuscriptStatuses: [
      ...Manuscript.InProgressStatuses,
      Manuscript.Statuses.inQA,
      Manuscript.Statuses.technicalChecks,
      Manuscript.Statuses.qualityChecksRequested,
      Manuscript.Statuses.qualityChecksSubmitted,
    ],
    teamMemberStatuses: [TeamMember.Statuses.active],
  })

  return User.find(userForAssignment.userId, 'identities')
}

const assignEditorialAssistant = async ({
  user,
  models,
  logger,
  manuscript,
  eventsService,
}) => {
  const { TeamMember, Team } = models
  const { id: manuscriptId, submissionId } = manuscript

  const editorialAssistantTeam = await Team.findOneByManuscriptAndRole({
    manuscriptId,
    role: Team.Role.editorialAssistant,
  })

  editorialAssistantTeam.addMember({
    user,
    options: { status: TeamMember.Statuses.active },
  })
  await editorialAssistantTeam.saveGraph({
    relate: true,
    noUpdate: true,
  })
  logger.info(
    `Assigned Editorial Assistant with id ${user.id} on manuscript with id ${manuscriptId}`,
  )
  await eventsService.publishSubmissionEvent({
    submissionId,
    eventName: 'SubmissionEditorialAssistantAssigned',
  })
}

const execute = async () => {
  try {
    const eventsService = events.initialize({ models })
    await createQueueService()
      .then(queueService => {
        global.applicationEventBus = queueService
      })
      .catch(err => {
        logger.error(err)
        process.exit(1)
      })

    const manuscripts = await getManuscriptsWithRemovedEA({ models })
    const groupedManuscriptsList = Object.values(
      groupBy(manuscripts, 'submissionId'),
    )

    await Promise.each(groupedManuscriptsList, async groupedManuscripts => {
      const user = await getNewEAByWorkload({
        models,
        journalId: groupedManuscripts[0].journalId,
      })

      await Promise.each(groupedManuscripts, async manuscript => {
        await assignEditorialAssistant({
          user,
          models,
          logger,
          manuscript,
          eventsService,
        })
        return Promise.resolve()
      })
      return Promise.resolve()
    })

    process.exit()
  } catch (err) {
    logger.error(err)
    throw new Error('Something went wrong.')
  }
}

execute()
