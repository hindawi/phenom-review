const { chain } = require('lodash')
const Promise = require('bluebird')
const { logger } = require('component-logger')
const { transaction } = require('objection')
const models = require('@pubsweet/models')

const { Manuscript, Review } = models

const execute = async () => {
  const customId = process.argv[2]

  if (!customId) {
    logger.error('Please provide customId as argument')
    process.exit()
  }

  let logsToBeDeleted = []
  let reviewsToBeDeleted = []
  let manuscriptsToBeDeleted = []

  // If the author tried to submit a revision while multiple drafts existed,
  // a previous draft would become 'olderVersion'
  const lastNonDraftVersion = await Manuscript.query()
    .where('customId', customId)
    .andWhere(q =>
      q.whereNotIn('status', [
        Manuscript.Statuses.draft,
        Manuscript.Statuses.olderVersion,
      ]),
    )
    .orderBy('created', 'desc')
    .withGraphFetched('[reviews, logs]')
    .limit(1)
    .first()

  if (!lastNonDraftVersion) {
    logger.error('No manuscript found with this customId')
    process.exit()
  }

  // Revision reviews
  // Review comments are deleted on cascade
  const invalidRevisionReviews = chain(lastNonDraftVersion)
    .get('reviews')
    .filter(r => r.recommendation === Review.Recommendations.revision)
    .orderBy('created', 'asc')
    .tail() // Gets all but the first element of array.
    .value()
  reviewsToBeDeleted = reviewsToBeDeleted.concat(invalidRevisionReviews)

  // Activity logs
  const activityLogs = chain(lastNonDraftVersion)
    .get('logs')
    .filter(l => l.action === 'revision_requested')
    .orderBy('created', 'asc')
    .tail() // Gets all but the first element of array.
    .value()
  logsToBeDeleted = logsToBeDeleted.concat(activityLogs)

  // Draft manuscripts
  // Reviews and review comments are deleted on cascade
  // Teams and Team-Members are deleted on cascade
  // Files are deleted on cascade
  const draftVersions = await Manuscript.query()
    .where('customId', customId)
    .andWhere('status', Manuscript.Statuses.draft)

  if (!draftVersions.length) {
    logger.error('Submission does not have drafts')
    process.exit()
  }

  // If the author tried to submit a revision while multiple drafts existed,
  // a previous draft would become 'olderVersion'
  const olderVersionDrafts = await Manuscript.query()
    .where('customId', customId)
    .andWhere('status', Manuscript.Statuses.olderVersion)
    .andWhere('version', draftVersions[0].version)

  let invalidDraftManuscripts = []
  if (draftVersions.length > 1) {
    invalidDraftManuscripts = chain(draftVersions)
      .orderBy('created', 'asc')
      .tail() // Gets all but the first element of array.
      .value()
  }
  manuscriptsToBeDeleted = manuscriptsToBeDeleted.concat(
    invalidDraftManuscripts,
    olderVersionDrafts,
  )

  await transaction(Manuscript.knex(), async trx => {
    await Promise.each(logsToBeDeleted, log => log.$query(trx).delete())
    await Promise.each(reviewsToBeDeleted, rev => rev.$query(trx).delete())
    await Promise.each(manuscriptsToBeDeleted, man => man.$query(trx).delete())
  })

  logger.info(`Deleted the following logs: ${logsToBeDeleted.map(l => l.id)}`)
  logger.info(
    `Deleted the following reviews: ${reviewsToBeDeleted.map(d => d.id)}`,
  )
  logger.info(
    `Deleted the following drafts: ${manuscriptsToBeDeleted.map(d => d.id)}`,
  )

  process.exit()
}

execute()
