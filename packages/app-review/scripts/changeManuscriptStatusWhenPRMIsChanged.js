const models = require('@pubsweet/models')
const { Promise } = require('bluebird')
const { logger } = require('component-logger')

const { Manuscript } = models

const execute = async () => {
  const manuscripts = await Manuscript.findAllByStatusFromSpecialIssue({
    status: Manuscript.Statuses.reviewCompleted,
  })

  if (!manuscripts.length)
    return logger.info(`There are no manuscripts left to update`)

  await Promise.each(manuscripts, async manuscript => {
    manuscript.updateProperties({ status: Manuscript.Statuses.makeDecision })
    await manuscript.save()
    logger.info(`Updated status for manuscript ${manuscript.id}`)
  })
}
execute()
