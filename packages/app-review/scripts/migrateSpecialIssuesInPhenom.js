const knex = require('knex')({
  client: 'pg',
  connection: {
    port: 5432,
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    password: process.env.DB_PASS,
    database: process.env.DATABASE,
  },
})
// Must be installed as global dependencies on Pod (one time only)
const moment = require('moment')
const cheerio = require('cheerio')
const { S3Client, GetObjectCommand } = require('@aws-sdk/client-s3')

async function execute() {
  let page = 0
  const pageSize = process.env.PAGE_SIZE_SI_MIGRATION

  const mtsSpecialIssues = await getMtsSpecialIssuesFromS3()

  while (true) {
    const phenomSpecialIssues = await knex
      .select('id', ' custom_id')
      .from('special_issue')
      .limit(pageSize)
      .offset(page * pageSize)
      .orderBy('created', 'asc')
    if (phenomSpecialIssues.length === 0) {
      break
    }
    const specialIssuesToUpdate = phenomSpecialIssues
      .filter(specialIssue =>
        mtsSpecialIssues.hasOwnProperty(String(specialIssue.custom_id)),
      )
      .map(si => {
        const mtsSpecialIssue = mtsSpecialIssues[String(si.custom_id)]
        const specialIssue = {}

        // description: string[]
        if (mtsSpecialIssue.Descriptions) {
          const descriptionList = convertHtmlToArray({
            html: mtsSpecialIssue.Descriptions,
            selector: 'data-issue-description-item=yes',
          })
          specialIssue.description = descriptionList
        }

        // topics: string[]
        if (mtsSpecialIssue.Topics) {
          const topicList = convertHtmlToArray({
            html: mtsSpecialIssue.Topics,
            selector: 'data-issue-topic=yes',
          })
          specialIssue.topics = topicList
        }

        // call_for_papers: string
        let callForPapers = ''
        if (mtsSpecialIssue.Descriptions) {
          const descriptionTextList = convertHtmlToArray({
            html: mtsSpecialIssue.Descriptions,
            selector: 'data-issue-description-item=yes',
          })
          if (descriptionTextList.length > 0) {
            callForPapers += `${descriptionTextList.join('\n\n')}\n\n`
          }
        }
        if (mtsSpecialIssue.Topics) {
          const topicTextList = convertHtmlToArray({
            html: mtsSpecialIssue.Topics,
            selector: 'data-issue-topic=yes',
          })
          callForPapers += (topicTextList || []).join('\n')
        }
        if (callForPapers.length > 0) {
          specialIssue.call_for_papers = callForPapers
        }

        // end_date: timespamp with time zone
        if (mtsSpecialIssue.SubmissionEndDate) {
          const endDate = moment(
            mtsSpecialIssue.SubmissionEndDate,
            'YYYY-MM-DD[T]HH:mm:ss.SSS',
          ).format('YYYY-MM-DD HH:mm:ss.SSS')
          specialIssue.end_date = endDate
        }

        // start_date: timestamp with time zone
        if (mtsSpecialIssue.SubmissionStartDate) {
          const startDate = moment(
            mtsSpecialIssue.SubmissionStartDate,
            'YYYY-MM-DD[T]HH:mm:ss.SSS',
          ).format('YYYY-MM-DD HH:mm:ss.SSS')
          specialIssue.start_date = startDate
        }

        // publication_date: timestamp with time zone
        if (mtsSpecialIssue.PublicationDate) {
          const publicationDate = moment(
            mtsSpecialIssue.PublicationDate,
            'MMM D YYYY h:mm a',
          ).format('YYYY-MM-DD HH:mm:ss.SSS')
          specialIssue.publication_date = publicationDate
        }

        // name: string
        specialIssue.name = mtsSpecialIssue.Title

        // acronym: string
        specialIssue.acronym = mtsSpecialIssue.Acronym

        // is_cancelled: boolean
        if (
          mtsSpecialIssue.IsCancelled &&
          !isNaN(Number(mtsSpecialIssue.IsCancelled))
        ) {
          specialIssue.is_cancelled = Number(mtsSpecialIssue.IsCancelled) === 1
        }
        specialIssue.id = si.id
        return specialIssue
      })

    // Update Phenom Special Issues
    const updates = specialIssuesToUpdate.map(({ id, ...data }) =>
      knex('special_issue')
        .update(data)
        .where({ id }),
    )

    await Promise.all(updates).catch(e => {
      console.error(e)
    })
    page++
  }
}
execute()
  .then(() => {
    process.exit(0)
  })
  .catch(e => {
    console.error(e)
    process.exit(1)
  })

function convertHtmlToArray({ html, selector }) {
  const items = []
  const $ = cheerio.load(html)
  $(`div[${selector}]`).map(function() {
    items.push($(this).text())
  })
  return items
}

async function getMtsSpecialIssuesFromS3() {
  const s3Client = new S3Client()

  const command = new GetObjectCommand({
    Key: 'mtsSpecialIssuesDict.json',
    Bucket: process.env.MTS_DATA_S3_BUCKET,
  })
  const response = await s3Client.send(command)
  const data = await response.Body.transformToString()
  return JSON.parse(data)
}
