const models = require('@pubsweet/models')
const { Promise } = require('bluebird')
const { logger } = require('component-logger')

const { TeamMember, Team } = models

const execute = async () => {
  const teamMembers = await TeamMember.findAllByRoleAndStatusWhereRespondedNull(
    {
      role: Team.Role.academicEditor,
      status: TeamMember.Statuses.accepted,
    },
  )
  await Promise.each(teamMembers, async teamMember => {
    teamMember.updateProperties({ responded: teamMember.created })
    await teamMember.save()

    logger.info(`Updated TeamMember with id ${teamMember.id}`)
  })

  process.exit()
}
execute()
