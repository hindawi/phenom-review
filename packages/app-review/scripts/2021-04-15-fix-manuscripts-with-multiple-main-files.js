const models = require('@pubsweet/models')
const { db } = require('@pubsweet/db-manager')
const { logger } = require('component-logger')
const { Promise } = require('bluebird')
const { groupBy } = require('lodash')

const s3Service = require('../../component-files/server/src/services/s3Service')

const { File, Manuscript } = models

const statusesToExclude = [
  Manuscript.Statuses.rejected,
  Manuscript.Statuses.published,
  Manuscript.Statuses.deleted,
  Manuscript.Statuses.withdrawn,
  Manuscript.Statuses.accepted,
  Manuscript.Statuses.void,
  Manuscript.Statuses.refusedToConsider,
  Manuscript.Statuses.olderVersion,
]
const getSubmissionWithMultipleMainManuscripts = () =>
  db
    .select(db.raw('DISTINCT ON (submission_id) submission_id'))
    .from(query =>
      query
        .select('m.submission_id')
        .count('f.id as manuscriptFiles')
        .from('manuscript as m')
        .join('file as f', 'f.manuscriptId', 'm.id')
        .whereNotIn('m.status', statusesToExclude)
        .andWhere('f.type', File.Types.manuscript)
        .groupBy('m.id')
        .as('multipleMainFiles'),
    )
    .where('manuscriptFiles', '>', 1)

const execute = async () => {
  try {
    const submissionsWithMultipleMainFiles = await getSubmissionWithMultipleMainManuscripts()

    await Promise.each(
      submissionsWithMultipleMainFiles,
      async ({ submissionId }) => {
        const manuscriptsInSubmission = await Manuscript.findAll({
          queryObject: {
            submissionId,
          },
        })
        await removeDuplicatedManuscriptFiles(manuscriptsInSubmission)
      },
    )
  } catch (err) {
    console.log(err)
  }

  process.exit()
}

execute()

const removeDuplicatedManuscriptFiles = async manuscriptsInSubmission => {
  await Promise.each(manuscriptsInSubmission, async ({ id: manuscriptId }) => {
    const manuscriptFiles = await File.findAllByManuscriptAndType({
      manuscriptId,
      type: File.Types.manuscript,
    })
    const groupedFilesList = Object.values(
      groupBy(manuscriptFiles, 'originalName'),
    )

    await Promise.each(groupedFilesList, async groupedFiles => {
      if (groupedFiles.length > 1) {
        const differentThanOriginalName = groupedFiles.filter(
          gf => gf.fileName !== gf.originalName,
        )
        // there are cases when no fileName is the same with the originalName,
        // in order to control this, the script will skip and we will check manually.
        if (groupedFiles.length === differentThanOriginalName.length) {
          logger.info(`The manuscript with id: ${manuscriptId} was not handled`)
        }
        await Promise.each(differentThanOriginalName, async fileToDelete => {
          await s3Service.delete(fileToDelete.providerKey)
          await fileToDelete.delete()
        })
      } else {
        // there are cases where the original name is not the same,
        // this means we don't know what file to delete and will check this manually.
        logger.info(
          `The file manuscript with id: ${manuscriptId} was not handled`,
        )
      }
    })
  })
}
