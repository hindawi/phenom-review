event=$(cat <<EOF
{
  "event": "pen:hindawi:hindawi-review:PeerReviewCycleCheckingProcessSentToPeerReview",
  "data": {
    "submissionId": "dfba1a33-c1a7-413c-a41b-d4018bb3086e",
    "checks": {
        "peerReviewCycle": {
            "conflictOfInterest": [],
            "decisionMadeInError": true,
            "improperReview": {
                "reviewers": []
            }
        }
    }
  }
}
EOF
)

aws sns publish --topic-arn arn:aws:sns:us-east-1:1465414804035:test1 --endpoint-url http://localhost:9911 --message "$event"
