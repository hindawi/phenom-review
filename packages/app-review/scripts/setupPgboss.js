const { db } = require('@pubsweet/db-manager')

const execute = async () => {
  await db.raw(`
    CREATE EXTENSION IF NOT EXISTS pgcrypto;
    GRANT CREATE ON DATABASE ${process.env.DATABASE} TO ${process.env.DB_USER};
  `)
}

execute()
