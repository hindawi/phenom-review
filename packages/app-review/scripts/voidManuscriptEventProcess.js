const config = require('config')
const { S3, SQS } = require('aws-sdk')
const { logger } = require('component-logger')
const { S3EventProducer, SqsPublishConsumer } = require('@hindawi/eve')

const s3Config = config.get('pubsweet-component-aws-s3')
const sqsConfig = config.get('pubsweet-component-aws-sqs')

const s3 = new S3({
  accessKeyId: s3Config.accessKeyId,
  secretAccessKey: s3Config.secretAccessKey,
})

const sqs = new SQS({
  secretAccessKey: sqsConfig.secretAccessKey,
  accessKeyId: sqsConfig.accessKeyId,
  endpoint: sqsConfig.endpoint,
  region: sqsConfig.region,
})

const execute = async () => {
  // Reads the events from the eventStorage source obtained from the environmental variables
  const s3EventProducer = new S3EventProducer(s3, s3Config.eventStorage)
  // Writes the events found into the SQS queue that can be specified in the environmental variables
  const sqsEventConsumer = new SqsPublishConsumer(sqs, sqsConfig.queueName)

  // eslint-disable-next-line no-restricted-syntax
  for await (const events of s3EventProducer.produce()) {
    // eslint-disable-next-line no-restricted-syntax
    for (const event of events) {
      let eventBody

      try {
        eventBody = JSON.parse(event.Message)
        logger.info(`Parsing event: ${eventBody.event}\n`)
      } catch (e) {
        logger.warn(`Can't parse event ${JSON.stringify(event)}\n${e}`)
      }
      // Verification to consume the found events
      if (
        eventBody &&
        eventBody.event &&
        ['SubmissionScreeningVoid'].includes(eventBody.event.split(':').pop())
      ) {
        // eslint-disable-next-line no-await-in-loop
        await sqsEventConsumer.consume(event)
      }
    }
  }
}

execute()
