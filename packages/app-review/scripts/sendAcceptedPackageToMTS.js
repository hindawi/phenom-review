const models = require('@pubsweet/models')
const { logger } = require('component-logger')
const { createQueueService } = require('@hindawi/queue-service')
const { useCases, eventMapper } = require('component-mts-production')
const { Manuscript, Team } = models
const BluebirdPromise = require('bluebird')

const execute = async () => {
  const submissionId = process.argv[2]

  if (!submissionId) {
    logger.error('Please provide submissionId as argument')
    process.exit()
  }

  await createQueueService()
    .then(queueService => {
      global.applicationEventBus = queueService
    })
    .catch(err => {
      logger.error(err)
      process.exit(1)
    })

  const manuscripts = await Manuscript.findManuscriptsBySubmissionId({
    submissionId,
    eagerLoadRelations:
      '[journal.journalPreprints.preprint, articleType, section, specialIssue, files]',
  })
  await BluebirdPromise.each(manuscripts, async manuscript => {
    const { id: manuscriptId } = manuscript
    if (!manuscript)
      throw new Error(`Manuscript with id ${manuscriptId} not found`)

    const {
      authors,
      editorialAssistant,
      correspondingEditorialAssistant,
    } = await useCases.getDataForSubmittedManuscriptUseCase
      .initialize(models)
      .execute({
        manuscriptId,
        journalId: manuscript.journalId,
      })

    const {
      academicEditor,
      reviews,
    } = await useCases.getDataForPeerReviewedManuscriptUseCase
      .initialize(models)
      .execute(manuscriptId)

    try {
      const finalRevisionDate = await Manuscript.getFinalRevisionDate({
        submissionId: manuscript.submissionId,
      })

      const mtsEventData = eventMapper.createMTSAcceptedEventData({
        authors,
        reviews,
        screeners: [],
        manuscript,
        academicEditor,
        finalRevisionDate,
        editorialAssistant,
        correspondingEditorialAssistant,
      })

      await applicationEventBus.publishMessage({
        event: 'MTSAcceptedArchiveRequested',
        data: mtsEventData,
      })
    } catch (e) {
      logger.warn(
        `Error creating MTSAcceptedArchiveRequested event. Cause: ${e.message}`,
      )
    }
  })
  process.exit(0)
}
execute()
