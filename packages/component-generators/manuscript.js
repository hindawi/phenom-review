const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generateManuscript(props) {
    return {
      id: chance.guid(),
      status: statuses.draft,
      updateProperties: jest.fn(),
      updateStatus: jest.fn(),
      updateIsLatestVersionFlag: jest.fn(),
      getEditorLabel: jest.fn(),
      save: jest.fn(),
      submissionId: chance.guid(),
      isLatestVersion: false,
      version: 1,
      ...props,
    }
  },
  getManuscriptStatuses() {
    return statuses
  },
  getManuscriptInProgressStatuses() {
    return [
      statuses.submitted,
      statuses.underReview,
      statuses.makeDecision,
      statuses.reviewCompleted,
      statuses.pendingApproval,
      statuses.reviewersInvited,
      statuses.revisionRequested,
      statuses.academicEditorInvited,
      statuses.academicEditorAssigned,
    ]
  },
  getManuscriptNonActionableStatuses() {
    return [
      statuses.void,
      statuses.draft,
      statuses.deleted,
      statuses.rejected,
      statuses.inQA,
      statuses.accepted,
      statuses.qualityChecksRequested,
      statuses.qualityChecksSubmitted,
      statuses.published,
      statuses.withdrawn,
      statuses.olderVersion,
      statuses.refusedToConsider,
    ]
  },
}

const statuses = {
  draft: 'draft',
  technicalChecks: 'technicalChecks',
  submitted: 'submitted',
  academicEditorInvited: 'academicEditorInvited',
  academicEditorAssigned: 'academicEditorAssigned',
  reviewersInvited: 'reviewersInvited',
  underReview: 'underReview',
  reviewCompleted: 'reviewCompleted',
  revisionRequested: 'revisionRequested',
  pendingApproval: 'pendingApproval',
  rejected: 'rejected',
  inQA: 'inQA',
  accepted: 'accepted',
  withdrawn: 'withdrawn',
  deleted: 'deleted',
  published: 'published',
  olderVersion: 'olderVersion',
  academicEditorAssignedEditorialType: 'academicEditorAssignedEditorialType',
  makeDecision: 'makeDecision',
  qualityChecksRequested: 'qualityChecksRequested',
  qualityChecksSubmitted: 'qualityChecksSubmitted',
  refusedToConsider: 'refusedToConsider',
  void: 'void',
}
