const { generateJob, getJobTypes } = require('./job')
const { generateUser } = require('./user')
const {
  generateFileTypes,
  generateFile,
  generateFileScanStatuses,
} = require('./file')
const { generateSection } = require('./section')
const { generateJournal } = require('./journal')
const { generateIdentity } = require('./identity')
const { generateAuditLog } = require('./auditLog')
const { generateSubmission } = require('./submission')
const {
  generateArticleType,
  getArticleTypesTypes,
  getArticleTypesEditorialTypes,
  getShortArticleTypes,
  getTypesWithPeerReview,
} = require('./articleType')
const { generateJournalArticleType } = require('./journalArticleType')
const { generateTeam, getTeamRoles } = require('./team')
const { generateSpecialIssue } = require('./specialIssue')
const { generatePeerReviewModel } = require('./peerReviewModel')
const { generateReview, getRecommendations } = require('./review')
const { generateComment, getCommentTypes } = require('./comment')
const { generateReviewerSuggestion } = require('./reviewerSuggestion')
const {
  generateTeamMember,
  getTeamMemberStatuses,
  getStatusExpiredLabels,
} = require('./teamMember')
const {
  generateManuscript,
  getManuscriptStatuses,
  getManuscriptInProgressStatuses,
  getManuscriptNonActionableStatuses,
} = require('./manuscript')

module.exports = {
  generateJob,
  getJobTypes,
  generateUser,
  getTeamRoles,
  generateTeam,
  generateFile,
  generateReview,
  generateJournal,
  generateSection,
  generateComment,
  getCommentTypes,
  generateIdentity,
  generateAuditLog,
  generateFileTypes,
  generateSubmission,
  getRecommendations,
  generateTeamMember,
  generateManuscript,
  generateArticleType,
  getShortArticleTypes,
  getArticleTypesTypes,
  generateSpecialIssue,
  getManuscriptStatuses,
  getTeamMemberStatuses,
  getStatusExpiredLabels,
  getTypesWithPeerReview,
  generatePeerReviewModel,
  generateFileScanStatuses,
  generateJournalArticleType,
  generateReviewerSuggestion,
  getArticleTypesEditorialTypes,
  getManuscriptInProgressStatuses,
  getManuscriptNonActionableStatuses,
}
