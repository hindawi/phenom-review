const Chance = require('chance')

const chance = new Chance()

module.exports = {
  generateFile(props) {
    return {
      id: chance.guid(),
      created: chance.date(),
      fileName: chance.name(),
      originalName: chance.name(),
      save: jest.fn(),
      ...props,
    }
  },
  generateFileTypes() {
    return {
      figure: 'figure',
      manuscript: 'manuscript',
      coverLetter: 'coverLetter',
      supplementary: 'supplementary',
      reviewComment: 'reviewComment',
      responseToReviewers: 'responseToReviewers',
    }
  },
  generateFileScanStatuses() {
    return {
      SKIPPED: 'skipped',
      SCANNING: 'scanning',
      HEALTHY: 'healthy',
      INFECTED: 'infected',
      ERROR: 'error',
    }
  },
}
