const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generateJournalArticleType(props) {
    return {
      id: chance.guid(),
      journalId: chance.guid(),
      articleTypeId: chance.guid(),
      save: jest.fn(),
      updateProperties: jest.fn(),
      delete: jest.fn(),
      ...props,
    }
  },
}
