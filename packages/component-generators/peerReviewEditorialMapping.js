const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generatePeerReviewEditorialMapping(props) {
    return {
      id: chance.guid(),
      peerReviewEditorialModel: {
        name: 'Chief Minus',
        triageEditor: { label: 'Chief Editor', enabled: true },
        academicEditor: {
          label: 'Academic Editor',
          labelInCaseOfConflict: 'Academic Editor',
          autoInviteInCaseOfConflict: false,
        },
        approvalEditor: 'triageEditor',
        figureHeadEditor: { label: null, enabled: false },
        hasEditorialAssistant: true,
        minNoOfReviewerReports: 1,
      },
      ...props,
    }
  },
}
