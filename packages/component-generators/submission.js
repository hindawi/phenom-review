const Chance = require('chance')

const chance = new Chance()
module.exports = {
  generateSubmission({
    props,
    noOfVersions = 1,
    generateManuscript,
    lastVersionStatus = 'inQA',
    statuses = { olderVersion: 'olderVersion' },
  }) {
    const submissionId = chance.guid()
    const submission = []
    for (let i = 0; i < noOfVersions; i += 1) {
      const manuscript = generateManuscript({
        submissionId,
        version: i + 1,
        status: statuses.olderVersion,
        ...props,
      })

      submission.push(manuscript)
    }

    submission[noOfVersions - 1].status = lastVersionStatus
    submission[noOfVersions - 1].isLatestVersion = true

    return submission
  },
}
