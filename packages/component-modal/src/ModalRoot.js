import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

const modalPosition = () => css`
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  position: fixed;
`

const ModalRoot = styled.div`
  align-items: center;
  display: flex;
  color: ${th('colorText')};
  background-color: ${({ overlayColor }) =>
    overlayColor || 'rgba(0, 0, 0, 0.8)'};
  justify-content: center;
  z-index: ${th('zIndex.modal')};

  font-family: ${th('fontInterface')}, sans-serif;
  font-size: ${th('fontSizeBase')};
  line-height: ${th('lineHeightBase')};

  ${modalPosition};

  * {
    box-sizing: border-box;
  }
`

export default ModalRoot
