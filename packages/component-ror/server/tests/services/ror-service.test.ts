/* eslint-disable sonarjs/no-duplicate-string */
import axios from 'axios'
import { rorService } from '../../src/services/ror-service'

jest.mock('axios')
const mockedAxios = axios as jest.Mocked<typeof axios>

describe('ror-service', () => {
  const data = {
    items: [
      {
        score: 1,
        chosen: true,
        organization: {
          id: 'https://ror.org/034t30j35',
          name: 'Chinese Academy of Sciences',
          addresses: [{ city: 'Beijing' }],
          country: { country_name: 'China', country_code: 'CN' },
        },
      },
      {
        score: 0.89,
        chosen: false,
        organization: {
          id: 'https://ror.org/05bxbmy32',
          name: 'Chinese Academy of Social Sciences',
          addresses: [{ city: 'Beijing' }],
          country: { country_name: 'China', country_code: 'CN' },
        },
      },
    ],
  }
  describe('getRORResults', () => {
    it('should return [] when affiliation is empty', async () => {
      const rorResult = await rorService.getRORResults('')
      expect(rorResult).toEqual([])
    })

    it('should return proper ROR results when axios call succeeds', async () => {
      mockedAxios.get.mockResolvedValue({ data })
      const rorResult = await rorService.getRORResults('aff')

      expect(rorResult).toEqual([
        {
          score: 1,
          chosen: true,
          organization: {
            addresses: [{ city: 'Beijing' }],
            country: { code: 'CN', name: 'China' },
            id: '034t30j35',
            name: 'Chinese Academy of Sciences',
          },
        },
        {
          score: 0.89,
          chosen: false,
          organization: {
            addresses: [{ city: 'Beijing' }],
            country: { code: 'CN', name: 'China' },
            id: '05bxbmy32',
            name: 'Chinese Academy of Social Sciences',
          },
        },
      ])
    })
    it('should return proper ROR results when axios call fails', async () => {
      mockedAxios.get.mockRejectedValue('Network error: Something went wrong')
      const rorResult = await rorService.getRORResults('aff')

      expect(rorResult).toEqual([])
    })
  })
  describe('getBestROR', () => {
    it('should return [] when affiliation is empty', async () => {
      const rorResult = await rorService.getBestROR('', 'RO')
      expect(rorResult).toEqual(undefined)
    })
    it('should return best ROR result when axios call succeeds', async () => {
      mockedAxios.get.mockResolvedValue({ data })
      const rorResult = await rorService.getBestROR('aff', 'country')
      expect(rorResult).toEqual({
        score: 1,
        chosen: true,
        organization: {
          addresses: [{ city: 'Beijing' }],
          country: { code: 'CN', name: 'China' },
          id: '034t30j35',
          name: 'Chinese Academy of Sciences',
        },
      })
    })
    it('should return undefined when axios call fails', async () => {
      mockedAxios.get.mockRejectedValue('Network error: Something went wrong')
      const rorResult = await rorService.getBestROR('aff', 'country')
      expect(rorResult).toEqual(undefined)
    })
    it('should return the correct affiliation based on country', async () => {
      const rorAff = {
        score: 0.9,
        chosen: true,
        organization: {
          addresses: [{ city: 'London' }],
          country: { code: 'GB', name: 'United Kingdom' },
          id: '02jx3x895',
          name: 'University College London',
        },
      }
      const data = {
        items: [
          {
            score: 0.89,
            chosen: false,
            organization: {
              addresses: [{ city: 'Beijing' }],
              country: { country_code: 'CN', country_name: 'China' },
              id: '05bxbmy32',
              name: 'Chinese Academy of Social Sciences',
            },
          },
          {
            score: 0.9,
            chosen: true,
            organization: {
              addresses: [{ city: 'London' }],
              country: { country_code: 'GB', country_name: 'United Kingdom' },
              id: '02jx3x895',
              name: 'University College London',
            },
          },
        ],
      }
      mockedAxios.get.mockResolvedValue({ data })
      const rorResult = await rorService.getBestROR('UCL', 'UK')
      expect(rorResult).toEqual(rorAff)
    })
  })
})
