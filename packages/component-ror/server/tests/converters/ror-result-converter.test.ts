import { convertToRORResult } from '../../src/converters/ror-result-converter'

describe('ror-result-converter', () => {
  describe('convertToRORResult', () => {
    it('should properly convert valid ror data', () => {
      const rorData = {
        score: 1,
        chosen: true,
        organization: {
          id: 'https://ror.org/034t30j35',
          name: 'Chinese Academy of Sciences',
          addresses: [{ city: 'Beijing' }],
          country: { country_name: 'China', country_code: 'CN' },
        },
      }

      const rorResult = convertToRORResult(rorData)
      expect(rorResult).toEqual({
        score: 1,
        chosen: true,
        organization: {
          addresses: [{ city: 'Beijing' }],
          country: { code: 'CN', name: 'China' },
          id: '034t30j35',
          name: 'Chinese Academy of Sciences',
        },
      })
    })
    it('should properly convert invalid ror data', () => {
      const rorData = undefined
      const rorResult = convertToRORResult(rorData)
      expect(rorResult).toEqual({
        score: null,
        chosen: false,
        organization: {
          addresses: [],
          country: null,
          id: null,
          name: null,
        },
      })
    })
  })
})
