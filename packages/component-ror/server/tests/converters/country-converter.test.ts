import { convertCountryCode } from '../../src/converters/country-converter'

describe('country-code-converter', () => {
  it('should convert UK to GB', () => {
    const countryCode = 'UK'
    const result = convertCountryCode(countryCode)

    expect(result).toEqual('GB')
  })
  it('should convert SRB to RS', () => {
    const countryCode = 'SRB'
    const result = convertCountryCode(countryCode)

    expect(result).toEqual('RS')
  })
  it('should convert MNT to ME', () => {
    const countryCode = 'MNT'
    const result = convertCountryCode(countryCode)

    expect(result).toEqual('ME')
  })
  it('should keep RO the same', () => {
    const countryCode = 'RO'
    const result = convertCountryCode(countryCode)

    expect(result).toEqual('RO')
  })
})
