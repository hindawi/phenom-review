import axios from 'axios'
import { logger } from 'component-logger'

import { RORResult, RORService } from '../types'
import { convertToRORResult } from '../converters/ror-result-converter'
import { getBestMatchROR } from './ror-match-service'

const ROR_API_ENDPOINT = 'https://api.ror.org/organizations'

class RORServiceImpl implements RORService {
  getRORById = async (rorId: string): Promise<RORResult | null> => {
    if (!rorId) {
      return null
    }
    return axios
      .get(`${ROR_API_ENDPOINT}/${rorId}`, { timeout: 5000 })
      .then(response => convertToRORResult({ organization: response.data }))
      .catch(error => {
        logger.info(`Error getting ROR result for id: ${rorId}`, error)
        return null
      })
  }

  getRORResults = async (affiliation: string): Promise<RORResult[]> => {
    if (!affiliation) {
      return []
    }

    return axios
      .get(`${ROR_API_ENDPOINT}?affiliation=${encodeURI(affiliation)}`)
      .then(response => response.data.items.map(convertToRORResult))
      .catch(error => {
        logger.error(
          `Error getting ROR results for affiliation: ${affiliation}`,
          error,
        )
        return []
      })
  }

  getBestROR = async (
    affiliation: string,
    country: string,
  ): Promise<RORResult | undefined> =>
    this.getRORResults(affiliation)
      .then(results => getBestMatchROR(affiliation, country, results))
      .catch(error => {
        logger.error(
          `Error getting best match ROR for affiliation: ${affiliation} and country: ${country}`,
          error,
        )
        return undefined
      })
}

export const rorService = new RORServiceImpl() as RORService
