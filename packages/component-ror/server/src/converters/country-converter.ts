const countryLookupTable = {
  UK: 'GB',
  SRB: 'RS',
  MNT: 'ME',
}

export const convertCountryCode = (countryCode: string): string =>
  countryLookupTable[countryCode] || countryCode
