import { RORResult, Address, Country } from '../types'

const getRORScore = (rorData: any): number => {
  const score = rorData?.score
  return score !== undefined ? score : null
}

const getRORChosen = (rorData: any): boolean => rorData?.chosen || false

const getRORId = (rorData: any): string => {
  let rorId
  const id = rorData?.organization?.id

  if (id) {
    const parts = id.split('/')
    rorId = parts[parts.length - 1]
  }

  return rorId || null
}

const getROROrganizationName = (rorData: any): string => {
  const name = rorData?.organization?.name

  return name !== undefined ? name : null
}

const getROROrganizationCountry = (rorData: any): Country => {
  const country = rorData?.organization?.country

  return !country
    ? null
    : {
        name: country.country_name,
        code: country.country_code,
      }
}

const getROROrganizationAddresses = (rorData: any): Address[] => {
  const addresses = rorData?.organization?.addresses || []

  return addresses.map(address => ({ city: address.city }))
}

export const convertToRORResult = (rorData: any): RORResult => ({
  score: getRORScore(rorData),
  chosen: getRORChosen(rorData),
  organization: {
    id: getRORId(rorData),
    name: getROROrganizationName(rorData),
    country: getROROrganizationCountry(rorData),
    addresses: getROROrganizationAddresses(rorData),
  },
})
