import gql from 'graphql-tag'

export const updateUser = gql`
  mutation updateUser($input: EditProfileInput) {
    updateUser(input: $input)
  }
`
export const subscribeToEmails = gql`
  mutation subscribeToEmails($input: SubscribeInput) {
    subscribeToEmails(input: $input)
  }
`
export const unsubscribeFromEmails = gql`
  mutation subscribeToEmails($input: UnsubscribeInput) {
    unsubscribeFromEmails(input: $input)
  }
`
export const changePassword = gql`
  mutation changePassword($input: ChangePasswordInput!) {
    changePassword(input: $input) {
      token
    }
  }
`
export const unlinkOrcid = gql`
  mutation unlinkOrcid {
    unlinkOrcid
  }
`
