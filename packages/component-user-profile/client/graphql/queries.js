import gql from 'graphql-tag'
import { userDetailsWithIdentities } from './fragments'

export const currentUser = gql`
  query {
    currentUser {
      ...userDetailsWithIdentities
      isSubscribedToEmails
      unsubscribeToken
    }
  }
  ${userDetailsWithIdentities}
`
export const getPersonTaxonomyTermsCount = gql`
  query getPersonTaxonomyTermsCount {
    personTaxonomyTermsCount: getPersonTaxonomyTermsCount
  }
`
