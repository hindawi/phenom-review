import { useEffect } from 'react'
import gql from 'graphql-tag'
import { get } from 'lodash'
import { useLazyQuery } from 'react-apollo'
import { Term } from '../hooks/Term'

const termFragment = gql`
  fragment termFragment on Term {
    id
    parentId
    name
    path
    depth
    isLeaf
    isAssigned
  }
`

const termRecursive = gql`
  fragment termRecursive on Term {
    children {
      ...termFragment
      children {
        ...termFragment
        children {
          ...termFragment
          children {
            ...termFragment
            children {
              ...termFragment
              children {
                ...termFragment
                children {
                  ...termFragment
                  children {
                    ...termFragment
                    children {
                      ...termFragment
                      children {
                        ...termFragment
                        children {
                          ...termFragment
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  ${termFragment}
`

const getSearchedTerms = gql`
  query getSearchedTerms($searchedValue: String!) {
    getSearchedTerms(searchedValue: $searchedValue) {
      ...termFragment
      ...termRecursive
    }
  }
  ${termRecursive}
`

export type QueryData = {
  terms: Term[]
}

export type QueryVariables = {
  searchedValue: string | null
}

export function sanitizeString(str: string): string {
  return str
    .replace(/[^a-z0-9\s-]/gi, '')
    .replace(/\s{2,}/g, ' ')
    .trim()
}

export function useSearchedTerms(searchedValue: string) {
  const [
    getSearchedTermsQuery,
    { data, loading: searchLoading, error },
  ] = useLazyQuery<QueryData, QueryVariables>(getSearchedTerms, {
    fetchPolicy: 'no-cache',
  })
  const terms = get(data, 'getSearchedTerms', [])
  useEffect(() => {
    if (searchedValue.length > 2) {
      getSearchedTermsQuery({
        variables: { searchedValue: sanitizeString(searchedValue) },
      })
    }
  }, [getSearchedTermsQuery, searchedValue])

  return [terms, searchLoading, error, getSearchedTermsQuery]
}
