import { useCallback, useEffect, useState } from 'react'

import { Term } from './Term'
import { useEditExpertise } from './useEditExpertise'

export type Expertise = {
  assignedTerms: Term[]
  unassignedTerms: Term[]
}

export type HookResult = {
  expertise: Expertise
  setExpertise: React.Dispatch<React.SetStateAction<Expertise>>
  currentlyAssignedTerms: Term[]
  setCurrentlyAssignedTerms: React.Dispatch<React.SetStateAction<Term[]>>
  assignTerm: (term: Term) => void
  unassignTerm: (term: Term) => void
  editExpertise: () => Promise<void>
  cannotEdit: boolean
  editingExpertise: boolean
  editExpertiseError: string | null
}

function isAlreadyAssigned(term: Term, currentlyAssignedTerms: Term[]) {
  return currentlyAssignedTerms.map(c => c.id).includes(term.id)
}

function excludeCurrentlyAssignedTerms(
  assignedTerms: Term[],
  currentlyAssignedTerms: Term[],
) {
  return assignedTerms.filter(
    assignedTerm => !isAlreadyAssigned(assignedTerm, currentlyAssignedTerms),
  )
}

function includeOnlyAssignedTerms(
  assignedTerms: Term[],
  currentlyAssignedTerms: Term[],
) {
  return assignedTerms.filter(assignedTerm =>
    isAlreadyAssigned(assignedTerm, currentlyAssignedTerms),
  )
}

function useCannotEdit({
  getAssignedTermsIds,
  getUnassignedTermsIds,
}: {
  getAssignedTermsIds: () => string[]
  getUnassignedTermsIds: () => string[]
}): [boolean, React.Dispatch<React.SetStateAction<boolean>>] {
  const isExpertisePristine = useCallback(() => {
    const assignedTermsIds = getAssignedTermsIds()
    const unassignedTermsIds = getUnassignedTermsIds()

    return assignedTermsIds.length === 0 && unassignedTermsIds.length === 0
  }, [getAssignedTermsIds, getUnassignedTermsIds])

  const [cannotEdit, setCannotEdit] = useState(true)

  useEffect(() => {
    setCannotEdit(isExpertisePristine())
  }, [isExpertisePristine])

  return [cannotEdit, setCannotEdit]
}

export function useExpertise(): HookResult {
  const [currentlyAssignedTerms, setCurrentlyAssignedTerms] = useState<Term[]>(
    [],
  )
  const {
    editExpertise,
    loading: editingExpertise,
    error: editExpertiseError,
  } = useEditExpertise()
  const [expertise, setExpertise] = useState<Expertise>({
    assignedTerms: [],
    unassignedTerms: [],
  })

  const assignTerm = (term: Term) => {
    setExpertise({
      assignedTerms: [...expertise.assignedTerms, term],
      unassignedTerms: expertise.unassignedTerms.filter(
        unassignedTerm => unassignedTerm.id !== term.id,
      ),
    })
  }
  const unassignTerm = (term: Term) => {
    setExpertise({
      assignedTerms: expertise.assignedTerms.filter(
        assignedTerm => assignedTerm.id !== term.id,
      ),
      unassignedTerms: [...expertise.unassignedTerms, term],
    })
  }
  const getAssignedTermsIds = useCallback(
    () =>
      excludeCurrentlyAssignedTerms(
        expertise.assignedTerms,
        currentlyAssignedTerms,
      ).map(term => term.id),
    [expertise, currentlyAssignedTerms],
  )

  const getUnassignedTermsIds = useCallback(
    () =>
      includeOnlyAssignedTerms(
        expertise.unassignedTerms,
        currentlyAssignedTerms,
      ).map(term => term.id),
    [expertise, currentlyAssignedTerms],
  )
  const [cannotEdit, setCannotEdit] = useCannotEdit({
    getAssignedTermsIds,
    getUnassignedTermsIds,
  })

  return {
    expertise,
    setExpertise,
    currentlyAssignedTerms,
    setCurrentlyAssignedTerms,
    assignTerm,
    unassignTerm,
    editingExpertise,
    editExpertiseError,
    cannotEdit,
    editExpertise: async (): Promise<void> => {
      await editExpertise(
        {
          assignedTermsIds: getAssignedTermsIds(),
          unassignedTermsIds: getUnassignedTermsIds(),
        },
        expertise.assignedTerms.length,
      )
      setCannotEdit(true)
    },
  }
}
