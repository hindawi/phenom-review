import { useCallback } from 'react'
import { useApolloClient } from 'react-apollo'
import gql from 'graphql-tag'

import { Term } from './Term'

export const getTermsByParentId = gql`
  query getTermsByParentId($parentId: ID) {
    terms: getTermsByParentId(parentId: $parentId) {
      id
      parentId
      name
      path
      depth
      isLeaf
      isAssigned
    }
  }
`

type QueryData = {
  terms: Term[]
}

type QueryVariables = {
  parentId: string | null
}

type HookResult = (id: string) => Promise<Term[]>

export function useTermsByParentId(): HookResult {
  const client = useApolloClient()

  return useCallback(
    async id => {
      const { data } = await client.query<QueryData, QueryVariables>({
        query: getTermsByParentId,
        variables: {
          parentId: id,
        },
        fetchPolicy: 'no-cache',
      })

      return data.terms
    },
    [client],
  )
}
