import { ApolloError } from 'apollo-client'
import { first } from 'lodash'

export function parseGQLError(error: ApolloError): string {
  const firstError = first(error.graphQLErrors)
  return firstError ? firstError.message : 'Something went wrong.'
}
