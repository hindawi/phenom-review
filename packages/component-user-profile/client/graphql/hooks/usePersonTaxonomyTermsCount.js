import { useQuery } from 'react-apollo'
import { get } from 'lodash'

import { getPersonTaxonomyTermsCount } from '../queries'

export function usePersonTaxonomyTermsCount() {
  const { data, loading, error } = useQuery(getPersonTaxonomyTermsCount)

  const personTaxonomyTermsCount = get(data, 'personTaxonomyTermsCount')

  return { personTaxonomyTermsCount, loading, error }
}
