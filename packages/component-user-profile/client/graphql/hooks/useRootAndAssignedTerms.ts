import { useQuery } from 'react-apollo'
import gql from 'graphql-tag'
import { get } from 'lodash'

import { parseGQLError } from './parse-gql-error'
import { Term } from './Term'

const getRootAndAssignedTerms = gql`
  query getTermsByParentId($parentId: ID) {
    rootTerms: getTermsByParentId(parentId: $parentId) {
      id
      parentId
      name
      path
      depth
      isLeaf
      isAssigned
    }
    assignedTerms: getPersonTaxonomyTerms {
      id
      parentId
      name
      path
      depth
      isLeaf
      isAssigned
    }
  }
`

type QueryData = {
  rootTerms: Term[]
  assignedTerms: Term[]
}

type QueryVariables = {
  parentId: string | null
}

export function useRootAndAssignedTerms(): {
  rootTerms: Term[]
  assignedTerms: Term[]
  loading: boolean
  error: string | null
} {
  const { data, loading, error } = useQuery<QueryData, QueryVariables>(
    getRootAndAssignedTerms,
    {
      variables: {
        parentId: null,
      },
      fetchPolicy: 'no-cache',
    },
  )
  const rootTerms = get(data, 'rootTerms', [] as Term[])
  const assignedTerms = get(data, 'assignedTerms', [] as Term[])

  return {
    rootTerms,
    assignedTerms,
    loading,
    error: error ? parseGQLError(error) : null,
  }
}
