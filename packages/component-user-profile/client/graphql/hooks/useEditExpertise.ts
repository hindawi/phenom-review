import { ExecutionResult, useMutation } from 'react-apollo'
import gql from 'graphql-tag'

import { getPersonTaxonomyTermsCount } from '../queries'
import { parseGQLError } from './parse-gql-error'

export const editExpertise = gql`
  mutation editExpertise($expertise: ExpertiseInput!) {
    editExpertise(expertise: $expertise)
  }
`

type ExpertiseInput = {
  assignedTermsIds: string[]
  unassignedTermsIds: string[]
}
type MutationData = boolean
type MutationVariables = {
  expertise: ExpertiseInput
}
type MutationResult = Promise<ExecutionResult<MutationData>>
type HookResult = {
  editExpertise: (
    expertise: ExpertiseInput,
    assignedTermsCount: number,
  ) => MutationResult
  loading: boolean
  error: string | null
}

export function useEditExpertise(): HookResult {
  const [mutation, { loading, error }] = useMutation<
    MutationData,
    MutationVariables
  >(editExpertise)
  return {
    editExpertise: (
      { assignedTermsIds, unassignedTermsIds },
      assignedTermsCount,
    ): MutationResult =>
      mutation({
        variables: {
          expertise: {
            assignedTermsIds,
            unassignedTermsIds,
          },
        },
        update: cache => {
          cache.writeQuery({
            query: getPersonTaxonomyTermsCount,
            data: {
              personTaxonomyTermsCount: assignedTermsCount,
            },
          })
        },
      }),
    loading,
    error: error ? parseGQLError(error) : null,
  }
}
