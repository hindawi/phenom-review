import { useState, useCallback, useEffect } from 'react'
import { debounce } from 'lodash'
import { useSearchedTerms } from './useSearchedTerms'
import { useTerms } from './useTerms'
import {
  TermDataNode,
  TermNodeExtraInfo,
} from '../../components/edit-expertise/tree'
import { Term } from '../hooks/Term'
import { useExpertiseContext } from '../../components/edit-expertise/drawer/expertise-context/useExpertiseContext'

function convertTermsToTermDataNode(
  terms: TermNodeExtraInfo[],
): TermDataNode[] {
  return terms.map(term => ({
    ...term,
    key: term.id,
    title: term.name,
    children:
      term.children.length > 0
        ? convertTermsToTermDataNode(term.children)
        : term.children,
  }))
}

function updateTermIsAssignedFromExpertise(
  terms: TermDataNode[],
  assignedIds: string[],
): TermDataNode[] {
  return terms.map(term => ({
    ...term,
    isAssigned: assignedIds.includes(term.id),
    children: term.children
      ? updateTermIsAssignedFromExpertise(term.children, assignedIds)
      : term.children,
  }))
}

function convertTreeToArray(tree: TermDataNode[]) {
  return tree.reduce<TermDataNode[]>((nodes, node) => {
    nodes.push(node)
    if (node.children) {
      nodes = nodes.concat(convertTreeToArray(node.children))
    }
    return nodes
  }, [])
}

function containsChildren(node: TermDataNode): boolean {
  return node.isLeaf === false && node.children.length !== 0
}

type HookResult = {
  onSearchKeyword: (e: React.ChangeEvent<HTMLInputElement>) => void
  treeData: TermDataNode[]
  searchResultTreeData: TermDataNode[]
  searchInUse: boolean
  searchLoading: boolean
  loadData: (id: string, children: TermDataNode[]) => Promise<void>
  toggleNodeAssignment: (node: TermDataNode) => void
  onExpand: (expandedKeysForTree: string[]) => Promise<void>
  expandedKeys: string[]
  inputSearchValue: string
  searchedTerms: Term[]
  error: string | null
  setTreeData: (value: React.SetStateAction<any[]>) => void
  initialLoading: boolean
  assignedTerms: Term[]
}
function termToNode(term: Term): TermDataNode {
  return {
    ...term,
    key: term.id,
    title: term.name,
    children: [],
  }
}
function toTreeData(terms: Term[]): TermDataNode[] {
  return terms.map(termToNode)
}
export function useSearchForKeywords(): HookResult {
  const [searchResultTreeData, setTreeData] = useState<TermDataNode[]>([])
  const [inputSearchValue, setInputSearchValue] = useState('')
  const searchInUse = inputSearchValue.length > 0
  const { expertise } = useExpertiseContext()

  const expertiseTermIds = expertise.assignedTerms.map(
    assignedTerm => assignedTerm.id,
  )

  const {
    treeData,
    loadData,
    toggleNodeAssignment,
    error,
    loading: initialLoading,
    assignedTerms,
  } = useTerms({
    toTreeData,
    customSetTreeData: searchInUse ? setTreeData : null,
    expertiseTermIds,
  })
  const [expandedKeys, setExpandedKeysForTree] = useState<string[]>([])

  const [searchedTerms, searchLoading] = useSearchedTerms(inputSearchValue)

  const debounceKeywordsResult = useCallback(
    debounce((nextValue: any) => setInputSearchValue(nextValue), 1000),
    [],
  )

  useEffect(() => {
    if (searchInUse && searchedTerms.length > 0) {
      setTreeData(currentSearchResultTreeData => {
        const currentTreeData =
          currentSearchResultTreeData.length > 0
            ? currentSearchResultTreeData
            : searchedTerms

        const idsOfExpandableNodes = convertTreeToArray(currentTreeData)
          .filter(containsChildren)
          .map(node => node.id)

        setExpandedKeysForTree(idsOfExpandableNodes)

        const expertiseTermIds = expertise.assignedTerms.map(
          assignedTerm => assignedTerm.id,
        )

        const preparedTreeForAntDApi = convertTermsToTermDataNode(
          currentTreeData,
        )

        return updateTermIsAssignedFromExpertise(
          preparedTreeForAntDApi,
          expertiseTermIds,
        )
      })
    }
  }, [searchInUse, searchedTerms, expertise.assignedTerms])

  useEffect(() => {
    if (searchLoading) {
      setTreeData([])
    }
  }, [inputSearchValue, searchLoading])

  const onSearchKeyword = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target

    debounceKeywordsResult(value)

    if (!value) {
      setTreeData(treeData)
      setExpandedKeysForTree([])
    }
  }
  const onExpand = async (expandedKeysForTree: string[]) => {
    setExpandedKeysForTree(expandedKeysForTree)
  }

  return {
    onSearchKeyword,
    searchResultTreeData,
    treeData,
    searchLoading,
    searchInUse,
    loadData,
    toggleNodeAssignment,
    expandedKeys,
    onExpand,
    inputSearchValue,
    searchedTerms,
    error,
    setTreeData,
    initialLoading,
    assignedTerms,
  }
}
