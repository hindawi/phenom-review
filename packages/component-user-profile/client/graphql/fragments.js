import gql from 'graphql-tag'

export const userDetailsWithIdentities = gql`
  fragment userDetailsWithIdentities on User {
    id
    isActive
    role
    identities {
      ... on Local {
        name {
          surname
          givenNames
          title
        }
        email
        aff
        affRorId
        isConfirmed
        country
      }
      ... on External {
        identifier
        email
        aff
      }
    }
  }
`
