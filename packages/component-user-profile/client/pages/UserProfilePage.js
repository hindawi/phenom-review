import React from 'react'
import { useKeycloakRole } from 'component-sso/client'
import { get } from 'lodash'
import { H2 } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withModal } from 'component-modal'
import { compose, withHandlers, withProps, withStateHandlers } from 'recompose'
import { Text, Breadcrumbs, MultiAction, withFetching } from '@hindawi/ui'
import { Row, Col } from '@hindawi/phenom-ui'
import {
  UserProfile,
  LinkOrcID,
  EmailNotifications,
  ExpertiseKeywords,
} from '../components'
import withUserGQL from '../graphql'

const AccountTop = styled.div`
  display: flex;
  align-items: baseline;
  margin-bottom: 12px;
`

const UserProfilePage = ({
  user,
  editUser,
  editMode,
  toggleEdit,
  unlinkOrcid,
  orcidIdentity,
  localIdentity,
  toggleEmailSubscription,
}) => {
  const shouldDisplayKeywords = process.env.EDITOR_EXPERTISE
  const isAllEditorExpertiseFunctionalityReady =
    process.env.SHOW_EDITOR_EXPERTISE_FUNCTIONALITY
  const { hasAERole } = useKeycloakRole()
  return (
    <Root>
      <Breadcrumbs mb={2}>Dashboard</Breadcrumbs>

      <AccountTop>
        <H2>Account Settings</H2>
        <Text ml={4} secondary>
          {get(localIdentity, 'email')}
        </Text>
      </AccountTop>
      <Row gutter={16}>
        <Col span={8}>
          <UserProfile
            editMode={editMode}
            editUser={editUser}
            mt={3}
            toggleEdit={toggleEdit}
            user={localIdentity}
          />

          <EmailNotifications
            isSubscribed={get(user, 'isSubscribedToEmails', true)}
            toggleEmailSubscription={toggleEmailSubscription}
          />

          <LinkOrcID
            id={get(user, 'id')}
            isLinked={get(user, 'identities', '').length !== 1}
            orcid={get(orcidIdentity, 'identifier', '')}
            unlinkOrcid={unlinkOrcid}
          />
        </Col>
        {isAllEditorExpertiseFunctionalityReady &&
          shouldDisplayKeywords &&
          hasAERole() && (
            <Col span={16}>
              <ExpertiseKeywords />
            </Col>
          )}
      </Row>
    </Root>
  )
}

// #region compose
export default compose(
  withUserGQL,
  withFetching,
  withModal({
    component: MultiAction,
    modalKey: 'subscriptionToEmails',
  }),
  withProps(({ data }) => ({
    user: get(data, 'currentUser', {}),
    orcidIdentity: get(data, 'currentUser.identities', []).find(
      i => i.identifier,
    ),
    localIdentity: get(data, 'currentUser.identities', []).find(
      i => i.__typename === 'Local',
    ),
  })),
  withStateHandlers(
    {
      editMode: false,
    },
    {
      toggleEdit: ({ editMode }) => () => ({
        editMode: !editMode,
      }),
    },
  ),
  withHandlers({
    toggleEmailSubscription: ({
      showModal,
      subscribeToEmails,
      unsubscribeFromEmails,
      user,
    }) => () =>
      showModal({
        modalKey: 'subscriptionToEmails',
        subtitle: `Are you sure you want to ${
          user.isSubscribedToEmails ? 'unsubscribe from ' : 'subscribe to'
        } emails ?`,
        title: user.isSubscribedToEmails
          ? `Unsubscribe from emails`
          : `Subscribe to emails`,
        confirmText: 'YES',
        cancelText: 'CLOSE',
        onConfirm: ({ hideModal, setFetching, setError, clearError }) => {
          clearError()
          setFetching(true)
          user.isSubscribedToEmails
            ? unsubscribeFromEmails({
                variables: {
                  input: { userId: user.id, token: user.unsubscribeToken },
                },
              })
                .then(() => {
                  setFetching(false)
                  hideModal()
                })
                .catch(e => {
                  setFetching(false)
                  setError(e.message)
                })
            : subscribeToEmails({
                variables: {
                  input: { userId: user.id },
                },
              })
                .then(() => {
                  setFetching(false)
                  hideModal()
                })
                .catch(e => {
                  setFetching(false)
                  setError(e.message)
                })
        },
      }),
    unlinkOrcid: ({ unlinkOrcid, showModal, localIdentity }) => () =>
      showModal({
        modalKey: 'subscriptionToEmails',
        subtitle: 'Are you sure you want to unlink your ORCID account?',
        title: 'Unlink ORCID',
        confirmText: 'YES',
        cancelText: 'CLOSE',
        onConfirm: ({ hideModal, setFetching, setError, clearError }) => {
          clearError()
          setFetching(true)
          unlinkOrcid()
            .then(() => {
              setFetching(false)
              hideModal()
            })
            .catch(e => {
              setFetching(false)
              setError(e.message)
            })
        },
      }),

    editUser: ({ updateUser, toggleEdit, setFetching, setError }) => ({
      givenNames,
      surname,
      aff,
      affRorId,
      title,
      country,
    }) => {
      setFetching(true)
      updateUser({
        variables: {
          input: {
            givenNames: givenNames.trim(),
            surname: surname.trim(),
            aff,
            affRorId,
            title,
            country,
          },
        },
      })
        .then(() => {
          setFetching(false)
          toggleEdit()
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
  }),
)(UserProfilePage)
// #endregion

// #region styles
const Root = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 calc(${th('gridUnit')} * 20);
  padding-top: calc(${th('gridUnit')} * 4);
`
// #endregion
