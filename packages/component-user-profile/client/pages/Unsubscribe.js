import React, { useEffect } from 'react'
import { useMutation } from 'react-apollo'
import styled from 'styled-components'
import { Button, H2 } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { Text, NewSVGLogo, SVGLogo, Icon } from '@hindawi/ui'
import { useJournal } from 'component-journal-info'
import { mutations } from '../graphql'

const Logo = ({ name, logo }) => {
  if (name === 'Hindawi') return <SVGLogo height={105} width={105} />
  if (name === 'GeoScienceWorld')
    return <NewSVGLogo logo={logo} publisher={name} />
}

const Unsubscribe = ({ history }) => {
  const { name, logo } = useJournal()
  const isUnsubscribing = history.location.pathname.includes('/unsubscribe')

  const handleSubscribe = () => {
    const base = isUnsubscribing ? '/subscribe' : '/unsubscribe'
    history.push(`${base}${history.location.search}`)
  }

  const [subscribeToEmails] = useMutation(mutations.subscribeToEmails)
  const [unsubscribeFromEmails] = useMutation(mutations.unsubscribeFromEmails)

  useEffect(() => {
    const searchParams = new URLSearchParams(history.location.search)
    const userId = searchParams.get('id')
    const token = searchParams.get('token')

    if (isUnsubscribing)
      unsubscribeFromEmails({ variables: { input: { userId, token } } }).catch(
        console.error,
      )
    else
      subscribeToEmails({ variables: { input: { userId } } }).catch(
        console.error,
      )
  }, [
    subscribeToEmails,
    unsubscribeFromEmails,
    isUnsubscribing,
    history.location.search,
  ])

  return (
    <Root>
      <Logo logo={logo} name={name} />
      <H2 mt={9}>
        <Icon fontSize="24px" icon="checks" mr={2} primary />
        {isUnsubscribing
          ? 'You have successfully unsubscribed.'
          : 'You have successfully re-subscribed.'}
      </H2>
      <Text mt={5}>
        {isUnsubscribing
          ? 'If you did this in error, you may re-subscribe using the button below.'
          : 'You can always unsubscribe by clicking the button below.'}
      </Text>
      <Button
        mt={9}
        onClick={handleSubscribe}
        pl={4}
        pr={4}
        primary
        width="fit-content"
      >
        {isUnsubscribing ? 'RESUBSCRIBE' : 'UNSUBSCRIBE'}
      </Button>
    </Root>
  )
}

export default Unsubscribe

const Root = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  padding-top: calc(${th('gridUnit')} * 18);
`
