import React from 'react'
import { Row, Item, Label, ActionLink } from '@hindawi/ui'
import { Card } from '@hindawi/phenom-ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'

const CardPhenom = styled(Card)`
  margin-top: calc(${th('gridUnit')} * 4);
  box-shadow: ${th('boxShadow')};
  border-radius: ${th('borderRadius')};
`
const EmailNotifications = ({ isSubscribed, toggleEmailSubscription }) => (
  <CardPhenom>
    <Row alignItems="center">
      <Item>
        <Label>Email Notifications </Label>
      </Item>
      <Item justify="flex-end">
        <ActionLink fontWeight={700} onClick={toggleEmailSubscription}>
          {isSubscribed ? 'Unsubscribe' : 'Re-subscribe'}
        </ActionLink>
      </Item>
    </Row>
  </CardPhenom>
)

export default EmailNotifications
