import styled from 'styled-components'

export const TermsContainer = styled.div`
  display: flex;
  padding: 8px;
  flex-direction: row;
  flex-wrap: wrap;
`
export const TermsContainerRoot = styled.div`
  background: #f5f5f5;
  border: 1px solid #e0e0e0;
  box-sizing: border-box;
  border-radius: 6px;
  height: 311px;
  overflow-y: auto;
`
export const TagWrapper = styled.div`
  margin: 4px 8px 4px 0;
`
