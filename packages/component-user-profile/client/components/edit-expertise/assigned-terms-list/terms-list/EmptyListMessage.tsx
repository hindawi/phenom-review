import React from 'react'
import { Title } from '@hindawi/phenom-ui'
import { Preset } from '@hindawi/phenom-ui/dist/Typography/Text'

export const EmptyListMessage = (): JSX.Element => (
  <Title
    preset={Preset.PRIMARY}
    style={{
      fontSize: '20px',
      color: '#828282',
      width: '100%',
      textAlign: 'center',
      margin: 0,
      lineHeight: '27px',
      marginTop: '40px',
    }}
  >
    No keywords assigned
  </Title>
)
