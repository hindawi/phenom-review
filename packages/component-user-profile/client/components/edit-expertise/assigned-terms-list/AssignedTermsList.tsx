import React from 'react'
import { Preset } from '@hindawi/phenom-ui/dist/Typography/Text'

import { Root, Text } from './styles'
import { Term } from '../../../graphql/hooks/Term'
import { TermsList } from './terms-list'

type AssignedTermsListProps = {
  terms: Term[]
  onRemove: (term: Term) => void
}

export const AssignedTermsList = ({
  terms,
  onRemove,
}: AssignedTermsListProps) => (
  <Root>
    <Text preset={Preset.REGULAR}>Assigned Keywords ({terms.length})</Text>
    <TermsList onRemove={onRemove} terms={terms} />
  </Root>
)
