import styled from 'styled-components'
import { Text as BaseText } from '@hindawi/phenom-ui'

export const Text = styled(BaseText)`
  margin-bottom: 8px;
`

export const Root = styled.div`
  display: flex;
  flex-direction: column;
`
