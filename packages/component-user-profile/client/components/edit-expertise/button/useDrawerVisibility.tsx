import { useState } from 'react'

type HookResult = {
  isVisible: boolean
  showDrawer: () => void
  closeDrawer: () => void
}

export function useDrawerVisibility(): HookResult {
  const [isVisible, setIsVisible] = useState(false)

  return {
    isVisible,
    showDrawer: () => setIsVisible(true),
    closeDrawer: () => setIsVisible(false),
  }
}
