import { useContext } from 'react'
import { HookResult } from '../../../../graphql/hooks/useExpertise'
import { ExpertiseContext } from './ExpertiseContext'

export const useExpertiseContext = (): HookResult =>
  useContext<HookResult>(ExpertiseContext)
