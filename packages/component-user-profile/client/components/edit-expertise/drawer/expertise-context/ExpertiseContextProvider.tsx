import React from 'react'

import { ExpertiseContext } from './ExpertiseContext'
import { useExpertise } from '../../../../graphql/hooks/useExpertise'

export const ExpertiseContextProvider = ({
  children,
}: {
  children: React.ReactChild
}): JSX.Element => {
  const expertiseAPI = useExpertise()

  return (
    <ExpertiseContext.Provider value={expertiseAPI}>
      {children}
    </ExpertiseContext.Provider>
  )
}
