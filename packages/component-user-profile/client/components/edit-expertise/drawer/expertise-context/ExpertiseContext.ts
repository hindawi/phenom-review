import React from 'react'
import { HookResult } from './../../../../graphql/hooks/useExpertise'

export const ExpertiseContext = React.createContext<HookResult>(
  {} as HookResult,
)
