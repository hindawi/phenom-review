import React from 'react'
import { Text } from '@hindawi/phenom-ui'
import { Preset } from '@hindawi/phenom-ui/dist/Typography/Text'

type ErrorMessageProps = {
  error: string | null
}

export const ErrorMessage = ({ error }: ErrorMessageProps): JSX.Element => {
  if (error) {
    return (
      <Text preset={Preset.MESSAGE} type="danger">
        {error}
      </Text>
    )
  }

  return null
}
