import React, { useEffect } from 'react'
import { Text as BaseText } from '@hindawi/phenom-ui'
import { Preset } from '@hindawi/phenom-ui/dist/Typography/Text'
import styled from 'styled-components'
import { AssignedTermsList } from './../../assigned-terms-list'
import { ErrorMessage } from './../error-message'
import { Term } from '../../../../graphql/hooks/Term'

import { TermDataNode, TermsTree, SearchKeywords } from '../../tree'
import { useExpertiseContext } from './../expertise-context'
import { useSearchForKeywords } from '../../../../graphql/hooks/useSearchForKeywords'

function nodeToTerm(node: TermDataNode): Term {
  return {
    id: node.id,
    parentId: node.parentId,
    name: node.name,
    path: node.path,
    depth: node.depth,
    isLeaf: node.isLeaf,
    isAssigned: node.isAssigned,
  }
}

function termToNode(term: Term): TermDataNode {
  return {
    ...term,
    key: term.id,
    title: term.name,
    children: [],
  }
}

function toggleAssignment(
  tree: TermDataNode[],
  targetNode: TermDataNode,
): TermDataNode[] {
  return tree.map(node => {
    if (node.id === targetNode.id) {
      return {
        ...node,
        isAssigned: !node.isAssigned,
      }
    }

    if (node.children) {
      return {
        ...node,
        children: toggleAssignment(node.children, targetNode),
      }
    }

    return node
  })
}

const Text = styled(BaseText)`
  margin-bottom: 8px;
`
export const DrawerContent = (): JSX.Element => {
  const {
    expertise,
    assignTerm,
    unassignTerm,
    setExpertise,
    setCurrentlyAssignedTerms,
  } = useExpertiseContext()
  const {
    onSearchKeyword,
    treeData,
    searchInUse,
    searchResultTreeData,
    expandedKeys,
    loadData,
    toggleNodeAssignment,
    searchLoading,
    onExpand,
    inputSearchValue,
    searchedTerms,
    error,
    setTreeData,
    initialLoading,
    assignedTerms,
  } = useSearchForKeywords()

  useEffect((): void => {
    if (!initialLoading && !error) {
      setExpertise({
        assignedTerms,
        unassignedTerms: [],
      })
      setCurrentlyAssignedTerms(assignedTerms)
    }
  }, [
    assignedTerms,
    initialLoading,
    setExpertise,
    setCurrentlyAssignedTerms,
    error,
  ])

  if (error) {
    return <ErrorMessage error={error} />
  }

  const LABEL = 'Select New Keywords'
  const emptySearchResult = searchedTerms.length === 0

  const updateExpertise = (term: Term): void => {
    if (term.isAssigned) {
      assignTerm(term)
    } else {
      unassignTerm(term)
    }
  }

  function deleteTerm(term: Term): void {
    unassignTerm(term)
    toggleNodeAssignment(termToNode(term))
  }

  const toggleNodeSearchAssignment = (node: TermDataNode) => {
    setTreeData(currentTreeData => toggleAssignment(currentTreeData, node))
  }

  const onAssignTerms = (node: TermDataNode) => {
    toggleNodeAssignment(node)
    updateExpertise(nodeToTerm({ ...node, isAssigned: !node.isAssigned }))
    toggleNodeSearchAssignment(node)
  }

  return (
    <>
      <Text preset={Preset.REGULAR}>{LABEL}</Text>
      <SearchKeywords onSearch={onSearchKeyword} />
      <TermsTree
        emptySearchResult={emptySearchResult}
        expandedKeys={expandedKeys}
        initialLoading={initialLoading}
        onAssign={onAssignTerms}
        onExpand={onExpand}
        onLoadData={({ id, children }) => loadData(id, children)}
        searchLoading={searchLoading}
        searchValue={inputSearchValue}
        treeData={searchInUse ? searchResultTreeData : treeData}
      />
      <AssignedTermsList
        onRemove={deleteTerm}
        terms={expertise.assignedTerms}
      />
    </>
  )
}
