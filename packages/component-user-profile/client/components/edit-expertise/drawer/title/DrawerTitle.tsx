import React from 'react'
import { Title } from '@hindawi/phenom-ui'

const TITLE_LABEL = 'Edit Your Expertise Keywords'

export const DrawerTitle = () => <Title preset="primary">{TITLE_LABEL}</Title>
