import styled from 'styled-components'

export const Root = styled.div`
  float: right;
  display: flex;
  align-items: center;
`
export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 154px;
`
