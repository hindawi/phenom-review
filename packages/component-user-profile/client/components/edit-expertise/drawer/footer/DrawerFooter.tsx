import React, { useState } from 'react'
import { Button, Spinner } from '@hindawi/phenom-ui'

import { Root, Container } from './styles'
import { ErrorMessage } from './../error-message'
import { useExpertiseContext } from './../expertise-context'

type DrawerFooterProps = {
  onClose: () => void
}

export const DrawerFooter = ({ onClose }: DrawerFooterProps): JSX.Element => {
  const {
    editExpertise,
    editingExpertise,
    editExpertiseError,
    cannotEdit,
  } = useExpertiseContext()
  const [savedExpertise, setSavedExpertise] = useState(false)

  async function saveExpertise() {
    await editExpertise()
    setSavedExpertise(true)
    onClose()
  }
  return (
    <Root>
      <ErrorMessage error={editExpertiseError} />
      <Button onClick={onClose} size="large" style={{ marginRight: 12 }}>
        Cancel
      </Button>
      <Container>
        {editingExpertise || savedExpertise ? (
          <Spinner size="small" />
        ) : (
          <Button
            disabled={cannotEdit}
            onClick={saveExpertise}
            size="large"
            type="primary"
          >
            Save Keywords
          </Button>
        )}
      </Container>
    </Root>
  )
}
