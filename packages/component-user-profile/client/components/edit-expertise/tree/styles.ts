import styled from 'styled-components'
import { Text as BaseText } from '@hindawi/phenom-ui'

import './terms-tree.css'

export const Root = styled.div`
  margin-bottom: 24px;
`
export const Text = styled(BaseText)`
  margin-bottom: 8px;
`
