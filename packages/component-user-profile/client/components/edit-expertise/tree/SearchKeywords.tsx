import React from 'react'
import { Input, IconSearch } from '@hindawi/phenom-ui'
import './search-tree.css'

export const SearchKeywords = ({
  onSearch,
}: {
  onSearch: (e: React.ChangeEvent<HTMLInputElement>) => void
}) => (
  <div className="searchTreeWrapper">
    <Input
      allowClear
      onChange={onSearch}
      placeholder="Type to search for keywords"
      suffix={<IconSearch />}
    />
  </div>
)
