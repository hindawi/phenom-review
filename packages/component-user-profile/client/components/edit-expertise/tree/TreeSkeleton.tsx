import React from 'react'
import { Tree, Skeleton } from '@hindawi/phenom-ui'

export const TreeSkeleton = ({ nrSkeletonRows }) => {
  const generateSkeletonMockTreeData = (nr: number) =>
    [...Array(nr)].map((_, index) => ({ key: index + 1 }))
  const skeletonMockTreeData = generateSkeletonMockTreeData(nrSkeletonRows)

  const renderItem = () => (
    <Skeleton
      active
      paragraph={{ rows: 1, width: [Math.floor(Math.random() * 300 + 50)] }}
      title={false}
    />
  )

  return (
    <div className="treeWrapper skeleton-tree">
      <Tree
        blockNode
        checkStrictly
        height={308}
        selectable={false}
        showLine={false}
        titleRender={() => renderItem()}
        treeData={skeletonMockTreeData}
      />
    </div>
  )
}
