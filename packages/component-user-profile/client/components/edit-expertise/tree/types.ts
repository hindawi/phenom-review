import { TreeDataNode, TreeEventDataNode } from '@hindawi/phenom-ui'
import { Term } from '../../../graphql/hooks/Term'

export type TermNodeExtraInfo = Term & {
  children: TermDataNode[]
}
export type TermDataNode = TreeDataNode & TermNodeExtraInfo
export type TermEventDataNode = TreeEventDataNode & TermNodeExtraInfo
