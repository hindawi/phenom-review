import { IconCheck as BaseIconCheck } from '@hindawi/phenom-ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export const Root = styled.div`
  display: flex;
`
export const IconCheck = styled(BaseIconCheck)`
  margin-left: calc(${th('gridUnit')} * 2);
  color: ${th('actionPrimaryColor')};
  font-size: ${th('fontSizeBase')};
  display: flex;
  align-items: center;
`
