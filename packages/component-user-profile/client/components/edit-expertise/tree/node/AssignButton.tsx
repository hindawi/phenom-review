import React from 'react'
import { Button, IconExpand, IconCollapse } from '@hindawi/phenom-ui'

import { TermDataNode } from './../types'

export const AssignButton = ({
  node,
  onAssign,
}: {
  node: TermDataNode
  onAssign: (node: TermDataNode) => void
}): JSX.Element => (
  <Button
    icon={node.isAssigned ? <IconCollapse /> : <IconExpand />}
    onClick={() => {
      onAssign(node)
    }}
    type="text"
  >
    {node.isAssigned ? 'Unassign' : 'Assign'}
  </Button>
)
