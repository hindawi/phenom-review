import React from 'react'
import parse from 'html-react-parser'
import { Title, Text } from '@hindawi/phenom-ui'
import { Preset } from '@hindawi/phenom-ui/dist/Typography/Text'

import { TermDataNode } from './../types'
import { sanitizeString } from '../../../../graphql/hooks/useSearchedTerms'

const underlineMatchedValue = (text: string, searchedValue: string): string => {
  const searchedValueRegex = RegExp(
    sanitizeString(searchedValue)
      .split(' ')
      .join('|'),
    'gi',
  )

  return text.replace(
    searchedValueRegex,
    match => `<span style="textDecoration:underline">${match}</span>`,
  )
}

const getNodeTitle = (
  searchedValue: string,
  node: TermDataNode,
): string | JSX.Element | JSX.Element[] => {
  if (searchedValue.length === 0) {
    return node.name
  }

  return parse(underlineMatchedValue(node.name, searchedValue))
}

const customStyleForLongWords = title =>
  title.length > 30 ? { fontSize: '11px', lineHeight: '1' } : {}

export const NodeLabel: React.FC<{
  node: TermDataNode
  searchValue: string
}> = ({ node, searchValue }): JSX.Element => {
  if (node.depth === 1) {
    return (
      <Title preset="small" style={{ marginBottom: 0 }}>
        {getNodeTitle(searchValue, node)}
      </Title>
    )
  }
  if (node.depth > 1 && !node.isLeaf) {
    return (
      <Text preset={Preset.REGULAR}>{getNodeTitle(searchValue, node)}</Text>
    )
  }
  if (node.depth > 1 && node.isLeaf) {
    return (
      <Text preset={Preset.PRIMARY} style={customStyleForLongWords(node.title)}>
        {getNodeTitle(searchValue, node)}
      </Text>
    )
  }
}
