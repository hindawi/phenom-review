import React from 'react'
import { Root, IconCheck } from './styles'

import { TermDataNode } from './../types'
import { NodeLabel } from './NodeLabel'

export const NodeInfo = ({
  node,
  searchValue,
}: {
  node: TermDataNode
  searchValue: string
}): JSX.Element => (
  <Root>
    <NodeLabel node={node} searchValue={searchValue} />
    {node.isAssigned ? <IconCheck /> : ''}
  </Root>
)
