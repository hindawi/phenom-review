import React from 'react'
import { Tree, IconCaretDown, Title } from '@hindawi/phenom-ui'

import { Root } from './styles'
import { TermDataNode, TermEventDataNode } from './types'
import { TreeNode } from './node'
import { TreeSkeleton } from './TreeSkeleton'

export type TermsTreeProps = {
  treeData: TermDataNode[]
  onLoadData: (treeNode: TermEventDataNode) => Promise<void>
  onAssign: (node: TermDataNode) => void
  onExpand: (expandedKeys: string[]) => Promise<void>
  expandedKeys: string[]
  searchLoading: boolean
  searchValue: string
  emptySearchResult: boolean
  initialLoading: boolean
}

const infoMessageWhenSearchKeyword = (message: string) => (
  <Title preset="primary" style={{ display: 'flex', justifyContent: 'center' }}>
    {message}
  </Title>
)
const infoMessageMapping = {
  NO_RESULTS: 'No results found',
  KEYWORD_LENGTH: 'Type at least 3 letters',
}

export const TermsTree = ({
  treeData,
  onLoadData,
  onAssign,
  expandedKeys,
  onExpand,
  searchValue,
  searchLoading,
  emptySearchResult,
  initialLoading,
}: TermsTreeProps): JSX.Element => {
  if (searchLoading || initialLoading) {
    return (
      <Root>
        <TreeSkeleton nrSkeletonRows={10} />
      </Root>
    )
  }
  const threeLettersTyped = searchValue.length > 2 || searchValue === ''

  return (
    <Root>
      <div className="treeWrapper">
        {(() => {
          if (!threeLettersTyped && !searchLoading) {
            return infoMessageWhenSearchKeyword(
              infoMessageMapping.KEYWORD_LENGTH,
            )
          } else if (emptySearchResult && !!searchValue && !searchLoading) {
            return infoMessageWhenSearchKeyword(infoMessageMapping.NO_RESULTS)
          }
          return (
            <Tree
              blockNode
              checkStrictly
              expandedKeys={expandedKeys}
              height={308}
              loadData={onLoadData}
              onExpand={onExpand}
              selectable={false}
              showLine={false}
              switcherIcon={
                <div className="switcherPhenomIcon">
                  <IconCaretDown />
                </div>
              }
              titleRender={(node: TermDataNode) => (
                <TreeNode
                  node={node}
                  onAssign={onAssign}
                  searchValue={searchValue}
                />
              )}
              treeData={treeData}
            />
          )
        })()}
      </div>
    </Root>
  )
}
