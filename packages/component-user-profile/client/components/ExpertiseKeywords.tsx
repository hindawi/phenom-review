import React from 'react'
import { Card, Spinner, Text } from '@hindawi/phenom-ui'
import { Preset } from '@hindawi/phenom-ui/dist/Typography/Text'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import { usePersonTaxonomyTermsCount } from '../graphql/hooks/usePersonTaxonomyTermsCount'
import { EditExpertiseButton } from './edit-expertise'

const CardPhenom = styled(Card)`
  margin-left: 16px;
  border-radius: ${th('gridUnit')};
  box-shadow: ${th('boxShadow')};
  max-height: 75vh;
`
const CardHeader = styled.div`
  display: flex;
  justify-content: space-between;
  min-height: 33px;
`

const ExpertiseWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`

const ExpertiseText = styled(Text)`
  padding-left: 24px;
  line-height: 15px;
`

export const ExpertiseKeywords = (): JSX.Element => {
  const {
    personTaxonomyTermsCount,
    loading,
    error,
  } = usePersonTaxonomyTermsCount()

  if (loading) {
    return <Spinner />
  }

  return (
    <CardPhenom bordered>
      <CardHeader>
        <ExpertiseWrapper>
          <Text preset={Preset.REGULAR}>Expertise Keywords</Text>
          {personTaxonomyTermsCount === 0 && (
            <ExpertiseText preset={Preset.MESSAGE}>
              No keywords assigned
            </ExpertiseText>
          )}
          {error && (
            <ExpertiseText preset={Preset.MESSAGE} type="warning">
              Error in loading total number
            </ExpertiseText>
          )}
          {personTaxonomyTermsCount > 0 && (
            <ExpertiseText preset={Preset.MESSAGE}>
              {personTaxonomyTermsCount} assigned
            </ExpertiseText>
          )}
        </ExpertiseWrapper>
        <EditExpertiseButton />
      </CardHeader>
    </CardPhenom>
  )
}
