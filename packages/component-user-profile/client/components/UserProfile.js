/* eslint-disable handle-callback-err  */
import { get } from 'lodash'
import { Formik } from 'formik'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import React, { Fragment } from 'react'
import { th } from '@pubsweet/ui-toolkit'
import { compose, withProps } from 'recompose'
import { H3, Spinner, TextField } from '@pubsweet/ui'
import { Card } from '@hindawi/phenom-ui'
import {
  Row,
  Item,
  Text,
  Menu,
  Icon,
  Label,
  ActionLink,
  ShadowedBox,
  MenuCountry,
  RowOverrideAlert,
  ValidatedFormField,
  validators,
  withFetching,
  withCountries,
  titleOptions,
  renderRor,
  RorInfoState,
} from '@hindawi/ui'

const CardPhenom = styled(Card)`
  box-shadow: ${th('boxShadow')};
  border-radius: ${th('borderRadius')};
`

const rorInfo = new RorInfoState()

const submitHandler = ({
  setFieldValue,
  setTouched,
  submitForm,
  initialValues,
}) => () => {
  if (initialValues.aff && rorInfo.getData().aff === '') {
    // means we're in the edit mode and not touching aff field
    rorInfo.setData({
      aff: initialValues.aff,
      affRorId: initialValues.affRorId,
    })
  }

  const { aff, affRorId } = rorInfo.getData()

  // Ror component is quite custom so we need to manually trigger
  // updates for Formik fields
  setFieldValue('aff', aff)
  setFieldValue('affRorId', affRorId)

  // we need to do this manually since Formik doesn't seem to be doing it for some reason
  setTouched({ aff: true })

  // because we manually update aff field with `setFieldValue`, that triggers some update mechanism
  // inside Formik and prevents it from triggering `submitForm`
  // this is why we need to wait a bit for that action to clear up and then be able to run submit form action
  setTimeout(() => {
    submitForm().then(() => {
      rorInfo.resetData()
    })
  }, 10)
}

const Profile = ({
  titles,
  toggleEdit,
  countryLabel,
  user: { affiliation, firstName, lastName, title, country },
}) => (
  <Formik onSubmit={toggleEdit}>
    {({ handleSubmit }) => (
      <CardPhenom>
        <Icon
          fontSize="16px"
          icon="edit"
          onClick={handleSubmit}
          right={16}
          top={16}
        />
        <Fragment>
          <Row alignItems="center" height={5}>
            <Item>
              <H3>Account Details</H3>
            </Item>
            <Item>
              <ActionLink
                data-test-id="change-password-btn"
                internal
                to="/profile/change-password"
              >
                Change Password
              </ActionLink>
            </Item>
          </Row>

          <Row alignItems="baseline" mt={4}>
            <Item vertical>
              <Label>First Name</Label>
              <Text data-test-id="first-name-text" secondary>
                {firstName}
              </Text>
            </Item>
            <Item vertical>
              <Label>Last Name</Label>
              <Text data-test-id="last-name-text" secondary>
                {lastName}
              </Text>
            </Item>
          </Row>

          <Row alignItems="baseline" mt={4}>
            <Item vertical>
              <Label>Title</Label>
              <Text data-test-id="title-text" secondary>
                {get(
                  titles.find(t => t.value === title),
                  'label',
                  '',
                )}
              </Text>
            </Item>
            <Item vertical>
              <Label>Country</Label>
              <Text data-test-id="country-text" secondary>
                {countryLabel(country || '')}
              </Text>
            </Item>
          </Row>

          <Row alignItems="baseline" mt={4}>
            <Item vertical>
              <Label>Affiliation</Label>
              <Text data-test-id="affiliation-text" secondary>
                {affiliation}
              </Text>
            </Item>
          </Row>
        </Fragment>
      </CardPhenom>
    )}
  </Formik>
)

const EditUserProfile = compose(
  withFetching,
  withProps(({ user }) => ({
    initialValues: {
      ...user,
      givenNames: user.firstName,
      aff: user.affiliation,
      surname: user.lastName,
      title: user.title,
    },
  })),
)(
  ({
    titles,
    onSave,
    countries,
    isFetching,
    toggleEdit,
    fetchingError,
    initialValues,
    editUser,
    ...rest
  }) => (
    <Formik initialValues={initialValues} onSubmit={editUser}>
      {({ setFieldValue, setTouched, submitForm }) => (
        <ShadowedBox position="relative" {...rest}>
          {isFetching ? (
            <StyledSpinner>
              <Spinner />
            </StyledSpinner>
          ) : (
            <Fragment>
              <Icon
                data-test-id="cancel-btn"
                fontSize="16px"
                icon="remove"
                onClick={toggleEdit}
                right={48}
                top={16}
              />
              <Icon
                data-test-id="save-btn"
                fontSize="16px"
                icon="save"
                onClick={submitHandler({
                  setFieldValue,
                  setTouched,
                  submitForm,
                  initialValues,
                })}
                right={16}
                top={16}
              />
            </Fragment>
          )}
          <Fragment>
            <Row height={5}>
              <Item>
                <H3>Edit Account Details</H3>
              </Item>
            </Row>

            <Row alignItems="baseline" mt={4}>
              <Item mr={2} vertical>
                <Label required>First Name</Label>
                <ValidatedFormField
                  component={TextField}
                  data-test-id="first-name-input"
                  inline
                  name="givenNames"
                  validate={[validators.required]}
                />
              </Item>

              <Item ml={2} vertical>
                <Label required>Last Name</Label>
                <ValidatedFormField
                  component={TextField}
                  data-test-id="last-name-input"
                  inline
                  name="surname"
                  validate={[validators.required]}
                />
              </Item>
            </Row>

            <RowOverrideAlert alignItems="baseline" mt={4}>
              <Item mr={2} vertical>
                <Label required>Title</Label>
                <ValidatedFormField
                  component={Menu}
                  data-test-id="title-input"
                  inline
                  name="title"
                  options={titles}
                  validate={[validators.required]}
                />
              </Item>

              <Item ml={2} vertical>
                <Label required>Country</Label>
                <ValidatedFormField
                  component={MenuCountry}
                  data-test-id="country-dropdown"
                  inline
                  name="country"
                  validate={[validators.required]}
                />
              </Item>
            </RowOverrideAlert>

            <Row alignItems="baseline" mt={2}>
              <Item vertical>
                <Label required>Affiliation</Label>
                <ValidatedFormField
                  component={renderRor({
                    initialValues,
                    rorInfo,
                    dropdownMatchSelectWidth: 200,
                  })}
                  data-test-id="affiliation-input"
                  inline
                  name="aff"
                  validate={[validators.required]}
                />
              </Item>
            </Row>

            {fetchingError && (
              <Row alignItems="center" mt={4}>
                <Text error>{fetchingError}</Text>
              </Row>
            )}
          </Fragment>
        </ShadowedBox>
      )}
    </Formik>
  ),
)

const UserProfile = ({
  user,
  onSave,
  editMode,
  toggleEdit,
  countries,
  countryLabel,
  ...rest
}) =>
  !editMode ? (
    <Profile
      countryLabel={countryLabel}
      titles={titleOptions}
      toggleEdit={toggleEdit}
      user={user}
      {...rest}
    />
  ) : (
    <EditUserProfile
      countries={countries}
      onSave={onSave}
      titles={titleOptions}
      toggleEdit={toggleEdit}
      user={user}
      {...rest}
    />
  )
UserProfile.propTypes = {
  /** Saves user profile submitted changes */
  onSave: PropTypes.func,
  /** Passes properties for the users profile */
  user: PropTypes.shape({
    /** Users unique id. */
    id: PropTypes.string,
    /** Type of created account. */
    type: PropTypes.string,
    /** Determine if account is admin ot not. */
    admin: PropTypes.bool,
    /** Email used for user authentification. */
    email: PropTypes.string,
    /**  */
    teams: PropTypes.array,
    /** Title of account userd. */
    title: PropTypes.string,
    /**  */
    agreeTC: PropTypes.bool,
    /** Country of account user. */
    contry: PropTypes.string,
    /** Status of account. */
    isActive: PropTypes.bool,
    /** Last Name of accounts user. */
    lastName: PropTypes.string,
    /** First name of accounts user. */
    username: PropTypes.string,
    /** Account user first name. */
    firstName: PropTypes.string,
    /**  */
    fragments: PropTypes.array,
    /** Accounts user affiliation. */
    affiliation: PropTypes.string,
    /** */
    collection: PropTypes.array,
    /** Determine if account is confirmed or not. */
    isConfirmed: PropTypes.bool,
    /** Determine if account is Triage Editor or not. */
    triageEditor: PropTypes.bool,
    /**  */
    notifications: PropTypes.shape({
      email: PropTypes.shape({
        user: PropTypes.bool,
        system: PropTypes.bool,
      }),
    }),

    /** Determine if account is academic editor or not. */
    academicEditor: PropTypes.bool,
    /** Users unique token */
    token: PropTypes.string,
  }),
}

UserProfile.defaultProps = {
  onSave: () => {},
  user: {},
}

export default compose(
  withCountries,
  withProps(({ user, editMode }) => ({
    user: {
      country: get(user, 'country', ''),
      lastName: get(user, 'name.surname', ''),
      firstName: get(user, 'name.givenNames', ''),
      affiliation: get(user, 'aff', ''),
      title: get(user, 'name.title', ''),
    },
    editMode,
  })),
)(UserProfile)

// #region styles
const StyledSpinner = styled.div`
  position: absolute;
  top: calc(${th('gridUnit')} * 2);
  right: calc(${th('gridUnit')} * 2);
`
// #endregion
