const initialize = ({ User }) => ({
  execute: async ({ input: { userId } }) => {
    const user = await User.find(userId)

    if (!user) throw new Error(`Invalid user ${userId}.`)
    if (user.isSubscribedToEmails) return

    user.updateProperties({ isSubscribedToEmails: true })
    await user.save()
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
