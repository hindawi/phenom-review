const updateUserUseCase = require('./updateUser')
const subscribeToEmailsUseCase = require('./subscribeToEmails')
const unsubscribeFromEmailsUseCase = require('./unsubscribeFromEmails')
const changePasswordUseCase = require('./changePassword')
const unlinkOrcidUseCase = require('./unlinkOrcid')
const cancelReviewerJobsUseCase = require('./cancelReviewerJobs')

module.exports = {
  updateUserUseCase,
  unlinkOrcidUseCase,
  changePasswordUseCase,
  subscribeToEmailsUseCase,
  cancelReviewerJobsUseCase,
  unsubscribeFromEmailsUseCase,
}
