const initialize = (User, cancelReviewerJobsUseCase) => ({
  execute: async ({ userId, token }) => {
    const user = await User.find(userId)

    if (!user) throw new Error(`Invalid user ${userId}.`)

    if (!user.isSubscribedToEmails) return

    if (user.unsubscribeToken !== token)
      throw new Error(`Invalid token ${token}.`)

    user.updateProperties({ isSubscribedToEmails: false })
    await user.save()

    await cancelReviewerJobsUseCase.execute(userId)
  },
})

const authsomePolicies = ['public']

export { initialize, authsomePolicies }
