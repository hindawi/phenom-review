const initialize = models => ({
  execute: async (userId: string) => {
    const { Job, TeamMember, Team } = models
    const reviewers = await TeamMember.findAllByUserAndRole({
      userId,
      role: Team.Role.reviewer,
    })
    if (!reviewers.length) return

    const reviewersJobs = await Job.findAllByTeamMembers(
      reviewers.map(reviewer => reviewer.id),
    )
    if (!reviewersJobs.length) return

    await Promise.all(
      reviewersJobs.map(async job => {
        Job.cancel(job.id)
        return job.delete()
      }),
    )
  },
})

export { initialize }
