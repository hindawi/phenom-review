const models = require('@pubsweet/models')
const events = require('component-events')
const { withAuthsomeMiddleware } = require('helper-service')
const { token: tokenService } = require('pubsweet-server/src/authentication')

const useCases = require('./useCases')

const resolvers = {
  Mutation: {
    async updateUser(_, { input }, ctx) {
      return useCases.updateUserUseCase
        .initialize(models)
        .execute({ userId: ctx.user, input })
    },
    async subscribeToEmails(_, { input }, ctx) {
      return useCases.subscribeToEmailsUseCase
        .initialize(models)
        .execute({ input })
    },
    async unsubscribeFromEmails(_, { input }, ctx) {
      const cancelReviewerJobsUseCase = useCases.cancelReviewerJobsUseCase.initialize(
        models,
      )
      return useCases.unsubscribeFromEmailsUseCase
        .initialize(models.User, cancelReviewerJobsUseCase)
        .execute({ userId: input.userId, token: input.token })
    },
    async changePassword(_, { input }, ctx) {
      return useCases.changePasswordUseCase
        .initialize(tokenService, models)
        .execute({ input, userId: ctx.user })
    },
    async unlinkOrcid(_, { input }, ctx) {
      const eventsService = events.initialize({ models })

      return useCases.unlinkOrcidUseCase
        .initialize({ models, eventsService })
        .execute({ userId: ctx.user })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
