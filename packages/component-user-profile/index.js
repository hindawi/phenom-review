const fs = require('fs')
const path = require('path')

const linkOrcid = require('./server/dist/linkOrcid')
const resolvers = require('./server/dist/resolvers')

module.exports = {
  resolvers,
  server: () => linkOrcid,
  typeDefs: fs.readFileSync(
    path.join(__dirname, '/server/src/typeDefs.graphqls'),
    'utf8',
  ),
}
