import React from 'react'
import { render, act } from '@testing-library/react'
import { MockedProvider } from '@apollo/react-testing'

import { FileLayoutWithScanning } from '../components'

import { getFileScanStatus as GET_FILE_SCAN_STATUS_QUERY } from '../graphql/queries'

jest.mock('../components/constants.js', () => ({
  ANTIVIRUS_SCAN_POLLING_INTERVAL: 100,
}))

const fileMock = {
  id: '123454312',
  size: 12341,
  mimeType: 'application/pdf',
  filename: 'mocked-file.pdf',
}

const waitForGraphqlQueryDataResponse = (ms = 0) =>
  act(async () => {
    await new Promise(resolve => setTimeout(resolve, ms))
  })

const renderWithStatusFromGraphqlQuery = (initialStatus, changedStatus) => {
  let queryCalled = false
  const graphqlMocks = [
    {
      request: {
        query: GET_FILE_SCAN_STATUS_QUERY,
        variables: {
          fileId: '123454312',
        },
      },
      newData: () => {
        if (queryCalled) {
          return {
            data: {
              getFileInfo: { scanStatus: changedStatus },
            },
          }
        }

        queryCalled = true
        return {
          data: {
            getFileInfo: { scanStatus: initialStatus },
          },
        }
      },
    },
  ]

  return render(
    <MockedProvider addTypename={false} mocks={graphqlMocks}>
      <FileLayoutWithScanning file={fileMock} />
    </MockedProvider>,
  )
}

describe('FileLayoutWithScanning', () => {
  it('should render state for fetching the scan status', async () => {
    const { getByText } = renderWithStatusFromGraphqlQuery('scanning')

    expect(getByText('Fetching scan status')).toBeInTheDocument()
  })

  it('should render state for scanning in progress', async () => {
    const { getByText } = renderWithStatusFromGraphqlQuery('scanning')

    await waitForGraphqlQueryDataResponse()

    expect(getByText('Scanning for viruses')).toBeInTheDocument()
  })

  it('should render state for scanning succeeded', async () => {
    jest.setTimeout(30000)

    const { getByText } = renderWithStatusFromGraphqlQuery(
      'scanning',
      'healthy',
    )

    await waitForGraphqlQueryDataResponse(200)

    expect(getByText('Uploaded')).toBeInTheDocument()
  })

  it('should render state for scanning failed', async () => {
    const { getByText } = renderWithStatusFromGraphqlQuery('error')

    await waitForGraphqlQueryDataResponse()

    expect(getByText('Scan failed! Retry scan.')).toBeInTheDocument()
  })

  it('should render state for existent previous uploaded files that skipped scanning', async () => {
    const { container } = renderWithStatusFromGraphqlQuery('skipped')

    await waitForGraphqlQueryDataResponse()

    expect(
      container.getElementsByClassName('phenom-file-card__button').length,
    ).toBe(3)
  })
})
