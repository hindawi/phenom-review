import React from 'react'
import { space } from 'styled-system'
import { withProps } from 'recompose'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Text, Fade, Icon, Loader } from '@hindawi/ui'

import { useFileDownload } from '../decorators'
import { fileHasPreview, parseFileSize } from '../fileUtils'

const FileLayout = ({
  onDelete,
  fileSize,
  hasDelete = false,
  item: file,
  hasPreview,
  dragHandle = null,
  onPreview = () => () => {},
  ...rest
}) => {
  const { isFetching, downloadFile, fetchingError } = useFileDownload(file)
  return (
    <Root data-test-id={`file-${file.id}`} {...rest}>
      {typeof dragHandle === 'function' ? dragHandle() : dragHandle}
      <FileInfo>
        {file.filename}
        <FileSize ml={2}>{fileSize}</FileSize>
      </FileInfo>

      {hasPreview && (
        <Icon
          color="textPrimaryColor"
          data-test-id={`${file.id}-preview`}
          fontSize="15px"
          icon="preview"
          ml={4}
          onClick={onPreview(file)}
          secondary
        />
      )}

      {isFetching ? (
        <Loader iconSize={4} mb={1} ml={2} mr={2} />
      ) : (
        <Icon
          color="textPrimaryColor"
          data-test-id={`${file.id}-preview`}
          fontSize="16px"
          icon="download"
          ml={4}
          mr={4}
          onClick={downloadFile}
          secondary
        />
      )}

      {hasDelete && (
        <Icon
          color="textPrimaryColor"
          data-test-id={`${file.id}-delete`}
          fontSize="16px"
          icon="delete"
          ml={4}
          mr={4}
          onClick={onDelete}
          secondary
        />
      )}
      {fetchingError && (
        <Fade>
          <ErrorWrapper>
            <Text error>{fetchingError}</Text>
          </ErrorWrapper>
        </Fade>
      )}
    </Root>
  )
}

export default withProps(({ item }) => ({
  fileSize: parseFileSize(item),
  hasPreview: fileHasPreview(item),
}))(FileLayout)

// #region styles
const Root = styled.div`
  align-items: center;
  background-color: ${({ grey }) =>
    grey ? th('grey20') : th('colorBackgroundHue')};
  box-shadow: ${({ shadow }) => (shadow ? th('boxShadow') : 'none')};
  border-radius: ${th('borderRadius')};
  display: flex;
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  height: calc(${th('gridUnit')} * 8);
  position: relative;
  white-space: nowrap;

  ${space};
`

const ErrorWrapper = styled.div`
  position: absolute;
  top: calc(${th('gridUnit')} * 10);
  left: 0;

  width: calc(${th('gridUnit')} * 40);
`

const FileInfo = styled.div`
  align-items: center;
  border-right: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  font-family: ${th('defaultFont')};
  display: flex;
  height: inherit;
  flex: 1;
  justify-content: space-between;
  margin-left: calc(${th('gridUnit')} * 2);
  color: ${th('textPrimaryColor')};
`

const FileSize = styled.span`
  font-weight: 700;
  margin: 0 calc(${th('gridUnit')} * 2);
  color: ${th('textSecondaryColor')};
`
// #endregion
