import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Fade, Text, Icon, Loader } from '@hindawi/ui'

import { useZipDownload } from '../decorators'

const DownloadZip = ({ manuscript, ...rest }) => {
  const { isFetching, downloadZip, fetchingError } = useZipDownload({
    manuscriptId: manuscript.id,
    archiveName: `ID-${manuscript.customId || manuscript.id}`,
  })

  return (
    <Root>
      {isFetching ? (
        <Loader iconSize={2} mb={1 / 2} />
      ) : (
        <Icon
          {...rest}
          fontSize="14px"
          icon="downloadZip"
          onClick={downloadZip}
        />
      )}
      {fetchingError && (
        <Fade>
          <ErrorWrapper>
            <Text error>{fetchingError}</Text>
          </ErrorWrapper>
        </Fade>
      )}
    </Root>
  )
}

export default DownloadZip

// #region styles
const Root = styled.div`
  align-items: center;
  display: flex;
  justify-content: flex-start;
  position: relative;
`

const ErrorWrapper = styled.div`
  position: absolute;
  top: calc(${th('gridUnit')} * 3);
  left: 0;

  width: calc(${th('gridUnit')} * 20);
`
// #endregion
