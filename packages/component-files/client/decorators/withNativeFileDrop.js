import { DropTarget } from 'react-dnd'
import { NativeTypes } from 'react-dnd-html5-backend'

import { getAllowedMimeTypes } from '../fileUtils'

export default DropTarget(
  NativeTypes.FILE,
  {
    drop(
      {
        files,
        maxFiles,
        setError,
        onNativeFileDrop,
        allowedFileExtensions = [],
      },
      monitor,
    ) {
      const allowedMimeTypes = getAllowedMimeTypes(allowedFileExtensions)

      const [file] = monitor.getItem().files
      const mimeType = file.type

      if (files.length >= maxFiles) {
        setError('No more files can be added to this section.')
        return
      }

      if (
        allowedFileExtensions.length > 0 &&
        !allowedMimeTypes.includes(mimeType)
      ) {
        setError('Please upload one of the supported file types.')
        return
      }

      typeof onNativeFileDrop === 'function' && onNativeFileDrop(file)
    },
  },
  (connect, monitor) => ({
    isFileOver: monitor.isOver(),
    canDropFile: monitor.canDrop(),
    connectFileDrop: connect.dropTarget(),
  }),
)
