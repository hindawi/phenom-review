import { DropTarget } from 'react-dnd'

import { getAllowedMimeTypes } from '../fileUtils'

export default DropTarget(
  'item',
  {
    drop(
      {
        files,
        setError,
        onChangeList,
        listName: toListName,
        allowedFileExtensions = [],
        maxFiles = Number.MAX_SAFE_INTEGER,
        ...destinationProps
      },
      monitor,
    ) {
      const {
        id: fileId,
        listName: fromListName,
        mimeType,
        ...sourceProps
      } = monitor.getItem()
      setError('')
      if (toListName === fromListName) return

      const allowedMimeTypes = getAllowedMimeTypes(allowedFileExtensions)

      if (
        allowedFileExtensions.length > 0 &&
        !allowedMimeTypes.includes(mimeType)
      ) {
        setError('Please upload one of the supported file types.')
        return
      }

      if (files.length >= maxFiles) {
        setError('No more files can be added to this section.')
        return
      }

      typeof onChangeList === 'function' &&
        onChangeList({
          fileId,
          toListName,
          fromListName,
          sourceProps,
          destinationProps: {
            ...destinationProps,
            setError,
          },
        })
    },
    canDrop({ listName: toListName }, monitor) {
      const { listName: fromListName } = monitor.getItem()
      return toListName !== fromListName
    },
  },
  (connect, monitor) => ({
    isFileItemOver: monitor.isOver(),
    canDropFileItem: monitor.canDrop(),
    connectDropTarget: connect.dropTarget(),
  }),
)
