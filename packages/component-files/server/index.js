const s3service = require('./src/services/s3Service')
const resolvers = require('./src/resolvers')
const eventHandlers = require('./src/eventHandlers')
const useCases = require('./src/use-cases')
const server = require('./src/restServer')

module.exports = { s3service, resolvers, eventHandlers, server, useCases }
