const configService = require('config')
const models = require('@pubsweet/models')
const { logger } = require('component-logger')

const s3Service = require('./services/s3Service')
const useCases = require('./use-cases')

module.exports = {
  async FileMalwareScanCompleted(data) {
    return useCases.persistAntiMalwareDataUseCase
      .initialize({ models, services: { s3Service, configService, logger } })
      .execute(data)
  },
}
