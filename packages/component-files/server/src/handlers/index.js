const zipFiles = require('./zipFiles')
const downloadFile = require('./downloadFile')

module.exports = {
  zipFiles,
  downloadFile,
}
