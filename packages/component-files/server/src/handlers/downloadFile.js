const downloadFile = ({ s3, models: { File } }) => async (req, res) => {
  const { fileId } = req.params

  const file = await File.find(fileId)

  const stream = s3.getObjectStream(file.providerKey)

  stream.pipe(res)
}

module.exports = downloadFile
