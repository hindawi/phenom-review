const deleteFileUseCase = require('./deleteFile')
const getSignedUrlUseCase = require('./getSignedURL')
const uploadFileUseCase = require('./uploadFile')
const persistAntiMalwareDataUseCase = require('./persistAntiMalwareData')
const getFileInfoUseCase = require('./getFileInfo')

const uploadCommentFile = require('./uploadCommentFile')
const uploadManuscriptFile = require('./uploadManuscriptFile')
const validateFilesUseCase = require('./validateFiles')

module.exports = {
  getFileInfoUseCase,
  deleteFileUseCase,
  getSignedUrlUseCase,
  uploadFileUseCase,
  persistAntiMalwareDataUseCase,
  uploadCommentFile,
  uploadManuscriptFile,
  validateFilesUseCase,
}
