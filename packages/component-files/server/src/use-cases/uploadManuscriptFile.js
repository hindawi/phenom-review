const { chain } = require('lodash')

const {
  getFileNumber,
  isMimetypeValid,
  decomposeFilename,
} = require('../fileUtils')

const initialize = ({
  models: { File, Manuscript },
  logEvent,
  services: { s3Service, eventsService, configService },
}) => {
  async function execute({
    entityId,
    fileInput,
    fileData,
    userId,
    providerKey,
  }) {
    const { filename, mimetype, createReadStream } = await fileData
    const { quarantineBucket } = configService.get('pubsweet-component-aws-s3')

    const isManuscriptFileAlreadyUploaded = await checkIfFileIsAlreadyUploaded({
      entityId,
      fileInput,
    })
    if (isManuscriptFileAlreadyUploaded)
      throw new Error('Only one Main Manuscript file is allowed')

    if (!isMimetypeValid({ mimetype, type: fileInput.type, File }))
      throw new Error('Please upload one of the supported file types.')

    const manuscript = await Manuscript.find(entityId, 'files')

    const parsedName = await addNumberToTheFileNameIfIsDuplicate({
      manuscript,
      filename,
    })

    await s3Service.upload({
      bucket: quarantineBucket,
      key: providerKey,
      stream: createReadStream(),
      mimetype,
      metadata: {
        filename,
        type: fileInput.type,
      },
    })

    const file = new File({
      manuscriptId: entityId,
      commentId: null,
      size: fileInput.size,
      fileName: parsedName,
      providerKey,
      mimeType: mimetype,
      originalName: filename,
      type: File.Types[fileInput.type],
      scanStatus: File.ScanStatuses.SCANNING,
      scanningStartTime: new Date(),
    })

    await saveFileInDb({
      manuscript,
      file,
    })

    await eventsService.publishFileEvent({
      eventName: 'FileMalwareScanRequested',
      data: { fileName: providerKey },
    })

    if (manuscript.status !== Manuscript.Statuses.draft) {
      await logEvent({
        userId,
        manuscriptId: entityId,
        action: logEvent.actions.file_added,
        objectType: logEvent.objectType.file,
        objectId: file.id,
      })
    }

    return {
      ...file,
      filename: file.fileName,
    }
  }

  async function checkIfFileIsAlreadyUploaded({ entityId, fileInput }) {
    const manuscriptTypeFiles = await File.findAllByManuscriptAndType({
      manuscriptId: entityId,
      type: File.Types.manuscript,
    })

    const isManuscriptTypeFile = fileInput.type === File.Types.manuscript

    return isManuscriptTypeFile && manuscriptTypeFiles.length > 0
  }

  async function addNumberToTheFileNameIfIsDuplicate({ manuscript, filename }) {
    const manuscriptVersions = await Manuscript.findManuscriptsBySubmissionId({
      submissionId: manuscript.submissionId,
      excludedStatus: Manuscript.Statuses.deleted,
      eagerLoadRelations: '[files]',
    })
    const lastFile = chain(manuscriptVersions)
      .flatMap(m => m.files)
      .filter(file => file.originalName === filename)
      .sortBy('created')
      .last()
      .value()

    if (lastFile) {
      const [name, ext] = decomposeFilename(lastFile.originalName)
      const fileNumber = getFileNumber(lastFile)
      return `${name} (${fileNumber}).${ext}`
    }

    return filename
  }

  async function saveFileInDb({ manuscript, file }) {
    manuscript.assignFile(file)
    await manuscript.save()
    return file.save()
  }

  return { execute }
}

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
