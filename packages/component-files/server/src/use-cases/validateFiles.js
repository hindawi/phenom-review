const initialize = ({ File }) => {
  async function validateSubmissionFiles({ manuscriptId }) {
    const files = await File.findBy({
      manuscriptId,
    })

    assertFilesAreNotInScanningStatus(files)

    assertFilesAreNotInfected(files)

    const mainFile = files.find(({ type }) => type === File.Types.manuscript)

    assertManuscriptHasMainFile(mainFile)

    assertMainManuscriptFileIsNotEmpty(mainFile)
  }

  async function validateCommentFile({ commentId }) {
    const files = await File.findBy({
      commentId,
    })
    if (!files.length) return

    assertFilesAreNotInScanningStatus(files)

    assertFilesAreNotInfected(files)
  }

  function assertFilesAreNotInScanningStatus(files) {
    const scanningFiles = files.find(
      ({ scanStatus }) => scanStatus === File.ScanStatuses.SCANNING,
    )
    if (scanningFiles) {
      throw new ValidationError(
        'Files still being scanned. Please try again once complete.',
      )
    }
  }

  function assertFilesAreNotInfected(files) {
    const infectedFiles = files.find(
      ({ scanStatus }) => scanStatus === File.ScanStatuses.INFECTED,
    )
    if (infectedFiles) {
      throw new ValidationError('You are not allowed to submit infected files.')
    }
  }

  function assertManuscriptHasMainFile(mainFile) {
    if (!mainFile)
      throw new ValidationError('At least one main manuscript is required.')
  }

  function assertMainManuscriptFileIsNotEmpty(mainFile) {
    if (mainFile.size === 0) {
      throw new ValidationError("Manuscript file shouldn't be empty")
    }
  }

  return { validateSubmissionFiles, validateCommentFile }
}

module.exports = {
  initialize,
}
