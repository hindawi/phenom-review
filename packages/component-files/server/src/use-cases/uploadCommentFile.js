const { isMimetypeValid } = require('../fileUtils')

const initialize = ({
  models: { File, Comment },
  services: { s3Service, eventsService, configService },
}) => ({
  async execute({ entityId, fileInput, fileData, providerKey }) {
    const { filename, mimetype, createReadStream } = await fileData
    const { quarantineBucket } = configService.get('pubsweet-component-aws-s3')

    if (!isMimetypeValid({ mimetype, type: fileInput.type, File }))
      throw new Error('Please upload one of the supported file types.')

    await s3Service.upload({
      bucket: quarantineBucket,
      key: providerKey,
      stream: createReadStream(),
      mimetype,
      metadata: {
        filename,
        type: fileInput.type,
      },
    })

    const file = new File({
      manuscriptId: null,
      commentId: entityId,
      size: fileInput.size,
      fileName: filename,
      providerKey,
      mimeType: mimetype,
      originalName: filename,
      type: File.Types[fileInput.type],
      scanStatus: File.ScanStatuses.SCANNING,
      scanningStartTime: new Date(),
    })

    await file.save()

    const comment = await Comment.find(entityId, 'files')
    comment.assignFile(file)
    await comment.save()

    await eventsService.publishFileEvent({
      eventName: 'FileMalwareScanRequested',
      data: { fileName: providerKey },
    })

    return {
      ...file,
      filename: file.fileName,
    }
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
