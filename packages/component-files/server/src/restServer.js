const models = require('@pubsweet/models')

const s3 = require('./services/s3Service')
const handlers = require('./handlers')

const restServer = () => app => {
  const mw = app.locals.passport.authenticate(['bearer', 'keycloakBearer'], {
    session: false,
  })
  app.get('/files/:fileId', mw, handlers.downloadFile({ s3, models }))
  app.get('/files/zip/:manuscriptId', mw, handlers.zipFiles({ s3, models }))
}

module.exports = restServer
