const { promisify } = require('util')
const { S3 } = require('aws-sdk')
const config = require('config')
const ASCIIFolder = require('fold-to-ascii')
const { logger } = require('component-logger')

const {
  region,
  accessKeyId,
  secretAccessKey,
  bucket: reviewBucket,
} = config.get('pubsweet-component-aws-s3')

const s3 = new S3({
  region,
  accessKeyId,
  secretAccessKey,
  useAccelerateEndpoint: true,
})

const uploadAsync = promisify(s3.upload.bind(s3))
const deleteObject = promisify(s3.deleteObject.bind(s3))
const getSignedUrl = promisify(s3.getSignedUrl.bind(s3))
const copyObject = promisify(s3.copyObject.bind(s3))
const getObjectAsync = promisify(s3.getObject.bind(s3))
const listObjectsAsync = promisify(s3.listObjectsV2.bind(s3))

module.exports.listObjects = prefix =>
  listObjectsAsync({
    Bucket: reviewBucket,
    Prefix: prefix,
  })

module.exports.getObject = key =>
  getObjectAsync({ Bucket: reviewBucket, Key: key })

module.exports.getObjectStream = key =>
  s3
    .getObject({ Bucket: reviewBucket, Key: key })
    .createReadStream()
    .on('error', error => {
      logger.error(error)
    })

module.exports.upload = async ({
  bucket = reviewBucket,
  key,
  mimetype,
  stream,
  metadata = {},
}) => {
  const uploadParams = {
    Bucket: bucket,
    Key: key,
    Body: stream,
    ContentType: mimetype,
    Metadata: metadata,
  }

  if (uploadParams.Metadata.filename) {
    uploadParams.Metadata.filename = ASCIIFolder.foldReplacing(
      uploadParams.Metadata.filename,
    )
  }

  return uploadAsync(uploadParams)
}

module.exports.delete = async key =>
  deleteObject({ Bucket: reviewBucket, Key: key })

module.exports.getSignedUrl = async key =>
  getSignedUrl('getObject', { Bucket: reviewBucket, Key: key })

module.exports.copyObject = async ({ prevKey, newKey }) => {
  const copyParams = {
    Bucket: reviewBucket,
    CopySource: `${reviewBucket}/${prevKey}`,
    Key: newKey,
  }

  return copyObject(copyParams)
}

module.exports.copyObjectToAnotherBucket = async ({
  key,
  sourceBucket,
  destinationBucket,
}) => {
  const copyParams = {
    CopySource: `${sourceBucket}/${key}`,
    Bucket: destinationBucket,
    Key: key,
  }

  await copyObject(copyParams)
}

module.exports.moveObjectToAnotherBucket = async ({
  key,
  sourceBucket,
  destinationBucket,
}) => {
  const copyParams = {
    CopySource: `${sourceBucket}/${key}`,
    Bucket: destinationBucket,
    Key: key,
  }

  await copyObject(copyParams)
  await deleteObject({ Bucket: sourceBucket, Key: key })
}

module.exports.deleteObject = async ({ key, bucket }) =>
  deleteObject({ Bucket: bucket, Key: key })
