const { uploadFileUseCase } = require('../src/use-cases')
const { generateFileTypes } = require('component-generators')

const models = {
  File: {
    Types: generateFileTypes(),
  },
}

const logEvent = jest.fn()
const services = jest.fn()
const useCases = {
  uploadManuscriptFile: {
    initialize: jest.fn(() => ({
      execute: jest.fn(),
    })),
  },
  uploadCommentFile: {
    initialize: jest.fn(() => ({
      execute: jest.fn(),
    })),
  },
}

describe('uploadFile use-case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should throw error if file type is not valid', async () => {
    const result = uploadFileUseCase
      .initialize({ logEvent, models, useCases, services })
      .execute({ params: { fileInput: { type: null } } })

    await expect(result).rejects.toThrowError('File must have a valid type.')
  })

  it('should upload a manuscript file', async () => {
    await uploadFileUseCase
      .initialize({ logEvent, models, useCases, services })
      .execute({
        params: { fileInput: { type: models.File.Types.manuscript } },
      })

    expect(useCases.uploadManuscriptFile.initialize).toHaveBeenCalledTimes(1)
  })

  it('should upload a supplementary file', async () => {
    await uploadFileUseCase
      .initialize({ logEvent, models, useCases, services })
      .execute({
        params: { fileInput: { type: models.File.Types.supplementary } },
      })

    expect(useCases.uploadManuscriptFile.initialize).toHaveBeenCalledTimes(1)
  })

  it('should upload a coverLetter file', async () => {
    await uploadFileUseCase
      .initialize({ logEvent, models, useCases, services })
      .execute({
        params: { fileInput: { type: models.File.Types.coverLetter } },
      })

    expect(useCases.uploadManuscriptFile.initialize).toHaveBeenCalledTimes(1)
  })

  it('should upload a figure file', async () => {
    await uploadFileUseCase
      .initialize({ logEvent, models, useCases, services })
      .execute({
        params: { fileInput: { type: models.File.Types.figure } },
      })

    expect(useCases.uploadManuscriptFile.initialize).toHaveBeenCalledTimes(1)
  })

  it('should upload a reviewComment file', async () => {
    await uploadFileUseCase
      .initialize({ logEvent, models, useCases, services })
      .execute({
        params: { fileInput: { type: models.File.Types.reviewComment } },
      })

    expect(useCases.uploadCommentFile.initialize).toHaveBeenCalledTimes(1)
  })

  it('should upload a responseToReviewers file', async () => {
    await uploadFileUseCase
      .initialize({ logEvent, models, useCases, services })
      .execute({
        params: { fileInput: { type: models.File.Types.responseToReviewers } },
      })

    expect(useCases.uploadCommentFile.initialize).toHaveBeenCalledTimes(1)
  })
})
