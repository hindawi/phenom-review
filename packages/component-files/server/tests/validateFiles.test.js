const {
  generateFile,
  generateJournal,
  generateFileTypes,
  generateFileScanStatuses,
  generateManuscript,
  generateArticleType,
  generateComment,
} = require('component-generators')

const { validateFilesUseCase } = require('../src/use-cases')

const models = {
  File: {
    findBy: jest.fn(),
    Types: generateFileTypes(),
    ScanStatuses: generateFileScanStatuses(),
  },
}

const { File } = models

describe('validateFilesInSubmission', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Should not submit manuscript with files in scanning status', async () => {
    const journal = generateJournal()
    const articleType = generateArticleType()
    const manuscript = generateManuscript({ journal, articleType })

    const mainFile = generateFile({ type: 'manuscript', scanStatus: 'healthy' })
    const coverLetter = generateFile({
      type: 'coverLetter',
      scanStatus: 'scanning',
    })

    jest.spyOn(File, 'findBy').mockResolvedValue([mainFile, coverLetter])

    const result = validateFilesUseCase
      .initialize({ File })
      .validateSubmissionFiles({ manuscriptId: manuscript.id })

    await expect(result).rejects.toThrow(
      'Files still being scanned. Please try again once complete.',
    )
  })

  it('Should not submit manuscript with infected files', async () => {
    const journal = generateJournal()
    const articleType = generateArticleType()
    const manuscript = generateManuscript({ journal, articleType })

    const mainFile = generateFile({ type: 'manuscript', scanStatus: 'healthy' })
    const coverLetter = generateFile({
      type: 'coverLetter',
      scanStatus: 'infected',
    })

    jest.spyOn(File, 'findBy').mockResolvedValue([mainFile, coverLetter])

    const result = validateFilesUseCase
      .initialize({ File })
      .validateSubmissionFiles({ manuscriptId: manuscript.id })

    await expect(result).rejects.toThrow(
      'You are not allowed to submit infected files.',
    )
  })

  it('Should not submit manuscript without the main file', async () => {
    const journal = generateJournal()
    const articleType = generateArticleType()
    const manuscript = generateManuscript({ journal, articleType })

    const coverLetter = generateFile({
      type: 'coverLetter',
      scanStatus: 'healthy',
    })

    jest.spyOn(File, 'findBy').mockResolvedValue([coverLetter])

    const result = validateFilesUseCase
      .initialize({ File })
      .validateSubmissionFiles({ manuscriptId: manuscript.id })

    await expect(result).rejects.toThrow(
      'At least one main manuscript is required.',
    )
  })

  it('Should not submit manuscript if main file has 0 size', async () => {
    const journal = generateJournal()
    const articleType = generateArticleType()
    const manuscript = generateManuscript({ journal, articleType })

    const mainFile = generateFile({
      type: 'manuscript',
      scanStatus: 'healthy',
      size: 0,
    })
    const coverLetter = generateFile({
      type: 'coverLetter',
      scanStatus: 'healthy',
    })

    jest.spyOn(File, 'findBy').mockResolvedValue([mainFile, coverLetter])

    const result = validateFilesUseCase
      .initialize({ File })
      .validateSubmissionFiles({ manuscriptId: manuscript.id })

    await expect(result).rejects.toThrow(`Manuscript file shouldn't be empty`)
  })
})

describe('Validate file in comments', () => {
  it('Should not submit review if comment file is in scanning', async () => {
    const comment = generateComment()
    const commentFile = generateFile({
      type: 'reviewComment',
      scanStatus: 'scanning',
    })
    jest.spyOn(File, 'findBy').mockResolvedValue([commentFile])

    const result = validateFilesUseCase
      .initialize({ File })
      .validateCommentFile({ commentId: comment.id })

    await expect(result).rejects.toThrow(
      'Files still being scanned. Please try again once complete.',
    )
  })
  it('Should not submit review if comment file is infected', async () => {
    const comment = generateComment()
    const commentFile = generateFile({
      type: 'reviewComment',
      scanStatus: 'infected',
    })
    jest.spyOn(File, 'findBy').mockResolvedValue([commentFile])

    const result = validateFilesUseCase
      .initialize({ File })
      .validateCommentFile({ commentId: comment.id })

    await expect(result).rejects.toThrow(
      'You are not allowed to submit infected files.',
    )
  })
})
