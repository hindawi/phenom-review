const { uploadCommentFile } = require('../src/use-cases')
const { models } = require('fixture-service')

const { Comment, File } = models
const quarantineBucket = 'quarantine-bucket'

const services = {
  s3Service: {
    upload: jest.fn(),
  },
  eventsService: {
    publishFileEvent: jest.fn(),
  },
  configService: {
    get: jest.fn(() => ({
      quarantineBucket,
    })),
  },
}

describe('uploadCommentFile use-case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should throw error if the filetype has .rar extension', async () => {
    const result = uploadCommentFile.initialize({ models, services }).execute({
      fileData: { mimetype: 'application/vnd.rar' },
      fileInput: { type: models.File.Types.reviewComment },
    })

    await expect(result).rejects.toThrowError(
      'Please upload one of the supported file types.',
    )
  })

  it('should throw error if the filetype has .zip extension', async () => {
    const result = uploadCommentFile.initialize({ models, services }).execute({
      fileData: { mimetype: 'application/zip' },
      fileInput: { type: models.File.Types.reviewComment },
    })

    await expect(result).rejects.toThrowError(
      'Please upload one of the supported file types.',
    )
  })

  it('should throw error if the filetype has .7z extension', async () => {
    const result = uploadCommentFile.initialize({ models, services }).execute({
      fileData: { mimetype: 'application/x-7z-compressed' },
      fileInput: { type: models.File.Types.reviewComment },
    })

    await expect(result).rejects.toThrowError(
      'Please upload one of the supported file types.',
    )
  })

  it('should upload file and send event successfully', async () => {
    const input = {
      entityId: 'entity-id-1',
      fileInput: { type: models.File.Types.reviewComment },
      fileData: {
        createReadStream: jest.fn(),
        filename: 'manuscript-file-name',
        mimetype: 'application/pdf',
      },
      providerKey: '123/456',
    }

    const mockDate = new Date('2022-10-01T00:02:00.000Z')
    const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate)

    const comment = {
      id: input.entityId,
      assignFile: jest.fn(),
      save: jest.fn(),
    }
    jest.spyOn(Comment, 'find').mockResolvedValue(comment)

    const result = await uploadCommentFile
      .initialize({ models, services })
      .execute(input)

    expect(services.s3Service.upload).toHaveBeenCalledTimes(1)
    expect(services.s3Service.upload).toHaveBeenCalledWith({
      bucket: quarantineBucket,
      key: input.providerKey,
      stream: input.fileData.createReadStream(),
      mimetype: input.fileData.mimetype,
      metadata: {
        filename: input.fileData.filename,
        type: input.fileInput.type,
      },
    })

    expect(Comment.find).toHaveBeenCalledTimes(1)
    expect(Comment.find).toHaveBeenCalledWith(input.entityId, 'files')

    expect(comment.assignFile).toHaveBeenCalledWith(
      expect.objectContaining({
        manuscriptId: null,
        mimeType: input.fileData.mimetype,
        type: input.fileInput.type,
        scanStatus: 'scanning',
        scanningStartTime: mockDate,
      }),
    )

    expect(services.eventsService.publishFileEvent).toHaveBeenCalledTimes(1)
    expect(services.eventsService.publishFileEvent).toHaveBeenCalledWith({
      eventName: 'FileMalwareScanRequested',
      data: { fileName: input.providerKey },
    })

    expect(result).toEqual(
      expect.objectContaining({
        manuscriptId: null,
        filename: input.fileData.filename,
        originalName: input.fileData.filename,
        mimeType: input.fileData.mimetype,
        scanStatus: File.ScanStatuses.SCANNING,
        scanningStartTime: mockDate,
        type: File.Types.reviewComment,
      }),
    )
    spy.mockRestore()
  })
})
