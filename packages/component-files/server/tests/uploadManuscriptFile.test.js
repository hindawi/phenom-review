const { uploadManuscriptFile } = require('../src/use-cases')
const { models } = require('fixture-service')

const { File, Manuscript } = models
const quarantineBucket = 'quarantine-bucket'

const services = {
  s3Service: {
    upload: jest.fn(),
  },
  eventsService: {
    publishFileEvent: jest.fn(),
  },
  configService: {
    get: jest.fn(() => ({
      quarantineBucket,
    })),
  },
}

describe('uploadManuscriptFile use-case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should throw error if there are multiple main manuscript files', async () => {
    jest
      .spyOn(File, 'findAllByManuscriptAndType')
      .mockResolvedValue(['manuscriptFile1', 'manuscriptFile2'])

    const result = uploadManuscriptFile
      .initialize({ models, services })
      .execute({
        fileData: { mimetype: 'application/vnd.rar' },
        fileInput: { type: File.Types.manuscript },
      })

    await expect(result).rejects.toThrowError(
      'Only one Main Manuscript file is allowed',
    )
  })

  it('should throw error if the filetype has .rar extension', async () => {
    jest.spyOn(File, 'findAllByManuscriptAndType').mockResolvedValue([])

    const result = uploadManuscriptFile
      .initialize({ models, services })
      .execute({
        fileData: { mimetype: 'application/vnd.rar' },
        fileInput: { type: File.Types.manuscript },
      })

    await expect(result).rejects.toThrowError(
      'Please upload one of the supported file types.',
    )
  })

  it('should throw error if the filetype has .zip extension', async () => {
    jest.spyOn(File, 'findAllByManuscriptAndType').mockResolvedValue([])

    const result = uploadManuscriptFile
      .initialize({ models, services })
      .execute({
        fileData: { mimetype: 'application/zip' },
        fileInput: { type: File.Types.manuscript },
      })

    await expect(result).rejects.toThrowError(
      'Please upload one of the supported file types.',
    )
  })

  it('should throw error if the filetype has .7z extension', async () => {
    jest.spyOn(File, 'findAllByManuscriptAndType').mockResolvedValue([])

    const result = uploadManuscriptFile
      .initialize({ models, services })
      .execute({
        fileData: { mimetype: 'application/x-7z-compressed' },
        fileInput: { type: File.Types.manuscript },
      })

    await expect(result).rejects.toThrowError(
      'Please upload one of the supported file types.',
    )
  })

  it('should upload file and send event successfully', async () => {
    const input = {
      entityId: 'entity-id-1',
      fileInput: { type: File.Types.manuscript },
      fileData: {
        createReadStream: jest.fn(),
        filename: 'manuscript-file-name',
        mimetype: 'application/pdf',
      },
      providerKey: '123/456',
      userId: 'user-id',
    }

    const mockDate = new Date('2022-10-01T00:02:00.000Z')
    const spy = jest.spyOn(global, 'Date').mockImplementation(() => mockDate)

    const manuscript = {
      id: 'manuscript-id',
      status: 'draft',
      submissionId: 'submission-id',
      assignFile: jest.fn(),
      save: jest.fn(),
    }

    jest.spyOn(File, 'findAllByManuscriptAndType').mockResolvedValue([])
    jest.spyOn(Manuscript, 'find').mockResolvedValue(manuscript)

    const result = await uploadManuscriptFile
      .initialize({ models, services })
      .execute(input)

    expect(services.s3Service.upload).toHaveBeenCalledTimes(1)
    expect(services.s3Service.upload).toHaveBeenCalledWith({
      bucket: quarantineBucket,
      key: input.providerKey,
      stream: input.fileData.createReadStream(),
      mimetype: input.fileData.mimetype,
      metadata: {
        filename: input.fileData.filename,
        type: input.fileInput.type,
      },
    })

    expect(services.eventsService.publishFileEvent).toHaveBeenCalledTimes(1)
    expect(services.eventsService.publishFileEvent).toHaveBeenCalledWith({
      eventName: 'FileMalwareScanRequested',
      data: { fileName: input.providerKey },
    })

    expect(result).toEqual(
      expect.objectContaining({
        manuscriptId: input.entityId,
        filename: input.fileData.filename,
        originalName: input.fileData.filename,
        mimeType: input.fileData.mimetype,
        scanStatus: File.ScanStatuses.SCANNING,
        scanningStartTime: mockDate,
        type: File.Types.manuscript,
      }),
    )

    spy.mockRestore()
  })
})
