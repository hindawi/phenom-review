const fs = require('fs')
const path = require('path')

const { resolvers, eventHandlers, server, useCases } = require('./server')

module.exports = {
  server,
  resolvers,
  useCases,
  eventHandlers,
  typeDefs: fs.readFileSync(
    path.join(__dirname, '/server/src/typeDefs.graphqls'),
    'utf8',
  ),
}
