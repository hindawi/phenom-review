const initialize = ({ repos, models, transaction }) => ({
  async execute({ submissionId, peerReviewEditorialModel }) {
    const { PeerReviewEditorialMappingRepo } = repos
    const peerReviewEditorialMappingRepo = new PeerReviewEditorialMappingRepo()
    let peerReviewEditorialMapping

    const trx = await transaction.start(models.Manuscript.knex())
    try {
      peerReviewEditorialMapping = await peerReviewEditorialMappingRepo.createPeerReviewEditorialMapping(
        submissionId,
        peerReviewEditorialModel,
        trx,
      )
      await trx.commit()
    } catch (err) {
      await trx.rollback()
      throw err
    }
    return peerReviewEditorialMapping
  },
})

const authsomePolicies = ['isAuthenticated']

export { initialize, authsomePolicies }
