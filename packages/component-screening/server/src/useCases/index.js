const approveEQSUseCase = require('./approveEQS')
const declineEQSUseCase = require('./declineEQS')
const returnToDraftUseCase = require('./returnToDraft')
const voidManuscriptUseCase = require('./voidManuscript')
const startPeerReviewUseCase = require('./startPeerReview')
const declineEQSByEventUseCase = require('./declineEQSByEvent')
const assignTriageEditorOnManuscriptUseCase = require('./assignTriageEditorOnManuscript')
const setPeerReviewEditorialMappingUseCase = require('./setPeerReviewEditorialMapping')

module.exports = {
  approveEQSUseCase,
  declineEQSUseCase,
  returnToDraftUseCase,
  voidManuscriptUseCase,
  startPeerReviewUseCase,
  declineEQSByEventUseCase,
  setPeerReviewEditorialMappingUseCase,
  assignTriageEditorOnManuscriptUseCase,
}
