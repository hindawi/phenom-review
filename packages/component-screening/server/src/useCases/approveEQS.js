const initialize = ({
  models,
  logEvent,
  useCases,
  eventsService,
  notificationService,
  inviteAcademicEditorService,
  getUserWithWorkloadUseCase,
  getUserWithWorkloadForEditorialConflictsUseCase,
}) => ({
  async execute({ manuscriptId, userId, input }) {
    const {
      User,
      Team,
      Journal,
      TeamMember,
      Manuscript,
      SpecialIssue,
      PeerReviewModel,
    } = models
    const { token } = input

    const manuscript = await Manuscript.find(
      manuscriptId,
      '[teams.members, articleType]',
    )

    const journal = await Journal.find(
      manuscript.journalId,
      '[teams.members, peerReviewModel]',
    )
    const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )
    manuscript.journal = journal

    const submittingAuthor = manuscript.getSubmittingAuthor()

    if (manuscript.hasPassedEqs !== null) {
      throw new ValidationError('Manuscript already handled.')
    }

    if (token !== manuscript.technicalCheckToken) {
      throw new Error('Invalid token.')
    }

    const hasSpecialIssueEditorialConflictOfInterest = await manuscript.getHasSpecialIssueEditorialConflictOfInterest(
      models,
    )

    manuscript.updateProperties({
      hasPassedEqs: true,
      technicalCheckToken: null,
      status: Manuscript.Statuses.submitted,
    })
    await manuscript.save()

    let peerReviewModel
    if (manuscript.specialIssueId) {
      peerReviewModel = await PeerReviewModel.findOneBySpecialIssue(
        manuscript.specialIssueId,
      )
    } else {
      peerReviewModel = await PeerReviewModel.findOneByManuscript(manuscriptId)
    }

    logEvent({
      userId: null,
      manuscriptId,
      action: logEvent.actions.eqs_approved,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })

    if (peerReviewModel.hasTriageEditor) {
      const getEligibleUserUseCase = hasSpecialIssueEditorialConflictOfInterest
        ? await getUserWithWorkloadForEditorialConflictsUseCase.initialize(
            models,
          )
        : await getUserWithWorkloadUseCase.initialize(models)

      await useCases.assignTriageEditorOnManuscriptUseCase
        .initialize({
          eventsService,
          getEligibleUserUseCase,
          models: { TeamMember, Team, User, SpecialIssue, Manuscript },
        })
        .execute({
          manuscriptId,
          submissionId: manuscript.submissionId,
          journalId: journal.id,
          sectionId: manuscript.sectionId,
          specialIssueId: manuscript.specialIssueId,
          hasSpecialIssueEditorialConflictOfInterest: await manuscript.getHasSpecialIssueEditorialConflictOfInterest(
            models,
          ),
        })
    }

    if (peerReviewModel.academicEditorAutomaticInvitation) {
      const academicEditor = await getUserWithWorkloadUseCase
        .initialize(models)
        .execute({
          journalId: journal.id,
          specialIssueId: manuscript.specialIssueId,
          sectionId: manuscript.sectionId,
          role: Team.Role.academicEditor,
          teamMemberStatuses: [
            TeamMember.Statuses.pending,
            TeamMember.Statuses.accepted,
          ],
          manuscriptStatuses: Manuscript.InProgressStatuses,
        })

      if (academicEditor) {
        await inviteAcademicEditorService.execute({
          submissionId: manuscript.submissionId,
          userId: academicEditor.userId,
          reqUserId: userId,
          hasWorkloadAssignment: true,
          useCommunication: false,
        })
      }
    }

    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      journalId: journal.id,
      manuscriptId: manuscript.id,
      sectionId: manuscript.sectionId,
    })
    if (triageEditor) {
      triageEditor.user = await User.find(triageEditor.userId)
      notificationService.notifyApprovalEditor({
        manuscript,
        submittingAuthor,
        editorialAssistant,
        journalName: journal.name,
        approvalEditor: triageEditor,
      })
    }
  },
})

const authsomePolicies = ['isEditorialAssistant', 'admin']

module.exports = {
  initialize,
  authsomePolicies,
}
