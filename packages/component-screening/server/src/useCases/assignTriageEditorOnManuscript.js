const initialize = ({
  eventsService,
  getEligibleUserUseCase,
  models: { TeamMember, Team, User, SpecialIssue, Manuscript },
}) => {
  const execute = async ({
    journalId,
    sectionId,
    manuscriptId,
    submissionId,
    specialIssueId,
  }) => {
    const triageEditor = await getEligibleUserUseCase.execute({
      journalId,
      sectionId,
      specialIssueId,
      manuscriptStatuses: getManuscriptStatusesWhenTEHasToDoWork(),
      role: Team.Role.triageEditor,
      models: { TeamMember, SpecialIssue },
      teamMemberStatuses: [TeamMember.Statuses.active],
    })

    if (!triageEditor) return

    const queryObject = {
      manuscriptId,
      role: Team.Role.triageEditor,
    }
    const triageEditorTeam = await Team.findOrCreate({
      queryObject,
      options: queryObject,
    })
    const triageEditorUser = await User.find(triageEditor.userId, 'identities')

    const manuscriptTriageEditor = triageEditorTeam.addMember({
      user: triageEditorUser,
      options: {
        status: TeamMember.Statuses.active,
      },
    })
    await manuscriptTriageEditor.save()
    await triageEditorTeam.save()

    eventsService.publishSubmissionEvent({
      submissionId,
      eventName: 'SubmissionTriageEditorAssigned',
    })
  }

  const getManuscriptStatusesWhenTEHasToDoWork = () => {
    const statusesForAllPeerReviewModels = [
      Manuscript.Statuses.submitted,
      Manuscript.Statuses.academicEditorInvited,
    ]
    const statusesOnlyForChiefMinus = [Manuscript.Statuses.pendingApproval]

    return [...statusesForAllPeerReviewModels, ...statusesOnlyForChiefMinus]
  }

  return {
    execute,
  }
}

module.exports = {
  initialize,
}
