const initialize = ({ Manuscript, logEvent }) => ({
  async execute(submissionId) {
    const manuscript = await Manuscript.findOneBy({
      queryObject: { submissionId },
    })

    if (!manuscript) {
      throw new Error('Manuscript not found.')
    }

    if (manuscript.hasPassedEqs !== null) {
      throw new ValidationError('Manuscript already handled.')
    }

    manuscript.updateProperties({
      hasPassedEqs: false,
      technicalCheckToken: null,
      status: Manuscript.Statuses.refusedToConsider,
    })
    await manuscript.save()

    logEvent({
      userId: null,
      manuscriptId: manuscript.id,
      action: logEvent.actions.eqs_declined,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

module.exports = {
  initialize,
  authsomePolicies: [],
}
