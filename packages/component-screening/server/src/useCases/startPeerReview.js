const initialize = ({
  models,
  logEvent,
  useCases,
  eventsService,
  notificationService,
  getUserWithWorkloadUseCase,
  getUserWithWorkloadForEditorialConflictsUseCase,
}) => ({
  async execute({ data }) {
    const { submissionId, editorialModel } = data
    const { Team, User, Journal, Manuscript, TeamMember, SpecialIssue } = models

    const manuscript = await Manuscript.findOneBy({
      queryObject: { submissionId },
      eagerLoadRelations: '[teams.members, articleType]',
    })
    if (manuscript.hasPassedEqs !== null) {
      throw new ValidationError('Manuscript already handled.')
    }

    const journal = await Journal.find(manuscript.journalId, '[teams.members]')
    manuscript.journal = journal

    const hasSpecialIssueEditorialConflictOfInterest = await manuscript.getHasSpecialIssueEditorialConflictOfInterest(
      models,
    )

    manuscript.updateProperties({
      hasPassedEqs: true,
      technicalCheckToken: null,
      status: Manuscript.Statuses.submitted,
    })
    await manuscript.save()

    await logEvent({
      userId: null,
      manuscriptId: manuscript.id,
      action: logEvent.actions.eqs_approved,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })

    const isTriageEditorEnabled =
      editorialModel && editorialModel.triageEditor.enabled
    if (isTriageEditorEnabled) {
      const getEligibleUserUseCase = hasSpecialIssueEditorialConflictOfInterest
        ? await getUserWithWorkloadForEditorialConflictsUseCase.initialize(
            models,
          )
        : await getUserWithWorkloadUseCase.initialize(models)

      await useCases.assignTriageEditorOnManuscriptUseCase
        .initialize({
          eventsService,
          getEligibleUserUseCase,
          models: { TeamMember, Team, User, SpecialIssue, Manuscript },
        })
        .execute({
          submissionId,
          manuscriptId: manuscript.id,
          journalId: journal.id,
          sectionId: manuscript.sectionId,
          specialIssueId: manuscript.specialIssueId,
        })

      await notifyTriageEditor({
        User,
        Team,
        TeamMember,
        manuscript,
        journalName: journal.name,
        notificationService,
      })
    }
  },
})

const notifyTriageEditor = async ({
  User,
  Team,
  TeamMember,
  manuscript,
  journalName,
  notificationService,
}) => {
  const triageEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus({
    manuscriptId: manuscript.id,
    role: Team.Role.triageEditor,
    status: TeamMember.Statuses.active,
  })
  if (!triageEditor) return

  const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
    {
      manuscriptId: manuscript.id,
      role: Team.Role.editorialAssistant,
      status: TeamMember.Statuses.active,
    },
  )
  const submittingAuthor = manuscript.getSubmittingAuthor()
  triageEditor.user = await User.find(triageEditor.userId)

  notificationService.notifyApprovalEditor({
    manuscript,
    submittingAuthor,
    editorialAssistant,
    journalName,
    approvalEditor: triageEditor,
  })
}

export { initialize }
