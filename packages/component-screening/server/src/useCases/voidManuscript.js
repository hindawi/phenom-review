const initialize = ({ logEvent, models: { Manuscript } }) => ({
  async execute({ data }) {
    const { submissionId } = data

    const manuscripts = await Manuscript.findBy({
      submissionId,
    })

    manuscripts.forEach(m => (m.status = Manuscript.Statuses.void))
    await Manuscript.updateMany(manuscripts)

    // Activity logger
    manuscripts.sort(Manuscript.compareVersion)
    const lastManuscript = manuscripts[manuscripts.length - 1]

    logEvent({
      userId: null,
      manuscriptId: lastManuscript.id,
      action: logEvent.actions.manuscript_voided,
      objectType: logEvent.objectType.manuscript,
      objectId: lastManuscript.id,
    })
  },
})

module.exports = {
  initialize,
}
