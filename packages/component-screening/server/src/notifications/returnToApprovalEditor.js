const initialize = ({ Email, getEmailCopyService, getPropsService }) => ({
  async notifyApprovalEditor({
    comments,
    manuscript,
    journalName,
    approvalEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { customId, title } = manuscript
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const submittingAuthorName = submittingAuthor.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      comments,
      emailType: 'approval-editor-manuscript-returned-by-eqa',
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
    })
    const emailProps = getPropsService.getProps({
      type: 'system',
      toUser: approvalEditor,
      subject: `${customId}: Manuscript returned with comments`,
      paragraph,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      manuscript,
      journalName,
      signatureName: editorialAssistant.getName(),
    })
    emailProps.bodyProps = bodyProps

    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = {
  initialize,
}
