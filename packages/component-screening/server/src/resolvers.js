const config = require('config')
const models = require('@pubsweet/models')
const Email = require('component-sendgrid')
const { logEvent } = require('component-activity-log/server')
const { getModifiedText } = require('component-transform-text')
const { withAuthsomeMiddleware } = require('helper-service')
const events = require('component-events')

const urlService = require('./urlService/urlService')
const useCases = require('./useCases')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const approveEQSNotifications = require('./notifications/approveEQS')
const getEmailCopy = require('./notifications/getEmailCopy')
const getProps = require('./notifications/getProps')

const {
  useCases: { inviteAcademicEditorUseCase },
  dependencies: {
    jobsService,
    invitations,
    emailPropsService,
    emailJobs: {
      academicEditor: { emailJobs },
    },
    removalJobs: {
      academicEditor: { academicEditorRemovalJobs },
    },
  },
} = require('component-peer-review')

const inviteAcademicEditorDependencies = {
  invitations,
  emailJobs,
  removalJobs: academicEditorRemovalJobs,
  getProps: emailPropsService,
  jobs: jobsService,
}

const {
  useCases: {
    getUserWithWorkloadUseCase,
    getUserWithWorkloadForEditorialConflictsUseCase,
  },
} = require('component-model')

const resolvers = {
  Query: {},
  Mutation: {
    async approveEQS(_, { manuscriptId, input }, ctx) {
      const eventsService = events.initialize({ models })
      const getEmailCopyService = getEmailCopy.initialize()
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const notificationService = approveEQSNotifications.initialize({
        Email,
        getEmailCopyService,
        getPropsService,
      })

      // #region inviteAcademicEditor
      const { Manuscript, TeamMember, Job } = models
      const invitationsService = inviteAcademicEditorDependencies.invitations.initialize(
        { Email },
      )
      const getPropsServiceInviteAcademicEditor = inviteAcademicEditorDependencies.getProps.initialize(
        {
          baseUrl,
          urlService,
          footerText,
          unsubscribeSlug,
          getModifiedText,
        },
      )
      const emailJobsService = inviteAcademicEditorDependencies.emailJobs.initialize(
        {
          Job,
          Email,
          logEvent,
          getPropsService: getPropsServiceInviteAcademicEditor,
        },
      )
      const removalJobsService = inviteAcademicEditorDependencies.removalJobs.initialize(
        {
          Job,
          logEvent,
          TeamMember,
          Manuscript,
        },
      )
      const jobsService = inviteAcademicEditorDependencies.jobs.initialize({
        models,
      })

      const inviteAcademicEditorService = inviteAcademicEditorUseCase.initialize(
        {
          models,
          logEvent,
          jobsService,
          eventsService,
          invitationsService,
          emailJobsService,
          removalJobsService,
        },
      )
      // #endregion

      return useCases.approveEQSUseCase
        .initialize({
          models,
          logEvent,
          useCases,
          eventsService,
          notificationService,
          inviteAcademicEditorService,
          getUserWithWorkloadUseCase,
          getUserWithWorkloadForEditorialConflictsUseCase,
        })
        .execute({ manuscriptId, input, userId: ctx.user })
    },
    async declineEQS(_, { manuscriptId, input }, ctx) {
      return useCases.declineEQSUseCase
        .initialize({ models, logEvent })
        .execute({ manuscriptId, input, userId: ctx.user })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
