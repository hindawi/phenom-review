const {
  getTeamRoles,
  generateJournal,
  generateManuscript,
  generateTeamMember,
  getTeamMemberStatuses,
  getManuscriptStatuses,
  generateUser,
} = require('component-generators')

const { startPeerReviewUseCase } = require('../src/useCases')

let models = {}
let Journal = {}
let Manuscript = {}
let TeamMember = {}
let User = {}

const logEvent = () => jest.fn(async () => {})
logEvent.actions = { eqs_approved: 'eqs_approved' }
logEvent.objectType = { manuscript: 'manuscript' }

const useCases = {
  assignTriageEditorOnManuscriptUseCase: {
    initialize: jest.fn(() => ({
      execute: jest.fn(),
    })),
  },
}
const eventsService = {}
const notificationService = { notifyApprovalEditor: jest.fn() }
const getUserWithWorkloadUseCase = { initialize: jest.fn() }
const getUserWithWorkloadForEditorialConflictsUseCase = {
  initialize: jest.fn(),
}

describe('startPeerReview', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    models = {
      Journal: {
        find: jest.fn(),
      },
      Manuscript: {
        Statuses: getManuscriptStatuses(),
        findOneBy: jest.fn(),
      },
      Team: {
        Role: getTeamRoles(),
      },
      TeamMember: {
        Statuses: getTeamMemberStatuses(),
        findTriageEditor: jest.fn(),
        findOneByManuscriptAndRoleAndStatus: jest.fn(),
      },
      User: {
        find: jest.fn(),
      },
    }
    ;({ Journal, Manuscript, TeamMember, User } = models)
  })
  it('Throws an error if the manuscript is already handled', async () => {
    const data = {}
    const manuscript = generateManuscript({ hasPassedEqs: true })

    jest.spyOn(Manuscript, 'findOneBy').mockResolvedValue(manuscript)

    const res = startPeerReviewUseCase.initialize({ models }).execute({ data })

    return expect(res).rejects.toThrow('Manuscript already handled.')
  })
  it('Executes the main flow when the Editorial Model requires a triage editor', async () => {
    const data = { editorialModel: { triageEditor: { enabled: true } } }
    const journal = generateJournal()
    const manuscript = generateManuscript({
      hasPassedEqs: null,
      getSubmittingAuthor: jest.fn(),
      getHasSpecialIssueEditorialConflictOfInterest: jest.fn(),
    })
    const teamMember = generateTeamMember()
    const user = generateUser()

    jest.spyOn(Manuscript, 'findOneBy').mockResolvedValue(manuscript)
    jest.spyOn(Journal, 'find').mockResolvedValue(journal)
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue(teamMember)

    jest.spyOn(User, 'find').mockResolvedValue(user)

    await startPeerReviewUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        eventsService,
        notificationService,
        getUserWithWorkloadUseCase,
        getUserWithWorkloadForEditorialConflictsUseCase,
      })
      .execute({ data })

    expect(
      useCases.assignTriageEditorOnManuscriptUseCase.initialize,
    ).toHaveBeenCalledTimes(1)
    expect(notificationService.notifyApprovalEditor).toHaveBeenCalledTimes(1)
  })

  it(`Executes the main flow when the Editorial Model doesn't require a triage editor`, async () => {
    const data = { editorialModel: { triageEditor: { enabled: false } } }
    const journal = generateJournal()
    const manuscript = generateManuscript({
      hasPassedEqs: null,
      getSubmittingAuthor: jest.fn(),
      getHasSpecialIssueEditorialConflictOfInterest: jest.fn(),
    })
    const teamMember = generateTeamMember()
    const user = generateUser()

    jest.spyOn(Manuscript, 'findOneBy').mockResolvedValue(manuscript)
    jest.spyOn(Journal, 'find').mockResolvedValue(journal)
    jest
      .spyOn(TeamMember, 'findOneByManuscriptAndRoleAndStatus')
      .mockResolvedValue(teamMember)

    jest.spyOn(User, 'find').mockResolvedValue(user)

    await startPeerReviewUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        eventsService,
        notificationService,
        getUserWithWorkloadUseCase,
        getUserWithWorkloadForEditorialConflictsUseCase,
      })
      .execute({ data })

    expect(
      useCases.assignTriageEditorOnManuscriptUseCase.initialize,
    ).toHaveBeenCalledTimes(0)
    expect(notificationService.notifyApprovalEditor).toHaveBeenCalledTimes(0)
  })
})
