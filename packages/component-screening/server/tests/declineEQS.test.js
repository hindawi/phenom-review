const Chance = require('chance')

const { models, fixtures } = require('fixture-service')
const { declineEQSUseCase } = require('../src/useCases')

const chance = new Chance()
const logEvent = () => jest.fn(async () => {})
const sendPackage = () => jest.fn(async () => {})
logEvent.actions = { eqs_declined: 'eqs_declined' }
logEvent.objectType = { manuscript: 'manuscript' }
describe('Decline EQS use case', () => {
  it('sets hasEQS to true', async () => {
    const { Manuscript } = models
    const token = chance.guid()
    const manuscript = fixtures.generateManuscript({
      properties: { technicalCheckToken: token, hasPassedEqs: null },
      Manuscript,
    })

    await declineEQSUseCase
      .initialize({ models, logEvent, sendPackage })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          token,
        },
      })

    expect(manuscript.hasPassedEqs).toBeFalsy()
    expect(manuscript.technicalCheckToken).toBeNull()
    expect(manuscript.status).toEqual(Manuscript.Statuses.refusedToConsider)
  })

  it('returns an error when the token is invalid', async () => {
    const { Manuscript } = models
    const manuscript = fixtures.generateManuscript({
      properties: { technicalCheckToken: chance.guid(), hasPassedEqs: null },
      Manuscript,
    })

    const result = declineEQSUseCase
      .initialize({ models, logEvent, sendPackage })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          token: chance.guid(),
        },
      })

    return expect(result).rejects.toThrow('Invalid token.')
  })

  it('returns an error if the manuscript has already passed/failed EQS', async () => {
    const { Manuscript } = models
    const token = chance.guid()
    const manuscript = fixtures.generateManuscript({
      properties: {
        technicalCheckToken: token,
        hasPassedEqs: chance.pickone([false, true]),
      },
      Manuscript,
    })

    const result = declineEQSUseCase
      .initialize({ models, logEvent, sendPackage })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          token,
        },
      })

    return expect(result).rejects.toThrow('Manuscript already handled.')
  })
})
