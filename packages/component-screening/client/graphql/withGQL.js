/* eslint-disable */
import { graphql } from 'react-apollo'
import { compose } from 'recompose'
import { queries } from 'component-activity-log/client'
import { queries as manuscriptDetailsQueries } from 'component-peer-review/client'

import * as mutations from './mutations'
import { parseSearchParams } from '../utils'

const mutationOptions = ({ location }) => {
  const { submissionId } = parseSearchParams(location.search)
  return {
    refetchQueries: [
      {
        query: queries.getAuditLogs,
        variables: { submissionId },
      },
      {
        query: manuscriptDetailsQueries.getSubmission,
        variables: { submissionId },
      },
    ],
  }
}

export default compose(
  graphql(mutations.declineEQS, {
    name: 'declineEQS',
    options: mutationOptions,
  }),
  graphql(mutations.approveEQS, {
    name: 'approveEQS',
    options: mutationOptions,
  }),
)
