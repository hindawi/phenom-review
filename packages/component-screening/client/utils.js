import { last } from 'lodash'

export const parseSearchParams = url => {
  const params = new URLSearchParams(url)
  const parsedObject = {}
  /* eslint-disable */
  for (let [key, value] of params) {
    parsedObject[key] = value
  }
  /* eslint-enable */
  return parsedObject
}

export const parseError = e => last(e.message.split(':')).trim()
