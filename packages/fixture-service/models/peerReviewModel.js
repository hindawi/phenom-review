const Chance = require('chance')

const chance = new Chance()
const fixtures = require('../fixtures')

const { findMock, findOneByMock } = require('./repositoryMocks')

class PeerReviewModel {
  constructor(props) {
    this.id = chance.guid()
    this.created = props.created || Date.now()
    this.updated = props.updated || Date.now()
    this.name = props.name || chance.word()
    this.approvalEditors = props.approvalEditors || []
    this.hasFigureheadEditor = props.hasFigureheadEditor
    this.figureheadEditorLabel = props.figureheadEditorLabel || ''
    this.hasSections = props.hasSections
    this.hasTriageEditor = props.hasTriageEditor
    this.triageEditorLabel = props.triageEditorLabel || ''
    this.triageEditorAssignmentTool = props.triageEditorAssignmentTool || [
      'manual',
    ]
    this.academicEditorLabel = props.academicEditorLabel || ''
    this.academicEditorAutomaticInvitation =
      props.academicEditorAutomaticInvitation || false
    this.reviewerAssignmentTool = props.reviewerAssignmentTool || ['manual']
  }

  static find = id => findMock(id, 'peerReviewModels', fixtures)
  static findOneBy = values =>
    findOneByMock(values, 'peerReviewModels', fixtures)

  static async findOneByManuscript(manuscriptId) {
    const manuscript = fixtures.manuscripts.find(m => m.id === manuscriptId)
    const section = fixtures.sections.find(s => s.id === manuscript.sectionId)
    const journalId = section ? section.journalId : manuscript.journalId

    const journal = fixtures.journals.find(j => j.id === journalId)
    return fixtures.peerReviewModels.find(
      prm => prm.id === journal.peerReviewModelId,
    )
  }

  static async findOneBySpecialIssue(specialIssueId) {
    const specialIssue = fixtures.specialIssues.find(
      specialIssues => specialIssues.id === specialIssueId,
    )
    return fixtures.peerReviewModels.find(
      prm => prm.id === specialIssue.peerReviewModelId,
    )
  }

  static async findOneByJournal(journalId) {
    const journal = fixtures.journals.find(journal => journal.id === journalId)
    return fixtures.peerReviewModels.find(
      prm => prm.id === journal.peerReviewModelId,
    )
  }

  static async findOneByManuscriptParent({ journalId, specialIssueId }) {
    if (specialIssueId) {
      return this.findOneBySpecialIssue(specialIssueId)
    }
    return this.findOneByJournal(journalId)
  }

  async save() {
    return Promise.resolve(this)
  }

  toDTO() {
    return {
      ...this,
    }
  }
}

module.exports = PeerReviewModel
