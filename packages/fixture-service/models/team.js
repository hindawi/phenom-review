const { assign, remove, filter, flatMap, find } = require('lodash')
const Chance = require('chance')

const chance = new Chance()
const fixtures = require('../fixtures')

const { teams } = fixtures
const { findMock, findInMock, findOneByMock } = require('./repositoryMocks')
const TeamMember = require('./teamMember')

class Team {
  constructor(props) {
    this.id = chance.guid()
    this.role = props.role || null
    this.manuscriptId = props.manuscriptId || null
    this.journalId = props.journalId || null
    this.sectionId = props.sectionId || null
    this.members = props.members || []
    this.specialIssueId = props.specialIssueId || null
  }

  static get Role() {
    return {
      author: 'author',
      admin: 'admin',
      triageEditor: 'triageEditor',
      reviewer: 'reviewer',
      academicEditor: 'academicEditor',
      editorialAssistant: 'editorialAssistant',
      researchIntegrityPublishingEditor: 'researchIntegrityPublishingEditor',
    }
  }

  static get JournalRoles() {
    return [
      this.Role.triageEditor,
      this.Role.academicEditor,
      this.Role.editorialAssistant,
    ]
  }

  static get StaffRoles() {
    return [this.Role.admin, this.Role.researchIntegrityPublishingEditor]
  }

  static async findOneByUserAndJournalId({ userId, journalId }) {
    let teams

    if (journalId) teams = filter(fixtures.teams, { journalId })
    if (userId) teams = filter(fixtures.teams, t => find(t.members, { userId }))

    if (!teams[0]) return []
    return teams
  }

  static async findAllBy({ role, manuscriptId, submissionId }) {
    let teams
    if (manuscriptId) {
      teams = filter(fixtures.teams, { role, manuscriptId })
    }
    if (submissionId) {
      const manuscripts = filter(fixtures.manuscripts, { submissionId })
      const manuscriptTeams = flatMap(manuscripts, 'teams')
      teams = filter(manuscriptTeams, { role })
    }

    if (!teams[0]) return []

    return teams
  }

  static async findAllBySubmissionAndRole({ role, submissionId }) {
    const manuscripts = filter(fixtures.manuscripts, { submissionId })
    const manuscriptTeams = flatMap(manuscripts, 'teams')
    const teams = filter(manuscriptTeams, { role })

    if (!teams[0]) return []

    return teams
  }

  static findOrCreate = async ({
    queryObject,
    eagerLoadRelations,
    options,
  }) => {
    let team = await this.findOneBy({
      queryObject,
      eagerLoadRelations,
    })

    if (!team) {
      team = new Team(options)
      await team.save()
    }

    return team
  }
  static findOneBy = values => findOneByMock(values, 'teams', fixtures)
  static find = id => findMock(id, 'teams', fixtures)
  static findIn = (field, options) =>
    findInMock(field, options, 'teams', fixtures)
  static query = () => ({
    upsertGraph: obj =>
      Array.isArray(obj) ? obj.map(i => i.save()) : obj.save(),
  })

  static async findAllByJournal({ journalId }) {
    return filter(fixtures.teams, { journalId })
  }

  static async findAllBySection({ sectionId }) {
    return filter(fixtures.teams, { sectionId })
  }

  static async findAllBySpecialIssue({ specialIssueId }) {
    return filter(fixtures.teams, { specialIssueId })
  }

  static async findOneByJournalAndRole({ journalId, role }) {
    return find(fixtures.teams, { journalId, role })
  }

  static async findOneByManuscriptAndRole({ manuscriptId, role }) {
    return find(fixtures.teams, { manuscriptId, role })
  }

  static knex() {
    return this
  }

  static async query() {
    return {
      upsertGraph: jest.fn(),
    }
  }

  addMember({ user, options }) {
    this.members = this.members || []
    const existingMember = this.members.find(
      member => member.userId === user.id,
    )
    const { defaultIdentity } = user

    if (existingMember) {
      if (existingMember.status === TeamMember.Statuses.removed) {
        throw new Error(
          `${this.role} invitation for ${defaultIdentity.email} was removed and can't be invited again`,
        )
      }
      if (existingMember.status === TeamMember.Statuses.expired) {
        existingMember.updateProperties({
          status: TeamMember.Statuses.pending,
        })
        return existingMember
      }
      throw new ValidationError(
        `User ${defaultIdentity.email} is already invited as ${this.role}`,
      )
    }

    const newMember = new TeamMember({
      ...options,
      userId: user.id,
      teamId: this.id,
      position: this.members.length,
    })
    newMember.setAlias(defaultIdentity)
    newMember.user = user
    this.members.push(newMember)

    return newMember
  }

  async findCorrespondingTeamMember() {
    return fixtures.teamMembers.find(
      tm => tm.teamId === this.id && tm.isCorresponding === true,
    )
  }

  async changeCorrespondingTeamMember(newCorrespondingTeamMember) {
    const correspondingTeamMember = this.findCorrespondingTeamMember()

    correspondingTeamMember.isCorresponding = false

    newCorrespondingTeamMember.isCorresponding = true
    return newCorrespondingTeamMember
  }

  async save() {
    const existingTeam = teams.find(t => t.id === this.id)
    if (existingTeam) {
      assign(existingTeam, this)
    } else {
      teams.push(this)
    }
    return Promise.resolve(this)
  }
  async saveRecursively() {
    await this.save()
    if (this.members.length === 0) return

    await Promise.all(this.members.map(async member => member.save()))
  }

  async saveGraph() {
    await this.save()
    if (this.members.length === 0) return

    await Promise.all(this.members.map(async member => member.save()))
  }

  async delete() {
    remove(teams, f => f.id === this.id)
  }
}

module.exports = Team
