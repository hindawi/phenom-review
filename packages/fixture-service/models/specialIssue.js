const { assign, filter } = require('lodash')
const Chance = require('chance')

const chance = new Chance()
const fixtures = require('../fixtures')

const { specialIssues } = fixtures
const { findMock, findAllMock, findOneByMock } = require('./repositoryMocks')

class SpecialIssue {
  constructor(props) {
    this.id = chance.guid()
    this.name = props.name || chance.sentence({ words: 5 })
    this.created = props.created || Date.now()
    this.startDate = props.activationDate || Date.now()
    this.endDate = props.endDate || Date.now()
    this.isActive = props.isActive === undefined ? false : props.isActive
    this.journalId = props.journalId || ''
    this.sectionId = props.sectionId || ''
    this.callForPapers = props.callForPapers || ''
    this.isCancelled = props.isCancelled || false
    this.cancelReason = props.cancelReason || null
    this.customId = chance.integer({ min: 100000, max: 999999 }).toString()
    this.peerReviewModelId = props.peerReviewModelId || null
  }

  static find = id => findMock(id, 'specialIssues', fixtures)
  static findOneBy = values => findOneByMock(values, 'specialIssues', fixtures)
  static findAll = ({ orderByField, order, queryObject }) =>
    findAllMock('specialIssues', fixtures, orderByField, order, queryObject)

  static findUnique = name => {
    const foundSpecialIssue = specialIssues.find(
      si => si.name.toLowerCase() === name.toLowerCase(),
    )

    return Promise.resolve(foundSpecialIssue)
  }

  static async findAllByJournal({ journalId }) {
    return filter(fixtures.specialIssues, { journalId })
  }
  static async findAllBySection({ sectionId }) {
    return filter(fixtures.specialIssues, { sectionId })
  }
  async save() {
    const existingSpecialIssue = specialIssues.find(si => si.id === this.id)

    if (existingSpecialIssue) {
      assign(existingSpecialIssue, this)
    } else {
      specialIssues.push(this)
    }
    return Promise.resolve(this)
  }

  updateProperties(properties) {
    assign(this, properties)
    return this
  }

  toDTO() {
    return {
      ...this,
    }
  }
  static async findAllThatShouldBeActive(date) {
    return fixtures.specialIssues.filter(
      si => si.startDate < date && si.endDate > date,
    )
  }

  static updateMany = async specialIssues => {
    await Promise.all(specialIssues.map(async m => m.save()))
  }

  async cancelJobByType({ Job, jobType }) {
    const specialIssueJob = await Job.findOneBy({
      queryObject: {
        jobType,
        specialIssueId: this.id,
      },
    })
    if (!specialIssueJob) {
      return
    }
    Job.cancel(specialIssueJob.id)
    specialIssueJob.delete()
  }

  async handleActivation({
    adminId,
    oldEndDate,
    jobsService,
    oldStartDate,
    models: { Job },
    dateService: { isDateToday, areDatesEqual, isDateInTheFuture },
  }) {
    if (!areDatesEqual(oldStartDate, this.startDate)) {
      await this.cancelJobByType({ Job, jobType: Job.Type.activation })
    }

    if (
      isDateToday(this.startDate) ||
      (!areDatesEqual(oldEndDate, this.endDate) &&
        isDateInTheFuture(this.endDate) &&
        !isDateInTheFuture(this.startDate))
    ) {
      this.updateProperties({ isActive: true, isCancelled: false })
      return
    }

    if (
      isDateInTheFuture(this.startDate) &&
      !areDatesEqual(oldStartDate, this.startDate)
    ) {
      jobsService.scheduleSpecialIssueActivation({
        specialIssue: this,
        teamMemberId: adminId,
      })
    }
  }
}

module.exports = SpecialIssue
