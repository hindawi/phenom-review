const Chance = require('chance')

const chance = new Chance()
const fixtures = require('../fixtures')

const { allMock, findMock } = require('./repositoryMocks')

class ArticleType {
  constructor(props) {
    this.id = chance.guid()
    this.name = props.name || ''
    this.created = props.created || Date.now()
    this.hasPeerReview =
      props.hasPeerReview === undefined ? false : props.hasPeerReview
  }

  static get Types() {
    return {
      editorial: 'Editorial',
      retraction: 'Retraction',
      caseReport: 'Case Report',
      caseSeries: 'Case Series',
      reviewArticle: 'Review Article',
      researchArticle: 'Research Article',
      letterToTheEditor: 'Letter to the Editor',
      expressionOfConcern: 'Expression of Concern',
      erratum: 'Erratum',
      corrigendum: 'Corrigendum',
    }
  }

  static get TypesWithRIPE() {
    return [
      this.Types.retraction,
      this.Types.expressionOfConcern,
      this.Types.erratum,
      this.Types.corrigendum,
    ]
  }

  static get EditorialTypes() {
    return [this.Types.editorial, this.Types.letterToTheEditor]
  }

  static async findArticleTypeByManuscript(manuscriptId) {
    const manuscript = fixtures.manuscripts.find(m => m.id === manuscriptId)
    return fixtures.articleTypes.find(at => at.id === manuscript.articleTypeId)
  }

  static all = () => allMock('articleTypes', fixtures)
  static find = id => findMock(id, 'articleTypes', fixtures)

  async save() {
    return Promise.resolve(this)
  }

  toDTO() {
    return {
      ...this,
    }
  }
}

module.exports = ArticleType
