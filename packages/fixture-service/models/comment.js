const { assign, remove } = require('lodash')
const Chance = require('chance')

const chance = new Chance()
const fixtures = require('../fixtures')

const { comments } = fixtures
const { findByMock, findOneByMock, findMock } = require('./repositoryMocks')

class Comment {
  constructor(props) {
    this.id = chance.guid()
    this.reviewId = props.reviewId
    this.content = props.content
    this.type = props.type
    this.files = props.files
  }

  static findBy = values => findByMock(values, 'comments', fixtures)
  static find = id => findMock(id, 'comments', fixtures)
  static findOneBy = values => findOneByMock(values, 'comments', fixtures)
  static async findOneByType({ reviewId, type }) {
    return comments.find(c => c.reviewId === reviewId && c.type === type)
  }

  static get Types() {
    return {
      public: 'public',
      private: 'private',
    }
  }

  async save() {
    const existingComment = comments.find(m => m.id === this.id)
    if (existingComment) {
      assign(existingComment, this)
    } else {
      comments.push(this)
    }
    return Promise.resolve(this)
  }

  async delete() {
    remove(comments, comm => comm.id === this.id)
  }

  updateProperties(properties) {
    assign(this, properties)
    return this
  }

  toDTO() {
    return { ...this }
  }
}

module.exports = Comment
