const { assign, remove, last, orderBy } = require('lodash')
const Chance = require('chance')

const chance = new Chance()
const fixtures = require('../fixtures')

const { reviews } = fixtures
const { findMock, findOneByMock, findByMock } = require('./repositoryMocks')

class Review {
  constructor(props) {
    this.id = chance.guid()
    this.manuscriptId = props.manuscriptId
    this.teamMemberId = props.teamMemberId
    this.submitted = props.submitted || new Date().toISOString()
    this.comments = props.comments || []
    this.recommendation = props.recommendation || []
    this.isValid = props.isValid || true
  }

  static get Recommendations() {
    return {
      minor: 'minor',
      major: 'major',
      reject: 'reject',
      publish: 'publish',
      responseToRevision: 'responseToRevision',
    }
  }

  static find = id => findMock(id, 'reviews', fixtures)
  static findBy = values => findByMock(values, 'reviews', fixtures)
  static findOneBy = values => findOneByMock(values, 'reviews', fixtures)

  static findOneInvalidByManuscriptAndRole({ role, manuscriptId }) {
    const team = fixtures.teams.find(
      t => t.role === role && t.manuscriptId === manuscriptId,
    )
    const teamMembers = fixtures.teamMembers.filter(tm => tm.teamId === team.id)
    const teamMembersIds = teamMembers.map(tm => tm.id)
    return fixtures.reviews.find(
      review =>
        review.isValid === false &&
        review.manuscriptId === manuscriptId &&
        teamMembersIds.includes(review.teamMemberId),
    )
  }

  static async findLatestEditorialReview({ manuscriptId, TeamRole }) {
    const editorialReviews = fixtures.reviews.filter(review => {
      if (!review.member) return false

      return (
        [
          TeamRole.admin,
          TeamRole.triageEditor,
          TeamRole.academicEditor,
          TeamRole.editorialAssistant,
        ].includes(review.member.team.role) &&
        review.manuscriptId === manuscriptId
      )
    })

    return last(orderBy(editorialReviews, 'updated'))
  }

  async save() {
    const existingReview = reviews.find(m => m.id === this.id)
    if (existingReview) {
      assign(existingReview, this)
    } else {
      reviews.push(this)
    }
    return Promise.resolve(this)
  }

  updateProperties(properties) {
    assign(this, properties)
    return this
  }

  async delete() {
    remove(reviews, r => r.id === this.id)
  }
  setSubmitted(date) {
    this.submitted = date
  }
  toDTO() {
    return {
      ...this,
      comments: this.comments
        ? this.comments.map(comment => comment.toDTO())
        : [],
      member: this.member ? this.member.toDTO() : undefined,
    }
  }
}

module.exports = Review
