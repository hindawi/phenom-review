const Chance = require('chance')

const chance = new Chance()
const { assign } = require('lodash')

const fixtures = require('../fixtures')

const { files } = fixtures

const { findMock, findByMock, allMock } = require('./repositoryMocks')

class File {
  constructor(props) {
    this.id = chance.guid()
    this.type = props.type || 'manuscript'
    this.fileName = props.fileName || 'test-file'
    this.size = props.size || '123'
    this.originalName = props.originalName || 'test-file'
    this.manuscriptId = props.manuscriptId
    this.position = props.position || 0
    this.mimeType = props.mimeType || 'application/pdf'
    this.scanStatus = props.scanStatus || 'scanning'
    this.scanningStartTime = props.scanningStartTime || null
  }

  static get Types() {
    return {
      figure: 'figure',
      manuscript: 'manuscript',
      supplementary: 'supplementary',
      coverLetter: 'coverLetter',
      reviewComment: 'reviewComment',
      responseToReviewers: 'responseToReviewers',
    }
  }

  static get ScanStatuses() {
    return {
      SKIPPED: 'skipped',
      SCANNING: 'scanning',
      HEALTHY: 'healthy',
      INFECTED: 'infected',
      TIMEOUT_PASSED: 'timeoutPassed',
      ERROR: 'error',
    }
  }

  updateProperties(properties) {
    assign(this, properties)
    return this
  }
  async save() {
    files.push(this)
    return Promise.resolve(this)
  }

  static find = id => findMock(id, 'files', fixtures)
  static findBy = values => findByMock(values, 'files', fixtures)
  static findAllByManuscriptAndType = () => allMock('files', fixtures)
}

module.exports = File
