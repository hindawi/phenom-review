const articleTypes = []

const generateArticleTypes = ArticleType => {
  const newArticleTypes = [
    new ArticleType({ name: 'Review Article', hasPeerReview: true }),
    new ArticleType({ name: 'Research Article', hasPeerReview: true }),
    new ArticleType({ name: 'Letter to the Editor', hasPeerReview: false }),
    new ArticleType({ name: 'Case Report', hasPeerReview: true }),
    new ArticleType({ name: 'Erratum', hasPeerReview: false }),
    new ArticleType({ name: 'Corrigendum', hasPeerReview: false }),
    new ArticleType({ name: 'Editorial', hasPeerReview: false }),
    new ArticleType({ name: 'Retraction', hasPeerReview: false }),
    new ArticleType({ name: 'Expression of Concern', hasPeerReview: false }),
    new ArticleType({ name: 'Case Series', hasPeerReview: true }),
  ]
  articleTypes.push(...newArticleTypes)
}

const getArticleTypeByPeerReview = hasPeerReview =>
  articleTypes.find(aT => aT.hasPeerReview === hasPeerReview)

module.exports = {
  generateArticleTypes,
  articleTypes,
  getArticleTypeByPeerReview,
}
