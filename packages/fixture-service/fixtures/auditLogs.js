const logs = []

const generateAuditLog = ({ properties, AuditLog }) => {
  const auditLog = new AuditLog(properties)
  logs.push(auditLog)
  return auditLog
}

module.exports = { logs, generateAuditLog }
