const jobs = []

const generateJob = ({ properties, Job }) => {
  const job = new Job(properties || {})
  jobs.push(job)

  return job
}

module.exports = { jobs, generateJob }
