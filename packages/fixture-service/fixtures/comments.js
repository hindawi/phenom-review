const comments = []

const generateComment = ({ properties, Comment }) => {
  const comment = new Comment(properties || {})
  comments.push(comment)

  return comment
}

module.exports = { comments, generateComment }
