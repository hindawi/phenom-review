const Chance = require('chance')

const chance = new Chance()
const reviewerSuggestions = []

function ReviewerSuggestion(props) {
  return {
    id: chance.guid(),
    created: props.create || Date.now(),
    updated: props.updated || Date.now(),
    manuscriptId: props.manuscriptId,
    givenNames: props.givenNames,
    surname: props.surname,
    reviews: props.numberOfReviews,
    email: props.email,
    aff: props.aff,
    affRorId: props.affRorId,
    async save() {
      reviewerSuggestions.push(this)
      return Promise.resolve(this)
    },
  }
}

const generateReviewerSuggestions = properties => {
  const reviewerSuggestion = new ReviewerSuggestion(properties)
  reviewerSuggestions.push(reviewerSuggestion)
  return reviewerSuggestions
}

module.exports = {
  reviewerSuggestions,
  ReviewerSuggestion,
  generateReviewerSuggestions,
}
