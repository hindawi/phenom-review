const files = []

const generateFile = ({ properties, File }) => {
  const file = new File(properties || {})
  files.push(file)

  return file
}

module.exports = { files, generateFile }
