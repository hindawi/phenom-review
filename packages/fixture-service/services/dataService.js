const Chance = require('chance')

const chance = new Chance()

module.exports = {
  createUserOnManuscript({ manuscript, fixtures, input = {}, role, models }) {
    const { Team, TeamMember, User, Identity } = models
    manuscript.teams = manuscript.teams || []
    let team = fixtures.getTeamByRoleAndManuscriptId({
      id: manuscript.id,
      role,
    })
    if (!team) {
      team = fixtures.generateTeam({
        properties: {
          role,
          manuscriptId: manuscript.id,
        },
        Team,
      })
      manuscript.teams.push(team)
    }

    const user = fixtures.generateUser({ User, Identity })

    const teamMember = fixtures.generateTeamMember({
      properties: {
        userId: user.id,
        teamId: team.id,
        ...input,
      },
      TeamMember,
    })

    setRelationsToManuscript({ teamMember, user, team, manuscript })

    return teamMember
  },
  createUserOnSection({ role, models, section, fixtures, input = {} }) {
    const { Team, TeamMember, User, Identity } = models

    let team = fixtures.getTeamByRoleAndSectionId({ id: section.id, role })
    if (!team) {
      team = fixtures.generateTeam({
        properties: {
          role,
          sectionId: section.id,
        },
        Team,
      })
      section.teams.push(team)
    }

    const user = fixtures.generateUser({ User, Identity })

    const teamMember = fixtures.generateTeamMember({
      properties: {
        userId: user.id,
        teamId: team.id,
        ...input,
      },
      TeamMember,
    })

    setRelationsToSection({ teamMember, user, section, team })

    return teamMember
  },
  addUserOnSpecialIssue({
    specialIssue,
    fixtures,
    input = {},
    role,
    user,
    models,
  }) {
    const { Team, TeamMember } = models

    let team = fixtures.getTeamByRoleAndManuscriptId({
      id: specialIssue.id,
      role,
    })
    if (!team) {
      team = fixtures.generateTeam({
        properties: { role, specialIssueId: specialIssue.id },
        Team,
      })
    }

    const teamMember = fixtures.generateTeamMember({
      properties: { userId: user.id, teamId: team.id, ...input },
      TeamMember,
    })

    setRelationsToManuscript({ teamMember, user, team, specialIssue })

    return teamMember
  },
  createUserOnJournal({ role, models, journal, fixtures, input = {} }) {
    const { Team, TeamMember, User, Identity } = models

    let team = fixtures.getTeamByRoleAndJournalId({ id: journal.id, role })
    if (!team) {
      team = fixtures.generateTeam({
        properties: {
          role,
          journalId: journal.id,
        },
        Team,
      })
      journal.teams.push(team)
    }

    const user = fixtures.generateUser({ User, Identity })

    const teamMember = fixtures.generateTeamMember({
      properties: {
        userId: user.id,
        teamId: team.id,
        ...input,
      },
      TeamMember,
    })

    setRelationsToJournal({ teamMember, user, journal, team })

    return teamMember
  },
  addUserOnManuscript({
    manuscript,
    fixtures,
    input = {},
    role,
    user,
    models,
  }) {
    const { Team, TeamMember } = models

    let team = fixtures.getTeamByRoleAndManuscriptId({
      id: manuscript.id,
      role,
    })
    if (!team) {
      team = fixtures.generateTeam({
        properties: { role, manuscriptId: manuscript.id },
        Team,
      })
      manuscript.teams.push(team)
    }

    const teamMember = fixtures.generateTeamMember({
      properties: { userId: user.id, teamId: team.id, ...input },
      TeamMember,
    })

    setRelationsToManuscript({ teamMember, user, team, manuscript })

    return teamMember
  },
  async createReviewOnManuscript({
    models,
    fixtures,
    manuscript,
    teamMember,
    recommendation,
  }) {
    const { Review, Comment } = models
    const review = fixtures.generateReview({
      properties: {
        manuscriptId: manuscript.id,
        recommendation,
        teamMemberId: teamMember.id,
      },
      Review,
    })
    review.manuscript = manuscript
    review.member = teamMember
    manuscript.reviews.push(review)

    const comment = fixtures.generateComment({
      properties: {
        reviewId: review.id,
        content: chance.sentence(),
        type: Comment.Types.public,
      },
      Comment,
    })
    comment.review = review
    review.comments.push(comment)

    return review
  },
  createGlobalUser({ fixtures, input = {}, role, models }) {
    const { Team, User, Identity, TeamMember } = models
    let team = fixtures.getGlobalTeamByRole(role)
    if (!team) {
      team = fixtures.generateTeam({
        properties: {
          role,
        },
        Team,
      })
    }

    const user = fixtures.generateUser({ User, Identity })

    const teamMember = fixtures.generateTeamMember({
      properties: {
        userId: user.id,
        teamId: team.id,
        ...input,
      },
      TeamMember,
    })
    createTeamMemberRelations({ teamMember, user, team })

    return teamMember
  },
}

const setRelationsToManuscript = ({ user, team, teamMember, manuscript }) => {
  createTeamMemberRelations({ teamMember, user, team })
  team.manuscript = manuscript
}

const setRelationsToSection = ({ user, team, teamMember, section }) => {
  createTeamMemberRelations({ teamMember, user, team })
  team.section = section
}

const setRelationsToJournal = ({ user, team, teamMember, journal }) => {
  createTeamMemberRelations({ teamMember, user, team })
  team.journal = journal
}

const createTeamMemberRelations = ({ teamMember, user, team }) => {
  teamMember.user = user
  teamMember.team = team
  teamMember.setAlias(user.defaultIdentity)
  team.members.push(teamMember)
  user.teamMemberships.push(teamMember)
}
