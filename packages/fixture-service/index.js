const models = require('./models')
const services = require('./services')
const fixtures = require('./fixtures')

module.exports = {
  models,
  services,
  fixtures,
}
