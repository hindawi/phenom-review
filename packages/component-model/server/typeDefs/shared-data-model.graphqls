interface Object {
  id: ID!
  created: DateTime!
  updated: DateTime
}

type Organization implements Object {
  id: ID!
  created: DateTime!
  updated: DateTime
  name: String!
  journals: [Journal]
}

type Journal implements Object {
  id: ID!
  created: DateTime!
  updated: DateTime
  journalTitle: String! # JATS <journal-title>
  manuscripts: [Manuscript]
  meta: JournalMeta
}

type JournalMeta {
  publisherName: String # JATS <publisher-name>
}

type Manuscript implements Object {
  id: ID!
  created: DateTime!
  updated: DateTime
  manuscriptVersions: [ManuscriptVersion]
  files: [File] # JATS <graphic>, <media>, <supplementary-material>
  teams: [Team]
  reviews: [Review]
  status: String # e.g. 'initial', 'QA'
  formState: String # persists serialized form state as JSON
  decision: String
  meta: ManuscriptMeta
}

# Manuscript version is a snapshot copy of a manuscript, whenever the manuscript is changed
# in such a way that a new version should be created and the previous recorded as history.
type ManuscriptVersion implements Object {
  id: ID!
  created: DateTime!
  updated: DateTime
  files: [File]
  teams: [Team]
  reviews: [Review]
  status: String
  formState: String
  decision: String
  meta: ManuscriptMeta
}

type ManuscriptMeta {
  title: String! # JATS <title>
  articleType: String # JATS @article-type
  articleIds: [ArticleId] # JATS <article-id>
  abstract: String # JATS <abstract>
  subjects: [String] # JATS <subject>
  history: [MetaDate] # JATS <history><date>
  publicationDates: [MetaDate] # JATS <pub-date>
  notes: [Note] # JATS <notes>
}

type ArticleId {
  pubIdType: String # JATS @pub-id-type
  id: String
}

type MetaDate {
  type: String # JATS @date-type e.g. 'epub' or 'submitted'
  date: DateTime # JATS @iso-8601-date
}

type Note implements Object {
  id: ID!
  created: DateTime!
  updated: DateTime
  notesType: String # JATS @notes-type
  content: String
}

enum ScanStatus {
  skipped
  scanning
  healthy
  infected
  timeoutPassed
  error
}

type File implements Object {
  id: ID!
  created: DateTime!
  updated: DateTime
  type: String # JATS <fig>, <supplementary-material>, <table-wrap> e.g. 'figure', 'supplementary', 'table'
  label: String # JATS <label> e.g. 'Fig 1', 'S1'
  filename: String # JATS <media>, <graphic> e.g. 'figure1.png'
  url: String # JATS @xlink:href
  mimeType: String # JATS @mime-type e.g. 'image/png'
  size: Int
  scanStatus: ScanStatus
}

type Review implements Object {
  id: ID!
  created: DateTime!
  updated: DateTime
  comments: [Comment]
  rejectDecisionInfo: RejectDecisionInfo
  recommendation: String
  open: Boolean
  user: User
}

type RejectDecisionInfo implements Object {
  id: ID!
  created: DateTime!
  updated: DateTime
  reasonsForRejection: ReasonsForRejection
  transferToAnotherJournal: TransferToAnotherJournal
}

type ReasonsForRejection {
  outOfScope: Boolean!
  technicalOrScientificFlaws: Boolean!
  publicationEthicsConcerns: Boolean!
  lackOfNovelty: Boolean
  otherReasons: String
}

type TransferToAnotherJournal {
  selectedOption: TransferToAnotherJournalOption
  transferSuggestions: String
}
enum TransferToAnotherJournalOption {
  YES
  NO
}

type Comment {
  type: String
  content: String
  files: [File]
}

type AuditLog {
  id: ID!
  created: DateTime!
  user: User
  action: String
  object: Object
  objectType: String
}

# All groups of people should be grouped in Teams.
# Teams are given permissions to an object.
# Permissions are based on the role.
type Team {
  id: ID!
  created: DateTime!
  updated: DateTime
  members: [TeamMember]
  role: String # e.g. author, seniorEditor, suggestedReviewer
  object: Object
  objectType: String
}
# Default roles 'submitter', 'author', 'reviewer', 'admin'
# Specialized roles 'deputyEditor', 'handlingEditor', 'seniorEditor', 'suggestedReviewer', 'opposedReviewer'

type TeamMember {
  user: User
  status: String
  alias: Alias
}

type Alias {
  name: Name
  email: Email
  aff: String # JATS <aff>
  affRorId: String
}

type User {
  id: ID!
  created: DateTime!
  updated: DateTime
  identities: [Identity]
  defaultIdentity: String # e.g. local
}

union Identity = Local | External

# local identity (not from ORCID, etc.)
type Local {
  name: Name
  email: Email
  aff: String # JATS <aff>
  affRorId: String
}

type External {
  identifier: String
  email: Email
  aff: String # JATS <aff>
  affRorId: String
}

type Name {
  surname: String
  givenNames: String
  title: String
}

scalar DateTime
scalar Email
