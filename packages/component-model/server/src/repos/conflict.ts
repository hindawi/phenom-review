import { Transaction } from 'knex'
import { Conflict as ConflictModel, ConflictI } from '../model/conflict'
// since we don't have TS on hindawiBaseModel and more importantly on pubswetModel,
// we cannot determine the models methods...

export type CreateConflictDTO = {
  firstConflictingTeamMemberId: string
  secondConflictingTeamMemberId: string
}

// this should become generic once we would be able to create a generic repo abstract class
interface RepoContract<Entity> {
  create(conflict: Partial<Entity>, trx?: Transaction): Promise<Entity>
  // update(conflict: Entity, trx?: Transaction): Promise<Entity>
  delete(conflict: Partial<Entity>, trx?: Transaction): Promise<Entity>
  // findById(id: string, trx?: Transaction): Promise<Entity>
  // all(trx?: Transaction): Promise<Entity[]>
  getConflictsForTeamMembers(
    teamMembers: string[],
    trx?: Transaction,
  ): Promise<Entity[]>
}

export class ConflictRepository implements RepoContract<ConflictI> {
  private ConflictModel: any = ConflictModel // :( any sad face

  public async create(
    conflict: CreateConflictDTO,
    trx?: Transaction,
  ): Promise<ConflictI> {
    const conflictModelInstance = new this.ConflictModel(conflict)
    return conflictModelInstance.save(trx)
  }

  public async findByConflictingTeamMembers(
    conflict: ConflictI,
  ): Promise<ConflictI> {
    return this.ConflictModel.query()
      .where(
        'firstConflictingTeamMemberId',
        conflict.firstConflictingTeamMemberId,
      )
      .andWhere(
        'secondConflictingTeamMemberId',
        conflict.secondConflictingTeamMemberId,
      )
      .limit(1)
      .first()
  }

  public async getConflictsForTeamMembers(
    teamMembers: string[],
    trx?: Transaction,
  ): Promise<ConflictI[]> {
    return this.ConflictModel.query(trx)
      .select('c.*')
      .from('conflict as c')
      .join('teamMember as tm', 'tm.id', 'c.firstConflictingTeamMemberId')
      .whereIn('c.firstConflictingTeamMemberId', teamMembers)
  }

  public async delete(conflict: ConflictI): Promise<ConflictI> {
    const conflictInstance = await this.findByConflictingTeamMembers(conflict)
    return conflictInstance.delete()
  }
  // public async findById(id: string): Promise<ConflictI> {
  //   return this.ConflictModel.findById(id)
  // }
  // public async all(): Promise<ConflictI[]> {
  //   return this.conflictModel.findByAll() // example
  // }
}
