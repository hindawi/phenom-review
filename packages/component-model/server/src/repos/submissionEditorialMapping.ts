import { Transaction } from 'knex'
import { ISubmissionEditorialModel } from '../model/submissionEditorialModel'
import {
  SubmissionEditorialMapping,
  SubmissionEditorialMappingI,
} from '../model/submissionEditorialMapping'

interface SubmissionEditorialMappingRepoI {
  /**
   * Gets the manuscripts submission specific config details in the editorial model that are saved
   * in the mapping table used for the manuscript submission activity
   * @param submissionId
   * @param trx
   */
  getSubmissionEditorialMapping(
    submissionId: string,
    trx?: Transaction,
  ): Promise<SubmissionEditorialMappingI>
  /**
   * At submission step 1, the user selects the configs for the submission that will be mapped to an editorial model;
   * this mapping between the submission id and the editorial model is saved in submissionEditorialMapping table;
   * @param submissionId
   * @param submissionEditorialModel
   * @param trx
   */
  createOrUpdateSubmissionEditorialMapping(
    submissionId: string,
    submissionEditorialModel: Partial<ISubmissionEditorialModel>,
    trx?: Transaction,
  ): Promise<SubmissionEditorialMappingI>
}

export class SubmissionEditorialMappingRepo
  implements SubmissionEditorialMappingRepoI {
  private SubmissionEditorialMappingModel: any = SubmissionEditorialMapping

  public async getSubmissionEditorialMapping(
    submissionId: string,
    trx?: Transaction,
  ): Promise<SubmissionEditorialMappingI> {
    return this.SubmissionEditorialMappingModel.query(trx)
      .where('submissionId', submissionId)
      .first()
  }

  public async createOrUpdateSubmissionEditorialMapping(
    submissionId: string,
    submissionEditorialModel: ISubmissionEditorialModel,
    trx?: Transaction,
  ) {
    const submissionEditorialMapping = await this.getSubmissionEditorialMapping(
      submissionId,
      trx,
    )

    if (submissionEditorialMapping) {
      submissionEditorialMapping.submissionEditorialModel = submissionEditorialModel
      return submissionEditorialMapping.save(trx)
    }
    const submissionEditorialMappingInstance = new this.SubmissionEditorialMappingModel(
      {
        submissionId,
        submissionEditorialModel,
      },
    )
    return submissionEditorialMappingInstance.save(trx)
  }
}
