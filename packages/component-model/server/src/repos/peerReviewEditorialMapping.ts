import { Transaction } from 'knex'
import {
  PeerReviewEditorialMapping,
  IPeerReviewEditorialMapping,
  IPeerReviewEditorialModel,
} from '../model/peerReviewEditorialMapping'

const { logger } = require('component-logger')

interface IPeerReviewEditorialMappingRepo {
  /**
   * Gets the submission specific config details that are saved in the mapping table used for the peer review activity
   * @param submissionId
   * @param trx
   */
  getPeerReviewEditorialMapping(
    submissionId: string,
    trx?: Transaction,
  ): Promise<IPeerReviewEditorialMapping | undefined>
  /**
   * The TriggeredPeerReview event will be send from FEM service, the event contains the mapping between submission and peerReviewEditorialModel;
   * this mapping between the submission id and the config is saved in peerReviewEditorialMapping table;
   * @param submissionId
   * @param peerReviewEditorialModel
   * @param trx
   */
  createPeerReviewEditorialMapping(
    submissionId: string,
    peerReviewEditorialModel: Partial<IPeerReviewEditorialModel>,
    trx?: Transaction,
  ): Promise<IPeerReviewEditorialMapping>
}

export class PeerReviewEditorialMappingRepo
  implements IPeerReviewEditorialMappingRepo {
  private PeerReviewEditorialMappingModel: any = PeerReviewEditorialMapping

  public async getPeerReviewEditorialMapping(
    submissionId: string,
    trx?: Transaction,
  ): Promise<IPeerReviewEditorialMapping | undefined> {
    return this.PeerReviewEditorialMappingModel.query(trx)
      .where('submissionId', submissionId)
      .first()
  }

  public async createPeerReviewEditorialMapping(
    submissionId: string,
    peerReviewEditorialModel: IPeerReviewEditorialModel,
    trx?: Transaction,
  ): Promise<IPeerReviewEditorialMapping> {
    const peerReviewEditorialMapping = await this.getPeerReviewEditorialMapping(
      submissionId,
      trx,
    )
    if (peerReviewEditorialMapping) {
      logger.info(
        `The peer review editorial model for the submissionId: ${submissionId} already exists.`,
      )
      return peerReviewEditorialMapping
    }
    const peerReviewEditorialMappingInstance = new this.PeerReviewEditorialMappingModel(
      {
        submissionId,
        peerReviewEditorialModel,
      },
    )
    return peerReviewEditorialMappingInstance.save(trx)
  }
}
