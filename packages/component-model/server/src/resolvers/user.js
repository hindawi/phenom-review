const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('../useCases')

const resolvers = {
  Query: {
    async currentUser(_, queryParam, ctx) {
      return useCases.currentUserUseCase.initialize(models).execute(ctx.user)
    },
    async getUser(_, { userId }, ctx) {
      return useCases.getUserUseCase.initialize(models).execute({ userId })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
