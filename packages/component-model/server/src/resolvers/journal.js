const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('../useCases')

const resolvers = {
  Query: {
    getJournal(_, { journalId }, ctx) {
      return useCases.getJournalUseCase.initialize(models).execute(journalId)
    },
    getActiveJournalsBySelectedArticleType(_, { articleTypeId }) {
      return useCases.getActiveJournalsBySelectedArticleTypeUseCase
        .initialize(models)
        .execute(articleTypeId)
    },
  },
  Journal: {
    articleTypes(journal, _, ctx) {
      return useCases.getJournalArticleTypesUseCase.initialize(models).execute({
        journal,
        userId: ctx.user,
        loaders: ctx.loaders,
        getJournalArticleTypesForUserUseCase:
          useCases.getJournalArticleTypesForUserUseCase,
      })
    },
    sections(journal) {
      return useCases.getJournalSectionsUseCase
        .initialize()
        .execute({ journal })
    },
    peerReviewModel(journal, _, ctx) {
      return useCases.getJournalPeerReviewModelUseCase
        .initialize({ loaders: ctx.loaders })
        .execute({ journal })
    },
    specialIssues(journal) {
      return useCases.getJournalSpecialIssuesUseCase
        .initialize()
        .execute({ journal })
    },
    preprints(journal) {
      return useCases.getJournalPreprintsUseCase
        .initialize()
        .execute({ journal })
    },
    preprintDescription(journal) {
      return useCases.getJournalPreprintDescriptionUseCase
        .initialize()
        .execute({ journal })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
