const { withAuthsomeMiddleware } = require('helper-service')

const userResolvers = require('./user')
const journalResolvers = require('./journal')
const identityResolvers = require('./identity')
const manuscriptResolvers = require('./manuscript')
const teamMemberResolvers = require('./teamMember')
const submissionResolvers = require('./submission')
const specialIssueResolvers = require('./specialIssue')
const sourceJournalResolvers = require('./sourceJournal').default

const { merge } = require('lodash')
const useCases = require('../useCases')

const mergedResolvers = merge(
  userResolvers,
  journalResolvers,
  identityResolvers,
  manuscriptResolvers,
  teamMemberResolvers,
  submissionResolvers,
  specialIssueResolvers,
  sourceJournalResolvers,
)

module.exports = withAuthsomeMiddleware(mergedResolvers, useCases)
