const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('../useCases')

const resolvers = {
  SpecialIssue: {
    peerReviewModel(specialIssue, _, ctx) {
      return useCases.getSpecialIssuePeerReviewModelUseCase
        .initialize({ models, loaders: ctx.loaders })
        .execute({ specialIssue })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
