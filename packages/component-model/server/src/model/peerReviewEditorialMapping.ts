import HindawiBaseModel from '../hindawiBaseModel'
import { Manuscript } from './manuscript'
import { Team } from './team'

const { logger } = require('component-logger')

interface TriageEditor {
  enabled: boolean
  label: string
}
interface AcademicEditor {
  label: string
  labelInCaseOfConflict: string
  autoInviteInCaseOfConflict: boolean
}
interface FigureHeadEditor {
  enabled: boolean
  label: string
}
export interface IPeerReviewEditorialModel {
  name: string
  hasEditorialAssistant: boolean
  approvalEditor: string
  triageEditor: TriageEditor
  academicEditor: AcademicEditor
  figureHeadEditor: FigureHeadEditor
  minNoOfReviewerReports: number
}
const PeerReviewEditorialModelSchema = {
  type: 'object',
  properties: {
    name: { type: ['string', false] },
    hasEditorialAssistant: { type: ['boolean', false] },
    approvalEditor: { type: ['string', false] },
    triageEditor: { type: ['object', 'null'] },
    academicEditor: { type: ['object', 'null'] },
    figureHeadEditor: { type: ['object', 'null'] },
    minNoOfReviewerReports: { type: 'number', default: 1 },
  },
}

export interface IPeerReviewEditorialMapping {
  submissionId: string
  peerReviewEditorialModel: IPeerReviewEditorialModel
  save: (trx) => Promise<IPeerReviewEditorialMapping>
}

export class PeerReviewEditorialMapping extends HindawiBaseModel {
  static get tableName() {
    return 'peer_review_editorial_mapping'
  }
  static get schema() {
    return {
      properties: {
        submissionId: { type: 'string', format: 'uuid' },
        peerReviewEditorialModel: PeerReviewEditorialModelSchema,
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Manuscript,
        join: {
          from: 'peer_review_editorial_mapping.submissionId',
          to: 'manuscript.submissionId',
        },
      },
    }
  }

  static async getApprovalEditorRoleForSubmission(submissionId: string) {
    const results = await (<IPeerReviewEditorialMapping[]>(
      (<unknown>this.findBy({ submissionId }))
    ))

    // as per https://jira.wiley.com/secure/RapidBoard.jspa?rapidView=3762&projectKey=PPBK&view=detail&selectedIssue=PPBK-4746:
    // if the editorial model doesn't have a decision editor, then the academic editor is able to make the decision
    // => if the decision editor is not a triage editor, he will always be an academic editor
    if (!results.length) {
      logger.warn(
        `Peer review editorial mapping for submission: ${submissionId}, is not assigned at this point`,
      )
      return Team.Role.academicEditor
    }

    const peerReviewEditorialMapping = results[0]

    const { peerReviewEditorialModel } = peerReviewEditorialMapping

    if (!peerReviewEditorialModel || !peerReviewEditorialModel.approvalEditor) {
      return Team.Role.academicEditor
    }

    return peerReviewEditorialModel.approvalEditor
  }
}
