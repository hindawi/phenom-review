import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { TeamMember, TeamMemberI } from './teamMember'

export interface ConflictI extends HindawiBaseModelProps {
  id: string
  firstConflictingTeamMemberId: string
  secondConflictingTeamMemberId: string
  delete: () => Promise<ConflictI>

  teamMember1?: TeamMemberI
  teamMember2?: TeamMemberI
}

export interface ConflictRepository
  extends HindawiBaseModelRepository<ConflictI> {
  new (...args: any[]): ConflictI
}

export class Conflict extends HindawiBaseModel implements ConflictI {
  firstConflictingTeamMemberId: string
  secondConflictingTeamMemberId: string

  teamMember1?: TeamMember
  teamMember2?: TeamMember

  static get tableName() {
    return 'conflict'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        firstConflictingTeamMemberId: { type: 'string', format: 'uuid' },
        secondConflictingTeamMemberId: { type: 'string', format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      teamMember1: {
        relation: HindawiBaseModel.HasOneRelation,
        modelClass: TeamMember,
        join: {
          from: 'conflict.firstConflictingTeamMemberId',
          to: 'team_member.id',
        },
      },
      teamMember2: {
        relation: HindawiBaseModel.HasOneRelation,
        modelClass: TeamMember,
        join: {
          from: 'conflict.secondConflictingTeamMemberId',
          to: 'team_member.id',
        },
      },
    }
  }
}
