import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Manuscript, ManuscriptI } from './manuscript'
import { User, UserI } from './user'

export interface TriageEditorI extends HindawiBaseModelProps {
  manuscriptId: string
  journalId: string
  userId: string
  email: string

  manuscripts?: ManuscriptI[]
  user?: UserI
}

export interface TriageEditorRepository
  extends HindawiBaseModelRepository<TriageEditorI> {
  new (...args: any[]): TriageEditorI
}

export class TriageEditor extends HindawiBaseModel implements TriageEditorI {
  manuscriptId: string
  journalId: string
  userId: string
  email: string

  manuscripts?: ManuscriptI[]
  user?: UserI

  static get tableName() {
    return 'triage_editor'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        manuscriptId: { type: 'string', format: 'uuid' },
        journalId: { type: 'string', format: 'uuid' },
        userId: { type: 'string', format: 'uuid' }, // this should be the id from the sso
        email: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Manuscript,
        join: {
          from: 'triage_editor.manuscriptId',
          to: 'manuscript.id',
        },
      },
      user: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'triage_editor.userId',
          to: 'user.id',
        },
      },
    }
  }
}
