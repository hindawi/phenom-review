/* eslint-disable sonarjs/no-duplicate-string */
import { chain } from 'lodash'
import { transaction } from 'objection'
import moment from 'moment-business-days'
import config from 'config'

import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Team, TeamI } from './team'
import { Journal, JournalI } from './journal'
import { Section, SectionI } from './section'
import { PeerReviewModel, PeerReviewModelI } from './peerReviewModel'
import { Manuscript, ManuscriptI } from './manuscript'
import { JobRepository, JobType } from './job'

const { services } = require('helper-service')
const { logger } = require('component-logger')

export interface SpecialIssueI extends HindawiBaseModelProps {
  name: string
  journalId?: string
  sectionId?: string
  peerReviewModelId?: string
  customId: string
  isActive: boolean
  isCancelled: boolean
  cancelReason?: string
  callForPapers: string
  startDate: string
  endDate: string

  journal?: JournalI
  section?: SectionI
  peerReviewModel?: PeerReviewModelI
  teams?: TeamI
  manuscripts?: ManuscriptI[]

  cancelJobByType(options: {
    Job: JobRepository
    jobType: JobType
  }): Promise<void>
  handleActivation({
    adminId,
    oldEndDate,
    jobsService,
    oldStartDate,
    models: { Job },
    dateService: { isDateToday, areDatesEqual, isDateInTheFuture },
  }: {
    adminId: string
    oldEndDate: string
    jobsService: any
    oldStartDate: string
    models: { Job: any }
    dateService: { isDateToday; areDatesEqual; isDateInTheFuture }
  }): Promise<void>
}

export interface SpecialIssueRepository
  extends HindawiBaseModelRepository<SpecialIssueI> {
  new (...args: any[]): SpecialIssueI

  generateUniqueCustomId(maxTries: number): Promise<string | undefined>
  findByCustomId(customId: string): Promise<SpecialIssueI>
  findOneByNameAndCustomId(options: {
    name: string
    customId: string
  }): Promise<SpecialIssue | undefined>
  findUnique(name: string): Promise<SpecialIssue | undefined>
  findAllByManuscripts(manuscriptIds: string[]): Promise<SpecialIssueI[]>
  findAllActiveSpecialIssues(): Promise<SpecialIssueI[]>
  findAllPendingActivationSpecialIssues(): Promise<SpecialIssueI[]>
  findAllCancelledSpecialIssues(): Promise<SpecialIssueI[]>
  findAllEndedSpecialIssues(): Promise<SpecialIssueI[]>
  findAllScheduledForActivation(): Promise<SpecialIssue[]>
  findAllScheduledForDeactivation(): Promise<SpecialIssue[]>
  updateMany(specialIssues: SpecialIssueI[]): Promise<void>
}

export class SpecialIssue extends HindawiBaseModel implements SpecialIssueI {
  name: string
  journalId?: string
  sectionId?: string
  peerReviewModelId?: string
  customId: string
  isActive: boolean
  isCancelled: boolean
  cancelReason?: string
  callForPapers: string
  startDate: string
  endDate: string

  journal?: JournalI
  section?: SectionI
  peerReviewModel?: PeerReviewModelI
  teams?: TeamI
  manuscripts?: ManuscriptI[]

  expirationDate: unknown

  static get tableName() {
    return 'special_issue'
  }

  static modifiers = {
    onlyActive(builder: any) {
      builder.where('isActive', true).andWhere('isCancelled', false)
    },
  }

  static generateCallForPapers(si) {
    // only generate callForPapers when the new UI is used
    if (!si.callForPapers && si.description?.length && si.topics?.length) {
      return `${si.description.join('\n\n')}\n\n${si.topics.join('\n')}`
    }
    return si.callForPapers
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        name: { type: 'string' },
        journalId: { type: ['string', 'null'], format: 'uuid' },
        sectionId: { type: ['string', 'null'], format: 'uuid' },
        peerReviewModelId: { type: ['string', 'null'], format: 'uuid' },
        customId: { type: ['string'] },
        isActive: { type: 'boolean' },
        isCancelled: { type: 'boolean' },
        isPublished: { type: 'boolean' },
        cancelReason: { type: ['string', 'null'] },
        callForPapers: { type: 'string' },
        startDate: {
          type: ['string', 'object'],
          format: 'date-time',
        },
        endDate: {
          type: ['string', 'object'],
          format: 'date-time',
        },
        publicationDate: {
          type: ['string', 'object', 'null'],
          format: 'date-time',
        },
        description: { type: ['array', 'null'], items: { type: 'string' } },
        type: { type: 'string' },
        acronym: { type: ['string', 'null'] },
        isAnnual: { type: 'boolean' },
        conflictOfInterest: { type: ['string', 'null'] },
        topics: { type: ['array', 'null'], items: { type: 'string' } },
      },
    }
  }

  static beforeInsert({ inputItems }) {
    inputItems.forEach(
      item => (item.callForPapers = SpecialIssue.generateCallForPapers(item)),
    )
  }

  // eslint-disable-next-line sonarjs/no-identical-functions
  static beforeUpdate({ inputItems }) {
    inputItems.forEach(
      item => (item.callForPapers = SpecialIssue.generateCallForPapers(item)),
    )
  }

  get submissionUrl() {
    const baseUrl = config.get('pubsweet-client.baseUrl')
    return services.createUrl(baseUrl, '/submit', {
      specialIssue: this.customId,
    })
  }

  // added to fix an error when saving the array fields (description, topics)
  static get jsonAttributes() {
    return []
  }

  static get relationMappings() {
    return {
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Journal,
        join: {
          from: 'special_issue.journalId',
          to: 'journal.id',
        },
      },
      section: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Section,
        join: {
          from: 'special_issue.sectionId',
          to: 'section.id',
        },
      },
      peerReviewModel: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: PeerReviewModel,
        join: {
          from: 'special_issue.peerReviewModelId',
          to: 'peer_review_model.id',
        },
      },
      teams: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Team,
        join: {
          from: 'special_issue.id',
          to: 'team.specialIssueId',
        },
      },
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Manuscript,
        join: {
          from: 'special_issue.id',
          to: 'manuscript.specialIssueId',
        },
      },
    }
  }
  static async generateUniqueCustomId(
    maxTries: number,
  ): Promise<string | undefined> {
    const customId = `${Math.round(100000 + Math.random() * 899990).toString()}`
    const found = await SpecialIssue.findByCustomId(customId)

    if (!found) {
      return customId
    }

    if (maxTries <= 0) {
      // if it gets here, the SI customId will be set by the column_default config set in the database(special_issue table)
      return undefined
    }
    return this.generateUniqueCustomId(maxTries - 1)
  }

  static async findByCustomId(customId: string): Promise<SpecialIssue> {
    return this.query()
      .from('special_issue as si')
      .where('si.custom_id', customId)
      .first()
  }
  static async findOneByNameAndCustomId({
    name,
    customId,
  }: {
    name: string
    customId: string
  }): Promise<SpecialIssue> {
    return this.query()
      .from('special_issue as si')
      .where('si.custom_id', customId)
      .andWhere('si.name', name)
      .first()
  }

  static async findUnique(name: string): Promise<SpecialIssue | undefined> {
    const objects = await this.query()
      .whereRaw('LOWER(name) LIKE ?', [name.toLowerCase()])
      .limit(1)

    if (!objects.length) {
      return undefined
    }

    return objects[0]
  }

  static findAllByManuscripts(
    manuscriptIds: string[],
  ): Promise<SpecialIssue[]> {
    return this.query()
      .select('si.*', 'm.id as manuscriptId')
      .from('special_issue as si')
      .join('manuscript as m', 'si.id', 'm.specialIssueId')
      .whereIn('m.id', manuscriptIds)
  }

  static async findAllActiveSpecialIssues(): Promise<SpecialIssue[]> {
    try {
      return this.query()
        .from('special_issue')
        .where('start_date', '<', new Date())
        .andWhere('end_date', '>', new Date())
        .andWhere({ is_active: true })
        .andWhere({ is_cancelled: false })
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllPendingActivationSpecialIssues(): Promise<
    SpecialIssue[]
  > {
    try {
      return this.query()
        .from('special_issue')
        .where('start_date', '>', new Date())
        .andWhere('end_date', '>', new Date())
        .andWhere({ is_active: false })
        .andWhere({ is_cancelled: false })
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllCancelledSpecialIssues(): Promise<SpecialIssue[]> {
    try {
      return this.query()
        .from('special_issue')
        .where({ is_cancelled: true })
        .andWhere({ is_active: true })
    } catch (e) {
      throw new Error(e)
    }
  }
  static async findAllEndedSpecialIssues(): Promise<SpecialIssue[]> {
    try {
      return this.query()
        .from('special_issue')
        .where('end_date', '<', new Date())
        .andWhere({ is_cancelled: false })
        .andWhere({ is_active: true })
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllBySection({ sectionId }: { sectionId: string }) {
    try {
      return await this.query()
        .from('special_issue AS si')
        .join('section AS s', 'si.section_id', 's.id')
        .where('s.id', sectionId)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllByJournal({ journalId }: { journalId: string }) {
    try {
      return await this.query()
        .from('special_issue AS si')
        .join('journal AS j', 'si.journal_id', 'j.id')
        .where('j.id', journalId)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllThatShouldBeActive(date: string) {
    try {
      return this.query()
        .from('special_issue')
        .where('start_date', '<', date)
        .andWhere('end_date', '>', date)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllScheduledForActivation(): Promise<SpecialIssue[]> {
    try {
      return this.query()
        .from('special_issue')
        .where('start_date', '>', new Date().toISOString())
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllScheduledForDeactivation(): Promise<SpecialIssue[]> {
    try {
      return this.query()
        .from('special_issue')
        .where('endDate', '>', new Date().toISOString())
    } catch (e) {
      throw new Error(e)
    }
  }

  static async updateMany(specialIssues: SpecialIssueI[]): Promise<void> {
    try {
      return await transaction(SpecialIssue.knex(), async trx => {
        const options = { noInsert: true, noDelete: true, relate: true }
        await SpecialIssue.query(trx).upsertGraph(specialIssues, options)
      })
    } catch (err) {
      logger.error('Something went wrong. No data was updated.', err)
      throw new Error('Something went wrong. No data was updated.')
    }
  }

  async cancelJobByType({
    Job,
    jobType,
  }: {
    Job: JobRepository
    jobType: JobType
  }): Promise<void> {
    const specialIssueJob = await Job.findOneBy({
      queryObject: {
        jobType,
        specialIssueId: this.id,
      },
    })
    if (!specialIssueJob) {
      logger.warn(
        `No special issue with specialIssueId ${this.id} and type ${jobType} has been found.`,
      )
      return
    }
    await Job.cancel(specialIssueJob.id)
    await specialIssueJob.delete()
  }

  async handleActivation({
    adminId,
    oldEndDate,
    jobsService,
    oldStartDate,
    models: { Job },
    dateService: { isDateToday, areDatesEqual, isDateInTheFuture },
  }: {
    adminId: string
    oldEndDate: string
    jobsService: any
    oldStartDate: string
    models: { Job: any }
    dateService: { isDateToday; areDatesEqual; isDateInTheFuture }
  }): Promise<void> {
    if (
      areDatesEqual(oldStartDate, this.startDate) &&
      areDatesEqual(oldEndDate, this.endDate)
    ) {
      return
    }

    await this.cancelJobByType({ Job, jobType: Job.Type.activation })

    if (
      isDateToday(this.startDate) ||
      (isDateInTheFuture(this.endDate) && !isDateInTheFuture(this.startDate))
    ) {
      this.updateProperties({ isActive: true, isCancelled: false })
      return
    }

    if (!isDateInTheFuture(this.startDate)) return

    jobsService.scheduleSpecialIssueActivation({
      specialIssue: this,
      teamMemberId: adminId,
    })
  }

  toDTO() {
    let leadGuestEditorMember
    if (this.teams) {
      const editorTeam = chain(this.teams).find(
        (t: Team) =>
          t.role === Team.Role.triageEditor && t.specialIssueId !== null,
      )
      leadGuestEditorMember = chain(editorTeam)
        .get('members.0')
        .value()
    }

    const expirationInterval = config.get('specialIssueExpirationInterval') as {
      value: number
      timeUnit: moment.unitOfTime.DurationConstructor
    }
    this.expirationDate = moment(this.endDate)
      .add(expirationInterval.value, expirationInterval.timeUnit)
      .toISOString()

    return {
      ...this,
      hasManuscripts: this.manuscripts && this.manuscripts.length,
      leadGuestEditor: leadGuestEditorMember
        ? leadGuestEditorMember.toDTO()
        : undefined,
    }
  }
}
