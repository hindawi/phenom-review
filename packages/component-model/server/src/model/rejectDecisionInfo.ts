import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'

export interface RejectDecisionInfoI extends HindawiBaseModelProps {
  outOfScope: boolean
  technicalOrScientificFlaws: boolean
  publicationEthicsConcerns: boolean
  otherReasons?: string
  transferToAnotherJournal: boolean
  transferSuggestions?: string

  toDTO(): object
}

export interface RejectDecisionInfoRepository
  extends HindawiBaseModelRepository<RejectDecisionInfoI> {
  new (...args: any[]): RejectDecisionInfoI
}

export class RejectDecisionInfo extends HindawiBaseModel
  implements RejectDecisionInfoI {
  outOfScope: boolean
  technicalOrScientificFlaws: boolean
  publicationEthicsConcerns: boolean
  lackOfNovelty: boolean
  otherReasons?: string
  transferToAnotherJournal: boolean
  transferSuggestions?: string

  static get tableName() {
    return 'reject_decision_info'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        outOfScope: { type: 'boolean' },
        technicalOrScientificFlaws: { type: 'boolean' },
        publicationEthicsConcerns: { type: 'boolean' },
        lackOfNovelty: { type: ['boolean', 'null'] },
        otherReasons: { type: ['string', 'null'] },
        transferToAnotherJournal: { type: 'boolean' },
        transferSuggestions: { type: ['string', 'null'] },
      },
    }
  }

  static get ReasonsForRejection() {
    return {
      OUT_OF_SCOPE: 'outOfScope',
      TECHNICAL_OR_SCIENTIFIC_FLAWS: 'technicalOrScientificFlaws',
      PUBLICATIONS_ETHICS_CONCERNS: 'publicationEthicsConcerns',
      LACK_OF_NOVELTY: 'lackOfNovelty',
      OTHER_REASONS: 'otherReasons',
    }
  }

  toDTO() {
    return {
      reasonsForRejection: {
        outOfScope: this.outOfScope,
        technicalOrScientificFlaws: this.technicalOrScientificFlaws,
        publicationEthicsConcerns: this.publicationEthicsConcerns,
        lackOfNovelty: this.lackOfNovelty ?? undefined,
        otherReasons: this.otherReasons || undefined,
      },
      transferToAnotherJournal: {
        selectedOption: this.transferToAnotherJournal === true ? 'YES' : 'NO',
        transferSuggestions: this.transferSuggestions || undefined,
      },
    }
  }
}
