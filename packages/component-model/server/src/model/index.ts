/* eslint-disable object-shorthand */
import { AcademicEditor, AcademicEditorRepository } from './academicEditor'
import { ArticleType, ArticleTypeRepository } from './articleType'
import { AuditLog, AuditLogRepository } from './auditLog'
import { Author, AuthorRepository } from './author'
import {
  AvailableCustomIds,
  AvailableCustomIdsRepository,
} from './availableCustomIds'
import { CommentRepository, Comment } from './comment'
import { Conflict, ConflictRepository } from './conflict'
import {
  EditorialAssistant,
  EditorialAssistantRepository,
} from './editorialAssistant'
import {
  EditorSuggestion,
  EditorSuggestionRepository,
} from './editorSuggestion'
import { FileRepository, File } from './file'
import { Identity, IdentityRepository } from './identity'
import { Job, JobRepository } from './job'
import { Journal, JournalRepository } from './journal'
import {
  JournalArticleType,
  JournalArticleTypeRepository,
} from './journalArticleType'
import { JournalPreprint, JournalPreprintRepository } from './journalPreprint'
import { Manuscript, ManuscriptRepository } from './manuscript'
import { PeerReviewModel, PeerReviewModelRepository } from './peerReviewModel'
import { Preprint, PreprintRepository } from './preprint'
import {
  RejectDecisionInfoRepository,
  RejectDecisionInfo,
} from './rejectDecisionInfo'
import { Review, ReviewRepository } from './review'
import {
  ReviewerSuggestion,
  ReviewerSuggestionRepository,
} from './reviewerSuggestion'
import { Section, SectionRepository } from './section'
import { SourceJournal, SourceJournalRepository } from './sourceJournal'
import { SpecialIssue, SpecialIssueRepository } from './specialIssue'
import {
  SubmissionEditorialMapping,
  SubmissionEditorialMappingRepository,
} from './submissionEditorialMapping'
import {
  SubmittingStaffMember,
  SubmittingStaffMemberRepository,
} from './submittingStaffMember'
import { Team, TeamRepository } from './team'
import { TeamMember, TeamMemberRepository } from './teamMember'
import { TriageEditor, TriageEditorRepository } from './triageEditor'
import { User, UserRepository } from './user'

export interface Models {
  AcademicEditor: AcademicEditorRepository
  ArticleType: ArticleTypeRepository
  AuditLog: AuditLogRepository
  Author: AuthorRepository
  AvailableCustomIds: AvailableCustomIdsRepository
  Comment: CommentRepository
  Conflict: ConflictRepository
  EditorialAssistant: EditorialAssistantRepository
  EditorSuggestion: EditorSuggestionRepository
  File: FileRepository
  Identity: IdentityRepository
  Job: JobRepository
  Journal: JournalRepository
  JournalArticleType: JournalArticleTypeRepository
  JournalPreprint: JournalPreprintRepository
  Manuscript: ManuscriptRepository
  PeerReviewModel: PeerReviewModelRepository
  Preprint: PreprintRepository
  RejectDecisionInfo: RejectDecisionInfoRepository
  Review: ReviewRepository
  ReviewerSuggestion: ReviewerSuggestionRepository
  Section: SectionRepository
  SourceJournal: SourceJournalRepository
  SpecialIssue: SpecialIssueRepository
  SubmissionEditorialMapping: SubmissionEditorialMappingRepository
  SubmittingStaffMember: SubmittingStaffMemberRepository
  Team: TeamRepository
  TeamMember: TeamMemberRepository
  TriageEditor: TriageEditorRepository
  User: UserRepository
}

const models: Models = {
  AcademicEditor: AcademicEditor,
  ArticleType: ArticleType,
  AuditLog: AuditLog,
  Author: Author,
  AvailableCustomIds: AvailableCustomIds,
  Comment: Comment,
  Conflict: Conflict,
  EditorialAssistant: EditorialAssistant,
  EditorSuggestion: EditorSuggestion,
  File: File,
  Identity: Identity,
  Job: Job,
  Journal: Journal,
  JournalArticleType: JournalArticleType,
  JournalPreprint: JournalPreprint,
  Manuscript: Manuscript,
  PeerReviewModel: PeerReviewModel,
  Preprint: Preprint,
  RejectDecisionInfo: RejectDecisionInfo,
  Review: Review,
  ReviewerSuggestion: ReviewerSuggestion,
  Section: Section,
  SourceJournal: SourceJournal,
  SpecialIssue: SpecialIssue,
  SubmissionEditorialMapping: SubmissionEditorialMapping,
  SubmittingStaffMember: SubmittingStaffMember,
  Team: Team,
  TeamMember: TeamMember,
  TriageEditor: TriageEditor,
  User: User,
}

export { models }
export default models
