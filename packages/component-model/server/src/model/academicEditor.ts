import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Manuscript, ManuscriptI } from './manuscript'
import { User, UserI } from './user'

export interface AcademicEditorI extends HindawiBaseModelProps {
  manuscriptId: string
  journalId: string
  userId: string
  email: string

  manuscripts?: ManuscriptI[]
  user?: UserI
}

export interface AcademicEditorRepository
  extends HindawiBaseModelRepository<AcademicEditorI> {
  new (...args: any[]): AcademicEditorI
}

export class AcademicEditor extends HindawiBaseModel
  implements AcademicEditorI {
  manuscriptId: string
  journalId: string
  userId: string
  email: string

  manuscripts?: Manuscript[]
  user?: User

  static get tableName() {
    return 'academic_editor'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        manuscriptId: { type: 'string', format: 'uuid' },
        journalId: { type: 'string', format: 'uuid' },
        userId: { type: 'string', format: 'uuid' }, // this should be the id from the sso
        email: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Manuscript,
        join: {
          from: 'academic_editor.manuscriptId',
          to: 'manuscript.id',
        },
      },
      user: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'academic_editor.userId',
          to: 'user.id',
        },
      },
    }
  }
}
