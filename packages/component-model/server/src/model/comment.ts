import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Review, ReviewI } from './review'
import { File, FileI } from './file'

export type CommentType = 'public' | 'private'

export interface CommentI extends HindawiBaseModelProps {
  type: CommentType
  content?: string
  reviewId: string

  review?: ReviewI
  files?: FileI[]

  assignFile(file: FileI): void
  toDTO(): object
}

export interface CommentRepository
  extends HindawiBaseModelRepository<CommentI> {
  new (...args: any[]): CommentI

  findOneByType({
    reviewId,
    type,
  }: {
    reviewId: string
    type: CommentType
  }): Promise<CommentI | undefined>
  Types: Record<CommentType, CommentType>
}

export class Comment extends HindawiBaseModel implements CommentI {
  type: CommentType
  content?: string
  reviewId: string

  review?: Review
  files?: File[]

  static get tableName() {
    return 'comment'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        type: { enum: Object.values(Comment.Types) },
        content: { type: ['string', 'null'] },
        reviewId: { type: 'string', format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      review: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Review,
        join: {
          from: 'comment.reviewId',
          to: 'review.id',
        },
      },
      files: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: File,
        join: {
          from: 'comment.id',
          to: 'file.commentId',
        },
      },
    }
  }

  static get Types(): Record<CommentType, CommentType> {
    return {
      public: 'public',
      private: 'private',
    }
  }

  static async findOneByType({
    reviewId,
    type,
  }: {
    reviewId: string
    type: CommentType
  }): Promise<Comment | undefined> {
    try {
      const results = await this.query()
        .select('c.*')
        .from('comment AS c')
        .join('review AS r', 'r.id', 'c.review_id')
        .where('c.review_id', reviewId)
        .andWhere('c.type', type)

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  assignFile(file: File) {
    this.files = this.files || []
    this.files.push(file)
  }

  toDTO() {
    return {
      ...this,
      files: this.files ? this.files.map((f: any) => f.toDTO()) : [],
    }
  }
}
