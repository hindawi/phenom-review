/* eslint-disable sonarjs/no-duplicate-string */
import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Journal, JournalI } from './journal'

export interface PeerReviewModelI extends HindawiBaseModelProps {
  name: string
  approvalEditors: string[]
  hasFigureheadEditor: boolean
  figureheadEditorLabel?: string
  hasSections: boolean
  hasTriageEditor: boolean
  triageEditorLabel: string
  triageEditorAssignmentTool: string[]
  academicEditorLabel: string
  academicEditorAutomaticInvitation: boolean
  reviewerAssignmentTool: string[]

  journal?: JournalI
}

export interface PeerReviewModelRepository
  extends HindawiBaseModelRepository<PeerReviewModelI> {
  new (...args: any[]): PeerReviewModelI

  findOneByManuscript(
    manuscriptId: string,
  ): Promise<PeerReviewModelI | undefined>
  findAllBySIs(siIds: string[]): Promise<PeerReviewModelI[]>
  findOneByJournal(journalId: string): Promise<PeerReviewModelI | undefined>
  findOneBySpecialIssue(
    specialIssueId: string,
  ): Promise<PeerReviewModelI | undefined>
  findOneByManuscriptParent(options: {
    journalId: string
    specialIssueId?: string
  }): Promise<PeerReviewModelI | undefined>

  Types: Record<string, string>
}

export class PeerReviewModel extends HindawiBaseModel
  implements PeerReviewModelI {
  name: string
  approvalEditors: string[]
  hasFigureheadEditor: boolean
  figureheadEditorLabel?: string
  hasSections: boolean
  hasTriageEditor: boolean
  triageEditorLabel: string
  triageEditorAssignmentTool: string[]
  academicEditorLabel: string
  academicEditorAutomaticInvitation: boolean
  reviewerAssignmentTool: string[]

  journal?: JournalI

  static get tableName() {
    return 'peer_review_model'
  }

  static get Types() {
    return {
      chiefMinus: 'Chief Minus',
      singleTierAcademicEditor: 'Single Tier Academic Editor',
      associateEditor: 'Associate Editor',
      specialIssueAssociateEditor: 'Special Issue Associate Editor',
      sectionEditor: 'Section Editor',
    }
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        name: { type: 'string' },
        approvalEditors: { type: 'array', items: { type: 'string' } },
        hasFigureheadEditor: { type: 'boolean' },
        figureheadEditorLabel: { type: ['string', null] },
        hasSections: { type: 'boolean' },
        hasTriageEditor: { type: 'boolean' },
        triageEditorLabel: { type: 'string' },
        triageEditorAssignmentTool: {
          type: 'array',
          items: { type: 'string' },
        },
        academicEditorLabel: { type: 'string' },
        academicEditorAutomaticInvitation: { type: 'boolean' },
        reviewerAssignmentTool: { type: 'array', items: { type: 'string' } },
      },
    }
  }

  static get jsonAttributes() {
    return []
  }

  static get relationMappings() {
    return {
      journal: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Journal,
        join: {
          from: 'peerReviewModel.id',
          to: 'journal.peerReviewModelId',
        },
      },
    }
  }

  static async findOneByManuscript(
    manuscriptId: string,
  ): Promise<PeerReviewModel> {
    try {
      const result = await this.query()
        .select('prm.*')
        .from('peer_review_model AS prm')
        .join('journal AS j', 'j.peer_review_model_id', 'prm.id')
        .join('manuscript AS m', 'm.journal_id', 'j.id')
        .where('m.id', manuscriptId)

      return result[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static findAllByJournals(journalIds: string[]) {
    return this.query()
      .select('prm.*', 'j.id as journalId')
      .from('peer_review_model as prm')
      .join('journal as j', 'prm.id', 'j.peer_review_model_id')
      .whereIn('j.id', journalIds)
  }

  static findAllBySIs(siIds: string[]): Promise<PeerReviewModelI[]> {
    return this.query()
      .select('prm.*', 'si.id as specialIssueId')
      .from('peer_review_model as prm')
      .join('special_issue as si', 'prm.id', 'si.peer_review_model_id')
      .whereIn('si.id', siIds)
  }

  static async findOneByJournal(journalId: string): Promise<PeerReviewModel> {
    try {
      const result = await this.query()
        .select('prm.*')
        .from('peer_review_model AS prm')
        .join('journal AS j', 'j.peer_review_model_id', 'prm.id')
        .where('j.id', journalId)

      return result[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneBySpecialIssue(
    specialIssueId: string,
  ): Promise<PeerReviewModel> {
    try {
      const result = await this.query()
        .select('prm.*')
        .from('peer_review_model AS prm')
        .join('special_issue AS si', 'si.peer_review_model_id', 'prm.id')
        .where('si.id', specialIssueId)

      return result[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByManuscriptParent({
    journalId,
    specialIssueId,
  }: {
    journalId: string
    specialIssueId?: string
  }) {
    if (specialIssueId) {
      return PeerReviewModel.findOneBySpecialIssue(specialIssueId)
    }
    return PeerReviewModel.findOneByJournal(journalId)
  }

  static findAllJournalOnesByManuscriptIds(
    manuscriptIds: string[],
  ): Promise<PeerReviewModelI[]> {
    return this.query()
      .select('prm.*', 'm.id as manuscriptId')
      .from('peer_review_model AS prm')
      .join('journal AS j', 'j.peer_review_model_id', 'prm.id')
      .join('manuscript AS m', 'm.journal_id', 'j.id')
      .whereIn('m.id', manuscriptIds)
  }

  static findAllSpecialIssueOnesByManuscriptIds(
    manuscriptIds: string[],
  ): Promise<PeerReviewModelI[]> {
    return this.query()
      .select('prm.*', 'm.id as manuscriptId')
      .from('peer_review_model AS prm')
      .join('special_issue AS si', 'si.peer_review_model_id', 'prm.id')
      .join('manuscript AS m', 'm.special_issue_id', 'si.id')
      .whereIn('m.id', manuscriptIds)
  }

  toDTO() {
    return { ...this }
  }
}
