import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Manuscript, ManuscriptI } from './manuscript'
import { User, UserI } from './user'

export interface EditorialAssistantI extends HindawiBaseModelProps {
  manuscriptId: string
  userId: string
  journalId: string
  status: string

  manuscripts?: ManuscriptI[]
  user?: UserI
}

export interface EditorialAssistantRepository
  extends HindawiBaseModelRepository<EditorialAssistantI> {
  new (...args: any[]): EditorialAssistantI
}

export class EditorialAssistant extends HindawiBaseModel
  implements EditorialAssistantI {
  manuscriptId: string
  userId: string
  journalId: string
  status: string

  manuscripts?: Manuscript[]
  user?: User

  static get tableName() {
    return 'editorial_assistant'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        manuscriptId: { type: 'string', format: 'uuid' },
        userId: { type: 'string', format: 'uuid' }, // this should be the id from the sso
        journalId: { type: 'string', format: 'uuid' },
        status: { type: 'string', format: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Manuscript,
        join: {
          from: 'editorial_assistant.manuscriptId',
          to: 'manuscript.id',
        },
      },
      users: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'editorial_assistant.userId',
          to: 'user.id',
        },
      },
    }
  }
}
