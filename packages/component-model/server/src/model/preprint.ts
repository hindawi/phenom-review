import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { JournalPreprint, JournalPreprintI } from './journalPreprint'

export interface PreprintI extends HindawiBaseModelProps {
  type: string
  format: string

  journalPreprints?: JournalPreprintI[]
}

export interface PreprintRepository
  extends HindawiBaseModelRepository<PreprintI> {
  new (...args: any[]): PreprintI
}

export class Preprint extends HindawiBaseModel implements PreprintI {
  type: string
  format: string

  journalPreprints?: JournalPreprintI[]

  static get tableName() {
    return 'preprint'
  }

  static get schema() {
    return {
      properties: {
        type: { type: 'string' },
        format: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {
      journalPreprints: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: JournalPreprint,
        join: {
          from: 'preprint.id',
          to: 'journalPreprint.preprintId',
        },
      },
    }
  }
}
