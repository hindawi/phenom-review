/* eslint-disable sonarjs/no-duplicate-string */
import { ValidationError } from '@pubsweet/errors'

import Objection, { transaction, raw } from 'objection'

import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { TeamMember, TeamMemberI } from './teamMember'
import { Manuscript, ManuscriptI } from './manuscript'
import { Journal, JournalI } from './journal'
import { Section, SectionI } from './section'
import { SpecialIssue, SpecialIssueI } from './specialIssue'
import { UserI } from './user'

const { logger } = require('component-logger')

export type TeamMemberRole =
  | 'author'
  | 'admin'
  | 'triageEditor'
  | 'reviewer'
  | 'academicEditor'
  | 'editorialAssistant'
  | 'submittingStaffMember'
  | 'researchIntegrityPublishingEditor'

export interface TeamI extends HindawiBaseModelProps {
  role?: TeamMemberRole
  manuscriptId?: string
  journalId?: string
  sectionId?: string
  specialIssueId?: string

  manuscript?: ManuscriptI
  journal?: JournalI
  section?: SectionI
  specialIssues?: SpecialIssueI[]
  members?: TeamMemberI[]

  addMember({
    user,
    options,
  }: {
    user: UserI
    options
  }): TeamMemberI | undefined
  changeCorrespondingTeamMember(
    newCorrespondingTeamMember: TeamMember,
  ): Promise<number>

  toDTO(): object
}

export interface TeamRepository extends HindawiBaseModelRepository<TeamI> {
  new (...args: any[]): TeamI

  findAllBy(options: {
    role: string
    manuscriptId: string
    submissionId: string
  }): Promise<TeamI[]>
  findOneByManuscriptAndRole(options: {
    role: string
    manuscriptId: string
    eagerLoadRelations: string | string[]
  }): Promise<TeamI | undefined>
  findOneByJournalAndRole(options: {
    role: string
    journalId: string
    eagerLoadRelations: string | string[]
  }): Promise<TeamI | undefined>
  findAllByJournal(journalId: string): Promise<TeamI[]>
  findOneByUserAndManuscriptId(options: {
    userId: string
    manuscriptId: string
  }): Promise<TeamI | undefined>
  findOneByManuscriptAndUserAndRole(options: {
    userId: string
    manuscriptId: string
    role: string
  }): Promise<TeamI | undefined>
  updateAndSaveMany(
    teams: Team[],
    trx: Objection.TransactionOrKnex,
  ): Promise<TeamI[]>

  Role: Record<TeamMemberRole, TeamMemberRole>
  JournalRoles: TeamMemberRole[]
  StaffRoles: TeamMemberRole[]
}

export class Team extends HindawiBaseModel implements TeamI {
  role?: TeamMemberRole
  manuscriptId?: string
  journalId?: string
  sectionId?: string
  specialIssueId?: string

  manuscript?: ManuscriptI
  journal?: JournalI
  section?: SectionI
  specialIssues?: SpecialIssueI[]
  members?: TeamMemberI[]

  static get tableName() {
    return 'team'
  }

  static get schema() {
    return {
      properties: {
        role: {
          type: 'string',
          enum: Object.values(Team.Role),
          default: null,
        },
        manuscriptId: { type: ['string', 'null'] },
        journalId: { type: ['string', 'null'] },
        sectionId: { type: ['string', 'null'] },
        specialIssueId: { type: ['string', 'null'] },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Manuscript,
        join: {
          from: 'team.manuscriptId',
          to: 'manuscript.id',
        },
      },
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Journal,
        join: {
          from: 'team.journalId',
          to: 'journal.id',
        },
      },
      section: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Section,
        join: {
          from: 'team.sectionId',
          to: 'section.id',
        },
      },
      specialIssues: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: SpecialIssue,
        join: {
          from: 'team.specialIssueId',
          to: 'special_issue.id',
        },
      },
      members: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: TeamMember,
        join: {
          from: 'team.id',
          to: 'team_member.teamId',
        },
      },
    }
  }

  static async findAllBy({
    role,
    manuscriptId,
    submissionId,
  }: {
    role: string
    manuscriptId: string
    submissionId: string
  }): Promise<Team[]> {
    try {
      return this.query()
        .skipUndefined()
        .select('t.*')
        .from('team AS t')
        .leftJoin('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('t.role', role)
        .andWhere('m.id', manuscriptId)
        .andWhere('m.submissionId', submissionId)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByManuscriptAndRole({
    role,
    manuscriptId,
    eagerLoadRelations,
  }: {
    role: string
    manuscriptId: string
    eagerLoadRelations: string | string[]
  }): Promise<Team> {
    try {
      return this.query()
        .select('t.*')
        .from('team as t')
        .join('manuscript as m', 'm.id', 't.manuscript_id')
        .where('m.id', manuscriptId)
        .andWhere('t.role', role)
        .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
        .first()
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByJournalAndRole({
    role,
    journalId,
    eagerLoadRelations,
  }: {
    role: string
    journalId: string
    eagerLoadRelations: string | string[]
  }): Promise<Team> {
    try {
      const results = await this.query()
        .select('t.*')
        .from('team as t')
        .join('journal as j', 'j.id', 't.journal_id')
        .where('j.id', journalId)
        .andWhere('t.role', role)
        .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllByJournal(journalId: string): Promise<Team[]> {
    try {
      return this.query()
        .select(
          't.*',
          raw('null as special_issue_name'),
          raw('null as section_name'),
        )
        .withGraphFetched('members')
        .from('team AS t')
        .where('t.journal_id', journalId)
        .union(function unionSections(this: any) {
          this.select(
            't.*',
            raw('null as special_issue_name'),
            's.name AS section_name',
          )
            .withGraphFetched('members')
            .from('team AS t')
            .leftJoin('section AS s', 's.id', 't.section_id')
            .where('s.journal_id', journalId)
        })
        .union(function unionSpecialIssues(this: any) {
          this.select(
            't.*',
            'si.name AS special_issue_name',
            raw('null as section_name'),
          )
            .withGraphFetched('members')
            .from('team AS t')
            .leftJoin('special_issue AS si', 'si.id', 't.special_issue_id')
            .where('si.journal_id', journalId)
        })
        .union(function unionSpecialIssuesSections(this: any) {
          this.select(
            't.*',
            'si.name AS special_issue_name',
            's.name AS section_name',
          )
            .withGraphFetched('members')
            .from('team AS t')
            .leftJoin('special_issue AS si', 'si.id', 't.special_issue_id')
            .leftJoin('section AS s', 's.id', 'si.section_id')
            .where('s.journal_id', journalId)
        })
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByUserAndJournalId({
    userId,
    journalId,
  }: {
    userId: string
    journalId: string
  }) {
    try {
      const results = await this.query()
        .select('t.*')
        .from('team AS t')
        .join('journal AS j', 't.journal_id', 'j.id')
        .join('team_member AS tm', 't.id', 'tm.team_id')
        .where('tm.user_id', userId)
        .andWhere('j.id', journalId)

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByUserAndManuscriptId({
    userId,
    manuscriptId,
  }: {
    userId: string
    manuscriptId: string
  }): Promise<Team> {
    return this.query()
      .alias('t')
      .join('team_member AS tm', 't.id', 'tm.team_id')
      .join('manuscript AS m', 't.manuscript_id', 'm.id')
      .where('tm.user_id', userId)
      .andWhere('m.id', manuscriptId)
      .first()
  }

  static async findOneByManuscriptAndUserAndRole({
    role,
    userId,
    manuscriptId,
  }: {
    userId: string
    manuscriptId: string
    role: string
  }): Promise<Team> {
    try {
      return this.query()
        .select('t.*')
        .from('team AS t')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .join('team_member AS tm', 't.id', 'tm.team_id')
        .where('tm.user_id', userId)
        .andWhere('t.manuscript_id', manuscriptId)
        .andWhere('t.role', role)
        .first()
    } catch (e) {
      throw new Error(e)
    }
  }
  static async findAllBySection({ sectionId }: { sectionId: string }) {
    try {
      return this.query()
        .select('t.*', 's.name AS sectionName')
        .from('team AS t')
        .join('section AS s', 't.section_id', 's.id')
        .where('s.id', sectionId)
        .withGraphFetched('members')
    } catch (e) {
      throw new Error(e)
    }
  }
  static async findAllBySpecialIssue({
    specialIssueId,
  }: {
    specialIssueId: string
  }) {
    try {
      return this.query()
        .select('t.*', 'si.name AS specialIssueName')
        .from('team AS t')
        .join('special_issue AS si', 't.special_issue_id', 'si.id')
        .where('si.id', specialIssueId)
        .withGraphFetched('members')
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllBySubmissionAndRole({
    submissionId,
    role,
  }: {
    submissionId: string
    role: string
  }) {
    try {
      return this.query()
        .from('team AS t')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('m.submission_id', submissionId)
        .andWhere('t.role', role)
    } catch (e) {
      throw new Error(e)
    }
  }

  static get Role(): Record<TeamMemberRole, TeamMemberRole> {
    return {
      author: 'author',
      admin: 'admin',
      triageEditor: 'triageEditor',
      reviewer: 'reviewer',
      academicEditor: 'academicEditor',
      editorialAssistant: 'editorialAssistant',
      submittingStaffMember: 'submittingStaffMember',
      researchIntegrityPublishingEditor: 'researchIntegrityPublishingEditor',
    }
  }

  static get JournalRoles(): TeamMemberRole[] {
    return [
      this.Role.triageEditor,
      this.Role.academicEditor,
      this.Role.editorialAssistant,
      this.Role.researchIntegrityPublishingEditor,
    ]
  }

  static get StaffRoles(): TeamMemberRole[] {
    return [
      this.Role.admin,
      this.Role.editorialAssistant,
      this.Role.researchIntegrityPublishingEditor,
    ]
  }

  addMember({
    user,
    options,
  }: {
    user: UserI
    options
  }): TeamMemberI | undefined {
    this.members = this.members || []
    const existingMember = this.members.find(
      member => member.userId === user.id,
    )
    const { defaultIdentity } = user
    if (!defaultIdentity) {
      return undefined
    }

    if (existingMember) {
      if (existingMember.status === TeamMember.Statuses.removed) {
        throw new Error(
          `${this.role} invitation for ${defaultIdentity.email} was removed and can't be invited again`,
        )
      }
      if (existingMember.status === TeamMember.Statuses.expired) {
        existingMember.updateProperties({
          status: TeamMember.Statuses.pending,
        })
        return existingMember
      }
      throw new ValidationError(
        `User ${defaultIdentity.email} is already invited as ${this.role}`,
      )
    }

    const newMember = new TeamMember({
      ...options,
      userId: user.id,
      teamId: this.id,
      position: this.members.length,
    })
    newMember.setAlias((options && options.alias) || defaultIdentity)
    newMember.user = user

    this.members.push(newMember)

    return newMember
  }

  async findCorrespondingTeamMember(
    trx: Objection.TransactionOrKnex,
  ): Promise<any> {
    return (this.$relatedQuery('members', trx) as any)
      .where('is_corresponding', true)
      .first()
  }

  static async updateAndSaveMany(
    teams: Team[],
    trx: Objection.TransactionOrKnex,
  ): Promise<Team[]> {
    return this.query(trx).upsertGraph(teams, {
      insertMissing: true,
      relate: true,
    })
  }

  async changeCorrespondingTeamMember(
    newCorrespondingTeamMember: TeamMember,
  ): Promise<number> {
    try {
      return transaction(Team.knex(), async trx => {
        const currentCorrespondingTeamMember = await this.findCorrespondingTeamMember(
          trx,
        )
        if (currentCorrespondingTeamMember) {
          await currentCorrespondingTeamMember.$query(trx).update({
            alias: currentCorrespondingTeamMember.alias,
            isCorresponding: false,
          })
        }

        return newCorrespondingTeamMember.$query(trx).update({
          alias: newCorrespondingTeamMember.alias,
          isCorresponding: true,
        })
      })
    } catch (err) {
      logger.error(
        'Something went wrong while trying to change the corresponding team member',
        err,
      )
      throw new Error(
        'Something went wrong while trying to change the corresponding team member',
      )
    }
  }

  toDTO() {
    const object = this.manuscript || this.journal

    return {
      ...this,
      object: object ? object.toDTO() : undefined,
      objectType: this.manuscriptId ? 'manuscript' : 'journal',
      members: this.members
        ? this.members.map((m: any) => m.toDTO())
        : undefined,
    }
  }
}
