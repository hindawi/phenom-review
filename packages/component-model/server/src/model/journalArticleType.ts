import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { ArticleType, ArticleTypeI } from './articleType'
import { Journal, JournalI } from './journal'

export interface JournalArticleTypeI extends HindawiBaseModelProps {
  journalId: string
  articleTypeId: string

  journal?: JournalI
  articleType?: ArticleTypeI
}

export interface JournalArticleTypeRepository
  extends HindawiBaseModelRepository<JournalArticleTypeI> {
  new (...args: any[]): JournalArticleTypeI

  findAllByJournals(journalIds: string[]): Promise<JournalArticleTypeI[]>
  findAllByArticleType(articleTypeId: string): Promise<JournalArticleTypeI[]>
}

export class JournalArticleType extends HindawiBaseModel
  implements JournalArticleTypeI {
  journalId: string
  articleTypeId: string

  journal?: JournalI
  articleType?: ArticleTypeI

  static get tableName() {
    return 'journal_article_type'
  }

  static get schema() {
    return {
      properties: {
        journalId: { type: 'string', format: 'uuid' },
        articleTypeId: { type: 'string', format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Journal,
        join: {
          from: 'journal_article_type.journalId',
          to: 'journal.id',
        },
      },
      articleType: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: ArticleType,
        join: {
          from: 'journal_article_type.articleTypeId',
          to: 'article_type.id',
        },
      },
    }
  }

  static findAllByJournals(
    journalIds: string[],
  ): Promise<JournalArticleType[]> {
    return this.query()
      .select('journalId', 'articleType.*')
      .join('articleType', 'articleType.id', 'articleTypeId')
      .whereIn('journalId', journalIds)
  }

  static findAllByArticleType(
    articleTypeId: string,
  ): Promise<JournalArticleType[]> {
    return this.query()
      .distinct('journalId')
      .join('journal as j', 'j.id', 'journalId')
      .where('articleTypeId', articleTypeId)
      .andWhere('j.isActive', true)
  }
}
