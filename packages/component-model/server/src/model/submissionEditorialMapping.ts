/**
 * we need a mapping entity so we can save the configuration that was chosen for a submission.
 */

import HindawiBaseModel, {
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Manuscript, ManuscriptI } from './manuscript'
import {
  ISubmissionEditorialModel,
  SubmissionEditorialModelSchema,
} from './submissionEditorialModel'

export interface SubmissionEditorialMappingI {
  submissionId: string
  submissionEditorialModel: ISubmissionEditorialModel
  save: (trx) => Promise<SubmissionEditorialMappingI>

  manuscript?: ManuscriptI
}

export interface SubmissionEditorialMappingRepository
  extends HindawiBaseModelRepository<SubmissionEditorialMappingI> {
  new (...args: any[]): SubmissionEditorialMappingI
}

export class SubmissionEditorialMapping extends HindawiBaseModel
  implements SubmissionEditorialMappingI {
  submissionId: string
  submissionEditorialModel: ISubmissionEditorialModel

  manuscript?: ManuscriptI

  static get tableName() {
    return 'submission_editorial_mapping'
  }
  static get schema() {
    return {
      properties: {
        submissionId: { type: 'string', format: 'uuid' },
        submissionEditorialModel: SubmissionEditorialModelSchema,
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Manuscript,
        join: {
          from: 'submission_editorial_mapping.submissionId',
          to: 'manuscript.submissionId',
        },
      },
    }
  }
}
