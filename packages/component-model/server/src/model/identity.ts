import bcrypt from 'bcrypt'
import config from 'config'

import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { User, UserI } from './user'

export type PersonTitle = 'mr' | 'mrs' | 'miss' | 'ms' | 'dr' | 'prof'
export interface IdentityI extends HindawiBaseModelProps {
  type: string
  isConfirmed: boolean
  passwordHash?: string
  email?: string
  aff?: string
  affRorId?: string
  country?: string
  surname?: string
  givenNames?: string
  title?: PersonTitle
  identifier?: string
  userId: string

  user?: UserI

  validPassword(password: string)
}

export interface IdentityRepository
  extends HindawiBaseModelRepository<IdentityI> {
  new (...args: any[]): IdentityI

  hashPassword(password: string): string
}

const BCRYPT_COST = config.util.getEnv('NODE_ENV') === 'test' ? 1 : 12

export class Identity extends HindawiBaseModel implements IdentityI {
  type: string
  isConfirmed: boolean
  passwordHash?: string
  email?: string
  aff?: string
  affRorId?: string
  country?: string
  surname?: string
  givenNames?: string
  title?: PersonTitle
  identifier?: string
  userId: string

  user?: User

  static get tableName() {
    return 'identity'
  }

  static get schema() {
    return {
      properties: {
        type: { type: 'string' },
        isConfirmed: { type: 'boolean' },
        passwordHash: { type: ['string', 'null'] },
        email: { type: ['string', 'null'] },
        aff: { type: ['string', 'null'] },
        affRorId: { type: ['string', 'null'] },
        country: { type: ['string', 'null'] },
        surname: { type: ['string', 'null'] },
        givenNames: { type: ['string', 'null'] },
        title: {
          type: ['string', 'null'],
          enum: ['mr', 'mrs', 'miss', 'ms', 'dr', 'prof', null],
        },
        identifier: { type: ['string', 'null'] },
        userId: { type: 'string', format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      user: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'identity.userId',
          to: 'user.id',
        },
      },
    }
  }

  $formatDatabaseJson(json: any) {
    json = super.$formatDatabaseJson(json)
    const { email } = json

    if (email) {
      return { ...json, email: email.toLowerCase() }
    }

    return json
  }

  async validPassword(password: string) {
    if (!this.passwordHash) return
    return bcrypt.compare(password, this.passwordHash)
  }

  static hashPassword(password: string) {
    return bcrypt.hashSync(password, BCRYPT_COST)
  }

  toDTO() {
    return {
      email: this.email,
      aff: this.aff,
      isConfirmed: this.isConfirmed,
      country: this.country,
      name: {
        surname: this.surname,
        givenNames: this.givenNames,
        title: this.title,
      },
      identifier: this.identifier,
    }
  }
}
