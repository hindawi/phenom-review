import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Manuscript, ManuscriptI } from './manuscript'

export type ReviewerSuggestionType = 'publons'

export interface ReviewerSuggestionI extends HindawiBaseModelProps {
  manuscriptId: string
  givenNames: string
  surname: string
  numberOfReviews: number
  aff: string
  email: string
  profileUrl: string
  isInvited: boolean
  type: ReviewerSuggestionType
  affRorId?: string

  manuscript?: ManuscriptI
}

export interface ReviewerSuggestionRepository
  extends HindawiBaseModelRepository<ReviewerSuggestionI> {
  new (...args: any[]): ReviewerSuggestionI
}

export class ReviewerSuggestion extends HindawiBaseModel
  implements ReviewerSuggestionI {
  manuscriptId: string
  givenNames: string
  surname: string
  numberOfReviews: number
  aff: string
  email: string
  profileUrl: string
  isInvited: boolean
  type: ReviewerSuggestionType
  affRorId?: string

  manuscript?: Manuscript

  static get tableName() {
    return 'reviewer_suggestion'
  }

  static get schema() {
    return {
      properties: {
        manuscriptId: { type: 'string', format: 'uuid' },
        givenNames: { type: 'string' },
        surname: { type: 'string' },
        numberOfReviews: { type: 'integer' },
        aff: { type: 'string' },
        email: { type: 'string' },
        profileUrl: { type: 'string' },
        isInvited: { type: 'boolean', default: false },
        type: { type: 'string', enum: ['publons'] },
        affRorId: { type: ['string', 'null'] },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Manuscript,
        join: {
          from: 'reviewer_suggestion.manuscriptId',
          to: 'manuscript.id',
        },
      },
    }
  }

  $formatDatabaseJson(json: any) {
    json = super.$formatDatabaseJson(json)
    const { email } = json

    if (email) {
      return { ...json, email: email.toLowerCase() }
    }

    return json
  }

  toDTO() {
    return { ...this }
  }
}
