export interface ISubmissionEditorialModel {
  name: string
  hasEditorialAssistant: boolean
  hasTransfers: boolean
  hasPreprints: boolean
  hasCoverLetter: boolean
  hasSupplementalMaterials: boolean
  hasAbstract: boolean
  hasPrefilledTitle: boolean
  hasCOIDeclaration: boolean
  hasDataAvailabilityStatement: boolean
  hasFundingStatement: boolean
  hasTermsAndConditions: boolean
}

export const SubmissionEditorialModelSchema = {
  type: 'object',
  properties: {
    name: { type: ['string', null] },
    hasTransfers: { type: ['boolean', false] },
    hasEditorialAssistant: { type: ['boolean', false] },
    hasPreprints: { type: ['boolean', false] },
    hasCoverLetter: { type: ['boolean', false] },
    hasSupplementalMaterials: { type: ['boolean', false] },
    hasAbstract: { type: ['boolean', false] },
    hasPrefilledTitle: { type: ['boolean', false] },
    hasCOIDeclaration: { type: ['boolean', false] },
    hasDataAvailabilityStatement: { type: ['boolean', false] },
    hasFundingStatement: { type: ['boolean', false] },
    hasTermsAndConditions: { type: ['boolean', false] },
  },
}
