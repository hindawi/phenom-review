import { get } from 'lodash'

import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Manuscript, ManuscriptI } from './manuscript'
import { TeamMember, TeamMemberI, TeamMemberRepository } from './teamMember'
import { Identity, IdentityI, IdentityRepository } from './identity'
import { User, UserI } from './user'

export type AuditLogObjectType = 'manuscript' | 'user' | 'review' | 'file'

export interface AuditLogI extends HindawiBaseModelProps {
  userId?: string
  manuscriptId?: string
  action: string
  objectType: AuditLogObjectType
  objectId: string
  journalId: string

  manuscript?: ManuscriptI
  user?: UserI

  version: unknown
}

export interface AuditLogRepository
  extends HindawiBaseModelRepository<AuditLogI> {
  new (...args: any[]): AuditLogI
  findAllBySubmissionId(submissionId: string): Promise<AuditLogI[]>
  getTarget({
    TeamMember,
    log,
    Identity,
  }: {
    TeamMember: TeamMemberRepository
    log: AuditLogI
    Identity: IdentityRepository
  }): Promise<TeamMemberI | IdentityI | undefined>
}

export class AuditLog extends HindawiBaseModel implements AuditLogI {
  userId?: string
  manuscriptId?: string
  action: string
  objectType: AuditLogObjectType
  objectId: string
  journalId: string

  manuscript?: Manuscript
  user?: User

  version: unknown

  static get tableName() {
    return 'audit_log'
  }

  static get schema() {
    return {
      properties: {
        userId: { type: ['string', null], format: 'uuid' },
        manuscriptId: { type: ['string', null], format: 'uuid' },
        action: {
          type: 'string',
        },
        objectType: {
          type: 'string',
          enum: ['manuscript', 'user', 'review', 'file'],
        },
        objectId: { type: 'string', format: 'uuid' },
        journalId: { type: 'string', format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      user: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'audit_log.userId',
          to: 'user.id',
        },
      },
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Manuscript,
        join: {
          from: 'audit_log.manuscriptId',
          to: 'manuscript.id',
        },
      },
    }
  }
  static async findAllBySubmissionId(
    submissionId: string,
  ): Promise<AuditLogI[]> {
    try {
      return await this.query()
        .select('au.*', 'm.version')
        .from('audit_log AS au')
        .join('manuscript AS m', 'au.manuscript_id', 'm.id')
        .where('m.submissionId', submissionId)
    } catch (e) {
      await AuditLog.getTarget({
        TeamMember,
        Identity,
        log: {} as any,
      })
      throw new Error(e)
    }
  }

  static async getTarget({
    TeamMember,
    log,
    Identity,
  }: {
    log: AuditLogI
    TeamMember: TeamMemberRepository
    Identity: IdentityRepository
  }) {
    let target: TeamMemberI | IdentityI | undefined
    target = await TeamMember.findOneByManuscriptAndUser({
      userId: log.objectId,
      manuscriptId: log.manuscriptId!,
      eagerLoadRelations: 'team',
    })
    if (target) return target

    target = await Identity.findOneBy({
      queryObject: { userId: log.objectId },
    })
    return target
  }

  transformLog({ userMember, target, activityLogEvents }: any) {
    return {
      id: this.id,
      created: this.created,
      version: this.version,
      action: get(activityLogEvents, this.action),
      user: {
        role: get(userMember, 'role'),
        email: get(userMember, 'alias.email'),
        reviewerNumber: get(userMember, 'reviewerNumber'),
      },
      target:
        target && target.team && target.team.role
          ? {
              role: target.team.role,
              email: target.getEmail(),
              reviewerNumber: target.reviewerNumber,
            }
          : {
              email: get(target, 'email'),
            },
    }
  }

  toDTO() {
    return {
      ...this,
      user: this.user ? this.user.toDTO() : undefined,
    }
  }
}
