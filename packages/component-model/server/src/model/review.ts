/* eslint-disable sonarjs/no-duplicate-string */
import { ValidationError } from '@pubsweet/errors'

import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Comment, CommentI } from './comment'
import { Manuscript, ManuscriptI } from './manuscript'
import { RejectDecisionInfo, RejectDecisionInfoI } from './rejectDecisionInfo'
import { TeamMemberRole } from './team'
import { TeamMember, TeamMemberI } from './teamMember'

export type ReviewRecommendation =
  | 'responseToRevision'
  | 'minor'
  | 'major'
  | 'reject'
  | 'publish'
  | 'revision'
  | 'returnToAcademicEditor'

export interface ReviewI extends HindawiBaseModelProps {
  recommendation: ReviewRecommendation
  manuscriptId: string
  teamMemberId: string
  rejectDecisionInfoId?: string
  submitted?: string
  isValid: boolean

  comments?: CommentI[]
  rejectDecisionInfo?: RejectDecisionInfoI
  manuscript?: ManuscriptI
  member?: TeamMemberI

  addComment(options: { content: string; type: string }): CommentI
  setSubmitted(date: string): void
  assignMember(teamMember: TeamMember): void
}

export interface ReviewRepository extends HindawiBaseModelRepository<ReviewI> {
  new (...args: any[]): ReviewI

  findAllByManuscriptAndTeamMember(options: {
    manuscriptId: string
    teamMemberId: string
  }): Promise<ReviewI[]>
  findOneByManuscriptAndTeamMember(options: {
    manuscriptId: string
    teamMemberId: string
  }): Promise<ReviewI | undefined>
  findAllValidAndSubmittedByManuscriptAndRole(options: {
    manuscriptId: string
    role: string
  }): Promise<ReviewI[]>
  findAllValidAndSubmitedBySubmissionAndRole(options: {
    submissionId: string
    role: string
  }): Promise<ReviewI[]>
  findAllValidByManuscriptAndRole(options: {
    manuscriptId: string
    role: string
  }): Promise<ReviewI[]>
  findLatestEditorialReview(options: {
    manuscriptId: string
    TeamRole: Record<TeamMemberRole, TeamMemberRole>
  }): Promise<ReviewI | undefined>
  findLatestValidAndSubmittedReviewBySubmissionAndRoles(options: {
    submissionId: string
    roles: string[]
  }): Promise<ReviewI>
  findAllValidByManuscriptId(options: {
    manuscriptId: string
    eagerLoadRelations: string | string[]
  }): Promise<ReviewI[]>
  findOneValidByTeamMember(teamMemberId: string): Promise<ReviewI | undefined>
}

export class Review extends HindawiBaseModel implements ReviewI {
  recommendation: ReviewRecommendation
  manuscriptId: string
  teamMemberId: string
  rejectDecisionInfoId?: string | undefined
  submitted?: string | undefined
  isValid: boolean

  comments?: Comment[]
  rejectDecisionInfo?: RejectDecisionInfo
  manuscript?: Manuscript
  member?: TeamMember

  static get tableName() {
    return 'review'
  }

  static get schema() {
    return {
      properties: {
        recommendation: { enum: Object.values(Review.Recommendations) },
        manuscriptId: { type: 'string', format: 'uuid' },
        teamMemberId: { type: 'string', format: 'uuid' },
        rejectDecisionInfoId: { type: ['string', 'null'], format: 'uuid' },
        submitted: { type: ['string', 'object', 'null'], format: 'date-time' },
        isValid: { type: 'boolean', default: true },
      },
    }
  }

  static get relationMappings() {
    return {
      comments: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Comment,
        join: {
          from: 'review.id',
          to: 'comment.reviewId',
        },
      },
      rejectDecisionInfo: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: RejectDecisionInfo,
        join: {
          from: 'review.rejectDecisionInfoId',
          to: 'reject_decision_info.id',
        },
      },
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: Manuscript,
        join: {
          from: 'review.manuscriptId',
          to: 'manuscript.id',
        },
      },
      member: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: TeamMember,
        join: {
          from: 'review.teamMemberId',
          to: 'team_member.id',
        },
      },
    }
  }

  static get Recommendations(): Record<
    ReviewRecommendation,
    ReviewRecommendation
  > {
    return {
      responseToRevision: 'responseToRevision',
      minor: 'minor',
      major: 'major',
      reject: 'reject',
      publish: 'publish',
      revision: 'revision',
      returnToAcademicEditor: 'returnToAcademicEditor',
    }
  }

  static findAllByManuscriptAndTeamMember({
    manuscriptId,
    teamMemberId,
  }: {
    manuscriptId: string
    teamMemberId: string
  }) {
    return this.query()
      .alias('r')
      .join('team_member as tm', 'tm.id', 'r.team_member_id')
      .where('r.manuscript_id', manuscriptId)
      .andWhere('r.team_member_id', teamMemberId)
  }

  static findOneByManuscriptAndTeamMember({
    manuscriptId,
    teamMemberId,
  }: {
    manuscriptId: string
    teamMemberId: string
  }): Promise<Review> {
    return this.findAllByManuscriptAndTeamMember({ manuscriptId, teamMemberId })
      .clone()
      .limit(1)
      .first()
  }

  static async findOneInvalidByManuscriptAndRole({
    manuscriptId,
    role,
  }: {
    manuscriptId: string
    role: string
  }): Promise<Review> {
    return this.query()
      .alias('r')
      .join('team AS t', 't.manuscript_id', 'r.manuscript_id')
      .where('r.manuscript_id', manuscriptId)
      .andWhere('r.isValid', false)
      .andWhere('t.role', role)
      .first()
  }

  static async findAllValidAndSubmittedByManuscriptAndRole({
    role,
    manuscriptId,
  }: {
    manuscriptId: string
    role: string
  }): Promise<Review[]> {
    return this.query()
      .alias('r')
      .join('team_member AS tm', 'tm.id', 'r.team_member_id')
      .join('team AS t', 't.id', 'tm.team_id')
      .whereNotNull('r.submitted')
      .andWhere('r.manuscript_id', manuscriptId)
      .andWhere('t.role', role)
      .andWhere('r.is_valid', true)
  }

  static async findAllValidAndSubmitedBySubmissionAndRole({
    role,
    submissionId,
  }: {
    submissionId: string
    role: string
  }): Promise<Review[]> {
    return this.query()
      .alias('r')
      .join('team_member AS tm', 'tm.id', 'r.team_member_id')
      .join('team AS t', 't.id', 'tm.team_id')
      .join('manuscript as m', 'm.id', 't.manuscript_id')
      .where('m.submission_id', submissionId)
      .andWhere('t.role', role)
      .andWhere('r.is_valid', true)
      .whereNotNull('r.submitted')
  }

  static findAllByManuscriptAndRole({
    manuscriptId,
    role,
    eagerLoadRelations,
  }: any) {
    return this.query()
      .alias('r')
      .join('team_member AS tm', 'tm.id', 'r.team_member_id')
      .join('team AS t', 't.id', 'tm.team_id')
      .where('r.manuscript_id', manuscriptId)
      .andWhere('t.role', role)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }
  static findAllSubmittedByManuscriptAndRole({
    manuscriptId,
    role,
    eagerLoadRelations,
  }: {
    manuscriptId: string
    role: string
    eagerLoadRelations: string | string[]
  }) {
    return this.findAllByManuscriptAndRole({
      manuscriptId,
      role,
      eagerLoadRelations,
    })
      .clone()
      .whereNotNull('r.submitted')
  }

  static async findAllValidByManuscriptAndRole({
    manuscriptId,
    role,
  }: {
    manuscriptId: string
    role: string
  }): Promise<Review[]> {
    return this.query()
      .alias('r')
      .join('team_member AS tm', 'tm.id', 'r.team_member_id')
      .join('team AS t', 't.id', 'tm.team_id')
      .where('r.manuscript_id', manuscriptId)
      .andWhere('t.role', role)
      .andWhere('r.is_valid', true)
  }

  static async findLatestEditorialReview({
    manuscriptId,
    TeamRole,
  }: {
    manuscriptId: string
    TeamRole: Record<TeamMemberRole, TeamMemberRole>
  }): Promise<Review> {
    try {
      const results = await this.query()
        .select('r.*')
        .from('review AS r')
        .join('team_member AS tm', 'tm.id', '=', 'r.team_member_id')
        .join('team AS t', 't.id', '=', 'tm.team_id')
        .where('r.manuscript_id', '=', manuscriptId)
        .andWhere(function andWhereIn(this: any) {
          this.whereIn('t.role', [
            TeamRole.admin,
            TeamRole.editorialAssistant,
            TeamRole.triageEditor,
            TeamRole.academicEditor,
          ])
        })
        .orderBy('updated', 'desc')

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findLatestValidAndSubmittedReviewBySubmissionAndRoles({
    submissionId,
    roles,
  }: {
    submissionId: string
    roles: string[]
  }): Promise<Review> {
    try {
      const results = await this.query()
        .select('r.*')
        .from('review AS r')
        .join('team_member AS tm', 'tm.id', '=', 'r.team_member_id')
        .join('team AS t', 't.id', '=', 'tm.team_id')
        .join('manuscript as m', 'm.id', 't.manuscript_id')
        .where('m.submission_id', submissionId)
        .andWhere('r.is_valid', true)
        .whereNotNull('r.submitted')
        .andWhere((query: any) => query.whereIn('t.role', roles))
        .orderBy('updated', 'desc')

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllValidByManuscriptId({
    manuscriptId,
    eagerLoadRelations,
  }: {
    manuscriptId: string
    eagerLoadRelations: string | string[]
  }): Promise<Review[]> {
    return this.query()
      .alias('r')
      .where('r.manuscript_id', manuscriptId)
      .andWhere('r.isValid', true)
      .withGraphFetched(this._parseEagerRelations(eagerLoadRelations))
  }

  static async findLatestEditorialReviewOfSubmissionByRecommendationType({
    submissionId,
    TeamRole,
    recommendationType,
  }: {
    submissionId: string
    TeamRole: Record<TeamMemberRole, TeamMemberRole>
    recommendationType: ReviewRecommendation
  }) {
    try {
      const results = await this.query()
        .select('r.*')
        .from('review AS r')
        .join('manuscript AS m', 'r.manuscript_id', '=', 'm.id')
        .join('team_member AS tm', 'tm.id', '=', 'r.team_member_id')
        .join('team AS t', 't.id', '=', 'tm.team_id')
        .where('m.submission_id', submissionId)
        .andWhere((query: any) =>
          query.whereIn('t.role', [
            TeamRole.admin,
            TeamRole.editorialAssistant,
            TeamRole.triageEditor,
            TeamRole.academicEditor,
          ]),
        )
        .andWhere('r.recommendation', recommendationType)
        .orderBy('r.updated', 'desc')

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static findOneByTeamMember(teamMemberId: string) {
    try {
      return this.query().findOne('team_member_id', teamMemberId)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneValidByTeamMember(teamMemberId: string): Promise<Review> {
    return this.findOneByTeamMember(teamMemberId)
      .clone()
      .where('isValid', true)
  }

  addComment({ content, type }: { content: string; type: string }): CommentI {
    this.comments = this.comments || []

    const commentTypeAlreadyExists = this.comments.some(c => c.type === type)
    if (commentTypeAlreadyExists) {
      throw new ValidationError('Cannot add multiple comments of the same type')
    }
    const comment = new Comment({
      content,
      type,
    })

    this.comments.push(comment)

    return comment
  }

  setSubmitted(date: string): void {
    this.submitted = date
  }

  assignMember(teamMember: TeamMember): void {
    this.member = teamMember
  }

  toDTO() {
    return {
      ...this,
      comments: this.comments
        ? this.comments.map((comment: any) => comment.toDTO())
        : [],
      member: this.member ? this.member.toDTO() : undefined,
      rejectDecisionInfo: this.rejectDecisionInfo
        ? this.rejectDecisionInfo.toDTO()
        : undefined,
    }
  }
}
