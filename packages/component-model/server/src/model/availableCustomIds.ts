import Objection from 'objection'
import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'

export interface AvailableCustomIdsI extends HindawiBaseModelProps {
  customId: string
}

export interface AvailableCustomIdsRepository
  extends HindawiBaseModelRepository<AvailableCustomIdsI> {
  new (...args: any[]): AvailableCustomIdsI

  pullCustomId(
    trx: Objection.TransactionOrKnex,
  ): Promise<AvailableCustomIdsI | undefined>
  getNumberOfCustomIds(): Promise<AvailableCustomIdsI>
  deleteUsedCustomId(customId: string): Promise<number>
}

export class AvailableCustomIds extends HindawiBaseModel
  implements AvailableCustomIdsI {
  customId: string

  static get tableName() {
    return 'available_custom_ids'
  }

  static get schema() {
    return {
      properties: {
        customId: { type: ['string'] },
      },
    }
  }

  static async pullCustomId(
    trx: Objection.TransactionOrKnex,
  ): Promise<AvailableCustomIds | undefined> {
    return this.query(trx)
      .delete()
      .where(
        'custom_id',
        this.query()
          .select('custom_id')
          .from('available_custom_ids')
          .offset(Math.floor(Math.random() * 1000))
          .limit(1),
      )
      .returning('custom_id')
      .first()
  }

  static async getNumberOfCustomIds(): Promise<AvailableCustomIds> {
    return this.query()
      .count()
      .from('available_custom_ids')
      .first()
  }

  static async deleteUsedCustomId(customId: string): Promise<number> {
    return this.query()
      .delete()
      .where('custom_id', customId)
  }
}
