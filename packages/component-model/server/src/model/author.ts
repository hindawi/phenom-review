import HindawiBaseModel, {
  HindawiBaseModelProps,
  HindawiBaseModelRepository,
} from '../hindawiBaseModel'
import { Manuscript, ManuscriptI } from './manuscript'
import { User, UserI } from './user'

export interface AuthorI extends HindawiBaseModelProps {
  manuscriptId: string
  userId: string
  email: string

  manuscripts?: ManuscriptI[]
  user?: UserI
}

export interface AuthorRepository extends HindawiBaseModelRepository<AuthorI> {
  new (...args: any[]): AuthorI
}

export class Author extends HindawiBaseModel implements AuthorI {
  manuscriptId: string
  userId: string
  email: string

  manuscripts?: Manuscript[]
  user?: User

  static get tableName() {
    return 'author'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        manuscriptId: { type: 'string', format: 'uuid' },
        userId: { type: 'string', format: 'uuid' }, // this should be the id from the sso
        email: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Manuscript,
        join: {
          from: 'author.manuscriptId',
          to: 'manuscript.id',
        },
      },
      user: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'author.userId',
          to: 'user.id',
        },
      },
    }
  }
}
