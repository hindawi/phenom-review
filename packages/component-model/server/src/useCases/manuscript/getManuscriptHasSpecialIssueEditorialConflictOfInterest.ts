const hasConflictOfInterest = async ({ models, loaders, manuscript }) => {
  const { TeamMember } = models
  let authors = []
  let userIds = []
  if (manuscript.authors) {
    ;({ authors } = manuscript)
  } else {
    authors = loaders.Manuscript.authorsLoader.load(manuscript.id)
  }

  if (authors && authors.length > 0) {
    userIds = authors.map(a => a.userId)
  }

  // try to add a loader for this using cacheKeyFn in part 3
  const editor = await TeamMember.findOneBySpecialIssueAndUsers({
    specialIssueId: manuscript.specialIssueId,
    userIds,
  })

  return !!editor
}

export const initialize = ({ models, loaders }) => ({
  execute: async ({ manuscript }) => {
    if (
      typeof manuscript.hasSpecialIssueEditorialConflictOfInterest !==
      'undefined'
    )
      return manuscript.hasSpecialIssueEditorialConflictOfInterest

    const hasSpecialIssueEditorialConflictOfInterest = await hasConflictOfInterest(
      { models, loaders, manuscript },
    )
    return hasSpecialIssueEditorialConflictOfInterest
  },
})
