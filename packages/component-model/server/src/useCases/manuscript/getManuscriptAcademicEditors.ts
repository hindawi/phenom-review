export const initialize = ({ loaders }) => ({
  execute: async ({ manuscript }) => {
    if (manuscript.academicEditors) return manuscript.academicEditors

    return loaders.Manuscript.allAcademicEditorsLoader.load(manuscript.id)
  },
})
