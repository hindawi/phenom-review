const initialize = models => ({
  execute: async ({ manuscriptId, userId }) => {
    const { TeamMember } = models

    return TeamMember.isApprovalEditor({
      userId,
      models,
      manuscriptId,
    })
  },
})

module.exports = {
  initialize,
}
