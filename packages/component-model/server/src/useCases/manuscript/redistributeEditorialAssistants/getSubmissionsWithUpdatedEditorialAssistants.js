const { Promise } = require('bluebird')
const { logger } = require('component-logger')

const initialize = ({ models, useCases }) => ({
  execute: async ({
    submissions,
    averageWorkload,
    journalEditorialAssistant,
    journalEditorialAssistants,
  }) => {
    const { TeamMember } = models
    const editorRemovedSubmissions = []
    const editorAssignedSubmissions = []

    await Promise.each(submissions, async submission => {
      const { submissionId } = submission[0]

      // find EA with workload smaller than average
      const newEditorialAssistant = journalEditorialAssistants.find(
        ea => ea.workload < averageWorkload,
      )

      if (!newEditorialAssistant) {
        logger.warn(
          `No editorial assistant has a workload smaller than ${averageWorkload}`,
        )
        return
      }

      const {
        newEditorialAssistants,
        currentEditorialAssistants,
      } = await useCases.getUpdatedEditorialAssistantsUseCase
        .initialize({ logger, models, Promise })
        .execute({
          submission, // current submission in the extra submissions of the current EA in the iteration
          newEditorialAssistant, // the future EA to be assigned
          journalEditorialAssistant, // current EA
        })

      editorRemovedSubmissions.push(submissionId)
      editorAssignedSubmissions.push(submissionId)

      // update the workload in the editorialAssistants array
      const newEditorialAssistantIndex = journalEditorialAssistants.findIndex(
        ea => ea.userId === newEditorialAssistant.userId,
      )
      journalEditorialAssistants[newEditorialAssistantIndex].workload += 1

      await TeamMember.query().upsertGraph(
        [...newEditorialAssistants, ...currentEditorialAssistants],
        {
          relate: true,
        },
      )
    })

    return { editorRemovedSubmissions, editorAssignedSubmissions }
  },
})

module.exports = { initialize }
