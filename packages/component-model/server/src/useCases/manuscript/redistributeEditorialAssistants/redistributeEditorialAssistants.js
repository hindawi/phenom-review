const initialize = ({ logger, models, useCases, eventsService, Promise }) => ({
  execute: async journalId => {
    const { TeamMember, Team, Manuscript } = models
    // TO DO: figure out in progress statuses for EAs
    const editorialAssistantWorkingStatuses = [
      ...Manuscript.InProgressStatuses,
      Manuscript.Statuses.qualityChecksRequested,
      Manuscript.Statuses.qualityChecksSubmitted,
      Manuscript.Statuses.technicalChecks,
      Manuscript.Statuses.draft,
    ]

    /**
     * Get all editorial assistants assigned to a specific journal with workload property
     *
     * when calculating the workload: for the submissions in the draft status, there needs to exist an extra condition to have a submitted date.
     */
    const editorialAssistants = await TeamMember.findAllByJournalWithWorkload({
      journalId,
      role: Team.Role.editorialAssistant,
      teamMemberStatuses: [TeamMember.Statuses.active],
      manuscriptStatuses: editorialAssistantWorkingStatuses,
    })

    /**
     * Return if no EAs found
     */
    if (editorialAssistants.length < 2) {
      logger.warn(`No editorial assistants found on journal ${journalId}`)
      return
    }

    /**
     * calculate the average number of submissions
     *
     * for the submissions in the draft status, there needs to exist an extra condition to have a submitted date.
     */
    const {
      count: numberOfSubmissions,
    } = await Manuscript.countSubmissionsByJournalAndStatuses({
      journalId,
      statuses: editorialAssistantWorkingStatuses,
    })
    const averageWorkload = Math.floor(
      numberOfSubmissions / editorialAssistants.length,
    )

    /**
     * Iterate over EAs and check if they need to redistribute some of their submissions
     */
    await Promise.each(editorialAssistants, async editorialAssistant => {
      const numberOfExtraSubmissions =
        editorialAssistant.workload - averageWorkload

      /**
       * Return if they dont have any submissions more than the averate workload.
       * Continue otherwise
       */
      if (numberOfExtraSubmissions < 1) {
        logger.info(
          `User ${
            editorialAssistant.userId
          } with workload ${editorialAssistant.workload || 0} does 
          not have any extra submissions. Skipping...`,
        )
        return
      }

      /**
       * Get a number of [numberOfExtraSubmissions] submissions from the ones the EA is assigned on
       *
       * for the submissions in the draft status, there needs to exist an extra condition to have a submitted date.
       */
      const submissionIds = await Manuscript.findSubmissionsByJournalAndUserAndRoleAndTeamMemberStatuses(
        {
          journalId,
          limit: numberOfExtraSubmissions,
          userId: editorialAssistant.userId,
          role: Team.Role.editorialAssistant,
          manuscriptStatuses: editorialAssistantWorkingStatuses,
          teamMemberStatuses: [TeamMember.Statuses.active],
        },
      )
      const extraSubmissions = []
      await Promise.each(submissionIds, async submission => {
        const { submissionId } = submission
        const manuscriptsInSubmission = await Manuscript.findAll({
          queryObject: {
            submissionId,
          },
        })
        if (!manuscriptsInSubmission.length) return

        // do not add it to the submission list if it is already there
        if (
          extraSubmissions.find(
            submissionManuscripts =>
              submissionManuscripts[0].submissionId === submissionId,
          )
        )
          return

        extraSubmissions.push(manuscriptsInSubmission)
      })

      const {
        editorRemovedSubmissions,
        editorAssignedSubmissions,
      } = await useCases.getSubmissionsWithUpdatedEditorialAssistantsUseCase // altough it says get.. it also does the redistribution too
        .initialize({ models, useCases, Promise })
        .execute({
          submissions: extraSubmissions, // these are in fact manuscripts in a submission...
          averageWorkload,
          journalEditorialAssistant: editorialAssistant, // current EA in the iteration
          journalEditorialAssistants: editorialAssistants, // all EAs on the journal
        })
      editorRemovedSubmissions.forEach(submissionId => {
        eventsService.publishSubmissionEvent({
          submissionId,
          eventName: 'SubmissionEditorialAssistantRemoved',
        })
      })
      editorAssignedSubmissions.forEach(submissionId => {
        eventsService.publishSubmissionEvent({
          submissionId,
          eventName: 'SubmissionEditorialAssistantAssigned',
        })
      })
    })
  },
})

module.exports = {
  initialize,
}
