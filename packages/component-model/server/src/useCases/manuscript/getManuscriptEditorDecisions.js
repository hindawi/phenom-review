const findValidReviews = async ({ submissionId, models }) => {
  const { Review, Team } = models

  const latestMajorEditorialReview = await Review.findLatestEditorialReviewOfSubmissionByRecommendationType(
    {
      submissionId,
      TeamRole: Team.Role,
      recommendationType: Review.Recommendations.major,
    },
  )

  let reviews = await Review.findAllValidAndSubmitedBySubmissionAndRole({
    submissionId,
    role: Team.Role.reviewer,
  })
  if (
    latestMajorEditorialReview &&
    latestMajorEditorialReview.recommendation === Review.Recommendations.major
  ) {
    reviews = reviews.filter(
      review =>
        new Date(review.submitted) >
        new Date(latestMajorEditorialReview.submitted),
    )
  }

  return reviews
}

const initialize = ({ models, useCases, userId }) => ({
  execute: async ({ manuscript }) => {
    const { Manuscript, Team, Review, PeerReviewEditorialMapping } = models
    const { reject, publish, minor, major } = Review.Recommendations

    // As an approval editor I can't make any decision on a draft manuscript
    if (manuscript.status === Manuscript.Statuses.draft) return []
    // As an academic editor, i should be able to take a decision on the manuscript
    // if the manuscript has conflicts of interest between the triage editor
    // and the author

    const submissionReviewerReports = await findValidReviews({
      submissionId: manuscript.submissionId,
      models: {
        Manuscript,
        Review,
        Team,
      },
    })
    if (manuscript.hasTriageEditorConflictOfInterest) {
      if (submissionReviewerReports.length)
        return [publish, minor, major, reject]
      return [minor, major, reject]
    }

    // As an approval editor I can only publish or reject the manuscript, if
    // there is no peer review cycle
    if (!manuscript.articleType.hasPeerReview) return [publish, reject]

    const approvalEditor = await PeerReviewEditorialMapping.getApprovalEditorRoleForSubmission(
      manuscript.submissionId,
    )

    let useCase =
      'getManuscriptEditorDecisionsWhenAcademicEditorIsApprovalEditorUseCase'
    if (approvalEditor === Team.Role.triageEditor)
      useCase =
        'getManuscriptEditorDecisionsWhenTriageEditorIsApprovalEditorUseCase'

    return useCases[useCase]
      .initialize({ models, userId })
      .execute({ manuscript, submissionReviewerReports })
  },
})

module.exports = {
  initialize,
}
