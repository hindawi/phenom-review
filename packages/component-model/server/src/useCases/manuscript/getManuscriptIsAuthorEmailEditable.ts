const isEAOrAdmin = async ({ manuscript, userId, TeamMember, Team }) => {
  const editorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndUserAndStatus(
    {
      role: Team.Role.editorialAssistant,
      userId,
      manuscriptId: manuscript.id,
      status: TeamMember.Statuses.active,
    },
  )
  const admin = await TeamMember.findOneByUserAndRole({
    userId,
    role: Team.Role.admin,
  })

  if (editorialAssistant || admin) {
    return true
  }

  return false
}

export const initialize = models => ({
  execute: async ({ manuscript, userId }) => {
    const { TeamMember, Team, Manuscript } = models

    const currentUserIsEAOrAdmin = await isEAOrAdmin({
      manuscript,
      userId,
      TeamMember,
      Team,
    })
    const manuscriptIsInProgress = [
      ...Manuscript.InProgressStatuses,
      Manuscript.Statuses.draft,
    ].includes(manuscript.status)

    if (currentUserIsEAOrAdmin && manuscriptIsInProgress) {
      return true
    }

    return false
  },
})
