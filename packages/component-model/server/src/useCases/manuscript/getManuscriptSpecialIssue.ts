export const initialize = ({ loaders }) => ({
  execute: async ({ manuscript }) => {
    if (manuscript.specialIssue) return manuscript.specialIssue

    const specialIssue = await loaders.Manuscript.specialIssueLoader.load(
      manuscript.id,
    )
    return specialIssue?.shift()
  },
})
