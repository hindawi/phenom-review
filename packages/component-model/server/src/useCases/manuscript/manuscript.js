const initialize = ({ Manuscript }) => ({
  execute: async ({ manuscriptId }) => {
    const manuscript = await Manuscript.find(manuscriptId)

    return manuscript.toDTO()
  },
})

const authsomePolicies = ['hasAccessToManuscript', 'admin']

module.exports = {
  initialize,
  authsomePolicies,
}
