const initialize = models => ({
  execute: async ({ manuscript }) => {
    const { Section } = models
    if (manuscript.section) return manuscript.section

    if (manuscript.sectionId) {
      return Section.find(manuscript.sectionId)
    }
  },
})

module.exports = {
  initialize,
}
