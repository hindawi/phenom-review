const { orderBy } = require('lodash')

const initialize = models => ({
  execute: async ({ manuscript, userId }) => {
    const { File, TeamMember, Team } = models
    let requestTeamMember, files

    requestTeamMember = await TeamMember.findOneByUserAndRole({
      role: Team.Role.admin,
      userId,
      eagerLoadRelations: 'team',
    })
    if (!requestTeamMember) {
      requestTeamMember = await TeamMember.findOneByManuscriptAndUser({
        manuscriptId: manuscript.id,
        userId,
        eagerLoadRelations: 'team',
      })
    }
    if (
      requestTeamMember &&
      requestTeamMember.status === TeamMember.Statuses.pending &&
      [Team.Role.academicEditor, Team.Role.reviewer].includes(
        requestTeamMember.team.role,
      )
    ) {
      return []
    }

    if (manuscript.files && manuscript.files.length > 0) {
      files = manuscript.files
    } else {
      files = await File.findBy({ manuscriptId: manuscript.id })
    }

    return orderBy(files, ['type', 'position'], ['asc', 'asc']).map(a =>
      a.toDTO(),
    )
  },
})

module.exports = {
  initialize,
}
