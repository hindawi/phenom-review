const initialize = ({
  userId,
  models: { Team, TeamMember, Review, Manuscript },
}) => ({
  execute: async ({ manuscript, submissionReviewerReports }) => {
    const { reject, publish, minor, major } = Review.Recommendations

    if (manuscript.status === Manuscript.Statuses.academicEditorInvited) {
      return [reject]
    }

    const requestingUserIsAdminOrEA = await TeamMember.findOneByUserAndRoles({
      userId,
      roles: [Team.Role.admin, Team.Role.editorialAssistant],
    })
    const manuscriptReviewerReports = await Review.findAllValidAndSubmittedByManuscriptAndRole(
      { manuscriptId: manuscript.id, role: Team.Role.reviewer },
    )

    const pendingOrAcceptedAcademicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatuses(
      {
        role: Team.Role.academicEditor,
        statuses: [TeamMember.Statuses.pending, TeamMember.Statuses.accepted],
        manuscriptId: manuscript.id,
      },
    )

    const pendingOrAcceptedReviewer = await TeamMember.findOneByManuscriptAndRoleAndStatuses(
      {
        role: Team.Role.reviewer,
        statuses: [TeamMember.Statuses.pending, TeamMember.Statuses.accepted],
        manuscriptId: manuscript.id,
      },
    )
    const latestEditorialReview = await Review.findLatestValidAndSubmittedReviewBySubmissionAndRoles(
      {
        submissionId: manuscript.submissionId,
        roles: [
          Team.Role.admin,
          Team.Role.editorialAssistant,
          Team.Role.triageEditor,
          Team.Role.academicEditor,
        ],
      },
    )

    const latestEditorialRecommendationType =
      latestEditorialReview && latestEditorialReview.recommendation

    // if the last decision was a major revision, there are reviewers invited on current version and there is no report from them
    // we should display only the reject decision options to the approval editor
    if (
      pendingOrAcceptedReviewer &&
      !manuscriptReviewerReports.length &&
      latestEditorialRecommendationType === Review.Recommendations.major
    ) {
      return [reject]
    }

    // As an approval editor, i can publish, reject or ask for a minor/major revision if I
    // have a reviewer report on any previous version.
    // If there are no AEs invited, EA/admin/TE can then request minor/major or reject
    if (submissionReviewerReports.length) {
      if (!pendingOrAcceptedAcademicEditor) return [minor, major, reject]
      return [publish, minor, major, reject]
    }

    // As an EA or Admin I should only be able to reject the
    // manuscript if the academic editor is pending/accepted
    if (requestingUserIsAdminOrEA && pendingOrAcceptedAcademicEditor) {
      return [reject]
    }

    // he cannot publish without reviews
    return [minor, major, reject]
  },
})

module.exports = {
  initialize,
}
