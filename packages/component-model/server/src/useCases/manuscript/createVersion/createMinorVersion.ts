import { createVersion } from "./createVersion";

const computeVersionNumber = v => {
  const [major, minor = 0] = v.split(".");
  return `${major}.${+minor + 1}`;
};

const initialize = args => ({
  execute: async function({ manuscript }): Promise<void> {
    const version = computeVersionNumber(manuscript.version);

    await createVersion.initialize(args).execute({
      manuscript: {
        ...manuscript,
        isPostAcceptance: true, // this is not used anymore. keeping it for consistency only
      },
      version,
    });
  },
});

export const createMinorVersion = {
  initialize,
};
