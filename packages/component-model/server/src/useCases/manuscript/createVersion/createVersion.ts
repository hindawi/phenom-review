const { logger } = require('component-logger')

async function createManuscript({ manuscript, version, Manuscript, trx }) {
  const newVersion = new Manuscript({
    ...manuscript,
    version,
    status: Manuscript.Statuses.draft,
    isLatestVersion: false, // this is not yet the latest version. it will become after revision is submited
  })

  delete newVersion.id
  await newVersion.save(trx)

  return newVersion
}

async function createResponseReview({
  newVersionId,
  newAuthors,
  Review,
  Comment,
  trx = null,
}) {
  const newSubmittingAuthor = newAuthors.find(author => author.isSubmitting)

  const review = new Review({
    manuscriptId: newVersionId,
    recommendation: Review.Recommendations.responseToRevision,
    teamMemberId: newSubmittingAuthor.id,
  })

  await review.save(trx)

  const comment = new Comment({
    reviewId: review.id,
    type: Comment.Types.public,
  })

  await comment.save(trx)
}

const initialize = args => ({
  async execute({
    manuscript: previousVersion,
    version,
    isMajorVersion = false,
  }): Promise<void> {
    const { Manuscript, Review, Comment } = args.models
    const {
      copySubmittingStaffMemberBetweenManuscripts,
      copyFilesBetweenManuscripts,
      copyAuthorsBetweenManuscripts,
      copyEAsBetweenManuscripts,
      copyTEsBetweenManuscripts,
      copyAEsBetweenManuscripts,
    } = args.useCases

    const existingManuscripts = await Manuscript.findManuscriptsBySubmissionId({
      order: 'desc',
      orderByField: 'version',
      submissionId: previousVersion.submissionId,
    })

    const newVersionManuscript = { ...previousVersion, version }
    const newerVersionExists = existingManuscripts.some(
      m => Manuscript.compareVersion(newVersionManuscript, m) <= 0,
    )
    if (newerVersionExists) {
      logger.error(
        `Submission: ${previousVersion.submissionId} already has a newer version.`,
      )
      throw new Error(`Revision was already requested.`)
    }

    await Manuscript.transaction(async trx => {
      const newVersion = await createManuscript({
        manuscript: previousVersion,
        version,
        Manuscript,
        trx,
      })

      await copyFilesBetweenManuscripts.initialize(args).execute({
        copyFromManuscriptId: previousVersion.id,
        copyToManuscriptId: newVersion.id,
        trx,
      })

      const newAuthors = await copyAuthorsBetweenManuscripts
        .initialize(args)
        .execute({
          copyFromManuscriptId: previousVersion.id,
          copyToManuscriptId: newVersion.id,
          trx,
        })

      await copySubmittingStaffMemberBetweenManuscripts
        .initialize(args)
        .execute({
          copyFromManuscriptId: previousVersion.id,
          copyToManuscriptId: newVersion.id,
          trx,
        })

      await copyEAsBetweenManuscripts.initialize(args).execute({
        copyFromManuscriptId: previousVersion.id,
        copyToManuscriptId: newVersion.id,
        trx,
      })

      await copyTEsBetweenManuscripts.initialize(args).execute({
        copyFromManuscriptId: previousVersion.id,
        copyToManuscriptId: newVersion.id,
        trx,
      })

      await copyAEsBetweenManuscripts.initialize(args).execute({
        copyFromManuscriptId: previousVersion.id,
        copyToManuscriptId: newVersion.id,
        trx,
      })

      if (isMajorVersion) {
        await createResponseReview({
          newVersionId: newVersion.id,
          newAuthors,
          Review,
          Comment,
          trx,
        })
      }
    })
  },
})

export const createVersion = {
  initialize,
}
