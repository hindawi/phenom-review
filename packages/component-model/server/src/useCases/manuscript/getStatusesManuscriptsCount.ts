import { ObjectionParametrizer } from '../../ObjectionParametrizer'
import { GetManuscriptsInput } from './getManuscripts'

export const initialize = ({ Manuscript }) => ({
  async execute({ input }: { input: Partial<GetManuscriptsInput> }) {
    const parameters = new ObjectionParametrizer<Partial<GetManuscriptsInput>>(
      input,
    ).getParams()
    const params = {
      ...parameters,
      count: 'status as count',
      groupBy: 'status',
    }
    const statuses = await Manuscript.findAllV2(params)
    return statuses
  },
})
