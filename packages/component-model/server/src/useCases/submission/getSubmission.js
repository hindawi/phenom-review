const { logger } = require('component-logger')

const initialize = models => ({
  execute: async ({ submissionId, userId }) => {
    const { TeamMember, Manuscript, Team } = models

    const requestTeamMember = await TeamMember.findOneBySubmissionAndUser({
      userId,
      submissionId,
      TeamRole: Team.Role,
    })

    if (!requestTeamMember) {
      throw new AuthorizationError(
        `Operation not permitted for user ${userId}!`,
      )
    }

    const forbiddenStatuses = [
      TeamMember.Statuses.declined,
      TeamMember.Statuses.expired,
      TeamMember.Statuses.removed,
      TeamMember.Statuses.conflicting,
    ]

    if (forbiddenStatuses.includes(requestTeamMember.status)) {
      throw new AuthorizationError(
        `Operation not permitted for user ${userId}!`,
      )
    }

    const eagerLoadRelations = [
      'files',
      'articleType',
      'peerReviewEditorialMapping',
      'journal.[peerReviewModel, journalPreprints, preprints]',
      'specialIssue.[peerReviewModel,section]',
    ]

    if (
      requestTeamMember.status === TeamMember.Statuses.pending &&
      [Team.Role.academicEditor, Team.Role.reviewer].includes(
        requestTeamMember.role,
      )
    ) {
      eagerLoadRelations.splice(eagerLoadRelations.indexOf('files'), 1)
    }

    const excludedStatuses = [Manuscript.Statuses.draft]
    if (
      ![
        Team.Role.admin,
        Team.Role.author,
        Team.Role.editorialAssistant,
        Team.Role.submittingStaffMember,
      ].includes(requestTeamMember.role)
    ) {
      excludedStatuses.push(
        Manuscript.Statuses.withdrawn,
        Manuscript.Statuses.void,
      )
    }

    let manuscripts = []
    if (requestTeamMember.role === Team.Role.admin) {
      manuscripts = await Manuscript.findAllForAdminBySubmission({
        submissionId,
        excludedStatuses,
        eagerLoadRelations,
        adminRole: Team.Role.admin,
      })
    } else {
      manuscripts = await Manuscript.findAllBySubmissionAndUserAndRole({
        submissionId,
        excludedStatuses,
        eagerLoadRelations,
        userId: requestTeamMember.userId,
        userRole: requestTeamMember.role,
      })
    }

    logger
      .addLabel('submissionId', submissionId)
      .addLabel('userId', userId)
      .addLabel('filename', __filename)
      .info(`Manuscripts count: ${manuscripts.length}`)

    return manuscripts.map(m => m.toDTO())
  },
})

const authsomePolicies = ['hasAccessToSubmission', 'admin']

module.exports = {
  initialize,
  authsomePolicies,
}
