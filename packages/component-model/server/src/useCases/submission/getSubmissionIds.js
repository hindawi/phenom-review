// Temporary use-case for redirect

const initialize = models => ({
  execute: async ({ customId }) => {
    const { Manuscript } = models

    return Manuscript.findLastManuscriptByCustomId({
      customId,
    })
  },
})

const authsomePolicies = ['hasAccessToSubmissionByCustomId', 'admin']

module.exports = {
  initialize,
  authsomePolicies,
}
