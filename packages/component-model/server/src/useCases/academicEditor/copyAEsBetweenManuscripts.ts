/* eslint-disable no-restricted-syntax */
type ExecuteProps = {
  copyFromManuscriptId: string
  copyToManuscriptId: string
  trx?: any // whatever shape comes from objection
}

export const initialize = ({ models: { Team, TeamMember } }) => ({
  async execute({
    copyFromManuscriptId,
    copyToManuscriptId,
    trx = null,
  }: ExecuteProps) {
    const existingAcademicEditors = await TeamMember.findAllByManuscriptAndRole(
      {
        manuscriptId: copyFromManuscriptId,
        role: Team.Role.academicEditor,
      },
    )

    if (!existingAcademicEditors.length) return null

    const newAcademicEditorTeam = new Team({
      role: Team.Role.academicEditor,
      manuscriptId: copyToManuscriptId,
    })

    await newAcademicEditorTeam.save(trx)

    const newAEs = []

    for (const existingAcademicEditor of existingAcademicEditors) {
      const newAcademicEditor = new TeamMember({
        ...existingAcademicEditor,
        teamId: newAcademicEditorTeam.id,
      })

      delete newAcademicEditor.id
      // eslint-disable-next-line no-await-in-loop
      const newAE = await newAcademicEditor.save(trx)
      newAEs.push(newAE)
    }

    return newAEs
  },
})

export const copyAEsBetweenManuscripts = {
  initialize,
}
