const initialize = ({ User, Team }) => ({
  execute: async ({ userId }) => {
    const user = await User.find(userId, 'identities')
    user.isAdmin = !!(await user.findTeamMemberByRole(Team.Role.admin))
    user.isRIPE = !!(await user.findTeamMemberByRole(
      Team.Role.researchIntegrityPublishingEditor,
    ))

    return user.toDTO()
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
