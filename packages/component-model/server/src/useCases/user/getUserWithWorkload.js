const { unionBy, orderBy } = require('lodash')
const { logger } = require('component-logger')

const initialize = ({ TeamMember }) => ({
  execute: async ({
    role,
    journalId,
    sectionId,
    specialIssueId,
    manuscriptStatuses,
    teamMemberStatuses,
  }) => {
    try {
      const parentTeamMembers = await TeamMember.findAllByManuscriptParentAndRole(
        { specialIssueId, sectionId, journalId, role },
      )
      const manuscriptMembersWithWorkload = await TeamMember.findAllWithWorkloadByManuscriptParent(
        {
          role,
          journalId,
          sectionId,
          specialIssueId,
          teamMemberStatuses,
          manuscriptStatuses,
        },
      )

      const parentMembersWithDefaultWorkload = parentTeamMembers.map(
        member => ({
          userId: member.userId,
          workload: 0,
        }),
      )

      const eligibleManuscriptMembers = manuscriptMembersWithWorkload.filter(
        mm =>
          parentMembersWithDefaultWorkload.find(pm => pm.userId === mm.userId),
      )

      const users = orderBy(
        unionBy(
          eligibleManuscriptMembers,
          parentMembersWithDefaultWorkload,
          'userId',
        ),
        ['workload'],
        ['asc'],
      )

      return users[0]
    } catch (e) {
      logger.error('Something went wrong.', e)
      throw new Error('Something went wrong.')
    }
  },
})

module.exports = {
  initialize,
}
