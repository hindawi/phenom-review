const initialize = ({ User, Team }) => ({
  execute: async ({ userId }) => {
    const currentUser = await User.find(userId, 'identities')

    const adminRole = await currentUser.isAdmin()
    if (adminRole) currentUser.role = Team.Role.admin
    else {
      const editorialAssistantRole = await currentUser.isEditorialAssistant()
      if (editorialAssistantRole)
        currentUser.role = Team.Role.editorialAssistant
      else currentUser.role = 'user'
    }

    return currentUser.toDTO()
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
