type ExecuteProps = {
  copyFromManuscriptId: string
  copyToManuscriptId: string
  trx?: any // whatever shape comes from objection
}

export const initialize = ({ models: { Team, TeamMember } }) => ({
  async execute({
    copyFromManuscriptId,
    copyToManuscriptId,
    trx = null,
  }: ExecuteProps) {
    const existingSubmittingStaffMember = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: copyFromManuscriptId,
        role: Team.Role.submittingStaffMember,
        status: TeamMember.Statuses.pending,
      },
    )

    if (!existingSubmittingStaffMember) return null

    const newSubmittingStaffMemberTeam = new Team({
      role: Team.Role.submittingStaffMember,
      manuscriptId: copyToManuscriptId,
    })

    await newSubmittingStaffMemberTeam.save(trx)

    const newSubmittingStaffMember = new TeamMember({
      ...existingSubmittingStaffMember,
      teamId: newSubmittingStaffMemberTeam.id,
    })

    delete newSubmittingStaffMember.id
    return newSubmittingStaffMember.save(trx)
  },
})

export const copySubmittingStaffMemberBetweenManuscripts = {
  initialize,
}
