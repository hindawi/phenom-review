const initialize = ({ loaders }) => ({
  execute: async ({ specialIssue }) => {
    if (specialIssue.peerReviewModel) return specialIssue.peerReviewModel

    const peerReviewModel = await loaders.SpecialIssue.peerReviewModelLoader.load(
      specialIssue.id,
    )
    return peerReviewModel?.shift()
  },
})

export default { initialize }
