type ExecuteProps = {
  copyFromManuscriptId: string
  copyToManuscriptId: string
  trx?: any // whatever shape comes from objection
}

export const initialize = ({ models: { Team, TeamMember } }) => ({
  async execute({
    copyFromManuscriptId,
    copyToManuscriptId,
    trx = null,
  }: ExecuteProps) {
    const existingEditorialAssistant = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: copyFromManuscriptId,
        role: Team.Role.editorialAssistant,
        status: TeamMember.Statuses.active,
      },
    )

    if (!existingEditorialAssistant) return null

    const newEditorialAssistantTeam = new Team({
      role: Team.Role.editorialAssistant,
      manuscriptId: copyToManuscriptId,
    })

    await newEditorialAssistantTeam.save(trx)

    const newEditorialAssistantMember = new TeamMember({
      ...existingEditorialAssistant,
      teamId: newEditorialAssistantTeam.id,
    })

    delete newEditorialAssistantMember.id
    const newEA = await newEditorialAssistantMember.save(trx)
    return newEA
  },
})

export const copyEAsBetweenManuscripts = {
  initialize,
}
