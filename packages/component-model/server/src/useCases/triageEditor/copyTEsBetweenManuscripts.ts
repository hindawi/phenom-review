type ExecuteProps = {
  copyFromManuscriptId: string
  copyToManuscriptId: string
  trx?: any // whatever shape comes from objection
}

export const initialize = ({ models: { Team, TeamMember } }) => ({
  async execute({
    copyFromManuscriptId,
    copyToManuscriptId,
    trx = null,
  }: ExecuteProps) {
    const existingTriageEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: copyFromManuscriptId,
        role: Team.Role.triageEditor,
        status: TeamMember.Statuses.active,
      },
    )

    if (!existingTriageEditor) return null

    const newTriageEditorTeam = new Team({
      role: Team.Role.triageEditor,
      manuscriptId: copyToManuscriptId,
    })

    await newTriageEditorTeam.save(trx)

    const newTriageEditor = new TeamMember({
      ...existingTriageEditor,
      teamId: newTriageEditorTeam.id,
    })

    delete newTriageEditor.id
    const newTE = await newTriageEditor.save(trx)
    return newTE
  },
})

export const copyTEsBetweenManuscripts = {
  initialize,
}
