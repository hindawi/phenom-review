const initialize = ({ ArticleType, Team }) => ({
  execute: async ({
    journal,
    userId,
    loaders,
    getJournalArticleTypesForUserUseCase,
  }) => {
    const staffRoles = await loaders.TeamMember.staffRolesLoader.load(userId)

    const articleTypes =
      staffRoles && staffRoles.length
        ? await loaders.Journal.journalArticleTypeLoader.load(journal.id)
        : await getJournalArticleTypesForUserUseCase
            .initialize({
              Team,
              ArticleType,
            })
            .execute({
              loaders,
              journalId: journal.id,
              userId,
            })

    return articleTypes.sort((a, b) => (a.name > b.name ? -1 : 1))
  },
})

module.exports = {
  initialize,
}
