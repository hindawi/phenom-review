const getJournalUseCase = require('./getJournal')
const getJournalSectionsUseCase = require('./getJournalSections')
const getJournalArticleTypesUseCase = require('./getJournalArticleTypes')
const getJournalArticleTypesForUserUseCase = require('./getJournalArticleTypesForUser')
const getJournalSpecialIssuesUseCase = require('./getJournalSpecialIssues')
const getJournalPeerReviewModelUseCase = require('./getJournalPeerReviewModel')
const getJournalPreprintsUseCase = require('./getJournalPreprints')
const getJournalPreprintDescriptionUseCase = require('./getJournalPreprintsDescription')
const getJournalsUseCase = require('./getJournals')
const getActiveJournalsBySelectedArticleTypeUseCase = require('./getActiveJournalsBySelectedArticleType')

module.exports = {
  getJournalUseCase,
  getJournalSectionsUseCase,
  getJournalArticleTypesUseCase,
  getJournalArticleTypesForUserUseCase,
  getJournalSpecialIssuesUseCase,
  getJournalPeerReviewModelUseCase,
  getJournalPreprintsUseCase,
  getJournalPreprintDescriptionUseCase,
  getJournalsUseCase,
  getActiveJournalsBySelectedArticleTypeUseCase,
}
