const initialize = ({ JournalArticleType, Journal }) => ({
  execute: async articleTypeId => {
    const journalsWithSelectedArticleType = await JournalArticleType.findAllByArticleType(
      articleTypeId,
    )
    const journalIds = journalsWithSelectedArticleType.map(
      journal => journal.journalId,
    )
    return Journal.findAllActiveByIds(journalIds)
  },
})

const authsomePolicies = ['isAuthenticated']

export { initialize, authsomePolicies }
