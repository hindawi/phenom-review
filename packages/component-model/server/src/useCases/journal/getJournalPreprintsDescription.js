const initialize = () => ({
  execute: ({ journal }) => {
    if (!journal.journalPreprints || !journal.journalPreprints.length)
      return null
    const preprint = journal.journalPreprints[0]
    return preprint.preprintDescription || null
  },
})

module.exports = {
  initialize,
}
