/* eslint-disable no-restricted-syntax */
type ExecuteProps = {
  copyFromManuscriptId: string
  copyToManuscriptId: string
  trx?: any // whatever shape comes from objection
}

// test comm

export const initialize = ({ models: { Team, TeamMember } }) => ({
  async execute({
    copyFromManuscriptId,
    copyToManuscriptId,
    trx = null,
  }: ExecuteProps) {
    const newAuthorTeam = new Team({
      role: Team.Role.author,
      manuscriptId: copyToManuscriptId,
    })

    await newAuthorTeam.save(trx)

    const existingAuthors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: copyFromManuscriptId,
      role: Team.Role.author,
    })

    const newAuthors = []

    for (const existingAuthor of existingAuthors) {
      const newAuthor = new TeamMember({
        ...existingAuthor,
        teamId: newAuthorTeam.id,
      })

      delete newAuthor.id
      // eslint-disable-next-line no-await-in-loop
      const savedNewAuthor = await newAuthor.save(trx)
      newAuthors.push(savedNewAuthor)
    }

    return newAuthors
  },
})

export const copyAuthorsBetweenManuscripts = {
  initialize,
}
