const { Team } = require('../model/team')
const { TeamMember } = require('../model/teamMember')

const {
  groupResultsByKey,
  sortResultsByKeyOrder,
} = require('component-dataloader-tools')

module.exports = {
  teamMemberTeamLoader: async teamMemberIds => {
    const teamMembers = await TeamMember.query()
      .select('team_member.*')
      .whereIn('id', teamMemberIds)
      .withGraphFetched('team')
    return Promise.all(
      sortResultsByKeyOrder({
        keys: teamMemberIds,
        results: teamMembers,
        getKey: member => member.id,
      }),
    )
  },

  staffRolesLoader: async userIds => {
    const members = await TeamMember.query()
      .distinct('userId', 'role')
      .whereIn('userId', userIds)
      .join('team', 'team.id', 'teamId')
      .whereIn('team.role', Team.StaffRoles)
      .andWhere('status', TeamMember.Statuses.pending)

    return Promise.all(
      groupResultsByKey({
        keys: userIds,
        results: members,
        getKey: member => member.userId,
      }),
    )
  },

  journalRolesLoader: async userJournalPairs => {
    const members = await TeamMember.query()
      .select('userId', 'journalId', 'role')
      .join('team', 'team.id', 'teamId')
      .whereIn(
        ['userId', 'team.journalId'],
        userJournalPairs.map(({ userId, journalId }) => [userId, journalId]),
      )

    const roles = sortResultsByKeyOrder({
      keys: userJournalPairs.map(
        ({ userId, journalId }) => `${userId}-${journalId}`,
      ),
      results: members,
      getKey: member => `${member.userId}-${member.journalId}`,
    }).map(member => (member ? member.role : member))

    return Promise.all(roles)
  },
}
