import { TeamMember } from '../model/teamMember'
import { Team } from '../model/team'
import { SpecialIssue } from '../model/specialIssue'
import { Journal } from '../model/journal'
import { ArticleType } from '../model/articleType'
import { PeerReviewModel } from '../model/peerReviewModel'

const { orderBy } = require('lodash')

const { groupResultsByKey } = require('component-dataloader-tools')

const groupResults = ({ manuscriptIds, results }) =>
  groupResultsByKey({
    keys: manuscriptIds,
    results,
    getKey: property => property.manuscriptId,
  })

const authorsLoader = async manuscriptIds => {
  const dbTeamMembers = await TeamMember.findAllByManuscriptsAndRole({
    manuscriptIds,
    role: Team.Role.author,
  })
  const teamMembers = orderBy(
    dbTeamMembers,
    'position',
    'asc',
  ).map(teamMember => teamMember?.toDTO())
  const membersByManuscript = groupResults({
    manuscriptIds,
    results: teamMembers,
  })
  return Promise.all(membersByManuscript)
}

const reviewersLoader = async manuscriptIds => {
  const dbTeamMembers = await TeamMember.findAllByManuscriptsAndRole({
    manuscriptIds,
    role: Team.Role.reviewer,
  })
  const teamMembers = dbTeamMembers.map(teamMember => teamMember?.toDTO())
  const membersByManuscript = groupResults({
    manuscriptIds,
    results: teamMembers,
  })
  return Promise.all(membersByManuscript)
}

const editorsLoader = async manuscriptIds => {
  // TODO: tech-debt | we should make separate queries for each role
  // at the moment there is no class for RIPE (researchIntegrityPublishEditor)
  // make one and then break this big query in separate calls
  const dbTeamMembers = await TeamMember.findAllByManuscriptsAndRoles({
    manuscriptIds,
    roles: [
      Team.Role.triageEditor,
      Team.Role.academicEditor,
      Team.Role.editorialAssistant,
      Team.Role.researchIntegrityPublishingEditor,
    ],
  })
  const teamMembers = dbTeamMembers.map(teamMember => teamMember?.toDTO())
  const membersByManuscript = groupResults({
    manuscriptIds,
    results: teamMembers,
  })
  return Promise.all(membersByManuscript)
}

const academicEditorsLoader = async manuscriptIds => {
  const dbTeamMembers = await TeamMember.findAllByManuscriptsAndRoleAndStatus({
    manuscriptIds,
    role: Team.Role.academicEditor,
    status: TeamMember.Statuses.accepted,
  })
  const teamMembers = dbTeamMembers.map(teamMember => teamMember?.toDTO())
  const membersByManuscript = groupResults({
    manuscriptIds,
    results: teamMembers,
  })
  return Promise.all(membersByManuscript)
}

const allAcademicEditorsLoader = async manuscriptIds => {
  const dbTeamMembers = await TeamMember.findAllByManuscriptsAndRole({
    manuscriptIds,
    role: Team.Role.academicEditor,
  })
  const teamMembers = dbTeamMembers.map(teamMember => teamMember?.toDTO())
  const membersByManuscript = groupResults({
    manuscriptIds,
    results: teamMembers,
  })
  return Promise.all(membersByManuscript)
}

const pendingAcademicEditorsLoader = async manuscriptIds => {
  const dbTeamMembers = await TeamMember.findAllByManuscriptsAndRoleAndStatus({
    manuscriptIds,
    role: Team.Role.academicEditor,
    status: TeamMember.Statuses.pending,
  })
  const teamMembers = dbTeamMembers.map(teamMember => teamMember?.toDTO())
  const membersByManuscript = groupResults({
    manuscriptIds,
    results: teamMembers,
  })
  return Promise.all(membersByManuscript)
}

const triageEditorsLoader = async manuscriptIds => {
  const dbTeamMembers = await TeamMember.findAllByManuscriptsAndRoleAndStatus({
    manuscriptIds,
    role: Team.Role.triageEditor,
    status: TeamMember.Statuses.active,
  })
  const teamMembers = dbTeamMembers.map(teamMember => teamMember?.toDTO())
  const membersByManuscript = groupResults({
    manuscriptIds,
    results: teamMembers,
  })
  return Promise.all(membersByManuscript)
}

const researchIntegrityPublishingEditorsLoader = async manuscriptIds => {
  const dbTeamMembers = await TeamMember.findAllByManuscriptsAndRoleAndStatus({
    manuscriptIds,
    role: Team.Role.researchIntegrityPublishingEditor,
    status: TeamMember.Statuses.accepted,
  })
  const teamMembers = dbTeamMembers.map(teamMember => teamMember?.toDTO())
  const membersByManuscript = groupResults({
    manuscriptIds,
    results: teamMembers,
  })
  return Promise.all(membersByManuscript)
}

const specialIssueLoader = async manuscriptIds => {
  const dbSpecialIssues = await SpecialIssue.findAllByManuscripts(manuscriptIds)
  const specialIssuesDTOs = dbSpecialIssues.map(si => si?.toDTO())
  const SIsByManuscript = groupResults({
    manuscriptIds,
    results: specialIssuesDTOs,
  })
  return Promise.all(SIsByManuscript)
}

const journalLoader = async manuscriptIds => {
  const journals = await Journal.findAllByManuscriptIds(manuscriptIds) // we cant use findAll with objection find because we also need the manuscriptId in the result
  const journalsByManuscript = groupResults({
    manuscriptIds,
    results: journals,
  })
  return Promise.all(journalsByManuscript)
}

const articleTypeLoader = async manuscriptIds => {
  const articleTypes = await ArticleType.findAllByManuscriptIds(manuscriptIds) // we cant use findAll with objection find because we also need the manuscriptId in the result

  const ArticleTypesByManuscript = groupResults({
    manuscriptIds,
    results: articleTypes,
  })
  return Promise.all(ArticleTypesByManuscript)
}

const journalPRMLoader = async manuscriptIds => {
  const PRMs = await PeerReviewModel.findAllJournalOnesByManuscriptIds(
    manuscriptIds,
  )
  const PRMSsByManuscript = groupResults({
    manuscriptIds,
    results: PRMs,
  })

  return Promise.all(PRMSsByManuscript)
}

const specialIssuePRMLoader = async manuscriptIds => {
  const PRMs = await PeerReviewModel.findAllSpecialIssueOnesByManuscriptIds(
    manuscriptIds,
  )

  const PRMSsByManuscript = groupResults({
    manuscriptIds,
    results: PRMs,
  })
  return Promise.all(PRMSsByManuscript)
}

module.exports = {
  authorsLoader,
  reviewersLoader,
  editorsLoader,
  triageEditorsLoader,
  academicEditorsLoader,
  researchIntegrityPublishingEditorsLoader,
  pendingAcademicEditorsLoader,
  specialIssueLoader,
  journalLoader,
  articleTypeLoader,
  journalPRMLoader,
  specialIssuePRMLoader,
  allAcademicEditorsLoader,
}
