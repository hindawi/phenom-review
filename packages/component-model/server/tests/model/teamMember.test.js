const { TeamMember } = require('../../dist/model/teamMember')
const { EditorSuggestion } = require('../../dist/model/editorSuggestion')

describe('Team Member model', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  describe('orderAcademicEditorsByScoreAndWorkloadAndEmail', () => {
    it('should order academic editors by their score', async () => {
      const editorSuggestions = [
        { teamMemberId: 'team-member-id-1', score: 0.43 },
        { teamMemberId: 'team-member-id-2', score: 0.88 },
        { teamMemberId: 'team-member-id-3', score: 0.25 },
      ]

      const teamMemberWithWorkload = [
        {
          userId: 'user-id-2',
          workload: 5,
        },
        {
          userId: 'user-id-3',
          workload: 2,
        },
      ]

      jest
        .spyOn(EditorSuggestion, 'findBy')
        .mockResolvedValueOnce(editorSuggestions)

      jest
        .spyOn(TeamMember, 'findTeamMemberWorkloadByUsersAndManuscriptParent')
        .mockResolvedValueOnce(teamMemberWithWorkload)

      const orderedAcademicEditors = await TeamMember.orderAcademicEditorsByScoreAndWorkloadAndEmail(
        {
          academicEditors: [
            { id: 'team-member-id-1', userId: 'user-id-1' },
            { id: 'team-member-id-3', userId: 'user-id-2' },
            { id: 'team-member-id-2', userId: 'user-id-3' },
          ],
          manuscript: { id: 'manuscript-id' },
        },
      )

      expect(orderedAcademicEditors).toEqual([
        {
          id: 'team-member-id-2',
          userId: 'user-id-3',
          score: 0.88,
          workload: 2,
        },
        {
          id: 'team-member-id-1',
          userId: 'user-id-1',
          score: 0.43,
          workload: 0,
        },
        {
          id: 'team-member-id-3',
          userId: 'user-id-2',
          score: 0.25,
          workload: 5,
        },
      ])
    })
  })
})
