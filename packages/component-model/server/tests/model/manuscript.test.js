process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')
const { models } = require('fixture-service')

const { Team, TeamMember } = models
const chance = new Chance()

const Manuscript = require('../../dist/model/manuscript')

jest.mock('component-model', () => ({
  HindawiBaseModel: jest.fn(),
}))

// this test is checkig obsolete functionality
describe('Manuscript model', () => {
  describe('toDTO', () => {
    it.skip('returns correct academic editor on manuscript when multiple exist', async () => {
      const currentAcademicEditorId = chance.guid()
      const manuscript = new Manuscript({
        submissionId: chance.guid(),
        id: chance.guid(),
        status: Manuscript.Statuses.reviewersInvited,
        version: '1',
      })

      manuscript.teams = [
        {
          id: chance.guid(),
          role: Team.Role.academicEditor,
          toDTO: function toDTO() {
            return this
          },
          members: [
            {
              id: chance.guid(),
              status: TeamMember.Statuses.expired,
              toDTO: function toDTO() {
                return this
              },
              position: 0,
            },
            {
              id: currentAcademicEditorId,
              status: TeamMember.Statuses.accepted,
              toDTO: function toDTO() {
                return this
              },
              position: 1,
            },
          ],
        },
        {
          id: chance.guid(),
          role: Team.Role.author,
          toDTO: function toDTO() {
            return this
          },
          members: [
            {
              id: chance.guid(),
              status: TeamMember.Statuses.pending,
              toDTO: function toDTO() {
                return this
              },
              position: 0,
            },
          ],
        },
      ]

      const { academicEditor } = manuscript.toDTO()

      expect(academicEditor).toBeDefined()
      expect(currentAcademicEditorId).toEqual(academicEditor.id)
    })
  })
})
