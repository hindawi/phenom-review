const { Promise } = require('bluebird')
const {
  getTeamRoles,
  generateTeamMember,
  generateManuscript,
  getTeamMemberStatuses,
  getManuscriptStatuses,
  getManuscriptInProgressStatuses,
} = require('component-generators')

const {
  getUpdatedEditorialAssistantsUseCase,
  redistributeEditorialAssistantsUseCase,
  getSubmissionsWithUpdatedEditorialAssistantsUseCase,
} = require('../../dist/useCases/manuscript')

const models = {
  Manuscript: {
    findAll: jest.fn(),
    Statuses: getManuscriptStatuses(),
    countSubmissionsByJournalAndStatuses: jest.fn(),
    InProgressStatuses: getManuscriptInProgressStatuses(),
    findSubmissionsByJournalAndUserAndRoleAndTeamMemberStatuses: jest.fn(),
  },
  Team: {
    Role: getTeamRoles(),
  },
  TeamMember: jest.fn(),
}
Object.assign(models.TeamMember, {
  knex: jest.fn(),
  query: jest.fn(() => ({
    upsertGraph: jest.fn(),
  })),
  Statuses: getTeamMemberStatuses(),
  findAllByJournalWithWorkload: jest.fn(),
  findOneByUserAndRoleAndStatusOnManuscript: jest.fn(),
})
const logger = {
  warn: jest.fn(),
  info: jest.fn(),
  error: jest.fn(),
}
const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

const { Team, TeamMember, Manuscript } = models
describe('Redistribute editorial assistants use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('returns a warning when no editorial assistants are found', async () => {
    jest
      .spyOn(TeamMember, 'findAllByJournalWithWorkload')
      .mockResolvedValueOnce([])

    await redistributeEditorialAssistantsUseCase
      .initialize({ models, transaction: jest.fn(), logger })
      .execute('invalid-journal-id')

    expect(logger.warn).toHaveBeenCalledWith(
      `No editorial assistants found on journal invalid-journal-id`,
    )
  })
  it('returns an info message when the manuscripts have been redistributed properly', async () => {
    const ea1 = generateTeamMember({
      team: { role: Team.Role.editorialAssistant },
    })
    ea1.workload = 10

    const ea2 = generateTeamMember({
      team: { role: Team.Role.editorialAssistant },
    })
    ea2.workload = 2

    const ea3 = generateTeamMember({
      team: { role: Team.Role.editorialAssistant },
    })
    ea3.workload = 1

    const manuscript = generateManuscript({
      status: Manuscript.Statuses.submitted,
    })

    jest
      .spyOn(TeamMember, 'findAllByJournalWithWorkload')
      .mockResolvedValueOnce([ea1, ea2, ea3])

    jest
      .spyOn(Manuscript, 'countSubmissionsByJournalAndStatuses')
      .mockResolvedValueOnce({
        count: ea1.workload + ea2.workload + ea3.workload,
      })
    jest
      .spyOn(
        Manuscript,
        'findSubmissionsByJournalAndUserAndRoleAndTeamMemberStatuses',
      )
      .mockResolvedValue([{ submissionId: manuscript.submissionId }])
    jest.spyOn(Manuscript, 'findAll').mockResolvedValue([manuscript])
    jest
      .spyOn(TeamMember, 'findOneByUserAndRoleAndStatusOnManuscript')
      .mockResolvedValue(ea1)

    await redistributeEditorialAssistantsUseCase
      .initialize({
        models,
        logger,
        Promise,
        eventsService,
        transaction: jest.fn(),
        useCases: {
          getUpdatedEditorialAssistantsUseCase,
          getSubmissionsWithUpdatedEditorialAssistantsUseCase,
        },
      })
      .execute('journal-id')

    expect(logger.error).not.toHaveBeenCalled()
    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledTimes(2)
  })
})
