const {
  getTeamRoles,
  generateManuscript,
  getTeamMemberStatuses,
} = require('component-generators')
const { getManuscriptFilesUseCase } = require('../../src/useCases/')

let models = {}
let TeamMember = {}
let File = {}

describe('getManuscriptFiles', () => {
  beforeEach(() => {
    jest.clearAllMocks()

    models = {
      Team: {
        Role: getTeamRoles(),
      },
      TeamMember: {
        Statuses: getTeamMemberStatuses(),
        findOneByUserAndRole: jest.fn(),
        findOneByManuscriptAndUser: jest.fn(),
      },
      File: {
        findBy: jest.fn(),
      },
    }
    ;({ TeamMember, File } = models)
  })
  it('returns all files for pending authors', async () => {
    const manuscript = generateManuscript()
    const userId = 'some-use-id'

    jest.spyOn(TeamMember, 'findOneByUserAndRole').mockResolvedValue()
    jest.spyOn(TeamMember, 'findOneByManuscriptAndUser').mockResolvedValue({
      team: { role: 'author' },
      status: 'pending',
    })
    jest.spyOn(File, 'findBy').mockResolvedValue([
      {
        toDTO: jest.fn(() => ({
          id: 'some-file-id',
        })),
      },
    ])

    const res = await getManuscriptFilesUseCase
      .initialize(models)
      .execute({ manuscript, userId })

    expect(res).toEqual([{ id: 'some-file-id' }])
  })
  it('returns an empty array for pending reviewers', async () => {
    const manuscript = generateManuscript()
    const userId = 'some-use-id'

    jest.spyOn(TeamMember, 'findOneByUserAndRole').mockResolvedValue()
    jest.spyOn(TeamMember, 'findOneByManuscriptAndUser').mockResolvedValue({
      team: { role: 'reviewer' },
      status: 'pending',
    })

    const res = await getManuscriptFilesUseCase
      .initialize(models)
      .execute({ manuscript, userId })

    expect(res).toEqual([])
  })
  it('returns an empty array for pending academic editors', async () => {
    const manuscript = generateManuscript()
    const userId = 'some-use-id'

    jest.spyOn(TeamMember, 'findOneByUserAndRole').mockResolvedValue()
    jest.spyOn(TeamMember, 'findOneByManuscriptAndUser').mockResolvedValue({
      team: { role: 'academicEditor' },
      status: 'pending',
    })

    const res = await getManuscriptFilesUseCase
      .initialize(models)
      .execute({ manuscript, userId })

    expect(res).toEqual([])
  })
  it('returns all files for pending triage editors', async () => {
    const manuscript = generateManuscript()
    const userId = 'some-use-id'

    jest.spyOn(TeamMember, 'findOneByUserAndRole').mockResolvedValue()
    jest.spyOn(TeamMember, 'findOneByManuscriptAndUser').mockResolvedValue({
      team: { role: 'triageEditor' },
      status: 'pending',
    })
    jest.spyOn(File, 'findBy').mockResolvedValue([
      {
        toDTO: jest.fn(() => ({
          id: 'some-file-id',
        })),
      },
    ])

    const res = await getManuscriptFilesUseCase
      .initialize(models)
      .execute({ manuscript, userId })

    expect(res).toEqual([{ id: 'some-file-id' }])
  })
  it('returns all files for pending admins', async () => {
    const manuscript = generateManuscript()
    const userId = 'some-use-id'

    jest.spyOn(TeamMember, 'findOneByUserAndRole').mockResolvedValue()
    jest.spyOn(TeamMember, 'findOneByManuscriptAndUser').mockResolvedValue({
      team: { role: 'admin' },
      status: 'pending',
    })
    jest.spyOn(File, 'findBy').mockResolvedValue([
      {
        toDTO: jest.fn(() => ({
          id: 'some-file-id',
        })),
      },
    ])

    const res = await getManuscriptFilesUseCase
      .initialize(models)
      .execute({ manuscript, userId })

    expect(res).toEqual([{ id: 'some-file-id' }])
  })
})
