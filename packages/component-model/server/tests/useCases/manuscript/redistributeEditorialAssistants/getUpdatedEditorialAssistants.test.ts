import * as bluebird from 'bluebird'

import {
  getTeamMemberStatuses,
  getTeamRoles,
  generateTeamMember,
} from 'component-generators'

import * as getUpdatedEditorialAssistants from '../../../../src/useCases/manuscript/redistributeEditorialAssistants/getUpdatedEditorialAssistants'

const getUpdatedEditorialAssistantsServiceWithMocks = () => {
  const TeamMember = jest.fn() as any
  Object.assign(TeamMember, {
    Statuses: getTeamMemberStatuses(),
    findOneByUserAndRoleAndStatusOnManuscript: jest.fn(),
  })
  const Team = { Role: getTeamRoles() }
  const logger = {
    info: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
  }

  return {
    getUpdatedEditorialAssistantsService: getUpdatedEditorialAssistants.initialize(
      { models: { TeamMember, Team }, Promise: bluebird.Promise, logger },
    ),
    mocks: { TeamMember, Team, logger },
  }
}

describe('Get Updated Editorial Assistants', () => {
  const newEditorialAssistant = generateTeamMember({
    userId: 'new-editorial-assistant-id',
    alias: 'new-editorial-assistant-alias',
  })
  const journalEditorialAssistant = generateTeamMember({
    userId: 'journal-editorial-assistant-id',
    alias: 'journal-editorial-assistant-alias',
  })
  const currentEditorialAssistant = generateTeamMember({
    teamId: 'current-editorial-assistant-team-id',
  })
  const submission = [{ id: 'manuscript-1' }, { id: 'manuscript-2' }]

  it('works as expected through a happy flow', async () => {
    const {
      getUpdatedEditorialAssistantsService,
      mocks,
    } = getUpdatedEditorialAssistantsServiceWithMocks()

    jest
      .spyOn(mocks.TeamMember, 'findOneByUserAndRoleAndStatusOnManuscript')
      .mockResolvedValue(currentEditorialAssistant)

    const {
      currentEditorialAssistants,
      newEditorialAssistants,
    } = await getUpdatedEditorialAssistantsService.execute({
      submission,
      newEditorialAssistant,
      journalEditorialAssistant,
    })

    expect(
      mocks.TeamMember.findOneByUserAndRoleAndStatusOnManuscript.mock.calls
        .length,
    ).toBe(2)
    expect(
      mocks.TeamMember.findOneByUserAndRoleAndStatusOnManuscript.mock.calls,
    ).toEqual([
      [
        {
          manuscriptId: 'manuscript-1',
          role: 'editorialAssistant',
          status: 'active',
          userId: 'journal-editorial-assistant-id',
        },
      ],
      [
        {
          manuscriptId: 'manuscript-2',
          role: 'editorialAssistant',
          status: 'active',
          userId: 'journal-editorial-assistant-id',
        },
      ],
    ])

    expect(currentEditorialAssistant.updateProperties.mock.calls.length).toBe(2)
    expect(currentEditorialAssistant.updateProperties.mock.calls).toEqual([
      [{ status: 'removed' }],
      [{ status: 'removed' }],
    ])

    expect(mocks.TeamMember.mock.calls.length).toBe(2)
    expect(mocks.TeamMember.mock.calls).toEqual([
      [
        {
          alias: 'new-editorial-assistant-alias',
          status: 'active',
          teamId: 'current-editorial-assistant-team-id',
          userId: 'new-editorial-assistant-id',
        },
      ],
      [
        {
          alias: 'new-editorial-assistant-alias',
          status: 'active',
          teamId: 'current-editorial-assistant-team-id',
          userId: 'new-editorial-assistant-id',
        },
      ],
    ])

    expect(currentEditorialAssistants).toEqual([
      currentEditorialAssistant,
      currentEditorialAssistant,
    ])
    expect(newEditorialAssistants.length).toBe(2)
  })

  it('logs and throws an error when current editorial assistant is not found', async done => {
    const {
      getUpdatedEditorialAssistantsService,
      mocks,
    } = getUpdatedEditorialAssistantsServiceWithMocks()

    jest
      .spyOn(mocks.TeamMember, 'findOneByUserAndRoleAndStatusOnManuscript')
      .mockResolvedValue(undefined)

    try {
      await getUpdatedEditorialAssistantsService.execute({
        submission,
        newEditorialAssistant,
        journalEditorialAssistant,
      })
    } catch (error) {
      expect(mocks.logger.error.mock.calls[0][0]).toBe(
        'No editorial assistant found for manuscript: manuscript-1, userId: journal-editorial-assistant-id',
      )
      expect(error.message).toBe('No editorial assistant found')
      done()
    }
  })
})
