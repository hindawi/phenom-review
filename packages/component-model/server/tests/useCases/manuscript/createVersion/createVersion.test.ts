import { generateManuscript } from 'component-generators'
import { createVersion } from '../../../../src/useCases/manuscript/createVersion/createVersion'

const models = require('@pubsweet/models')

const mockResults = {
  findManuscriptsBySubmissionId: [] as any,
}

const mocks = {
  models: {
    Manuscript: {
      findManuscriptsBySubmissionId: () =>
        mockResults.findManuscriptsBySubmissionId,
      transaction: () => jest.fn,
      compareVersion: models.Manuscript.compareVersion,
    },
  },
  useCases: {
    copySubmittingStaffMemberBetweenManuscripts: jest.fn(),
    copyFilesBetweenManuscripts: jest.fn(),
    copyAuthorsBetweenManuscripts: jest.fn(),
    copyEAsBetweenManuscripts: jest.fn(),
    copyTEsBetweenManuscripts: jest.fn(),
    copyAEsBetweenManuscripts: jest.fn(),
  },
}
const manuscriptV1 = generateManuscript({ version: '1' })
const manuscriptV2 = generateManuscript({ version: '2' })
const manuscriptV3 = generateManuscript({ version: '3' })

describe('Create a new manuscript version', () => {
  afterEach(() => {
    jest.resetAllMocks()
    mockResults.findManuscriptsBySubmissionId = []
  })
  it('Creates next version', async () => {
    mockResults.findManuscriptsBySubmissionId = [manuscriptV1]
    const createVersionUsecase = createVersion.initialize(mocks)
    const result = await createVersionUsecase.execute({
      manuscript: {},
      version: '2',
    })
    expect(result).toBe(undefined)
  })

  it('Fails when version exists', async () => {
    mockResults.findManuscriptsBySubmissionId = [manuscriptV1, manuscriptV2]
    const createVersionUsecase = createVersion.initialize(mocks)
    await expect(
      createVersionUsecase.execute({
        manuscript: {},
        version: '2',
      }),
    ).rejects.toThrowError()
  })

  it('Fails when next version exists', async () => {
    mockResults.findManuscriptsBySubmissionId = [
      manuscriptV1,
      manuscriptV2,
      manuscriptV3,
    ]
    const createVersionUsecase = createVersion.initialize(mocks)
    await expect(() =>
      createVersionUsecase.execute({
        manuscript: {},
        version: '2.1',
      }),
    ).rejects.toThrowError()
  })
})
