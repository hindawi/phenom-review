const {
  getManuscriptTriageEditorUseCase,
} = require('../../dist/useCases/manuscript')

const mockManuscript = {
  id: 'ca231f9b-b182-428d-b4d3-ff6f1120bd05',
  journalId: '2c86989c-0c53-4ae1-b8f0-0d615393f185',
  created: '2020-04-07T12:19:56.596Z',
  updated: '2020-04-21T09:04:32.907Z',
  status: 'submitted',
  title: 'Second regular manuscript',
  abstract: 'hope this one works',
  customId: '8843797',
  version: '1',
  hasPassedEqs: true,
  hasPassedEqa: null,
  technicalCheckToken: null,
  submissionId: '354b6efa-fcd2-4a3a-bdee-aaae79c822bf',
  agreeTc: true,
  conflictOfInterest: '',
  dataAvailability: 'lots',
  fundingStatement: 'none',
  articleTypeId: '422068b3-83c3-4a43-8c40-e4553d8f7574',
  sectionId: null,
  specialIssueId: null,
  isPostAcceptance: false,
  submittedDate: '2020-04-07T12:20:32.909Z',
  acceptedDate: null,
  qualityChecksSubmittedDate: null,
  role: undefined,
  statusCategory: 'inProgress',
  specialIssue: undefined,
  articleType: {
    id: '422068b3-83c3-4a43-8c40-e4553d8f7574',
    created: '2020-04-01T13:52:57.699Z',
    updated: '2020-04-03T13:27:18.530Z',
    name: 'Research Article',
    hasPeerReview: true,
  },
  journal: {
    id: '2c86989c-0c53-4ae1-b8f0-0d615393f185',
    created: '2020-04-01T14:11:23.982Z',
    updated: '2020-04-01T14:11:23.982Z',
    name: 'Primul Jurnal',
    publisherName: null,
    code: '0001',
    email: 'admin@hindawi.com',
    issn: null,
    apc: 100,
    isActive: true,
    activationDate: '2020-04-01T00:00:00.000Z',
    peerReviewModelId: 'b3f848fd-6d53-45bd-88a3-4c169ce894fa',
    peerReviewModel: {
      id: 'b3f848fd-6d53-45bd-88a3-4c169ce894fa',
      updated: '2020-04-01T13:52:57.693Z',
      created: '2020-04-01T13:52:57.693Z',
      name: 'Chief Minus',
      hasFigureheadEditor: false,
      figureheadEditorLabel: null,
      hasTriageEditor: true,
      triageEditorLabel: 'Chief Editor',
      academicEditorLabel: 'Academic Editor',
      hasSections: false,
      academicEditorAutomaticInvitation: false,
    },
  },
  files: [],
  meta: {
    title: 'Second regular manuscript',
    agreeTc: true,
    abstract: 'hope this one works',
    articleTypeId: '422068b3-83c3-4a43-8c40-e4553d8f7574',
    dataAvailability: 'lots',
    fundingStatement: 'none',
    conflictOfInterest: '',
  },
  section: undefined,
  triageEditor: undefined,
  reviews: [],
  teams: [],
  academicEditor: undefined,
  authors: [],
  reviewers: [],
  pendingAcademicEditor: undefined,
  researchIntegrityPublishingEditor: undefined,
}

const mockTriageEditorDTO = {
  id: 'e7725a33-1584-4562-bfbe-02fa5c9c709f',
  alias: {
    aff: 'TE aff',
    country: 'RO',
    givenNames: 'TE',
    name: { surname: 'TE', givenNames: 'TE', title: 'mr' },
    email: 'email@randomprovider.com',
    surname: 'TE',
    title: 'mr',
  },
  role: 'triageEditor',
  status: 'active',
  teamId: 'c2017fc3-3f24-4e0d-96c9-967288f87936',
  userId: '398f0727-06c8-47dc-a7ec-dc525bee607a',
}

const MockTeam = {
  Role: {
    admin: 'admin',
    editorialAssistant: 'editorialAssistant',
    author: 'author',
  },
}

const MockTeamMember = {
  findOneByManuscriptAndRoleAndStatus: (_, __) =>
    Promise.resolve({ toDTO: () => mockTriageEditorDTO }),
  Statuses: {
    active: 'active',
  },
}

const mockModels = {
  Team: MockTeam,
  TeamMember: MockTeamMember,
}

const mockLoaders = {
  Manuscript: {
    triageEditorsLoader: {
      load: (_, __) => Promise.resolve([mockTriageEditorDTO]),
    },
  },
}

describe('Get Manuscript Triage Editor email handling', () => {
  it('SHOULD NOT remove the email address from alias if current user is admin', async () => {
    mockManuscript.role = MockTeam.Role.admin
    const triageEditor = await getManuscriptTriageEditorUseCase
      .initialize({ models: mockModels, loaders: mockLoaders })
      .execute({ manuscript: mockManuscript })

    expect(triageEditor.alias.email).toEqual(mockTriageEditorDTO.alias.email)
  })

  it('SHOULD NOT remove the email address from alias if current user is EA', async () => {
    mockManuscript.role = MockTeam.Role.editorialAssistant
    const triageEditor = await getManuscriptTriageEditorUseCase
      .initialize({ models: mockModels, loaders: mockLoaders })
      .execute({ manuscript: mockManuscript })

    expect(triageEditor.alias.email).toEqual(mockTriageEditorDTO.alias.email)
  })

  it('SHOULD remove the email address from alias if current user is NOT admin or Ea', async () => {
    mockManuscript.role = MockTeam.Role.author
    const triageEditor = await getManuscriptTriageEditorUseCase
      .initialize({ models: mockModels, loaders: mockLoaders })
      .execute({ manuscript: mockManuscript })

    expect(triageEditor.alias.email).toBeUndefined()
  })
})
