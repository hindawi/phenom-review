import { expect } from '@jest/globals'

const {
  copySubmittingStaffMemberBetweenManuscripts,
} = require('../../src/useCases/submittingStaffMember/copySubmittingStaffMemberBetweenManuscripts')
const { generateTeamMember } = require('component-generators')

const newTeamId = 'team2'
const copyFromManuscriptId = 'manuscript1'
const copyToManuscriptId = 'manuscript2'

const mockedTeamMember = generateTeamMember({
  id: 'tm1',
  teamId: 'team1',
})

class mockTeamModel {
  constructor(args) {
    return {
      id: newTeamId,
      save() {
        return this
      },
      ...args,
    }
  }
  static Role = {
    submittingStaffMember: 'submittingStaffMember',
  }
}

class mockTeamMemberModel {
  constructor(args) {
    return {
      ...args,
      save() {
        return this
      },
    }
  }
  static Statuses = {
    pending: 'pending',
  }
  static findOneByManuscriptAndRoleAndStatus() {
    return mockedTeamMember
  }
}

const mockModels = {
  Team: mockTeamModel,
  TeamMember: mockTeamMemberModel,
}

let newSubmittingStaffMember

describe('copy submitting staff member', () => {
  beforeAll(async () => {
    newSubmittingStaffMember = await copySubmittingStaffMemberBetweenManuscripts
      .initialize({
        models: mockModels,
      })
      .execute({
        copyFromManuscriptId,
        copyToManuscriptId,
      })
  })
  it('should have copied the existing submitting staff member', () => {
    expect(
      Object.prototype.hasOwnProperty.call(newSubmittingStaffMember, 'userId'),
    ).toBe(true)
  })
  it('should have created a new submittingStaffMember team and add it to the new team members', () => {
    expect(
      Object.prototype.hasOwnProperty.call(newSubmittingStaffMember, 'teamId'),
    ).toBe(true)
    expect(newSubmittingStaffMember.teamId).toEqual(newTeamId)
  })
})
