import React from 'react'
import 'jest-styled-components'
import '@testing-library/jest-dom/extend-expect'
import { cleanup, fireEvent, act } from '@testing-library/react'

import { theme } from '../'
import { render, mockMatchMedia } from './testUtils'
import AuthorCardEdit from '../src/AuthorCardEdit'

mockMatchMedia()

const author = {
  alias: {
    aff: 'Hin',
    email: 'anca@gmail.com',
    country: 'Romania',
    name: {
      givenNames: 'Anca',
      surname: 'Ursachi',
    },
  },
  isSubmitting: true,
  isCorresponding: true,
}

describe('AuthorCardEdit component', () => {
  afterEach(cleanup)

  it('Should validate empty email input', async done => {
    const saveAuthorMock = jest.fn()
    const { getByTestId, getAllByText } = render(
      <AuthorCardEdit index={0} item={author} saveAuthor={saveAuthorMock} />,
    )

    fireEvent.change(getByTestId('email-author'), {
      target: { value: '' },
    })
    fireEvent.blur(getByTestId('email-author'), {
      target: { value: '' },
    })

    setTimeout(() => {
      expect(getAllByText(/required/i)).toBeTruthy()
      expect(saveAuthorMock).toHaveBeenCalledTimes(1)
      done()
    })
  })

  it('Should validate invalid email input', async done => {
    const saveAuthorMock = jest.fn()
    const { getByTestId, getByText } = render(
      <AuthorCardEdit index={0} item={author} saveAuthor={saveAuthorMock} />,
    )

    fireEvent.change(getByTestId('email-author'), {
      target: { value: 'email' },
    })

    fireEvent.blur(getByTestId('email-author'), {
      target: { value: 'email' },
    })

    setTimeout(() => {
      expect(getByText(/Invalid email/i)).toBeInTheDocument()
      expect(getByText(/Invalid email/i)).toHaveStyleRule(
        'color',
        theme.colorError,
      )
      expect(saveAuthorMock).toHaveBeenCalledTimes(1)
      done()
    })
  })

  it('Should validate empty first name input', async done => {
    const saveAuthorMock = jest.fn()
    const { getByTestId, getAllByText } = render(
      <AuthorCardEdit index={0} item={author} saveAuthor={saveAuthorMock} />,
    )

    fireEvent.change(getByTestId('givenNames-author'), {
      target: { value: '' },
    })
    fireEvent.blur(getByTestId('givenNames-author'), {
      target: { value: '' },
    })

    setTimeout(() => {
      expect(getAllByText(/required/i)).toBeTruthy()
      expect(saveAuthorMock).toHaveBeenCalledTimes(1)
      done()
    })
  })

  it('Should validate empty last name input', async done => {
    const saveAuthorMock = jest.fn()
    const { getByTestId, getAllByText } = render(
      <AuthorCardEdit index={0} item={author} saveAuthor={saveAuthorMock} />,
    )

    fireEvent.change(getByTestId('surname-author'), {
      target: { value: '' },
    })
    fireEvent.blur(getByTestId('surname-author'), {
      target: { value: '' },
    })

    setTimeout(() => {
      expect(getAllByText(/required/i)).toBeTruthy()
      expect(saveAuthorMock).toHaveBeenCalledTimes(1)
      done()
    })
  })

  it('should save author if everything is valid', async done => {
    const saveAuthorMock = jest.fn()
    const index = 0
    const { container, getByTestId, queryByText } = render(
      <AuthorCardEdit
        index={index}
        item={author}
        saveAuthor={saveAuthorMock}
      />,
    )

    fireEvent.change(getByTestId('email-author'), {
      target: { value: 'email@gmail.com' },
    })

    fireEvent.change(getByTestId('givenNames-author'), {
      target: { value: 'givenNames' },
    })

    fireEvent.change(getByTestId('surname-author'), {
      target: { value: 'surname' },
    })

    fireEvent.change(
      container.querySelector('[data-test-id="affiliation-author"] input'),
      {
        target: { value: 'affiliation' },
      },
    )

    fireEvent.change(getByTestId('country-author'), {
      target: { value: 'country' },
    })

    setTimeout(() => {
      expect(saveAuthorMock).toHaveBeenCalledTimes(1)
      expect(queryByText(/required/i)).toBeNull()
      expect(queryByText(/Invalid email/i)).toBeNull()
      done()
    })
  })
})
