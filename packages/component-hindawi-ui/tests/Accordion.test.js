import React from 'react'
import 'jest-styled-components'
import '@testing-library/jest-dom/extend-expect'
import { cleanup, fireEvent } from '@testing-library/react'

import { render } from './testUtils'
import Accordion from '../src/new/Accordion'

describe('Accordion', () => {
  afterEach(cleanup)

  it('should toggle the item when clicking it', () => {
    const { queryByText } = render(
      <Accordion>
        <Accordion.Item title="Abstract">long text</Accordion.Item>
        <Accordion.Item title="Author Declarations">
          some declarations
        </Accordion.Item>
        <Accordion.Item number={9} title="Files">
          nine files
        </Accordion.Item>
      </Accordion>,
    )

    expect(queryByText('long text')).toBeNull()
    expect(queryByText('some declarations')).toBeNull()
    expect(queryByText('nine files')).toBeNull()

    fireEvent.click(queryByText('Author Declarations'))

    expect(queryByText('long text')).toBeNull()
    expect(queryByText('some declarations')).toBeInTheDocument()
    expect(queryByText('nine files')).toBeNull()

    fireEvent.click(queryByText('Author Declarations'))

    expect(queryByText('long text')).toBeNull()
    expect(queryByText('some declarations')).toBeNull()
    expect(queryByText('nine files')).toBeNull()
  })

  it('should close other items when clicking another one', () => {
    const { queryByText } = render(
      <Accordion>
        <Accordion.Item title="Abstract">long text</Accordion.Item>
        <Accordion.Item title="Author Declarations">
          some declarations
        </Accordion.Item>
        <Accordion.Item number={9} title="Files">
          nine files
        </Accordion.Item>
      </Accordion>,
    )
    fireEvent.click(queryByText('Abstract'))

    expect(queryByText('long text')).toBeInTheDocument()
    expect(queryByText('some declarations')).toBeNull()
    expect(queryByText('nine files')).toBeNull()

    fireEvent.click(queryByText('Author Declarations'))

    expect(queryByText('long text')).toBeNull()
    expect(queryByText('some declarations')).toBeInTheDocument()
    expect(queryByText('nine files')).toBeNull()
  })
})
