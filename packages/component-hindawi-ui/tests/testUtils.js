import React from 'react'
import { ModalProvider } from 'component-modal'
import { ThemeProvider } from 'styled-components'
import { render as rtlRender, fireEvent, wait } from '@testing-library/react'

import { theme } from '../'

export const render = ui => {
  const Component = () => (
    <ModalProvider>
      <div id="ps-modal-root" />
      <ThemeProvider theme={theme}>{ui}</ThemeProvider>
    </ModalProvider>
  )

  const utils = rtlRender(<Component />)
  return {
    ...utils,
    selectOption: async value => {
      fireEvent.click(utils.container.querySelector(`button[type=button]`))
      await wait(() => utils.getByText(value))
      fireEvent.click(utils.getByText(value))
    },
  }
}

export const mockMatchMedia = () =>
  Object.defineProperty(window, 'matchMedia', {
    writable: true,
    value: jest.fn().mockImplementation(query => ({
      matches: false,
      media: query,
      onchange: null,
      addListener: jest.fn(), // Deprecated
      removeListener: jest.fn(), // Deprecated
      addEventListener: jest.fn(),
      removeEventListener: jest.fn(),
      dispatchEvent: jest.fn(),
    })),
  })
