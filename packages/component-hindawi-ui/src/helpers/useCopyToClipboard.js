import { useState } from 'react'

const useCopyToClipboard = ({ innitialIsValueCopied }) => {
  const [isTextCopied, setIsTextCopied] = useState(innitialIsValueCopied)

  const copy = ({ value, onCopySuccess, onCopyError }) =>
    navigator.clipboard.writeText(value).then(
      _ => {
        setIsTextCopied(true)
        typeof onCopySuccess === 'function' && onCopySuccess()
      },
      _ => {
        typeof onCopyError === 'function' && onCopyError()
      },
    )

  return [isTextCopied, copy]
}

export default useCopyToClipboard
