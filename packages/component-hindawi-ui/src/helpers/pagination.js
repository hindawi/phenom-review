import { useState } from 'react'
import { compose, withStateHandlers, withProps } from 'recompose'

export const withPagination = compose(
  withStateHandlers(
    { page: 0, itemsPerPage: 3 },
    {
      toFirst: () => () => ({ page: 0 }),
      toTarget: ({ page, itemsPerPage }, { items }) => () => ({
        page: page + 1,
      }),
      nextPage: ({ page, itemsPerPage }, { items }) => () => ({
        page:
          page * itemsPerPage + itemsPerPage < items.length ? page + 1 : page,
      }),
      toLast: ({ itemsPerPage }, { items }) => () => {
        const floor = Math.floor(items.length / itemsPerPage)
        return { page: items.length % itemsPerPage ? floor : floor - 1 }
      },
      prevPage: ({ page }) => () => ({
        page: Math.max(0, page - 1),
      }),
    },
  ),
  withProps(({ items = [], itemsPerPage = 1, page = 0 }) => ({
    maxItems: items.length,
    hasMore: itemsPerPage * (page + 1) < items.length,
    paginatedItems: items.slice(page * itemsPerPage, itemsPerPage * (page + 1)),
  })),
)

export const usePagination = (totalCount, initialItemsPerPage) => {
  const [page, setPage] = useState(0)
  const [itemsPerPage, setItemsPerPage] = useState(initialItemsPerPage)

  const toFirst = () => {
    setPage(0)
  }

  const toLast = () => {
    const floor = Math.floor(totalCount / itemsPerPage)
    setPage(totalCount % itemsPerPage ? floor : floor - 1)
  }

  const changeItemsPerPage = e => {
    setItemsPerPage(e.target.value)
    setPage(0)
  }

  const nextPage = () => {
    setPage(page =>
      page * itemsPerPage + itemsPerPage < totalCount ? page + 1 : page,
    )
  }

  const prevPage = () => {
    setPage(page => Math.max(0, page - 1))
  }

  return {
    page,
    toLast,
    setPage,
    toFirst,
    prevPage,
    nextPage,
    changeItemsPerPage,
    itemsPerPage,
    hasMore: itemsPerPage * (page + 1) < totalCount,
  }
}
