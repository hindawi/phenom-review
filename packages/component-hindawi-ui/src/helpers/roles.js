export default {
  USER: 'user',
  ADMIN: 'admin',
  AUTHOR: 'author',
  REVIEWER: 'reviewer',
  TRIAGE_EDITOR: 'triageEditor',
  ACADEMIC_EDITOR: 'academicEditor',
  EDITORIAL_ASSISTANT: 'editorialAssistant',
  RESEARCH_INTEGRITY_PUBLISHING_EDITOR: 'researchIntegrityPublishingEditor',
  SUBMITTING_STAFF_MEMBER: 'submittingStaffMember',
}
