const parseAffiliations = (authors = []) =>
  authors.reduce(
    (acc, curr) => {
      if (acc.affiliations.includes(curr.alias.aff)) {
        acc.authors = [
          ...acc.authors,
          {
            ...curr,
            affiliationNumber:
              acc.affiliations.findIndex(e => e === curr.alias.aff) + 1,
          },
        ]
      } else {
        acc.affiliations = [...acc.affiliations, curr.alias.aff]
        acc.authors = [
          ...acc.authors,
          {
            ...curr,
            affiliationNumber:
              acc.affiliations.findIndex(e => e === curr.alias.aff) + 1,
          },
        ]
      }

      return acc
    },
    { affiliations: [], authors: [] },
  )

export default parseAffiliations
