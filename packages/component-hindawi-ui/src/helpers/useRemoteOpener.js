import { useState } from 'react'

const useRemoteOpener = (startExpanded = false) => {
  const [expanded, setExpanded] = useState(startExpanded)
  const toggle = () => setExpanded(!expanded)

  return [expanded, toggle]
}

export default useRemoteOpener
