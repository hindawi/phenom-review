import React, { Fragment } from 'react'
import Tippy from '@tippy.js/react'
import { ThemeProvider, withTheme } from 'styled-components'

import { AuthorTag, Text, Row } from '../'

const AuthorTooltip = ({ author = {}, key, theme = {} }) => (
  <ThemeProvider theme={theme}>
    <Fragment>
      <Row mt={1}>
        <AuthorTag author={author} key={key} />
      </Row>
      <Row>
        <Text>{author.alias.email}</Text>
      </Row>
      <Row>
        <Text>{author.alias.aff}</Text>
      </Row>
    </Fragment>
  </ThemeProvider>
)

const AuthorWithTooltip = ({ theme = {}, ...rest }) => (
  <Tippy
    arrow
    content={<AuthorTooltip theme={theme} {...rest} />}
    placement="bottom"
    theme="light"
  >
    <span>
      <AuthorTag {...rest} />
    </span>
  </Tippy>
)

export default withTheme(AuthorWithTooltip)
