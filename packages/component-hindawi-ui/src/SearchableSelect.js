import React, { useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import Downshift from 'downshift'
import styled, { css } from 'styled-components'
import { th, override, validationColor } from '@pubsweet/ui-toolkit'

import { useClientRect, Label, Icon } from '../'

const SearchableSelect = ({
  value,
  options,
  disabled,
  onChange,
  isLoading,
  noOptions,
  noResults,
  filterFunc,
  fieldLabel,
  placeholder,
  selectedItem,
  validationStatus,
  displayedOptions,
  onInputValueChange,
}) => {
  const [inputNode, inputPosition] = useClientRect()
  const downshiftInstance = useRef()

  useEffect(() => {
    if (!value && downshiftInstance) downshiftInstance.current.clearSelection()
  }, [value])

  const findValueLabel = (value, options) => {
    const found = options.find(o => o.value === value)
    return found ? found.label : ''
  }
  return (
    <Downshift
      defaultHighlightedIndex={0}
      initialInputValue={findValueLabel(value, options)}
      itemToString={item => (item ? item.label : '')}
      onChange={onChange}
      onInputValueChange={inputValue =>
        onInputValueChange && onInputValueChange(inputValue)
      }
      ref={downshiftInstance}
      selectedItem={selectedItem}
    >
      {({
        isOpen,
        inputValue,
        toggleMenu,
        selectedItem,
        getItemProps,
        getRootProps,
        getMenuProps,
        getInputProps,
        getLabelProps,
        clearSelection,
        highlightedIndex,
      }) => (
        <Root {...getRootProps()}>
          {fieldLabel && (
            <Label as="label" {...getLabelProps()}>
              {fieldLabel}
            </Label>
          )}
          <InputWrapper>
            <Input
              {...getInputProps({
                placeholder,
                disabled,
                validationStatus,
              })}
              ref={inputNode}
            />
            {inputValue ? (
              <StyledIcon
                disabled={disabled}
                fontSize="16px"
                icon="remove"
                onClick={
                  disabled
                    ? undefined
                    : () => {
                        clearSelection()
                        onChange()
                      }
                }
              />
            ) : (
              <StyledIcon
                disabled={disabled}
                fontSize="16px"
                icon="search"
                onClick={disabled ? undefined : toggleMenu}
              />
            )}
          </InputWrapper>
          <Dropdown>
            <ItemList
              {...getMenuProps({
                isOpen,
                inputPosition,
                displayedOptions,
              })}
            >
              {isOpen
                ? renderOptions({
                    options,
                    isLoading,
                    noResults,
                    noOptions,
                    inputValue,
                    filterFunc,
                    selectedItem,
                    getItemProps,
                    highlightedIndex,
                  })
                : null}
            </ItemList>
          </Dropdown>
        </Root>
      )}
    </Downshift>
  )
}

SearchableSelect.propTypes = {
  displayedOptions: PropTypes.number,
  fieldLabel: PropTypes.node,
  filterFunc: PropTypes.func,
  noOptions: PropTypes.node,
  noResults: PropTypes.node,
  placeholder: PropTypes.string,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string,
      label: PropTypes.node,
      sublabel: PropTypes.node,
    }),
  ),
  onChange: PropTypes.func,
}

SearchableSelect.defaultProps = {
  displayedOptions: 5,
  fieldLabel: '',
  filterFunc: (item, inputValue) =>
    !inputValue ||
    item.label.toLowerCase().includes(inputValue.toLowerCase()) ||
    (item.sublabel &&
      item.sublabel.toLowerCase().includes(inputValue.toLowerCase())),
  noOptions: 'No options available',
  noResults: 'No results found',
  placeholder: '',
  onChange: () => {},
  options: [],
}

const renderOptions = ({
  options,
  isLoading,
  noResults,
  noOptions,
  inputValue,
  filterFunc,
  selectedItem,
  getItemProps,
  highlightedIndex,
}) => {
  if (isLoading) return <NoItems>Loading...</NoItems>
  if (!options.length) return <NoItems>{noOptions}</NoItems>

  const renderedOptions = options.filter(item => filterFunc(item, inputValue))

  return renderedOptions.length ? (
    renderedOptions.map((item, index) => (
      <Item
        {...getItemProps({
          key: item.value,
          index,
          item,
          isHighlighted: highlightedIndex === index,
        })}
      >
        <ItemLabel isSelected={selectedItem === item}>{item.label}</ItemLabel>
        {item.sublabel && <ItemSublabel>{item.sublabel}</ItemSublabel>}
      </Item>
    ))
  ) : (
    <NoItems>{noResults}</NoItems>
  )
}

const Dropdown = ({ children }) =>
  ReactDOM.createPortal(children, document.body)

export default SearchableSelect

// #region styles
const dropdownPosition = ({ inputPosition }) =>
  css`
    top: ${inputPosition.bottom}px;
    left: ${inputPosition.left}px;
    width: ${inputPosition.width}px;
  `

const Root = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  ${override('ui.TextField')};
`

const InputWrapper = styled.div`
  position: relative;
  width: 100%;
`

const Input = styled.input`
  box-sizing: border-box;
  width: 100%;
  height: calc(${th('gridUnit')} * 8);
  padding: 0;
  padding-left: calc(${th('gridUnit')});
  padding-right: calc(${th('gridUnit')} * 8);

  border-width: ${th('borderWidth')};
  border-style: ${th('borderStyle')};
  border-color: ${({ disabled }) =>
    disabled ? th('colorBorder') : validationColor};
  border-radius: ${th('borderRadius')};

  font-family: ${th('defaultFont')};
  font-size: ${th('fontSizeBase')};
  color: ${({ disabled }) =>
    disabled ? th('colorBorder') : th('textPrimaryColor')};
  ::placeholder {
    color: ${({ disabled }) =>
      disabled ? th('colorBorder') : th('textPrimaryColor')};
    opacity: 1;
    font-family: ${th('defaultFont')};
  }
  :focus {
    outline: none;
    border-color: ${th('action.colorActive')};
  }
`

const StyledIcon = styled(Icon)`
  position: absolute;
  top: 10px;
  right: 8px;

  color: ${({ disabled }) =>
    disabled ? th('colorBorder') : th('textPrimaryColor')};
  cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer')};
`

const ItemList = styled.ul`
  position: absolute;
  ${dropdownPosition}

  max-height: calc(
    ${th('gridUnit')} * 8 * ${({ displayedOptions }) => displayedOptions}
  );
  overflow: auto;
  padding: 0;
  margin: 0;
  margin-top: 2px;

  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  border-radius: ${th('borderRadius')};
  z-index: ${th('zIndex.select')};
  list-style-type: none;
  visibility: ${({ isOpen }) => (isOpen ? 'visible' : 'hidden')};
  box-shadow: 0 2px 6px -1px rgba(125, 125, 125, 0.5);
`

const Item = styled.li`
  display: flex;
  align-items: center;
  justify-content: space-between;

  padding: 0 calc(${th('gridUnit')} * 4);
  height: calc(${th('gridUnit')} * 8);

  background: ${({ isHighlighted }) =>
    isHighlighted ? th('highlightColor') : th('white')};
  font-family: ${th('defaultFont')};
  font-size: ${th('fontSizeBase')};
  cursor: pointer;
`

const ItemLabel = styled.span`
  margin-right: 16px;
  display: inline-block;
  align-items: center;

  color: ${th('textPrimaryColor')};
  font-weight: ${({ isSelected }) => (isSelected ? '700' : 'initial')};

  min-width: 0;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

const ItemSublabel = styled.span`
  display: flex;
  align-items: center;
  color: ${th('textSecondaryColor')};
`

const NoItems = styled.li`
  display: flex;
  align-items: center;
  padding: 0 calc(${th('gridUnit')} * 4);
  height: calc(${th('gridUnit')} * 8);

  color: ${th('textSecondaryColor')};
  background: ${th('white')};
  font-family: ${th('defaultFont')};
  font-size: ${th('fontSizeBaseMedium')};
  font-style: italic;
  cursor: default;
`
// #endregion
