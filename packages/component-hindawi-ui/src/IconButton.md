A Feather clickable icon.

```js
<IconButton onClick={() => console.log('i clicked on you')} icon="eye" />
```
