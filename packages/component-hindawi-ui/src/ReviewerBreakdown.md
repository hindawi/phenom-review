Reviewer Breakdown Component

```js
const reviewers = [
  {
    id: '37b0a97e-7d61-4524-9b3e-55b494e05a51',
    isSubmitting: null,
    isCorresponding: null,
    invited: '2019-02-27T05:56:51.040Z',
    responded: '2019-02-27T10:12:49.267Z',
    user: {
      id: '5aead681-f12b-4528-b215-d7726cb4ef64',
    },
    status: 'pending',
    alias: {
      aff: 'TSD',
      email: 'alexandru.munteanu+rev3@hindawi.com',
      country: 'BE',
      name: {
        surname: 'REV3',
        givenNames: 'AlexREV3',
      },
    },
  },
  {
    id: 'db0cf125-195f-4cd5-aa4a-d41e151f6ffd',
    isSubmitting: null,
    isCorresponding: null,
    invited: '2019-02-27T10:12:49.257Z',
    responded: '2019-02-27T10:12:49.257Z',
    user: {
      id: 'f35cb7f6-c605-4bcf-b3ed-fc6e9f93c39f',
    },
    status: 'accepted',
    alias: {
      aff: 'TSD',
      email: 'alexandru.munteanu+rev4@hindawi.com',
      country: 'AL',
      name: {
        surname: 'REV4',
        givenNames: 'AlexREV4',
      },
    },
  },
  {
    id: 'db0cf125-195f-4cd5-aa4a-d41e151f6ffd',
    isSubmitting: null,
    isCorresponding: null,
    invited: '2019-02-27T10:12:49.257Z',
    responded: '2019-02-27T10:12:49.257Z',
    user: {
      id: 'f35cb7f6-c605-4bcf-b3ed-fc6e9f93c39f',
    },
    status: 'declined',
    alias: {
      aff: 'TSD',
      email: 'alexandru.munteanu+rev4@hindawi.com',
      country: 'AL',
      name: {
        surname: 'REV4',
        givenNames: 'AlexREV4',
      },
    },
  },
]
;<ReviewerBreakdown reviewers={reviewers} />
```
