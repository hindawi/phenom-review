import React from 'react'
import { space } from 'styled-system'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const StatusTag = ({
  children,
  old,
  version,
  statusColor,
  hasVersion,
  ...rest
}) => (
  <Root
    data-test-id="manuscript-status"
    old={old}
    pl={4}
    pr={hasVersion ? 0 : 4}
    statusColor={statusColor}
    {...rest}
  >
    {children}
    {!old && hasVersion && (
      <VersionTag ml={4} statusColor={statusColor}>
        v{version}
      </VersionTag>
    )}
  </Root>
)

export default StatusTag

// #region styles
const getStatusColor = ({ statusColor, theme, old }) =>
  old ? theme.textSecondaryColor : theme[statusColor]

const VersionTag = styled.span`
  align-items: center;
  background-color: ${getStatusColor};
  border-top-right-radius: ${th('borderRadius')};
  border-bottom-right-radius: ${th('borderRadius')};
  color: ${th('backgroundColor')};
  display: flex;
  font-family: ${th('defaultFont')};
  height: inherit;
  justify-content: center;
  min-width: calc(${th('gridUnit')} * 6);
  text-transform: lowercase;

  ${space};
`

const Root = styled.div`
  align-items: center;
  background-color: ${th('white')};
  border: 1px solid ${getStatusColor};
  border-radius: ${th('borderRadius')};
  border-width: 1px;
  border-style: solid;
  color: ${getStatusColor};
  display: flex;
  height: calc(${props => props.height || 6} * ${th('gridUnit')});
  font-size: ${th('text.fontSizeBaseSmall')};
  font-family: ${th('defaultFont')};
  font-weight: 700;
  text-align: center;
  text-transform: upperCase;
  width: fit-content;
  white-space: nowrap;

  ${space};
`
// #endregion
