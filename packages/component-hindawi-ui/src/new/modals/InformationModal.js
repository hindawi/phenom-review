import React from 'react'
import { H2, Button } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { Row, Text, Icon } from '@hindawi/ui'
import { Modal } from './Modal'

export const InformationModal = ({ icon, title, error, toggle }) => (
  <Modal toggle={toggle}>
    <Row alignItems="start">
      {icon && (
        <Icon
          fontSize="48px"
          icon={icon}
          lineHeight="48px"
          mr={7}
          reverse
          warning
        />
      )}
      <Row alignItems="start" flexDirection="column">
        <H2 color={th('grey70')}>{title}</H2>
        {error && (
          <Text color={th('colorError')} mt={2}>
            {error}
          </Text>
        )}
      </Row>
    </Row>
    <Row mt={12}>
      <Button mr={3} onClick={toggle} primary width={25}>
        OK
      </Button>
    </Row>
  </Modal>
)
