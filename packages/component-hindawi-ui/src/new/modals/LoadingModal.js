import React from 'react'
import { Loader } from '@hindawi/ui'
import { Modal } from './Modal'

export const LoadingModal = ({ toggle }) => (
  <Modal toggle={toggle}>
    <Loader iconSize={15} />
  </Modal>
)
