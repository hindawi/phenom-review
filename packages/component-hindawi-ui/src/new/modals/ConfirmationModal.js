import React from 'react'
import { H2, Button, Spinner } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { Row, Text, Icon, AuthorsListWithAffiliation } from '@hindawi/ui'
import { Modal } from './Modal'

export const ConfirmationModal = ({
  icon,
  error,
  title,
  toggle,
  authors,
  loading,
  subtitle,
  onConfirm,
  acceptButtonLabel = 'Accept',
  cancelButtonLabel = 'Cancel',
}) => (
  <Modal toggle={toggle}>
    <Row alignItems="start">
      {icon && (
        <Icon
          fontSize="48px"
          icon={icon}
          lineHeight="48px"
          mr={7}
          reverse
          warning
        />
      )}
      <Row alignItems="start" flexDirection="column">
        <H2 color={th('grey70')}>{title}</H2>
        {subtitle && (
          <Text color={th('grey70')} mt={2}>
            {subtitle}
          </Text>
        )}
        {authors && (
          <div>
            <AuthorsListWithAffiliation authors={authors} />
            <Text color={th('grey70')} mt={2}>
              Do you want to invite this reviewer anyway?
            </Text>
          </div>
        )}
        {error && (
          <Text color={th('colorError')} mt={2}>
            {error}
          </Text>
        )}
      </Row>
    </Row>

    <Row justify="flex-end" mt={12}>
      <Button invert onClick={toggle} width={25}>
        {cancelButtonLabel}
      </Button>
      <Button
        ml={3}
        onClick={loading ? undefined : onConfirm}
        primary
        width={25}
      >
        {loading ? <Spinner color="white" size={4} /> : acceptButtonLabel}
      </Button>
    </Row>
  </Modal>
)
