/* eslint-disable sonarjs/cognitive-complexity */
import React, { useCallback, useState, useEffect, useRef } from 'react'
import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'
import { th } from '@pubsweet/ui-toolkit'
import { Text, Row, Icon } from '@hindawi/ui'
import { ellipsis, regular, regularItalic } from '../styledHelpers'

const Dropdown = ({
  value,
  variant = 'medium',
  options,
  optionId,
  onChange,
  maxItems,
  placeholder,
  optionLabel,
  clearOptionId,
}) => {
  const ref = useRef(null)
  const [opened, setOpen] = useState(false)
  const [selectedValue, select] = useState(value)

  const openMenu = useCallback(() => setOpen(true), [setOpen])
  const closeMenu = useCallback(() => setOpen(false), [setOpen])

  useEffect(() => {
    select(value)
  }, [value])

  const handleSelect = useCallback(
    value => () => {
      const isClearOption = value[optionId] === clearOptionId

      onChange(isClearOption ? undefined : value)
      select(isClearOption ? undefined : value)
    },
    [onChange, clearOptionId, optionId],
  )

  const handleClickOutside = useCallback(
    event => {
      if (!opened) return

      if (ref.current && !ref.current.contains(event.target)) {
        closeMenu()
      }
    },
    [opened, closeMenu],
  )

  const handleEscape = useCallback(
    event => {
      if (event.key === 'Escape' && opened) {
        closeMenu()
      }
    },
    [opened, closeMenu],
  )

  useEffect(() => {
    if (opened) {
      document.addEventListener('click', handleClickOutside)
      document.addEventListener('keydown', handleEscape)

      return () => {
        document.removeEventListener('click', handleClickOutside)
        document.removeEventListener('keydown', handleEscape)
      }
    }
  }, [handleClickOutside, handleEscape, opened])

  return (
    <Root>
      <MenuOpener
        onClick={openMenu}
        opened={opened}
        ref={ref}
        selected={selectedValue}
        variant={variant}
      >
        <Text>{selectedValue ? selectedValue[optionLabel] : placeholder}</Text>
        <Icon
          color="grey80"
          fontSize={14}
          icon={opened ? 'caretUp' : 'caretDown'}
          ml={2}
        />
      </MenuOpener>

      {opened && (
        <OptionsContainer
          numberOfItems={Math.min(options.length, maxItems)}
          variant={variant}
        >
          {options.map(option => (
            <MenuOption
              isClearOption={option[optionId] === clearOptionId}
              key={option[optionId]}
              onMouseDown={handleSelect(option)}
              variant={variant}
            >
              {option[optionLabel]}
            </MenuOption>
          ))}
        </OptionsContainer>
      )}
    </Root>
  )
}

// #region styles
const OPTION_HEIGHT = {
  small: 6,
  medium: 8,
}

const containerHeight = ({ numberOfItems, variant }) => css`
  height: calc(
    ${th('gridUnit')} * ${numberOfItems} * ${OPTION_HEIGHT[variant]}
  );
`

const optionStyle = ({ variant }) => css`
  cursor: pointer;
  height: calc(${th('gridUnit')} * ${OPTION_HEIGHT[variant]});
  padding: 0 calc(${th('gridUnit')} * 2);
  width: 100%;
`

const Root = styled(Row)`
  position: relative;
  width: auto;
`

const MenuOpener = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border: 1px solid ${th('furniture')};
  border-radius: ${th('gridUnit')};
  font-size: ${th('mainTextSize')};

  ${optionStyle};
  ${ellipsis};

  ${({ selected }) =>
    selected
      ? css`
          color: ${th('grey70')};
          ${regular}
        `
      : css`
          color: ${th('grey60')};
          ${regularItalic}
        `};

  &:hover {
    border-color: ${th('grey80')};
  }

  ${({ opened }) => {
    if (!opened)
      return css`
        border: 1px solid ${th('transparent')};
      `
  }}}
`

const OptionsContainer = styled.div`
  background-color: ${th('white')};
  border-radius: ${th('gridUnit')};
  box-shadow: 0 0 ${th('gridUnit')} rgba(36, 36, 36, 0.8);

  overflow-y: auto;
  position: absolute;
  top: calc(${th('gridUnit')} * 10);
  width: 100%;
  z-index: 2;

  ${containerHeight};
  ${regular};
`

const menuOption = ({ isClearOption }) =>
  isClearOption
    ? css`
        color: ${th('grey70')};
        border-bottom: 1px solid ${th('furniture')};

        ${regularItalic};
      `
    : null

const MenuOption = styled.div`
  font-size: ${th('mainTextSize')};
  ${optionStyle};
  padding-top: ${th('gridUnit')};
  ${ellipsis};
  ${menuOption};

  &:hover {
    background-color: ${th('grey20')};
  }

  &:first-child {
    border-top-left-radius: ${th('gridUnit')};
    border-top-right-radius: ${th('gridUnit')};
  }

  &:last-child {
    border-bottom-left-radius: ${th('gridUnit')};
    border-bottom-right-radius: ${th('gridUnit')};
  }
`
// #endregion

Dropdown.propTypes = {
  value: PropTypes.shape({}).isRequired,
  options: PropTypes.arrayOf(PropTypes.any).isRequired,
  maxItems: PropTypes.number,
  optionId: PropTypes.string,
  optionLabel: PropTypes.string,
  placeholder: PropTypes.string,
  clearOptionId: PropTypes.string,
  onChange: PropTypes.func.isRequired,
}

Dropdown.defaultProps = {
  optionId: 'id',
  optionLabel: 'label',
  placeholder: 'Select...',
  maxItems: 6,
  clearOptionId: 'none',
}

MenuOption.propTypes = {
  isClearOption: PropTypes.bool,
}

OptionsContainer.prototypes = {
  numberOfItems: PropTypes.number,
}

MenuOpener.propTypes = {
  selected: PropTypes.shape({}),
}

export default Dropdown
