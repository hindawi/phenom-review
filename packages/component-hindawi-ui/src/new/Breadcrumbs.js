import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Row, Icon, Text } from '@hindawi/ui'

const Breadcrumbs = ({
  label,
  goBack,
  backLabel,
  entityName,
  separator = '/',
  ...rest
}) => (
  <Root justifyContent="flex-start" onClick={goBack} {...rest}>
    <Icon bold color="colorWarning" fontSize="16px" icon="bredcrumbs2" />
    <Text fontWeight="bold" ml={2}>
      {backLabel}
    </Text>

    {label && (
      <Fragment>
        <Text ml={4}>{`${label} ${separator}`}</Text>
        {entityName && (
          <Text fontWeight="bold" ml={1}>
            {entityName}
          </Text>
        )}
      </Fragment>
    )}
  </Root>
)

Breadcrumbs.propTypes = {
  goBack: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  backLabel: PropTypes.string.isRequired,
  entityName: PropTypes.string.isRequired,
  separator: PropTypes.string,
}

Breadcrumbs.defaultProps = {
  separator: '/',
}

// #region styles

const Root = styled(Row)`
  cursor: pointer;
  width: max-content;
  justify-content: center;
  align-items: center;
`
// #endregion

export default Breadcrumbs
