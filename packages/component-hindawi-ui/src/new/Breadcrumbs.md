A clickable text button.

```js
<Breadcrumbs
  backLabel="BACK"
  label="DASHBOARD"
  entityName="ARTICLE DETAILS"
  goBack={() => alert('Go back')}
/>
```
