import React from 'react'
import { TextField } from '@pubsweet/ui'

const PasswordField = props => <TextField {...props} type="password" />

export default PasswordField
