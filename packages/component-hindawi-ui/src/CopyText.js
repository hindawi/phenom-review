import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button, Icon } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { useCopyToClipboard } from './helpers'

const CopyText = ({
  children: value,
  textAfterCopy,
  onCopySuccess,
  onCopyError,
}) => {
  const [isTextCopied, copy] = useCopyToClipboard(false)

  textAfterCopy = textAfterCopy || value

  const isSecuredContext = window.isSecureContext

  const handleOnClick = () => {
    copy({
      value,
      onCopySuccess,
      onCopyError,
    })
  }

  const innitialContent = (
    <Fragment>
      {value}
      {isSecuredContext && (
        <CopyButton onClick={handleOnClick}>Copy</CopyButton>
      )}
    </Fragment>
  )

  const afterCopyValue = (
    <Fragment>
      {textAfterCopy}
      <SuccessIcon size={3}>check</SuccessIcon>
    </Fragment>
  )

  return isTextCopied ? afterCopyValue : innitialContent
}

CopyText.propTypes = {
  children: PropTypes.string.isRequired,
  onCopySuccess: PropTypes.func,
  onCopyError: PropTypes.func,
  textAfterCopy: PropTypes.string,
}

CopyText.defaultProps = {
  onCopySuccess: () => {},
  onCopyError: () => {},
  textAfterCopy: undefined,
}

const SuccessIcon = styled(Icon)`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 10px;
  width: 14px;
  height: 14px;
  text-align: center;
  line-height: 14px;
  border-radius: 100%;
  border: 1px ${th('statusApproved')} solid;
  color: ${th('statusApproved')};
  margin: 2px 0 2px 8px;
  > svg {
    stroke: ${th('statusApproved')};
    &:hover {
      stroke: ${th('statusApproved')};
    }
  }
`

const CopyButton = styled(Button)`
  font-size: 12px;
  font-weight: normal;
  line-height: 16px;
  background-color: ${th('grey60')};
  color: ${th('white')};
  height: auto;
  width: auto;
  display: block;
  margin-left: 8px;
  padding: 0 4px;
  border: none;
  min-width: 0;
  &:hover {
    background-color: ${th('grey50')};
    border: none;
  }
`

export default CopyText
