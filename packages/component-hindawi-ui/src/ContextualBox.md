_Component to show or hide it's children. Can be controlled from a parent component by passing the expanded state and toggle callback._

---

A collapseable contextual box.

```jsx
<ContextualBox label="Reviewer reports">
  <div
    style={{
      padding: 16,
      backgroundColor: 'lavenderblush',
    }}
  >
    What is Lorem Ipsum? Lorem Ipsum is simply dummy text of the printing and
    typesetting industry. Lorem Ipsum has been the industry's standard dummy
    text ever since the 1500s, when an unknown printer took a galley of type and
    scrambled it to make a type specimen book. It has survived not only five
    centuries, but also the leap into electronic typesetting, remaining
    essentially unchanged. It was popularised in the 1960s with the release of
    Letraset sheets containing Lorem Ipsum passages, and more recently with
    desktop publishing software like Aldus PageMaker including versions of Lorem
    Ipsum. Why do we use it? It is a long established fact that a reader will be
    distracted by the readable content of a page when looking at its layout. The
    point of using Lorem Ipsum is that it has a more-or-less normal distribution
    of letters, as opposed to using 'Content here, content here', making it look
    like readable English. Many desktop publishing packages and web page editors
    now use Lorem Ipsum as their default model text, and a search for 'lorem
    ipsum' will uncover many web sites still in their infancy. Various versions
    have evolved over the years, sometimes by accident, sometimes on purpose
    (injected humour and the like).
  </div>
</ContextualBox>
```

A contextual box without border, shadow and no backgrounds.

```jsx
import { File } from '../'
;<ContextualBox label="Files" transparent>
  <div>
    <File item={{ id: '1', name: 'myfile.pdf', size: 123123 }} />
    <File item={{ id: '2', name: 'myfile2.pdf', size: 1123 }} />
  </div>
</ContextualBox>
```

React components can be passed as right children too. All the props provided to the contextual box are also passed to the right header children.

```jsx
const MyRightComponent = ({ headLabel }) => (
  <div>{headLabel}: 1 accepted, 4 denied</div>
)

;<ContextualBox
  headLabel="I am the right header * "
  label="React component to the right"
  rightChildren={MyRightComponent}
>
  <div
    style={{
      padding: 16,
      backgroundColor: 'lavenderblush',
    }}
  >
    What is Lorem Ipsum? Lorem Ipsum is simply dummy text of the printing and
    typesetting industry. Lorem Ipsum has been the industry's standard dummy
    text ever since the 1500s, when an unknown printer took a galley of type and
    scrambled it to make a type specimen book. It has survived not only five
    centuries, but also the leap into electronic typesetting, remaining
    essentially unchanged. It was popularised in the 1960s with the release of
    Letraset sheets containing Lorem Ipsum passages, and more recently with
    desktop publishing software like Aldus PageMaker including versions of Lorem
    Ipsum. Why do we use it? It is a long established fact that a reader will be
    distracted by the readable content of a page when looking at its layout. The
    point of using Lorem Ipsum is that it has a more-or-less normal distribution
    of letters, as opposed to using 'Content here, content here', making it look
    like readable English. Many desktop publishing packages and web page editors
    now use Lorem Ipsum as their default model text, and a search for 'lorem
    ipsum' will uncover many web sites still in their infancy. Various versions
    have evolved over the years, sometimes by accident, sometimes on purpose
    (injected humour and the like).
  </div>
</ContextualBox>
```

A controlled ContextualBox. This is usually used together with the RemoteOpener component.

```jsx
import RemoteOpener from './RemoteOpener'
const MyRightComponent = () => <div>works like a charm!</div>

;<RemoteOpener>
  {({ expanded, toggle }) => (
    <div>
      <button onClick={toggle}>Toggle</button>
      <ContextualBox
        toggle={toggle}
        expanded={expanded}
        label="Controlled contextual box"
        rightChildren={MyRightComponent}
      >
        <div>Peek a boo!</div>
      </ContextualBox>
    </div>
  )}
</RemoteOpener>
```
