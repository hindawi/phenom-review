import React, { Fragment } from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import { DateParser } from '@pubsweet/ui'
import { withProps, compose } from 'recompose'

import {
  Label,
  Item,
  Box,
  Row,
  Text,
  Tag,
  getReportComments,
  getShortRole,
} from '../'
import { RejectDecisionInfo } from './RejectDecisionInfo'

const EditorialReportCard = ({
  decisions,
  editorRole,
  editorName,
  publicLabel,
  privateLabel,
  publicReport,
  privateReport,
  recommendation,
  triageEditorLabel,
  academicEditorLabel,
  report: { created, member, rejectDecisionInfo },
  ...rest
}) => (
  <Box padding={4} {...rest}>
    <Row alignItems="flex-start" justify="space-between">
      <Item vertical>
        <Label mb={2}>
          {editorRole === 'AE' ? 'Recommendation' : 'Decision'}
        </Label>
        <Text>{recommendation}</Text>
      </Item>

      <Item alignItems="center" justify="flex-end">
        {member && (
          <Fragment>
            <Text mr={1}>{editorName}</Text>
            {editorRole && <Tag mr={4}>{editorRole}</Tag>}
          </Fragment>
        )}
        {created && (
          <DateParser timestamp={created}>
            {date => <Text secondary>{date}</Text>}
          </DateParser>
        )}
      </Item>
    </Row>

    {publicReport && (
      <Row mt={4}>
        <Item vertical>
          <Label mb={2}>{publicLabel}</Label>
          <Text whiteSpace="pre-wrap">{publicReport}</Text>
        </Item>
      </Row>
    )}

    {privateReport && (
      <Row mt={4}>
        <Item vertical>
          <Label mb={2}>{privateLabel}</Label>
          <Text whiteSpace="pre-wrap">{privateReport}</Text>
        </Item>
      </Row>
    )}

    {rejectDecisionInfo && (
      <RejectDecisionInfo rejectDecisionInfo={rejectDecisionInfo} />
    )}
  </Box>
)

export default compose(
  withProps(
    ({ report, decisions, academicEditorLabel, triageEditorLabel }) => ({
      recommendation: get(
        decisions.find(d => d.value === get(report, 'recommendation', '')),
        'message',
        '',
      ),
      publicReport: getReportComments({ report, type: 'public' }),
      privateReport: getReportComments({ report, type: 'private' }),
      editorName: `${get(report, 'member.alias.name.givenNames', '')} ${get(
        report,
        'member.alias.name.surname',
        '',
      )}`,
      editorRole: getShortRole({
        role: get(report, 'member.role', ''),
        academicEditorLabel,
        triageEditorLabel,
      }),
    }),
  ),
)(EditorialReportCard)

EditorialReportCard.propTypes = {
  /** Array containing the list of recommendations / decisions */
  decisions: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string,
      label: PropTypes.string,
    }),
  ),
  /** Label that will be publicly viewed, for example, a message for the author will be seen by other roles also. */
  publicLabel: PropTypes.string,
  /** Label that will only be viewed as private message, for example, by the Editorial Team. */
  privateLabel: PropTypes.string,
  /** Message by editorial team and other information. */
  report: PropTypes.shape({
    id: PropTypes.string,
    comments: PropTypes.arrayOf(PropTypes.object),
    created: PropTypes.string,
    submitted: PropTypes.string,
    recommendation: PropTypes.string,
    member: PropTypes.object,
    rejectDecisionInfo: PropTypes.object,
  }),
}

EditorialReportCard.defaultProps = {
  decisions: [],
  publicLabel: '',
  privateLabel: '',
  report: {},
  // journal: {},
}
