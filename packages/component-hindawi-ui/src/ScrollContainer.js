import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { get } from 'lodash'

const ScrollContainer = styled.div`
  overflow-y: scroll;
  height: calc(
    100vh - ${th('gridUnit')} * ${props => get(props, 'height', 101)}
  );
`

export default ScrollContainer
