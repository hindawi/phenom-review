Dropdown for various user actions

```js
const { DropdownOption } = require('./Dropdown')
;<Dropdown>
  <DropdownOption>Ministry of Magic</DropdownOption>
  <DropdownOption>University of Unusual</DropdownOption>
  <DropdownOption>International Invokers</DropdownOption>
  <DropdownOption>Establishment of Evokations</DropdownOption>
  <DropdownOption>Pathfinder Patronus</DropdownOption>
  <DropdownOption>Supernatural Service</DropdownOption>
  <DropdownOption>Dementor Detention Desk</DropdownOption>
</Dropdown>
```
