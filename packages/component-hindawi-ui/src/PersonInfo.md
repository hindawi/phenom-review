Details of a person.

```js
const anAuthor = {
  id: '992eb580-63d7-4e10-bccf-55255f8fa450',
  isSubmitting: true,
  isCorresponding: true,
  alias: {
    aff: 'Boko Haram',
    email: 'alexandru.munteanu+a1@hindawi.com',
    country: 'RO',
    name: {
      givenNames: 'AlexA1',
      surname: 'Author1',
      title: null,
    },
  },
}
;<PersonInfo person={anAuthor} />
```
