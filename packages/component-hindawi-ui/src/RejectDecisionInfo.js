import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Text as ParagraphMessage, IconLock } from '@hindawi/phenom-ui'
import { Preset } from '@hindawi/phenom-ui/dist/Typography/Text'
import { Label, Item, Row, Text } from '../'

const EditorialMessage = ({ label, children, color = '#56A03D' }) => (
  <Row mt={5}>
    <Item vertical>
      <Label mb={1}>{label}</Label>
      {children}
      <Item alignItems="center" mt={1}>
        <IconLock
          style={{
            color,
            fontSize: '11px',
            marginRight: '3px',
          }}
        />
        <ParagraphMessage preset={Preset.MESSAGE} style={{ color }}>
          Not visible to authors and reviewers
        </ParagraphMessage>
      </Item>
    </Item>
  </Row>
)

const VerticalSeparator = styled.div`
  background-color: ${th('colorFurniture')};
  height: 1px;
  margin-top: 16px;
}
`

export const RejectDecisionInfo = ({ rejectDecisionInfo }) => (
  <>
    <VerticalSeparator />

    <EditorialMessage label="Reasons for rejection">
      <ul style={{ paddingLeft: '18px', marginBottom: '0px' }}>
        {rejectDecisionInfo.reasonsForRejection.outOfScope && (
          <li>
            <Text>Out of scope</Text>
          </li>
        )}

        {rejectDecisionInfo.reasonsForRejection.technicalOrScientificFlaws && (
          <li>
            <Text>Technical/scientific flaws</Text>
          </li>
        )}

        {rejectDecisionInfo.reasonsForRejection.publicationEthicsConcerns && (
          <li>
            <Text>Publication ethics concerns</Text>
          </li>
        )}

        {rejectDecisionInfo.reasonsForRejection.lackOfNovelty && (
          <li>
            <Text>Lack of novelty or perceived impact</Text>
          </li>
        )}
      </ul>
    </EditorialMessage>

    {rejectDecisionInfo.reasonsForRejection.otherReasons && (
      <EditorialMessage label="Other reason(s)">
        <Text whiteSpace="pre-wrap">
          {rejectDecisionInfo.reasonsForRejection.otherReasons}
        </Text>
      </EditorialMessage>
    )}

    <VerticalSeparator />

    <EditorialMessage label="Transfer to other journals recommended">
      <Text>
        {rejectDecisionInfo.transferToAnotherJournal.selectedOption === 'YES'
          ? 'Yes'
          : 'No'}
      </Text>
    </EditorialMessage>

    {rejectDecisionInfo.transferToAnotherJournal.transferSuggestions && (
      <EditorialMessage label="Transfer suggestions">
        <Text whiteSpace="pre-wrap">
          {rejectDecisionInfo.transferToAnotherJournal.transferSuggestions}
        </Text>
      </EditorialMessage>
    )}
  </>
)
