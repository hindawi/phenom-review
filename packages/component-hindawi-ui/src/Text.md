A piece of text. (Body 1)

```jsx
<Text fontWeight={700}>my boy is amazing</Text>
```

A piece of text with a bullet.

```jsx
import ActionLink from './ActionLink'
;<Text bullet display="inline">
  I am like a list item
</Text>
```

A secondary text. (Body 2)

```jsx
<Text secondary>my boy is amazing</Text>
```

Error text.

```jsx
<Text error>why error?</Text>
```

A text used for manuscript custom IDs.

```jsx
<Text customId>ID 444222</Text>
```

A text used for journal.

```jsx
<Text journal>text for journal</Text>
```

A small text.

```jsx
<Text small>my boy is amazing</Text>
```

A small secondary text.

```jsx
<Text small secondary>
  my boy is amazing
</Text>
```

Text used as a label line.

```jsx
<Text labelLine>SUPPLEMENTARY FILES</Text>
```
