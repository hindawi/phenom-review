/* eslint-disable sonarjs/no-duplicate-string */
import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th, override } from '@pubsweet/ui-toolkit'
import { border } from 'styled-system'

import Icon from '../Icon'
import { widthHelper } from '../styledHelpers'

// #region styled components
const validatedBorder = ({ validationStatus }) =>
  validationStatus === 'error' ? th('colorError') : th('colorFurniture')

const Root = styled.div`
  ${widthHelper}
`

const CloseOverlay = styled.div`
  background-color: transparent;
  position: fixed;
  bottom: 0;
  left: 0;
  top: 0;
  right: 0;
  z-index: 10;

  ${override('ui.Menu.CloseOverlay')};
`

const Label = styled.label`
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  display: block;
  ${override('ui.Label')};
  ${override('ui.Menu.Label')};
`

const Opener = styled.button.attrs(props => ({
  type: 'button',
}))`
  opacity: ${props => (props.disabled ? 0.7 : 1)};
  background: transparent;
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  border-color: ${validatedBorder};
  border-radius: ${th('borderRadius')};
  cursor: pointer;
  font-family: inherit;

  width: 100%;
  height: calc(${th('gridUnit')} * 12);
  padding: 0;

  display: flex;
  align-items: center;

  &:hover {
    border-color: ${props =>
      props.disabled ? th('colorBorder') : th('menu.hoverColor')};
  }

  &[disabled] {
    cursor: default;
  }

  ${override('ui.Menu.Opener')};

  ${border}
`

const Value = styled.span`
  border-right: ${th('borderWidth')} ${th('borderStyle')}
    ${th('colorFurniture')};
  flex-grow: 1;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
  flex-wrap: wrap;
  display: flex;
  text-align: left;
  line-height: calc(${th('gridUnit')} * 4);
  padding: 0 calc(${th('gridUnit')} * 2);

  &:hover {
    border-color: ${props =>
      props.disabled ? th('colorFurniture') : th('colorSecondary')};
  }

  &[disabled] {
    opacity: 0.3;
    color: ${th('textPrimaryColor')};
  }

  ${override('ui.Menu.Value')};
`

const Placeholder = styled(Value)`
  ${override('ui.Menu.Placeholder')};
`

const ArrowContainer = styled.span`
  width: calc(${th('gridUnit')} * 4);
  height: calc(${th('gridUnit')} * 4 - ${th('borderWidth')} * 2);

  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: calc(${th('gridUnit')} * 2);

  &[disabled] {
    cursor: default;
  }

  ${override('ui.Menu.ArrowContainer')};
`
const Main = styled.div.attrs(props => ({
  role: 'listbox',
}))`
  position: relative;

  ${override('ui.Menu.Main')};
`

const OptionsContainer = styled.div`
  position: absolute;
  left: 0;
  right: 0;

  background-color: goldenrod;

  ${override('ui.Menu.OptionsContainer')};
`

const Options = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;

  background-color: ${th('colorBackground')};
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  border-radius: ${th('borderRadius')};
  max-height: ${({ maxHeight }) => `${maxHeight}px`};
  overflow-y: auto;
  z-index: 100;

  ${override('ui.Menu.Options')};
`

const Option = styled.div.attrs(props => ({
  role: 'option',
  tabIndex: '0',
  'aria-selected': props.active,
}))`
  color: ${props => (props.active ? props.theme.textColor : '#444')};
  font-weight: ${props => (props.active ? '600' : 'inherit')};
  cursor: pointer;
  font-family: ${th('fontAuthor')};
  padding: calc(${th('gridUnit')} * 2 - ${th('borderWidth')} * 2)
    calc(${th('gridUnit')} * 4);
  border: ${th('borderWidth')} ${th('borderStyle')} transparent;
  border-width: ${th('borderWidth')} 0 ${th('borderWidth')} 0;
  white-space: nowrap;

  &:hover {
    background: ${th('colorBackgroundHue')};
    border-color: ${th('colorBorder')};
  }

  &:first-child:hover {
    border-top-color: ${th('colorBackgroundHue')};
  }

  &:last-child:hover {
    border-bottom-color: ${th('colorBackgroundHue')};
  }

  ${override('ui.Menu.Option')};
`
// #endregion

class Menu extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      open: false,
      selected: props.value,
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.value !== state.selected) {
      return { selected: props.value }
    }
    return null
  }

  toggleMenu = () => {
    this.setState({
      open: !this.state.open,
    })
  }

  resetMenu = props => {
    this.state = {
      open: false,
      selected: undefined,
    }
  }

  selectOneOfMultiElement = (event, value) => {
    event.stopPropagation()
    if (this.props.selectElement) this.props.selectElement(value)
  }

  removeSelect = (event, value) => {
    event.stopPropagation()
    let { selected } = this.state
    const index = selected.indexOf(value)
    selected = [...selected.slice(0, index), ...selected.slice(index + 1)]
    this.setState({ selected })
    if (this.props.onChange) this.props.onChange(selected)
  }

  handleSelect = ({ selected, open }) => {
    const { multi } = this.props
    let values
    if (multi) {
      values = this.state.selected ? this.state.selected : []
      if (values.indexOf(selected) === -1) values.push(selected)
    } else {
      values = selected
    }

    this.setState({
      open,
      selected: values,
    })
    if (this.props.onChange) this.props.onChange(values)
  }

  handleKeyPress = (event, selected, open) => {
    if (event.which === 13) {
      this.handleSelect(selected, open)
    }
  }

  optionLabel = value => {
    const { options } = this.props

    return options.find(option => option.value === value)
      ? options.find(option => option.value === value).label
      : ''
  }

  render() {
    const {
      maxHeight = 250,
      label,
      options,
      inline,
      placeholder,
      renderOption: RenderOption,
      renderOpener: RenderOpener,
      className,
      width,
      borderBottomRightRadius,
      borderTopRightRadius,
      multi,
      reset,
      ...rest
    } = this.props
    const { open, selected } = this.state

    if (reset === true) this.resetMenu(this.props)

    return (
      <Root className={className} inline={inline} width={width}>
        {label && <Label>{label}</Label>}
        {open && <CloseOverlay onClick={this.toggleMenu} />}
        <Main>
          <RenderOpener
            borderBottomRightRadius={borderBottomRightRadius}
            borderTopRightRadius={borderTopRightRadius}
            open={open}
            optionLabel={this.optionLabel}
            placeholder={placeholder}
            removeSelect={this.removeSelect}
            selected={selected}
            selectOneOfMultiElement={this.selectOneOfMultiElement}
            toggleMenu={this.toggleMenu}
            {...rest}
          />
          <OptionsContainer>
            {open && (
              <Options maxHeight={maxHeight}>
                {options.map(option => (
                  <RenderOption
                    data-test-id={`${rest['data-test-id']}-${option.value}`}
                    handleKeyPress={this.handleKeyPress}
                    handleSelect={this.handleSelect}
                    key={option.value}
                    label={option.label}
                    multi={multi}
                    selected={selected}
                    value={option.value}
                  />
                ))}
              </Options>
            )}
          </OptionsContainer>
        </Main>
      </Root>
    )
  }
}

const DefaultMenuOption = ({
  selected,
  label,
  value,
  handleSelect,
  handleKeyPress,
  multi,
  ...rest
}) => {
  const dataTestId = rest['data-test-id']
  const option = (
    <Option
      active={value === selected}
      data-test-id={dataTestId}
      key={value}
      onClick={() => handleSelect({ open: false, selected: value })}
      onKeyPress={event => handleKeyPress(event, value)}
    >
      {label || value}
    </Option>
  )

  if (!multi) return option
  return multi && !selected.includes(value) ? option : null
}

const DefaultOpener = ({
  toggleMenu,
  open,
  selected,
  placeholder,
  optionLabel,
  borderBottomRightRadius,
  borderTopRightRadius,
  validationStatus,
  disabled = false,
  ...rest
}) => {
  const dataTestId = rest['data-test-id']
  return (
    <Opener
      borderBottomRightRadius={borderBottomRightRadius}
      borderTopRightRadius={borderTopRightRadius}
      data-test-id={`${dataTestId}-filter`}
      disabled={disabled}
      onClick={toggleMenu}
      validationStatus={validationStatus}
    >
      {(!selected || selected.length === 0) && (
        <Placeholder>{placeholder}</Placeholder>
      )}
      {selected && !Array.isArray(selected) && (
        <Value disabled={disabled}>{optionLabel(selected)}</Value>
      )}
      {!disabled && (
        <ArrowContainer>
          <Icon
            color="colorFurnitureHue"
            icon={open ? 'caretUp' : 'caretDown'}
            open={open}
          />
        </ArrowContainer>
      )}
    </Opener>
  )
}

Menu.propTypes = {
  /** Menu items. */
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.any.isRequired,
    }),
  ).isRequired,
  /** Custom component for the selected option. */
  renderOpener: PropTypes.oneOfType([PropTypes.func, PropTypes.element]),
  /** Custom option component. The component will be rendered with *optionProps*. */
  renderOption: PropTypes.oneOfType([PropTypes.func, PropTypes.element]),
  reset: PropTypes.bool,
  /** Placeholder until a value is selected. */
  placeholder: PropTypes.string,
}

Menu.defaultProps = {
  renderOption: DefaultMenuOption,
  renderOpener: DefaultOpener,
  reset: false,
  placeholder: 'Choose in the list',
}

export default Menu
