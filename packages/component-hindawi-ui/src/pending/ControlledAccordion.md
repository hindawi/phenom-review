A controlled accordion component.

```js
class Master extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      expanded: false,
    }

    this.toggle = this.toggle.bind(this)
  }

  toggle() {
    this.setState(prev => ({
      expanded: !prev.expanded,
    }))
  }

  render() {
    const { expanded } = this.state
    return (
      <ControlledAccordion
        label="Controller accordion"
        expanded={expanded}
        toggle={this.toggle}
      >
        <div>peek a boo</div>
      </ControlledAccordion>
    )
  }
}

;<Master />
```
