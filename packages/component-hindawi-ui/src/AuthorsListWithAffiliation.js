import React from 'react'
import { get } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { compose, withProps } from 'recompose'

import { Row, Item, Text, Bullet } from '../'
import { parseAffiliations } from './helpers'

const getName = author =>
  `${get(author, 'alias.name.givenNames', '')} ${get(
    author,
    'alias.name.surname',
    '',
  )}`

const AuthorsListWithAffiliation = ({ authors, affiliationList }) => (
  <Root>
    {authors.map(author => (
      <Row alignItems="center" justify="flex-start" ml={2}>
        <Bullet mr={1} />
        <Text color={th('grey70')}>{getName(author)}</Text>
        {author.affiliationNumber && (
          <Superscript>{author.affiliationNumber}</Superscript>
        )}
      </Row>
    ))}

    <AffiliationColumn mt={1}>
      {affiliationList.map((aff, i) => (
        <Item data-test-id={`affiliation-${i + 1}`} flex={1} key={aff}>
          <Superscript>{i + 1}</Superscript>
          <Text color={th('grey70')}>{aff}</Text>
        </Item>
      ))}
    </AffiliationColumn>
  </Root>
)
export default compose(
  withProps(({ authors = [] }) => ({
    parsedAffiliations: parseAffiliations(authors),
  })),
  withProps(({ parsedAffiliations }) => ({
    authors: get(parsedAffiliations, 'authors', []),
    affiliationList: get(parsedAffiliations, 'affiliations', []),
  })),
)(AuthorsListWithAffiliation)

AuthorsListWithAffiliation.defaultProps = {
  authors: [],
}

// #region styles
const Root = styled.div`
  margin-top: 12px;
  align-items: center;
  display: flex;
  flex-flow: row wrap;

  font-family: ${th('defaultFont')};
`
const AffiliationColumn = styled(Row)`
  margin-top: 12px;
  align-items: flex-start;
  flex-direction: column;
`
const Superscript = styled.span`
  color: ${th('grey70')};
  position: relative;
  top: -0.5em;
  font-size: 80%;
  font-family: ${th('defaultFont')};
`

// #endregion
