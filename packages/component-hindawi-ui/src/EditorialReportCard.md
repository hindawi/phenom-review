Editorial Report Card

Card with message for the author

```js
const report = {
  id: '71effdc0-ccb1-4ea9-9422-dcc9f8713347',
  userId: '9ac5b5b5-252c-4933-9e66-72ec7c644a5c',
  comments: [
    {
      files: [],
      public: true,
      content: 'Public report here. Message for the author.',
    },
  ],
  createdOn: 1538053564396,
  updatedOn: 1538053600643,
  submittedOn: 1538053600624,
  recommendation: 'publish',
  recommendationType: 'editorialRecommendation',
  reviewer: {
    id: '9ac5b5b5-252c-4933-9e66-72ec7c644a5c',
    type: 'user',
    lastName: 'John',
    firstName: 'Doe',
    triageEditor: true,
    academicEditor: true,
  },
}
const decisions = [
  {
    value: 'publish',
    label: 'Publish',
  },
  {
    value: 'major',
    label: 'Major revision',
  },
  {
    value: 'minor',
    label: 'Minor revision',
  },
  {
    value: 'reject',
    label: 'Reject',
  },
]
;<EditorialReportCard
  report={report}
  decisions={decisions}
  publicLabel="Message For Author"
  privateLabel="Message For Editorial Team"
/>
```

Card with message for the editorial team

```js
const report = {
  id: '71effdc0-ccb1-4ea9-9422-dcc9f8713347',
  userId: '9ac5b5b5-252c-4933-9e66-72ec7c644a5c',
  comments: [
    {
      files: [],
      public: false,
      content: 'Confidential note is here. Message for editorial team.',
    },
  ],
  createdOn: 1538053564396,
  updatedOn: 1538053600643,
  submittedOn: 1538053600624,
  recommendation: 'publish',
  recommendationType: 'editorialRecommendation',
  reviewer: {
    id: '9ac5b5b5-252c-4933-9e66-72ec7c644a5c',
    type: 'user',
    lastName: 'John',
    firstName: 'Doe',
    triageEditor: true,
    academicEditor: false,
  },
}
const decisions = [
  {
    value: 'publish',
    label: 'Publish',
  },
  {
    value: 'major',
    label: 'Major revision',
  },
  {
    value: 'minor',
    label: 'Minor revision',
  },
  {
    value: 'reject',
    label: 'Reject',
  },
]
;<EditorialReportCard
  report={report}
  decisions={decisions}
  publicLabel="Message For Author"
  privateLabel="Message For Editorial Team"
/>
```

Card with message for the editorial team and for the author

```js
const report = {
  id: '71effdc0-ccb1-4ea9-9422-dcc9f8713347',
  userId: '9ac5b5b5-252c-4933-9e66-72ec7c644a5c',
  comments: [
    {
      files: [],
      public: false,
      content: 'Confidential note is here. Message for editorial team.',
    },
    {
      files: [],
      public: true,
      content: 'Public report here. Message for the author.',
    },
  ],
  createdOn: 1538053564396,
  updatedOn: 1538053600643,
  submittedOn: 1538053600624,
  recommendation: 'publish',
  recommendationType: 'editorialRecommendation',
  reviewer: {
    id: '9ac5b5b5-252c-4933-9e66-72ec7c644a5c',
    type: 'user',
    lastName: 'John',
    firstName: 'Doe',
    triageEditor: false,
    academicEditor: true,
  },
}
const decisions = [
  {
    value: 'publish',
    label: 'Publish',
  },
  {
    value: 'major',
    label: 'Major revision',
  },
  {
    value: 'minor',
    label: 'Minor revision',
  },
  {
    value: 'reject',
    label: 'Reject',
  },
]
;<EditorialReportCard
  report={report}
  decisions={decisions}
  publicLabel="Message For Author"
  privateLabel="Message For Editorial Team"
/>
```
