import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Row, Text, Icon } from '@hindawi/ui'

const Banner = ({ name, children }) => {
  const stored = JSON.parse(window.localStorage.getItem(name))
  const [showBanner, setShowBanner] = useState(stored === null ? true : stored)
  useEffect(() => {
    window.localStorage.setItem(name, showBanner)
  }, [name, showBanner])
  if (!showBanner) return null
  return (
    <Root justify="center">
      <BannerText align="center" pb={2} pt={2}>
        {children}
      </BannerText>
      <Icon
        fontSize="16px"
        icon="remove"
        ml={4}
        mr={4}
        onClick={() => setShowBanner(false)}
      />
    </Root>
  )
}

export default Banner

Banner.propTypes = {
  /** Closeable banner showed under the header */
  name: PropTypes.string.isRequired,
  /** The name of the variable stored in the browser storage */
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  /** Children to render */
}

const Root = styled(Row)`
  background-color: ${th('infoColor')};
  position: sticky;
  z-index: 1;
  top: ${th('appBar.height')};
`

const BannerText = styled(Text)`
  line-height: ${th('bannerTextLineHeight')};
  font-size: ${th('bannerTextSize')};
`
