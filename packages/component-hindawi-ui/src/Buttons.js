import React from 'react'
import { Button } from '@pubsweet/ui'

import Icon from './Icon'

const Buttons = () => (
  <React.Fragment>
    <code>{`<Button primary />`} test</code>
    <Button mb={4} primary>
      Primary full width
    </Button>

    <code>{`<Button primary width={16} />`}</code>
    <Button mb={4} primary width={16}>
      Primary button
    </Button>

    <code>{`<Button primary width={16} />`}</code>
    <Button mb={4} primary width={16}>
      <Icon icon="checks" mr={1} />
      Primary icon button
    </Button>

    <code>{`<Button primary small width={16} />`}</code>
    <Button mb={4} primary small width={16}>
      Small primary button
    </Button>

    <code>{`<Button disabled primary width={12} />`}</code>
    <Button disabled mb={4} primary width={12}>
      Primary disabled
    </Button>

    <code>{`<Button disabled primary width={12} />`}</code>
    <Button disabled mb={4} primary width={12}>
      <Icon icon="checks" mr={1} />
      Primary disabled with icon
    </Button>

    <hr />

    <code>{`<Button />`}</code>
    <Button mb={4}>Secondary full width</Button>

    <code>{`<Button small />`}</code>
    <Button mb={4} small>
      Small secondary
    </Button>

    <code>{`<Button small disabled />`}</code>
    <Button disabled mb={4} small>
      Small secondary disabled
    </Button>

    <code>{`<Button disabled />`}</code>
    <Button disabled mb={4}>
      <Icon icon="checks" mr={1} />
      Full width, secondary disabled with icon
    </Button>

    <hr />

    <code>{`<Button xs />`}</code>
    <Button xs>XS Button</Button>
  </React.Fragment>
)

export default Buttons
