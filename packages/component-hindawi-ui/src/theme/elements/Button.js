/* eslint-disable sonarjs/no-duplicate-string */
import { space } from 'styled-system'
import { css } from 'styled-components'
import { lighten, th } from '@pubsweet/ui-toolkit'

const primary = css`
  background-color: ${th('actionPrimaryColor')};
  border: none;
  color: ${th('white')};

  &:hover {
    background-color: ${lighten('actionPrimaryColor', 0.1)};
  }

  &:focus,
  &:active {
    background-color: ${th('actionPrimaryColor')};
    outline: none;
  }

  &[disabled] {
    border: none;
    &,
    &:hover {
      background-color: ${th('furnitureColor')};
    }
  }
`
const secondary = css`
  background: none;
  border: ${th('buttons.borderSize')} solid ${th('grey70')};
  border-radius: ${th('borderRadius')};
  color: ${th('grey70')};

  &:hover {
    background: ${th('grey30')};
  }

  &:focus,
  &:active {
    background: ${th('grey30')};
    outline: none;
  }

  &[disabled] {
    border-color: ${th('grey40')};
    color: ${th('grey40')};

    &:hover {
      background: none;
    }
  }
`
const outline = css`
  background: none;
  color: ${th('grey70')};
`

const invert = props =>
  props.secondary &&
  css`
    background-color: ${th('textPrimaryColor')};
    border: ${th('buttons.borderSize')} solid ${th('textPrimaryColor')};
    color: ${th('white')};

    &:hover {
      border-color: ${lighten('textPrimaryColor', 0.2)};
      background-color: ${lighten('textPrimaryColor', 0.2)};
    }

    &:focus,
    &:active {
      outline: none;
      border-color: ${th('textPrimaryColor')};
      background-color: ${th('textPrimaryColor')};
    }

    &[disabled] {
      border-color: ${th('colorBorder')};
      color: ${th('colorTextPlaceholder')};
      background-color: ${th('colorBorder')}
      opacity: 1;
    }
  `

const selected = css`
  background-color: black;
  color: ${th('white')};
  border-color: black;
  &:hover,
  &:focus,
  &:active {
    border-color: black;
    background-color: black;
    color: ${th('white')};
  }
`

const buttonWidth = props =>
  props.width
    ? css`
        min-width: calc(${th('gridUnit')} * ${props.width});
      `
    : css`
        width: 100%;
      `

const buttonSize = props => {
  if (props.medium) {
    return css`
      height: calc(${th('gridUnit')} * 8);
      font-size: ${th('buttons.medium.fontSize')};
      min-width: calc(${th('gridUnit')} * 26);
    `
  } else if (props.small) {
    return css`
      height: calc(${th('gridUnit')} * 8);
      font-size: ${th('buttons.small.fontSize')};
      min-width: calc(${th('gridUnit')} * 32);
    `
  } else if (props.xs) {
    return css`
      height: calc(${th('gridUnit')} * 6);
      font-size: ${th('buttons.small.fontSize')};
      line-height: calc(
        ${th('gridUnit')} * 3 - ${th('buttons.borderSize')} * 2
      );
      min-width: calc(${th('gridUnit')} * ${th('button.smallWidth')});
    `
  } else if (props.square) {
    return css`
      font-weight: 600;
      height: calc(${th('gridUnit')} * 9);
      min-width: calc(${th('gridUnit')} * 9);
      border-width: calc(${th('gridUnit')} / 4);

      &[disabled] {
        background-color: ${th('contrastGrayColor')};
        color: ${th('white')};
        border-color: ${th('contrastGrayColor')};
        pointer-events: none;
      }
    `
  }
  return css`
    font-size: ${th('buttons.default.fontSize')};
    height: calc(${th('gridUnit')} * ${th('buttons.default.height')});

    ${buttonWidth};
  `
}

const getButtonType = props => {
  if (props.primary) return primary
  else if (props.outline) return outline
  return secondary
}

export default css`
  align-items: center;
  cursor: pointer;
  display: flex;
  justify-content: center;
  font-family: ${th('defaultFont')};
  font-weight: 700;

  padding: 0 calc(${th('gridUnit')} * 3);

  &:hover,
  &:focus {
    transition: none;
  }

  ${props => getButtonType(props)};
  ${props => (props.selected ? selected : '')}
  ${props => props.invert && invert}
  ${buttonSize};
  ${space};
`
