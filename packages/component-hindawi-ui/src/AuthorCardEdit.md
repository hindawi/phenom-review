```js
const author = {
  alias: {
    aff: 'Hin',
    email: 'anca@gmail.com',
    country: 'Romania',
    name: {
      givenNames: 'Anca',
      surname: 'Ursachi',
    },
  },
  isSubmitting: true,
  isCorresponding: true,
}
;<div>
  <AuthorCardEdit index={0} item={author} saveAuthor={() => {}} />
</div>
```
