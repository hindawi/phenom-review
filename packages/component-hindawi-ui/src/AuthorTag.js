import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { Tag, Text, heightHelper, ActionLink } from '../'

const AuthorTag = ({
  author: {
    id,
    alias: {
      aff,
      email,
      country,
      name: { surname, givenNames },
    },
    affiliationNumber,
    isSubmitting,
    isCorresponding,
  },
  hasStaffRole,
}) => (
  <Root data-test-id={`author-tag-${id}`}>
    {hasStaffRole ? (
      <ActionLink>{`${givenNames} ${surname}`}</ActionLink>
    ) : (
      <Text>{`${givenNames} ${surname}`}</Text>
    )}
    {isSubmitting && (
      <Tag authorTag ml={1}>
        SA
      </Tag>
    )}
    {isCorresponding && (
      <Tag authorTag ml={1} mr={1}>
        CA
      </Tag>
    )}
    {affiliationNumber && <Superscript>{affiliationNumber}</Superscript>}
  </Root>
)

export default AuthorTag

// #region styles
const Root = styled.div`
  align-items: center;
  display: flex;

  ${heightHelper};
`
const Superscript = styled.span`
  position: relative;
  top: -0.5em;
  font-size: 80%;
  font-family: ${th('defaultFont')};
`
// #endregion
