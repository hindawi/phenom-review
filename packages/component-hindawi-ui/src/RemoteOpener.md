Toggle a boolean flag and pass it around in your React components tree.

```js
<RemoteOpener>
  {({ expanded, toggle }) => (
    <div>
      <button onClick={toggle}>Toggle</button>
      <span>{expanded ? 'Collapse me!' : 'Expand me!'}</span>
    </div>
  )}
</RemoteOpener>
```
