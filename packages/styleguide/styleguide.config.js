const fs = require('fs')
const path = require('path')

const getSections = () =>
  fs
    .readdirSync('../')
    .filter(
      dir =>
        dir.startsWith('component-') &&
        fs.existsSync(`../${dir}/client/components`),
    )
    .map(dir => ({
      name: dir.split('-')[1],
      sectionDepth: 1,
      components: [`../${dir}/client/components/[A-Z]*.js`],
    }))

module.exports = {
  sections: [
    {
      name: 'Hindawi UI',
      sectionDepth: 1,
      components: ['../component-hindawi-ui/src/[A-Z]*.js'],
    },
    {
      name: 'Peer Review',
      sectionDepth: 1,
      components: [
        '../component-hindawi-ui/src/peerReviewComponents/[A-Z]*.js',
      ],
    },
    {
      name: 'Modals UI',
      sectionDepth: 1,
      components: ['../component-hindawi-ui/src/modals/[A-Z]*.js'],
    },
    ...getSections(),
  ],
  serverPort: process.env.NODE_ENV !== 'production' ? 6060 : 3000,
  skipComponentsWithoutExample: true,
  pagePerSection: true,
  styleguideComponents: {
    Wrapper: path.join(__dirname, 'src/Wrapper'),
  },
}
