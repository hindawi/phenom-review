process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const logger = require('@pubsweet/logger')
const fixturesService = require('fixture-service')

const resetPasswordUseCase = require('../src/use-cases/resetPassword')

jest.mock('@pubsweet/logger', () => ({
  error: jest.fn(),
}))

const chance = new Chance()
const { fixtures, models } = fixturesService
const { User, Identity } = models

describe('Reset password use case', () => {
  it('returns success when the body is correct', async () => {
    const user = fixtures.generateUser({
      properties: {
        passwordResetToken: chance.guid(),
      },
      User,
      Identity,
    })

    await resetPasswordUseCase.initialize(logger, models).execute({
      email: user.identities[0].email,
      password: 'Password!23',
      token: user.passwordResetToken,
    })

    expect(user.passwordResetTimestamp).toBeNull()
    expect(user.passwordResetToken).toBeNull()
  })

  it('returns an error when the password is weak', async () => {
    const result = resetPasswordUseCase.initialize(logger, models).execute({
      email: chance.email(),
      password: 'weak-password',
      token: chance.guid(),
    })

    return expect(result).rejects.toThrow(
      'Password is too weak. Please check password requirements.',
    )
  })

  it('returns an error when tokens do not match', async () => {
    const user = fixtures.generateUser({
      properties: {
        passwordResetToken: chance.guid(),
      },
      User,
      Identity,
    })

    const result = resetPasswordUseCase.initialize(logger, models).execute({
      email: user.identities[0].email,
      password: 'Password!23',
      token: chance.hash(),
    })

    return expect(result).rejects.toThrow('Invalid request.')
  })
})
