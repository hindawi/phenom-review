const { cookieService } = require('component-cookie-service')

const cookiesKeys = {
  userId: 'puid',
}

const initCookieService = context => {
  const { req, res } = context
  return cookieService({ req, res })
}

const setUserId = context => {
  const { user } = context
  const cookies = initCookieService(context)

  const ms = 1000
  const seconds = 60
  const minutes = 60
  const hours = 24
  const days = 365

  const maxAge = ms * seconds * minutes * hours * days // 1 year

  return cookies.set({
    cookie: cookiesKeys.userId,
    value: user,
    options: {
      maxAge,
      httpOnly: false,
    },
  })
}

const removeUserId = context => {
  const cookies = initCookieService(context)
  return cookies.remove(cookiesKeys.userId)
}

module.exports = {
  setUserId,
  removeUserId,
}
