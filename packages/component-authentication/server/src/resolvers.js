const { logger } = require('component-logger')

const models = require('@pubsweet/models')

const { withAuthsomeMiddleware } = require('helper-service')
const { token: tokenService } = require('pubsweet-server/src/authentication')

const useCases = require('./use-cases')
const {
  useCases: { currentUserUseCase },
} = require('component-model')
const notification = require('./notifications/notification')
const cookies = require('./cookies')

const resolvers = {
  Query: {
    async currentUser(_, { input }, ctx) {
      cookies.setUserId(ctx)
      return currentUserUseCase.initialize(models).execute({ userId: ctx.user })
    },
  },
  Mutation: {
    async signUp(_, { input }, ctx) {
      return useCases.signUpUseCase
        .initialize(notification, tokenService, models)
        .execute(input)
    },
    async signUpFromInvitation(_, { input }, ctx) {
      return useCases.signUpFromInvitationUseCase
        .initialize({ logger, tokenService, models })
        .execute(input)
    },
    async requestPasswordReset(_, { email }, ctx) {
      return useCases.requestPasswordResetUseCase
        .initialize(notification, models)
        .execute(email)
    },
    async resetPassword(_, { input }, ctx) {
      return useCases.resetPasswordUseCase
        .initialize(logger, models)
        .execute(input)
    },
    async confirmAccount(_, { input }, ctx) {
      return useCases.confirmAccountUseCase
        .initialize({ logger, tokenService, models })
        .execute(input)
    },
    async localLogin(_, { input }, ctx) {
      return useCases.localLoginUseCase
        .initialize(tokenService, models)
        .execute(input)
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, {
  ...useCases,
  currentUserUseCase,
})
