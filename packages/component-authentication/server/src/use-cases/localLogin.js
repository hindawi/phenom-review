const initialize = (tokenService, { Identity }) => ({
  execute: async ({ email, password }) => {
    const identity = await Identity.findOneByEmail(email, 'user')

    if (!identity) {
      throw new ValidationError('Wrong username or password.')
    }

    const { user } = identity
    if (!(await identity.validPassword(password)) || !user.isActive) {
      throw new ValidationError('Wrong username or password.')
    }

    const token = tokenService.create({
      username: identity.email,
      id: identity.userId,
    })

    return { token }
  },
})

const authsomePolicies = ['public']

module.exports = {
  initialize,
  authsomePolicies,
}
