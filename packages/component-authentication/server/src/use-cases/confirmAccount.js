const initialize = ({ logger, tokenService, models: { User } }) => ({
  execute: async input => {
    const INVALID_REQ_MSG = 'Invalid request.'
    const user = await User.find(input.userId, 'identities')
    if (!user) throw new Error(INVALID_REQ_MSG)

    const identity = user.identities.find(identity => identity.type === 'local')
    if (!identity) throw new Error(INVALID_REQ_MSG)

    if (user.confirmationToken !== input.token) {
      logger.warn(
        `User confirmation tokens do not match: REQ ${input.token} vs. DB ${user.confirmationToken}`,
      )
      throw new Error(INVALID_REQ_MSG)
    }

    if (!user.isActive) throw new Error('User has been deactivated.')
    if (identity.isConfirmed)
      throw new ConflictError('User has already been confirmed.')

    identity.isConfirmed = true
    await identity.save()

    user.confirmationToken = null

    await user.save()

    const token = tokenService.create({
      username: identity.email,
      id: user.id,
    })

    return { token }
  },
})

const authsomePolicies = ['public']

module.exports = {
  initialize,
  authsomePolicies,
}
