const getEmailCopy = ({ emailType, role }) => {
  let paragraph
  let hasIntro = true
  let hasSignature = true
  switch (emailType) {
    case 'user-signup':
      paragraph = `Thank you for creating an account on Hindawi’s review system.
        To submit a manuscript and access your dashboard, please confirm your account by clicking on the link below.`
      break
    case 'user-forgot-password':
      hasIntro = false
      hasSignature = false
      paragraph = `You have requested to reset your password. In order to reset your password please click the link below.`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return { paragraph, hasLink: true, hasIntro, hasSignature }
}

module.exports = {
  getEmailCopy,
}
