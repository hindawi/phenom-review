import { compose, withHandlers } from 'recompose'
import { withSteps, withFetching } from '@hindawi/ui'

import { SignUpForm } from '../components'
import withAuthenticationGQL from '../graphql'
import { setToken, getRedirectTo, parseError } from '../utils'

export default compose(
  withSteps,
  withFetching,
  withAuthenticationGQL,
  withHandlers({
    onSubmit: ({
      step,
      signUp,
      history,
      setError,
      location,
      nextStep,
      loginUser,
      setFetching,
    }) => (values, formProps) => {
      if (step === 0) {
        nextStep()
        formProps.resetForm(values)
      } else {
        setFetching(true)
        const { confirmPassword, ...input } = values
        signUp({
          variables: { input },
        })
          .then(r => {
            setFetching(false)
            setToken(r.data.signUp.token)
            history.push(getRedirectTo(location))
          })
          .catch(e => {
            setFetching(false)
            setError(parseError(e))
          })
      }
    },
  }),
)(SignUpForm)
