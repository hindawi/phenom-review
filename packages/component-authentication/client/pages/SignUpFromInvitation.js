import { compose, withHandlers, withProps } from 'recompose'
import { withSteps, withFetching } from '@hindawi/ui'

import { SignUpForm } from '../components'
import withAuthenticationGQL from '../graphql'
import {
  getRedirectTo,
  parseSearchParams,
  setToken,
  parseError,
} from '../utils'

export default compose(
  withSteps,
  withFetching,
  withAuthenticationGQL,
  withProps(({ location }) => ({
    initialValues: parseSearchParams(location.search),
    isInvited: true,
  })),
  withHandlers({
    onSubmit: ({
      step,
      history,
      setError,
      location,
      nextStep,
      setFetching,
      signUpFromInvitation,
    }) => (values, formProps) => {
      if (step === 0) {
        nextStep()
        formProps.resetForm(values)
      } else {
        const { userId, confirmPassword, ...input } = values
        setFetching(true)
        window.localStorage.removeItem('token')
        signUpFromInvitation({
          variables: { input },
        })
          .then(r => {
            setFetching(false)
            setToken(r.data.signUpFromInvitation.token)
            history.push(getRedirectTo(location))
          })
          .catch(e => {
            setFetching(false)
            setError(parseError(e))
          })
      }
    },
  }),
)(SignUpForm)
