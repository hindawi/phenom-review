import React from 'react'
import { get } from 'lodash'
import { Query } from 'react-apollo'
import { Redirect } from 'react-router'
import { Spinner } from '@pubsweet/ui'

import { queries } from '../graphql'

const toLogin = location => ({
  pathname: '/login',
  state: {
    from: location,
  },
})
const AuthenticatedComponent = ({ children, location }) => (
  <Query query={queries.currentUser}>
    {({ loading, data }) => {
      if (!window.localStorage.getItem('token')) {
        return <Redirect to={toLogin(location)} />
      }
      if (loading) return <Spinner />

      if (!get(data, 'currentUser')) {
        return <Redirect to={toLogin(location)} />
      }

      return typeof children === 'function'
        ? children({ currentUser: get(data, 'currentUser') })
        : children
    }}
  </Query>
)

export default AuthenticatedComponent
