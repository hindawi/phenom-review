import React, { useContext, useEffect } from 'react'
import { get } from 'lodash'
import { Spinner } from '@pubsweet/ui'
import { useQuery } from 'react-apollo'
import { Redirect } from 'react-router-dom'
import { useKeycloak, useKeycloakRole } from 'component-sso/client'

import { queries } from './graphql'
import { setToken, removeToken } from './utils'
import CurrentUserManager, {
  CurrentUserContext,
} from './components/CurrentUserManager'

import { TrackedRoute } from './TrackedRoute'

const toLogin = location => ({
  pathname: '/login',
  state: {
    from: location,
  },
})

const AuthenticatedComponent = ({
  component: Component,
  location,
  ...rest
}) => {
  const { loading, data } = useQuery(queries.currentUser)
  const currentUser = get(data, 'currentUser')

  if (!window.localStorage.getItem('token')) {
    return <Redirect to={toLogin(location)} />
  }

  if (loading) return <Spinner />

  if (!currentUser) {
    return <Redirect to={toLogin(location)} />
  }

  return (
    <TrackedRoute
      render={routerProps => (
        <Component currentUser={currentUser} {...routerProps} />
      )}
      {...rest}
    />
  )
}

const AuthenticatedKeycloakComponent = ({
  component,
  keycloak,
  location,
  ...rest
}) => {
  const { loading, data } = useQuery(queries.currentUser)
  const currentUser = get(data, 'currentUser')
  const { setCurrentUser } = useContext(CurrentUserContext)

  useEffect(() => {
    if (currentUser) {
      setCurrentUser(currentUser)
    }
  }, [data])

  if (!window.localStorage.getItem('token')) {
    keycloak.login()
    return null
  }

  if (loading) return <Spinner />

  if (!currentUser) {
    removeToken()
    keycloak.logout()
    return <Redirect to={toLogin(location)} />
  }

  return <TrackedRoute component={component} {...rest} />
}

const AuthenticatedKeycloakRoute = ({
  keycloak,
  component: Component,
  location,
  ...rest
}) => {
  if (keycloak.authenticated) {
    if (!window.localStorage.getItem('token')) {
      setToken(keycloak.token)
    }
  } else {
    removeToken()
    keycloak.login()

    return null
  }

  return (
    <CurrentUserManager>
      <AuthenticatedKeycloakComponent
        component={Component}
        keycloak={keycloak}
        location={location}
        {...rest}
      />
    </CurrentUserManager>
  )
}

const AuthenticatedRoute = props => {
  const keycloak = useKeycloak()
  const { hasAccessRights } = useKeycloakRole()

  if (keycloak) {
    if (!hasAccessRights(props.allow)) {
      return <Redirect to="/" />
    }

    return <AuthenticatedKeycloakRoute keycloak={keycloak} {...props} />
  }
  return <AuthenticatedComponent {...props} />
}

export default AuthenticatedRoute
