export const publisher = {
  links: {
    websiteLink: 'https://www.hindawi.com/',
    privacyLink: 'https://www.hindawi.com/privacy/',
    termsLink: 'https://www.hindawi.com/terms/',
    ethicsLink: 'https://www.hindawi.com/ethics/',
    coiLink: 'https://www.hindawi.com/ethics/#conflicts-of-interest',
    apcLink: 'https://www.hindawi.com/journals/{journalCode}/apc',
    journalAuthorsGuidelinesLink:
      'https://www.hindawi.com/journals/{journalCode}/guidelines/ ',
    authorsGuidelinesLink: 'https://www.hindawi.com/publish-research/authors/',
  },
}

export const journal = {
  statuses: [],
}
