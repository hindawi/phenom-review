import React from 'react'
import 'jest-styled-components'
import '@testing-library/jest-dom/extend-expect'
import { cleanup, fireEvent } from '@testing-library/react'

import { render } from './testUtils'
import ResetPasswordForm from '../components/ResetPasswordForm'

describe('Reset Password Form', () => {
  afterEach(cleanup)

  it('should not submit if validation fails', async done => {
    const requestResetPasswordMock = jest.fn()
    const { getByText, getAllByText } = render(
      <ResetPasswordForm requestResetPassword={requestResetPasswordMock} />,
    )

    fireEvent.click(getByText(/send email/i))

    setTimeout(() => {
      expect(getAllByText(/required/i)).toBeTruthy()
      expect(requestResetPasswordMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('should validate empty input', async done => {
    const requestResetPasswordMock = jest.fn()
    const { getByTestId, getByText, getAllByText } = render(
      <ResetPasswordForm requestResetPassword={requestResetPasswordMock} />,
    )

    fireEvent.blur(getByTestId('email-reset-password'), {
      target: { value: '' },
    })

    fireEvent.click(getByText(/send email/i))
    setTimeout(() => {
      expect(getAllByText(/required/i)).toBeTruthy()
      expect(requestResetPasswordMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('should validate invalid email', async done => {
    const requestResetPasswordMock = jest.fn()
    const { getByTestId, getByText } = render(
      <ResetPasswordForm requestResetPassword={requestResetPasswordMock} />,
    )

    fireEvent.change(getByTestId('email-reset-password'), {
      target: { value: 'email' },
    })
    fireEvent.click(getByText(/send email/i))

    setTimeout(() => {
      expect(getByText(/Invalid email/i)).toBeInTheDocument()
      expect(requestResetPasswordMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('should return error if something is wrong', async done => {
    const { getByText } = render(
      <ResetPasswordForm fetchingError="Oops! Something went wrong..." />,
    )

    setTimeout(() => {
      expect(getByText(/Oops! Something went wrong.../i)).toBeInTheDocument()
      done()
    })
  })

  it('should call reset password form if everything is valid', async done => {
    const requestResetPasswordMock = jest.fn()
    const { getByTestId, getByText } = render(
      <ResetPasswordForm requestResetPassword={requestResetPasswordMock} />,
    )

    fireEvent.change(getByTestId('email-reset-password'), {
      target: { value: 'email@gmail.com' },
    })
    fireEvent.click(getByText(/send email/i))

    setTimeout(() => {
      expect(requestResetPasswordMock).toHaveBeenCalledTimes(1)
      expect(requestResetPasswordMock).toHaveBeenCalledWith(
        {
          email: 'email@gmail.com',
        },
        expect.anything(),
      )
      done()
    })
  })
})
