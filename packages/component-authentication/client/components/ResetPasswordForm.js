import React, { Fragment } from 'react'
import { Formik } from 'formik'
import { Button, H2, Spinner, TextField } from '@pubsweet/ui'

import {
  Row,
  Item,
  Text,
  Label,
  ShadowedBox,
  ValidatedFormField,
  validators,
} from '@hindawi/ui'

const ResetPasswordForm = ({
  history,
  isFetching,
  fetchingError,
  requestResetPassword,
}) => (
  <Formik onSubmit={requestResetPassword}>
    {({ handleSubmit }) => (
      <ShadowedBox center mt={10}>
        <H2>Reset password</H2>
        <Row mt={6}>
          <Item vertical>
            <Label required>Email</Label>
            <ValidatedFormField
              component={TextField}
              data-test-id="email-reset-password"
              inline
              name="email"
              validate={[validators.required, validators.emailValidator]}
            />
          </Item>
        </Row>

        {fetchingError && (
          <Row justify="flex-start" mt={4}>
            <Text error>{fetchingError}</Text>
          </Row>
        )}

        <Row mt={6}>
          {isFetching ? (
            <Spinner />
          ) : (
            <Fragment>
              <Button fullWidth mr={2} onClick={() => history.goBack()}>
                BACK
              </Button>
              <Button fullWidth ml={2} onClick={handleSubmit} primary>
                SEND EMAIL
              </Button>
            </Fragment>
          )}
        </Row>
      </ShadowedBox>
    )}
  </Formik>
)

export default ResetPasswordForm
