import React from 'react'
import { get } from 'lodash'
import { Query } from 'react-apollo'
import { withRouter } from 'react-router'
import { useJournal } from 'component-journal-info'
import { compose, withProps, withHandlers } from 'recompose'
import { useKeycloak } from 'component-sso/client'
import useKeycloakRole from 'component-sso/client/useKeycloakRole'

import { queries } from '../../graphql'
import { AuthenticatedAppBarContent, UnauthenticatedAppBarContent } from './'

const unauthedPaths = [
  '/login',
  '/signup',
  '/invite',
  '/eqs-decision',
  '/confirm-signup',
  '/password-reset',
  '/forgot-password',
  '/emails/decline-review',
  '/emails/accept-review-new-user',
]

const submitButtonPaths = ['/', '/ea-dashboard', '/admin-dashboard']

const AppBar = ({
  goTo,
  logout,
  goToDashboard,
  isUnauthedRoute,
  location,
  keycloak,
}) => {
  const { logo, name } = useJournal()
  const isUnauthenticated = !keycloak.authenticated
  const authorizationInfo = useKeycloakRole()

  if (isUnauthedRoute || isUnauthenticated) {
    return (
      <UnauthenticatedAppBarContent goTo={goTo} logo={logo} publisher={name} />
    )
  }

  const showSubmitButton = submitButtonPaths.includes(location.pathname)
  const transferButtonFeatureFlag = false
  const showTransferButton =
    transferButtonFeatureFlag &&
    (authorizationInfo.hasEARole() || authorizationInfo.hasAdminRole())
  return (
    <Query query={queries.currentUser}>
      {({ data, loading, client }) => {
        const currentUser = get(data, 'currentUser', null)

        return loading || !currentUser ? null : (
          <AuthenticatedAppBarContent
            currentUser={currentUser}
            goTo={goTo}
            goToDashboard={goToDashboard}
            logo={logo}
            logout={logout(client)}
            publisher={name}
            showSubmitButton={showSubmitButton}
            showTransferButton={showTransferButton}
          />
        )
      }}
    </Query>
  )
}

export default compose(
  withRouter,
  withProps(({ location }) => ({
    isUnauthedRoute: unauthedPaths.includes(location.pathname),
    keycloak: useKeycloak(),
  })),
  withHandlers({
    goToDashboard: ({ history }) => () => {
      history.push('/')
    },
    goTo: ({ history }) => path => {
      history.push(path)
    },
    logout: ({ history, keycloak }) => gqlClient => () => {
      gqlClient.clearStore()
      window.localStorage.removeItem('token')

      if (keycloak) {
        // * Redirect to dashboard
        history.push('/')
        keycloak.logout()
      } else {
        history.replace('/login')
      }
    },
  }),
)(AppBar)
